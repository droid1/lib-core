# Lib-Core

Core library for Android.
It includes various base classes and helper/utility classes for general Android apps.




_Note: This library was mostly written several years ago (at the time of Gingerbread through Kitkat) and it may be a bit outdated at this point (despite what the git history indicates). This will be updated in the near future, if that makes sense, to use more recent Android APIs._


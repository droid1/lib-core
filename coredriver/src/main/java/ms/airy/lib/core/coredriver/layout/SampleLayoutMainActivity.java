package ms.airy.lib.core.coredriver.layout;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import ms.airy.lib.core.coredriver.R;

public class SampleLayoutMainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample_layout_main);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new SampleLayoutMainFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.sample_layout_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if (id == R.id.action_layout_premiere_layout) {
            doPremiereLayout();
            return true;
        } else if (id == R.id.action_layout_graffiti_layout) {
            doGraffitiLayout();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void doPremiereLayout()
    {

    }
    private void doGraffitiLayout()
    {

    }



    /**
     * A placeholder fragment containing a simple view.
     */
    public static class SampleLayoutMainFragment extends Fragment {

        public SampleLayoutMainFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_sample_layout_main, container, false);
            return rootView;
        }
    }
}

package ms.airy.lib.core.coredriver.help;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;

import java.util.ArrayList;
import java.util.List;

import ms.airy.lib.core.coredriver.R;
import ms.airy.lib.core.help.common.QuestionInfo;
import ms.airy.lib.core.help.helper.QuestionInfoRegistry;
import ms.airy.lib.core.help.ui.QuestionInfoListFragment;


public class SampleQuestionInfoListActivity extends Activity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample_question_info_list);

        if (savedInstanceState == null) {
            FragmentTransaction transaction = getFragmentManager().beginTransaction();

            // TBD:
            // Is there a better way?
            List<QuestionInfo> questionInfos = new ArrayList<QuestionInfo>();
            // Populate questionInfos here.
            // ....


            QuestionInfo questionInfo1 = new QuestionInfo(1L);
            questionInfo1.setQuestionAndAnswerText(
                    "short question 1",
                    "short answer 1"
            );
            questionInfos.add(questionInfo1);

            QuestionInfo questionInfo2 = new QuestionInfo(22L);
            questionInfo2.setQuestionAndAnswerText(
                    "long question 2 abc def ghi jkl mno pqr stu vwx yzz abc def ghi jkl mno pqr stu vwx yzz",
                    "long answer 2 abc def ghi jkl mno pqr stu vwx yzz  abc def ghi jkl mno pqr stu vwx yzz  abc def ghi jkl mno pqr stu vwx yzz  abc def ghi jkl mno pqr stu vwx yzz  abc def ghi jkl mno pqr stu vwx yzz "
            );
            questionInfos.add(questionInfo2);

            QuestionInfo questionInfo3 = new QuestionInfo(3L);
            String question3 = "Hello World!!!!";
            String answer3 = "Internal Url: <a href=\"question:///22\">Question 2</a><br>"
                    + "External Url: <a href=\"http://www.google.com/\">Google</a>";
            questionInfo3.setQuestionAndAnswerText(
                    question3,
                    answer3

            );
            questionInfos.add(questionInfo3);



            QuestionInfo questionInfo4 = new QuestionInfo(4L);
            questionInfo4.setQuestionAndAnswerText(
                    "short question 4",
                    "short answer 4"
            );
            questionInfos.add(questionInfo4);


            QuestionInfo questionInfo5 = new QuestionInfo(5L);
            questionInfo5.setQuestionAndAnswerText(
                    "short question 5",
                    "short answer 5"
            );
            questionInfos.add(questionInfo5);


            QuestionInfo questionInfo6 = new QuestionInfo(6L);
            questionInfo6.setQuestionAndAnswerText(
                    "short question 6",
                    "short answer 6"
            );
            questionInfos.add(questionInfo6);



            QuestionInfoRegistry.getInstance(this).setQuestionInfos(questionInfos);

            // QuestionInfoListFragment uses QuestionInfoRegistry questionInfo list implicitly.
            QuestionInfoListFragment questionInfoListFragment = QuestionInfoListFragment.newInstance();
            transaction.add(R.id.container, questionInfoListFragment);

            transaction.commit();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.sample_question_info_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}

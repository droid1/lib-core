package ms.airy.lib.core.coredriver.dialog;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import ms.airy.lib.core.coredriver.R;
import ms.airy.lib.core.dialog.ActionableAlertDialogListener;
import ms.airy.lib.core.dialog.ActionableAssertDialogListener;
import ms.airy.lib.core.dialog.ActionableChoiceDialogListener;
import ms.airy.lib.core.dialog.stock.ActionableImageDialogFragment;
import ms.airy.lib.core.dialog.stock.ActionableItemListDialogFragment;
import ms.airy.lib.core.dialog.stock.ActionableResponseDialogFragment;
import ms.airy.lib.core.dialog.stock.ActionableTimerDialogFragment;
import ms.airy.lib.core.dialog.stock.ActionableWebViewDialogFragment;
import ms.airy.lib.core.dialog.stock.ActionableYesNoDialogFragment;

import java.net.MalformedURLException;
import java.net.URL;


public class SampleDialogMainActivity extends Activity implements ActionableAlertDialogListener, ActionableAssertDialogListener, ActionableChoiceDialogListener
{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample_dialog_main);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new SampleDialogMainFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.sample_dialog_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_dialog_show_confirm_dialog) {
            doConfirmDialog();
            return true;
        } else if (id == R.id.action_dialog_show_yesno_dialog) {
            doYesNoDialog();
            return true;
        } else if (id == R.id.action_dialog_show_multiplechoice_dialog) {
            doMultipleChoiceDialog();
            return true;
        } else if (id == R.id.action_dialog_show_singlechoice_dialog) {
            doSingleChoiceDialog();
            return true;
        } else if (id == R.id.action_dialog_show_image_dialog) {
            doImageDialog();
            return true;
        } else if (id == R.id.action_dialog_show_timer_dialog) {
            doTimerDialog();
            return true;
        } else if (id == R.id.action_dialog_show_webview_dialog) {
            doWebViewDialog();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void doConfirmDialog()
    {
        showBasicConfirmDialog();
    }
    private void showBasicConfirmDialog()
    {
//        // confirm dialog
//        DialogFragment newFragment = ActionableConfirmDialogFragment.newInstance(
//                "C Dialog", "Confirm?");
//        newFragment.show(getFragmentManager(), "confirm_dialog");

        // response dialog
        DialogFragment newFragment = ActionableResponseDialogFragment.newInstance(
                "R Dialog", "Response?", "Positive", "Negative", "Neutral");
        newFragment.show(getFragmentManager(), "response_dialog");
    }

    private void doYesNoDialog()
    {
        showBasicYesNoDialog();
    }
    private void showBasicYesNoDialog()
    {
        DialogFragment newFragment = ActionableYesNoDialogFragment.newInstance(
                "Y/N Dialog", "Yes? or No?");
        newFragment.show(getFragmentManager(), "yesno_dialog");
    }

    private void doMultipleChoiceDialog()
    {
        showBasicItemListDialogMultipleChoice();
    }
    private void showBasicItemListDialogMultipleChoice()
    {
        CharSequence[] itemsArray = new CharSequence[]{"A", "B", "C", "D"};
        boolean[] selectedItems = new boolean[] {false, true, true, false};
        DialogFragment newFragment = ActionableItemListDialogFragment.newMultipleChoiceInstance(
                "Multiple Choice",
                itemsArray,
                selectedItems
        );
        newFragment.show(getFragmentManager(), "multiplechoice_dialog");
    }

    private void doSingleChoiceDialog()
    {
        showBasicItemListDialogSingleChoice();
    }
    private void showBasicItemListDialogSingleChoice()
    {
        CharSequence[] itemsArray = new CharSequence[]{"a", "b", "c"};
        DialogFragment newFragment = ActionableItemListDialogFragment.newSingleChoiceInstance(
                "Single Choice",
                itemsArray
        );
        newFragment.show(getFragmentManager(), "singlechoice_dialog");
    }

    private void doImageDialog()
    {
        showBasicImageDialog();
    }
    private void showBasicImageDialog()
    {
        DialogFragment newFragment = ActionableImageDialogFragment.newInstance(
                R.string.basic_image_dialog_title, R.drawable.ic_launcher);
        newFragment.show(getFragmentManager(), "image_dialog");
    }


    private void doTimerDialog()
    {
        showBasicTimerDialog();
    }
    private void showBasicTimerDialog()
    {
//        // [1]
//        DialogFragment newFragment = ActionableTimerDialogFragment.newInstance(
//                R.string.basic_timer_dialog_title, 15000L, 5000L, 200L);

        // [2]
        DialogFragment newFragment = ActionableTimerDialogFragment.newInstance(
                R.string.basic_timer_dialog_title, R.layout.timer_dialog_sample_custom_view, 15000L, 5000L, 200L);
        // Log.w(">>>>>>>>>>>>>>>>>>> custom view id = " + R.layout.timer_dialog_sample_custom_view);

        newFragment.show(getFragmentManager(), "timer_dialog");
    }


    private void doWebViewDialog()
    {
        showBasicWebViewDialog();
    }
    private void showBasicWebViewDialog()
    {
        String contentData = "<html><body><h1>Sample content</h1><p>Test ...</p></body></html>";
        URL contentURL = null;
        try {
            contentURL = new URL("http://www.google.com/");
        } catch (MalformedURLException e) {
            Log.w("Malformed URL error.", e);
        }

        // [1]
        DialogFragment newFragment1 = ActionableWebViewDialogFragment.newInstance(
                "Web View Test 1",
                R.layout.webview_dialog_sample_custom_view,
                contentData
        );
        newFragment1.show(getFragmentManager(), "webview_dialog");

//        // [2]
//        DialogFragment newFragment2 = ActionableWebViewDialogFragment.newInstance(
//                "Web View Test 2",
//                R.layout.webview_dialog_sample_custom_view,
//                contentURL
//        );
//        newFragment2.show(getFragmentManager(), "webview_dialog");

    }



    @Override
    public void onPositiveClick(String tag)
    {
        if(Log.I) Log.i("onPositiveClick: tag = " + tag);

    }

    @Override
    public void onNegativeClick(String tag)
    {
        if(Log.I) Log.i("onNegativeClick: tag = " + tag);

    }

    @Override
    public void onNeutralClick(String tag)
    {
        if(Log.I) Log.i("onNeutralClick: tag = " + tag);

    }

    @Override
    public void onDialogItemClick(String tag, int which, boolean isChecked)
    {
        if(Log.I) Log.i("onDialogItemClick: tag = " + tag + "; which = " + which + "; isChecked = " + isChecked);

    }



    /**
     * A placeholder fragment containing a simple view.
     */
    public static class SampleDialogMainFragment extends Fragment
    {

        public SampleDialogMainFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState)
        {
            View rootView = inflater.inflate(R.layout.fragment_sample_dialog_main, container, false);
            return rootView;
        }
    }
}

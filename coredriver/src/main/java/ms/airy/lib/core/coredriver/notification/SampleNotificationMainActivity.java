package ms.airy.lib.core.coredriver.notification;

import android.app.Activity;
import android.app.Fragment;
import android.app.Notification;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import ms.airy.lib.core.coredriver.R;
import ms.airy.lib.core.coredriver.help.SampleHelpMainActivity;
import ms.airy.lib.core.notification.NotificationBuilder;
import ms.airy.lib.core.notification.NotificationHelper;
import ms.airy.lib.core.notification.scheduler.NotificationBroadcaster;
import ms.airy.lib.core.notification.scheduler.NotificationScheduler;


public class SampleNotificationMainActivity extends Activity
{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample_notification_main);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new SampleNotificationMainFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.sample_notification_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_notification_notification_helper) {
            doNotificationHelper();
            return true;
        } else if (id == R.id.action_notification_notification_scheduler) {
            doNotificationScheduler();
            return true;
        } else if (id == R.id.action_notification_notification_scheduler_cron) {
            doNotificationSchedulerCron();
            return true;
        } else if (id == R.id.action_notification_notification_broadcaster) {
            doNotificationBroadcaster();
            return true;
        } else if (id == R.id.action_notification_notification_broadcaster_cron) {
            doNotificationBroadcasterCron();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void doNotificationHelper()
    {
        Log.i("doNotificationHelper()");

        Notification notification = NotificationBuilder.buildNotification(this, 1, SampleHelpMainActivity.class, "Noti Test 1", "Test notification 1.", android.R.drawable.ic_input_add);
        NotificationHelper.getInstance(this, "NOTI_TEST").showNotification(123, notification);

    }
    private void doNotificationScheduler()
    {
        Log.i("doNotificationScheduler()");

        Notification notification = NotificationBuilder.buildNotification(this, 2, SampleNotificationMainActivity.class, "Noti Test 2", "Test notification 2.", android.R.drawable.ic_btn_speak_now);
        Log.i("notification = " + notification);

        long now = System.currentTimeMillis();
        NotificationScheduler.getInstance(this).scheduleNotification(notification, now + 30 * 1000L);

    }
    private void doNotificationSchedulerCron()
    {
        Log.i("doNotificationSchedulerCron()");

        Notification notification = NotificationBuilder.buildNotification(this, 3, SampleNotificationMainActivity.class, "Noti Test 3", "Test notification 3.", android.R.drawable.ic_lock_idle_alarm);
        Log.i("notification = " + notification);

        String cronExp = "* * * * *";   // Every minutes.
        //String cronExp = "*/2 * * * *";   // Every two minutes.
        // String cronExp = "*/3 2,5,15 */2 3-11 *";
//        NotificationScheduler.getInstance(this).scheduleNotification(notification, cronExp, 5);
        NotificationScheduler.getInstance(this).scheduleNotification(222, notification, cronExp, 5);
//        NotificationScheduler.getInstance(this).scheduleNotification(notification, cronExp, 5, true);
//        NotificationScheduler.getInstance(this).scheduleNotification(333, notification, cronExp, 5, true);

    }


    private void doNotificationBroadcaster()
    {
        Log.i("doNotificationBroadcaster()");

        Notification notification = NotificationBuilder.buildNotification(this, 2, SampleNotificationMainActivity.class, "Noti Test 2", "Test notification 2.", android.R.drawable.ic_btn_speak_now);
        Log.i("notification = " + notification);

        NotificationBroadcaster notificationBroadcaster = NotificationBroadcaster.getInstance(this);
        notificationBroadcaster.setNotificationTag("test1");

        long now = System.currentTimeMillis();
        notificationBroadcaster.scheduleNotification(notification, now + 30 * 1000L);

    }
    private void doNotificationBroadcasterCron()
    {
        Log.i("doNotificationBroadcasterCron()");

        Notification notification = NotificationBuilder.buildNotification(this, 3, SampleNotificationMainActivity.class, "Noti Test 3", "Test notification 3.", android.R.drawable.ic_lock_idle_alarm);
        Log.i("notification = " + notification);

        NotificationBroadcaster notificationBroadcaster = NotificationBroadcaster.getInstance(this);
        notificationBroadcaster.setNotificationTag("test2");

        String cronExp = "* * * * *";   // Every minutes.
        //String cronExp = "*/2 * * * *";   // Every two minutes.
        // String cronExp = "*/3 2,5,15 */2 3-11 *";
//        notificationBroadcaster.scheduleNotification(notification, cronExp, 5);
        notificationBroadcaster.scheduleNotification(334, notification, cronExp, 5);
//        notificationBroadcaster.scheduleNotification(notification, cronExp, 5, true);
//        notificationBroadcaster.scheduleNotification(333, notification, cronExp, 5, true);

    }




    /**
     * A placeholder fragment containing a simple view.
     */
    public static class SampleNotificationMainFragment extends Fragment {

        public SampleNotificationMainFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_sample_notification_main, container, false);
            return rootView;
        }
    }

}

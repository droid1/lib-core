package ms.airy.lib.core.coredriver.config;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import ms.airy.lib.core.config.ConfigMaster;
import ms.airy.lib.core.coredriver.R;

public class SampleConfigMainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample_config_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.sample_config_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == R.id.action_config_test_config_master) {
            testConfigMaster();
            return true;
        }
        if (id == R.id.action_config_do_remote_config) {
            return true;
        }
        if (id == R.id.action_config_do_prefs_config) {
            return true;
        }
        if (id == R.id.action_config_do_resource_config) {
            return true;
        }
        if (id == R.id.action_config_do_json_config) {
            return true;
        }
        if (id == R.id.action_config_do_props_config) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void testConfigMaster()
    {
        String testStrVal = ConfigMaster.getInstance(this).getStringProperty("airy.lib.core.coredriver.test_string1");
        Log.w(">>>>>>>>>>> testStrVal = " + testStrVal);

        Integer testIntVal = ConfigMaster.getInstance(this).getIntProperty("airy.lib.core.coredriver.test_integer1");
        Log.w(">>>>>>>>>>> testIntVal = " + testIntVal);

        Boolean testBoolVal = ConfigMaster.getInstance(this).getBooleanProperty("airy.lib.core.coredriver.test_bool1");
        Log.w(">>>>>>>>>>> testBoolVal = " + testBoolVal);


    }

}

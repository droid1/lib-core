package ms.airy.lib.core.coredriver;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import ms.airy.lib.core.coredriver.auth.SampleAuthMainActivity;
import ms.airy.lib.core.coredriver.config.SampleConfigMainActivity;
import ms.airy.lib.core.coredriver.contacts.SampleContactsMainActivity;
import ms.airy.lib.core.coredriver.dialog.SampleDialogMainActivity;
import ms.airy.lib.core.coredriver.hangout.SampleHangoutMainActivity;
import ms.airy.lib.core.coredriver.help.SampleHelpMainActivity;
import ms.airy.lib.core.coredriver.layout.SampleLayoutMainActivity;
import ms.airy.lib.core.coredriver.location.SampleLocationMainActivity;
import ms.airy.lib.core.coredriver.miscellaneous.SampleMiscellaneousMainActivity;
import ms.airy.lib.core.coredriver.navigation.SampleNavigationMainActivity;
import ms.airy.lib.core.coredriver.notification.SampleNotificationMainActivity;
import ms.airy.lib.core.coredriver.popup.SamplePopupMainActivity;
import ms.airy.lib.core.coredriver.startup.SampleStartupMainActivity;
import ms.airy.lib.core.coredriver.status.SampleStatusMainActivity;
import ms.airy.lib.core.coredriver.time.cron.SampleCronParserMainActivity;
import ms.airy.lib.core.coredriver.time.thread.SampleHandlerThreadMainActivity;


public class CoreDriverMainActivity extends Activity
{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_core_driver_main);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new CoreDriverMainFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.core_driver_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_coredriver_show_sample_auth_activity) {
            startActivity(new Intent(this, SampleAuthMainActivity.class));
            return true;
        } else if (id == R.id.action_coredriver_show_sample_config_activity) {
            startActivity(new Intent(this, SampleConfigMainActivity.class));
            return true;
        } else if (id == R.id.action_coredriver_show_sample_navigation_activity) {
            startActivity(new Intent(this, SampleNavigationMainActivity.class));
            return true;
        } else if (id == R.id.action_coredriver_show_sample_startup_activity) {
            startActivity(new Intent(this, SampleStartupMainActivity.class));
            return true;
        } else if (id == R.id.action_coredriver_show_sample_location_activity) {
            startActivity(new Intent(this, SampleLocationMainActivity.class));
            return true;
        } else if (id == R.id.action_coredriver_show_sample_contacts_activity) {
            startActivity(new Intent(this, SampleContactsMainActivity.class));
            return true;
        } else if (id == R.id.action_coredriver_show_sample_layout_activity) {
            startActivity(new Intent(this, SampleLayoutMainActivity.class));
            return true;
        } else if (id == R.id.action_coredriver_show_sample_popup_activity) {
            startActivity(new Intent(this, SamplePopupMainActivity.class));
            return true;
        } else if (id == R.id.action_coredriver_show_sample_status_activity) {
            startActivity(new Intent(this, SampleStatusMainActivity.class));
            return true;
        } else if (id == R.id.action_coredriver_show_sample_notification_activity) {
            startActivity(new Intent(this, SampleNotificationMainActivity.class));
            return true;
        } else if (id == R.id.action_coredriver_show_sample_help_activity) {
            startActivity(new Intent(this, SampleHelpMainActivity.class));
            return true;
        } else if (id == R.id.action_coredriver_show_sample_dialog_activity) {
            startActivity(new Intent(this, SampleDialogMainActivity.class));
            return true;
        } else if (id == R.id.action_coredriver_show_sample_hangout_activity) {
            startActivity(new Intent(this, SampleHangoutMainActivity.class));
            return true;
        } else if (id == R.id.action_coredriver_show_sample_handler_thread_activity) {
            startActivity(new Intent(this, SampleHandlerThreadMainActivity.class));
            return true;
        } else if (id == R.id.action_coredriver_show_sample_cron_parser_activity) {
            startActivity(new Intent(this, SampleCronParserMainActivity.class));
            return true;
        } else if (id == R.id.action_coredriver_show_sample_miscellaneous_activity) {
            startActivity(new Intent(this, SampleMiscellaneousMainActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }



    /**
     * A placeholder fragment containing a simple view.
     */
    public static class CoreDriverMainFragment extends Fragment {

        public CoreDriverMainFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_core_driver_main, container, false);
            return rootView;
        }
    }
}

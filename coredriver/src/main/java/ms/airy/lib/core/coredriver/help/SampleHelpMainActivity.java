package ms.airy.lib.core.coredriver.help;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import ms.airy.lib.core.coredriver.R;
import ms.airy.lib.core.help.common.HelpSection;
import ms.airy.lib.core.help.content.LocalContentLoader;
import ms.airy.lib.core.help.content.OnlineContentLoader;
import ms.airy.lib.core.help.fragment.ContextSensitiveHelpFragment;
import ms.airy.lib.core.help.fragment.HelpSectionListFragment;
import ms.airy.lib.core.help.manager.HelpManager;
import ms.airy.lib.core.help.manager.HelpRegistry;
import ms.airy.lib.core.toast.ToastHelper;
import ms.airy.lib.core.toast.ToastManager;

import java.util.Map;

public class SampleHelpMainActivity extends Activity 
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample_help_main);

        if (savedInstanceState == null) {
            FragmentTransaction transaction = getFragmentManager().beginTransaction();

//            transaction.add(R.id.container, new SampleHelpMainFragment());

            // Testing...
            // Help Section List.
            transaction.add(R.id.container, HelpSectionListFragment.newInstance());
//            transaction.add(R.id.container, HelpSectionListFragment.newInstance(R.layout.help_list_sample_text_view));
//            transaction.add(R.id.container, HelpSectionListFragment.newInstance(R.layout.help_list_sample_custom_view, R.id.helplist_row_textview));


            // testing help menu...
            transaction.add(ContextSensitiveHelpFragment.newInstance(1L, "Test Help"), "help_fragment");

            transaction.commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {    
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.sample_help_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_help_question_list) {
            doQuestionList();
            return true;
        }
        if (id == R.id.action_help_local_content_loader) {
            doLocalContentLoader();
            return true;
        }
        if (id == R.id.action_help_online_content_loader) {
            doOnlineContentLoader();
            return true;
        }
        if (id == R.id.action_help_help_registry) {
            doHelpRegistry();
            return true;
        }
        if (id == R.id.action_help_help_manager) {
            doHelpManager();
            return true;
//        } else if (id == R.id.action_help_help_fragment) {
//            doHelpFragment();
//            return true;
        }
        return super.onOptionsItemSelected(item);
    }



    private void doQuestionList()
    {
        Intent intent = new Intent(this, SampleQuestionInfoListActivity.class);
        startActivity(intent);
    }


    private void doLocalContentLoader()
    {
        Log.i("doLocalContentLoader");

        LocalContentLoader localContentLoader = LocalContentLoader.getInstance(this);
        Map<Long, String> sections = localContentLoader.getSectionMap();

        Log.w("sections = ");
        for(Long id : sections.keySet()) {
            Log.w("id = " + id + ": content = " + sections.get(id));
        }

    }

    private void doOnlineContentLoader()
    {
        Log.i("doOnlineContentLoader");

        String content1 = OnlineContentLoader.getInstance(this).getContent(1L);
        Log.w("Content for id = 1: " + content1);

        String content2 = OnlineContentLoader.getInstance(this).getContent(2L);
        Log.w("Content for id = 2: " + content2);

        String content3 = OnlineContentLoader.getInstance(this).getContent(5L);
        Log.w("Content for id = 5: " + content3);

        String content4 = OnlineContentLoader.getInstance(this).getContent(27L);
        Log.w("Content for id = 27: " + content4);

        String content5 = OnlineContentLoader.getInstance(this).getContent(100L);
        Log.w("Content for id = 100: " + content5);

    }

    private void doHelpRegistry()
    {
        Log.i("doHelpRegistry");


        HelpSection section1 = HelpRegistry.getInstance(this).getSection(1L);
        Log.w("HelpSection for id = 1: " + section1);

        HelpSection section2 = HelpRegistry.getInstance(this).getSection(2L);
        Log.w("HelpSection for id = 2: " + section2);

        HelpSection section3 = HelpRegistry.getInstance(this).getSection(5L);
        Log.w("HelpSection for id = 5: " + section3);

        HelpSection section4 = HelpRegistry.getInstance(this).getSection(27L);
        Log.w("HelpSection for id = 27: " + section4);

        HelpSection section5 = HelpRegistry.getInstance(this).getSection(100L);
        Log.w("HelpSection for id = 100: " + section5);

    }

    private void doHelpManager()
    {
        Log.i("doHelpManager");

//        boolean suc = HelpManager.getInstance(this).showHelp(this, 1L);
        boolean suc = HelpManager.getInstance(this).showHelp(this, 27L, "Test Help");
//        boolean suc = HelpManager.getInstance(this).showHelp(this.getFragmentManager(), 27L, "Test Help");
        ToastHelper.getInstance(this).showToast("suc = " + suc);

    }

//    private void doHelpFragment()
//    {
//        Log.i("doHelpFragment");
//
//    }

    



    /**
     * A placeholder fragment containing a simple view.
     */
    public static class SampleHelpMainFragment extends Fragment
    {
        public SampleHelpMainFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState)
        {
            View rootView = inflater.inflate(R.layout.fragment_sample_help_main, container, false);
            return rootView;
        }
    }
}

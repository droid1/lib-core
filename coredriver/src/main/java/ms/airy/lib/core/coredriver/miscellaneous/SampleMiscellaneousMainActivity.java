package ms.airy.lib.core.coredriver.miscellaneous;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.Date;

import ms.airy.lib.core.coredriver.R;
import ms.airy.lib.core.device.screen.ScreenOrientationLockHelper;
import ms.airy.lib.core.mortal.MortalObject;
import ms.airy.lib.core.toast.ToastHelper;


public class SampleMiscellaneousMainActivity extends Activity
{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample_miscellaneous_main);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new SampleMiscellaneousMainFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.sample_miscellaneous_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_miscellaneous_about_info) {
            doAboutInfo();
            return true;
        }
        if (id == R.id.action_miscellaneous_mortal_objects) {
            doMortalObjects();
            return true;
        }
        if (id == R.id.action_miscellaneous_lock_screen_orientation) {
            doLockScreenOrientation();
            return true;
        }
        if (id == R.id.action_miscellaneous_unlock_screen_orientation) {
            doUnlockScreenOrientation();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void doAboutInfo()
    {
        Intent intent = new Intent(this, SampleAboutInfoListActivity.class);
        startActivity(intent);
    }

    private void doMortalObjects()
    {
        Date oldDate = new Date();

        long now1 = System.nanoTime();
        MortalObject<Boolean> bool1 = new MortalObject<Boolean>(true, false, 1500L);
        for(int i=0;i<40;i++) {
            long delta = (System.nanoTime() - now1) / 1000L;
            boolean val = bool1.getValue();
            Log.w("Current value = " + val + " at " + delta);
            try {
                Thread.sleep(50L);
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        }

        long now2 = System.nanoTime();
        Date newDate = new Date();
        MortalObject<Date> date2 = new MortalObject<Date>(oldDate, newDate, 1500L);
        for(int i=0;i<40;i++) {
            long delta = (System.nanoTime() - now2) / 1000L;
            Date val = date2.getValue();
            Log.w("Current value = " + val + " at " + delta);
            try {
                Thread.sleep(50L);
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        }
    }

    private void doLockScreenOrientation()
    {
        ScreenOrientationLockHelper.getInstance(this).lockScreenOrientation(this);
        ToastHelper.getInstance(this).showToast("Screen orientation locked.");
    }
    private void doUnlockScreenOrientation()
    {
        ScreenOrientationLockHelper.getInstance(this).unlockScreenOrientation(this);
        ToastHelper.getInstance(this).showToast("Screen orientation unlocked.");
    }



    /**
     * A placeholder fragment containing a simple view.
     */
    public static class SampleMiscellaneousMainFragment extends Fragment {

        public SampleMiscellaneousMainFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_sample_miscellaneous_main, container, false);
            return rootView;
        }
    }
}

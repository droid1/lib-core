package ms.airy.lib.core.coredriver.miscellaneous;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import ms.airy.lib.core.about.common.AboutInfo;
import ms.airy.lib.core.about.helper.AboutInfoRegistry;
import ms.airy.lib.core.about.ui.AboutInfoListFragment;
import ms.airy.lib.core.coredriver.R;


// temporary
// Client/appn needs to override this or create a new activity:
// (1) Populate the aboutInfo list in AboutInfoRegistry, and
// (2) Add AboutInfoListFragment.
public class SampleAboutInfoListActivity extends Activity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample_about_info_list);

        if (savedInstanceState == null) {
            FragmentTransaction transaction = getFragmentManager().beginTransaction();

            // TBD:
            // Is there a better way?
            List<AboutInfo> aboutInfos = new ArrayList<AboutInfo>();
            // Populate aboutInfos here.
            // ....

            AboutInfo aboutInfo1 = new AboutInfo(1L);
            aboutInfo1.setTitle("title 1");
            aboutInfo1.setDescription("description 1");
            aboutInfo1.setDetailUrl("http://www.google.com/");
            aboutInfos.add(aboutInfo1);

            AboutInfo aboutInfo2 = new AboutInfo(2L);
            aboutInfo2.setTitle("title 2");
            aboutInfo2.setDescription("description 2 description 2 description 2 description 2 description 2 description 2 description 2 description 2 description 2 description 2 description 2 description 2 description 2 description 2 description 2 description 2 description 2description 2");
            aboutInfo2.setDetailAction(Intent.ACTION_VIEW);   // ???
            aboutInfo2.setImageResIds(
                    R.drawable.airymobile_logo_small,
                    R.drawable.airymobile_logo_medium,
                    R.drawable.airymobile_logo_large
            );
            aboutInfos.add(aboutInfo2);

            AboutInfoRegistry.getInstance(this).setAboutInfos(aboutInfos);

            // AboutInfoListFragment uses AboutInfoRegistry aboutInfo list implicitly.
            AboutInfoListFragment aboutInfoListFragment = AboutInfoListFragment.newInstance();
            transaction.add(R.id.container, aboutInfoListFragment);

            transaction.commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.about_info_list, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}

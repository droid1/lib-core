package ms.airy.lib.core.coredriver.time.cron;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import ms.airy.lib.core.coredriver.R;
import ms.airy.lib.core.cron.CronExpressionParser;

public class SampleCronParserMainActivity extends Activity
{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample_cron_parser_main);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new SampleCronParserMainFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.sample_cron_parser_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_cron_parse_cron_expression) {
            doParseCronExpression();
            return true;
        } else if (id == R.id.action_cron_get_next_cron_time) {
            doGetNextCronTime();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void doParseCronExpression()
    {
        String cronExp = "*/2 5,10 * * *";
//       String cronExp = "* 5,10 * * *";
        CronExpressionParser parser = new CronExpressionParser(cronExp);


    }
    private void doGetNextCronTime()
    {
//        String cronExp = "*/2 5,10 * * *";
//        String cronExp = "*/2 3,5,10 10-20 * *";
        String cronExp = "*/2 * * * *";
        CronExpressionParser parser = new CronExpressionParser(cronExp);

        Log.e("parser = " + parser);

        long nextCronTime = parser.getNextCronTime();
        Log.w("nextCronTime = " + nextCronTime);

        long previousCronTime = parser.getPreviousCronTime();
        Log.w("previousCronTime = " + previousCronTime);
    }




    /**
     * A placeholder fragment containing a simple view.
     */
    public static class SampleCronParserMainFragment extends Fragment {

        public SampleCronParserMainFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_sample_cron_parser_main, container, false);
            return rootView;
        }
    }
}

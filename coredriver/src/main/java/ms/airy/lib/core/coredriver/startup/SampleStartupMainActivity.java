package ms.airy.lib.core.coredriver.startup;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import ms.airy.lib.core.app.shortcut.LauncherShortcutManager;
import ms.airy.lib.core.coredriver.CoreDriverMainActivity;
import ms.airy.lib.core.coredriver.R;
import ms.airy.lib.core.startup.StartupManager;
import ms.airy.lib.core.startup.job.StartupJob;
import ms.airy.lib.core.startup.job.stock.PrimaryAccountJob;
import ms.airy.lib.core.startup.registry.BaseStartupJobRegistry;
import ms.airy.lib.core.startup.registry.StartupJobRegistry;
import ms.airy.lib.core.util.IdUtil;


public class SampleStartupMainActivity extends Activity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample_startup_main);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new SampleStartupMainFragment())
                    .commit();

            // temporary
            LauncherShortcutManager.getInstance(this).createShortcutIfFirstRun("test 1 shortcut", R.drawable.ic_launcher, CoreDriverMainActivity.class, true);
            // LauncherShortcutManager.getInstance(this).createShortcutUponInstallation("test 2 shortcut", R.drawable.ic_launcher, CoreDriverMainActivity.class, true);
            // temporary

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.sample_startup_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if (id == R.id.action_startup_startup_manager) {
            doStartupManager();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void doStartupManager()
    {
        // ???
        StartupManager manager = StartupManager.getInstance(this);
        StartupJobRegistry registry = new BaseStartupJobRegistry();
        long taskId = IdUtil.generateRandomId05();
        StartupJob job = new PrimaryAccountJob(this, taskId);
        registry.addJob(job);
        manager.initialize(registry);

        manager.doStartupProcessing(this);
    }



    /**
     * A placeholder fragment containing a simple view.
     */
    public static class SampleStartupMainFragment extends Fragment {

        public SampleStartupMainFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_sample_startup_main, container, false);
            return rootView;
        }
    }
}

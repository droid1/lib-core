package ms.airy.lib.core.coredriver.hangout;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import ms.airy.lib.core.coredriver.R;
import ms.airy.lib.core.hangout.config.HangoutConfigHelper;
import ms.airy.lib.core.hangout.ui.BaseHangoutBulletinActivity;
import ms.airy.lib.core.hangout.ui.BaseHangoutPrefsActivity;


public class SampleHangoutMainActivity extends Activity
{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample_hangout_main);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new SampleHangoutMainFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.sample_hangout_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_hangout_hangout_manager) {
            doHangoutManager();
            return true;
        } else if (id == R.id.action_hangout_start_hangout_bulletin) {
            doHangoutBulletin();
            return true;
        } else if (id == R.id.action_hangout_start_hangout_prefs) {
            doHangoutPrefs();
            return true;
        } else if (id == R.id.action_hangout_start_hangout_dialog) {
            doHangoutDialog();
            return true;
        } else if (id == R.id.action_hangout_hangout_notifications) {
            doHangoutNotifications();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void doHangoutManager()
    {
        // TBD:

        String cronExpression = HangoutConfigHelper.getInstance(this).getHangoutScheduleExpression();
        Log.w(">>>>>>>>>>> cronExpression = " + cronExpression);

        long leadTimeMillis = HangoutConfigHelper.getInstance(this).getHangoutCronLeadMillis();
        Log.w(">>>>>>>>>>> leadTimeMillis = " + leadTimeMillis);

    }
    private void doHangoutBulletin()
    {
        startActivity(new Intent(this, BaseHangoutBulletinActivity.class));
    }
    private void doHangoutPrefs()
    {
        startActivity(new Intent(this, BaseHangoutPrefsActivity.class));
    }

    private void doHangoutDialog()
    {
        // TBD:
    }
    private void doHangoutNotifications()
    {
        // TBD:
    }



    /**
     * A placeholder fragment containing a simple view.
     */
    public static class SampleHangoutMainFragment extends Fragment
    {
        public SampleHangoutMainFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_sample_hangout_main, container, false);
            return rootView;
        }
    }
}

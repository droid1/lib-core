package ms.airy.lib.core.coredriver.auth;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import org.minijson.builder.JsonBuilderException;

import ms.airy.lib.core.auth.AuthManager;
import ms.airy.lib.core.auth.common.AccountType;
import ms.airy.lib.core.auth.common.UserAccount;
import ms.airy.lib.core.auth.helper.AccountPicker;
import ms.airy.lib.core.auth.ui.BaseAccountConsoleActivity;
import ms.airy.lib.core.auth.ui.BaseAuthLoginActivity;
import ms.airy.lib.core.auth.ui.BaseAuthLogoutActivity;
import ms.airy.lib.core.coredriver.R;


public class SampleAuthMainActivity extends Activity implements AccountPicker.AccountPickerCallback
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample_auth_main);

        if (savedInstanceState == null) {
            FragmentTransaction transaction = getFragmentManager().beginTransaction();

            transaction.add(R.id.container, new SampleAuthMainFragment());

            transaction.commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.sample_auth_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_auth_account_picker) {
            doAccountPicker();
            return true;
        } else if (id == R.id.action_auth_auth_helper) {
            doAuthHelper();
            return true;
        } else if (id == R.id.action_auth_auth_manager) {
            doAuthManager();
            return true;
        } else if (id == R.id.action_auth_account_store) {
            doAuthConfigManager();
            return true;
        } else if (id == R.id.action_auth_account_console) {
            doAccountConsole();
            return true;
        } else if (id == R.id.action_auth_auth_fragment) {
            doAuthFragment();
            return true;
        } else if (id == R.id.action_auth_login_activity) {
            doLoginActivity();
            return true;
        } else if (id == R.id.action_auth_logout_activity) {
            doLogoutActivity();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void doAccountPicker()
    {
        AccountPicker accountPickHelper = new AccountPicker(this, this, AccountType.TYPE_GOOGLE);
//        accountPickHelper.addAccountPickCallback(this);
        accountPickHelper.pickAccount(this);
    }
    @Override
    public void onAccountPicked(String tag, int type, String name)
    {
        Log.w(">>>>>>>>>>>>>>>>>> onAccountPicked() tag = " + tag + "; type = " + type + "; name = " + name);
    }
    @Override
    public void onAccountPickCanceled(String tag, int type)
    {
        Log.w(">>>>>>>>>>>>>>>>>> onAccountPickCanceled() tag = " + tag + "; type = " + type);
    }

    private void doAuthHelper()
    {
        // temporary

        try {
            UserAccount userAccount = AuthManager.getInstance(this).getUserAccount(AccountType.TYPE_GOOGLE, "abc@gmail.com");
            String json = userAccount.toJsonString();
            Log.w(">>>>> json = " + json);
        } catch (JsonBuilderException e) {
            Log.e("Error!", e);
        }

    }
    private void doAuthManager()
    {

    }

    private void doAuthConfigManager()
    {

    }

    private void doAccountConsole()
    {
        startActivity(new Intent(this, BaseAccountConsoleActivity.class));
    }
    private void doAuthFragment()
    {
        startActivity(new Intent(this, TestAuthSupportActivity.class));
    }

    private void doLoginActivity()
    {
        startActivity(new Intent(this, BaseAuthLoginActivity.class));
    }
    private void doLogoutActivity()
    {
        startActivity(new Intent(this, BaseAuthLogoutActivity.class));
    }




    /**
     * A placeholder fragment containing a simple view.
     */
    public static class SampleAuthMainFragment extends Fragment {

        public SampleAuthMainFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_sample_auth_main, container, false);
            return rootView;
        }
    }
}

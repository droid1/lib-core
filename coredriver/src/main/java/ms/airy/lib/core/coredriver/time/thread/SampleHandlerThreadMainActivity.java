package ms.airy.lib.core.coredriver.time.thread;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import ms.airy.lib.core.coredriver.R;
import ms.airy.lib.core.time.common.PollingSchedule;
import ms.airy.lib.core.time.handler.PollingEventCallback;
import ms.airy.lib.core.time.thread.PollingHandlerThread;

public class SampleHandlerThreadMainActivity extends Activity implements PollingEventCallback
{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample_handler_thread_main);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new SampleHandlerThreadMainFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.sample_handler_thread_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_handler_thread_create_polling_handler_thread) {
            doCreatePollingHandlerThread();
            return true;
        } else if (id == R.id.action_handler_thread_start_polling_handler_thread) {
            doStartPollingHandlerThread();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void doCreatePollingHandlerThread()
    {
        PollingHandlerThread handlerThread = new PollingHandlerThread("text", PollingSchedule.CONSTANT, this);

        // test
        handlerThread.setPollingInterval(555L);
        // ...

    }

    private void doStartPollingHandlerThread()
    {
        PollingHandlerThread handlerThread = new PollingHandlerThread("text", PollingSchedule.CONSTANT, this);

        handlerThread.start();

        // test
        handlerThread.setPollingInterval(1555L);
        // ...

        // test
        handlerThread.startPolling(20);
        // ...

        // test
        // handlerThread.quit();

    }






    @Override
    public void processPollingEvent()
    {
        Log.w("SampleHandlerThreadMainActivity.processPollingEvent() called.");

    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class SampleHandlerThreadMainFragment extends Fragment {

        public SampleHandlerThreadMainFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_sample_handler_thread_main, container, false);
            return rootView;
        }
    }
}

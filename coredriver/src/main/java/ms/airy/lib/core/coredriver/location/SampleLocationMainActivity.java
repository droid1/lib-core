package ms.airy.lib.core.coredriver.location;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import ms.airy.lib.core.coredriver.R;
import ms.airy.lib.core.location.LocationTrackingScheduler;
import ms.airy.lib.core.location.prefs.LocationPrefsStoreManager;


public class SampleLocationMainActivity extends Activity
{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample_location_main);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new SampleLocationMainFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.sample_location_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if (id == R.id.action_location_location_preferences) {
            doLocationPreferences();
            return true;
        } else if (id == R.id.action_location_location_tracking_scheduler) {
            doLocationTrackingScheduler();
            return true;
        } else if (id == R.id.action_location_location_tracking_processor) {
            doLocationTrackingProcessor();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void doLocationPreferences()
    {
        // tbd:
        // Settings ui???


        // temporary
        LocationPrefsStoreManager.getInstance(this).removeAllPrefs();
    }
    private void doLocationTrackingScheduler()
    {
        // temporary
        LocationPrefsStoreManager.getInstance(this).setLocationTrackingEnabled(true);

        // ???
        LocationTrackingScheduler.getInstance(this).scheduleLocationTracking(false);
        // LocationTrackingScheduler.getInstance(this).cancelLocationTracking();

    }
    private void doLocationTrackingProcessor()
    {

    }




    /**
     * A placeholder fragment containing a simple view.
     */
    public static class SampleLocationMainFragment extends Fragment
    {

        public SampleLocationMainFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_sample_location_main, container, false);
            return rootView;
        }
    }
}

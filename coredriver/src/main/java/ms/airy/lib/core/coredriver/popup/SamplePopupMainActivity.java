package ms.airy.lib.core.coredriver.popup;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import ms.airy.lib.core.coredriver.R;
import ms.airy.lib.core.popup.PopupManager;
import ms.airy.lib.core.popup.common.PopupInfo;
import ms.airy.lib.core.popup.fragment.BasePopupManagerFragment;
import ms.airy.lib.core.popup.helper.LocalPopupLoader;
import ms.airy.lib.core.popup.helper.PopupRegistry;
import ms.airy.lib.core.popup.prefs.PopupHostPrefsManager;
import ms.airy.lib.core.popup.prefs.PopupInfoPrefsManager;

import java.util.Map;
import java.util.Random;


public class SamplePopupMainActivity extends Activity
{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample_popup_main);

        if (savedInstanceState == null) {
            FragmentTransaction transaction = getFragmentManager().beginTransaction();

            transaction.add(R.id.container, new SamplePopupMainFragment());

            // testing...
            BasePopupManagerFragment popupFragment = BasePopupManagerFragment.newInstance(null, 0, 500L, 20);
            transaction.add(popupFragment, "popup_test_fragment1");

            transaction.commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.sample_popup_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_popup_local_popup_loader) {
            doLocalPopupLoader();
            return true;
        } else if (id == R.id.action_popup_popup_registry) {
            doPopupRegistry();
            return true;
        } else if (id == R.id.action_popup_popup_prefs_manager) {
            doPopupPrefsManager();
            return true;
        } else if (id == R.id.action_popup_popup_manager) {
            doPopupManager();
            return true;
        } else if (id == R.id.action_popup_popup_manager_fragment) {
            doPopupManagerFragment();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void doLocalPopupLoader()
    {
        Map<String, PopupInfo> popupMap = LocalPopupLoader.getInstance(this).getPopupMap();

        for(String key : popupMap.keySet()) {
            Log.w("key = " + key);
            Log.w("popupInfo: " + popupMap.get(key));
        }
    }

    private void doPopupRegistry()
    {
        boolean isReady = PopupRegistry.getInstance(this).isReady();
        Log.w(">>>>>>> isReady = " + isReady);

        Map<String, PopupInfo> popupMap =  PopupRegistry.getInstance(this).getPopupInfos();
        if(popupMap != null) {
            for (String key : popupMap.keySet()) {
                Log.w("key = " + key);
                Log.w("popupInfo: " + popupMap.get(key));
            }
        }
    }

    private void doPopupPrefsManager()
    {
        // testing...
//        PopupInfoPrefsManager.getInstance(this).removeAllPrefs();
//        PopupHostPrefsManager.getInstance(this).removeAllPrefs();
        // ...
    }

    private void doPopupManager()
    {
        final Random random = new Random();

        int popupType = random.nextInt(3) + 1;
        Log.w("=========== popupType = " + popupType);

        Map<String, PopupInfo> popupInfos = PopupRegistry.getInstance(this).getPopupInfos();
        if(popupInfos != null) {
            for(String k : popupInfos.keySet()) {
                Log.w(">> key = " + k + "; popupInfo = " + popupInfos.get(k));
            }
        }

//        PopupInfo popupInfo1 = PopupManager.getInstance(this).getPopup(popupType);
//        Log.w("===BEFORE=== popupInfo1 = " + popupInfo1);
//        if(popupInfo1 == null) {
//            return;
//        }

        PopupManager.getInstance(this).showPopup(this, popupType);

//        PopupInfo popupInfo2 = PopupManager.getInstance(this).getPopup(popupType);
//        Log.w("===AFTER=== popupInfo2 = " + popupInfo2);
    }

    private void doPopupManagerFragment()
    {

    }




    /**
     * A placeholder fragment containing a simple view.
     */
    public static class SamplePopupMainFragment extends Fragment {

        public SamplePopupMainFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_sample_popup_main, container, false);
            return rootView;
        }
    }
}

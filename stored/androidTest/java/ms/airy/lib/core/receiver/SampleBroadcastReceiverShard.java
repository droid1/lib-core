package ms.airy.lib.core.receiver;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;


/**
 * Created by harry on 2/17/14.
 */
public class SampleBroadcastReceiverShard extends AbstractBroadcastReceiverShard
        implements BroadcastReceiverShard
{

    protected SampleBroadcastReceiverShard(Activity parentActivity)
    {
        super(parentActivity);
    }


    @Override
    public void onReceive(Context context, Intent intent)
    {
        String action = intent.getAction();
        // ...

    }

}

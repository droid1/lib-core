package ms.airy.lib.core.prefs;

import android.os.Bundle;

import ms.airy.lib.core.R;


// http://developer.android.com/guide/topics/ui/settings.html
public class SampleGeneralPreferenceFragment extends BasePreferenceFragment
{

    public SampleGeneralPreferenceFragment()
    {
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.pref_general);

        bindPreferenceSummaryToValue(findPreference("example_text"));
        bindPreferenceSummaryToValue(findPreference("example_list"));


    }

}

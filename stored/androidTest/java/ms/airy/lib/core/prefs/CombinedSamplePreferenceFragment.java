package ms.airy.lib.core.prefs;

import android.os.Bundle;
import android.preference.PreferenceCategory;


// http://developer.android.com/guide/topics/ui/settings.html
public class CombinedSamplePreferenceFragment extends BasePreferenceFragment
{

    public CombinedSamplePreferenceFragment()
    {
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.pref_general);
        bindPreferenceSummaryToValue(findPreference("example_text"));
        bindPreferenceSummaryToValue(findPreference("example_list"));

        // Add 'notifications' preferences, and a corresponding header.
        PreferenceCategory fakeHeader1 = new PreferenceCategory(getActivity());
        fakeHeader1.setTitle(R.string.pref_header_notifications);
        getPreferenceScreen().addPreference(fakeHeader1);
        addPreferencesFromResource(R.xml.pref_notification);
        bindPreferenceSummaryToValue(findPreference("notifications_new_message_ringtone"));

        // Add 'data and sync' preferences, and a corresponding header.
        PreferenceCategory fakeHeader2 = new PreferenceCategory(getActivity());
        fakeHeader2.setTitle(R.string.pref_header_data_sync);
        getPreferenceScreen().addPreference(fakeHeader2);
        addPreferencesFromResource(R.xml.pref_data_sync);
        bindPreferenceSummaryToValue(findPreference("sync_frequency"));

    }

}

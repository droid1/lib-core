package ms.airy.lib.core.prefs;

import android.os.Bundle;

import ms.airy.lib.core.R;


// http://developer.android.com/guide/topics/ui/settings.html
public class SampleDataSyncPreferenceFragment extends BasePreferenceFragment
{

    public SampleDataSyncPreferenceFragment()
    {
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

//        // Add 'data and sync' preferences, and a corresponding header.
//        PreferenceCategory fakeHeader = new PreferenceCategory(getActivity());
//        fakeHeader.setTitle(R.string.pref_header_data_sync);
//        getPreferenceScreen().addPreference(fakeHeader);
        addPreferencesFromResource(R.xml.pref_data_sync);

        bindPreferenceSummaryToValue(findPreference("sync_frequency"));

    }

}

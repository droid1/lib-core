package ms.airy.lib.core.prefs;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class SamplePreferencesActivity extends Activity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample_preferences);

        // ???
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    // [1]
                    .add(R.id.container, new CombinedSamplePreferenceFragment())
                    // [2] This does not work...
                    // How to add multiple fragments ????
                    // Especially, without using a fixed layout file, how to add dynamically add multiple fragments???
                    // ???
                    // http://developer.android.com/about/versions/android-4.2.html#NestedFragments
                    // ...
//                    .add(R.id.container1, new SampleGeneralPreferenceFragment(), "general")
//                    .add(R.id.container2, new SampleNotificationsPreferenceFragment(), "notifications")
//                    .add(R.id.container3, new SampleDataSyncPreferenceFragment(), "datasync")
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.sample_preferences, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}

package ms.airy.lib.core.prefs;

import android.os.Bundle;


// http://developer.android.com/guide/topics/ui/settings.html
public class SampleNotificationsPreferenceFragment extends BasePreferenceFragment
{

    public SampleNotificationsPreferenceFragment()
    {
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

//        // Add 'notifications' preferences, and a corresponding header.
//        PreferenceCategory fakeHeader = new PreferenceCategory(getActivity());
//        fakeHeader.setTitle(R.string.pref_header_notifications);
//        getPreferenceScreen().addPreference(fakeHeader);
        addPreferencesFromResource(R.xml.pref_notification);

        bindPreferenceSummaryToValue(findPreference("notifications_new_message_ringtone"));

    }

}

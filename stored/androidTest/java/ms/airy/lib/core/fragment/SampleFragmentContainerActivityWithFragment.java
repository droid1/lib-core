package ms.airy.lib.core.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


public class SampleFragmentContainerActivityWithFragment extends Activity implements FragmentContainer
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample_fragment_container);

        String fragExtra = getIntent().getStringExtra("fragment");


        if (savedInstanceState == null) {
            FragmentTransaction transaction = getFragmentManager().beginTransaction()
                    .add(R.id.container, new SampleFragmentContainerFragment());
            transaction.add(new SampleFragmentContainerSupportFragment(), "fragment_container_support");
            if(fragExtra != null && fragExtra.equals("shard")) {
                transaction.add(new SampleActiveFragmentWithShard(), "sample_active_fragment");
            } else {
                transaction.add(new SampleActiveFragment(), "sample_active_fragment");
            }
            transaction.commit();
        }
    }

    // temporary
    private SampleActiveFragmentInterface getSampleActiveFragment()
    {
        SampleActiveFragmentInterface fragment = (SampleActiveFragmentInterface) getFragmentManager().findFragmentByTag("sample_active_fragment");
        return fragment;
    }


    private SampleFragmentContainerSupportFragment getFragmentContainerSupportFragment()
    {
        SampleFragmentContainerSupportFragment fragment = (SampleFragmentContainerSupportFragment) getFragmentManager().findFragmentByTag("fragment_container_support");
        return fragment;
    }

    @Override
    public boolean registerFragment(Fragment fragment, String channel)
    {
        return getFragmentContainerSupportFragment().registerFragment(fragment, channel);
    }

    @Override
    public boolean unregisterFragment(Fragment fragment)
    {
        return getFragmentContainerSupportFragment().unregisterFragment(fragment);
    }

    @Override
    public boolean processFragmentMessages(Fragment sender, String channel, List<FragmentMessage> messages)
    {
        return getFragmentContainerSupportFragment().processFragmentMessages(sender, channel, messages);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.sample_fragment_container, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_send_message_from_fragment_to_activtiy) {
            sendMessageFromFragmentToActivity();
            return true;
        } else if (id == R.id.action_send_message_from_activtiy_to_fragment) {
            sendMessageFromActivityToFragment();
            return true;
        } else if (id == R.id.action_send_message_from_fragment_to_fragment) {
            sendMessageFromFragmentToFragment();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void sendMessageFromFragmentToActivity()
    {
        Log.i("sendMessageFromFragmentToActivity()");

        SampleActiveFragmentInterface f = getSampleActiveFragment();
        f.sendTestMessageToActivity();
    }

    private void sendMessageFromActivityToFragment()
    {
        Log.i("sendMessageFromActivityToFragment()");

        final String channel = "test";
        List<FragmentMessage> messages = new ArrayList<FragmentMessage>();
        messages.add(new FragmentMessage(1, "message1"));
        messages.add(new FragmentMessage(2, "message2"));

        ActiveFragment f = getSampleActiveFragment();
        f.processContainerMessages(this, channel, messages);
    }

    private void sendMessageFromFragmentToFragment()
    {
        Log.i("sendMessageFromFragmentToFragment()");

        final String channel = "test";
        SampleActiveFragmentInterface f = getSampleActiveFragment();
        f.sendTestMessageToFragments(channel);
    }





    /**
     * A placeholder fragment containing a simple view.
     */
    public static class SampleFragmentContainerFragment extends Fragment {

        public SampleFragmentContainerFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_sample_fragment_container, container, false);
            return rootView;
        }
    }
}

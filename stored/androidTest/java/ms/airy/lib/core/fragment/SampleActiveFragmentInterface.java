package ms.airy.lib.core.fragment;


// For testing purposes...
public interface SampleActiveFragmentInterface extends ActiveFragment
{
    void sendTestMessageToActivity();
    void sendTestMessageToFragments(String channel);
}

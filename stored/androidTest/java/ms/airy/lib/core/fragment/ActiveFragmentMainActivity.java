package ms.airy.lib.core.fragment;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;


public class ActiveFragmentMainActivity extends Activity
{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active_fragment_main);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.active_fragment_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_start_base_activity_base_fragment) {
            Intent i1 = new Intent(this, SampleFragmentContainerActivity.class);
            i1.putExtra("fragment", "base");
            startActivity(i1);
            return true;
        } else if (id == R.id.action_start_base_activity_fragment_shard) {
            Intent i2 = new Intent(this, SampleFragmentContainerActivity.class);
            i2.putExtra("fragment", "shard");
            startActivity(i2);
            return true;
        } else if (id == R.id.action_start_fragment_activity_base_fragment) {
            Intent i3 = new Intent(this, SampleFragmentContainerActivityWithFragment.class);
            i3.putExtra("fragment", "base");
            startActivity(i3);
            return true;
        } else if (id == R.id.action_start_fragment_activity_fragment_shard) {
            Intent i4 = new Intent(this, SampleFragmentContainerActivityWithFragment.class);
            i4.putExtra("fragment", "shard");
            startActivity(i4);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }







    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_active_fragment_main, container, false);
            return rootView;
        }
    }

}

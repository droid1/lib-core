package ms.airy.lib.core.fragment;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;


public class SampleActiveFragment extends BaseActiveFragment implements ActiveFragment, SampleActiveFragmentInterface
{
    private static final String CHANNEL = "sample";
    public static final String ARG_MY_STATE = "_my_state";
    private Bundle myState;


    public SampleActiveFragment()
    {
        super();

        Log.i("CTOR: SampleActiveFragment()");
        setChannel(CHANNEL);
    }




    // TBD:
    public static SampleActiveFragment newInstance(Bundle state)
    {
        SampleActiveFragment fragment = new SampleActiveFragment();
        Bundle args = new Bundle();
        args.putBundle(ARG_MY_STATE, state);
        fragment.setArguments(args);
        return fragment;
    }




    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(getArguments() != null) {
            myState = getArguments().getBundle(ARG_MY_STATE);
        } else {
            // ???
        }
    }





    @Override
    public boolean processContainerMessages(FragmentContainer container, String channel, List<FragmentMessage> messages)
    {
        if(Log.I) {
            Log.i("processContainerMessages() container = " + container + "; channel = " + channel);
            if(messages != null && !messages.isEmpty()) {
                for(FragmentMessage fm : messages) {
                    Log.i("fm = " + fm);
                }
            }
        }

        // TBD:
        return false;
    }


    @Override
    public void sendTestMessageToActivity()
    {
        List<FragmentMessage> messages = new ArrayList<FragmentMessage>();
        messages.add(new FragmentMessage(1, "message1"));
        messages.add(new FragmentMessage(2, "message2"));

        FragmentContainer c = getFragmentContainer();
        c.processFragmentMessages(this, null, messages);
    }

    @Override
    public void sendTestMessageToFragments(String channel)
    {
        // final String channel = "test";
        List<FragmentMessage> messages = new ArrayList<FragmentMessage>();
        messages.add(new FragmentMessage(3, "message3"));
        messages.add(new FragmentMessage(4, "message4"));

        FragmentContainer c = getFragmentContainer();
        c.processFragmentMessages(this, channel, messages);
    }

}

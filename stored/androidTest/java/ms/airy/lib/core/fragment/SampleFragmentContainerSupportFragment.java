package ms.airy.lib.core.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;


// Sample implementation of FragmentContainerSupportFragment.
public class SampleFragmentContainerSupportFragment extends FragmentContainerSupportFragment implements FragmentContainer
{
    public SampleFragmentContainerSupportFragment()
    {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return null;
    }

    @Override
    public boolean processFragmentMessages(Fragment sender, String channel, List<FragmentMessage> messages)
    {
        if(Log.I) {
            Log.i("processFragmentMessages() sender = " + sender + "; channel = " + channel);
            if(messages != null && !messages.isEmpty()) {
                for(FragmentMessage fm : messages) {
                    Log.i("fm = " + fm);
                }
            }
        }

        // TBD:
        // [1] Process whatever is needed in tha activity.
        // ...

        // Then:
        // [2] Call all ActiveFragments.
        boolean suc = relayFragmentMessages(sender, channel, messages);

        // temporary
        return false;
    }

}

package ms.airy.lib.core.fragment;

import java.util.List;

/**
 * Created by harry on 2/26/14.
 */
public class SampleActiveFragmentShard extends BaseActiveFragmentShard implements ActiveFragment
{
    private static final String CHANNEL = "sample";

    public SampleActiveFragmentShard(FragmentContainer fragmentContainer)
    {
        super(fragmentContainer);
        setChannel(CHANNEL);
    }


    @Override
    public boolean processContainerMessages(FragmentContainer container, String channel, List<FragmentMessage> messages)
    {
        // TBD:
        return false;
    }

}

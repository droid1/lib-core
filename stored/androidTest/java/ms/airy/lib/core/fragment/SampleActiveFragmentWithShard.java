package ms.airy.lib.core.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


public class SampleActiveFragmentWithShard extends Fragment implements ActiveFragment, SampleActiveFragmentInterface
{
    public static final String ARG_MY_STATE = "_my_state";
    private Bundle myState;

    private BaseActiveFragmentShard activeFragmentShard = null;



    public SampleActiveFragmentWithShard()
    {
        Log.i("CTOR: SampleActiveFragmentWithShard()");
    }




    // TBD:
    public static SampleActiveFragmentWithShard newInstance(Bundle state)
    {
        SampleActiveFragmentWithShard fragment = new SampleActiveFragmentWithShard();
        Bundle args = new Bundle();
        args.putBundle(ARG_MY_STATE, state);
        fragment.setArguments(args);
        return fragment;
    }




    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(getArguments() != null) {
            myState = getArguments().getBundle(ARG_MY_STATE);
        } else {
            // ???
        }
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return null;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        Activity activity = getActivity();
        if(activity instanceof FragmentContainer) {
            activeFragmentShard = new SampleActiveFragmentShard((FragmentContainer) activity);
        } else {
            // ???
        }

    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }


    @Override
    public void onStart()
    {
        super.onStart();
        if(activeFragmentShard != null) {
            activeFragmentShard.getFragmentContainer().registerFragment(this, activeFragmentShard.getChannel());
        }
    }

    @Override
    public void onStop()
    {
        if(activeFragmentShard != null) {
            activeFragmentShard.getFragmentContainer().unregisterFragment(this);
        }
        super.onStop();
    }



    @Override
    public boolean processContainerMessages(FragmentContainer container, String channel, List<FragmentMessage> messages)
    {
        if(Log.I) {
            Log.i("processContainerMessages() container = " + container + "; channel = " + channel);
            if(messages != null && !messages.isEmpty()) {
                for(FragmentMessage fm : messages) {
                    Log.i("fm = " + fm);
                }
            }
        }

        if(activeFragmentShard != null) {
            return activeFragmentShard.processContainerMessages(container, channel, messages);
        } else {
            return false;
        }
    }


    @Override
    public void sendTestMessageToActivity()
    {
        if(activeFragmentShard != null) {
            List<FragmentMessage> messages = new ArrayList<FragmentMessage>();
            messages.add(new FragmentMessage(1, "message1"));
            messages.add(new FragmentMessage(2, "message2"));

            FragmentContainer c = activeFragmentShard.getFragmentContainer();
            c.processFragmentMessages(this, null, messages);
        } else {
            Log.e("activeFragmentShard is null!");
        }
    }

    @Override
    public void sendTestMessageToFragments(String channel)
    {
        if(activeFragmentShard != null) {
            // final String channel = "test";
            List<FragmentMessage> messages = new ArrayList<FragmentMessage>();
            messages.add(new FragmentMessage(3, "message3"));
            messages.add(new FragmentMessage(4, "message4"));

            FragmentContainer c = activeFragmentShard.getFragmentContainer();
            c.processFragmentMessages(this, channel, messages);
        } else {
            Log.e("activeFragmentShard is null!");
        }
    }

}

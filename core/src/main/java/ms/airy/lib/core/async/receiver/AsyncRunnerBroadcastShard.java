package ms.airy.lib.core.async.receiver;

import android.content.Context;
import android.content.Intent;

import ms.airy.lib.core.async.common.AsyncConsts;
import ms.airy.lib.core.location.common.LocationTrackerConsts;
import ms.airy.lib.core.location.tracking.LocationTrackingHelper;
import ms.airy.lib.core.receiver.AbstractBroadcastReceiverShard;
import ms.airy.lib.core.receiver.BroadcastReceiverShard;


/**
 * BroadcastReceiver.onReceive() delegation.
 */
public class AsyncRunnerBroadcastShard extends AbstractBroadcastReceiverShard implements BroadcastReceiverShard
{
    public AsyncRunnerBroadcastShard()
    {
//        this(null);
    }
//    public LocationTrackerShard(Activity parentActivity)
//    {
//        // parentActivity could be null...
//        super(parentActivity);
//    }


    @Override
    public void onReceive(Context context, Intent intent)
    {
        Log.i("AsyncRunnerBroadcastShard.onReceive().");


        // intent filter ???
        // ....
        String action = intent.getAction();
        if(Log.D) Log.d("onReceive() action = " + action);
        if(action == null ||
                ! action.equals(AsyncConsts.ACTION_ASYNC_RESULT_POST)) {
            // bail out???
            if(Log.I) Log.i("Unhandled intent: action = " + action);
            return;
        }


        // TBD:
        // Post the result/error to the original AsyncRunner object,
        // which in turn calls the listener methods....
        // ....


    }


}

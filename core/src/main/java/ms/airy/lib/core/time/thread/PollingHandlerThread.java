package ms.airy.lib.core.time.thread;

import android.os.HandlerThread;
import android.os.Looper;
import android.os.SystemClock;

import ms.airy.lib.core.time.common.PollingSchedule;
import ms.airy.lib.core.time.handler.ConstantPollingHandler;
import ms.airy.lib.core.time.handler.FibonacciPollingHandler;
import ms.airy.lib.core.time.handler.LinearPollingHandler;
import ms.airy.lib.core.time.handler.PollingEventCallback;
import ms.airy.lib.core.time.handler.PollingHandler;
import ms.airy.lib.core.time.handler.PollingHandlerFactory;


/**
 * This creates a new PollingHandler within a thread.
 * This can be used only for a small number of pollings, because of the way it's implemented.
 */
public class PollingHandlerThread extends HandlerThread
{
    private int pollingSchedule;
    private PollingHandler pollingHandler = null;
    private PollingEventCallback pollingEventCallback = null;
//    private boolean handlerInitialized = false;

    public PollingHandlerThread(String name)
    {
        this(name, PollingSchedule.getDefaultSchedule(), null);
    }
    public PollingHandlerThread(String name, int pollingSchedule, PollingEventCallback pollingEventCallback)
    {
        super(name);
        init(pollingSchedule, pollingEventCallback);
    }
    public PollingHandlerThread(String name, int priority)
    {
        this(name, priority, PollingSchedule.getDefaultSchedule(), null);
    }
    public PollingHandlerThread(String name, int priority, int pollingSchedule, PollingEventCallback pollingEventCallback)
    {
        super(name, priority);
        init(pollingSchedule, pollingEventCallback);
    }
    private void init(int pollingSchedule)
    {
        init(pollingSchedule, null);
    }
    private void init(int pollingSchedule, PollingEventCallback pollingEventCallback)
    {
        this.pollingSchedule = pollingSchedule;
//        // pollingHandler will be initialized in start().
//        // but, just in case, as a fallback, construct one here???
//        // ????
//        // Does this make sense????
//        pollingHandler = PollingHandlerFactory.buildPollingHandler(pollingSchedule);
//        if(pollingEventCallback != null) {
//            pollingHandler.addPollingEventCallback(pollingEventCallback);
//        }
//        // Note: handlerInitialized is still false at this point...
//        // ....

        // ???
        this.pollingEventCallback = pollingEventCallback;
    }


    public final int getPollingSchedule()
    {
        return pollingSchedule;
    }
//    public void setPollingSchedule(int pollingSchedule)
//    {
//        this.pollingSchedule = pollingSchedule;
//    }


    public final PollingEventCallback getPollingEventCallback()
    {
        return pollingEventCallback;
    }
    public final void setPollingEventCallback(PollingEventCallback pollingEventCallback)
    {
        this.pollingEventCallback = pollingEventCallback;
        if(pollingHandler != null && pollingEventCallback != null) {
            // TBD:
            // Remove the previous callback, if any. ????
            pollingHandler.addPollingEventCallback(pollingEventCallback);
        }
    }


    // ????
    public final PollingHandler getPollingHandler()
    {
//        if(handlerInitialized) {
//            return pollingHandler;
//        } else {
//            // ???
//            // Log.w("pollingHandler might not have been properly initialized.");
//            // return pollingHandler;
//
//            // ???
//            Log.i("pollingHandler might not have been properly initialized.");
//            return null;
//        }

        if(pollingHandler == null) {
            // ???
            Log.i("pollingHandler might not have been properly initialized.");
            return null;
        } else {
            return pollingHandler;
        }
    }


    // Only applicable to Constant type.
    public final void setPollingInterval(long interval)
    {
        if(getPollingHandler() != null && getPollingHandler() instanceof ConstantPollingHandler) {
            ((ConstantPollingHandler) getPollingHandler()).setPollingInterval(interval);
        } else {
            Log.w("setPollingInterval() failed. interval = " + interval);
        }
    }

    // Only applicable to Linear type.
    public final void setPollingIntervalAndDelta(long interval, long delta)
    {
        if(getPollingHandler() != null && getPollingHandler() instanceof LinearPollingHandler) {
            ((LinearPollingHandler) getPollingHandler()).setPollingIntervalAndDelta(interval, delta);
        } else {
            Log.w("setPollingIntervalAndDelta() failed. interval = " + interval);
        }
    }

    // Only applicable to Fibonacci type.
    public final void setInitialDelays(long delay1, long delay2)
    {
        if(getPollingHandler() != null && getPollingHandler() instanceof FibonacciPollingHandler) {
            ((FibonacciPollingHandler) getPollingHandler()).setInitialDelays(delay1, delay2);
        } else {
            Log.w("setInitialDelays() failed. delay1 = " + delay1 + "; delay2 = " + delay2);
        }
    }


    public void startPolling(int repeats)
    {
        if(getPollingHandler() != null) {
            getPollingHandler().startPolling(repeats);
        } else {
            Log.w("startPolling() failed because pollingHandler is not set/initialized.");
        }
    }
    public void startPolling(long startUptime, int repeats)
    {
        if(getPollingHandler() != null) {
            getPollingHandler().startPolling(startUptime, repeats);
        } else {
            Log.w("startPolling() failed because pollingHandler is not set/initialized.");
        }
    }
    public void startPolling(long duration)
    {
        if(getPollingHandler() != null) {
            getPollingHandler().startPolling(duration);
        } else {
            Log.w("startPolling() failed because pollingHandler is not set/initialized.");
        }
    }
    public void startPolling(long startUptime, long duration)
    {
        if(getPollingHandler() != null) {
            getPollingHandler().startPolling(startUptime, duration);
        } else {
            Log.w("startPolling() failed because pollingHandler is not set/initialized.");
        }
    }


    // TBD:
    // stopPolling?
    // ...



    @Override
    public synchronized void start()
    {
        Log.i("PollingHandlerThread.start() called.");
        super.start();

        // Initialize the pollingHandler here.
        Looper looper = getLooper();
        if(looper == null) {
            Log.w("PollingHandlerThread.start() looper is null!");
        }
        pollingHandler = PollingHandlerFactory.buildPollingHandler(pollingSchedule, looper);
        if(pollingEventCallback != null) {
            pollingHandler.addPollingEventCallback(pollingEventCallback);
        }
//        handlerInitialized = true;

        // TBD:
        // Start polling???
        // Because the client needs to provide explicit startUptime and repeats,
        // we cannot hard-code it here...
        // ...
    }

    @Override
    public void run()
    {
        Log.i("PollingHandlerThread.run() called.");
        super.run();
    }

    @Override
    public void interrupt()
    {
        Log.i("PollingHandlerThread.interrupt() called.");
        super.interrupt();
    }

    @Override
    public boolean quit()
    {
        Log.i("PollingHandlerThread.quit() called.");
        return super.quit();
    }


//    @Override
//    protected void onLooperPrepared()
//    {
//        Log.i("PollingHandlerThread.onLooperPrepared() called.");
//        super.onLooperPrepared();
//
//        Looper looper = getLooper();
//        pollingHandler = PollingHandlerFactory.buildPollingHandler(pollingSchedule, looper);
//        if(pollingEventCallback != null) {
//            pollingHandler.addPollingEventCallback(pollingEventCallback);
//        }
//        handlerInitialized = true;
//    }



}

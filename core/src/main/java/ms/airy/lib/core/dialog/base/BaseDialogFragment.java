package ms.airy.lib.core.dialog.base;

import android.app.AlertDialog;
import android.app.DialogFragment;
import android.os.Bundle;


// http://developer.android.com/guide/topics/ui/dialogs.html
public abstract class BaseDialogFragment extends DialogFragment
{
    // We can use either char-sequence or res id.
    // If explicit string/char-sequence is specified,
    //     it takes precedence over the resource id.
    // Note: The current API does NOT allow specifying both during construction...
    protected static final String ARG_TITLE = "base_dialog_title";
    protected static final String ARG_TITLE_RESID = "dialog_dialog_title_resid";

    // TBD:

    protected static final String ARG_POSITIVE_LABEL = "base_dialog_positive_label";
    protected static final String ARG_POSITIVE_LABEL_RESID = "base_dialog_positive_label_resid";

    protected static final String ARG_NEGATIVE_LABEL = "base_dialog_negative_label";
    protected static final String ARG_NEGATIVE_LABEL_RESID = "base_dialog_negative_label_resid";

    protected static final String ARG_NEUTRAL_LABEL = "base_dialog_neutral_label";
    protected static final String ARG_NEUTRAL_LABEL_RESID = "base_dialog_neutral_label_resid";

    protected static BaseDialogFragment addIntArgument(BaseDialogFragment fragment, String key, int val)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        args.putInt(key, val);
        return fragment;
    }
    protected static BaseDialogFragment addLongArgument(BaseDialogFragment fragment, String key, long val)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        args.putLong(key, val);
        return fragment;
    }
    protected static BaseDialogFragment addStringArgument(BaseDialogFragment fragment, String key, String val)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        args.putString(key, val);
        return fragment;
    }

    public BaseDialogFragment()
    {
    }




    // The "main" method to build an alert dialog.
    protected abstract AlertDialog.Builder buildDialogBuilder();


}

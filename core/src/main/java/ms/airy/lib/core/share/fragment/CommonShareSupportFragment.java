package ms.airy.lib.core.share.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import ms.airy.lib.core.share.TextSharerable;
import ms.airy.lib.core.share.util.TextShareUtil;
import ms.airy.lib.core.toast.ToastHelper;


/**
 * Example usage:
 * In Activity.onCreate()
 *   fragmentTransaction.add(CommonShareSupportFragment.newInstance(menuLabel), fragmentTag);
 */
public class CommonShareSupportFragment extends Fragment
{
    // temporary
    private static final String DEFAULT_SHARE_MENU_LABEL = "Share";

//    public static final String ARG_TEXT_SHAREABLE = "share_text_shareable";
//    public static final String ARG_SHARE_SUBJECT = "share_share_subject";
//    public static final String ARG_SHARE_TEXT = "share_share_text";
//    public static final String ARG_SHARE_STREAM = "share_share_stream";
    public static final String ARG_MENU_LABEL = "share_menu_label";
    public static final String ARG_MENU_LABEL_RESID = "share_menu_label_resid";
    public static final String ARG_SHOW_CHOOSER = "share_show_chooser";
    public static final String ARG_SHOW_AS_ACTION = "share_show_as_action";
    // ...

    // Note that we cannot specify both menuLabel and menuLabelResId
    //     during construction.
    public static CommonShareSupportFragment newInstance()
    {
        return newInstance(null);
    }
    public static CommonShareSupportFragment newInstance(String menuLabel)
    {
        return newInstance(menuLabel, false);
    }
    public static CommonShareSupportFragment newInstance(int menuLabelResId)
    {
        return newInstance(menuLabelResId, false);
    }
    public static CommonShareSupportFragment newInstance(String menuLabel, boolean showChooser)
    {
        return newInstance(menuLabel, showChooser, false);
    }
    public static CommonShareSupportFragment newInstance(int menuLabelResId, boolean showChooser)
    {
        return newInstance(menuLabelResId, showChooser, false);
    }
    public static CommonShareSupportFragment newInstance(String menuLabel, boolean showChooser, boolean showAsAction)
    {
        CommonShareSupportFragment fragment = new CommonShareSupportFragment();
        Bundle args = new Bundle();
        if(menuLabel != null) {
            args.putString(ARG_MENU_LABEL, menuLabel);
        }
        args.putBoolean(ARG_SHOW_CHOOSER, showChooser);
        args.putBoolean(ARG_SHOW_AS_ACTION, showAsAction);
        fragment.setArguments(args);
        return fragment;
    }
    public static CommonShareSupportFragment newInstance(int menuLabelResId, boolean showChooser, boolean showAsAction)
    {
        CommonShareSupportFragment fragment = new CommonShareSupportFragment();
        Bundle args = new Bundle();
        if(menuLabelResId > 0) {    // ???
            args.putInt(ARG_MENU_LABEL_RESID, menuLabelResId);
        }
        args.putBoolean(ARG_SHOW_CHOOSER, showChooser);
        args.putBoolean(ARG_SHOW_AS_ACTION, showAsAction);
        fragment.setArguments(args);
        return fragment;
    }

    // Not used...
    protected static CommonShareSupportFragment addIntArgument(CommonShareSupportFragment fragment, String key, int val)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        args.putInt(key, val);
        return fragment;
    }
    protected static CommonShareSupportFragment addLongArgument(CommonShareSupportFragment fragment, String key, long val)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        args.putLong(key, val);
        return fragment;
    }
    protected static CommonShareSupportFragment addStringArgument(CommonShareSupportFragment fragment, String key, String val)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        args.putString(key, val);
        return fragment;
    }


    private String menuLabel = null;
    private int menuLabelResId = 0;
    private boolean showChooser = false;
    private boolean showAsAction = false;

    public CommonShareSupportFragment()
    {
    }


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
        setRetainInstance(true);   // ???
        // ...

        Bundle args = getArguments();
        if(args != null) {
            // if both menuLabelResId and menuLabel are specified,
            //    menuLabelResId takes precedence...
            menuLabelResId = args.getInt(ARG_MENU_LABEL_RESID);
            if(menuLabelResId == 0) {
                menuLabel = args.getString(ARG_MENU_LABEL);
                if(menuLabel == null) {
                    menuLabel = DEFAULT_SHARE_MENU_LABEL;
                }
            }
            if(args.containsKey(ARG_SHOW_CHOOSER)) {
                showChooser = args.getBoolean(ARG_SHOW_CHOOSER);
            }
            if(args.containsKey(ARG_SHOW_AS_ACTION)) {
                showAsAction = args.getBoolean(ARG_SHOW_AS_ACTION);
            }
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        return null;
    }


    // temporary
    private static final int MENU_ITEM_SHARE = 772331;

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        MenuItem menu1 = null;
        if(menuLabelResId > 0) {
            menu1 = menu.add(Menu.NONE, MENU_ITEM_SHARE, Menu.NONE, menuLabelResId);
        } else {
            menu1 = menu.add(Menu.NONE, MENU_ITEM_SHARE, Menu.NONE, menuLabel);
        }
        if(menu1 != null) {
            if(showAsAction) {
                menu1.setIcon(android.R.drawable.ic_menu_share);  // ???
                menu1.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
            }
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch(item.getItemId()) {
            case MENU_ITEM_SHARE:
                startShare();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // TBD:
    // Localize the text strings????
    private void startShare()
    {
        if(Log.I) Log.i("startShare().");

        TextSharerable textSharerable = null;
        if(getActivity() instanceof TextSharerable) {
            textSharerable = (TextSharerable) getActivity();
        } else {
            Log.w("The parent activity is not instanceof TextSharerable!!!");
            // ???? What to do???
            ToastHelper.getInstance(getActivity()).showToast("Share is not enabled.");
            return;
        }

        String shareSubject = textSharerable.getShareSubject();
        String shareText = textSharerable.getShareText();

        if(shareText == null || shareText.isEmpty()) {
            // ???? What to do???
            ToastHelper.getInstance(getActivity()).showToast("Nothing to share.");
        } else {

            Intent sendIntent = TextShareUtil.buildShareIntent(shareText, shareSubject);

//            Intent sendIntent = new Intent(Intent.ACTION_SEND);
//            sendIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);  // ????
//            sendIntent.putExtra(Intent.EXTRA_TEXT, shareText);
//            if(shareSubject != null && !shareSubject.isEmpty()) {
//                sendIntent.putExtra(Intent.EXTRA_SUBJECT, shareSubject);
//            }
//            sendIntent.setType("text/plain");   // ????

            if(showChooser) {
                // TBD: Localize the text.
                getActivity().startActivity(Intent.createChooser(sendIntent, "Share via"));
            } else {
                getActivity().startActivity(sendIntent);
            }
        }

    }

}

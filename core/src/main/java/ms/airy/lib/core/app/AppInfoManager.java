package ms.airy.lib.core.app;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;


// TBD:
public final class AppInfoManager
{
    private static AppInfoManager INSTANCE = null;

    public static AppInfoManager getInstance(Context context)
    {
        if (INSTANCE == null) {
            INSTANCE = new AppInfoManager(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;

    // "Cache"
    private String appName = null;
    private int appUserId = 0;      // ???
    // private String appVersion = null;   //  --> Use version manager.

    private AppInfoManager(Context context)
    {
        this.context = context;
    }


    // temporary
    public String getApplicationName()
    {
        if(appName == null) {
            initAppInfo();
        }
        return appName;
    }
    public int getApplicationUID()
    {
        if(appUserId == 0) {
            initAppInfo();
        }
        return appUserId;
    }
    private void initAppInfo()
    {
        String packageName = null;
        try {
            // package name of this app.
            // TBD: Does this really work if we use libraries/apps ???
            packageName = context.getPackageName();
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(packageName, 0);
            appName = packageInfo.applicationInfo.name;
            appUserId = packageInfo.applicationInfo.uid;
            // appVersion = packageInfo.versionName;
        } catch(PackageManager.NameNotFoundException e) {
            Log.e("Failed to get the app info: packageName = " + packageName, e);
        }
    }

//    // temporary
//    public String getCurrentVersion()
//    {
//        if(appVersion == null) {
//            initAppInfo();
//        }
//        return appVersion;
//    }




}

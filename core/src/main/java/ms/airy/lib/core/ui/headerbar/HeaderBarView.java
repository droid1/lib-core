package ms.airy.lib.core.ui.headerbar;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.KeyEvent;
import android.widget.LinearLayout;

// http://developer.android.com/guide/topics/ui/custom-components.html
public class HeaderBarView extends LinearLayout
{

    // ....
    private HeaderButtonLayout mHeaderButtonLayout = null;
    private HeaderBannerView mTitleView = null;
    private HeaderButton mMenuButton = null;
    // ....
    
    
    public HeaderBarView(Context context)
    {
        super(context);
        init();
        //...
    }

    public HeaderBarView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
        //...
    }

    public HeaderBarView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        init();
        //...
    }

    
    
    private void init()
    {
        if(Log.D) Log.d("init()");

        // TBD
        setOrientation(HORIZONTAL);
        setMinimumHeight(150);  // temporary
        // ...
        
        // Title view
        mTitleView = new HeaderBannerView(getContext());

        // Initialize the child elements.
        mHeaderButtonLayout = new HeaderButtonLayout(getContext());
        
        
        
//        // test
//        TextView tv2 = new TextView(getContext());
//        tv2.setText("xxxxx");
//        tv2.setWidth(200);
//        
//        Button btn1 = new Button(getContext());
//        btn1.setText("yyyyy");
//
//        HeaderButton btn2 = new HeaderButton(getContext());
//        //btn2.setIcon(icon);
//        //btn2.setImageURI(Uri.parse("http://media.photobucket.com/image/ladybug%20bmp/JosephAnthony/th_ladybug.bmp"));
//        
//        addView(tv2);
//        addView(btn1);
//        addView(btn2);
//        // test
        

        
        
        // Redo the layout...
        layoutChildViews();

    }
    
    
    // TBD:
    private void layoutChildViews()
    {
        if(Log.D) Log.d("layoutChildViews()");
        
        
        // [0] Remove all views first.
        removeAllViews();
        

        // [1] Home
        if(mTitleView != null) {
            //LinearLayout.LayoutParams homeParams = new LinearLayout.LayoutParams(getLayoutParams());  // ????
            LinearLayout.LayoutParams homeParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT); // ????
//            ViewGroup vg = (ViewGroup)(mTitleView.getParent());
//            if(vg != null) {
//                vg.removeView(mTitleView);
//            }
            addView(mTitleView, homeParams);
        }        
        
        // [2] Nav buttons
        if(mHeaderButtonLayout != null) {
            //LinearLayout.LayoutParams navParams = new LinearLayout.LayoutParams(getLayoutParams());  // ????
            LinearLayout.LayoutParams navParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT); // ????
//            ViewGroup vg = (ViewGroup)(mHeaderButtonLayout.getParent());
//            if(vg != null) {
//                vg.removeView(mHeaderButtonLayout);
//            }
            addView(mHeaderButtonLayout, navParams);
        }
        
        
        // ???
        LinearLayout bufferSpace = new LinearLayout(getContext());
        LinearLayout.LayoutParams bufferParams = new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT); // ????
        bufferParams.weight = 0.5f;  // ???
        bufferParams.width = 100;    // ???
        addView(bufferSpace, bufferParams);
        // ???
        
        
        // [3] Menu
        //     TBD: How to put menu button at the bottom??????
        if(mMenuButton != null) {
            //LinearLayout.LayoutParams menuParams = new LinearLayout.LayoutParams(getLayoutParams());  // ????
            LinearLayout.LayoutParams menuParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT); // ????
            menuParams.weight = 10.0f;
            menuParams.width = mMenuButton.getWidth();
            menuParams.gravity = Gravity.BOTTOM;
//            ViewGroup vg = (ViewGroup)(mMenuButton.getParent());
//            if(vg != null) {
//                vg.removeView(mMenuButton);
//            }
            addView(mMenuButton, menuParams);
        }

        // ....
        
        
        
        // ?????
        invalidate();
    }
    
    
    
    public CharSequence getTitle()
    {
        return mTitleView.getTitle();
    }
    public void setTitle(CharSequence title)
    {
        mTitleView.setTitle(title);
    }
    
    public CharSequence getCaption()
    {
        return mTitleView.getCaption();
    }
    public void setCaption(CharSequence caption)
    {
        mTitleView.setCaption(caption);
    }
    
    public HeaderBannerView getTitleView()
    {
        return mTitleView;
    }
    public void setTitleView(HeaderBannerView titleView)
    {
        this.mTitleView = titleView;
        layoutChildViews();  // ????
    }

    public HeaderButton getMenuButton()
    {
        return mMenuButton;
    }
    public void setMenuButton(HeaderButton menuButton)
    {
        this.mMenuButton = menuButton;
        layoutChildViews();  // ????
    }

    
    public void addHeaderButton(HeaderButton button)
    {
        if(Log.D) Log.d("addHeaderButton()");
        mHeaderButtonLayout.addHeaderButton(button);
        layoutChildViews();  // ????
    }
    public void addHeaderButtons(HeaderButton... buttons)
    {
        if(Log.D) Log.d("addHeaderButtons()");
        mHeaderButtonLayout.addHeaderButtons(buttons);
        layoutChildViews();  // ????
    }
    public void addHeaderButton(int location, HeaderButton button)
    {
        if(Log.D) Log.d("addHeaderButton(): location = " + location);
        mHeaderButtonLayout.addHeaderButton(location, button);
        layoutChildViews();  // ????
    }
    public void addHeaderButtons(int location, HeaderButton... buttons)
    {
        if(Log.D) Log.d("addHeaderButtons(): location = " + location);
        mHeaderButtonLayout.addHeaderButtons(location, buttons);
        layoutChildViews();  // ????
    }    
    
    // TBD:
    //public void removeHeaderButton(String tag) {}
    //public void removeHeaderButtons(String... tags) {}
    

    // TBD:
    public void removeAllHeaderButtons()
    {
        mHeaderButtonLayout.removeAllHeaderButtons();
        layoutChildViews();  // ????
    }

    
    
    @Override
    public int getOrientation()
    {
        if(Log.D) Log.d("getOrientation()");

        // TODO Auto-generated method stub
        return super.getOrientation();
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        if(Log.D) Log.d("onDraw()");

        // TODO Auto-generated method stub
        super.onDraw(canvas);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b)
    {
        if(Log.D) Log.d("onLayout()");

        // TODO Auto-generated method stub
        super.onLayout(changed, l, t, r, b);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        if(Log.D) Log.d("onMeasure()");

        // TODO Auto-generated method stub
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    public void setOrientation(int orientation)
    {
        if(Log.D) Log.d("setOrientation()");

        // TODO Auto-generated method stub
        super.setOrientation(orientation);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if(Log.D) Log.d("onKeyDown()");

        // TODO Auto-generated method stub
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyShortcut(int keyCode, KeyEvent event)
    {
        if(Log.D) Log.d("onKeyShortcut()");

        // TODO Auto-generated method stub
        return super.onKeyShortcut(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event)
    {
        if(Log.D) Log.d("onKeyUp()");

        // TODO Auto-generated method stub
        return super.onKeyUp(keyCode, event);
    }
    
    
    
}

package ms.airy.lib.core.popup.helper;

import android.content.Context;
import android.os.AsyncTask;

import ms.airy.lib.core.popup.common.PopupInfo;
import ms.airy.lib.core.popup.prefs.PopupInfoPrefsManager;

import java.util.HashMap;
import java.util.Map;


/**
 */
public class AsyncPopupInfoProcessingTask extends AsyncTask<Map<String,PopupInfo>, Void, Map<String,PopupInfo>>
{
    private final Context context;
    private final PopupInfoProcessingCallback callback;
    public AsyncPopupInfoProcessingTask(Context context, PopupInfoProcessingCallback callback)
    {
        this.context = context.getApplicationContext();
        this.callback = callback;
    }

    @Override
    protected Map<String,PopupInfo> doInBackground(Map<String,PopupInfo>... params)
    {
        Map<String,PopupInfo> inMap = params[0];

        // TBD: Is this necesasry to copy the map? Just update the popupInfos???
        Map<String,PopupInfo> popupMap = new HashMap<String, PopupInfo>();
        for(String key : inMap.keySet()) {
            PopupInfo popupInfo = inMap.get(key);
            int counter = PopupInfoPrefsManager.getInstance(context).getCounter(key);
            int response = PopupInfoPrefsManager.getInstance(context).getResponse(key);
            long actionTime = PopupInfoPrefsManager.getInstance(context).getActionTime(key);
            popupInfo.setCounter(counter);
            popupInfo.setResponse(response);
            popupInfo.setActionTime(actionTime);
            popupMap.put(key, popupInfo);
        }

        return popupMap;
    }

    @Override
    protected void onPostExecute(Map<String,PopupInfo> popupMap)
    {
        if(Log.I) Log.i("AsyncPopupInfoProcessingTask.onPostExecute() popupMap = " + popupMap);
        callback.popupInfoProcessed(popupMap);
    }


}

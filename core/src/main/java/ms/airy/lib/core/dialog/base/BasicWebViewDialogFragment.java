package ms.airy.lib.core.dialog.base;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;

import java.net.MalformedURLException;
import java.net.URL;


// http://developer.android.com/guide/topics/ui/dialogs.html
// http://developer.android.com/reference/android/app/AlertDialog.html
public class BasicWebViewDialogFragment extends BaseDialogFragment
{
    // Layout containing the web view.
    private static final String ARG_LAYOUT_RESID = "webview_dialog_layout_resid";

    // Content string. (html)
    private static final String ARG_CONTENT_DATA = "webview_dialog_content_data";
    // Content URL (local or remote). If both data and URL are specified, the data takes precedence.
    private static final String ARG_CONTENT_URL = "webview_dialog_content_url";


    private View customView = null;
//    private ViewGroup customView = null;
    private WebView webView = null;

    // If both args are supplied, then the contentData takes precedence.
    private String contentData = null;
    private URL contentURL = null;
//    private String contentUrl = null;


    public static BasicWebViewDialogFragment newInstance(CharSequence title)
    {
        return newInstance(title, 0);
    }
    public static BasicWebViewDialogFragment newInstance(CharSequence title, int layoutResId)
    {
        BasicWebViewDialogFragment fragment = new BasicWebViewDialogFragment();
        addArguments(fragment, title, layoutResId);
        return fragment;
    }
    public static BasicWebViewDialogFragment newInstance(CharSequence title, String contentData)
    {
        return newInstance(title, 0, contentData);
    }
    public static BasicWebViewDialogFragment newInstance(CharSequence title, int layoutResId, String contentData)
    {
        BasicWebViewDialogFragment fragment = new BasicWebViewDialogFragment();
        addArguments(fragment, title, layoutResId);
        addArguments(fragment, contentData, null);
        return fragment;
    }
    public static BasicWebViewDialogFragment newInstance(CharSequence title, URL contentURL)
    {
        return newInstance(title, 0, contentURL);
    }
    public static BasicWebViewDialogFragment newInstance(CharSequence title, int layoutResId, URL contentURL)
    {
        BasicWebViewDialogFragment fragment = new BasicWebViewDialogFragment();
        addArguments(fragment, title, layoutResId);
        addArguments(fragment, null, contentURL);
        return fragment;
    }

    public static BasicWebViewDialogFragment newInstance(int titleResId)
    {
        return newInstance(titleResId, 0);
    }
    public static BasicWebViewDialogFragment newInstance(int titleResId, int layoutResId)
    {
        BasicWebViewDialogFragment fragment = new BasicWebViewDialogFragment();
        addArguments(fragment, titleResId, layoutResId);
        return fragment;
    }
    public static BasicWebViewDialogFragment newInstance(int titleResId, String contentData)
    {
        return newInstance(titleResId, 0, contentData);
    }
    public static BasicWebViewDialogFragment newInstance(int titleResId,  int layoutResId, String contentData)
    {
        BasicWebViewDialogFragment fragment = new BasicWebViewDialogFragment();
        addArguments(fragment, titleResId, layoutResId);
        addArguments(fragment, contentData, null);
        return fragment;
    }
    public static BasicWebViewDialogFragment newInstance(int titleResId, URL contentURL)
    {
        return newInstance(titleResId, 0, contentURL);
    }
    public static BasicWebViewDialogFragment newInstance(int titleResId,  int layoutResId, URL contentURL)
    {
        BasicWebViewDialogFragment fragment = new BasicWebViewDialogFragment();
        addArguments(fragment, titleResId, layoutResId);
        addArguments(fragment, null, contentURL);
        return fragment;
    }

    public BasicWebViewDialogFragment()
    {
    }

    protected static DialogFragment addArguments(DialogFragment fragment, CharSequence title)
    {
        return addArguments(fragment, title, 0);
    }
    protected static DialogFragment addArguments(DialogFragment fragment, CharSequence title, int layoutResId)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        if(title != null && title.length() > 0) {
            args.putCharSequence(ARG_TITLE, title);
        }
        if(layoutResId > 0) {
            args.putInt(ARG_LAYOUT_RESID, layoutResId);
        }
        return fragment;
    }
    protected static DialogFragment addArguments(DialogFragment fragment, int titleResId)
    {
        return addArguments(fragment, titleResId, 0);
    }
    protected static DialogFragment addArguments(DialogFragment fragment, int titleResId, int layoutResId)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        if(titleResId > 0) {  // ???
            args.putInt(ARG_TITLE_RESID, titleResId);
        }
        if(layoutResId > 0) {
            args.putInt(ARG_LAYOUT_RESID, layoutResId);
        }
        return fragment;
    }

    protected static DialogFragment addArguments(DialogFragment fragment, String contentData, URL contentURL)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        if(contentData != null) {
            args.putString(ARG_CONTENT_DATA, contentData);
        }
        if(contentURL != null) {
            args.putString(ARG_CONTENT_URL, contentURL.toExternalForm());
        }
        return fragment;
    }


    // ???
    protected WebView getWebView()
    {
        return webView;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        AlertDialog.Builder dialogBuilder = buildDialogBuilder();
        return dialogBuilder.create();
    }

    @Override
    protected AlertDialog.Builder buildDialogBuilder()
    {
        CharSequence title = getArguments().getCharSequence(ARG_TITLE);
        int titleResId = getArguments().getInt(ARG_TITLE_RESID);
        int layoutResId = getArguments().getInt(ARG_LAYOUT_RESID);
        if(layoutResId > 0) {
            // Log.w(">>>>>>>>>>>>>>>> layoutResId = " + layoutResId);

            // customView = getActivity().findViewById(layoutResId);
            LayoutInflater inflater = getActivity().getLayoutInflater();
            customView = inflater.inflate(layoutResId, null);
            // Log.w(">>>>>>>>>>>>>>>> customView = " + customView);

            webView = (WebView) ((ViewGroup) customView).getChildAt(0);
            // webView = (WebView) ((ViewGroup) customView).findViewWithTag("progressbar");
            // ...
//            // TBD: How to specify/pre-define the progress bar id.
//            webView = (WebView) customView.findViewById(1);   /// ????
//            // ...
            // webView = (WebView) getActivity().findViewById(layoutResId);
            // ...
        } else {
            Log.i("Layout not provided.");

            // TBD:
            // Use Linear layout with vertical scroll bar???
            // ....

//            customView = new FrameLayout(getActivity());
//            FrameLayout.LayoutParams cvlp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
//            // cvlp.setMargins(10,25,10,25);   // ???
//            ((FrameLayout) customView).setLayoutParams(cvlp);

            customView = new LinearLayout(getActivity());

            webView = new WebView(getActivity());
            webView.setPadding(10,25,10,25);
            // webView.setMinimumHeight(60);
            // webView.setMinimumWidth(60);
            ViewGroup.LayoutParams pblp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            webView.setLayoutParams(pblp);
            ((ViewGroup) customView).addView(webView);


            // ...
//            webView = new WebView(getActivity(), null, android.R.attr.progressBarStyleHorizontal);

            // ...
        }

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());

        dialogBuilder.setIcon(android.R.drawable.alert_dark_frame);
        if(title != null && title.length() > 0) {
            dialogBuilder.setTitle(title);
        } else {
            if (titleResId > 0) {  // ???
                dialogBuilder.setTitle(titleResId);
            }
        }
        if(customView != null) {
//          if(webView != null) {
            // ????
              dialogBuilder.setView(customView);
//              dialogBuilder.setView(webView);

        }

        contentData = getArguments().getString(ARG_CONTENT_DATA);
        if(contentData == null) {   // empty content is valid????
            String url = getArguments().getString(ARG_CONTENT_URL);
            try {
                contentURL = new URL(url);
            } catch (MalformedURLException e) {
                Log.e("Invalid URL: " + url, e);
            }
//            contentUrl = getArguments().getString(ARG_CONTENT_URL);
        }
        // Note that
        // both contentData and contentURL can be null at this point...
        // ...


        return dialogBuilder;
    }


    @Override
    public void onStart()
    {
        super.onStart();

        // TBD:
        // Load the content
        if(webView != null) {
            if(contentData != null) {
                // encoding: "base64" or null (for url percent encoding).
                webView.loadData(contentData, "text/html; charset=UTF-8", null);
            } else {
                if(contentURL != null) {
                    // TBD: It takes time to load the data from remote url...
                    //      Show "splash" image initially????
                    webView.loadUrl(contentURL.toExternalForm());
                } else {
                    // ????
                    Log.w("Content is not set for the webView.");
                }
//                if(contentUrl != null) {
//                    webView.loadUrl(contentUrl);
//                } else {
//                    // ????
//                    Log.w("Content is not set for the webView.");
//                }
            }
        } else {
            // ????
        }
        // ...

    }

    @Override
    public void onStop()
    {
        // TBD:
        super.onStop();
    }



}

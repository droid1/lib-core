package ms.airy.lib.core.dialog;


public interface ActionableNoticeDialogListener extends ActionableDialogListener
{
    // For "neutral" actions.
    // Note that the concetps of "negative" and "neutral" can somewhat overlap...
    void onNeutralClick(String tag);
}

package ms.airy.lib.core.status.client.callback;

import ms.airy.lib.core.status.client.AsyncStatusSendCallback;
import ms.airy.lib.core.status.common.StatusMessage;


/**
 */
public abstract class AbstractAsyncStatusSendCallback implements AsyncStatusSendCallback
{
    @Override
    public void onSendMessage(long id, int type, String message)
    {
        if(Log.I) Log.i("AbstractAsyncStatusSendCallback.onSendMessage() id = " + id + "; type = " + type + "; message = " + message);

    }

    @Override
    public void onSendStatusMessage(StatusMessage statusMessage)
    {
        if(Log.I) Log.i("AbstractAsyncStatusSendCallback.onSendStatusMessage() statusMessage = " + statusMessage);

    }

}

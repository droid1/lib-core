package ms.airy.lib.core.toast;

import android.view.Gravity;

/**

 */
class ToastUtil
{
    protected ToastUtil() {}

    // temporary
    protected static final int DEFAULT_GRAVITY = Gravity.BOTTOM|Gravity.RIGHT;
    protected static final int DEFAULT_XOFFSET = 0;
    protected static final int DEFAULT_YOFFSET = 0;
    protected static final int YDELTA = 10;  // temporary


}

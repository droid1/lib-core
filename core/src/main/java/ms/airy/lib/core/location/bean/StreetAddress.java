package ms.airy.lib.core.location.bean;

import java.util.Locale;


// Subset of Android Address class.
// http://developer.android.com/reference/android/location/Address.html
// It does not include, among other things, geocode, phone, etc...
public class StreetAddress
{
    private String featureName;    // e.g., Levi's Stadium....
    private String addressLine1;
    private String addressLine2;
    // private String subLocality;    // e.g., neighborhood?
    private String locality;       // e.g., city?
    // private String subAdminArea;   // e.g., county?
    private String adminArea;      // e.g., state/province?
    private String countryCode;
    // private String countryName;
    private String postalCode;     // e.g. ZIP code.
    private Locale locale;

    public StreetAddress()
    {
    }
    public StreetAddress(String featureName)
    {
        this.featureName = featureName;
    }
    public StreetAddress(Locale locale)
    {
        this.locale = locale;
    }


    public String getFeatureName()
    {
        return featureName;
    }
    public void setFeatureName(String featureName)
    {
        this.featureName = featureName;
    }

    public String getAddressLine1()
    {
        return addressLine1;
    }
    public void setAddressLine1(String addressLine1)
    {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2()
    {
        return addressLine2;
    }
    public void setAddressLine2(String addressLine2)
    {
        this.addressLine2 = addressLine2;
    }

    public String getLocality()
    {
        return locality;
    }
    public void setLocality(String locality)
    {
        this.locality = locality;
    }

    public String getAdminArea()
    {
        return adminArea;
    }
    public void setAdminArea(String adminArea)
    {
        this.adminArea = adminArea;
    }

    public String getCountryCode()
    {
        return countryCode;
    }
    public void setCountryCode(String countryCode)
    {
        this.countryCode = countryCode;
    }

    public String getPostalCode()
    {
        return postalCode;
    }
    public void setPostalCode(String postalCode)
    {
        this.postalCode = postalCode;
    }

    public Locale getLocale()
    {
        return locale;
    }
    public void setLocale(Locale locale)
    {
        this.locale = locale;
    }


    public static final class Builder
    {
        private String featureName;    // e.g., Levi's Stadium....
        private String addressLine1;
        private String addressLine2;
        private String locality;       // e.g., city?
        private String adminArea;      // e.g., state/province?
        private String countryCode;
        private String postalCode;     // e.g. ZIP code.
        private Locale locale;

        public Builder()
        {
        }
        public Builder(String featureName)
        {
            this.featureName = featureName;
        }
        public Builder(Locale locale)
        {
            this.locale = locale;
        }

        public String getFeatureName()
        {
            return featureName;
        }
        public Builder setFeatureName(String featureName)
        {
            this.featureName = featureName;
            return this;
        }

        public String getAddressLine1()
        {
            return addressLine1;
        }
        public Builder setAddressLine1(String addressLine1)
        {
            this.addressLine1 = addressLine1;
            return this;
        }

        public String getAddressLine2()
        {
            return addressLine2;
        }
        public Builder setAddressLine2(String addressLine2)
        {
            this.addressLine2 = addressLine2;
            return this;
        }

        public String getLocality()
        {
            return locality;
        }
        public Builder setLocality(String locality)
        {
            this.locality = locality;
            return this;
        }

        public String getAdminArea()
        {
            return adminArea;
        }
        public Builder setAdminArea(String adminArea)
        {
            this.adminArea = adminArea;
            return this;
        }

        public String getCountryCode()
        {
            return countryCode;
        }
        public Builder setCountryCode(String countryCode)
        {
            this.countryCode = countryCode;
            return this;
        }

        public String getPostalCode()
        {
            return postalCode;
        }
        public Builder setPostalCode(String postalCode)
        {
            this.postalCode = postalCode;
            return this;
        }

        public Locale getLocale()
        {
            return locale;
        }
        public Builder setLocale(Locale locale)
        {
            this.locale = locale;
            return this;
        }


        public StreetAddress build()
        {
            StreetAddress streetAddress = new StreetAddress();
            streetAddress.setFeatureName(this.featureName);
            streetAddress.setAddressLine1(this.addressLine1);
            streetAddress.setAddressLine2(this.addressLine2);
            streetAddress.setLocality(this.locality);
            streetAddress.setAdminArea(this.adminArea);
            streetAddress.setCountryCode(this.countryCode);
            streetAddress.setPostalCode(this.postalCode);
            streetAddress.setLocale(this.locale);
            // ....
            return streetAddress;
        }
    }


    // For debugging...
    @Override
    public String toString()
    {
        return "StreetAddress{" +
                "featureName='" + featureName + '\'' +
                ", addressLine1='" + addressLine1 + '\'' +
                ", addressLine2='" + addressLine2 + '\'' +
                ", locality='" + locality + '\'' +
                ", adminArea='" + adminArea + '\'' +
                ", countryCode='" + countryCode + '\'' +
                ", postalCode='" + postalCode + '\'' +
                ", locale=" + locale +
                '}';
    }

}

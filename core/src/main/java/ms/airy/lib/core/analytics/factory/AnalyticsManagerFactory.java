package ms.airy.lib.core.analytics.factory;

import android.content.Context;

import ms.airy.lib.core.analytics.config.AnalyticsConfigHelper;
import ms.airy.lib.core.analytics.manager.AnalyticsManager;
import ms.airy.lib.core.config.ConfigMaster;


public class AnalyticsManagerFactory
{
    private static AnalyticsManagerFactory INSTANCE = null;
    public static AnalyticsManagerFactory getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new AnalyticsManagerFactory(context!= null ? context.getApplicationContext() : null);  // ???
        } else {
            if(INSTANCE.getContext() == null && context != null) {  // ???
                INSTANCE = new AnalyticsManagerFactory(context.getApplicationContext());
            }
        }
        return INSTANCE;
    }

    // Application context.
    private final Context context;

    // "cache"
    private final boolean trackingEnabled;
    private final boolean amazonEnabled;
    private final boolean googleEnabled;

    // Dependency injected....
    private AnalyticsManager analyticsManager = null;

    private AnalyticsManagerFactory(Context context)
    {
        this.context = context;
        // config vars. cached.
        trackingEnabled = AnalyticsConfigHelper.getInstance(context).isAnalyticsEnabled();
        amazonEnabled = AnalyticsConfigHelper.getInstance(context).isAmazonAnalyticsEnabled();
        googleEnabled = AnalyticsConfigHelper.getInstance(context).isGoogleAnalyticsEnabled();

        if(context != null) {   // Can context be null???
            // ???
        }
    }
    protected final Context getContext()
    {
        return context;
    }

    public final boolean isTrackingEnabled()
    {
        return trackingEnabled;
    }
    public final boolean isAmazonEnabled()
    {
        return (trackingEnabled && amazonEnabled);
    }
    public final boolean isGoogleEnabled()
    {
        return (trackingEnabled && googleEnabled);
    }


    public AnalyticsManager getAnalyticsManager()
    {
        if(analyticsManager == null) {
            // ??????
        }
        return analyticsManager;
    }

    public void setAnalyticsManager(AnalyticsManager analyticsManager)
    {
        this.analyticsManager = analyticsManager;
    }

}

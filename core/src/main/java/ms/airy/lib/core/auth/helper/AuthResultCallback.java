package ms.airy.lib.core.auth.helper;


/**
*/
public interface AuthResultCallback
{
    void onAuthorizationSuccess(int type, String name, String authToken, String authScopes, long authTime);
    void onAuthorizationFailure(int type, String name, String reason);
}

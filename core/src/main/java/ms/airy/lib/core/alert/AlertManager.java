package ms.airy.lib.core.alert;

import android.content.Context;


// Creates/manages notifications for users.
// ???
// --> Just use NotificationScheduler...
// ...
public final class AlertManager
{
    private static AlertManager INSTANCE = null;
    public static AlertManager getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new AlertManager(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;


    private AlertManager(Context context)
    {
        this.context = context;
    }


    // TBD:
    // ....


}

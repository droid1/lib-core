package ms.airy.lib.core.auth.helper;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ms.airy.lib.core.auth.common.AccountType;
import ms.airy.lib.core.auth.common.TypeAndAccounts;
import ms.airy.lib.core.auth.common.UserAccount;
import ms.airy.lib.core.auth.config.AppAuthConfigManager;
import ms.airy.lib.core.auth.prefs.AccountPrefsStoreManager;
import ms.airy.lib.core.user.UserManager;


// TBD:
public final class AccountHelper
{
    private static AccountHelper INSTANCE = null;

    public static AccountHelper getInstance(Context context)
    {
        if (INSTANCE == null) {
            INSTANCE = new AccountHelper(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;

    private AccountHelper(Context context)
    {
        this.context = context;
    }



    // Returns null if the account of given type/name does not exist.
    public Account getAccount(int type, String name)
    {
        Account[] accounts = getAccounts(type);
        if(accounts != null) {
            for (Account a : accounts) {
                if (a.name.equals(name)) {
                    return a;
                }
            }
        }
        return null;
    }

    public Account[] getAccounts(int type)
    {
        return getAccounts(AccountTypeUtil.getTypeString(type));
    }
    public Account[] getAccounts(String type)
    {
        Account[] accounts = AccountManager.get(context).getAccountsByType(type);
        return accounts;
    }
    // ???
    protected Account[] getAccounts(String[] types)
    {
        AccountManager accountManager = AccountManager.get(context);
        List<Account> list = new ArrayList<Account>();
        if(types != null && types.length > 0) {
            for(String t : types) {
                Account[] accts = accountManager.getAccountsByType(t);
                list.addAll(Arrays.asList(accts));
            }
        }
        Account[] accounts = list.toArray(new Account[]{});
        return accounts;
    }

    public String[] getAccountNames(int type)
    {
        return getAccountNames(AccountTypeUtil.getTypeString(type));
    }
    public String[] getAccountNames(String type)
    {
        Account[] accounts = getAccounts(type);
        String[] names = new String[accounts.length];
        for (int i = 0; i < names.length; i++) {
            names[i] = accounts[i].name;
        }
        return names;
    }
    // ???
    protected String[] getAccountNames(String[] types)
    {
        Account[] accounts = getAccounts(types);
        String[] names = new String[accounts.length];
        for (int i = 0; i < names.length; i++) {
            names[i] = accounts[i].name;
        }
        return names;
    }


    // The following methods are to be used only at the beginning, when there is no primary account set.

    // ???
    public Account getDefaultAccount()
    {
        Account defaultAccount = null;
        Account[] accounts = getAccountsForDefaultType();
        if(accounts != null && accounts.length > 0) {
            // TBD: Check "features", etc. ???
            // for now, just use the first one (is the list alphabetical? chronological?)
            // temporary
            defaultAccount = accounts[0];
            // ...
        }
        return defaultAccount;
    }
    public int getDefaultAccountType()
    {
        Account defaultAccount = getDefaultAccount();
        int type = AccountType.TYPE_NONE;
        if(defaultAccount != null) {
            type = AccountTypeUtil.getAccountType(defaultAccount.type);
        }
        return type;
    }
    public String getDefaultAccountName()
    {
        Account defaultAccount = getDefaultAccount();
        String name = null;
        if(defaultAccount != null) {
            name = defaultAccount.name;
        }
        return name;
    }
    
    public Account[] getAccountsForDefaultType()
    {
        Account[] accounts = null;
        TypeAndAccounts typeAndAccounts = getDefaultTypeAndAccounts();
        if(typeAndAccounts != null) {
            int type = typeAndAccounts.getType();
            accounts = typeAndAccounts.getAccounts();
        }
        return accounts;
    }
    public String[] getAccountNamesForDefaultType()
    {
        Account[] accounts = getAccountsForDefaultType();
        String[] names = null;
        if(accounts != null) {
            names = new String[accounts.length];
            for (int i = 0; i < names.length; i++) {
                names[i] = accounts[i].name;
            }
        }
        return names;
    }


    // This is to be used when there is no primary account set.
    // If non-null TypeAndAccounts is returned,
    // then the returned type includes at least one Account.
    public TypeAndAccounts getDefaultTypeAndAccounts()
    {
        // TBD:
        // Check the app's "principal account type" first, if exists???
        //   --> the Ordered Type List should include the principal account type at the top....
        TypeAndAccounts typeAndAccounts = null;
        for (int type : AccountTypeUtil.getOrderedTypeList()) {
            Account[] accounts = getAccounts(type);
            if (accounts != null && accounts.length > 0) {
                // It returns the first type with non-empty account list.
                typeAndAccounts = new TypeAndAccounts(type, accounts);
                break;
            }
        }
        return typeAndAccounts;
    }

    public List<TypeAndAccounts> getTypeAndAccountsList()
    {
        List<TypeAndAccounts> typeAndAccountList = new ArrayList<TypeAndAccounts>();
        for (int type : AccountTypeUtil.getOrderedTypeList()) {
            Account[] accounts = getAccounts(type);
            // Excludes the types with no accounts...
            if (accounts != null && accounts.length > 0) {
                TypeAndAccounts typeAndAccounts = new TypeAndAccounts(type, accounts);
                typeAndAccountList.add(typeAndAccounts);
            }
        }
        return typeAndAccountList;
    }

    public Map<Integer, Account[]> getTypeAndAccountsMap()
    {
        Map<Integer, Account[]> typeAndAccountMap = new HashMap<Integer, Account[]>();
        for (int type : AccountTypeUtil.getOrderedTypeList()) {
            Account[] accounts = getAccounts(type);
            // Excludes the types with no accounts...
            if (accounts != null && accounts.length > 0) {
                typeAndAccountMap.put(type, accounts);
            }
        }
        return typeAndAccountMap;
    }
    // ???
    protected Account[] getAllAccounts()
    {
        List<Account> accounts = new ArrayList<Account>();
        Map<Integer, Account[]> map = getTypeAndAccountsMap();
        for(Integer t : map.keySet()) {
            Account[] aa = map.get(t);
            if(aa != null && aa.length > 0) {
                accounts.addAll(Arrays.asList(aa));
            }
        }
        return accounts.toArray(new Account[]{});
    }


    public Integer[] getAllAccountTypes()
    {
        List<Integer> types = new ArrayList<Integer>();
        for (int type : AccountTypeUtil.getOrderedTypeList()) {
            Account[] accounts = getAccounts(type);
            // Excludes the types with no accounts...
            if (accounts != null && accounts.length > 0) {
                types.add(type);
            }
        }
        return types.toArray(new Integer[]{});
    }
    public String[] getAllAccountTypeStrings()
    {
        List<String> types = new ArrayList<String>();
        for (int type : AccountTypeUtil.getOrderedTypeList()) {
            Account[] accounts = getAccounts(type);
            // Excludes the types with no accounts...
            if (accounts != null && accounts.length > 0) {
                types.add(AccountTypeUtil.getTypeString(type));
            }
        }
        return types.toArray(new String[]{});
    }


    // An account (e.g., stored account in prefs as the primary account, etc.)
    //     might have been removed since the last time.
    // We need to check if the account is still in the device.
    public boolean isAccountValid(int type, String name)
    {
        Account[] accounts = getAccounts(type);
        if(accounts == null || accounts.length == 0) {
            return false;
        }
        for(Account a : accounts) {
            if(a.name.equals(name)) {
                return true;
            }
        }
        return false;
    }
    public boolean isPrimaryAccountValid()
    {
        // The current implementation of getPrimaryAccount() already does the validity check.
        Account account = getPrimaryAccount();
        return (account != null);
    }

    // temporary
    public boolean hasPrimaryAccount()
    {
        return (getPrimaryAccount() != null);
    }

    // This could return null,
    //   (1) if the primary account has never been set, or
    //   (2) the current primary account has been removed from the device.
    public Account getPrimaryAccount()
    {
        Account account = AccountPrefsStoreManager.getInstance(context).getPrimaryAccount();
        if (Log.I) Log.i("Primary account = " + account);
        return account;
    }

    public int getPrimaryAccountType()
    {
        // (1) this is quicker, but this does not check the validity of the account.
//        int type = AccountPrefsStoreManager.getInstance(context).getPrimaryAccountType();
//        if (Log.I) Log.i("Primary account type = " + type);
//        return type;
        // (2) this is probably slower, but it does validity check.
        int type = AccountType.TYPE_NONE;  // ???
        Account account = getPrimaryAccount();
        if(account != null) {
            type = AccountTypeUtil.getAccountType(account.type);
        }
        return type;
    }

    public String getPrimaryAccountName()
    {
        // (1) this is quicker, but this does not check the validity of the account.
//        String name = AccountPrefsStoreManager.getInstance(context).getPrimaryAccountName();
//        if (Log.I) Log.i("Primary account name = " + name);
//        return name;
        // (2) this is probably slower, but it does validity check.
        String name = null;  // ???
        Account account = getPrimaryAccount();
        if(account != null) {
            name = account.name;
        }
        return name;
    }

    public void setPrimaryAccount(Account account)
    {
        AccountPrefsStoreManager.getInstance(context).setPrimaryAccount(account);
    }
    public void setPrimaryAccount(int type, String name)
    {
        AccountPrefsStoreManager.getInstance(context).setPrimaryAccount(type, name);
    }

    public boolean isPrimaryAccountTypeGoogle()
    {
        int pt = getPrimaryAccountType();
        return (pt == AccountType.TYPE_GOOGLE);
    }


    // TBD:
    // Set main accounts, etc..
    // Unset main accounts --> Log out from the provider???
    // ...


    public Account getMainAccount(int type)
    {
        Account account = AccountPrefsStoreManager.getInstance(context).getMainAccountForType(type);
        if (Log.I) Log.i("Main account for type = " + type + ": account = " + account);
        return account;
    }
    public String getMainAccountName(int type)
    {
        // (1) this is quicker, but this does not check the validity of the account.
//        String name = AccountPrefsStoreManager.getInstance(context).getMainAccountNameForType(type);
//        if (Log.I) Log.i("Main account: name = " + name + " for type = " + type);
//        return name;
        // (2) this is probably slower, but it does validity check.
        String name = null;  // ???
        Account account = getMainAccount(type);
        if(account != null) {
            name = account.name;
        }
        return name;
    }

    public void setMainAccount(Account account)
    {
        AccountPrefsStoreManager.getInstance(context).setMainAccount(account);
    }
    public void setMainAccount(int type, String name)
    {
        AccountPrefsStoreManager.getInstance(context).setMainAccount(type, name);
    }


    public int getSizeForAccountType(int type)
    {
        int size = 0;
        Account[] accounts = getAccounts(type);
        if(accounts != null) {
            size = accounts.length;
        }
        return size;
    }

    public int getSizeForDefaultAccountType()
    {
        int size = 0;
        TypeAndAccounts typeAndAccounts = getDefaultTypeAndAccounts();
        if(typeAndAccounts != null) {
            Account[] accounts = typeAndAccounts.getAccounts();
            if(accounts != null) {
                size = accounts.length;
            }
        }
        return size;
    }

    public int getSizeForPrincipalAccountType()
    {
        int type = AppAuthConfigManager.getInstance(context).getApplicationAuthConfig().getPrincipalAccountType();
        if(AccountTypeUtil.isValid(type)) {
            return getSizeForAccountType(type);
        } else {
            Log.w("Invalid principal account type = " + type);
            return 0;
        }
    }


    public Account[] getAccountsForPrincipalType()
    {
        int type = AppAuthConfigManager.getInstance(context).getApplicationAuthConfig().getPrincipalAccountType();
        if(AccountTypeUtil.isValid(type)) {
            return getAccounts(type);
        } else {
            Log.w("Invalid principal account type = " + type);
            return null;
        }
    }


    public boolean isPrincipalAccountValid()
    {
        Account account = getPrincipalAccount();
        return (account != null);
    }

    // temporary
    public boolean hasPrincipalAccount()
    {
        return (getPrincipalAccount() != null);
    }

    public Account getPrincipalAccount()
    {
        int type = AppAuthConfigManager.getInstance(context).getApplicationAuthConfig().getPrincipalAccountType();
        if(AccountTypeUtil.isValid(type)) {
            return getMainAccount(type);
        } else {
            Log.w("Invalid principal account type = " + type);
            return null;
        }
    }
    // Note that this could be different from ApplicationAuthConfig.getPrincipalAccountType()
    //     because the principal account may not have been set.
    public int getPrincipalAccountType()
    {
        int type = AccountType.TYPE_NONE;
        Account account = getPrincipalAccount();
        if(account != null) {
            type = AccountTypeUtil.getAccountType(account.type);
        }
        return type;
    }
    public String getPrincipalAccountName()
    {
        String name = null;  // ???
        Account account = getPrincipalAccount();
        if(account != null) {
            name = account.name;
        }
        return name;
    }

    public void setPrincipalAccount(String name)
    {
        int type = AppAuthConfigManager.getInstance(context).getApplicationAuthConfig().getPrincipalAccountType();
        if(AccountTypeUtil.isValid(type)) {
            setMainAccount(type, name);
        } else {
            Log.w("Invalid principal account type = " + type);
        }
    }


}

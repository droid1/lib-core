package ms.airy.lib.core.auth.oauth2;

import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.os.Bundle;

import java.io.IOException;

import ms.airy.lib.core.auth.helper.AccountTypeUtil;


// Default AccountManagerCallback implementation.
// Despite the package name, the classes in this package are not specific to "OAuth2"
// It's Android AccountManager helper classes....
public class DefaultAccountAuthCallback implements AccountManagerCallback<Bundle>
{
    private final OAuth2AuthCallback callback;   // parent.
    private final int accountType;
    private final String accountName;
    private final String authScopes;

    public DefaultAccountAuthCallback(OAuth2AuthCallback callback, int type, String name, String scopes)
    {
        this.callback = callback;
        this.accountType = type;
        this.accountName = name;
        this.authScopes = scopes;
    }


    //////////////////////////////////////////////////
    // AccountManagerCallback<Bundle> interface

    @Override
    public void run(AccountManagerFuture<Bundle> future)
    {
        Log.i(">>>>>>>> DefaultAccountAuthCallback.run()");

        try {
            if(future.isDone()) {
                Log.d("DefaultAccountAuthCallback.run(). future.isDone() == true.");

                Bundle bundle = future.getResult();

                String type = bundle.getString(AccountManager.KEY_ACCOUNT_TYPE);
                String name = bundle.getString(AccountManager.KEY_ACCOUNT_NAME);
                String authToken = bundle.getString(AccountManager.KEY_AUTHTOKEN);
                // KEY_AUTH_FAILED_MESSAGE ???

                if(accountType != AccountTypeUtil.getAccountType(type) || !accountName.equals(name)) {
                    // This cannot happen!!!
                    Log.w("DefaultAccountAuthCallback: Returned account type/name do not match. type = " + type + "; name = " + name);
                    callback.onOAuth2AuthFailure(accountType, accountName, "Returned account type/name do not match. type = " + type + "; name = " + name);
                } else if(authToken == null || authToken.isEmpty()) {
                    // ????
                    Log.w("DefaultAccountAuthCallback: Null/empty authToken retrieved.");
                    callback.onOAuth2AuthFailure(accountType, accountName, "Null/empty authToken retrieved.");
                } else {
                    Log.i("DefaultAccountAuthCallback: Auth success.");
                    // Note: authTime timestamp can be a bit misleading
                    //       since the authToken may have been returned from Android AccountManager cache...
                    //       (unless we have called invalidateAuthToken() before calling getAuthToken()...)
                    long authTime = System.currentTimeMillis();
                    callback.onOAuth2AuthSuccess(accountType, accountName, authToken, authScopes, authTime);
                }
            } else {
                // Can this happen???
                Log.w("DefaultAccountAuthCallback.run(). future.isDone() == false.");
            }
        } catch(AuthenticatorException e1) {
            Log.e("Auth error while fetching authToken for accountType = " + accountType + "; accountName = " + accountName);
            callback.onOAuth2AuthFailure(accountType, accountName, e1.getMessage());
        } catch(OperationCanceledException e2) {
            Log.e("Auth canceled while fetching authToken for accountType = " + accountType + "; accountName = " + accountName);
            callback.onOAuth2AuthFailure(accountType, accountName, e2.getMessage());
        } catch(IOException e3) {
            Log.e("Network error while fetching authToken for accountType = " + accountType + "; accountName = " + accountName);
            callback.onOAuth2AuthFailure(accountType, accountName, e3.getMessage());
        }
    }

}

package ms.airy.lib.core.notification;

import android.app.Activity;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;


/**
 */
public class NotificationBuilder
{
    private NotificationBuilder() {}


    public static Notification buildNotification(Context context, int requestCode, Class<? extends Activity> clss, String contentTitle, String contentText, int smallIconRes)
    {
        PendingIntent contentIntent = PendingIntent.getActivity(context,
                requestCode,
                new Intent(context, clss),
                Intent.FLAG_ACTIVITY_NEW_TASK);
        return buildNotification(context, contentIntent, contentTitle, contentText, smallIconRes);
    }

    public static Notification buildNotification(Context context, PendingIntent contentIntent, String contentTitle, String contentText, int smallIconRes)
    {
        // TBD:
//        Notification notification = new Notification.Builder(mContext)
//                .setContentTitle(contentTitle)
//                .setContentText(contentText)
//                .setSmallIcon(smallIconRes)
//                .setContentIntent(contentIntent)
//                .build();
        Notification notification = new NotificationCompat.Builder(context)
                .setContentTitle(contentTitle)
                .setContentText(contentText)
                .setSmallIcon(smallIconRes)
                .setContentIntent(contentIntent)
                .setAutoCancel(true)
                .build();
        return notification;
    }


}

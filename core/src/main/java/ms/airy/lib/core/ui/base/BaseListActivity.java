package ms.airy.lib.core.ui.base;

import ms.airy.lib.core.help.fragment.HelpSectionDialogFragment;
//import com.google.analytics.tracking.android.EasyTracker;
import ms.airy.lib.core.common.Config;
import ms.airy.lib.core.registry.Manifest;
import ms.airy.lib.core.ui.dashbar.DashBar;
import ms.airy.lib.core.ui.dashbar.DashBarView;
import ms.airy.lib.core.ui.headerbar.HeaderBar;
import ms.airy.lib.core.ui.headerbar.HeaderBarView;
import ms.airy.lib.core.util.ConfigFactoryManager;

import android.app.ActionBar;
import android.app.ListActivity;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.LinearLayout;


// TBD: Not working yet.
public class BaseListActivity extends ListActivity implements Navigatable
{
    // temporary
    protected static final int RUNTIME_OS_VERSION = android.os.Build.VERSION.SDK_INT;

    // temporary
    private Config mConfig;
    private boolean mUseTracking;
    private boolean mUseHeaderBar;
    private boolean mUseDashBar;

    // temporary
    private boolean mUseActionBar = false; //....
    
    // temporary
    private HeaderBar mHeaderBar = null;
    private DashBar mDashBar = null;

    // tbd
    // Note that we only support "one-level" UP....
    private Intent mCalledIntent = null;
    private Intent mUpIntent = null;

    
    // Tracker. 
    // https://developers.google.com/analytics/devguides/collection/android/v3/
    // private EasyTracker mTracker = null;


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        
        // ????
        setConfig();

        
        mCalledIntent = getIntent();
        Bundle extras = mCalledIntent.getExtras();
        if(extras != null) {
            if(extras.containsKey(Manifest.EXTRA_CORE_UP_INTENT)) {
                mUpIntent = (Intent) extras.get(Manifest.EXTRA_CORE_UP_INTENT);
            }
        }

        
        
        if(mUseTracking) {
            
            // mTracker = EasyTracker.getInstance(this);

            // Start the mTracker in manual dispatch mode...
            //EasyTracker.getInstance(this).activityStart(this);

        }

        
        
//      Button createEventButton = (Button)findViewById(R.id.NewEventButton);
//      createEventButton.setOnClickListener(new OnClickListener() {
//        @Override
//        public void onClick(View v) {
//          mTracker.trackEvent(
//              "Clicks",  // Category
//              "Button",  // Action
//              "clicked", // Label
//              77);       // Value
//        }
//      });
      
//      Button createPageButton = (Button)findViewById(R.id.NewPageButton);
//      createPageButton.setOnClickListener(new OnClickListener() {
//        @Override
//        public void onClick(View v) {
//          // Add a Custom Variable to this pageview, with name of "Medium" and value "MobileApp"
//          mTracker.setCustomVar(1, "Medium", "Mobile App");
//          // Track a page view. This is probably the best way to track which parts of your application
//          // are being used.
//          // E.g.
//          // mTracker.trackPageView("/help"); to track someone looking at the help screen.
//          // mTracker.trackPageView("/level2"); to track someone reaching level 2 in a game.
//          // mTracker.trackPageView("/uploadScreen"); to track someone using an upload screen.
//          mTracker.trackPageView("/testApplicationHomeScreen");
//        }
//      });

//      Button dispatchButton = (Button)findViewById(R.id.DispatchButton);
//      dispatchButton.setOnClickListener(new OnClickListener() {
//        @Override
//        public void onClick(View v) {
//          // Manually start a dispatch, not needed if the mTracker was started with a dispatch
//          // interval.
//          mTracker.dispatch();
//        }
//      });

        
        
    }
    
    
    
    
    @Override
    protected void onStart()
    {
        super.onStart();
        
        // Remove stock action bar
        ActionBar actionBar = getActionBar();
        //int height = actionBar.getHeight();
        // width????
        if(actionBar != null) {
            actionBar.hide();
        }
         
    }

    
    // TBD: Use more general View instead of ViewGroup????
    protected void initializeHeaderBar(ViewGroup topLevel)
    {
        if(mHeaderBar == null) {
            mHeaderBar = new HeaderBar(this);
            refreshUpIntent();
            redoLayout(topLevel);
        } else {
            // ....
            //mHeaderBar.resetHeaderItems();
        }
    }
    protected void initializeDashBar(ViewGroup topLevel)
    {
        if(mDashBar == null) {
            mDashBar = new DashBar(this);
            redoLayout(topLevel);
        } else {
            // ????
            //mDashBar.resetNavItems();
        }
    }
    // TBD: 
    private void initializeMenuBars(ViewGroup topLevel)
    {
        // initialize the mHeaderBar/mDashBar...
        if(mHeaderBar == null) {
            mHeaderBar = new HeaderBar(this);
            refreshUpIntent();
        }
        if(mDashBar == null) {
            mDashBar = new DashBar(this);
        }
        // ????
        redoLayout(topLevel);
    }
    

    // HeaderBar exposes public API for adding/removing NavButton, etc.
    protected HeaderBar getHeaderBar()
    {
        return mHeaderBar;
    }
    protected void setHeaderBar(HeaderBar headerBar, ViewGroup topLevel)
    {
        mHeaderBar = headerBar;
        redoLayout(topLevel);
    }    
    
    // TBD:
    public Intent getUpIntent()
    {
        if(mUpIntent != null) {
            return mUpIntent;
        } else {
            return mCalledIntent;  // ????
        }
    }
    protected void setUpIntent(Intent upIntent)
    {
        mUpIntent = upIntent;
        refreshUpIntent();
    }
    protected void refreshUpIntent()
    {
        if(mHeaderBar == null) {
            if(Log.W) Log.w("UpIntent cannot be set because HeaderBar is null.");
            return;
        } else {
            mHeaderBar.setUpIntent(mUpIntent);
        }
    }

    
    // DashBar exposes public API for adding/removing NavButton, etc.
    protected DashBar getDashBar()
    {
        return mDashBar;
    }
    private void setDashBar(DashBar dashBar, ViewGroup topLevel)
    {
        mDashBar = dashBar;
        redoLayout(topLevel);
    }


    protected void redoLayout(ViewGroup topLevel)
    {

        // temporary....
        // int height = getWindow().getWindowManager().getDefaultDisplay().getHeight();
        // int width = getWindow().getWindowManager().getDefaultDisplay().getWidth();
        Point outSize = new Point();
        getWindow().getWindowManager().getDefaultDisplay().getSize(outSize);   // getSize() vs getRealSize() ????
        int height = outSize.y;
        int width = outSize.x;
        if(Log.D) Log.d("height = " + height);
        if(Log.D) Log.d("width = " + width);

        // TBD: Get the current size of topLevel view????
        // ...
        
        
        // temporary....

        // Vertical content layout.
        LinearLayout newContentLayout = new LinearLayout(this);
        newContentLayout.setOrientation(LinearLayout.VERTICAL);

        // [1] Add the HeaderBar.
        if(isUseHeaderBar()) {
            HeaderBarView hView = getHeaderBar().getView();
            if(hView != null) {
                ViewGroup hVg = (ViewGroup)(hView.getParent());
                if(hVg != null) {
                    hVg.removeView(hView);
                }
                LinearLayout.LayoutParams headerBarParams = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT); // ????
                newContentLayout.addView(hView, headerBarParams);
            } else {
                // This cannot happen...
            }
        }
        
        // [2] Add the original content layout.
        if(topLevel != null) {
            ViewGroup vg = (ViewGroup)(topLevel.getParent());
            if(vg != null) {
                vg.removeView(topLevel);
            }
            LinearLayout.LayoutParams topLevelParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT); // ????
            newContentLayout.addView(topLevel, topLevelParams);
        }

        // Horizontal top level layout.
        LinearLayout newTopLayout = new LinearLayout(this);
        newTopLayout.setOrientation(LinearLayout.HORIZONTAL);
        
        // [3] Add the DashBar.
        if(isUseDashBar()) {
            DashBarView dView = getDashBar().getView();
            if(dView != null) {
                ViewGroup dVg = (ViewGroup)(dView.getParent());
                if(dVg != null) {
                    dVg.removeView(dView);
                }
                LinearLayout.LayoutParams dashBarParams = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT); // ????
        //                  LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT); // ????
                newTopLayout.addView(getDashBar().getView(), dashBarParams);
            } else {
                // This cannot ahppen...
            }
        }
        
        // [4] Add the contentLayout to the Top level layout.
        LinearLayout.LayoutParams contentLayoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT); // ????
        newTopLayout.addView(newContentLayout, contentLayoutParams);

        // [5] Set the new layout for the view.
        setContentView(newTopLayout);
    }



    @Override
    public void onStop()
    {
        super.onStop();
        
//        // Stop the tracker.
//        EasyTracker.getInstance(this).activityStop(this);
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

        // Stop the mTracker when it is no longer needed.
        // if(mTracker != null) {
        //     mTracker.stop();
        // }
    }

    
    private void setConfig()
    {
        // ????
        setConfig(ConfigFactoryManager.getConfigFactory().getConfig());
    }
    public void setConfig(Config config)
    {
        mConfig = config;
        updateConfigValues();
    }
    
    private void updateConfigValues()
    {
        // TBD. ????
        // Note: this might overwrite user set values...... 
        mUseTracking = mConfig.isUseGa();
        mUseHeaderBar = mConfig.isUseHeaderBar();
        mUseDashBar = mConfig.isUseDashBar();
        // ...
        
        // TBD: Refresh other vars such as mTracker as well...
        // ...
    }
    
    


    public boolean isUseTracking()
    {
        return mUseTracking;
    }
    public void setUseTracking(boolean useTracking)
    {
        this.mUseTracking = useTracking;
    }

    public boolean isUseHeaderBar()
    {
        if(mUseHeaderBar && mHeaderBar != null) {
            return true;
        } else {
            return false;
        }
    }
    public void setUseHeaderBar(boolean useHeaderBar)
    {
        this.mUseHeaderBar = useHeaderBar;
    }

    public boolean isUseDashBar()
    {
        if(mUseDashBar && mDashBar != null) {
            return true;
        } else {
            return false;
        }
    }
    public void setUseDashBar(boolean useDashBar)
    {
        this.mUseDashBar = useDashBar;
    }



    // temporary
    public void showHelpDialog(String topic)
    {
        HelpSectionDialogFragment helpFragment = HelpSectionDialogFragment.newInstance(topic);
        helpFragment.show(getFragmentManager(), HelpSectionDialogFragment.FRAGMENT_TAG);
    }
    // temporary
    
    
    
    
    @Override
	public boolean onCreateOptionsMenu(Menu menu)
    {
		super.onCreateOptionsMenu(menu);
        
        
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.base_list, menu);

        
	    return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
	    switch (item.getItemId()) {
	    
//	    case R.id.menu_base_list_main:
//	        doMain();
//	        return true;
//	    case R.id.menu_base_list_search:
//	        // doSearch();
//	        return true;
//	    case R.id.menu_base_list_help:
//	        // doHelp();
//	        return true;
//	    case R.id.menu_base_list_close:
//	        // doClose();
//	        return true;

	    default:
	        return super.onOptionsItemSelected(item);
	    }
	}




    // temporary
    private void doMain()
    {
        // TBD: Is this save or cancel....?
        // TBD: Or, cancel all data change but save/keep the view-related states (e.g., full-screen)???
        
        // TBD
        if(Log.D) Log.d("doMain() called.");
//        //Intent intent = new Intent(this, MemoroidMain.class);
//        Intent intent = new Intent(CommonConstants.ACTION_APP_MAIN);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);   // this with "singleTop" unwinds the activity stack.
//        startActivity(intent);
//        finish();
    }

 
}
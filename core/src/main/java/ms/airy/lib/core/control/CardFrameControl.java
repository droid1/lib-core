package ms.airy.lib.core.control;

import android.content.Context;
import android.util.AttributeSet;


// TBD:
// "CardFrame" has fixed sizes.
public class CardFrameControl extends AnamorphicFrameLayout
{
    public CardFrameControl(Context context)
    {
        super(context);
    }

    public CardFrameControl(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public CardFrameControl(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
    }

    // tbd
    // ...

}

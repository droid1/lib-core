package ms.airy.lib.core.contacts.projection;

import android.provider.ContactsContract;


// TBD:
// Predefined "projections"
public final class ContactsDataProjectionRegistry
{
    private ContactsDataProjectionRegistry() {};


    // temporary

    private static final String[] _PROJECTION_01 = new String[] {
            ContactsContract.Contacts._ID,
            ContactsContract.Contacts.DISPLAY_NAME_PRIMARY,
            ContactsContract.Contacts.CONTACT_STATUS,
            ContactsContract.Contacts.CONTACT_PRESENCE,
            ContactsContract.Contacts.PHOTO_ID,
            ContactsContract.Contacts.LOOKUP_KEY
    };
    public static String[] getProjection01()
    {
        return _PROJECTION_01;
    }

    // ????
    // All "data"
    private static final String[] _PROJECTION_02 = new String[] {
            ContactsContract.Data._ID,
            ContactsContract.Data.MIMETYPE,
            ContactsContract.Data.DATA1,
            ContactsContract.Data.DATA2,
            // ...
            ContactsContract.Data.DATA15,
            // ...
            ContactsContract.Data.LOOKUP_KEY
    };
    public static String[] getProjection02()
    {
        return _PROJECTION_02;
    }

    // ????
    // All "email" related data?
    private static final String[] _PROJECTION_03 = new String[] {
            ContactsContract.CommonDataKinds.Email._ID,
            ContactsContract.CommonDataKinds.Email.ADDRESS,
            ContactsContract.CommonDataKinds.Email.TYPE,
            ContactsContract.CommonDataKinds.Email.LABEL,
            ContactsContract.CommonDataKinds.Email.LOOKUP_KEY
    };
    public static String[] getProjection03()
    {
        return _PROJECTION_03;
    }


    // ????
    public static String[] getDefaultProjection()
    {
        // temporary
        return getProjection01();
    }

    // ????
    public static String[] getEmailProjection()
    {
        // temporary
        return getProjection03();
    }

}

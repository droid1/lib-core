package ms.airy.lib.core.popup.helper;

import android.content.Context;
import android.os.AsyncTask;

import ms.airy.lib.core.popup.common.PopupInfo;

import java.util.Map;


/**
 */
public class AsyncLocalPopupLoadTask extends AsyncTask<Void, Void, Map<String,PopupInfo>>
{
    private final Context context;
    private final LocalPopupLoadCallback callback;
    public AsyncLocalPopupLoadTask(Context context, LocalPopupLoadCallback callback)
    {
        this.context = context.getApplicationContext();
        this.callback = callback;
    }

    @Override
    protected Map<String,PopupInfo> doInBackground(Void... params)
    {
        Map<String,PopupInfo> popupMap = LocalPopupLoader.getInstance(context).getPopupMap();
        return popupMap;
    }

    @Override
    protected void onPostExecute(Map<String,PopupInfo> popupMap)
    {
        if(Log.I) Log.i("AsyncLocalPopupLoadTask.onPostExecute() popupMap = " + popupMap);
        callback.localPopupContentLoaded(popupMap);
    }


}

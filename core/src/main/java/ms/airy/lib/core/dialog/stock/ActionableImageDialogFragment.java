package ms.airy.lib.core.dialog.stock;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

import ms.airy.lib.core.R;
import ms.airy.lib.core.dialog.ActionableAssertDialogListener;
import ms.airy.lib.core.dialog.base.BasicImageDialogFragment;


public class ActionableImageDialogFragment extends BasicImageDialogFragment
{
//    private ActionableAssertDialog mActivity = null;

    public static ActionableImageDialogFragment newInstance(CharSequence title, int imageResId)
    {
        ActionableImageDialogFragment fragment = new ActionableImageDialogFragment();
        addArguments(fragment, title, imageResId);
        return fragment;
    }
    public static ActionableImageDialogFragment newInstance(int titleResId, int imageResId)
    {
        ActionableImageDialogFragment fragment = new ActionableImageDialogFragment();
        addArguments(fragment, titleResId, imageResId);
        return fragment;
    }
    public ActionableImageDialogFragment()
    {
    }


    protected AlertDialog.Builder buildDialogBuilder()
    {
        AlertDialog.Builder dialogBuilder = super.buildDialogBuilder();

        dialogBuilder.setPositiveButton(R.string.basic_image_dialog_ok,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int whichButton) {
                        ((ActionableAssertDialogListener) getActivity()).onPositiveClick(ActionableImageDialogFragment.this.getTag());
                    }
                }
        );
        dialogBuilder.setNegativeButton(R.string.basic_image_dialog_dismiss,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int whichButton) {
                        ((ActionableAssertDialogListener) getActivity()).onNegativeClick(ActionableImageDialogFragment.this.getTag());
                    }
                }
        );
        dialogBuilder.setNeutralButton(R.string.basic_image_dialog_cancel,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int whichButton) {
                        ((ActionableAssertDialogListener) getActivity()).onNeutralClick(ActionableImageDialogFragment.this.getTag());
                    }
                }
        );

        return dialogBuilder;
    }


    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try {
            ActionableAssertDialogListener mActivity = (ActionableAssertDialogListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement ActionableAssertDialog");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
//        mActivity = null;
    }


}

package ms.airy.lib.core.control;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;


// TBD:
// Use interface ????
public abstract class RepeatingTimerControl extends TimerControl
{
    // repeats == 1 means no repeats.
    private int repeats;
    // 0 <= currentRep < repeats
    private int currentRep;


    public RepeatingTimerControl(Context context)
    {
        super(context);
        initControl();
    }
    public RepeatingTimerControl(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        initControl();
    }
    public RepeatingTimerControl(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        initControl();
    }

    private void initControl()
    {
        setFocusable(false); // For display only. No user input.

        // temporary
        repeats = 1;
        currentRep = 0;
    }


    public int getRepeats()
    {
        return repeats;
    }
    public void setRepeats(int repeats)
    {
        this.repeats = repeats;
        setDirty();  // ???
    }

    public int getCurrentRep()
    {
        return currentRep;
    }
    public void setCurrentRep(int currentRep)
    {
        this.currentRep = currentRep;
    }




    // Life cycle methods

    // TBD:
    // Implement the "repetition"
    // ...
    // ...




    @Override
    public Parcelable onSaveInstanceState()
    {
        Log.d("onSaveInstanceState() Called.");
        Parcelable superState = super.onSaveInstanceState();
        SavedState savedState = new SavedState(superState);
        savedState.setRepeats(getRepeats());
        savedState.setCurrentRep(getCurrentRep());
        return savedState;
    }

    @Override
    public void onRestoreInstanceState(Parcelable state)
    {
        Log.d("onRestoreInstanceState() Called.");
        SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(savedState.getSuperState());
        setRepeats(savedState.getRepeats());
        setCurrentRep(savedState.getCurrentRep());
    }


    protected static class SavedState extends TimerControl.SavedState implements Parcelable
    {
        private int repeats;
        private int currentRep;
        // etc...

        public SavedState(Parcelable superState)
        {
            super(superState);
        }
        public SavedState(Parcel source)
        {
            super(source);
            repeats = source.readInt();
            currentRep = source.readInt();
        }


        public int getRepeats()
        {
            return repeats;
        }
        public void setRepeats(int repeats)
        {
            this.repeats = repeats;
        }

        public int getCurrentRep()
        {
            return currentRep;
        }
        public void setCurrentRep(int currentRep)
        {
            this.currentRep = currentRep;
        }


        public static final Creator<SavedState> CREATOR = new Creator<SavedState>() {
            @Override
            public SavedState createFromParcel(Parcel source)
            {
                return new SavedState(source);
            }
            @Override
            public SavedState[] newArray(int size)
            {
                return new SavedState[size];
            }
        };

        @Override
        public void writeToParcel(Parcel dest, int flags)
        {
            super.writeToParcel(dest, flags);
            dest.writeInt(repeats);
            dest.writeInt(currentRep);
        }


        // For debugging...
        @Override
        public String toString()
        {
            return "SavedState{" +
                    "repeats=" + repeats +
                    "currentRep=" + currentRep +
                    '}';
        }
    }
}

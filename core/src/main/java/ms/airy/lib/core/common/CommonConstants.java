package ms.airy.lib.core.common;


// Constants used "across activity boundaries".
// TBD: Define constants (e.g., action-related, intent-related) in LibRegistry?
public class CommonConstants
{
	protected CommonConstants() {}	// Prevent instantiation.

    // common consts...
    //public final static String GUID_NULL = "00000000-0000-0000-0000-000000000000";
    //public final static String GUID_ONE = "00000000-0000-0000-0000-000000000001";
    public final static String USER_SELF_NAME = "_user_self_name";
    public final static String USER_SELF_GUID = "00000000-0000-0000-0000-111111111111";

    
    // Notification IDs.
    public static final int NOTIFICATION_ID_MEMO = 101;    
    // ....

	// TBD:
	// Actions, Attributes, ...	
    public final static int ACTOR_INVALID = 100000;
    public final static String ACTION_EDIT_OPTION = "ms.airy.lib.action.EDIT_OPTION";
    public final static String ACTION_NEW_MEMO = "ms.airy.lib.action.NEW_MEMO";
	public final static String ACTION_PLAY = "ms.airy.lib.action.PLAY";

	
	

}

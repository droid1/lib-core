package ms.airy.lib.core.status.client;

import ms.airy.lib.core.status.common.StatusMessage;

import java.util.List;


/**
 */
public interface AsyncStatusFetchCallback
{
    // TBD: Add filters such as type, etc. ???

    // void onFetchMessage(String message, long id);
    void onFetchStatusMessage(StatusMessage statusMessage, long id);
    void onFetchNewMessages(List<Long> ids);
    // void onFetchNewStatusMessages(List<StatusMessage> statusMessages);
    void onFetchMessages(List<Long> ids, long since);
    // void onFetchStatusMessages(List<StatusMessage> statusMessages, long since);
}

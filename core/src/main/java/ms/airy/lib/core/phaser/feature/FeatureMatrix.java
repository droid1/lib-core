package ms.airy.lib.core.phaser.feature;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import ms.airy.lib.core.phaser.common.Feature;


// Place-holder.
// Juse use FeatureHelper...
public final class FeatureMatrix
{
    private final Set<Feature> features;
//    private final Map<Long, Feature> features;

    // ???
    private FeatureMatrix() 
    {
        features = new HashSet<Feature>();
//        features = new HashMap<Long, Feature>();
    }

    private void init()
    {
        // TBD:
        // Add all rank- phase- dependent features here ????
        // ....

        // ...
        Feature f1 = new Feature("feature-abc");
        features.add(f1);
        // etc..

    }


    public void addFeature(Feature feature)
    {
        features.add(feature);
    }

    public Set<Feature> getFeatures()
    {
        return features;
    }


}

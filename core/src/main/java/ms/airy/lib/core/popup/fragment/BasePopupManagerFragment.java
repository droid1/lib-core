package ms.airy.lib.core.popup.fragment;

import android.app.Fragment;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import ms.airy.lib.core.R;
import ms.airy.lib.core.popup.PopupManager;
import ms.airy.lib.core.popup.prefs.PopupPrefsUtil;

import java.util.Set;


// In the current implementation,
// Popup may be shown when a new activity is opened, etc.
// and, also when the device orientation changes, etc..  --> need to disable this...
public class BasePopupManagerFragment extends Fragment
{
    // temporary
    private static final long DEFAULT_DELAY_MILLIS = 0L;
    private static final int DEFAULT_REFRACTORY_SECONDS = 1800;  // half an hour.

    // String
    public static final String ARG_POPUP_CATEGORY = "popup_category";
    // int
    public static final String ARG_POPUP_TYPE = "popup_type";
    // long
    public static final String ARG_POPUP_DELAY = "popup_delay_millis";
    // int
    public static final String ARG_POPUP_REFRACTORY_SECS = "popup_refractory_seconds";
    // ...


    // TBD:
    public static BasePopupManagerFragment newInstance()
    {
        return newInstance(null);
    }
    public static BasePopupManagerFragment newInstance(String category)
    {
        return newInstance(category, 0);
    }
    public static BasePopupManagerFragment newInstance(String category, int popupType)
    {
        BasePopupManagerFragment fragment = new BasePopupManagerFragment();
        Bundle args = new Bundle();
        if(category != null && !category.isEmpty()) {
            args.putString(ARG_POPUP_CATEGORY, category);
        }
        if(popupType > 0) {
            args.putInt(ARG_POPUP_TYPE, popupType);
        }
        fragment.setArguments(args);
        return fragment;
    }
    public static BasePopupManagerFragment newInstance(String category, int popupType, long popupDelay)
    {
        BasePopupManagerFragment fragment = newInstance(category, popupType);
        addLongArgument(fragment, ARG_POPUP_DELAY, popupDelay);
        return fragment;
    }
    public static BasePopupManagerFragment newInstance(String category, int popupType, long popupDelay, int refractorySecs)
    {
        BasePopupManagerFragment fragment = newInstance(category, popupType, popupDelay);
        addIntArgument(fragment, ARG_POPUP_REFRACTORY_SECS, refractorySecs);
        return fragment;
    }

    protected static BasePopupManagerFragment addIntArgument(BasePopupManagerFragment fragment, String key, int val)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        args.putInt(key, val);
        return fragment;
    }
    protected static BasePopupManagerFragment addLongArgument(BasePopupManagerFragment fragment, String key, long val)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        args.putLong(key, val);
        return fragment;
    }
    protected static BasePopupManagerFragment addStringArgument(BasePopupManagerFragment fragment, String key, String val)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        args.putString(key, val);
        return fragment;
    }

    public BasePopupManagerFragment()
    {
    }


    // TBD:
    private String category = null;
    // private Set<String> categories;
    private int popupType = 0;
    private long popupDelay = 0L;
    private int refractorySecs = 0;
    // ...

    // TBD:
    // In the current implementation using onStart() to show popup,
    // popup may be displayed even when config changes, like when the device orientation changes..
    // We need to disable that..
    //   --> Unfortunately, this does not seem to work....
    private boolean popupDisabledForNow = false;
    // trying this...
    // set this to true on configChanged,
    // and set it back to false after a little while (e.g., 0.5 seconds)
//    private boolean popupTemporarilyPaused = false;  --> This does not work either...
    // private String fragmentTag = null;
    private boolean popupTemporarilyDisabled = false;   // to support refractory period.
    // ...

    // For delaying showing the popups...
    private DelayPopupHandler handler = null;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if(savedInstanceState == null) {
            Bundle args = getArguments();
            if (args != null) {
                popupType = args.getInt(ARG_POPUP_TYPE);
                category = args.getString(ARG_POPUP_CATEGORY);
                if (args.containsKey(ARG_POPUP_DELAY)) {
                    popupDelay = args.getLong(ARG_POPUP_DELAY);
                } else {
                    popupDelay = DEFAULT_DELAY_MILLIS;
                }
                if (args.containsKey(ARG_POPUP_REFRACTORY_SECS)) {
                    refractorySecs = args.getInt(ARG_POPUP_REFRACTORY_SECS);
                } else {
                    refractorySecs = DEFAULT_REFRACTORY_SECONDS;
                }
            }

            // first time.
            popupDisabledForNow = false;

            // No need to use separate thread.
            handler = new DelayPopupHandler(this);
        } else {
            // ????
            popupDisabledForNow = true;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        return null;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
    }


    @Override
    public void onStart()
    {
        super.onStart();

        // tbd
        setRetainInstance(true);
        // ...

        if(popupDisabledForNow == false) {
//            if(popupTemporarilyPaused == false) {
                // Check SharedPrefs db...
                String fragmentTag = getTag();  // ????
                if (Log.I) Log.i(">>>>>>>>>>> fragmentTag = " + fragmentTag);
                boolean isInRefractory = PopupPrefsUtil.isInRefractoryPeriod(getActivity(), fragmentTag, this.refractorySecs);
                popupTemporarilyDisabled = isInRefractory;

                if (popupTemporarilyDisabled == false) {
                    if (popupDelay == 0L) {
                        showPopup();   // This won't work unless PopupManager has already been initialized...
                    } else {
                        // This is to trigger initializing PopupRegistry.
                        // The whole point of delay-showing is to give time to load/process the popups.
                        boolean isReady = PopupManager.getInstance(getActivity()).isReady();
                        if (Log.I) Log.i("PopupManager is ready? " + isReady);
                        handler.sendEmptyMessageDelayed(DELAYED_POPUP, popupDelay);
                    }
                } else {
                    Log.d("popupTemporarilyDisabled == true.");
                    // TBD:
                    // Still call PopupManager method to initialize the popup registry????
                }
//            } else {
//                Log.d("popupTemporarilyDisabled == true.");
//                // One time use. Set it back to false.
//                popupTemporarilyDisabled = false;
//            }
        } else {
            Log.d("popupDisabledForNow == true.");
        }
//        // Actually, reset this regardless of other flags. ???
//        // popupTemporarilyDisabled = false;
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }

    @Override
    public void onPause()
    {
        super.onPause();
    }

    @Override
    public void onStop()
    {
        super.onStop();
    }

//    @Override
//    public void onConfigurationChanged(Configuration newConfig)
//    {
//        Log.i("BasePopupManagerFragment.onConfigurationChanged(). Setting popupTemporarilyPaused to true.");
//        super.onConfigurationChanged(newConfig);
//
//        // ???
//        popupTemporarilyPaused = true;
//    }


    private void showPopup()
    {
        if(popupDisabledForNow || popupTemporarilyDisabled) {
            return;
        }

        boolean isToBeShown = false;
        if(category != null && popupType > 0) {
            isToBeShown = PopupManager.getInstance(getActivity()).showPopup(getActivity(), category, popupType);
        } else if(category != null) {
            isToBeShown = PopupManager.getInstance(getActivity()).showPopup(getActivity(), category);
        } else if(popupType > 0) {
            isToBeShown = PopupManager.getInstance(getActivity()).showPopup(getActivity(), popupType);
        } else {
            // ????
            isToBeShown = PopupManager.getInstance(getActivity()).showPopup(getActivity());
        }
        if(isToBeShown) {
            String fragmentTag = getTag();
            PopupPrefsUtil.setLastPopupTime(getActivity(), fragmentTag);
        } else {
            Log.d("Popup will not be shown.");
        }
    }


    private static final int DELAYED_POPUP = 111;
    private static final class DelayPopupHandler extends Handler
    {
        private final BasePopupManagerFragment fragment;

        public DelayPopupHandler(BasePopupManagerFragment fragment)
        {
            super();
            this.fragment = fragment;
        }
        public DelayPopupHandler(Looper looper, BasePopupManagerFragment fragment)
        {
            super(looper);
            this.fragment = fragment;
        }

        @Override
        public void handleMessage(Message msg)
        {
            if(Log.I) Log.i("DelayPopupHandler.handleMessage() msg = " + msg);

            switch(msg.what) {
                case DELAYED_POPUP:
                    // Is it possible that this fragment (or, its parent activity)
                    // might have been paused/stopped, etc... when this is called????
                    fragment.showPopup();
                    break;
                default:
                    Log.i("Unrecognized message: what = " + msg.what);
            }
            super.handleMessage(msg);
        }
    }

}

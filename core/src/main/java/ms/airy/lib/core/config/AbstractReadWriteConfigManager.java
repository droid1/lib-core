package ms.airy.lib.core.config;

import android.content.Context;


/**
 */
public abstract class AbstractReadWriteConfigManager extends AbstractConfigManager
{
    protected AbstractReadWriteConfigManager(Context context)
    {
        super(context);
    }

    public abstract boolean set(String key, Object val);
    public abstract boolean setString(String key, String val);

    // TBD:
    // Different types?
    // int, boolean, float, etc... ???
    // ....

}

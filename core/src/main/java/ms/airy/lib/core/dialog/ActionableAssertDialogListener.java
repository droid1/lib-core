package ms.airy.lib.core.dialog;


/**
 * Generalization of ActionableAlertDialog/ActionableNoticeDialog with three buttons,
*/
public interface ActionableAssertDialogListener extends ActionableAlertDialogListener, ActionableNoticeDialogListener
{
//    void onPositiveClick(String tag);
//    void onNegativeClick(String tag);
//    void onNeutralClick(String tag);
}

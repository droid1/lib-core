package ms.airy.lib.core.notification.bootup;

import android.content.Context;


public final class NotificationBootUpHelper
{
    private static NotificationBootUpHelper INSTANCE = null;
    public static NotificationBootUpHelper getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new NotificationBootUpHelper(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;


    private NotificationBootUpHelper(Context context)
    {
        this.context = context;
    }


    // TBD:
    // ....


    public void prepareNotificationModule()
    {
        // TBD:
        // Recreate all notifications
        // and/or,
        // create a "special" notification inviting the user to open the app..
        // etc...
    }


}

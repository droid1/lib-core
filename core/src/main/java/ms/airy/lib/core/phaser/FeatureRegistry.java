package ms.airy.lib.core.phaser;

import ms.airy.lib.core.phaser.common.Feature;
import ms.airy.lib.core.phaser.common.Phase;
import ms.airy.lib.core.phaser.common.Rank;
import ms.airy.lib.core.phaser.feature.FeatureHelper;
import ms.airy.lib.core.phaser.phase.PhaseHelper;
import ms.airy.lib.core.phaser.rank.RankHelper;
import ms.airy.lib.core.phaser.state.StateHelper;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


// TBD:
// ...
// Note:
// We have this dual implementations
// (1) A client app can use a list of Feature objects:
//           A Feature contain all the necessary information
//           regarding disabled/enbaled, user state dependency, etc.
// (2) A client app use use a list of feature codes:
//           The feature's phaser information is stored in each attr's Helper singletons...
// .....
// This was not intentional. During the implementation, we changed the design...
// For now, only option (1) (FeatureManager) is implemented...
// ...
// Not implemented....
public final class FeatureRegistry
{
    // Set of FeatureCodes.
    private final Set<String> features;

    public FeatureRegistry()
    {
        features = new HashSet<String>();
    }


    // Singleton
    private static final class FeatureRegistryHolder
    {
        private static final FeatureRegistry INSTANCE = new FeatureRegistry();
    }
    public static FeatureRegistry getInstance()
    {
        return FeatureRegistryHolder.INSTANCE;
    }


    public Set<String> getFeatures()
    {
        return features;
    }


    // Add or update...

    public void updatePhasedFeature(String featureCode, boolean disabled)
    {
        if(disabled == false) {
            FeatureHelper.getInstance().enableFeature(featureCode);
        } else {
            FeatureHelper.getInstance().disableFeature(featureCode);
        }
    }
    public void updatePhasedFeature(String featureCode, Integer minPhase, Integer maxPhase)
    {
        // TBD:
    }
    public void updatePhasedFeatureForLevel(String featureCode, Integer minLevel, Integer maxLevel)
    {
        // TBD:
    }
    public void updatePhasedFeatureForUser(long state, long role, long badge)
    {
        // TBD:
    }
    public void updatePhasedFeatureForRank(String featureCode, Integer minRank, Integer maxRank)
    {
        // TBD:
    }



    // TBD:...
    public boolean isFeatureAvailableForUser(String featureCode, String userId)
    {
        // TBD:
        // Get feature from the feature map
        // get user object (or, the user's, state, role, badge, etc...)
        // then call Feature's convenience functions...
        // ....


        return true;
    }



    // TBD:
    // ......

//    public boolean isFeatureAvailableForAll(String featureCode)
//    {
//        return isFeatureAvailable(featureCode, Rank.MIN);
//    }

    // temporary
//    public boolean isFeatureAvailable(String featureCode, int rank)
//    {
//        return isFeatureAvailable(featureCode, rank, Phase.getCurrentPhase());
//    }
    public boolean isFeatureAvailable(String featureCode, int rank, int phase)
    {
        // ????
//        if(! Phase.isPhaseIncluded(phase)) {
//            return false;
//        }
        if(! PhaseHelper.getInstance().isFeatureAvailable(featureCode, phase)) {
            return false;
        }
        if(! RankHelper.getInstance().isFeatureAvailable(featureCode, rank)) {
            return false;
        }
        return true;
    }


    // Features dependency on "conditions" or state ???
    // E.g., "multiplayer" game feature is available only for the user currently signed in, etc....
    // ...

//    public boolean isFeatureAvailableForAll(String featureCode, long state)
//    {
//        return isFeatureAvailable(featureCode, Rank.MIN, state);
//    }

    // temporary
//    public boolean isFeatureAvailable(String featureCode, int rank, long state)
//    {
//        return isFeatureAvailable(featureCode, rank, state, Phase.getCurrentPhase());
//    }
    public boolean isFeatureAvailable(String featureCode, int rank, long state, int phase)
    {
        // ???
//        if(! Phase.isPhaseIncluded(phase)) {
//            return false;
//        }
        if(! PhaseHelper.getInstance().isFeatureAvailable(featureCode, phase)) {
            return false;
        }
        if(! RankHelper.getInstance().isFeatureAvailable(featureCode, rank)) {
            return false;
        }
        if(! StateHelper.getInstance().isFeatureAvailable(featureCode, state)) {
            return false;
        }
        return true;
    }


}

package ms.airy.lib.core.location.common;


// Note that this frequency defines the overall tracking
//    as used by the Tracking HeartBeat.
// actual tracking may happen more frequently.
// ...
// Note that there are two factors in controlling how often location is tracked
//        (because the location info is collected "in bursts").
// (1) Frequency: say, every 15 minutes.
// (2) Duration: Once location tracking "wakes up",
//     it waits for certain duration to collect the location data, and then stops.
// ....
// "Foreground" does not mean it is done necessarily in the foreground.
//     It is done in the "active" manner
//          (e.g., because the activity is currently running in the foreground, etc.).
//     The actual data collecting may happen in a separate thread (other than the UI thread).
// "Background", on the other hand, means
//     the data collection is done in the "passive" manner
//          (e.g., while the app/activity is not running in the foreground).
//     It never runs in the main thread.
// ....
// "Foreground" tracking starts by the app and it runs
//     either as long as it is needed, or for the default duration, as decided by the app.
//     Foreground tracking may be repeated through handler, or it may continue running.
// "Background" tracking starts through Alarm Manager, and
//     it runs for the specified duration.
public final class TrackingFrequency
{
    public static final int NONE = 0;   // ??? No tracking?
    // The exemplary numbers on the right are
    //    for "foreground/active" update and "background/passive" updates, respectively.
    // Note that the foreground tracking may be
    //    continuously running until explicitly stopped rather than being done in intervals.
    // ...
    // Note: These intervals are chosen so that they can be easily expressed in cron format...
    public static final int SLOWEST = (0x1 << 0);   // every ~1 hour    4 hours
    public static final int SLOW = (0x1 << 1);      // every ~30 mins   2 hours
    public static final int NORMAL = (0x1 << 2);    // every ~15 mins   1 hour
    public static final int FAST = (0x1 << 3);      // every ~10 mins   30 mins
    public static final int FASTEST = (0x1 << 4);   // every ~5 mins    15 mins
    // ...

    private TrackingFrequency() {}

    // temporary
    public static int getDefaultFrequency()
    {
        return NORMAL;
    }



    // Note that the cron expressions and the update duration/min update millis
    //    should be "consistent" with each other.
    // tbd:
    //    is there a better way????
    // ???



    // temporary
    // Note that the "foreground" version is not being used.
    public static String getUpdateCronExpression(int frequency)
    {
        return getUpdateCronExpression(frequency, false);  // false
    }
    public static String getUpdateCronExpression(int frequency, boolean runInForeground)
    {
        if(runInForeground) {
            return getUpdateCronExpressionForForegroundUpdates(frequency);
        } else {
            return getUpdateCronExpressionForBackgroundUpdates(frequency);
        }
    }
    private static String getUpdateCronExpressionForForegroundUpdates(int frequency)
    {
        switch(frequency) {
            case SLOWEST:
            default:
                return "0 * * * *";
            case SLOW:
                return "*/30 * * * *";
            case NORMAL:
                return "*/15 * * * *";
            case FAST:
                return "*/10 * * * *";
            case FASTEST:
                return "*/5 * * * *";
        }
    }
    public static String getUpdateCronExpressionForBackgroundUpdates(int frequency)
    {
        switch(frequency) {
            case SLOWEST:
            default:
                return "0 */4 * * *";
            case SLOW:
                return "0 */2 * * *";
            case NORMAL:
                return "0 * * * *";
            case FAST:
                return "*/30 * * * *";
            case FASTEST:
                return "*/15 * * * *";
        }
    }


    // temporary
    // Note that the "background" version is not being used.
    public static long getPollingInterval(int frequency)
    {
        return getPollingInterval(frequency, true);   // true?
    }
    public static long getPollingInterval(int frequency, boolean runInForeground)
    {
        if(runInForeground) {
            return getPollingIntervalForForegroundUpdates(frequency);
        } else {
            return getPollingIntervalForBackgroundUpdates(frequency);
        }
    }
    public static long getPollingIntervalForForegroundUpdates(int frequency)
    {
        switch(frequency) {
            case SLOWEST:
            default:
                return 60 * 60 * 1000L;
            case SLOW:
                return 30 * 60 * 1000L;
            case NORMAL:
                return 15 * 60 * 1000L;
            case FAST:
                return 10 * 60 * 1000L;
            case FASTEST:
                return 5 * 60 * 1000L;
        }
    }
    private static long getPollingIntervalForBackgroundUpdates(int frequency)
    {
        switch(frequency) {
            case SLOWEST:
            default:
                return 4 * 60 * 60 * 1000L;
            case SLOW:
                return 2 * 60 * 60 * 1000L;
            case NORMAL:
                return 60 * 60 * 1000L;
            case FAST:
                return 30 * 60 * 1000L;
            case FASTEST:
                return 15 * 60 * 1000L;
        }
    }


    // temporary
//    public static long getUpdateDuration(int frequency)
//    {
//        return getUpdateDuration(frequency, false);
//    }
    public static long getUpdateDuration(int frequency, boolean runInForeground)
    {
        if(runInForeground) {
            return getUpdateDurationForForegroundUpdates(frequency);
        } else {
            return getUpdateDurationForBackgroundUpdates(frequency);
        }
    }
    public static long getUpdateDurationForForegroundUpdates(int frequency)
    {
        switch(frequency) {
            case SLOWEST:
            default:
                return 24 * 60 * 1000L;
            case SLOW:
                return 12 * 60 * 1000L;
            case NORMAL:
                return 6 * 60 * 1000L;
            case FAST:
                return 4 * 60 * 1000L;
            case FASTEST:
                return 2 * 60 * 1000L;
        }
    }
    public static long getUpdateDurationForBackgroundUpdates(int frequency)
    {
        switch(frequency) {
            case SLOWEST:
            default:
                return 48 * 60 * 1000L;
            case SLOW:
                return 24 * 60 * 1000L;
            case NORMAL:
                return 12 * 60 * 1000L;
            case FAST:
                return 8 * 60 * 1000L;
            case FASTEST:
                return 4 * 60 * 1000L;
        }
    }


    // temporary
//    public static long getMinUpdateMillis(int frequency)
//    {
//        return getMinUpdateMillis(frequency, false);
//    }
    public static long getMinUpdateMillis(int frequency, boolean runInForeground)
    {
        if(runInForeground) {
            return getMinUpdateMillisForForegroundUpdates(frequency);
        } else {
            return getMinUpdateMillisForBackgroundUpdates(frequency);
        }
    }
    public static long getMinUpdateMillisForForegroundUpdates(int frequency)
    {
        switch(frequency) {
            case SLOWEST:
            default:
                return 6 * 60 * 1000L;
            case SLOW:
                return 3 * 60 * 1000L;
            case NORMAL:
                return 2 * 60 * 1000L;
            case FAST:
                return 1 * 60 * 1000L;
            case FASTEST:
                return 30 * 1000L;
        }
    }
    public static long getMinUpdateMillisForBackgroundUpdates(int frequency)
    {
        switch(frequency) {
            case SLOWEST:
            default:
                return 12 * 60 * 1000L;
            case SLOW:
                return 6 * 60 * 1000L;
            case NORMAL:
                return 3 * 60 * 1000L;
            case FAST:
                return 2 * 60 * 1000L;
            case FASTEST:
                return 1 * 60 * 1000L;
        }
    }


}

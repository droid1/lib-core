package ms.airy.lib.core.navigation.swipetab;

import android.app.Activity;
import android.app.ActionBar;
import android.support.v13.app.FragmentPagerAdapter;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

import java.util.List;

import ms.airy.lib.core.R;


public class SwipeTabMainActivity extends Activity
{
    SwipeTabSectionsPagerAdapter mSwipeTabSectionsPagerAdapter;

    /**
     * The {@link android.support.v4.view.ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sample_activity_swipe_tab_main);

        // Set up the action bar.
        final ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSwipeTabSectionsPagerAdapter = new SwipeTabSectionsPagerAdapter(getFragmentManager(), this);
        // temporary
        List<SwipeTabPageInfo> pageInfos = new SampleSwipeTabPageRegistry().getPageInfos();
        mSwipeTabSectionsPagerAdapter.setPageInfos(pageInfos);
        // temporary


        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSwipeTabSectionsPagerAdapter);

        // When swiping between different sections, select the corresponding
        // tab. We can also use ActionBar.Tab#select() to do this if we have
        // a reference to the Tab.
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                actionBar.setSelectedNavigationItem(position);
            }
        });

        // For each of the sections in the app, add a tab to the action bar.
        for (int i = 0; i < mSwipeTabSectionsPagerAdapter.getCount(); i++) {
            // Create a tab with text corresponding to the page title defined by
            // the adapter. Also specify this Activity object, which implements
            // the TabListener interface, as the callback (listener) for when
            // this tab is selected.

//            ActionBarSwipeTabListener actionBarSwipeTabListener = new ActionBarSwipeTabListener(this, "tag-" + i, SwipeTabMainFragment.class);
            ActionBarSwipeTabListener actionBarSwipeTabListener = new ActionBarSwipeTabListener(mViewPager);

            actionBar.addTab(
                    actionBar.newTab()
                            .setText(mSwipeTabSectionsPagerAdapter.getPageTitle(i))
                            .setTabListener(actionBarSwipeTabListener));
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.sample_swipe_tab_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}

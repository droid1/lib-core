package ms.airy.lib.core.help.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import ms.airy.lib.core.R;
import ms.airy.lib.core.help.common.HelpConsts;
import ms.airy.lib.core.help.common.QuestionInfo;
import ms.airy.lib.core.help.helper.QuestionInfoRegistry;
import ms.airy.lib.core.help.view.QuestionWebViewClient;


// QuestionInfo "detail" page.
// TBD: Use WebViewFragment ????
public class QuestionAndAnswerFragment extends Fragment
{
    public static QuestionAndAnswerFragment newInstance(long questionId)
    {
        QuestionAndAnswerFragment fragment = new QuestionAndAnswerFragment();
        Bundle args = new Bundle();
        if(questionId > 0L) {
            args.putLong(HelpConsts.KEY_EXTRA_QUESTION_ID, questionId);
        }
        fragment.setArguments(args);
        return fragment;
    }

    private WebView myWebView = null;
    private long questionId = 0L;

    public QuestionAndAnswerFragment()
    {
    }

    public final WebView getWebView()
    {
        return myWebView;
    }
    public boolean canGoBack()
    {
        if(myWebView != null) {
            return myWebView.canGoBack();
        }
        return false;
    }
    public void goBack()
    {
        if(myWebView != null) {
            myWebView.goBack();
        } else {
            // ignore
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

//        // Enable the menu
//        setHasOptionsMenu(true);
////        // tbd
////        setRetainInstance(true);

        Bundle args = getArguments();
        if (args != null) {
            this.questionId = args.getLong(HelpConsts.KEY_EXTRA_QUESTION_ID, 0L);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_question_and_answer, container, false);
        myWebView = (WebView) rootView.findViewById(R.id.webbview_question_and_answer);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        if(myWebView != null) {
            myWebView.setWebViewClient(new QuestionWebViewClient(getActivity()));

//            String testContent = "Internal Url: <a href=\"question:///1234\">Question 2</a><br>"
//                    + "External Url: <a href=\"http://www.google.com/\">Google</a>";
//            myWebView.loadData(testContent, "text/html", "utf-8");

            String content = "<center><h2>Question Not Found</h2></center>";

            if(questionId > 0L) {
                QuestionInfo questionInfo = QuestionInfoRegistry.getInstance(getActivity()).getQuestionInfo(questionId);
                if(questionInfo != null) {
                    String question = questionInfo.getQuestion(getActivity());
                    if(question != null && !question.isEmpty()) {
                        content = "<H3>" + question + "</H3>";
                        String answer = questionInfo.getAnswer(getActivity());
                        if(answer != null && !answer.isEmpty()) {
                            content += "<p>" + answer + "</p>";
                        }
                    }
                }
            }
            myWebView.loadData(content, "text/html", "utf-8");
        }

    }



}

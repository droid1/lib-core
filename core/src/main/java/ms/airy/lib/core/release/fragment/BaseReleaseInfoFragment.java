package ms.airy.lib.core.release.fragment;

import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.Date;

import ms.airy.lib.core.dialog.stock.SimpleConfirmDialogFragment;
import ms.airy.lib.core.release.BaseReleaseInfoManager;
import ms.airy.lib.core.release.PlannedReleaseInfo;
import ms.airy.lib.core.util.DateRangeUtil;
import ms.airy.lib.core.util.DateStringUtil;
import ms.airy.lib.core.util.DateUtil;


// TBD:
// The sole purpose of this fragment is 
// to add a menu item to open the next release info,
// and possibly to show a popup, if appropriate...
// ...
public class BaseReleaseInfoFragment extends Fragment
{
    // TBD:

    public BaseReleaseInfoFragment()
    {
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // Enable the menu
        setHasOptionsMenu(true);

        // tbd
        setRetainInstance(true);
        // ...
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        return null;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

    }


    @Override
    public void onStart()
    {
        super.onStart();
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }

    @Override
    public void onPause()
    {
        super.onPause();
    }

    @Override
    public void onStop()
    {
        super.onStop();
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
    }



    // temporary
    private static final int MENU_ITEM_SHOW_NEXT_RELEASE_INFO = 237141;



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        // TBD:
        // Add the menu item only if the next release is found...
        PlannedReleaseInfo releaseInfo = BaseReleaseInfoManager.getInstance(getActivity()).getNextPlannedRelease();
        if(releaseInfo != null) {
            // temporary
            MenuItem item1 = menu.add(Menu.NONE, MENU_ITEM_SHOW_NEXT_RELEASE_INFO, Menu.NONE, "Next Release");
            // item1.setIcon(android.R.drawable.ic_menu_help);
            // ...
        }
        // ....

        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId()) {
            case MENU_ITEM_SHOW_NEXT_RELEASE_INFO:
                showNextReleaseInfoDialog();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showNextReleaseInfoDialog()
    {
        PlannedReleaseInfo releaseInfo = BaseReleaseInfoManager.getInstance(getActivity()).getNextPlannedRelease();
        if(releaseInfo != null) {
            String releaseTitle = releaseInfo.getReleaseTitle();
            String releaseMessage = releaseInfo.getReleaseMessage();

            // TBD:
            // Note that the date user sees may be different from the releaseDay I specified...
            //      Could this be a problem????
            // Also, we should really use date format based on the user's locale.
            //      not hard-coded format in DateUtil....
            String releaseDay = releaseInfo.getReleaseDay();
            long relTime = DateStringUtil.parseDate(releaseDay, "US/Pacific");   // In my timezone
            String relDate = DateUtil.formatDateNoHours(relTime);   // In user's timezone...
            // ....

            String title = "Next Release.";
            if(releaseTitle != null) {
                title = releaseTitle;
            } else {
                // Use releaseName ???
            }
            String message = "Release date: " + relDate + "\n";
            if(releaseMessage != null) {
                message += releaseMessage;
            }

            // TBD:
            // Use timer dialog???
            //  (Note: timer dialog is not very effective when the time range is long....
            //    like days instead of hours or minutes....)
            // Or, use WebView-based activity instead of dialog???
            // Fetch the release info from the cloud ????
            // ....

            DialogFragment dialog = SimpleConfirmDialogFragment.newInstance(title, message);
            dialog.show(getActivity().getFragmentManager(), "next_release_info");
        } else {
            // this cannot happen since this method can only be invoked from the menu...
            // ....
        }
    }


}

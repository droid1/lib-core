package ms.airy.lib.core.memo.util;


/**
 */
public final class MemoLoaderUtil
{
    private MemoLoaderUtil() {}


    public static boolean isIncludingThisMemo(String fileName)
    {
        if(fileName == null || !fileName.endsWith(".json")
                || fileName.endsWith("-old.json")
                || fileName.endsWith("-template.json")) {
            return false;
        } else {
            return true;
        }
    }


    // Not being used.
    // To be deleted.....
    // Memo file name: memo-011.json  --> Memo id: 11.
    // This function also acts as a filter.
    // Files with particular patterns (e.g., xxx-old.json) are filtered out.
    private static long getMemoIdFromFileName(String fileName)
    {
        if(fileName == null || !fileName.endsWith(".json")
                || fileName.endsWith("-old.json")
                || fileName.endsWith("-template.json")) {
            return 0L;
        }
        int i1 = fileName.lastIndexOf(".");
        if(i1 == -1) {
            i1 = fileName.length();  // No suffix/extension.
        }
        int i2 = fileName.lastIndexOf("-");
        if(i2 == -1) {
            return 0L;   // ???
        }
        String idStr = fileName.substring(i2+1, i1);
        long memoId = 0L;
        try {
            memoId = Long.parseLong(idStr);
        } catch(NumberFormatException e) {
            // ignore
            Log.w("Invalid memo Id: " + idStr, e);
        }
        return memoId;
    }


}

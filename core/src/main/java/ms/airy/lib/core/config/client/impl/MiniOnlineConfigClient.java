package ms.airy.lib.core.config.client.impl;

import org.miniclient.ApiUserClient;
import org.miniclient.RestApiException;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ms.airy.lib.core.config.client.OnlineConfigClient;


/**
 */
public class MiniOnlineConfigClient extends AbstractOnlineConfigClient implements OnlineConfigClient
{
    private ApiUserClient miniConfigWebClient = null;

    public MiniOnlineConfigClient()
    {
        this((String) null);
    }
    public MiniOnlineConfigClient(String configWebServiceEndpoint)
    {
        super(configWebServiceEndpoint);
        this.miniConfigWebClient = MiniClientBuilder.getInstance().getConfigWebClient(configWebServiceEndpoint);
    }
    public MiniOnlineConfigClient(ApiUserClient miniConfigWebClient)
    {
        super(miniConfigWebClient.getResourceBaseUrl());
        this.miniConfigWebClient = miniConfigWebClient;
    }

    public ApiUserClient getMiniConfigWebClient()
    {
        return miniConfigWebClient;
    }
    protected void setMiniConfigWebClient(String configWebServiceEndpoint)
    {
        super.setConfigWebServiceEndpoint(configWebServiceEndpoint);
        this.miniConfigWebClient = MiniClientBuilder.getInstance().getConfigWebClient(configWebServiceEndpoint);
    }
    protected void setMiniConfigWebClient(ApiUserClient miniConfigWebClient)
    {
        super.setConfigWebServiceEndpoint(miniConfigWebClient.getResourceBaseUrl());
        this.miniConfigWebClient = miniConfigWebClient;
    }

    @Override
    protected void setConfigWebServiceEndpoint(String configWebServiceEndpoint)
    {
        super.setConfigWebServiceEndpoint(configWebServiceEndpoint);
        this.miniConfigWebClient = MiniClientBuilder.getInstance().getConfigWebClient(configWebServiceEndpoint);
    }




    // TBD:
    // Add user's "id" to all requests
    // e.g., appId, deviceId, user's accountName (if the user is "logged on"), etc..
    // ...


    @Override
    public Object getProperty(String key)
    {
        if(Log.I) Log.i("MiniOnlineConfigClient.getProperty() key = " + key);

        // ???
        Object value = null;
        try {
            // TBD:
            Map<String,Object> params = new HashMap<String, Object>();
            // params.put("appId", appId);  // ???
            // params.put("deviceId", deviceId);  // ???
            // params.put("userAccountName", accountName);  // ???
            params.put("key", key);  // ???
            List<Object> list =  miniConfigWebClient.list(params);
            if(list != null && !list.isEmpty()) {
                // ???
                value = list.get(0);
            }
        } catch (RestApiException e) {
            Log.e("Failed to get send config for key = " + key, e);
        } catch (IOException e) {
            Log.e("IO Exception while sending config for key = " + key, e);
        }

        return value;
    }

    @Override
    public boolean setProperty(String key, Object value)
    {
        if(Log.I) Log.i("MiniOnlineConfigClient.setProperty() key = " + key + "; value = " + value);

        // ???
        boolean result = false;
        try {
            // TBD:
            Map<String,Object> params = new HashMap<String, Object>();
            // params.put("appId", appId);  // ???
            // params.put("deviceId", deviceId);  // ???
            // params.put("userAccountName", accountName);  // ???
            params.put("key", key);  // ???
            params.put("value", value);  // ???
            Object obj = miniConfigWebClient.create(params);
            // ???
            if(obj != null) {
                result = true;
            }
        } catch (RestApiException e) {
            Log.e("Failed to get set config for key = " + key, e);
        } catch (IOException e) {
            Log.e("IO Exception while setting config for key = " + key, e);
        }

        return result;
    }

}

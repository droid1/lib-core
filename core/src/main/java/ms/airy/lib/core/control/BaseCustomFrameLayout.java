package ms.airy.lib.core.control;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ms.airy.lib.core.control.common.ControlSize;


// TBD:
// Support "overlays" ???
// ...

// This is (almost) the same as BaseCustomFrameLayout.
// Since we cannot use multiple inheritance, we have one base class for each common layout...
public abstract class BaseCustomFrameLayout extends FrameLayout
{
    // dirty flag.
    // To be used in update().
    private boolean dirty;

    // All custom controls may be displayed differently based on its "size".
    private String controlSize;

    // Cache the device orientation. landscape or portrait.
    private /* volatile */ int orientation;


    // Overlays ???
    // "z-order" -> view ???
    // Lower z-order views are drawn first.
    // Views with the same z-values are ordered as well...
    // Note that minus z values are allowed,
    //   and the views with z <0 are drawn BEFORE the main view.
    // when z is not specified, the default value z==1 is used.
    private final Map<Integer, List<View>> overlayMap = new HashMap<Integer, List<View>>();
    // ....


    public BaseCustomFrameLayout(Context context)
    {
        super(context);
        initControl();
    }
    public BaseCustomFrameLayout(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        initControl();
    }
    public BaseCustomFrameLayout(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        initControl();
    }


    // Note that it's not a good idea to call virtual/non-final method in ctor....
    private void initControl()
    {
        dirty = false;
        controlSize = ControlSize.SIZE_MEDIUM;  // ???



        // temporary
        this.setWillNotDraw(false);
        // temporary



    }


    // TBD:
    // Is it safe to override View.isDirty()?????
    @Override
    public boolean isDirty()
    {
        return dirty;
    }
    protected void setDirty()
    {
        setDirty(true);
    }
    protected void setDirty(boolean dirty)
    {
        this.dirty = dirty;
    }

    public String getControlSize()
    {
        return controlSize;
    }
    public void setControlSize(String controlSize)
    {
        // TBD: Validate ???
        this.controlSize = controlSize;
    }


    public int getOrientation()
    {
        return orientation;
    }
    public void setOrientation(int orientation)
    {
        this.orientation = orientation;
    }
    protected void resetOrientation()
    {
        Configuration c = getResources().getConfiguration();
        //Log.d("cOrientation = " + c.orientation);

        this.orientation = c.orientation;
//        if(c.orientation == Configuration.ORIENTATION_PORTRAIT ) {
//            // TBD
//            orientation = Configuration.ORIENTATION_PORTRAIT;
//        } else if(c.orientation == Configuration.ORIENTATION_LANDSCAPE ){
//            // TBD
//            orientation = Configuration.ORIENTATION_LANDSCAPE;
//        } else {
//            // What to do??? Can this happen?
//            //orientation = Configuration.ORIENTATION_PORTRAIT; // ???
//        }

        // ...

    }



    // tbd
    // add, remove, reorder overlays, etc..
    // ....




    /**
     * Primarily, to call invalidate().
     * This should be overridden in subclasses.
     * invalidate may need to be called under certain conditions, etc..
     *
     * @param force If true, forceInvalidate.
     * @param layout If true, call requestLayout(), and forceInvalidate.
     */
    // public abstract void update(boolean force, boolean layout);
    public void update(boolean force, boolean layout)
    {
        if(force || layout || isDirty() ) {   // layout==true implies force==true.
            if(layout) {
                requestLayout();
            }
            invalidate();
            setDirty(false);  // ???
        } else {
            // ignore
        }
    }
    public void update(boolean force)
    {
        update(force, false);
    }
    public void update()
    {
        update(false);
    }



    @Override
    protected Parcelable onSaveInstanceState()
    {
        Log.d("onSaveInstanceState() Called.");
        Parcelable superState = super.onSaveInstanceState();
        SavedState savedState = new SavedState(superState);
        savedState.setDirty(dirty);
        savedState.setControlSize(controlSize);
        return savedState;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state)
    {
        Log.d("onRestoreInstanceState() Called.");
        SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(savedState.getSuperState());
        setDirty(savedState.isDirty());
        setControlSize(savedState.getControlSize());
    }


    protected static class SavedState extends BaseSavedState implements Parcelable
    {
        private boolean dirty;
        private String controlSize;
        // etc...

        public SavedState(Parcelable superState)
        {
            super(superState);
        }
        public SavedState(Parcel source)
        {
            super(source);
            dirty = (source.readInt() != 0);
            controlSize = source.readString();
        }

        public boolean isDirty()
        {
            return dirty;
        }
        public void setDirty(boolean dirty)
        {
            this.dirty = dirty;
        }

        public String getControlSize()
        {
            return controlSize;
        }
        public void setControlSize(String controlSize)
        {
            this.controlSize = controlSize;
        }

        public static final Creator<SavedState> CREATOR = new Creator<SavedState>() {
            @Override
            public SavedState createFromParcel(Parcel source)
            {
                return new SavedState(source);
            }
            @Override
            public SavedState[] newArray(int size)
            {
                return new SavedState[size];
            }
        };

        @Override
        public void writeToParcel(Parcel dest, int flags)
        {
            super.writeToParcel(dest, flags);
            dest.writeInt(dirty ? 1 : 0);
            dest.writeString(controlSize);
        }

        // For debugging...
        @Override
        public String toString()
        {
            return "SavedState{" +
                    "dirty=" + dirty +
                    ", controlSize=" + controlSize +
                    '}';
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }


    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom)
    {
        if(Log.D) Log.d("onLayout() Called: changed = " + changed + "; left = " + left + "; top = " + top + "; right = " + right + "; bottom = " + bottom);
        // ???
        super.onLayout(changed, left, top, right, bottom);
        // ...
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh)
    {
        if(Log.D) Log.d("onSizeChanged() Called: w = " + w + "; h = " + h + "; oldw = " + oldw + "; oldh = " + oldh);
        super.onSizeChanged(w, h, oldw, oldh);
    }


}

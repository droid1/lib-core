package ms.airy.lib.core.location.common;


// TBD:
public final class LocationTrackerConsts
{
    // temporary
    public static final String ACTION_SCHEDULED_LOCATION_TRACKING = "airy.action.SCHEDULED_LOCATION_TRACKING";
    public static final String ACTION_SCHEDULED_LOCATION_PROCESSING = "airy.action.SCHEDULED_LOCATION_PROCESSING";
    // ....

    // TBD:
//    public static final String KEY_EXTRA_LOCATION_TAG = "airy.extra.key.location_tag";
//    public static final String KEY_EXTRA_LOCATION_ID = "airy.extra.key.location_id";
//    public static final String KEY_EXTRA_LOCATION = "airy.extra.key.location";
    // ...



    private LocationTrackerConsts() {}
}

package ms.airy.lib.core.web;

import java.net.URI;
import java.net.URISyntaxException;

import android.net.Uri;

public class UriUtil
{

    private UriUtil() {}


    // ???
    public static URI convertUri(Uri uri)
    {
        try {
            return new URI(uri.toString());
        } catch (URISyntaxException e) {
            // ignore
            if(Log.I) Log.i("Uri parse error.", e);
        }
        return null;
    }
    
    public static Uri convertURI(URI uri)
    {
        try {
            return Uri.parse(uri.toString());
        } catch (NullPointerException e) {
            // ignore
            if(Log.I) Log.i("URI parse error.", e);
        }
        return null;
    }
    
}

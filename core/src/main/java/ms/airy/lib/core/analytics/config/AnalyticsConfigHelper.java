package ms.airy.lib.core.analytics.config;

import android.content.Context;

import ms.airy.lib.core.config.ConfigMaster;


public class AnalyticsConfigHelper
{
    private static final String KEY_ANALYTICS_ENABLED = "airy.lib.core.analytics.analytics_enabled";
    // private static final String KEY_ANALYTICS_SERVICE = "airy.lib.core.analytics.analytics_service";
    private static final String KEY_AMAZON_ENABLED = "airy.lib.core.analytics.amazon_enabled";
    private static final String KEY_GOOGLE_ENABLED = "airy.lib.core.analytics.google_enabled";


    // "Cache"
    // Global flag.
    private Boolean analyticsEnabled = null;
//    // "google" or "amazon" for now.
//    private String analyticsService = null;
    // We can potentially support one or more analytics services.
    private Boolean amazonEnabled = null;
    private Boolean googleEnabled = null;


    private static AnalyticsConfigHelper INSTANCE = null;
    public static AnalyticsConfigHelper getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new AnalyticsConfigHelper(context!= null ? context.getApplicationContext() : null);  // ???
        } else {
            if(INSTANCE.getContext() == null && context != null) {  // ???
                INSTANCE = new AnalyticsConfigHelper(context.getApplicationContext());
            }
        }
        return INSTANCE;
    }

    private final Context context;
    private final ConfigMaster configMaster;
    private AnalyticsConfigHelper(Context context)
    {
        this.context = context;
        this.configMaster = ConfigMaster.getInstance(context);
        if(context != null) {   // Can context be null???
            // this.configMaster = ConfigMaster.getInstance(context);
            // TBD:
            // Read config from res ???
            // --> We use lazy initialization.
            // ...
        }
    }
    protected final Context getContext()
    {
        return context;
    }



    ///////////////////////////////////////////////////////////
    // temporary


    public boolean isAnalyticsEnabled()
    {
        if(analyticsEnabled == null) {
            analyticsEnabled = configMaster.getBooleanProperty(KEY_ANALYTICS_ENABLED);
            if(analyticsEnabled == null) {
                analyticsEnabled = false;    // ???
            }
        }
//        if(analyticsEnabled == null) {
//            return false;     // ???
//        }
        return analyticsEnabled;
    }

//    public String getAnalyticsService()
//    {
//        if(analyticsService == null) {
//            analyticsService = configMaster.getStringProperty(KEY_ANALYTICS_SERVICE);
//        }
//        if(analyticsService == null) {
//            analyticsService = AnalyticsService.getDefaultService();
//        }
//        return analyticsService;
//    }

    public boolean isAmazonAnalyticsEnabled()
    {
        if(isAnalyticsEnabled() == false) {
            return false;
        }
        if(amazonEnabled == null) {
            amazonEnabled = configMaster.getBooleanProperty(KEY_AMAZON_ENABLED);
            if(amazonEnabled == null) {
                amazonEnabled = false;    // ???
            }
        }
//        if(amazonEnabled == null) {
//            return false;     // ???
//        }
        return amazonEnabled;
    }

    public boolean isGoogleAnalyticsEnabled()
    {
        if(isAnalyticsEnabled() == false) {
            return false;
        }
        if(googleEnabled == null) {
            googleEnabled = configMaster.getBooleanProperty(KEY_GOOGLE_ENABLED);
            if(googleEnabled == null) {
                googleEnabled = false;    // ???
            }
        }
//        if(googleEnabled == null) {
//            return false;     // ???
//        }
        return googleEnabled;
    }

}

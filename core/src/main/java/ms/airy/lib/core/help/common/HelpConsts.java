package ms.airy.lib.core.help.common;


public class HelpConsts
{
    // TBD:
    // question-answer url is somethin like this:
    //    question:///1234  (Note three slashes)
    // where 1234 is the question id.
    public static final String SCHEME_QUESTION = "question";
    // ...

    // TBD:
    // generic arg of a string type.
    //   (Semantics to be determined based on the Action value..)
    public static final String KEY_EXTRA_ARG = "ms.airy.lib.core.help.EXTRA_ARG";
    // To pass the question id.
    public static final String KEY_EXTRA_QUESTION_ID = "ms.airy.lib.core.help.EXTRA_QUESTION_ID";
    // public static final String KEY_EXTRA_QUESTION_UUID = "ms.airy.lib.core.help.EXTRA_QUESTION_UUID";
    // etc...

}

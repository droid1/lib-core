package ms.airy.lib.core.help.manager;

import android.content.Context;

import ms.airy.lib.core.help.common.HelpSection;
import ms.airy.lib.core.help.common.TimestampSectionPair;
import ms.airy.lib.core.help.content.ContentLoaderCallback;
import ms.airy.lib.core.help.content.LocalContentLoader;
import ms.airy.lib.core.help.content.LocalLoaderUtil;
import ms.airy.lib.core.help.content.OnlineContentLoader;
import ms.airy.lib.core.help.content.OnlineLoaderUtil;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


public final class HelpRegistry implements ContentLoaderCallback
{
    // "Life time" of the cache.
    // Note that the app will be (frequently) unloaded and loaded by Android framework.
    // There is no point of having a long TTL...
    private static final long DEFAULT_TTL = 1 * 24 * 60 * 60 * 1000L;   // 1 day by default.
    private static HelpRegistry INSTANCE = null;

    public static HelpRegistry getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new HelpRegistry(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Aplication context
    private final Context context;

    // Remote help service URL
    private URL helpServiceURL = null;
    // Local help file path
    private File helpFile = null;


    // "Cache"
    // Map of id -> {HelpSection, expirationTime}.
    private final Map<Long, TimestampSectionPair> registry = new HashMap<Long, TimestampSectionPair>();

    private HelpRegistry(Context context)
    {
        this.context = context;

        // ???
        long[] ids = LocalContentLoader.getInstance(context).getSectionIds();
        OnlineContentLoader.getInstance(context).prefetchOnlineContent(ids);
        // ???
    }


    // Returns the map currently "cached".
    public Map<Long, TimestampSectionPair> getCurrentRegistry()
    {
        return registry;
    }

    public Set<Long> getIdSetFromCurrentRegistry()
    {
        Set<Long> set = registry.keySet();
        return set;
    }

    public HelpSection getSection(long id)
    {
        return getSection(id, null);
    }
    public HelpSection getSection(long id, String title)
    {
        long now = System.currentTimeMillis();
        HelpSection section = null;

        // [1] "cache"
        // if(registry.containsKey(id)) ...
        TimestampSectionPair pair = registry.get(id);
        if(pair != null) {
            section = pair.getHelpSection();
            if(section != null) {
                Long expTime = pair.getExpirationTime();
                if (expTime == null || expTime > now) {
                    return section;
                }
            }
        }


        // [2] Online content
        // TBD:
        OnlineContentLoader onlineContentLoader = OnlineContentLoader.getInstance(context);
        onlineContentLoader.addContentLoaderCallback(this);
        String content = onlineContentLoader.getContent(id);
        if(content != null) {
            // temporary
            if(title == null) {
                title = OnlineLoaderUtil.getTitleFromHtml(content);
            }
            section = new HelpSection(title, content);
        } else {
            // Async call has been made...
        }


        // [3] Local content
        if(section == null) {
            content = LocalContentLoader.getInstance(context).getContent(id);
            if(content != null) {
                // temporary
                if(title == null) {
                    title = LocalLoaderUtil.getTitleFromHtml(content);
                }
                section = new HelpSection(title, content);
            }
        }


        // [4] Store the new section in the registry
        if(section != null) {
            long newExpTime = now + DEFAULT_TTL;
            pair = new TimestampSectionPair(section, newExpTime);
            registry.put(id, pair);
        }

        if(Log.V) Log.v("HelpSection for id = " + id + ": " + section);
        return section;
    }


    //////////////////////////////////////////////
    // ContentLoaderCallback interface

    @Override
    public void onOnlineContentLoaded(long sectionId, String content)
    {
        if(Log.I) Log.i("HelpRegistry.onOnlineContentLoaded(). sectionId = " + sectionId + ": content = " + content);

        if(content != null && !content.isEmpty()) {
            // Store the new section in the registry
            long newExpTime = System.currentTimeMillis() + DEFAULT_TTL;
            // temporary
            String title = OnlineLoaderUtil.getTitleFromHtml(content);
            HelpSection section = new HelpSection(title, content);
            TimestampSectionPair pair = new TimestampSectionPair(section, newExpTime);
            registry.put(sectionId, pair);
        } else {
            Log.w("HelpRegistry.onOnlineContentLoaded(). Empty content returned for sectionId = " + sectionId);
        }
    }

}

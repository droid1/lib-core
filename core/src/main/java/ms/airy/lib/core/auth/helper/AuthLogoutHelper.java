package ms.airy.lib.core.auth.helper;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;

import java.util.HashSet;
import java.util.Set;

import ms.airy.lib.core.dialog.ActionableAssertDialogListener;
import ms.airy.lib.core.dialog.stock.SimpleResponseDialogFragment;


// TBD:
// It can be used with or without dialog.
public final class AuthLogoutHelper implements ActionableAssertDialogListener
{
    // temporary
    private static final String DIALOG_TITLE = "Logout";
    private static final String DIALOG_TAG = "auth_logout_dialog";

    private static AuthLogoutHelper INSTANCE = null;
    public static AuthLogoutHelper getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new AuthLogoutHelper(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;
    
    // Logout Message
    private String message;
    
    // TBD:
    // target account type and name ???
    // ....
    

    private AuthLogoutHelper(Context context)
    {
        this(context, null);
    }
    private AuthLogoutHelper(Context context, AuthLogoutCallback callback)
    {
        this.context = context;
        if(callback != null) {
            addAuthLogoutCallback(callback);
        }
        
        // TBD:
        // Load the message ...
        // temporary
        message = "Log out from ...";   // + Account Type...
        // ...
    }


    public void startLogoutFlow(Activity activity)
    {
        // TBD ...
        // ...

        showAccountPickerDialog(activity);
    }

    private void showAccountPickerDialog(Activity activity)
    {
        if(activity == null) {
            if(context instanceof Activity) {
                activity = (Activity) context;
            }
        }
        if(activity == null) {
            Log.e("activity is null. Cannot display logout dialog.");
            return;   // ??
        }
        // temporary
        // positive + neutral buttons only.
        DialogFragment fragment = SimpleResponseDialogFragment.newInstance(
                DIALOG_TITLE,
                message,
                null,
                "",
                null
        ).addActionable(this);
        fragment.show(activity.getFragmentManager(), DIALOG_TAG);
    }




    //////////////////////////////////////////
    // ActionableAssertDialogListener interface

    @Override
    public void onPositiveClick(String tag)
    {
        if(DIALOG_TAG.equals(tag)) {
            for (AuthLogoutCallback c : callbacks) {
                c.onLogoutFlowCanceled();
            }
        } else {
            // Ignore.
        }
    }

    @Override
    public void onNegativeClick(String tag)
    {
        // Ignore.
        // No negative button is displayed.
    }

    @Override
    public void onNeutralClick(String tag)
    {
        if(DIALOG_TAG.equals(tag)) {
            
            // TBD:
            // Initiate the logout..
            // ...
            
            for (AuthLogoutCallback c : callbacks) {
                c.onLogoutFlowStarted();
            }
        } else {
            // Ignore.
        }
    }




    //////////////////////////////////////////
    // For "relaying" ActionableAssertDialogListener methods ....

    public static interface AuthLogoutCallback
    {
        // void onAuthLogouted(Account account);
        void onLogoutFlowStarted();
        void onLogoutFlowCanceled();
    }

    // We allow multiple callbacks, but most likely, the client only needs one.
    private final Set<AuthLogoutCallback> callbacks = new HashSet<AuthLogoutCallback>();
    public final void addAuthLogoutCallback(AuthLogoutCallback callback)
    {
        callbacks.add(callback);
    }

}

package ms.airy.lib.core.ui.headerbar;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.drawable.Drawable;
import android.view.View;


// Substitute for ActionBar....
public class HeaderBar
{
    // ????
    //Context mContext;
    
    // TBD: Use BaseActivity???
    private Activity mParent;
    
    // To be used for "Up" button...
    private Intent mUpIntent;
    
    
    // The ui part
    //private HeaderBarFragment fragment;

    // The ui part
    private HeaderBarView mView;

    
    // TBD: Do we need this????
    // Maybe just use the data in LinearLayout ?????
    
    
    // Title
    private String mTitle;
    
    // Caption
    private String mCaption;
    
    
    // "Home" ????
    private HeaderItem mMenu;

    
    // "Action items"  ???? 
    //private HeaderItemList mItems;
    private List<HeaderItem> mItems;
    
    
    // TBD
    private boolean mIncludeTitleView = true;
    private boolean mIncludeMenuButton = true;
    // ...
    
    
    // Client sets this flag to indicate that the all items have been added.
    private boolean mInitialized = false;
    


    // TBD: .....
    // Change context to parent activity
    public HeaderBar(Activity parent)
    {
        // ....
        mParent = parent;
        //mContext = context;
        mView = new HeaderBarView(mParent);

        // ...
        resetHeaderItems();
        
        // ???
        mInitialized = false;
    }
    
    
    protected Activity getParent()
    {
        return mParent;    
    }
        
    public Intent getUpIntent()
    {
        return mUpIntent;
    }
    public void setUpIntent(Intent upIntent)
    {
        this.mUpIntent = upIntent;
    }
    public void resetUpIntent()
    {
        this.mUpIntent = null;
    }


    // ....
    protected void resetHeaderItems()
    {
        //mItems = new HeaderItemList();
        mItems = new ArrayList<HeaderItem>();
        // Remove all views in mView...
        //if(mView != null) {
            mView.removeAllHeaderButtons();
        //}
        // ...
        
        if(mIncludeTitleView) {
            // TBD
            mTitle = "title"; // ????
            mView.setTitle(mTitle);
        }
        
        if(mIncludeMenuButton) {
            Drawable menuIcon = null;   // TBD
            ActionTarget menuTarget = new ActionTarget(ActionType.MENU);  // TBD
            HeaderItem menu = new HeaderItem(mParent, "menu", "Menu", menuIcon, menuTarget);
            setMenu(menu);
        }
        
        // ???
        //mInitialized = false;
    }

    public boolean isInitialized()
    {
        return mInitialized;
    }
    public void setInitialized()
    {
        setInitialized(true);
    }
    public void setInitialized(boolean initialized)
    {
        this.mInitialized = initialized;
    }


    public HeaderBarView getView()
    {
        if(Log.D) Log.d("getView()");

        return mView;
    }

    // TBD: Should we expose this????
    private void setView(HeaderBarView view)
    {
        if(Log.D) Log.d("setView()");

        this.mView = view;
        // TBD:
        // Update all other attributes????
        
        
        // ?????
        mView.invalidate();
    }

    // ???
    private void refreshView()
    {
        // TBD:
        // based on the current mItems
        // update the linearlayout...
        // ....

        // ?????
        mView.invalidate();
    }

    
    
    // ???
    protected void doActionTarget(ActionTarget target)
    {
        if(Log.D) Log.d("doActionTarget()");

        if(target != null) {
            ActionType type = target.getType();
            if(Log.D) Log.d("actionType = " + type);

            if(type == ActionType.HOME) {
                // ???
                Intent intent = null;
                //try {
                    intent = mParent.getPackageManager().getLaunchIntentForPackage(mParent.getPackageName());
                //} catch(NameNotFoundException ex) {
                //    // ignore
                //    if(Log.I) Log.i("Default intent not found. ", ex);
                //}
                
                if(intent == null) {
                    if(Log.I) Log.i("Default intent not found. ");

                    intent = new Intent(Intent.ACTION_MAIN);
                    intent.addCategory(Intent.CATEGORY_LAUNCHER);
                    //intent.addCategory(Intent.CATEGORY_DEFAULT);
                    //intent.setPackage(mParent.getPackageName());  // Note: Main/home activity might be in a different package than application package.
                }
                
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mParent.startActivity(intent);
                
                // ????
            } else if(type == ActionType.UP) {
                // ???
                if(mUpIntent != null) {
                    // TBD....
                    Intent i = new Intent(mUpIntent);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mParent.startActivity(i);
                } else {
                    if(Log.W) Log.w("UpIntent is null. Up action is undefined. Just closing the activity....");
                    // ???
                    this.getParent().finish();
                    // ...
                }                
            } else if(type == ActionType.MENU) {
                // ????
                mParent.openOptionsMenu();
                //mParent.openContextMenu(mParent.getCurrentFocus());  // ?????
                // ???
            } else {
                if(type == ActionType.FRAGMENT) {
                    // TBD: 
                    Fragment fragment = target.getFragment();
                    // ????
                    // ....
                } else if(type == ActionType.ACTIVITY) {
                    Class<?> activity = target.getActivity();
                    if(activity != null) {
                        Intent intent = new Intent(mParent, activity);
                        mParent.startActivity(intent); // ???
                    } else {
                        // ????
                    }
                } else if(type == ActionType.INTENT) {
                    Intent intent = target.getIntent();
                    if(intent != null) {
                        mParent.startActivity(intent); // ???
                    } else {
                        // ????
                    }
                } else if(type == ActionType.CALLBACK) {
                    ActionCallback callback = target.getCallback();
                    if(callback != null) {
                        // TBD: ....
                        callback.doAction();
                    } else {
                        // ????
                    }
                } else {
                    // ???
                }
            }
        } else {
            // ignore ????
            // ....
        }
    }
    
    
    public void addHeaderItem(final HeaderItem item)
    {
        if(Log.D) Log.d("addHeaderItem()");
        mItems.add(item);
        HeaderButton button = item.getHeaderButton();
        button.setActionListener(new ActionListener() {
            public void onClick(View v)
            {
                // TBD:
                ActionTarget target = item.getTarget();
                HeaderBar.this.doActionTarget(target);
            }            
        });
        mView.addHeaderButton(button);
    }
    public void addHeaderItems(final HeaderItem... items)
    {
        if(Log.D) Log.d("addHeaderItems()");
        //mItems.addAll(Arrays.asList(items));
        // mView.???
        for(final HeaderItem i : items) {
            mItems.add(i);
            HeaderButton button = i.getHeaderButton();
            button.setActionListener(new ActionListener() {
                public void onClick(View v)
                {
                    // TBD:
                    ActionTarget target = i.getTarget();
                    HeaderBar.this.doActionTarget(target);
                }            
            });
            mView.addHeaderButton(button);
        }
    }
    public void addHeaderItem(int location, final HeaderItem item)
    {
        if(Log.D) Log.d("addHeaderItem(): location = " + location);
        mItems.add(location, item);
        HeaderButton button = item.getHeaderButton();
        button.setActionListener(new ActionListener() {
            public void onClick(View v)
            {
                // TBD:
                ActionTarget target = item.getTarget();
                HeaderBar.this.doActionTarget(target);
            }            
        });
        mView.addHeaderButton(location, button);
    }
    public void addHeaderItems(int location, final HeaderItem... items)
    {
        if(Log.D) Log.d("addHeaderItems(): location = " + location);
        //mItems.addAll(location, Arrays.asList(items));
        // mView.????
        for(int n=items.length-1; n>=0; n--) {   // Does this make sense?????
            final HeaderItem item = items[n];
            mItems.add(location, item);
            HeaderButton button = item.getHeaderButton();
            button.setActionListener(new ActionListener() {
                public void onClick(View v)
                {
                    // TBD:
                    ActionTarget target = item.getTarget();
                    HeaderBar.this.doActionTarget(target);
                }            
            });
            mView.addHeaderButton(location, button);
        }
    }
    
    // TBD:
    //public void removeHeaderItem(String tag) {}
    //public void removeHeaderItems(String... tags) {}
    
    // TBD:
    public void removeAllHeaderItems()
    {
        if(Log.D) Log.d("removeAllHeaderItems()");
        
        mItems = new ArrayList<HeaderItem>();
        mView.removeAllHeaderButtons();
    }
    
    
    
    public String getTitle()
    {
        if(Log.D) Log.d("getTitle()");

        return mTitle;
    }
    public void setTitle(final String title)
    {
        if(Log.D) Log.d("setTitle()");

        this.mTitle = title;
        mView.setTitle(mTitle);
    }

    
    public String getCaption()
    {
        if(Log.D) Log.d("getCaption()");

        return mCaption;
    }
    public void setCaption(final String caption)
    {
        if(Log.D) Log.d("setCaption()");

        this.mCaption = caption;
        mView.setCaption(mCaption);
    }

    public HeaderItem getMenu()
    {
        if(Log.D) Log.d("getMenu()");

        return mMenu;
    }
    public void setMenu(final HeaderItem menu)
    {
        if(Log.D) Log.d("setMenu()");

        this.mMenu = menu;
        HeaderButton button = mMenu.getHeaderButton();
        button.setActionListener(new ActionListener() {
            public void onClick(View v)
            {
                // TBD:
                ActionTarget target = HeaderBar.this.mMenu.getTarget();
                HeaderBar.this.doActionTarget(target);
            }
        });
        mView.setMenuButton(button);
    }

    // TBD: Does this work???
    public void setMenuAction(final ActionTarget target)
    {
        if(Log.D) Log.d("setMenuAction()");

        if(mMenu == null) {
            if(Log.W) Log.w("Menu is null, Action target cannot be set.");
            return;   // ???
        } else {
            mMenu.setTarget(target);
            HeaderButton button = mMenu.getHeaderButton();
            button.setActionListener(new ActionListener() {
                public void onClick(View v)
                {
                    // TBD:
                    ActionTarget target = HeaderBar.this.mMenu.getTarget();
                    HeaderBar.this.doActionTarget(target);
                }
            });
        }
    }

    
    
}

package ms.airy.lib.core.loader;

import ms.airy.lib.core.core.Config;
import ms.airy.lib.core.core.LogBase;


// Unusual implementation.
// The public APIs are all static,
// which are actually wrappers around the methods of a singleton.
// Note also that all APIs are package scoped.
// In general, you can have one shared Log instance per package.
// If a class specific Log (say, with a different tag) is needed,
// then a separate LogXYZ class needs to be created for each such class.
final class Log extends LogBase
{
    // Static Log Tag.
    private static final String TAG = "amcc.loader";
    // Static Log Level. Set it to a package/class specific value here, if needed.
    private static final int LEVEL = Config.LOGLEVEL;


    // A singleton using the On-Demand-Holder pattern.
    private Log()
    {
        super(TAG, LEVEL);
    }
    private static class LogHolder
    {
        private static final Log INSTANCE = new Log();
    }
    private static Log getInstance()
    {
        return LogHolder.INSTANCE;
    }


    // Returns the (read-only) logger tag.
    static String getTag()
    {
        return Log.getInstance().getLogTag();
    }

    // Returns the (read-only) logger level.
    static int getLevel()
    {
        return Log.getInstance().getLogLevel();
    }


    // "isLoggable()" boolean flags.
    static final boolean V = Log.getInstance().isLoggable(android.util.Log.VERBOSE);
    static final boolean D = Log.getInstance().isLoggable(android.util.Log.DEBUG);
    static final boolean I = Log.getInstance().isLoggable(android.util.Log.INFO);
    static final boolean W = Log.getInstance().isLoggable(android.util.Log.WARN);
    static final boolean E = Log.getInstance().isLoggable(android.util.Log.ERROR);


    // Static logger methods.
    // v() through e().

    static void v(String logMe)
    {
        Log.getInstance().logV(logMe);
    }
    static void v(String logMe, Throwable ex)
    {
        Log.getInstance().logV(logMe, ex);
    }

    static void d(String logMe)
    {
        Log.getInstance().logD(logMe);
    }
    static void d(String logMe, Throwable ex)
    {
        Log.getInstance().logD(logMe, ex);
    }

    static void i(String logMe)
    {
        Log.getInstance().logI(logMe);
    }
    static void i(String logMe, Throwable ex)
    {
        Log.getInstance().logI(logMe, ex);
    }

    static void w(String logMe)
    {
        Log.getInstance().logW(logMe);
    }
    static void w(String logMe, Throwable ex)
    {
        Log.getInstance().logW(logMe, ex);
    }

    static void e(String logMe)
    {
        Log.getInstance().logE(logMe);
    }
    static void e(String logMe, Throwable ex)
    {
        Log.getInstance().logE(logMe, ex);
    }

}

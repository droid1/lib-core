package ms.airy.lib.core.time.handler;

import android.os.Looper;

import ms.airy.lib.core.time.common.PollingSchedule;

import java.util.ArrayList;
import java.util.List;


/**
 * Polling using linearly increasing intervals.
 */
public class LinearPollingHandler extends PollingHandler
{
    // Initial polling interval.
    // In milli seconds.
    private long interval = 10000L;
    // Increase in each polling.
    private long delta = 1000L;

    public LinearPollingHandler()
    {
        init();
    }
    // TBD: do we need callback?
    public LinearPollingHandler(Callback callback)
    {
        super(callback);
        init();
    }
    public LinearPollingHandler(Looper looper)
    {
        super(looper);
        init();
    }
    // TBD: do we need callback?
    public LinearPollingHandler(Looper looper, Callback callback)
    {
        super(looper, callback);
        init();
    }
    private void init()
    {
        setPollingSchedule(PollingSchedule.LINEAR);
    }


    public final long getPollingInterval()
    {
        return interval;
    }
    public final void setPollingInterval(long interval)
    {
        this.interval = interval;
    }

    public final long getDelta()
    {
        return delta;
    }
    public final void setDelta(long delta)
    {
        this.delta = delta;
    }

    public final void setPollingIntervalAndDelta(long interval, long delta)
    {
        if(Log.D) Log.d("setPollingIntervalAndDelta() called with interval = " + interval + " & delta = " + delta);
        this.interval = interval;
        this.delta = delta;
    }



    // Note the name of the methods.
    // The "fetch" method is not a "getter", and it is not idempotent.
    // Calling these methods changes the internal state of the handler.
    @Override
    protected long[] fetchNextDelays(int size)
    {
        long[] delays = computeNextDelays(size, this.interval, this.delta);
        if(delays != null && delays.length > 0) {
            this.interval = delays[0];
            // delta does not change.
        }
        return delays;
    }
    @Override
    protected long[] fetchNextDelays(long duration)
    {
        long[] delays = computeNextDelays(duration, this.interval, this.delta);
        if(delays != null && delays.length > 0) {
            this.interval = delays[0];
        }
        return delays;
    }
    @Override
    protected long[] computeNextDelays(int size)
    {
        return computeNextDelays(size, this.interval, this.delta);
    }
    @Override
    protected long[] computeNextDelays(long duration)
    {
        return computeNextDelays(duration, this.interval, this.delta);
    }
    private long[] computeNextDelays(int size, long interval, long delta)
    {
        // Note: the first element of delays is interval + delta, not interval.
        long[] delays = new long[size];
        for (int i = 0; i < size; i++) {
            delays[i] = interval + delta * (i+1);
        }
        return delays;
    }
    private long[] computeNextDelays(long duration, long interval, long delta)
    {
        List<Long> list = new ArrayList<Long>();
        long totalDelay = 0;
        int idx = 0;
        while(totalDelay < duration && idx < 10000) {   // arbitrary cutoff to prevent infinite loop.
            long delay = interval + delta * (idx+1);
            list.add(delay);
            totalDelay += delay;
            idx++;
        }
        // Long[] arr = list.toArray(new Long[]{});
        long[] delays = new long[list.size()];
        for (int i = 0; i < list.size(); i++) {
            delays[i] = list.get(i);
        }
        return delays;
    }


}

package ms.airy.lib.core.notification;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import java.util.HashMap;
import java.util.Map;


/**
 * "Multi-ton" based on the tag.
 */
public final class NotificationHelper
{
    // If the notiId is not specified, value 0 is used.
    // Each NotificationHelper for a given tag (including null) can have one default notification with id==0.
    private static final int DEF_NOTIFICATION_ID = 0;

    // Tag -> NotificationHelper singleton for the given tag.
    private final static Map<String, NotificationHelper> HELPERS = new HashMap<String, NotificationHelper>();
    // No tag, default singleton.
    private static NotificationHelper INSTANCE = null;

    public static NotificationHelper getInstance(final Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new NotificationHelper(context.getApplicationContext());
        }
        return INSTANCE;
    }
    public static NotificationHelper getInstance(final Context context, final String tag)
    {
        if(tag == null || tag.isEmpty()) {   // ????
            return getInstance(context);
        }
        if(HELPERS.get(tag) == null) {
            HELPERS.put(tag, new NotificationHelper(context.getApplicationContext(), tag));
        }
        return HELPERS.get(tag);
    }


    private final Context context;
    private final String tag;
    private final NotificationManager notiMgr;

    private NotificationHelper(Context context)
    {
        this(context, null);
    }
    private NotificationHelper(Context context, String tag)
    {
        this.context = context;
        this.tag = tag;
        notiMgr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    public String getTag()
    {
        return tag;
    }


    // Create/update notification with the given notiId.
    public void showNotification(int requestCode, Class<? extends Activity> clss, String contentTitle, String contentText, int smallIconRes)
    {
        showNotification(DEF_NOTIFICATION_ID, requestCode, clss, contentTitle, contentText, smallIconRes);
    }
    public void showNotification(int notiId, int requestCode, Class<? extends Activity> clss, String contentTitle, String contentText, int smallIconRes)
    {
//        PendingIntent contentIntent = PendingIntent.getActivity(context,
//                requestCode,
//                new Intent(context, clss),
//                Intent.FLAG_ACTIVITY_NEW_TASK);
//        showNotification(notiId, contentIntent, contentTitle, contentText, smallIconRes);

        Notification notification = NotificationBuilder.buildNotification(context, requestCode, clss, contentTitle, contentText, smallIconRes);
        showNotification(notiId, notification);
    }

    public void showNotification(PendingIntent contentIntent, String contentTitle, String contentText, int smallIconRes)
    {
        showNotification(DEF_NOTIFICATION_ID, contentIntent, contentTitle, contentText, smallIconRes);
    }
    public void showNotification(int notiId, PendingIntent contentIntent, String contentTitle, String contentText, int smallIconRes)
    {
//        // TBD:
////        Notification notification = new Notification.Builder(mContext)
////                .setContentTitle(contentTitle)
////                .setContentText(contentText)
////                .setSmallIcon(smallIconRes)
////                .setContentIntent(contentIntent)
////                .build();
//        Notification notification = new NotificationCompat.Builder(context)
//                .setContentTitle(contentTitle)
//                .setContentText(contentText)
//                .setSmallIcon(smallIconRes)
//                .setContentIntent(contentIntent)
//                .build();

        Notification notification = NotificationBuilder.buildNotification(context, contentIntent, contentTitle, contentText, smallIconRes);
        showNotification(notiId, notification);
    }

    public void showNotification(Notification notification)
    {
        showNotification(DEF_NOTIFICATION_ID, notification);
    }
    public void showNotification(int notiId, Notification notification)
    {
        notiMgr.notify(tag, notiId, notification);
    }

    public void cancelNotification()
    {
        cancelNotification(DEF_NOTIFICATION_ID);
    }
    public void cancelNotification(int notiId)
    {
        notiMgr.cancel(tag, notiId);
    }



    // TBD:
    private NotificationCompat.Builder buildNotificationBuilder()
    {
        NotificationCompat.Builder builder = null;



        return builder;
    }


}

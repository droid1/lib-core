package ms.airy.lib.core.time.handler;

import android.os.Looper;

import ms.airy.lib.core.time.common.PollingSchedule;


/**
 */
public class PollingHandlerFactory
{
    public static PollingHandler buildPollingHandler(int pollingSchedule)
    {
        PollingHandler handler = null;
        switch(pollingSchedule) {
            case PollingSchedule.CONSTANT:
            default:
                handler = new ConstantPollingHandler();
                break;
            case PollingSchedule.LINEAR:
                handler = new LinearPollingHandler();
                break;
            case PollingSchedule.FIBONACCI:
                handler = new FibonacciPollingHandler();
                break;
        }
        return handler;
    }

    public static PollingHandler buildPollingHandler(int pollingSchedule, Looper looper)
    {
        if(looper == null) {
            Log.w("buildPollingHandler() Input looper is null!");
            return buildPollingHandler(pollingSchedule);
        }
        PollingHandler handler = null;
        switch(pollingSchedule) {
            case PollingSchedule.CONSTANT:
            default:
                handler = new ConstantPollingHandler(looper);
                break;
            case PollingSchedule.LINEAR:
                handler = new LinearPollingHandler(looper);
                break;
            case PollingSchedule.FIBONACCI:
                handler = new FibonacciPollingHandler(looper);
                break;
        }
        return handler;
    }


}

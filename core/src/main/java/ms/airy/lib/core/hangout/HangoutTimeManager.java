package ms.airy.lib.core.hangout;

import android.content.Context;


// "Office hours"
public final class HangoutTimeManager
{
    private static HangoutTimeManager INSTANCE = null;
    public static HangoutTimeManager getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new HangoutTimeManager(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;


    private HangoutTimeManager(Context context)
    {
        this.context = context;
    }


    // TBD:
    // ....


}

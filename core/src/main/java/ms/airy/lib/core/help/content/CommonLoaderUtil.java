package ms.airy.lib.core.help.content;


/**
 */
public abstract class CommonLoaderUtil
{
    protected CommonLoaderUtil() {}


    // Heck.
    // We really need to use the html parser libary...
    public static String getTitleFromHtml(String htmlContent)
    {
        if(htmlContent == null || htmlContent.isEmpty()) {
            // ???
            return null;
        }

        int idx1 = htmlContent.indexOf("<title>");
        if(idx1 == -1) {
            idx1 = htmlContent.indexOf("<TITLE>");
        }
        if(idx1 == -1) {
            return null;
        }

        int idx2 = htmlContent.indexOf("</title>");
        if(idx2 == -1) {
            idx2 = htmlContent.indexOf("</TITLE>");
        }

        String title = null;
        if(idx2 == -1) {
            title = htmlContent.substring(idx1 + "<title>".length());
        } else {
            title = htmlContent.substring(idx1 + "<title>".length(), idx2);
        }
        title = title.trim();

        if(Log.V) Log.v("htmlContent title = " + title);
        return title;
    }



}

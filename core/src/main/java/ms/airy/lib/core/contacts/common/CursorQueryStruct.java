package ms.airy.lib.core.contacts.common;

import android.widget.CursorAdapter;


// With a lack of a better name...
// This is just a triplet of objects pertinent to CursorLoader...
public class CursorQueryStruct
{
//    private final int id;   // Loader ID.
    private final CursorAdapter cursorAdapter;
    private final String selectQuery;
    private final String[] projection;


    public CursorQueryStruct(CursorAdapter cursorAdapter, String selectQuery)
    {
        this(cursorAdapter, selectQuery, null);
    }
    public CursorQueryStruct(CursorAdapter cursorAdapter, String selectQuery, String[] projection)
    {
//        this.id = id;
        this.cursorAdapter = cursorAdapter;
        this.selectQuery = selectQuery;
        this.projection = projection;
    }

//    public int getId()
//    {
//        return id;
//    }

    public CursorAdapter getCursorAdapter()
    {
        return cursorAdapter;
    }

    public String getSelectQuery()
    {
        return selectQuery;
    }

    public String[] getProjection()
    {
        return projection;
    }
}

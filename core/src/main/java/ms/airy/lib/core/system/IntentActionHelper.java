package ms.airy.lib.core.system;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ms.airy.lib.core.app.AppInfoManager;
import ms.airy.lib.core.user.prefs.UserDevicePrefsStoreManager;


public final class IntentActionHelper
{
    private static IntentActionHelper INSTANCE = null;
    public static IntentActionHelper getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new IntentActionHelper(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;
    private IntentActionHelper(Context context)
    {
        this.context = context;
    }

    // "cache"
    private final Map<String,Boolean> actionAvailabilityMap = new HashMap<String, Boolean>();

    // Note: this does not really work, in general.
    // The caller may not actually call the returned activities from queryIntentActivities()
    //        if the activities may not have been "exported"..
    // TBD: Is there a better way?
    //      How to fix this????
    // Cf. queryIntentActivitiesAsUser() ????
    public boolean canHandleIntentAction(String action)
    {
        if(! actionAvailabilityMap.containsKey(action)) {
            try {
                final PackageManager packageManager = context.getPackageManager();
                final Intent intent = new Intent(action);

                // TBD:
                List<ResolveInfo> list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);

                // TBD: What is queryIntentActivitiesAsUser()
                //      Android Studio shows this in the source code, but it does not actually exist.
                // int appUID = AppInfoManager.getInstance(context).getApplicationUID();
                // List<ResolveInfo> list = packageManager.queryIntentActivitiesAsUser(intent, PackageManager.MATCH_DEFAULT_ONLY, appUID);

                boolean canHandle = (list.size() > 0);
                actionAvailabilityMap.put(action, canHandle);
            } catch(Exception e) {
                // Ignore
                if(Log.I) Log.i("canHandleIntentAction() Unknown error: action = " + action, e);
            }
        }
        if(actionAvailabilityMap.containsKey(action)) {
            return actionAvailabilityMap.get(action);
        } else {
            // ???
            return false;
        }
    }

    // TBD:
    public boolean canHandleIntent(Intent intent)
    {
        final PackageManager packageManager = context.getPackageManager();
        List<ResolveInfo> list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }

}

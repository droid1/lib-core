package ms.airy.lib.core.feedback.client.impl;

import ms.airy.lib.core.feedback.client.OnlineSurveyClient;
import ms.airy.lib.core.feedback.common.SurveyEntry;

import org.miniclient.ApiUserClient;
import org.miniclient.RestApiException;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 */
public class MiniOnlineSurveyClient extends AbstractOnlineFeedbackClient implements OnlineSurveyClient
{
    private ApiUserClient miniFeedbackWebClient = null;

    public MiniOnlineSurveyClient()
    {
        this((String) null);
    }
    public MiniOnlineSurveyClient(String feedbackWebServiceEndpoint)
    {
        super(feedbackWebServiceEndpoint);
        this.miniFeedbackWebClient = MiniClientBuilder.getInstance().getFeedbackWebClient(feedbackWebServiceEndpoint);
    }
    public MiniOnlineSurveyClient(ApiUserClient miniFeedbackWebClient)
    {
        super(miniFeedbackWebClient.getResourceBaseUrl());
        this.miniFeedbackWebClient = miniFeedbackWebClient;
    }

    public ApiUserClient getMiniFeedbackWebClient()
    {
        return miniFeedbackWebClient;
    }
    protected void setMiniFeedbackWebClient(String feedbackWebServiceEndpoint)
    {
        super.setFeedbackWebServiceEndpoint(feedbackWebServiceEndpoint);
        this.miniFeedbackWebClient = MiniClientBuilder.getInstance().getFeedbackWebClient(feedbackWebServiceEndpoint);
    }
    protected void setMiniFeedbackWebClient(ApiUserClient miniFeedbackWebClient)
    {
        super.setFeedbackWebServiceEndpoint(miniFeedbackWebClient.getResourceBaseUrl());
        this.miniFeedbackWebClient = miniFeedbackWebClient;
    }

    @Override
    protected void setFeedbackWebServiceEndpoint(String feedbackWebServiceEndpoint)
    {
        super.setFeedbackWebServiceEndpoint(feedbackWebServiceEndpoint);
        this.miniFeedbackWebClient = MiniClientBuilder.getInstance().getFeedbackWebClient(feedbackWebServiceEndpoint);
    }


    @Override
    public SurveyEntry getSpecialSurvey()
    {
        Log.i("MiniOnlineSurveyClient.getSpecialSurvey().");

        Object result = null;
        try {
            // TBD:
            Map<String,Object> params = new HashMap<String, Object>();
            List<Object> objs = miniFeedbackWebClient.list(params);
            if(objs != null && objs.size() > 0) {
                result = objs.get(0);   // ???
            }
        } catch (RestApiException e) {
            Log.e("Failed to get survey.", e);
        } catch (IOException e) {
            Log.e("IO Exception while fetching survey.", e);
        }

        SurveyEntry surveyEntry = null;
        if(result != null) {
            // TBD:
            // convert the map to surveyEntry
            // ...
        }

        return surveyEntry;
    }

    @Override
    public SurveyEntry getDailySurvey(long time)
    {
        if(Log.I) Log.i("MiniOnlineSurveyClient.getDailySurvey(). time = " + time);

        Object result = null;
        try {
            // TBD:
            Map<String,Object> params = new HashMap<String, Object>();
            params.put("time", time);  // ???
            List<Object> objs = miniFeedbackWebClient.list(params);
            if(objs != null && objs.size() > 0) {
                result = objs.get(0);   // ???
            }
        } catch (RestApiException e) {
            Log.e("Failed to get survey for time = " + time, e);
        } catch (IOException e) {
            Log.e("IO Exception while fetching survey for time = " + time, e);
        }

        SurveyEntry surveyEntry = null;
        if(result != null) {
            // TBD:
            // convert the map to surveyEntry
            // ...
        }

        return surveyEntry;
    }

    @Override
    public SurveyEntry getSurvey(long id)
    {
        if(Log.I) Log.i("MiniOnlineSurveyClient.getSurvey(). id = " + id);

        Object result = null;
        try {
            // TBD:
            Map<String,Object> params = new HashMap<String, Object>();
            params.put("id", id);  // ???
            List<Object> objs = miniFeedbackWebClient.list(params);
            if(objs != null && objs.size() > 0) {
                result = objs.get(0);   // ???
            }
        } catch (RestApiException e) {
            Log.e("Failed to get survey for id = " + id, e);
        } catch (IOException e) {
            Log.e("IO Exception while fetching survey for id = " + id, e);
        }

        SurveyEntry surveyEntry = null;
        if(result != null) {
            // TBD:
            // convert the map to surveyEntry
            // ...
        }

        return surveyEntry;
    }
}

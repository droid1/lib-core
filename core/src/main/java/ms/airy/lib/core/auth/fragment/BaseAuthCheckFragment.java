package ms.airy.lib.core.auth.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;


// TBD:
// To be included in the app's launcher activity.
//   (tbd: Just use BaseAuthSupportFragment ???)
// This fragment checks, for example, if the user has set the "primary account", etc.
// ....
public class BaseAuthCheckFragment extends Fragment
{
    // boolean
    protected static final String ARG_AUTO_PICK_PRIMARY = "base_auto_pick_primary";
    protected static final String ARG_SHOW_PRIMARY_PICKER = "base_show_primary_picker";
    // ...



    public BaseAuthCheckFragment()
    {
    }


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // tbd
        setRetainInstance(true);
        // ...
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        return null;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        // TBD:
        // Check if the user has the primary account set.
        //...
        // If not,
        //    if there is only one "default account",
        //        then automatically select it (without notifying the user) and move on
        //    else
        //        show the picker dialog to the user or just auto pick ???
        // ...


    }


    @Override
    public void onStart()
    {
        super.onStart();
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }

    @Override
    public void onPause()
    {
        super.onPause();
    }

    @Override
    public void onStop()
    {
        super.onStop();
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
    }


}

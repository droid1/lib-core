package ms.airy.lib.core.common;

public enum Gender
{
    UNKNOWN ("unknown"),
    MALE ("male"),
    FEMALE ("female");
    
    private final String gender;

    Gender(String gender)
    {
        //this.gender = gender.toLowerCase();
        this.gender = gender;
    }

    public static Gender parseGender(String str)
    {
        if(str == null || str.isEmpty()) {
            throw new IllegalArgumentException("Error: Empty argument.");
        }
        String value = str.toLowerCase();
        if(value.equals(MALE.toString())) {
            return MALE;
        } else if(value.equals(FEMALE.toString())) {
            return FEMALE;
        } else {
            // TBD: 
            //return UNKNOWN;  // ???
            throw new IllegalArgumentException("Error: Invalid argument: " + str);
        }        
    }

    @Override
    public String toString()
    {
        return this.gender;
    }
    
}

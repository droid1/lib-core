package ms.airy.lib.core.control;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;

import java.util.HashMap;
import java.util.Map;

import ms.airy.lib.core.control.common.ControlAxis;


// TBD:
// Support fixed/orientation-dep. aspect ratio ???
// ...
public abstract class FixedShapeFrameLayout extends AnamorphicFrameLayout
{
    // tbd:
    // aspect ratio ???
    // private boolean aspectRatioFixed = false;  // --> Just use (aspectRatio == null).
    // note that we define aspect ratio as a string/constant, "w:h", not as a float.
    // This value is for axis == "horz"  (NOT "vert", as is more reasonable for a phone device)
    //   When the device is in "vert", we need to "flip" this value.
    private String aspectRatio;
    // ???
    // Note that if aspectAxisDependent == false,
    //     then we use the same (or, flipped/inverted, to be more precise) aspect ratios
    //     for vert and horz.
    // if aspectAxisDependent == true,
    //     then we can potentially use different aspect ratios for vert and horz.
    // This is however way too complicated...
    // For now, just use a single fixed value for horz and vert (flipped).
//    private boolean aspectAxisDependent;
//    private final Map<Integer, String> axisAspectRatioMap = new HashMap<Integer, String>();
    // ???
//    private boolean aspectOrientationDependent;
//    private final Map<Integer, String> orientationAspectRatioMap = new HashMap<Integer, String>();
    // ???


    public FixedShapeFrameLayout(Context context)
    {
        super(context);
        initControl();
    }
    public FixedShapeFrameLayout(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        initControl();
    }
    public FixedShapeFrameLayout(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        initControl();
    }

    // Note that it's not a good idea to call virtual/non-final method in ctor....
    private void initControl()
    {
        // ???
        aspectRatio = null;
    }


    public String getAspectRatio()
    {
        return aspectRatio;
    }
    public void setAspectRatio(String aspectRatio)
    {
        this.aspectRatio = aspectRatio;
    }



    @Override
    protected Parcelable onSaveInstanceState()
    {
        Log.d("onSaveInstanceState() Called.");
        Parcelable superState = super.onSaveInstanceState();
        SavedState savedState = new SavedState(superState);
        savedState.setAspectRatio(aspectRatio);
        // ...
        return savedState;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state)
    {
        Log.d("onRestoreInstanceState() Called.");
        SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(savedState.getSuperState());
        setAspectRatio(savedState.getAspectRatio());
        // ...
    }


    // temporary
    protected static class SavedState extends AnamorphicFrameLayout.SavedState implements Parcelable
    {
        private String aspectRatio;

        public SavedState(Parcelable superState)
        {
            super(superState);
        }
        public SavedState(Parcel source)
        {
            super(source);
            aspectRatio = source.readString();
        }

        public String getAspectRatio()
        {
            return aspectRatio;
        }
        public void setAspectRatio(String aspectRatio)
        {
            this.aspectRatio = aspectRatio;
        }

        public static final Creator<SavedState> CREATOR = new Creator<SavedState>() {
            @Override
            public SavedState createFromParcel(Parcel source)
            {
                return new SavedState(source);
            }
            @Override
            public SavedState[] newArray(int size)
            {
                return new SavedState[size];
            }
        };

        @Override
        public void writeToParcel(Parcel dest, int flags)
        {
            super.writeToParcel(dest, flags);
            dest.writeString(aspectRatio);
        }

        // For debugging...
        @Override
        public String toString()
        {
            return "SavedState{" +
                    "aspectRatio='" + aspectRatio + '\'' +
                    '}';
        }
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        if(Log.D) Log.d("onMeasure() Called: widthMeasureSpec = " + widthMeasureSpec + "; heightMeasureSpec = " + heightMeasureSpec);

        // TBD:
        // How to enforce apsectRatio here ????
        if(aspectRatio == null) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        } else {

            // tbd
            // what to do ???
            // ...


            // temporary
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }


    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom)
    {
        if(Log.D) Log.d("onLayout() Called: changed = " + changed + "; left = " + left + "; top = " + top + "; right = " + right + "; bottom = " + bottom);

        // tbd
        // what to do ???
        // ...

        // ???
        super.onLayout(changed, left, top, right, bottom);
        // ...
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh)
    {
        if(Log.D) Log.d("onSizeChanged() Called: w = " + w + "; h = " + h + "; oldw = " + oldw + "; oldh = " + oldh);

        // tbd
        // what to do ???
        // ...

        super.onSizeChanged(w, h, oldw, oldh);
    }


}

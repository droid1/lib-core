package ms.airy.lib.core.config.common;


// Used as bitmasks.
public final class ConfigType
{
    // Shared Preferences. Only read-write config.
    public static final int TYPE_PREFS = (0x1 << 0);
    // "String" resources under /res/values.
    public static final int TYPE_RES = (0x1 << 1);
    // JSON file under /res/raw
    public static final int TYPE_JSON = (0x1 << 2);
    // Properties file under /res/raw or /res/assets
    public static final int TYPE_PROPS = (0x1 << 3);
    // Not being used.
    public static final int TYPE_XML = (0X1 << 4);
    // etc..

    // temporary
    public static final int ALL = TYPE_PREFS
            | TYPE_RES
            | TYPE_JSON
            | TYPE_PROPS
            | TYPE_XML;

    private ConfigType() {}

    // temporary
    public static int getDefaultType()
    {
        // ???
        return TYPE_RES;
    }

    public static boolean isPrefsConfigEnabled(int type)
    {
        return (type & TYPE_PREFS) != 0;
    }
    public static boolean isResourceConfigEnabled(int type)
    {
        return (type & TYPE_RES) != 0;
    }
    public static boolean isJsonConfigEnabled(int type)
    {
        return (type & TYPE_JSON) != 0;
    }
    public static boolean isPropertiesConfigEnabled(int type)
    {
        return (type & TYPE_PROPS) != 0;
    }


}

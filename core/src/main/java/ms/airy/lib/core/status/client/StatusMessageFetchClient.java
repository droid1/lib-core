package ms.airy.lib.core.status.client;

import ms.airy.lib.core.status.common.StatusMessage;

import java.util.List;


/**
 */
public interface StatusMessageFetchClient
{
    // TBD: Add filters such as type, etc. ???

    // String fetchMessage(long id);
    StatusMessage fetchStatusMessage(long id);
    List<Long> fetchNewMessages();
    // List<StatusMessage> fetchNewStatusMessages();
    List<Long> fetchMessages(long since);
    // List<StatusMessage> fetchStatusMessages(long since);
}

package ms.airy.lib.core.help.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import ms.airy.lib.core.help.manager.HelpManager;
import ms.airy.lib.core.toast.ToastHelper;


/**
 * Example usage:
 * In Activity.onCreate()
 *   fragmentTransaction.add(ContextSensitiveHelpFragment.newInstance(sectionId, sectionTitle), fragmentTag);
 */
public class ContextSensitiveHelpFragment extends Fragment
{
    // Long
    public static final String ARG_SECTION_ID = "help_section_id";
    // String
    public static final String ARG_SECTION_TITLE = "help_section_title";
    // ...

    // TBD:
    public static ContextSensitiveHelpFragment newInstance(long sectionId)
    {
        return newInstance(sectionId, null);
    }
    public static ContextSensitiveHelpFragment newInstance(long sectionId, String sectionTitle)
    {
        ContextSensitiveHelpFragment fragment = new ContextSensitiveHelpFragment();
        Bundle args = new Bundle();
        args.putLong(ARG_SECTION_ID, sectionId);
        if(sectionTitle != null && !sectionTitle.isEmpty()) {
            args.putString(ARG_SECTION_TITLE, sectionTitle);
        }
        fragment.setArguments(args);
        return fragment;
    }
    protected static ContextSensitiveHelpFragment addIntArgument(ContextSensitiveHelpFragment fragment, String key, int val)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        args.putInt(key, val);
        return fragment;
    }
    protected static ContextSensitiveHelpFragment addLongArgument(ContextSensitiveHelpFragment fragment, String key, long val)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        args.putLong(key, val);
        return fragment;
    }
    protected static ContextSensitiveHelpFragment addStringArgument(ContextSensitiveHelpFragment fragment, String key, String val)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        args.putString(key, val);
        return fragment;
    }
    public ContextSensitiveHelpFragment()
    {
    }


    // Each help fragment is associated with a sectionId.
    private long sectionId = 0L;
    // Default help dialog title can be overwritten if sectionTitle is set.
    private String sectionTitle = null;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        // tbd
        setRetainInstance(true);
        // ...

        Bundle args = getArguments();
        if(args != null) {
            sectionId = args.getLong(ARG_SECTION_ID);
            sectionTitle = args.getString(ARG_SECTION_TITLE);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
//        TextView textView = new TextView(getActivity());
//        textView.setText(R.string.hello_blank_fragment);
//        return textView;
        return null;
    }


    // temporary
    private static final int MENU_ITEM_HELP = 113795;

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        // TBD: Put the help menu at the bottom? How?
        // TBD: Put the menu label ("help") in the resource file.
        MenuItem item1 = menu.add(Menu.NONE, MENU_ITEM_HELP, Menu.NONE, "Help");
        // item1.setIcon(android.R.drawable.ic_menu_help);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch(item.getItemId()) {
            case MENU_ITEM_HELP:
                showHelp();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showHelp()
    {
        boolean suc = HelpManager.getInstance(getActivity()).showHelp(getActivity(), sectionId, sectionTitle);
        // ToastHelper.getInstance(getActivity()).showToast("suc = " + suc);
        if(Log.I) Log.i("showHelp(). suc = " + suc);
    }

}

package ms.airy.lib.core.startup;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import java.util.List;

import ms.airy.lib.core.startup.job.StartupJob;
import ms.airy.lib.core.startup.prefs.StartupPrefsStoreManager;
import ms.airy.lib.core.startup.registry.JobRegistryManager;
import ms.airy.lib.core.startup.registry.StartupJobRegistry;
import ms.airy.lib.core.startup.task.BackgroundStartupTask;
import ms.airy.lib.core.startup.task.ForegroundStartupTask;
import ms.airy.lib.core.startup.task.StartupTask;
import ms.airy.lib.core.app.VersionManager;


// TBD:
public final class StartupManager
{
    private static StartupManager INSTANCE = null;
    public static StartupManager getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new StartupManager(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;

    // init() should be called before StartupManager can be used.
    private boolean initialized;

    private StartupManager(Context context)
    {
        this.context = context;
        this.initialized = false;
    }

    // TBD:
    // Instead of this explicit initialization,
    // set startupJobRegistry using config, etc. in JobRegistryManager ??
    public void initialize(StartupJobRegistry startupJobRegistry)
    {
        JobRegistryManager.getInstance(context).setStartupJobRegistry(startupJobRegistry);
        this.initialized = true;
    }


    // TBD:
    // These methods are really messy...
    // The way they are implemented (here and in StartupPrefsStoreManager),
    // these methods may not return consistent results...
    // In particular, the getters and isXXX() methods may not be idempotent....
    // ...

    public boolean isFirstStartup()
    {
        return isFirstStartup(false);
    }
    public boolean isFirstStartup(boolean updateStartupTime)
    {
        boolean isFirstStartup = StartupPrefsStoreManager.getInstance(context).isFirstStartup();

        // temporary
        // When is the best time to call this ???
        // By who????
        if(updateStartupTime) {
            StartupPrefsStoreManager.getInstance(context).setLastStartupTime();
        }
        // temporary

        return isFirstStartup;
    }

    public void updateLastStartupTime()
    {
        StartupPrefsStoreManager.getInstance(context).setLastStartupTime();
    }

    public boolean isFirstStartupForVersion()
    {
        return isFirstStartupForVersion(false);
    }
    public boolean isFirstStartupForVersion(boolean updateStartupCounter)
    {
        int versionCode = VersionManager.getInstance(context).getVersionCode();
        return isFirstStartupForVersion(versionCode, updateStartupCounter);
    }
    private boolean isFirstStartupForVersion(int versionCode)
    {
        return isFirstStartupForVersion(versionCode, false);
    }
    private boolean isFirstStartupForVersion(int versionCode, boolean updateStartupCounter)
    {
        boolean isFirstStartup = StartupPrefsStoreManager.getInstance(context).isFirstStartupForVersion(versionCode);

        // temporary
        // When is the best time to call this ???
        if(updateStartupCounter) {
            StartupPrefsStoreManager.getInstance(context).increaseStartupCounterForVersion(versionCode);
        }
        // temporary

        return isFirstStartup;
    }

    // TBD:
    // To be consistent, use the updateStartupCounter flag for these methods as well????
    public int getStartupCounter()
    {
        int versionCode = VersionManager.getInstance(context).getVersionCode();
        return getStartupCounter(versionCode);
    }
    private int getStartupCounter(int versionCode)
    {
        return StartupPrefsStoreManager.getInstance(context).getStartupCounterForVersion(versionCode);
    }

    public void updateStartupCounter()
    {
        int versionCode = VersionManager.getInstance(context).getVersionCode();
        updateStartupCounter(versionCode);
    }
    public void updateStartupCounter(int versionCode)
    {
        StartupPrefsStoreManager.getInstance(context).increaseStartupCounterForVersion(versionCode);
    }



    // How to call these methods????
    // Do this when an app is opened (e.g., by the user) for the first time??
    // or,
    // Do this at bootup???
    // Use "android.intent.action.BOOT_COMPLETED" ??? on BroadcaseReceiver???
    // <uses-permission android:name="android.permission.RECEIVE_BOOT_COMPLETED" />
    // ...
    // <action android:name="android.intent.action.BOOT_COMPLETED" />
    // <action android:name="android.intent.action.QUICKBOOT_POWERON" />
    // ....



    // TBD:
    public void doStartupProcessing(Activity activity)
    {
        if(initialized == false) {
            Log.w("StartupManager has not been initialized. Cannot do startup task processing.");
            return;
        }

        // TBD: Foreground tasks first or background tasks first??
        doForegroundProcessing(activity);
        doBackgroundProcessing();
    }
    private void doForegroundProcessing(Activity activity)
    {
        List<StartupTask> tasks = getForegroundStartupTasks();
        if(tasks != null && !tasks.isEmpty()) {
            Log.w("Start processsing " + tasks.size() + " foreground startup tasks.");
            for(StartupTask task : tasks) {

                // ???
                if(Log.I) Log.i("Running foreground task: " + task.getId());
                if(task instanceof ForegroundStartupTask) {
                    ((ForegroundStartupTask) task).run(activity);
                } else {
                    // Note that "background task" can be still run on foreground.
                    ((BackgroundStartupTask) task).run();
                }

            }
        } else {
            Log.i("No foreground startup tasks found.");
        }
    }
    private void doBackgroundProcessing()
    {
        List<StartupTask> tasks = getBackgroundStartupTasks();
        if(tasks != null && !tasks.isEmpty()) {
            Log.w("Start processsing " + tasks.size() + " background startup tasks.");
            for(StartupTask task : tasks) {
                if(! (task instanceof BackgroundStartupTask)) {
                    // This cannot happen...
                    Log.w("doBackgroundProcessing(). Task is not a BackgroundStartupTask.");
                    continue;
                }

                // ???
                AsyncTask at = new AsyncTask<StartupTask, Void, Void>() {
                    @Override
                    protected Void doInBackground(StartupTask... params)
                    {
                        StartupTask st = params[0];
                        if(Log.I) Log.i("Running background task: " + st.getId());

                        ((BackgroundStartupTask) st).run();
                        return null;
                    }
                };
                at.execute(task);

            }
        } else {
            Log.i("No background startup tasks found.");
        }
    }


    // Returns the tasks that requires processing...
    protected List<StartupTask> getStartupTasks()
    {
        return JobRegistryManager.getInstance(context).getStartupTasks();
    }
    protected List<StartupTask> getForegroundStartupTasks()
    {
        return JobRegistryManager.getInstance(context).getForegroundStartupTasks();
    }
    protected List<StartupTask> getBackgroundStartupTasks()
    {
        return JobRegistryManager.getInstance(context).getBackgroundStartupTasks();
    }


}

package ms.airy.lib.core.util;

// System-level utils.
// e.g., file locks, etc.
public class SystemUtil
{
    private SystemUtil() {}

    // Used to lock the sqlite database file.
    public static final Object[] sDatabaseFileLock = new Object[0];
    
    // ...
    
}

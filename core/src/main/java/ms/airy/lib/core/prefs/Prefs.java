package ms.airy.lib.core.prefs;



// TBD



public class Prefs
{
	// Pref keys.
	// This value should match those in preferences.xml
	public static final String PREF_KEY_USER_NICKNAME = "Pref.User.Nickname";
    public static final String PREF_KEY_USER_CURRENCY = "Pref.User.Currency";
    public static final String PREF_KEY_USER_TIMEZONE = "Pref.User.Timezone";
    // Timestamp (instead of boolean). Non-zero value means Eula has been accepted.
    public static final String PREF_KEY_USER_EULA_ACCEPTED = "Pref.User.EulaAccepted";
    public static final String PREF_KEY_USER_ = "Pref.User.";

    // ???
	public static final String PREF_KEY_LOCATION_CITY = "Pref.Location.City";
	public static final String PREF_KEY_LOCATION_STATE = "Pref.Location.State";
	public static final String PREF_KEY_LOCATION_ZIP = "Pref.Location.Zip";
	public static final String PREF_KEY_LOCATION_COUNTRY = "Pref.Location.Country";
    public static final String PREF_KEY_LOCATION_GEOCODE = "Pref.Location.Geocode";
	// ...
	
    // Disable network connection
    public static final String PREF_KEY_APP_LOCAL_MODE = "Pref.App.LocalMode";
    public static final String PREF_KEY_APP_USE_LOCATION = "Pref.App.UseLocation";
    public static final String PREF_KEY_APP_USE_GPS = "Pref.App.UseGPS";
    public static final String PREF_KEY_APP_ENABLE_ENCRYPTION = "Pref.App.EnableEncryption";
    public static final String PREF_KEY_APP_ENABLE_COMPRESSION = "Pref.App.EnableCompression";
    public static final String PREF_KEY_APP_ENCRYPT = "Pref.App.Encrypt";
    public static final String PREF_KEY_APP_COMPRESSION = "Pref.App.Compression";

    public static final String PREF_KEY_APP_SKIP_INTRO = "Pref.App.SkipIntro";
    public static final String PREF_KEY_APP_CONFIRM_EXIT = "Pref.App.ConfirmExit";
    public static final String PREF_KEY_APP_CONFIRM_DELETE = "Pref.App.ConfirmDelete";
    public static final String PREF_KEY_APP_CONFIRM_CANCEL = "Pref.App.ConfirmCancel";
    // "Home" page. (e.g., Main view, last edited memo, memo list, etc....)
    //     e.g., "Open last viewed memo", "Open last viewed folder" "Open last viewed project", ...
    //           "Always open "scratchpaper note", ....
    //     Text memo list, List memo list, Audio memo list, etc... 
    public static final String PREF_KEY_APP_FIRST_SCREEN = "Pref.App.FirstScreen";
    // Whether to save a memo when "back" is pressed, or discard it. Or, just leave it "open"??? (<- is it possible?)
    // "no action", "save", "discard", "confirm".
    public static final String PREF_KEY_APP_BACK_ACTION = "Pref.App.BackAction";
    // TBD: Default folder. (Store both guid and name for performance reasons?)
    // When a new memo is added from the top level memo list, where to add the memo....
    public static final String PREF_KEY_APP_DEFAULT_FOLDER = "Pref.App.DefaultFolder";
    // default external dir (under /mnt/sdcard/...)
    public static final String PREF_KEY_APP_DEFAULT_DIRECTORY = "Pref.App.DefaultDirectory";
    // TBD: Whether to use the last used directory (instead of the default one)
    // Also, whether to remember the last opened file, and the file name, etc.
    // Default permission. (Read-write, read-only, hidden in the list, etc.)
    public static final String PREF_KEY_APP_DEFAULT_FLAG = "Pref.App.DefaultFlag";
    // TBD:
    // whether to show on-screen menus.
    // whether to automatically import external files.
    // ...

    
    // TBD: Give the user an option to remove/clean all memo data???
    // ...
    
    // Whether to use memo-specific settings (except for the theme-related settings).
    public static final String PREF_KEY_APP_ALLOW_MEMO_SETTING = "Pref.App.AllowMemoSetting";

    public static final String PREF_KEY_APP_DEFAULT_THEME = "Pref.App.DefaultTheme";
    public static final String PREF_KEY_APP_ALLOW_MEMO_THEME = "Pref.App.AllowMemoTheme";
    public static final String PREF_KEY_APP_ALLOW_LIST_THEME = "Pref.App.AllowListTheme";
    public static final String PREF_KEY_APP_ALLOW_FOLDER_THEME = "Pref.App.AllowFolderTheme";

    public static final String PREF_KEY_APP_USE_TRASH_CAN = "Pref.App.UseTrashCan";
    public static final String PREF_KEY_APP_USE_TEXT_MEMO = "Pref.App.UseTextMemo";
    public static final String PREF_KEY_APP_USE_LIST_MEMO = "Pref.App.UseListMemo";
    public static final String PREF_KEY_APP_USE_DRAWING_MEMO = "Pref.App.UseDrawingMemo";
    public static final String PREF_KEY_APP_USE_IMAGE_MEMO = "Pref.App.UseImageMemo";
    public static final String PREF_KEY_APP_USE_AUDIO_MEMO = "Pref.App.UseAudioMemo";
    public static final String PREF_KEY_APP_USE_VIDEO_MEMO = "Pref.App.UseVideoMemo";
    public static final String PREF_KEY_APP_USE_TEMPLATE = "Pref.App.UseTemplate";
    public static final String PREF_KEY_APP_USE_LABEL = "Pref.App.UseLabel";
    public static final String PREF_KEY_APP_USE_CLIPBOARD = "Pref.App.UseClipboard";
    public static final String PREF_KEY_APP_USE_ALERT_POPUP = "Pref.App.UseAlertPopup";
    public static final String PREF_KEY_APP_USE_NOTIFICATION = "Pref.App.UseNotificaiton";
    public static final String PREF_KEY_APP_USE_ALARM = "Pref.App.UseAlarm";
    //public static final String PREF_KEY_APP_USE_EMAIL_ALERT = "Pref.App.UseEmailAlert";  // email account?
    //public static final String PREF_KEY_APP_USE_SMS_ALERT = "Pref.App.UseSmsAlert";
    public static final String PREF_KEY_APP_ENABLE_SHARING = "Pref.App.EnableSharing";
    public static final String PREF_KEY_APP_ENABLE_EXPORT = "Pref.App.EnableExport";

    // TBD: Backup vs. Export???
    public static final String PREF_KEY_APP_ENABLE_BACKUP = "Pref.App.EnableBackup";
    // Backup targets/Media? (e.g., sdcard, server, pc via usb/bluetooth, etc. ...)
    public static final String PREF_KEY_APP_BACKUP_MEDIA = "Pref.App.BackupMedia";
    public static final String PREF_KEY_APP_AUTO_BACKUP = "Pref.App.AutoBackup";
    public static final String PREF_KEY_APP_BACKUP_INTERVAL = "Pref.App.BackupInterval"; // ???
    public static final String PREF_KEY_APP_BACKUP_TIME = "Pref.App.BackupTime";  // ????
    // TBD: backup configurations such as Backup server, SD card directory, PC bluetooth setting, pc target directory, etc.
    // Also, how many copies to keep (for each target), etc.
    // ...

    // Error feedback (e.g., in case of FC, etc.)
    public static final String PREF_KEY_APP_ENABLE_FEEDBACK = "Pref.App.EnableFeedback";
    // Enable feature usage logging.
    public static final String PREF_KEY_APP_ENABLE_USAGE_LOGGING = "Pref.App.EnableUsageLogging";
    public static final String PREF_KEY_APP_AUTO_USAGE_REPORT = "Pref.App.AutoUsageReport";
    public static final String PREF_KEY_APP_USAGE_REPORT_INTERVAL = "Pref.App.UsageReportInterval";  // ???
    public static final String PREF_KEY_APP_USAGE_REPORT_TIME = "Pref.App.UsageReportTime";  // ???
    public static final String PREF_KEY_APP_ = "Pref.App.";
	
    
    // TBD: "MemoWidget", "MemoFolder" settings???
    // .....
    
    // TBD: File-related settings?
    // ....
    // whether to "hide" by default the file-linked memos in the memo list
    // ....
	
    // TBD: whether to show an info page?
    //      for each object type. memo, folder, template, etc....
    
    public static final String PREF_KEY_MEMO_DEFAULT_MIME = "Pref.Memo.DefaultMime";
    public static final String PREF_KEY_MEMO_DEFAULT_FLAG = "Pref.Memo.DefaultFlag";
    public static final String PREF_KEY_MEMO_USE_UNDO_SERVICE = "Pref.Memo.UseUndoService";
    public static final String PREF_KEY_MEMO_USE_TIME_TAG = "Pref.Memo.UseTimeTag";
    public static final String PREF_KEY_MEMO_USE_LOCATION_TAG = "Pref.Memo.UseLocationTag";
    public static final String PREF_KEY_MEMO_AUTO_EXPIRATION = "Pref.Memo.AutoExpiration";
    
    //public static final String PREF_KEY_MEMO_DEFAULT_FILE_TYPE = "Pref.Memo.DefaultFileType";
    public static final String PREF_KEY_MEMO_DEFAULT_VIEW_TYPE = "Pref.Memo.DefaultViewType";
    public static final String PREF_KEY_MEMO_CONFIRM_DELETE = "Pref.Memo.ConfirmDelete";
    public static final String PREF_KEY_MEMO_CONFIRM_CANCEL = "Pref.Memo.ConfirmCancel";

    public static final String PREF_KEY_MEMO_SORT_FIELD = "Pref.Memo.SortField";
    public static final String PREF_KEY_MEMO_SORT_ORDER = "Pref.Memo.SortOrder";
    //public static final String PREF_KEY_MEMO_SORT_USE_LAST = "Pref.Memo.SortUseLast";
    public static final String PREF_KEY_MEMO_ENABLE_DYNAMIC_VIEW = "Pref.Memo.EnableDynamicView";
    public static final String PREF_KEY_MEMO_EDIT_MODE = "Pref.Memo.EditMode";
    public static final String PREF_KEY_MEMO_DEFAULT_FILTER = "Pref.Memo.DefaultFilter";  // ????
    //public static final String PREF_KEY_MEMO_FILTER_USE_LAST = "Pref.Memo.FilterUseLast";
    public static final String PREF_KEY_MEMO_DEFAULT_THEME = "Pref.Memo.DefaultTheme";
    public static final String PREF_KEY_MEMO_FULLSCREEN = "Pref.Memo.FullScreen";
    //public static final String PREF_KEY_MEMO_PAGINATION = "Pref.Memo.Pagination";
    public static final String PREF_KEY_MEMO_PAGE_MAX_LINES = "Pref.Memo.PageMaxLines";
    public static final String PREF_KEY_MEMO_LINKIFY = "Pref.Memo.Linkify";
    public static final String PREF_KEY_MEMO_ENCRYPT = "Pref.Memo.Encrypt";
    public static final String PREF_KEY_MEMO_COMPRESSION = "Pref.Memo.Compression";
    public static final String PREF_KEY_MEMO_SAVE_CURSOR_POS = "Pref.Memo.SaveCursorPos";
    //public static final String PREF_KEY_MEMO_MAX_LENGTH = "Pref.Memo.MaxLength";
    public static final String PREF_KEY_MEMO_ENABLE_ALERT = "Pref.Memo.EnableAlert";
    public static final String PREF_KEY_MEMO_USE_ALERT_POPUP = "Pref.Memo.UseAlertPopup";
    public static final String PREF_KEY_MEMO_USE_NOTIFICATION = "Pref.Memo.UseNotificaiton";
    public static final String PREF_KEY_MEMO_USE_ALARM = "Pref.Memo.UseAlarm";
    //public static final String PREF_KEY_MEMO_USE_EMAIL_ALERT = "Pref.Memo.UseEmailAlert";  // email account?
    //public static final String PREF_KEY_MEMO_USE_SMS_ALERT = "Pref.Memo.UseSmsAlert";
    public static final String PREF_KEY_MEMO_ = "Pref.Memo.";


    public static final String PREF_KEY_LIST_DEFAULT_TYPE = "Pref.List.DefaultType";
    public static final String PREF_KEY_LIST_DEFAULT_FLAG = "Pref.List.DefaultFlag";
    public static final String PREF_KEY_LIST_SORT_FIELD = "Pref.List.SortField";
    public static final String PREF_KEY_LIST_SORT_ORDER = "Pref.List.SortOrder";
    //public static final String PREF_KEY_LIST_SORT_USE_LAST = "Pref.List.SortUseLast";
    public static final String PREF_KEY_LIST_DEFAULT_FILTER = "Pref.List.DefaultFilter";  // ????
    //public static final String PREF_KEY_LIST_FILTER_USE_LAST = "Pref.List.FilterUseLast";
    public static final String PREF_KEY_LIST_DEFAULT_THEME = "Pref.List.DefaultTheme";
    public static final String PREF_KEY_LIST_FULLSCREEN = "Pref.List.FullScreen";
    public static final String PREF_KEY_LIST_MAX_ITEM_COUNT = "Pref.List.MaxItemCount";
    public static final String PREF_KEY_LIST_MULTILINE_ITEM = "Pref.List.MultilineItem";
    public static final String PREF_KEY_LIST_ENABLE_ITEM_ALERT = "Pref.List.EnableItemAlert";
    public static final String PREF_KEY_LIST_USE_ALERT_POPUP = "Pref.List.UseAlertPopup";
    public static final String PREF_KEY_LIST_USE_NOTIFICATION = "Pref.List.UseNotificaiton";
    public static final String PREF_KEY_LIST_USE_ALARM = "Pref.List.UseAlarm";
    //public static final String PREF_KEY_LIST_USE_EMAIL_ALERT = "Pref.List.UseEmailAlert";  // email account?
    //public static final String PREF_KEY_LIST_USE_SMS_ALERT = "Pref.List.UseSmsAlert";
    public static final String PREF_KEY_LIST_ = "Pref.List.";
    

    // boolean (true: "enable"/"confirm", false: "disable").
    public static final String PREF_KEY_FOLDER_DELETE_NON_EMPTY = "Pref.Folder.DeleteNonEmpty";
    public static final String PREF_KEY_FOLDER_DEFAULT_FLAG = "Pref.Folder.DefaultFlag";
    public static final String PREF_KEY_FOLDER_SORT_FIELD = "Pref.Folder.SortField";
    public static final String PREF_KEY_FOLDER_SORT_ORDER = "Pref.Folder.SortOrder";
    //public static final String PREF_KEY_FOLDER_SORT_USE_LAST = "Pref.Folder.SortUseLast";
    public static final String PREF_KEY_FOLDER_DEFAULT_FILTER = "Pref.Folder.DefaultFilter";  // ????
    //public static final String PREF_KEY_FOLDER_FILTER_USE_LAST = "Pref.Folder.FilterUseLast";
    public static final String PREF_KEY_FOLDER_DEFAULT_THEME = "Pref.Folder.DefaultTheme";
    public static final String PREF_KEY_FOLDER_FULLSCREEN = "Pref.Folder.FullScreen";
    //public static final String PREF_KEY_FOLDER_PAGINATION = "Pref.Folder.Pagination";
    public static final String PREF_KEY_FOLDER_PAGE_MAX_LINES = "Pref.Folder.PageMaxLines";
    public static final String PREF_KEY_FOLDER_LINKIFY = "Pref.Folder.Linkify";
    public static final String PREF_KEY_FOLDER_ENCRYPT = "Pref.Folder.Encrypt";
    public static final String PREF_KEY_FOLDER_COMPRESSION = "Pref.Folder.Compression";
    public static final String PREF_KEY_FOLDER_TREE_DEPTH = "Pref.Folder.TreeDepth";
    public static final String PREF_KEY_FOLDER_INDENTATION = "Pref.Folder.Indentation";
    public static final String PREF_KEY_FOLDER_ = "Pref.Folder.";
    // TBD: Use a full path vs. name only.
    // ....
    

    public static final String PREF_KEY_PROJECT_INCLUDE_FOLDERS = "Pref.Project.IncludeFolders";
    //public static final String PREF_KEY_PROJECT_INCLUDE_PROJECTS = "Pref.Project.IncludeProjects";
    public static final String PREF_KEY_PROJECT_DEFAULT_FLAG = "Pref.Project.DefaultFlag";
    public static final String PREF_KEY_PROJECT_SORT_FIELD = "Pref.Project.SortField";
    public static final String PREF_KEY_PROJECT_SORT_ORDER = "Pref.Project.SortOrder";
    //public static final String PREF_KEY_PROJECT_SORT_USE_LAST = "Pref.Project.SortUseLast";
    public static final String PREF_KEY_PROJECT_DEFAULT_FILTER = "Pref.Project.DefaultFilter";  // ????
    //public static final String PREF_KEY_PROJECT_FILTER_USE_LAST = "Pref.Project.FilterUseLast";
    public static final String PREF_KEY_PROJECT_MAX_ENTRY_COUNT = "Pref.Project.MaxEntryCount";
    public static final String PREF_KEY_PROJECT_ENABLE_ALERT = "Pref.Project.EnableAlert";
    public static final String PREF_KEY_PROJECT_USE_ALERT_POPUP = "Pref.Project.UseAlertPopup";
    public static final String PREF_KEY_PROJECT_USE_NOTIFICATION = "Pref.Project.UseNotificaiton";
    public static final String PREF_KEY_PROJECT_USE_ALARM = "Pref.Project.UseAlarm";
    //public static final String PREF_KEY_PROJECT_USE_EMAIL_ALERT = "Pref.Project.UseEmailAlert";  // email account?
    //public static final String PREF_KEY_PROJECT_USE_SMS_ALERT = "Pref.Project.UseSmsAlert";
    public static final String PREF_KEY_PROJECT_ = "Pref.Project.";


    // Alow user custom templates.
    public static final String PREF_KEY_TEMPLATE_ALLOW_CUSTOM = "Pref.Template.AllowCustom";
    public static final String PREF_KEY_TEMPLATE_DEFAULT_FLAG = "Pref.Template.DefaultFlag";
    public static final String PREF_KEY_TEMPLATE_SORT_FIELD = "Pref.Template.SortField";
    public static final String PREF_KEY_TEMPLATE_SORT_ORDER = "Pref.Template.SortOrder";
    //public static final String PREF_KEY_TEMPLATE_SORT_USE_LAST = "Pref.Template.SortUseLast";
    public static final String PREF_KEY_TEMPLATE_DEFAULT_FILTER = "Pref.Template.DefaultFilter";  // ????
    //public static final String PREF_KEY_TEMPLATE_FILTER_USE_LAST = "Pref.Template.FilterUseLast";
    // "Primary" uses, primary templates, ...
    //      (e.g., simple note, check list, shopping list, ...)
    //      (primary view: list or note ...)    
    public static final String PREF_KEY_TEMPLATE_DEFAULT_TEXT_MEMO = "Pref.Template.DefaultTextMemo";
    public static final String PREF_KEY_TEMPLATE_DEFAULT_LIST_MEMO = "Pref.Template.DefaultListMemo";
    public static final String PREF_KEY_TEMPLATE_DEFAULT_DRAWING_MEMO = "Pref.Template.DefaultDrawingMemo";
    public static final String PREF_KEY_TEMPLATE_DEFAULT_IMAGE_MEMO = "Pref.Template.DefaultImageMemo";
    public static final String PREF_KEY_TEMPLATE_DEFAULT_AUDIO_MEMO = "Pref.Template.DefaultAudioMemo";
    public static final String PREF_KEY_TEMPLATE_DEFAULT_VIDEO_MEMO = "Pref.Template.DefaultVideoMemo";
    public static final String PREF_KEY_TEMPLATE_ = "Pref.Template.";

    // Favortes/Alerts
    public static final String PREF_KEY_FAVORITE_DEFAULT_FILTER = "Pref.Favorite.DefaultFilter";
    public static final String PREF_KEY_ALERT_DEFAULT_FILTER = "Pref.Alert.DefaultFilter";

    
    // TBD:
	// Use multiple sheets (mobile-only sheet, universal sheet, etc.) ???
    // Use tab screen.
	// Export to tweet/blog/gdocs/...
    // Configure blog, twitter accounts, etc...
	// ...
    // "Shake undo"
    // gesture-based editing.
    // "alarm server"
    // alert sound/ringtone, vibrate, interval, ... -> Use the Alarm clock service???
    // ...
    	
    
    // TBD:
    // Menu customization.
    // For MemoridMain and all other activities
    // ....
    public static final String PREF_KEY_MENU_MAIN_ = "Pref.Menu.Main.";
    public static final String PREF_KEY_MENU_MEMO_ = "Pref.Menu.Memo.";
    // ....
    
    // TBD:
    // List column customization?
    // for each list types (memo list, folder list, etc.)
    // ...
    
    // TBD:
    // Search related options.
    // e.g., which db to search (memos, projects, etc.)
    // ...
    
    
    // TBD:
    // section for cleaning up data?
    // ...
    
    
	
	// Default values.
    // TBD: Use config/xml files? (e.g., in addition to these hard-coded values)
    public static final String PREF_DEFAULT_USER_NICKNAME = "";
    //public static final String PREF_DEFAULT_USER_NICKNAME = createRandomNickname(); // ???
    public static final String PREF_DEFAULT_USER_CURRENCY = "";
    public static final String PREF_DEFAULT_USER_TIMEZONE = "";
    public static final Long PREF_DEFAULT_USER_EULA_ACCEPTED = 0L;  // timestamp.
    public static final String PREF_DEFAULT_LOCATION_CITY = "";
    public static final String PREF_DEFAULT_LOCATION_STATE = "";
    public static final String PREF_DEFAULT_LOCATION_ZIP = "";
    public static final String PREF_DEFAULT_LOCATION_COUNTRY = "";
    public static final String PREF_DEFAULT_LOCATION_GEOCODE = "";

    public static final Boolean PREF_DEFAULT_APP_LOCAL_MODE = false;
    public static final Boolean PREF_DEFAULT_APP_USE_LOCATION = false;
    public static final Boolean PREF_DEFAULT_APP_USE_GPS = false;
    public static final Boolean PREF_DEFAULT_APP_ENABLE_ENCRYPTION = false;
    public static final Boolean PREF_DEFAULT_APP_ENABLE_COMPRESSION = false;
    public static final Boolean PREF_DEFAULT_APP_ENCRYPT = false;
    public static final Boolean PREF_DEFAULT_APP_COMPRESSION = false;
    public static final Boolean PREF_DEFAULT_APP_SKIP_INTRO = false;
    public static final Boolean PREF_DEFAULT_APP_CONFIRM_EXIT = false;
    public static final Boolean PREF_DEFAULT_APP_CONFIRM_DELETE = true;
    public static final Boolean PREF_DEFAULT_APP_CONFIRM_CANCEL = false;
    public static final String PREF_DEFAULT_APP_FIRST_SCREEN = "";
    public static final String PREF_DEFAULT_APP_BACK_ACTION = "";
    public static final String PREF_DEFAULT_APP_DEFAULT_FOLDER = "";
    public static final String PREF_DEFAULT_APP_DEFAULT_DIRECTORY = "";
    public static final Long PREF_DEFAULT_APP_DEFAULT_FLAG = 0L; // ???
    public static final Boolean PREF_DEFAULT_APP_ALLOW_MEMO_SETTING = true;
    public static final String PREF_DEFAULT_APP_DEFAULT_THEME = "";
    public static final Boolean PREF_DEFAULT_APP_ALLOW_MEMO_THEME = true;
    public static final Boolean PREF_DEFAULT_APP_ALLOW_LIST_THEME = true;
    public static final Boolean PREF_DEFAULT_APP_ALLOW_FOLDER_THEME = true;
    public static final Boolean PREF_DEFAULT_APP_USE_TRASH_CAN = false;
    public static final Boolean PREF_DEFAULT_APP_USE_TEXT_MEMO = true;
    public static final Boolean PREF_DEFAULT_APP_USE_LIST_MEMO = true;
    public static final Boolean PREF_DEFAULT_APP_USE_DRAWING_MEMO = true;
    public static final Boolean PREF_DEFAULT_APP_USE_IMAGE_MEMO = true;
    public static final Boolean PREF_DEFAULT_APP_USE_AUDIO_MEMO = true;
    public static final Boolean PREF_DEFAULT_APP_USE_VIDEO_MEMO = false;
    public static final Boolean PREF_DEFAULT_APP_USE_TEMPLATE = true;
    public static final Boolean PREF_DEFAULT_APP_USE_LABEL = true;
    public static final Boolean PREF_DEFAULT_APP_USE_CLIPBOARD = true;
    public static final Boolean PREF_DEFAULT_APP_USE_ALERT_POPUP = false;
    public static final Boolean PREF_DEFAULT_APP_USE_NOTIFICATION = true;
    public static final Boolean PREF_DEFAULT_APP_USE_ALARM = false;
    public static final Boolean PREF_DEFAULT_APP_ENABLE_SHARING = true;
    public static final Boolean PREF_DEFAULT_APP_ENABLE_EXPORT = false;
    public static final Boolean PREF_DEFAULT_APP_ENABLE_BACKUP = true;
    public static final Boolean PREF_DEFAULT_APP_AUTO_BACKUP = false;
    public static final String PREF_DEFAULT_APP_BACKUP_INTERVAL = ""; // ???
    public static final String PREF_DEFAULT_APP_BACKUP_MEDIA = "";
    public static final String PREF_DEFAULT_APP_BACKUP_TIME = "";  // ????
    public static final Boolean PREF_DEFAULT_APP_ENABLE_FEEDBACK = true;    
    public static final Boolean PREF_DEFAULT_APP_ENABLE_USAGE_LOGGING = false;
    public static final Boolean PREF_DEFAULT_APP_AUTO_USAGE_REPORT = false;
    public static final String PREF_DEFAULT_APP_USAGE_REPORT_INTERVAL = "";  // ???
    public static final String PREF_DEFAULT_APP_USAGE_REPORT_TIME = "";  // ???

    public static final String PREF_DEFAULT_MEMO_DEFAULT_MIME = "";
    public static final Long PREF_DEFAULT_MEMO_DEFAULT_FLAG = 0L; // ???
    public static final Boolean PREF_DEFAULT_MEMO_USE_UNDO_SERVICE = true;
    public static final Boolean PREF_DEFAULT_MEMO_USE_TIME_TAG = true;
    public static final Boolean PREF_DEFAULT_MEMO_USE_LOCATION_TAG = true;
    public static final Boolean PREF_DEFAULT_MEMO_AUTO_EXPIRATION = true;
    public static final String PREF_DEFAULT_MEMO_DEFAULT_VIEW_TYPE = "";
    public static final Boolean PREF_DEFAULT_MEMO_CONFIRM_DELETE = true;
    public static final Boolean PREF_DEFAULT_MEMO_CONFIRM_CANCEL = false;
    public static final String PREF_DEFAULT_MEMO_SORT_FIELD = "";
    public static final String PREF_DEFAULT_MEMO_SORT_ORDER = "";
    public static final Boolean PREF_DEFAULT_MEMO_ENABLE_DYNAMIC_VIEW = false;
    public static final String PREF_DEFAULT_MEMO_EDIT_MODE = "";
    public static final String PREF_DEFAULT_MEMO_DEFAULT_FILTER = "";  // ????
    public static final String PREF_DEFAULT_MEMO_DEFAULT_THEME = "";
    public static final Boolean PREF_DEFAULT_MEMO_FULLSCREEN = false;
    //public static final Boolean PREF_DEFAULT_MEMO_PAGINATION = false;
    public static final Integer PREF_DEFAULT_MEMO_PAGE_MAX_LINES = 0;
    public static final Boolean PREF_DEFAULT_MEMO_LINKIFY = false;
    public static final Boolean PREF_DEFAULT_MEMO_ENCRYPT = false;
    public static final Boolean PREF_DEFAULT_MEMO_COMPRESSION = false;
    public static final Boolean PREF_DEFAULT_MEMO_SAVE_CURSOR_POS = false;
    //public static final Integer PREF_DEFAULT_MEMO_MAX_LENGTH = 0;
    public static final Boolean PREF_DEFAULT_MEMO_ENABLE_ALERT = true;
    public static final Boolean PREF_DEFAULT_MEMO_USE_ALERT_POPUP = false;
    public static final Boolean PREF_DEFAULT_MEMO_USE_NOTIFICATION = true;
    public static final Boolean PREF_DEFAULT_MEMO_USE_ALARM = false;

    public static final String PREF_DEFAULT_LIST_DEFAULT_TYPE = "";
    public static final Long PREF_DEFAULT_LIST_DEFAULT_FLAG = 0L; // ???
    public static final String PREF_DEFAULT_LIST_SORT_FIELD = "";
    public static final String PREF_DEFAULT_LIST_SORT_ORDER = "";
    public static final String PREF_DEFAULT_LIST_DEFAULT_FILTER = "";  // ????
    public static final String PREF_DEFAULT_LIST_DEFAULT_THEME = "";
    public static final Boolean PREF_DEFAULT_LIST_FULLSCREEN = false;
    public static final Integer PREF_DEFAULT_LIST_MAX_ITEM_COUNT = 0;
    public static final Boolean PREF_DEFAULT_LIST_MULTILINE_ITEM = true;
    public static final Boolean PREF_DEFAULT_LIST_ENABLE_ITEM_ALERT = true;
    public static final Boolean PREF_DEFAULT_LIST_USE_ALERT_POPUP = false;
    public static final Boolean PREF_DEFAULT_LIST_USE_NOTIFICATION = true;
    public static final Boolean PREF_DEFAULT_LIST_USE_ALARM = false;

    public static final Boolean PREF_DEFAULT_FOLDER_DELETE_NON_EMPTY = false;
    public static final Long PREF_DEFAULT_FOLDER_DEFAULT_FLAG = 0L; // ???
    public static final String PREF_DEFAULT_FOLDER_SORT_FIELD = "";
    public static final String PREF_DEFAULT_FOLDER_SORT_ORDER = "";
    public static final String PREF_DEFAULT_FOLDER_DEFAULT_FILTER = "";  // ????
    public static final String PREF_DEFAULT_FOLDER_DEFAULT_THEME = "";
    public static final Boolean PREF_DEFAULT_FOLDER_FULLSCREEN = false;
    //public static final Boolean PREF_DEFAULT_FOLDER_PAGINATION = false;
    public static final Integer PREF_DEFAULT_FOLDER_PAGE_MAX_LINES = 0;
    public static final Boolean PREF_DEFAULT_FOLDER_LINKIFY = false;
    public static final Boolean PREF_DEFAULT_FOLDER_ENCRYPT = false;
    public static final Boolean PREF_DEFAULT_FOLDER_COMPRESSION = false;
    public static final Integer PREF_DEFAULT_FOLDER_TREE_DEPTH = 1;
    public static final String PREF_DEFAULT_FOLDER_INDENTATION = "";

    public static final Boolean PREF_DEFAULT_PROJECT_INCLUDE_FOLDERS = false;
    public static final Long PREF_DEFAULT_PROJECT_DEFAULT_FLAG = 0L; // ???
    public static final String PREF_DEFAULT_PROJECT_SORT_FIELD = "";
    public static final String PREF_DEFAULT_PROJECT_SORT_ORDER = "";
    public static final String PREF_DEFAULT_PROJECT_DEFAULT_FILTER = "";  // ????
    public static final Integer PREF_DEFAULT_PROJECT_MAX_ENTRY_COUNT = 0;
    public static final Boolean PREF_DEFAULT_PROJECT_ENABLE_ALERT = true;
    public static final Boolean PREF_DEFAULT_PROJECT_USE_ALERT_POPUP = false;
    public static final Boolean PREF_DEFAULT_PROJECT_USE_NOTIFICATION = true;
    public static final Boolean PREF_DEFAULT_PROJECT_USE_ALARM = false;

    public static final Boolean PREF_DEFAULT_TEMPLATE_ALLOW_CUSTOM = false;
    public static final Long PREF_DEFAULT_TEMPLATE_DEFAULT_FLAG = 0L; // ???
    public static final String PREF_DEFAULT_TEMPLATE_SORT_FIELD = "";
    public static final String PREF_DEFAULT_TEMPLATE_SORT_ORDER = "";
    public static final String PREF_DEFAULT_TEMPLATE_DEFAULT_FILTER = "";  // ????
    public static final String PREF_DEFAULT_TEMPLATE_DEFAULT_TEXT_MEMO = "";
    public static final String PREF_DEFAULT_TEMPLATE_DEFAULT_LIST_MEMO = "";
    public static final String PREF_DEFAULT_TEMPLATE_DEFAULT_DRAWING_MEMO = "";
    public static final String PREF_DEFAULT_TEMPLATE_DEFAULT_IMAGE_MEMO = "";
    public static final String PREF_DEFAULT_TEMPLATE_DEFAULT_AUDIO_MEMO = "";
    public static final String PREF_DEFAULT_TEMPLATE_DEFAULT_VIDEO_MEMO = "";
	
    public static final String PREF_DEFAULT_FAVORITE_DEFAULT_FILTER = "";
    public static final String PREF_DEFAULT_ALERT_DEFAULT_FILTER = "";
	
    
	// TBD: "Private" prefs.
	public static final String APP_KEY_DEVICE_ID = "App.Device.Id";
	public static final String APP_KEY_USER_INSTALL_DATE = "App.User.InstallDate";
	//public static final String APP_KEY_USER_LAST_PLAY_DATE = "App.User.LastPlayDate";
	// Terms of services....
	

	// TBD
	private static String createRandomNickname()
	{
	    // TBD
	    return "";
	}
	
    // No need to directly initialize this class.
    protected Prefs() {}
    
}

package ms.airy.lib.core.analytics.manager;

import java.util.List;

import ms.airy.lib.core.analytics.CustomEventTracker;
import ms.airy.lib.core.analytics.ScreenViewTracker;


public interface AnalyticsManager extends ScreenViewTracker, CustomEventTracker
{
////    ScreenViewTracker getScreenViewTracker();
////    CustomEventTracker getCustomEventTracker();
//    List<ScreenViewTracker> getScreenViewTrackers();
//    List<CustomEventTracker> getCustomEventTrackers();

    boolean isTrackingEnabled();
//    boolean isAmazonEnabled();
//    boolean isGoogleEnabled();

}

package ms.airy.lib.core.invite.client;

import java.util.List;

import ms.airy.lib.core.invite.common.InviteInfo;
import ms.airy.lib.core.invite.common.RecipientInfo;


public interface AsyncInviteClient
{
    void sendInvite(String recipient);
    void getInviteInfo(long inviteId);
    void getInvites(int status);
    void getRecipientInfo(String recipient);
    void getRecipients(int status);
}

package ms.airy.lib.core.util;


// TBD: Do we really need this?
//      Does this really work?
// Looks like, for our use cases, we'll have to use dependency injection.....
public class ConfigFactoryManager
{
    private ConfigFactoryManager() {}

    public static AbstractConfigFactory getConfigFactory()
    {
        // temporary
        return BaseConfigFactory.getInstance();
    }
    
}

package ms.airy.lib.core.help.client.impl;

import ms.airy.lib.core.help.client.OnlineHelpClient;


/**
 */
public class MockOnlineHelpClient extends AbstractOnlineHelpClient implements OnlineHelpClient
{
    public MockOnlineHelpClient()
    {
    }
    public MockOnlineHelpClient(String helpWebServiceEndpoint)
    {
        super(helpWebServiceEndpoint);
    }

    @Override
    public String getSectionContent(long sectionId)
    {
        Log.d("MockOnlineHelpClient.getSectionContent(). sectionId = " + sectionId);

        // temporary
        String content = null;
        if(sectionId == 1L) {
            content = "Section 1 content";
        } else if(sectionId == 2L) {
            content = "Section 2 content";
        } else if(sectionId == 3L) {
            content = "Section 3 content";
        } else if(sectionId < 10L) {
            content = "dummy content";
        } else {
            content = null;
        }

        return content;
    }

}

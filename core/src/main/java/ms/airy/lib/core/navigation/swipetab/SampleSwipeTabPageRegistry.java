package ms.airy.lib.core.navigation.swipetab;

/**
 * Created by harry on 1/30/14.
 */
public class SampleSwipeTabPageRegistry extends AbstractSwipeTabPageRegistry
{
    public SampleSwipeTabPageRegistry()
    {
        super();
    }

    @Override
    protected void init()
    {
        // temporary
        SwipeTabPageInfo info1 = new SwipeTabPageInfo("sec1", "Section 1");
        SwipeTabMainFragment fragment1 = SwipeTabMainFragment.newInstance(1);
        info1.setFragment(fragment1);
        this.pageInfos.add(info1);

        SwipeTabPageInfo info2 = new SwipeTabPageInfo("sec2", "Section 2");
        SwipeTabMainFragment fragment2 = SwipeTabMainFragment.newInstance(2);
        info2.setFragment(fragment2);
        this.pageInfos.add(info2);

    }

}

package ms.airy.lib.core.dialog.base;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.ImageView;


// http://developer.android.com/guide/topics/ui/dialogs.html
// http://developer.android.com/reference/android/app/AlertDialog.html
public class BasicImageDialogFragment extends BaseDialogFragment
{
    private static final String ARG_IMAGE_RESID = "image_dialog_image_resid";


    private ImageView imageView = null;

    public static BasicImageDialogFragment newInstance(CharSequence title, int imageResId)
    {
        BasicImageDialogFragment fragment = new BasicImageDialogFragment();
        addArguments(fragment, title, imageResId);
        return fragment;
    }
    public static BasicImageDialogFragment newInstance(int titleResId, int imageResId)
    {
        BasicImageDialogFragment fragment = new BasicImageDialogFragment();
        addArguments(fragment, titleResId, imageResId);
        return fragment;
    }
    public BasicImageDialogFragment()
    {
    }

    protected static DialogFragment addArguments(DialogFragment fragment, CharSequence title, int imageResId)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        if(title != null && title.length() > 0) {
            args.putCharSequence(ARG_TITLE, title);
        }
        if(imageResId > 0) {
            args.putInt(ARG_IMAGE_RESID, imageResId);
        }
        return fragment;
    }
    protected static DialogFragment addArguments(DialogFragment fragment, int titleResId, int imageResId)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        if(titleResId > 0) {  // ???
            args.putInt(ARG_TITLE_RESID, titleResId);
        }
        if(imageResId > 0) {
            args.putInt(ARG_IMAGE_RESID, imageResId);
        }
        return fragment;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        AlertDialog.Builder dialogBuilder = buildDialogBuilder();
        return dialogBuilder.create();
    }

    @Override
    protected AlertDialog.Builder buildDialogBuilder()
    {
        CharSequence title = getArguments().getCharSequence(ARG_TITLE);
        int titleResId = getArguments().getInt(ARG_TITLE_RESID);
        int imageResId = getArguments().getInt(ARG_IMAGE_RESID);
        if(imageResId > 0) {
            imageView = new ImageView(getActivity());
            imageView.setImageResource(imageResId);
        } else {
            Log.i("Image not set.");
        }

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());

        dialogBuilder.setIcon(android.R.drawable.alert_dark_frame);
        if(title != null && title.length() > 0) {
            dialogBuilder.setTitle(title);
        } else {
            if (titleResId > 0) {  // ???
                dialogBuilder.setTitle(titleResId);
            }
        }
        if(imageView != null) {
            // ????
            dialogBuilder.setView(imageView);

//            FrameLayout fl = (FrameLayout) findViewById(android.R.id.custom);
//            fl.addView(imageView, new LayoutParams(MATCH_PARENT, WRAP_CONTENT));

        }

        return dialogBuilder;
    }



}

package ms.airy.lib.core.help.common;

import java.io.Serializable;


/**
 */
public class HelpSection implements Serializable
{
    // Title is optional.
    private String title;
    // private String format;   // text or html?
    // Content format is always html, for now.
    private String content;


    public HelpSection()
    {
        this(null);
    }
    public HelpSection(String title)
    {
        this(title, null);
    }
    public HelpSection(String title, String content)
    {
        this.title = title;
        this.content = content;
    }

    public String getTitle()
    {
        return title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getContent()
    {
        return content;
    }
    public void setContent(String content)
    {
        this.content = content;
    }


    // For debugging...
    @Override
    public String toString()
    {
        return "HelpSection{" +
                "title='" + title + '\'' +
                ", content='" + content + '\'' +
                '}';
    }

}

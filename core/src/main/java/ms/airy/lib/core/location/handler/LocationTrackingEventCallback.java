package ms.airy.lib.core.location.handler;


// Not being used.
public interface LocationTrackingEventCallback
{
    void startScheduledLocationTracking();
    void cancelScheduledLocationTracking();
}

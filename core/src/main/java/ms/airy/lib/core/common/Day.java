package ms.airy.lib.core.common;

import java.text.SimpleDateFormat;
import java.util.Date;

// Either month and day (for an indefinite year),
// or, year, month, and day.
// To be used for representing a birthday, etc.
// String representations:
//    "mm-dd", or 
//    "yyyy-mm-dd".
public class Day
{
    // ??? --> "00-00"
    public static final Day NULL_DAY = new Day(0, 0);

    // All values start from 1.
    // 0 is a special value indicating that the value has not been set.
    // Year is AD only (for now).
    // TBD: use String, maybe?
    private int year = 0;
    private int month = 0;
    private int day = 0;
    private String stringDay = null;   // "cache"

    public Day()
    {
        // Today.
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd"); 
        Date date = new Date();
        String str = dateFormat.format(date);
        parseString(str);
    }
    public Day(String str)
    {
        parseString(str);
    }
    public Day(int month, int day)
    {
        this(0, month, day);
    }
    public Day(int year, int month, int day)
    {
        // TBD: Validation, etc. ????
        this.year = year;
        this.month = month;
        this.day = day;
    }
    
        
    public int getYear()
    {
        return year;
    }
    public void setYear(int year)
    {
        this.year = year;
        stringDay = null;
    }

    public int getMonth()
    {
        return month;
    }
    public void setMonth(int month)
    {
        this.month = month;
        stringDay = null;
    }

    public int getDay()
    {
        return day;
    }
    public void setDay(int day)
    {
        this.day = day;
        stringDay = null;
    }
    
    
    
    // TBD:
    // getStringDay() based on a locale????
    // e.g., January 21st.
    // ...

    private void parseString(String str)
    {
        // TBD: Validation, etc. ????
        String[] parts = str.split("/-/");
        if(parts.length == 3) {
            year = Integer.parseInt(parts[0]);
            month = Integer.parseInt(parts[1]);
            day = Integer.parseInt(parts[2]);
        } else if(parts.length == 2) {
            year = 0;
            month = Integer.parseInt(parts[0]);
            day = Integer.parseInt(parts[1]);
        } else {
            // ????
            if(Log.I) Log.i("Invalid day string: str = " + str);
            // ignore. => 0-0-0
        }
    }
    
    
    public static String formatDay(int year, int month, int day)
    {
        // TBD:
        StringBuffer sb = new StringBuffer();
        // year == 0 means the date does not specify the year.....
        if(year > 0) {
            sb.append(String.format("%04d", year));
            sb.append("-");
        }
        sb.append(String.format("%02d", month));
        sb.append("-");
        sb.append(String.format("%02d", day));
        
        return sb.toString();        
    }

    @Override
    public String toString()
    {
        if(stringDay == null) {
            stringDay = formatDay(year, month, day);
        }
        return stringDay;
    }

    
    // TBD
    // Implement equals()
    // ...
        

}

package ms.airy.lib.core.help.fragment;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import ms.airy.lib.core.toast.ToastManager;


// TBD:
// (old implementation)
// this vs. ContextSensitiveHelpFragment ???
public class HelpSectionDialogFragment extends DialogFragment
{
    // TBD
    public static final String FRAGMENT_TAG = "HELP_FRAGMENT";

    // TBD
    private static final String PARAM_TOPIC = "topic";

    // TBD
    private String mTopic;
    private String mContent;      // Help content. TBD: Read it from resource??? 
    private String mHelpPageUrl;  // TBD.
    
    
    public static HelpSectionDialogFragment newInstance(String topic)
    {
        HelpSectionDialogFragment f = new HelpSectionDialogFragment();
        
        Bundle args = new Bundle();
        args.putString(PARAM_TOPIC, topic);
        f.setArguments(args);

        return f;
    }

    
    public String getTopic()
    {
        return mTopic;
    }
    public void setTopic(String topic)
    {
        this.mTopic = topic;
    }

    public String getContent()
    {
        return mContent;
    }
    public void setContent(String content)
    {
        this.mContent = content;
    }

    public String getHelpPageUrl()
    {
        return mHelpPageUrl;
    }
    public void setHelpPageUrl(String helpPageUrl)
    {
        this.mHelpPageUrl = helpPageUrl;
    }


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // tbd
        setRetainInstance(true);
        // ...

        mTopic = getArguments().getString(PARAM_TOPIC);
        
        // ....
        
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState)
    {
        //return super.onCreateView(inflater, container, savedInstanceState);

        // temporary
        
        LinearLayout v = new LinearLayout(getActivity());
        v.setOrientation(LinearLayout.VERTICAL);

        TextView topicView = new TextView(getActivity());
        topicView.setText(mTopic);
        LinearLayout.LayoutParams topicViewParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT); // ????
        v.addView(topicView, topicViewParams);

        // tbd
        // actual help content ???
        // ...

        
        LinearLayout buttonRow = new LinearLayout(getActivity());
        buttonRow.setOrientation(LinearLayout.HORIZONTAL);

        LinearLayout.LayoutParams bParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT); // ????
        
        Button cancelButton = new Button(getActivity());
        cancelButton.setText("Dismiss");
        cancelButton.setMinimumWidth(300);
        cancelButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                // ...
                ToastManager.showToast(getActivity(), "Cancel clicked", Toast.LENGTH_SHORT);
                HelpSectionDialogFragment.this.dismiss();
                // ....
            }
        });
        buttonRow.addView(cancelButton, bParams);
        
        Button moreButton = new Button(getActivity());
        moreButton.setText("More");
        moreButton.setMinimumWidth(300);
        moreButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                // ...
                ToastManager.showToast(getActivity(), "More clicked", Toast.LENGTH_SHORT);
            }
        });
        buttonRow.addView(moreButton, bParams);
        
        LinearLayout.LayoutParams buttonRowParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT); // ????
        v.addView(buttonRow, buttonRowParams);
        
        return v;
    }    

    
    
}

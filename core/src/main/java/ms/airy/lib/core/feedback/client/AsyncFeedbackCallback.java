package ms.airy.lib.core.feedback.client;


public interface AsyncFeedbackCallback
{
    void onSendFeedback(long surveyEntryId, int answer, String comment);
}

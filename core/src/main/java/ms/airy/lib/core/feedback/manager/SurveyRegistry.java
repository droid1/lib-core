package ms.airy.lib.core.feedback.manager;

import android.content.Context;

import ms.airy.lib.core.feedback.common.SurveyEntry;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;


public final class SurveyRegistry
{
    private static SurveyRegistry INSTANCE = null;
    public static SurveyRegistry getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new SurveyRegistry(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Aplication context
    private final Context context;

    // Remote feedback service URL
    private URL feedbackServiceURL = null;


    // "Cache"
    // Map of id -> {HelpSection, expirationTime}.
    private final Map<Long, SurveyEntry> registry = new HashMap<Long, SurveyEntry>();

    private SurveyRegistry(Context context)
    {
        this.context = context;
        // TBD:
        // Build the registry???
        // e.g., from assets file??
        // or, just hard-code all surveys???
        // ....
        // TBD: survey entry from web service???
        //  (e.g., dynamically added after the app/version has been released...)
        // .....
    }


    public SurveyEntry getSurveyEntry(long id)
    {
        return registry.get(id);
    }

}

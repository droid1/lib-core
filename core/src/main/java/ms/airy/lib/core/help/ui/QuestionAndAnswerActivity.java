package ms.airy.lib.core.help.ui;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;

import ms.airy.lib.core.R;
import ms.airy.lib.core.help.common.HelpConsts;


// Note:
// Because of the way the question/answer framework is designed (currently),
// QuestionAndAnswerActivity cannot be called until
// QuestionInfoListActivity (or, its subclass) has been called
// where the questionInfo list is initialized.
// TBD: We need a better way to do this...
// ...
public class QuestionAndAnswerActivity extends Activity
{
    private long questionId = 0L;
    private QuestionAndAnswerFragment questionAndAnswerFragment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_and_answer);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            questionId = extras.getLong(HelpConsts.KEY_EXTRA_QUESTION_ID, 0L);
        }

        if (savedInstanceState == null)
        {
            FragmentTransaction transaction = getFragmentManager().beginTransaction();

            questionAndAnswerFragment = QuestionAndAnswerFragment.newInstance(questionId);
            transaction.add(R.id.container, questionAndAnswerFragment);

            transaction.commit();
        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if(questionAndAnswerFragment != null) {
            // Check if the key event was the Back button and if there's history
            if ((keyCode == KeyEvent.KEYCODE_BACK) && questionAndAnswerFragment.canGoBack()) {
                questionAndAnswerFragment.goBack();
                return true;
            }
        }
        // If it wasn't the Back key or there's no web page history, bubble up to the default
        // system behavior (probably exit the activity)
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.question_and_answer, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}

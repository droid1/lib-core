package ms.airy.lib.core.ui.dashbar;

// To be used as a NavTarget....
public interface NavCallback
{
    void doNav();
}

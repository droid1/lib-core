package ms.airy.lib.core.dialog;

import android.app.DialogFragment;


public interface ActionableNoticeDialogHost
{
    DialogFragment addActionable(ActionableNoticeDialogListener actionable);
}

package ms.airy.lib.core.startup.job.stock;

import android.accounts.Account;
import android.app.Activity;
import android.content.Context;
import android.view.Menu;
import android.view.MenuItem;

import ms.airy.lib.core.auth.config.AppAuthConfigManager;
import ms.airy.lib.core.auth.config.ApplicationAuthConfig;
import ms.airy.lib.core.auth.helper.AccountHelper;
import ms.airy.lib.core.auth.helper.AccountPickHelper;
import ms.airy.lib.core.auth.helper.AccountPicker;
import ms.airy.lib.core.startup.job.BaseForegroundStartupJob;
import ms.airy.lib.core.startup.job.BaseStartupJob;
import ms.airy.lib.core.startup.job.ForegroundStartupJob;
import ms.airy.lib.core.startup.job.StartupJob;
import ms.airy.lib.core.startup.job.condition.BaseTaskCondition;
import ms.airy.lib.core.startup.task.BaseForegroundStartupTask;
import ms.airy.lib.core.startup.task.BaseStartupTask;


public class PrimaryAccountJob extends BaseForegroundStartupJob implements ForegroundStartupJob
{
    public PrimaryAccountJob(Context context, long id)
    {
        super(new BaseTaskCondition(false, 0), new PrimaryAccountTask(context, id), 100, false);
    }


    private static class PrimaryAccountTask extends BaseForegroundStartupTask implements AccountPicker.AccountPickerCallback
    {
        private PrimaryAccountTask(Context context, long id)
        {
            super(context, id);
        }

        @Override
        public void run(Activity activity)
        {
            Log.i("PrimaryAccountJob.run()");

            ApplicationAuthConfig authConfig = AppAuthConfigManager.getInstance(getContext()).getApplicationAuthConfig();

            // Show one picker at a time. If primary account picker item is shown, the rest are ignored, for now.
            if(authConfig.isPrimaryAccountRequired()
                    && ! AccountHelper.getInstance(getContext()).hasPrimaryAccount()) {
                int size = AccountHelper.getInstance(getContext()).getSizeForDefaultAccountType();
                if(size == 0) {
                    // Nothing to select
                    // ???
                } else {
                    if(authConfig.isAutoSelectAccount()
                            || (size == 1 && authConfig.isAutoSelectAccountIfOne())) {
                        // auto select.
                        Account account = AccountHelper.getInstance(getContext()).getDefaultAccount();
                        AccountHelper.getInstance(getContext()).setPrimaryAccount(account);
                    } else {
                        // Show account picker
                        // ...
                    }
                }
            }

            // ???
            super.run(activity);
        }

        private void showPrimaryAccountPicker(Activity activity)
        {
            int accountType = AccountHelper.getInstance(getContext()).getDefaultAccountType();
            AccountPicker accountPicker = new AccountPicker(getContext(), this, accountType);
            accountPicker.pickAccount(activity, "primary_account_job");
        }


        /////////////////////////////////////////////////
        // AccountPicker.AccountPickerCallback interface

        @Override
        public void onAccountPicked(String tag, int type, String name)
        {
            if(Log.I) Log.i("PrimaryAccountTask.onAccountPicked() tag = " + tag + "; type = " + type + "; name = " + name);

            // TBD: Validate args?
            AccountHelper.getInstance(getContext()).setPrimaryAccount(type, name);
        }

        @Override
        public void onAccountPickCanceled(String tag, int type)
        {
            if(Log.I) Log.i("PrimaryAccountTask.onAccountPickCanceled(). tag = " + tag + "; type = " + type);

            // Do nothing..
        }

    }

}

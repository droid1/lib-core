package ms.airy.lib.core.location.scheduler;

import android.content.Context;

import ms.airy.lib.core.cron.MultiCronHelper;


// For tracking locations on a regular interval.
//    this is for "short term" use on relatively fast tempo, like every 10 minutes.
public final class LocationTrackingStarter
{
    private static LocationTrackingStarter INSTANCE = null;
    public static LocationTrackingStarter getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new LocationTrackingStarter(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context.
    private final Context context;
    // private final LocationHandler locationHandler;
//    private final SchedulerThread schedulerThread;
    private LocationTrackingStarter(Context context)
    {
        this.context = context;
        // locationHandler = new LocationHandler();  // ??? Looper ???
//        schedulerThread = new SchedulerThread("");
//        schedulerThread.start();
    }


    // TBD:

    public void scheduleLocationTracking()
    {
        long futureTime = System.currentTimeMillis() + 1500L;   // 1.5 sec buffer...
        scheduleLocationTracking(futureTime);
    }
    public void scheduleLocationTracking(long futureTime)
    {
        // Log.w(">>>>>>>>> locationTime = " + locationTime);

        // tbd
        String name = "LocationTrackingScheduler";
        SchedulerThread schedulerThread = new SchedulerThread(context, name);
        // Log.w(">>>>>>>>> AAA");

        // TBD:
        schedulerThread.scheduleLocationTracking(futureTime);
        // Log.w(">>>>>>>>> BBB");

        schedulerThread.start();
    }



    // temporary
    private static final int CRON_REPEATS = 100;

    public void scheduleLocationTracking(String cronExpression, int repeats)
    {
        if(Log.I) Log.i(">>>>>>>>> scheduleLocationTracking(). cronExpression = " + cronExpression + "; repeats = " + repeats);

        // tbd
        String name = "LocationTrackingScheduler";
        SchedulerThread schedulerThread = new SchedulerThread(context, name);

//        // testing
//        Log.i("---------------- Calling gc");
//        System.gc();
//        Log.i("---------------- Called gc");


        long nextTime = System.currentTimeMillis();
        for(int i=0; i<repeats; i++) {
            Log.i("i = " + i);

            nextTime = MultiCronHelper.getInstance().getNextCronTime(cronExpression, nextTime + 1000L);

            if(Log.I) Log.i(">>>>>>>>> scheduleLocationTracking(). nextTime = " + nextTime);

            // "Clone" locations?
            // ???

            schedulerThread.scheduleLocationTracking(nextTime);
        }

        schedulerThread.start();
    }

}

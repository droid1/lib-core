package ms.airy.lib.core.feedback.manager;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;

import ms.airy.lib.core.dialog.ActionableAssertDialogListener;
import ms.airy.lib.core.dialog.stock.SimpleResponseDialogFragment;
import ms.airy.lib.core.feedback.common.SurveyEntry;


public final class FeedbackManager implements ActionableAssertDialogListener
{
    // temporary
    private static final String DEF_HELP_DIALOG_TITLE = "Feedback";

    private static FeedbackManager INSTANCE = null;
    public static FeedbackManager getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new FeedbackManager(context);
        }
        return INSTANCE;
    }

    // Unfortunately, we cannot keep activity here.
    // It'll become stale.
    // Either we make FeedbackManager non-singleton and instantiate in onCreate() every time,
    //      or, pass the relevant activity in showSurvey(), etc...
    // We choose the latter option...
//    // This is needed to get the fragment manager.
//    private final Activity activity;
    // We use the application context.
    private final Context context;

    // This is always true, for now.
    private boolean useDialogWebView;

    private FeedbackManager(Context context)
    {
//        // Note that, when we call showSurvey(), the context should be Activity.
//        // Hence this if() should always be true.
//        if(context instanceof Activity) {
//            this.activity = (Activity) context;
//        } else {
//            this.activity = null;
//        }
        this.context = context.getApplicationContext();
        useDialogWebView = true;
    }



    // TBD:Map<Long, String> getSectionTitleMap()
    // Show help in web view
    // ...


    // TBD:
    // button labels, etc.

    public boolean showSurvey(Activity activity, long surveyId)
    {
        return showSurvey(activity, surveyId, null);
    }
//    public boolean showSurvey(FragmentManager fragmentManager, long surveyId)
//    {
//        return showSurvey(fragmentManager, surveyId, null);
//    }
    public boolean showSurvey(Activity activity, long surveyId, String title)
//    public boolean showSurvey(FragmentManager fragmentManager, long surveyId, String title)
    {
        SurveyEntry entry = SurveyRegistry.getInstance(context).getSurveyEntry(surveyId);

        if(entry == null) {
            return false;  // ???
        }

        String entryTitle = entry.getTitle();
        if(entryTitle == null) {   // is empty title allowed???
            entryTitle = DEF_HELP_DIALOG_TITLE;
        }
        String entryQuestion = entry.getQuestion();
        DialogFragment fragment1 = SimpleResponseDialogFragment.newInstance(entryTitle, entryQuestion)
                .addActionable(this);

        // fragment1.getFragmentManager() seems to return null... Why not??
//        fragment1.show(fragment1.getFragmentManager(), "survey_dialog");

//        if(this.activity == null) {
//            return false;
//        }
//        fragment1.show(this.activity.getFragmentManager(), "survey_dialog");

        fragment1.show(activity.getFragmentManager(), "survey_dialog");
//        fragment1.show(fragmentManager, "survey_dialog");
        return true;
    }


    ////////////////////////////////////////////
    // ActionableAlertDialog interface


    @Override
    public void onPositiveClick(String tag)
    {
        if(Log.I) Log.i("onPositiveClick() tag = " + tag);

    }

    @Override
    public void onNegativeClick(String tag)
    {
        if(Log.I) Log.i("onNegativeClick() tag = " + tag);

    }

    @Override
    public void onNeutralClick(String tag)
    {
        if(Log.I) Log.i("onNeutralClick() tag = " + tag);

    }


}

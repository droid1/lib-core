package ms.airy.lib.core.util;

import ms.airy.lib.core.common.Config;

public class BaseConfigFactory extends AbstractConfigFactory
{
    private Config config = null;

    private BaseConfigFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class ConfigFactoryHolder
    {
        private static final BaseConfigFactory INSTANCE = new BaseConfigFactory();
    }
    
    // Singleton method
    public static BaseConfigFactory getInstance()
    {
        return ConfigFactoryHolder.INSTANCE;
    }
    
    
    @Override
    public Config getConfig()
    {
        if(config == null) {
            config = new Config();
        }
        return config;
    }

    
}

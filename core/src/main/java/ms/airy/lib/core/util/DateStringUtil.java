package ms.airy.lib.core.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;


// Temporary
// Vs. DateUtil???
public final class DateStringUtil
{
    private DateStringUtil() {}
    
    
    // TBD: The format string should be read from the config?
    // ???
    // But, we cannot change the value once the values are stored in the DB???
    // ???

    public static String formatDate(Long time)
    {
        if(time == null) {
            return null;  // ???
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        //TimeZone timeZone = TimeZone.getDefault();
        ////TimeZone timeZone = TimeZone.getTimeZone("US/Pacific");
        //dateFormat.setTimeZone(timeZone);
        String date = dateFormat.format(new Date(time));
        return date;
    }
    public static String formatDate(Long time, String timeZone)
    {
    	return formatDate(time, timeZone, true);
    }
    public static String formatDate(Long time, String timeZone, boolean showTimeZone)
    {
        if(time == null) {
            return null;  // ???
        }
        SimpleDateFormat dateFormat = null;
        if(showTimeZone) {
        	dateFormat = new SimpleDateFormat("yyyy-MM-dd z");
        } else {
        	dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        }
        TimeZone tz = TimeZone.getTimeZone(timeZone);
        dateFormat.setTimeZone(tz);
        String date = dateFormat.format(new Date(time));
        return date;
    }

    public static boolean isValidDate(String strDate)
    {
    	Long time = parseDate(strDate);
    	if(time == null) {
    		return false;
    	} else {
    		return true;
    	}
    }

    public static boolean isValidDate(String strDate, String timeZone)
    {
    	Long time = parseDate(strDate, timeZone);
    	if(time == null) {
    		return false;
    	} else {
    		return true;
    	}
    }

    public static Long parseDate(String strDate)
    {
        if(strDate == null) {
            return null;  // ???
        }
        Long t = null;
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            //TimeZone timeZone = TimeZone.getDefault();
            ////TimeZone timeZone = TimeZone.getTimeZone("US/Pacific");
            //dateFormat.setTimeZone(timeZone);
            Date date = dateFormat.parse(strDate);
            t = date.getTime();
        } catch (ParseException e1) {
            if(Log.W) Log.w("Failed to parse strDate: " + strDate, e1);
        } catch (Exception e2) {
            if(Log.W) Log.w("Failed to parse Date: strDate = " + strDate, e2);
        }
        return t;
    }
    public static Long parseDate(String strDate, String timeZone)
    {
        if(strDate == null) {
            return null;  // ???
        }
        Long t = null;
        try {
            // SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd z");
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            TimeZone tz = TimeZone.getTimeZone(timeZone);
            dateFormat.setTimeZone(tz);
            Date date = dateFormat.parse(strDate);
            t = date.getTime();
        } catch (ParseException e1) {
            if(Log.W) Log.w("Failed to parse strDate: " + strDate, e1);
        } catch (Exception e2) {
            if(Log.W) Log.w("Failed to parse Date: strDate = " + strDate, e2);
        }
        return t;
    }

}

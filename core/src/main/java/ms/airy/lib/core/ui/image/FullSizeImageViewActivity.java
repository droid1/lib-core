package ms.airy.lib.core.ui.image;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import ms.airy.lib.core.R;
import ms.airy.lib.core.ui.common.UIConsts;


public class FullSizeImageViewActivity extends Activity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_size_image_view);

        if (savedInstanceState == null) {
            int imageResId = 0;
            Bundle extras = getIntent().getExtras();
            if(extras != null) {
                imageResId = extras.getInt(UIConsts.EXTRA_FULL_SIZE_IMAGE_RESID, 0);
            }

            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            FullSizeImageViewFragment fragment = FullSizeImageViewFragment.newInstance(imageResId);
            transaction.add(R.id.container, fragment);
            transaction.commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.full_size_image_view_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}

package ms.airy.lib.core.feedback.common;

import java.io.Serializable;


/**
 * Response to a feedback message.
 */
public class ResponseEntry implements Serializable
{
    private final long id;     // > 0L.
    private String title;      // optional
    private String message;
    private long feedbackId;   // ???? feedback message id, if any,.
    // private int status;        // TBD

    public ResponseEntry(long id)
    {
        this(id, null, null);
    }

    public ResponseEntry(long id, String title, String message)
    {
        this.id = id;
        this.title = title;
        this.message = message;
    }


    public long getId()
    {
        return id;
    }

    public String getTitle()
    {
        return title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getMessage()
    {
        return message;
    }
    public void setMessage(String message)
    {
        this.message = message;
    }

    public long getFeedbackId()
    {
        return feedbackId;
    }
    public void setFeedbackId(long feedbackId)
    {
        this.feedbackId = feedbackId;
    }


    // For debugging...
    @Override
    public String toString()
    {
        return "ResponseEntry{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", message='" + message + '\'' +
                ", feedbackId=" + feedbackId +
                '}';
    }
}

package ms.airy.lib.core.time.handler;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;

import java.util.HashSet;
import java.util.Set;


/**
 * Uses the delay times based on Fibonacci sequence.
 */
public class FibonacciHandler extends Handler
{
    // temporary
    private static final int FIBONACCHI_EVENT = 111;

    // Initial delay intervals.
    // In milli seconds.
    private long delay1 = 1000L;
    private long delay2 = 1000L;

    public FibonacciHandler()
    {
    }
    public FibonacciHandler(Callback callback)
    {
        super(callback);
    }
    public FibonacciHandler(Looper looper)
    {
        super(looper);
    }
    public FibonacciHandler(Looper looper, Callback callback)
    {
        super(looper, callback);
    }


    public void setInitialDelays(long delay1, long delay2)
    {
        this.delay1 = delay1;
        this.delay2 = delay2;
    }


    // Note the name of the methods.
    // The "fetch" method is not a "getter", and it is not idempotent.
    // Calling these methods changes the internal state of the handler.
    public long fetchNextDelay()
    {
        long[] delays = fetchNextDelays(1);
        return delays[0];
    }
    public long[] fetchNextDelays(int size)
    {
        long[] delays = computeNextDelays(size, this.delay1, this.delay2);
        this.delay1 = this.delay2;
        this.delay2 = delays[0];    // note: the first element of the returned arrays is the same as delay2.
        return delays;
    }
    public long computeNextDelay()
    {
        long[] delays = computeNextDelays(1);
        return delays[0];
    }
    public long[] computeNextDelays(int size)
    {
        return computeNextDelays(size, this.delay1, this.delay2);
    }
    public long[] computeNextDelays(int size, long delay1, long delay2)
    {
        long[] delays = new long[size];

        long prev1 = delay1;
        long prev2 = delay2;
        for (int i = 0; i < size; i++) {
            delays[i] = prev1 + prev2;
            prev1 = prev2;
            prev2 = delays[i];
        }

        return delays;
    }


    public boolean sendDefaultMessageDelayed()
    {
        return sendEmptyMessageDelayed(FIBONACCHI_EVENT);
    }
    public void sendDefaultMessageDelayed(int what, int repeats)
    {
        sendEmptyMessageDelayed(FIBONACCHI_EVENT, repeats);
    }

    public boolean sendEmptyMessageDelayed(int what)
    {
        long currentUptime = SystemClock.uptimeMillis();
        long nextUptime = currentUptime + fetchNextDelay();
        return sendEmptyMessageAtTime(what, nextUptime);
    }
    public void sendEmptyMessageDelayed(int what, int repeats)
    {
        long uptime1 = SystemClock.uptimeMillis();
        long[] delays = fetchNextDelays(repeats);
        for(int i=0; i<repeats; i++) {
            long uptime2 = uptime1 + delays[i];
            boolean suc = sendEmptyMessageAtTime(what, uptime2);
            Log.d("Called sendEmptyMessageAtTime() at uptime = " + uptime2 + ". Result = " + suc);
            uptime1 = uptime2;
        }
    }

    public boolean sendMessageDelayed(Message msg)
    {
        long currentUptime = SystemClock.uptimeMillis();
        long nextUptime = currentUptime + fetchNextDelay();
        return sendMessageAtTime(msg, nextUptime);
    }
    public void sendMessageDelayed(Message msg, int repeats)
    {
        long uptime1 = SystemClock.uptimeMillis();
        long[] delays = fetchNextDelays(repeats);
        for(int i=0; i<repeats; i++) {
            long uptime2 = uptime1 + delays[i];
            boolean suc = sendMessageAtTime(msg, uptime2);
            Log.d("Called sendMessageAtTime() at uptime = " + uptime2 + ". Result = " + suc);
            uptime1 = uptime2;
        }
    }



    @Override
    public void handleMessage(Message msg)
    {
        switch(msg.what) {
            case FIBONACCHI_EVENT:
                doFibonacciEventProcessing();
                // return; // ???
                break;
            default:
                Log.i("Unrecognized message: what = " + msg.what);
        }
        super.handleMessage(msg);
    }

    private void doFibonacciEventProcessing()
    {
        Log.d("doFibonacciEventProcessing()");
        for(FibonacciEventCallback callback : fibonacciEventCallbacks) {
            callback.doDelayedProcessing();
        }
    }


    // TBD:
    // Event callbacks
    private final Set<FibonacciEventCallback> fibonacciEventCallbacks = new HashSet<FibonacciEventCallback>();
    protected final Set<FibonacciEventCallback> getFibonacciEventCallbacks()
    {
        return fibonacciEventCallbacks;
    }

    // This needs to be called by the "client"
    public final void addFibonacciEventCallback(FibonacciEventCallback fibonacciEventCallback)
    {
        fibonacciEventCallbacks.add(fibonacciEventCallback);
    }


}

package ms.airy.lib.core.auth.helper;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import ms.airy.lib.core.auth.common.AccountType;
import ms.airy.lib.core.auth.common.TypeAndAccounts;
import ms.airy.lib.core.dialog.ActionableChoiceDialogListener;
import ms.airy.lib.core.dialog.stock.SimpleItemListDialogFragment;


// TBD:
// Temporary. Can we make this into a singleton???
// Currently, this does not work.
//    We need to be able to support multiple clients (e.g., with different accountTypes, etc.)
// User AccountPicker, for now...
public class AccountPickHelper implements ActionableChoiceDialogListener
{
    // temporary
    private static final String DIALOG_TITLE = "Select Account";
    private static final String DIALOG_TAG = "account_picker_dialog";

    // Application context
    private final Context context;

    // We have pretty fragile implementation.

    // This is needed because we use async call for pickAccount().
    private int accountType = AccountType.TYPE_NONE;
    private String[] accountNames = null;

    // TBD:
    // Dialog title?
    // ...

    public AccountPickHelper(Context context)
    {
        this(context, null);
    }
    public AccountPickHelper(Context context, AccountPickCallback callback)
    {
        this.context = context;
        if(callback != null) {
            addAccountPickCallback(callback);
        }
    }



    // Async methods.
    // The method names should really have been
    //    letUserPickAccount() or initiatePickingAccount(), or something like that.
    // The caller should provide AccountPickCallback implementation.
    public void pickAccount(String type)
    {
        pickAccount(type, null);
    }
    public void pickAccount(String type, Activity activity)
    {
        this.accountType = AccountTypeUtil.getAccountType(type);
        accountNames = AccountHelper.getInstance(context).getAccountNames(type);
        showAccountPickerDialog(activity);
    }

    public void pickAccount(int type)
    {
        pickAccount(type, null);
    }
    public void pickAccount(int type, Activity activity)
    {
        this.accountType = type;
        accountNames = AccountHelper.getInstance(context).getAccountNames(type);
        showAccountPickerDialog(activity);
    }

    // TBD:
    protected void pickAccount(String[] types)
    {
        pickAccount(types, null);
    }
    protected void pickAccount(String[] types, Activity activity)
    {
        //this.accountType = ???
        accountNames = AccountHelper.getInstance(context).getAccountNames(types);
        showAccountPickerDialog(activity);
    }

    // When there is no primary account...
    public void pickAccountForDefaultType()
    {
        pickAccountForDefaultType(null);
    }
    public void pickAccountForDefaultType(Activity activity)
    {
//        //this.accountType = ???
//        accountNames = AccountHelper.getInstance(context).getAccountNamesForDefaultType();

        TypeAndAccounts typeAndAccounts = AccountHelper.getInstance(context).getDefaultTypeAndAccounts();
        this.accountType = typeAndAccounts.getType();
        accountNames = typeAndAccounts.getAccountNames();

        showAccountPickerDialog(activity);
    }


    private void showAccountPickerDialog(Activity activity)
    {
        if(activity == null) {
            if(context instanceof Activity) {
                activity = (Activity) context;
            }
        }
        if(activity == null) {
            Log.e("activity is null. Cannot display account picker dialog.");
            return;   // ??
        }
        // temporary
        // neutral button only.
//        DialogFragment fragment = SimpleItemListDialogFragment.newSingleChoiceInstance(title_resid,
//                list_resid, -1, -1, 0)
//                .addActionable(this);
        DialogFragment fragment = SimpleItemListDialogFragment.newSingleChoiceInstance(DIALOG_TITLE,
                this.accountNames, "", "", null)
                .addActionable(this);
        fragment.show(activity.getFragmentManager(), DIALOG_TAG);
    }



    //////////////////////////////////////////
    // ActionableChoiceDialogListener interface

    @Override
    public void onDialogItemClick(String tag, int which, boolean isChecked)
    {
        // isChecked is always true for singleChoice dialog.
        if(Log.I) Log.i("AccountPickerHelper.onDialogItemClick() tag = " + tag + "; which = " + which + "; isChecked = " + isChecked);

        if(DIALOG_TAG.equals(tag)) {
            String name = null;
            if(accountNames != null) {
                if(accountNames.length > which) {
                    name = accountNames[which];
                } else {
                    // ???
                    Log.w("Invalid index: which = " + which + "; accountNames = " + Arrays.toString(accountNames));
                }
            } else {
                // ???
                Log.w("accountNames array is null.");
            }
            for(AccountPickCallback c : callbacks) {
                c.onAccountPicked(accountType, name);
            }
        } else {
            // ignore
        }
    }

    @Override
    public void onPositiveClick(String tag)
    {
    }

    @Override
    public void onNegativeClick(String tag)
    {
    }

    @Override
    public void onNeutralClick(String tag)
    {
        if(DIALOG_TAG.equals(tag)) {
            for (AccountPickCallback c : callbacks) {
                c.onAccountPickCanceled(accountType);
            }
        } else {
            // Ignore.
        }
    }


    //////////////////////////////////////////
    // For "relaying" ActionableChoiceDialog.onDialogItemClick() ....

    public static interface AccountPickCallback
    {
        // void onAccountPicked(Account account);
        // void onAccountPicked(int which);
        void onAccountPicked(int type, String name);
        void onAccountPickCanceled(int type);
    }


    // We allow multiple callbacks, but most likely, the client only needs one.
    private final Set<AccountPickCallback> callbacks = new HashSet<AccountPickCallback>();
    public final void addAccountPickCallback(AccountPickCallback callback)
    {
        callbacks.add(callback);
    }


}

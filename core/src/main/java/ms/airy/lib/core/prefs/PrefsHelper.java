package ms.airy.lib.core.prefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import ms.airy.lib.core.util.CoreUtil;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;


// TBD


// TBD: Which key/setting belongs to which prefsDB???
// TBD: Create three separate classes????
//      Or/in addition, create separate classes for each category (app, memo, folder, etc.)???
public class PrefsHelper extends Prefs
{
    // temporary
    protected static final int RUNTIME_OS_VERSION = android.os.Build.VERSION.SDK_INT;

	// Private pref DB names.
    private static final String PREFSDB_SHARED = "AeryMobile.LibCore.SharedPrefs";
    private static final String PREFSDB_PRIVATE = "AeryMobile.LibCore.PrivatePrefs";
    ////private static final String PREFSDB_DEFAULT = "AeryMobile.LibCore.DefaultPrefs";

	// Default value hashmap.
	private static Map<String, Object> DEFAULT_VALUES;
	static {
	    DEFAULT_VALUES = new HashMap<String, Object>();
	    
	    DEFAULT_VALUES.put(PREF_KEY_USER_NICKNAME, PREF_DEFAULT_USER_NICKNAME);  // Do not reset. but how?
	    DEFAULT_VALUES.put(PREF_KEY_USER_CURRENCY, PREF_DEFAULT_USER_CURRENCY);
        DEFAULT_VALUES.put(PREF_KEY_USER_TIMEZONE, PREF_DEFAULT_USER_TIMEZONE);
        //DEFAULT_VALUES.put(PREF_KEY_USER_EULA_ACCEPTED, PREF_DEFAULT_USER_EULA_ACCEPTED);  -> Private
	    DEFAULT_VALUES.put(PREF_KEY_LOCATION_CITY, PREF_DEFAULT_LOCATION_CITY);
	    DEFAULT_VALUES.put(PREF_KEY_LOCATION_STATE, PREF_DEFAULT_LOCATION_STATE);
	    DEFAULT_VALUES.put(PREF_KEY_LOCATION_ZIP, PREF_DEFAULT_LOCATION_ZIP);
	    DEFAULT_VALUES.put(PREF_KEY_LOCATION_COUNTRY, PREF_DEFAULT_LOCATION_COUNTRY);
	    DEFAULT_VALUES.put(PREF_KEY_LOCATION_GEOCODE, PREF_DEFAULT_LOCATION_GEOCODE);

	    DEFAULT_VALUES.put(PREF_KEY_APP_LOCAL_MODE, PREF_DEFAULT_APP_LOCAL_MODE);
	    DEFAULT_VALUES.put(PREF_KEY_APP_USE_LOCATION, PREF_DEFAULT_APP_USE_LOCATION);
        DEFAULT_VALUES.put(PREF_KEY_APP_USE_GPS, PREF_DEFAULT_APP_USE_GPS);
        DEFAULT_VALUES.put(PREF_KEY_APP_ENABLE_ENCRYPTION, PREF_DEFAULT_APP_ENABLE_ENCRYPTION);
        DEFAULT_VALUES.put(PREF_KEY_APP_ENABLE_COMPRESSION, PREF_DEFAULT_APP_ENABLE_COMPRESSION);
        DEFAULT_VALUES.put(PREF_KEY_APP_ENCRYPT, PREF_DEFAULT_APP_ENCRYPT);
        DEFAULT_VALUES.put(PREF_KEY_APP_COMPRESSION, PREF_DEFAULT_APP_COMPRESSION);
	    DEFAULT_VALUES.put(PREF_KEY_APP_SKIP_INTRO, PREF_DEFAULT_APP_SKIP_INTRO);
	    DEFAULT_VALUES.put(PREF_KEY_APP_CONFIRM_EXIT, PREF_DEFAULT_APP_CONFIRM_EXIT);
	    DEFAULT_VALUES.put(PREF_KEY_APP_CONFIRM_DELETE, PREF_DEFAULT_APP_CONFIRM_DELETE);
	    DEFAULT_VALUES.put(PREF_KEY_APP_CONFIRM_CANCEL, PREF_DEFAULT_APP_CONFIRM_CANCEL);
	    DEFAULT_VALUES.put(PREF_KEY_APP_FIRST_SCREEN, PREF_DEFAULT_APP_FIRST_SCREEN);
        DEFAULT_VALUES.put(PREF_KEY_APP_BACK_ACTION, PREF_DEFAULT_APP_BACK_ACTION);
        DEFAULT_VALUES.put(PREF_KEY_APP_DEFAULT_FOLDER, PREF_DEFAULT_APP_DEFAULT_FOLDER);
        DEFAULT_VALUES.put(PREF_KEY_APP_DEFAULT_DIRECTORY, PREF_DEFAULT_APP_DEFAULT_DIRECTORY);
        DEFAULT_VALUES.put(PREF_KEY_APP_DEFAULT_FLAG, PREF_DEFAULT_APP_DEFAULT_FLAG);
	    DEFAULT_VALUES.put(PREF_KEY_APP_ALLOW_MEMO_SETTING, PREF_DEFAULT_APP_ALLOW_MEMO_SETTING);
	    DEFAULT_VALUES.put(PREF_KEY_APP_DEFAULT_THEME, PREF_DEFAULT_APP_DEFAULT_THEME);
	    DEFAULT_VALUES.put(PREF_KEY_APP_ALLOW_MEMO_THEME, PREF_DEFAULT_APP_ALLOW_MEMO_THEME);
	    DEFAULT_VALUES.put(PREF_KEY_APP_ALLOW_LIST_THEME, PREF_DEFAULT_APP_ALLOW_LIST_THEME);
	    DEFAULT_VALUES.put(PREF_KEY_APP_ALLOW_FOLDER_THEME, PREF_DEFAULT_APP_ALLOW_FOLDER_THEME);
	    DEFAULT_VALUES.put(PREF_KEY_APP_USE_TRASH_CAN, PREF_DEFAULT_APP_USE_TRASH_CAN);
	    DEFAULT_VALUES.put(PREF_KEY_APP_USE_TEXT_MEMO, PREF_DEFAULT_APP_USE_TEXT_MEMO);
	    DEFAULT_VALUES.put(PREF_KEY_APP_USE_LIST_MEMO, PREF_DEFAULT_APP_USE_LIST_MEMO);
	    DEFAULT_VALUES.put(PREF_KEY_APP_USE_DRAWING_MEMO, PREF_DEFAULT_APP_USE_DRAWING_MEMO);
	    DEFAULT_VALUES.put(PREF_KEY_APP_USE_IMAGE_MEMO, PREF_DEFAULT_APP_USE_IMAGE_MEMO);
	    DEFAULT_VALUES.put(PREF_KEY_APP_USE_AUDIO_MEMO, PREF_DEFAULT_APP_USE_AUDIO_MEMO);
	    DEFAULT_VALUES.put(PREF_KEY_APP_USE_VIDEO_MEMO, PREF_DEFAULT_APP_USE_VIDEO_MEMO);
	    DEFAULT_VALUES.put(PREF_KEY_APP_USE_TEMPLATE, PREF_DEFAULT_APP_USE_TEMPLATE);
	    DEFAULT_VALUES.put(PREF_KEY_APP_USE_LABEL, PREF_DEFAULT_APP_USE_LABEL);
	    DEFAULT_VALUES.put(PREF_KEY_APP_USE_CLIPBOARD, PREF_DEFAULT_APP_USE_CLIPBOARD);
        DEFAULT_VALUES.put(PREF_KEY_APP_USE_ALERT_POPUP, PREF_DEFAULT_APP_USE_ALERT_POPUP);
	    DEFAULT_VALUES.put(PREF_KEY_APP_USE_NOTIFICATION, PREF_DEFAULT_APP_USE_NOTIFICATION);
	    DEFAULT_VALUES.put(PREF_KEY_APP_USE_ALARM, PREF_DEFAULT_APP_USE_ALARM);
	    DEFAULT_VALUES.put(PREF_KEY_APP_ENABLE_SHARING, PREF_DEFAULT_APP_ENABLE_SHARING);
	    DEFAULT_VALUES.put(PREF_KEY_APP_ENABLE_EXPORT, PREF_DEFAULT_APP_ENABLE_EXPORT);
	    DEFAULT_VALUES.put(PREF_KEY_APP_ENABLE_BACKUP, PREF_DEFAULT_APP_ENABLE_BACKUP);
	    DEFAULT_VALUES.put(PREF_KEY_APP_BACKUP_MEDIA, PREF_DEFAULT_APP_BACKUP_MEDIA);
	    DEFAULT_VALUES.put(PREF_KEY_APP_AUTO_BACKUP, PREF_DEFAULT_APP_AUTO_BACKUP);
	    DEFAULT_VALUES.put(PREF_KEY_APP_BACKUP_INTERVAL, PREF_DEFAULT_APP_BACKUP_INTERVAL);
	    DEFAULT_VALUES.put(PREF_KEY_APP_BACKUP_TIME, PREF_DEFAULT_APP_BACKUP_TIME);
	    DEFAULT_VALUES.put(PREF_KEY_APP_ENABLE_FEEDBACK, PREF_DEFAULT_APP_ENABLE_FEEDBACK);
	    DEFAULT_VALUES.put(PREF_KEY_APP_ENABLE_USAGE_LOGGING, PREF_DEFAULT_APP_ENABLE_USAGE_LOGGING);
	    DEFAULT_VALUES.put(PREF_KEY_APP_AUTO_USAGE_REPORT, PREF_DEFAULT_APP_AUTO_USAGE_REPORT);
	    DEFAULT_VALUES.put(PREF_KEY_APP_USAGE_REPORT_INTERVAL, PREF_DEFAULT_APP_USAGE_REPORT_INTERVAL);
	    DEFAULT_VALUES.put(PREF_KEY_APP_USAGE_REPORT_TIME, PREF_DEFAULT_APP_USAGE_REPORT_TIME);

	    DEFAULT_VALUES.put(PREF_KEY_MEMO_DEFAULT_MIME, PREF_DEFAULT_MEMO_DEFAULT_MIME);
        DEFAULT_VALUES.put(PREF_KEY_MEMO_DEFAULT_FLAG, PREF_DEFAULT_MEMO_DEFAULT_FLAG);
	    DEFAULT_VALUES.put(PREF_KEY_MEMO_USE_UNDO_SERVICE, PREF_DEFAULT_MEMO_USE_UNDO_SERVICE);
	    DEFAULT_VALUES.put(PREF_KEY_MEMO_USE_TIME_TAG, PREF_DEFAULT_MEMO_USE_TIME_TAG);
	    DEFAULT_VALUES.put(PREF_KEY_MEMO_USE_LOCATION_TAG, PREF_DEFAULT_MEMO_USE_LOCATION_TAG);
	    DEFAULT_VALUES.put(PREF_KEY_MEMO_AUTO_EXPIRATION, PREF_DEFAULT_MEMO_AUTO_EXPIRATION);
	    DEFAULT_VALUES.put(PREF_KEY_MEMO_DEFAULT_VIEW_TYPE, PREF_DEFAULT_MEMO_DEFAULT_VIEW_TYPE);
	    DEFAULT_VALUES.put(PREF_KEY_MEMO_CONFIRM_DELETE, PREF_DEFAULT_MEMO_CONFIRM_DELETE);
	    DEFAULT_VALUES.put(PREF_KEY_MEMO_CONFIRM_CANCEL, PREF_DEFAULT_MEMO_CONFIRM_CANCEL);
	    DEFAULT_VALUES.put(PREF_KEY_MEMO_SORT_FIELD, PREF_DEFAULT_MEMO_SORT_FIELD);
	    DEFAULT_VALUES.put(PREF_KEY_MEMO_SORT_ORDER, PREF_DEFAULT_MEMO_SORT_ORDER);
	    DEFAULT_VALUES.put(PREF_KEY_MEMO_ENABLE_DYNAMIC_VIEW, PREF_DEFAULT_MEMO_ENABLE_DYNAMIC_VIEW);
	    DEFAULT_VALUES.put(PREF_KEY_MEMO_EDIT_MODE, PREF_DEFAULT_MEMO_EDIT_MODE);
	    DEFAULT_VALUES.put(PREF_KEY_MEMO_DEFAULT_FILTER, PREF_DEFAULT_MEMO_DEFAULT_FILTER);
	    DEFAULT_VALUES.put(PREF_KEY_MEMO_DEFAULT_THEME, PREF_DEFAULT_MEMO_DEFAULT_THEME);
        DEFAULT_VALUES.put(PREF_KEY_MEMO_FULLSCREEN, PREF_DEFAULT_MEMO_FULLSCREEN);
        //DEFAULT_VALUES.put(PREF_KEY_MEMO_PAGINATION, PREF_DEFAULT_MEMO_PAGINATION);
        DEFAULT_VALUES.put(PREF_KEY_MEMO_PAGE_MAX_LINES, PREF_DEFAULT_MEMO_PAGE_MAX_LINES);
	    DEFAULT_VALUES.put(PREF_KEY_MEMO_LINKIFY, PREF_DEFAULT_MEMO_LINKIFY);
	    DEFAULT_VALUES.put(PREF_KEY_MEMO_ENCRYPT, PREF_DEFAULT_MEMO_ENCRYPT);
	    DEFAULT_VALUES.put(PREF_KEY_MEMO_COMPRESSION, PREF_DEFAULT_MEMO_COMPRESSION);
	    DEFAULT_VALUES.put(PREF_KEY_MEMO_SAVE_CURSOR_POS, PREF_DEFAULT_MEMO_SAVE_CURSOR_POS);
	    DEFAULT_VALUES.put(PREF_KEY_MEMO_ENABLE_ALERT, PREF_DEFAULT_MEMO_ENABLE_ALERT);
        DEFAULT_VALUES.put(PREF_KEY_MEMO_USE_ALERT_POPUP, PREF_DEFAULT_MEMO_USE_ALERT_POPUP);
	    DEFAULT_VALUES.put(PREF_KEY_MEMO_USE_NOTIFICATION, PREF_DEFAULT_MEMO_USE_NOTIFICATION);
	    DEFAULT_VALUES.put(PREF_KEY_MEMO_USE_ALARM, PREF_DEFAULT_MEMO_USE_ALARM);
	    
	    DEFAULT_VALUES.put(PREF_KEY_LIST_DEFAULT_TYPE, PREF_DEFAULT_LIST_DEFAULT_TYPE);
        DEFAULT_VALUES.put(PREF_KEY_LIST_DEFAULT_FLAG, PREF_DEFAULT_LIST_DEFAULT_FLAG);
	    DEFAULT_VALUES.put(PREF_KEY_LIST_SORT_FIELD, PREF_DEFAULT_LIST_SORT_FIELD);
	    DEFAULT_VALUES.put(PREF_KEY_LIST_SORT_ORDER, PREF_DEFAULT_LIST_SORT_ORDER);
        DEFAULT_VALUES.put(PREF_KEY_LIST_DEFAULT_FILTER, PREF_DEFAULT_LIST_DEFAULT_FILTER);
	    DEFAULT_VALUES.put(PREF_KEY_LIST_DEFAULT_THEME, PREF_DEFAULT_LIST_DEFAULT_THEME);
	    DEFAULT_VALUES.put(PREF_KEY_LIST_FULLSCREEN, PREF_DEFAULT_LIST_FULLSCREEN);
	    DEFAULT_VALUES.put(PREF_KEY_LIST_MAX_ITEM_COUNT, PREF_DEFAULT_LIST_MAX_ITEM_COUNT);
	    DEFAULT_VALUES.put(PREF_KEY_LIST_MULTILINE_ITEM, PREF_DEFAULT_LIST_MULTILINE_ITEM);
	    DEFAULT_VALUES.put(PREF_KEY_LIST_ENABLE_ITEM_ALERT, PREF_DEFAULT_LIST_ENABLE_ITEM_ALERT);
        DEFAULT_VALUES.put(PREF_KEY_LIST_USE_ALERT_POPUP, PREF_DEFAULT_LIST_USE_ALERT_POPUP);
	    DEFAULT_VALUES.put(PREF_KEY_LIST_USE_NOTIFICATION, PREF_DEFAULT_LIST_USE_NOTIFICATION);
	    DEFAULT_VALUES.put(PREF_KEY_LIST_USE_ALARM, PREF_DEFAULT_LIST_USE_ALARM);
	    
	    DEFAULT_VALUES.put(PREF_KEY_FOLDER_DELETE_NON_EMPTY, PREF_DEFAULT_FOLDER_DELETE_NON_EMPTY);
        DEFAULT_VALUES.put(PREF_KEY_FOLDER_DEFAULT_FLAG, PREF_DEFAULT_FOLDER_DEFAULT_FLAG);
	    DEFAULT_VALUES.put(PREF_KEY_FOLDER_SORT_FIELD, PREF_DEFAULT_FOLDER_SORT_FIELD);
	    DEFAULT_VALUES.put(PREF_KEY_FOLDER_SORT_ORDER, PREF_DEFAULT_FOLDER_SORT_ORDER);
        DEFAULT_VALUES.put(PREF_KEY_FOLDER_DEFAULT_FILTER, PREF_DEFAULT_FOLDER_DEFAULT_FILTER);
	    DEFAULT_VALUES.put(PREF_KEY_FOLDER_DEFAULT_THEME, PREF_DEFAULT_FOLDER_DEFAULT_THEME);
        DEFAULT_VALUES.put(PREF_KEY_FOLDER_FULLSCREEN, PREF_DEFAULT_FOLDER_FULLSCREEN);
        //DEFAULT_VALUES.put(PREF_KEY_FOLDER_PAGINATION, PREF_DEFAULT_FOLDER_PAGINATION);
        DEFAULT_VALUES.put(PREF_KEY_FOLDER_PAGE_MAX_LINES, PREF_DEFAULT_FOLDER_PAGE_MAX_LINES);
	    DEFAULT_VALUES.put(PREF_KEY_FOLDER_LINKIFY, PREF_DEFAULT_FOLDER_LINKIFY);
	    DEFAULT_VALUES.put(PREF_KEY_FOLDER_ENCRYPT, PREF_DEFAULT_FOLDER_ENCRYPT);
	    DEFAULT_VALUES.put(PREF_KEY_FOLDER_COMPRESSION, PREF_DEFAULT_FOLDER_COMPRESSION);
	    DEFAULT_VALUES.put(PREF_KEY_FOLDER_TREE_DEPTH, PREF_DEFAULT_FOLDER_TREE_DEPTH);
	    DEFAULT_VALUES.put(PREF_KEY_FOLDER_INDENTATION, PREF_DEFAULT_FOLDER_INDENTATION);
	    
	    DEFAULT_VALUES.put(PREF_KEY_PROJECT_INCLUDE_FOLDERS, PREF_DEFAULT_PROJECT_INCLUDE_FOLDERS);
        DEFAULT_VALUES.put(PREF_KEY_PROJECT_DEFAULT_FLAG, PREF_DEFAULT_PROJECT_DEFAULT_FLAG);
	    DEFAULT_VALUES.put(PREF_KEY_PROJECT_SORT_FIELD, PREF_DEFAULT_PROJECT_SORT_FIELD);
	    DEFAULT_VALUES.put(PREF_KEY_PROJECT_SORT_ORDER, PREF_DEFAULT_PROJECT_SORT_ORDER);
        DEFAULT_VALUES.put(PREF_KEY_PROJECT_DEFAULT_FILTER, PREF_DEFAULT_PROJECT_DEFAULT_FILTER);
	    DEFAULT_VALUES.put(PREF_KEY_PROJECT_MAX_ENTRY_COUNT, PREF_DEFAULT_PROJECT_MAX_ENTRY_COUNT);
	    DEFAULT_VALUES.put(PREF_KEY_PROJECT_ENABLE_ALERT, PREF_DEFAULT_PROJECT_ENABLE_ALERT);
        DEFAULT_VALUES.put(PREF_KEY_PROJECT_USE_ALERT_POPUP, PREF_DEFAULT_PROJECT_USE_ALERT_POPUP);
	    DEFAULT_VALUES.put(PREF_KEY_PROJECT_USE_NOTIFICATION, PREF_DEFAULT_PROJECT_USE_NOTIFICATION);
	    DEFAULT_VALUES.put(PREF_KEY_PROJECT_USE_ALARM, PREF_DEFAULT_PROJECT_USE_ALARM);
	    
	    DEFAULT_VALUES.put(PREF_KEY_TEMPLATE_ALLOW_CUSTOM, PREF_DEFAULT_TEMPLATE_ALLOW_CUSTOM);
        DEFAULT_VALUES.put(PREF_KEY_TEMPLATE_DEFAULT_FLAG, PREF_DEFAULT_TEMPLATE_DEFAULT_FLAG);
	    DEFAULT_VALUES.put(PREF_KEY_TEMPLATE_SORT_FIELD, PREF_DEFAULT_TEMPLATE_SORT_FIELD);
	    DEFAULT_VALUES.put(PREF_KEY_TEMPLATE_SORT_ORDER, PREF_DEFAULT_TEMPLATE_SORT_ORDER);
        DEFAULT_VALUES.put(PREF_KEY_TEMPLATE_DEFAULT_FILTER, PREF_DEFAULT_TEMPLATE_DEFAULT_FILTER);
	    DEFAULT_VALUES.put(PREF_KEY_TEMPLATE_DEFAULT_TEXT_MEMO, PREF_DEFAULT_TEMPLATE_DEFAULT_TEXT_MEMO);
	    DEFAULT_VALUES.put(PREF_KEY_TEMPLATE_DEFAULT_LIST_MEMO, PREF_DEFAULT_TEMPLATE_DEFAULT_LIST_MEMO);
	    DEFAULT_VALUES.put(PREF_KEY_TEMPLATE_DEFAULT_DRAWING_MEMO, PREF_DEFAULT_TEMPLATE_DEFAULT_DRAWING_MEMO);
	    DEFAULT_VALUES.put(PREF_KEY_TEMPLATE_DEFAULT_IMAGE_MEMO, PREF_DEFAULT_TEMPLATE_DEFAULT_IMAGE_MEMO);
	    DEFAULT_VALUES.put(PREF_KEY_TEMPLATE_DEFAULT_AUDIO_MEMO, PREF_DEFAULT_TEMPLATE_DEFAULT_AUDIO_MEMO);
	    DEFAULT_VALUES.put(PREF_KEY_TEMPLATE_DEFAULT_VIDEO_MEMO, PREF_DEFAULT_TEMPLATE_DEFAULT_VIDEO_MEMO);

        DEFAULT_VALUES.put(PREF_KEY_FAVORITE_DEFAULT_FILTER, PREF_DEFAULT_FAVORITE_DEFAULT_FILTER);
        DEFAULT_VALUES.put(PREF_KEY_ALERT_DEFAULT_FILTER, PREF_DEFAULT_ALERT_DEFAULT_FILTER);

	}
	

	private Context mContext;
    private SharedPreferences mDefaultPrefs;
    private SharedPreferences mSharedPrefs;
	private SharedPreferences mPrivatePrefs;

	public PrefsHelper(Context context)
	{
		mContext = context;
	}
	

	// Lazy initialization.
    private SharedPreferences getDefaultPrefs()
    {
        if(mDefaultPrefs == null) {
            mDefaultPrefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        }
        return mDefaultPrefs;
    }
    // ????
    // TBD
    @Deprecated
    private SharedPreferences getSharedPrefs()
    {
        if(mSharedPrefs == null) {
            //mSharedPrefs = mContext.getSharedPreferences(PREFSDB_SHARED, Context.MODE_WORLD_WRITEABLE);
            mSharedPrefs = mContext.getSharedPreferences(PREFSDB_SHARED, Context.MODE_WORLD_READABLE);
        }
        return mSharedPrefs;
    }
    private SharedPreferences getPrivatePrefs()
    {
        if(mPrivatePrefs == null) {
            mPrivatePrefs = mContext.getSharedPreferences(PREFSDB_PRIVATE, Context.MODE_PRIVATE);
        }
        return mPrivatePrefs;
    }
	
	
    // TBD.
	public boolean resetAllPreferences()
	{
	    // TBD: Do this for all three types of prefsDB.
        SharedPreferences.Editor editor = getDefaultPrefs().edit();
        // ...
        
        Set<Entry<String, Object>> entries =  DEFAULT_VALUES.entrySet();
        for(Entry<String, Object> entry : entries) {
            String key = entry.getKey();
            Object val = entry.getValue();
            // TBD: Certain values should never be reset.
            //      e.g., Nickname if it is set, etc...
            if(val instanceof Boolean) {
                editor.putBoolean(key, (Boolean) val);
            } else if(val instanceof Integer) {
                editor.putInt(key, (Integer) val);
            } else if(val instanceof String) {
                editor.putString(key, (String) val);
            } else {
                // TBD ...
            }
        }
        
        return editor.commit();
	}
	
	
    
    //////////////////////////////////////////////////////////////////////
    // [0] Generic get/set methods (for DefaultPrefs only).
	//     TBD: for SharedPrefs/PrivatePrefs, define/use key-specific methods for each keys.
    //////////////////////////////////////////////////////////////////////
	
    private boolean getDefaultBoolean(String key)
    {
        try {
            return (Boolean) DEFAULT_VALUES.get(key);
        } catch(Exception ex) {
            if(Log.W) Log.w("getDefaultBoolean(): Runtime error. key = " + key, ex);
        }
        return false;
    }
    public boolean getBoolean(String key)
    {
        return getDefaultPrefs().getBoolean(key, getDefaultBoolean(key));
    }
    public boolean setBoolean(String key, boolean val)
    {
        SharedPreferences.Editor editor = getDefaultPrefs().edit();
        editor.putBoolean(key, val);
        return editor.commit();
    }

    private int getDefaultInt(String key)
    {
        try {
            return (Integer) DEFAULT_VALUES.get(key);
        } catch(Exception ex) {
            if(Log.W) Log.w("getDefaultInt(): Runtime error. key = " + key, ex);
        }
        return 0;
    }
    public int getInt(String key)
    {
        return getDefaultPrefs().getInt(key, getDefaultInt(key));
    }
    public boolean setInt(String key, int val)
    {
        SharedPreferences.Editor editor = getDefaultPrefs().edit();
        editor.putInt(key, val);
        return editor.commit();
    }

    private long getDefaultLong(String key)
    {
        try {
            return (Long) DEFAULT_VALUES.get(key);
        } catch(Exception ex) {
            if(Log.W) Log.w("getDefaultLong(): Runtime error. key = " + key, ex);
        }
        return 0L;
    }
    public long getLong(String key)
    {
        return getDefaultPrefs().getLong(key, getDefaultLong(key));
    }
    public boolean setLong(String key, long val)
    {
        SharedPreferences.Editor editor = getDefaultPrefs().edit();
        editor.putLong(key, val);
        return editor.commit();
    }

    private float getDefaultFloat(String key)
    {
        try {
            return (Float) DEFAULT_VALUES.get(key);
        } catch(Exception ex) {
            if(Log.W) Log.w("getDefaultFloat(): Runtime error. key = " + key, ex);
        }
        return 0.0f;
    }
    public float getFloat(String key)
    {
        return getDefaultPrefs().getFloat(key, getDefaultFloat(key));
    }
    public boolean setFloat(String key, float val)
    {
        SharedPreferences.Editor editor = getDefaultPrefs().edit();
        editor.putFloat(key, val);
        return editor.commit();
    }

    private String getDefaultString(String key)
    {
        try {
            return (String) DEFAULT_VALUES.get(key);
        } catch(Exception ex) {
            if(Log.W) Log.w("getDefaultString(): Runtime error. key = " + key, ex);
        }
        return "";
    }
    public String getString(String key)
    {
        String val = getDefaultPrefs().getString(key, getDefaultString(key));
        if(val != null) {
            val = val.trim();
        }
        return val;
    }
    public boolean setString(String key, String val)
    {
        SharedPreferences.Editor editor = getDefaultPrefs().edit();
        editor.putString(key, val);
        return editor.commit();
    }
	
    
    
    /// To be deleted ////////////////////////////////////////////////////
    
    //////////////////////////////////////////////////////////////////////
    // [1] General preferences
    //////////////////////////////////////////////////////////////////////

    public String getNickname()
    {
        String val = getDefaultPrefs().getString(PREF_KEY_USER_NICKNAME, PREF_DEFAULT_USER_NICKNAME);
        if(val != null) {
            val = val.trim();
        }
        return val;
    }
    public boolean setNickname(String val)
    {
        if(val != null) {
            val = val.trim();
            SharedPreferences.Editor editor = getDefaultPrefs().edit();
            editor.putString(PREF_KEY_USER_NICKNAME, val);
            return editor.commit();
        } else {
            return false;
        }
    }
    
    //////////////////////////////////////////////////////////////////////
    // [2] App preferences
    //////////////////////////////////////////////////////////////////////
	
    public boolean getSkipIntro()
    {
        return getDefaultPrefs().getBoolean(PREF_KEY_APP_SKIP_INTRO, PREF_DEFAULT_APP_SKIP_INTRO);
    }
    public boolean setSkipIntro(boolean val)
    {
        SharedPreferences.Editor editor = getDefaultPrefs().edit();
        editor.putBoolean(PREF_KEY_APP_SKIP_INTRO, val);
        return editor.commit();
    }

    public boolean getConfirmExit()
    {
        return getDefaultPrefs().getBoolean(PREF_KEY_APP_CONFIRM_EXIT, PREF_DEFAULT_APP_CONFIRM_EXIT);
    }
    public boolean setConfirmExit(boolean val)
    {
        SharedPreferences.Editor editor = getDefaultPrefs().edit();
        editor.putBoolean(PREF_KEY_APP_CONFIRM_EXIT, val);
        return editor.commit();
    }

    public boolean getConfirmDelete()
    {
        return getDefaultPrefs().getBoolean(PREF_KEY_APP_CONFIRM_DELETE, PREF_DEFAULT_APP_CONFIRM_DELETE);
    }
    public boolean setConfirmDelete(boolean val)
    {
        SharedPreferences.Editor editor = getDefaultPrefs().edit();
        editor.putBoolean(PREF_KEY_APP_CONFIRM_DELETE, val);
        return editor.commit();
    }

    public boolean getConfirmCancel()
    {
        return getDefaultPrefs().getBoolean(PREF_KEY_APP_CONFIRM_CANCEL, PREF_DEFAULT_APP_CONFIRM_CANCEL);
    }
    public boolean setConfirmCancel(boolean val)
    {
        SharedPreferences.Editor editor = getDefaultPrefs().edit();
        editor.putBoolean(PREF_KEY_APP_CONFIRM_CANCEL, val);
        return editor.commit();
    }

    /// To be deleted ////////////////////////////////////////////////////
    

    
    
    
    
    //////////////////////////////////////////////////////////////////////
    // [*] Private settings.
    //////////////////////////////////////////////////////////////////////

    
    public long getEulaAccepted()
    {
        return getPrivatePrefs().getLong(Prefs.PREF_KEY_USER_EULA_ACCEPTED, Prefs.PREF_DEFAULT_USER_EULA_ACCEPTED);
    }
    public boolean isEulaAccepted()
    {
        long acceptedTime = getEulaAccepted();
        if(acceptedTime > 0L) {
            return true;
        } else {
            return false;
        }
    }
    public boolean setEulaAccepted()
    {
        return setEulaAccepted(true);
    }
    public boolean setEulaAccepted(long now)
    {
        SharedPreferences.Editor editor = getPrivatePrefs().edit();
        editor.putLong(Prefs.PREF_KEY_USER_EULA_ACCEPTED, now);
        return editor.commit();
    }
    public boolean setEulaAccepted(boolean accepted)
    {
        long now = 0L;
        if(accepted) {
            now = System.currentTimeMillis();
        }
        return setEulaAccepted(now);
    }
        

    
    
    //////////////////////////////////////////////////////////////////////
    // [*] Miscellaneous
    //////////////////////////////////////////////////////////////////////

	// "Read-only"
	public String getDeviceId()
	{
		// Temporary
		String deviceId = getPrivatePrefs().getString(APP_KEY_DEVICE_ID, null);
		if(deviceId == null || deviceId.length() == 0) {
			// Unique string...
			// temporary
			Random RNG = new Random();
			long r = RNG.nextLong();
			String text = (new Date()).toString() + Long.toString(r);
			try {
				deviceId = CoreUtil.SHA1(text);
			} catch(Exception ex) {
				if(Log.W) Log.w("Failed to create a unique device Id through SHA-1 hash.", ex);
				// What to do???
				//deviceId = System.getString(mContext.getContentResolver(), android.Provider.Settings.System.ANDROID_ID);
				Formatter formatter = new Formatter();
				deviceId = formatter.format("%40s", Long.toString(r)).toString();  // ???
				/*
				<uses-permission android:name="android.permission.READ_PHONE_STATE"></uses-permission>
				TelephonyManager mTelephonyMgr = TelephonyManager)getSystemService(TELEPHONY_SERVICE); 
				String imei = mTelephonyMgr.getDeviceId(); // Requires READ_PHONE_STATE
		        String phoneNumber=mTelephonyMgr.getLine1Number(); // Requires READ_PHONE_STATE
		        String softwareVer = mTelephonyMgr.getDeviceSoftwareVersion(); // Requires READ_PHONE_STATE
		        String simSerial = mTelephonyMgr.getSimSerialNumber(); // Requires READ_PHONE_STATE
		        String subscriberId = mTelephonyMgr.getSubscriberId(); // Requires READ_PHONE_STATE
				*/
			}
			
			// Save it.
	        SharedPreferences.Editor editor = getPrivatePrefs().edit();
			editor.putString(APP_KEY_DEVICE_ID, deviceId);
			editor.commit();
			
			if(Log.I) Log.i("A new device id created: " + deviceId);
		}

		return deviceId;
	}
	
	// Will we ever need this?
	private void resetDeviceId()
	{
        SharedPreferences.Editor editor = getPrivatePrefs().edit();
		editor.putString(APP_KEY_DEVICE_ID, "");
		editor.commit();		
	}
	
	
	public String getAppInstallDate()
	{
		// Temporary
		String installDate = getPrivatePrefs().getString(APP_KEY_USER_INSTALL_DATE, null);
		if(installDate == null || installDate.length() == 0) {
			// This cannot happen...
			installDate = initAppInstallDate();
		}
		
		return installDate;
	}

	public String initAppInstallDate()
	{
		String installDate = getPrivatePrefs().getString(APP_KEY_USER_INSTALL_DATE, null);
		if(installDate == null || installDate.length() == 0) {
            // installDate = (new Date()).toGMTString();
            installDate = currentTimeAsGMTString();
			setAppInstallDate(installDate);
        }
		return installDate;
	}

	public boolean setAppInstallDate(String installDate)
	{
		// TBD: validate installDate??
        SharedPreferences.Editor editor = getPrivatePrefs().edit();
		editor.putString(APP_KEY_USER_INSTALL_DATE, installDate);
		boolean suc = editor.commit();
		if(suc) {
			if(Log.I) Log.i("Install date has been set: " + installDate);
		} else {
			if(Log.W) Log.w("Failed to set the install date: " + installDate);			
		}
		return suc;
	}

    // temporary
    private static String currentTimeAsGMTString()
    {
        // TBD: Use System.currentTimeMillis() rather than new Date()....
        return toGMTString(new Date());
    }
    private static String toGMTString(Date date)
    {
        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sd.setTimeZone(TimeZone.getTimeZone("GMT"));
        return sd.format(date);
    }

}

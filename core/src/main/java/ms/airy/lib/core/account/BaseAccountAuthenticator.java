package ms.airy.lib.core.account;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.accounts.NetworkErrorException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;


// Resources:
// http://developer.android.com/reference/android/accounts/AccountManager.html
// http://developer.android.com/reference/android/accounts/AbstractAccountAuthenticator.html
// http://www.finalconcept.com.au/article/view/android-account-manager-step-by-step
// http://www.finalconcept.com.au/article/view/android-account-manager-step-by-step-1
// http://www.c99.org/2010/01/23/writing-an-android-sync-provider-part-1/
public class BaseAccountAuthenticator extends AbstractAccountAuthenticator
{
    private Context mContext;
    
    public BaseAccountAuthenticator(Context context)
    {
        super(context);
        mContext = context;
    }

    @Override
    public Bundle addAccount(AccountAuthenticatorResponse response, String accountType,
            String authTokenType, String[] requiredFeatures, Bundle options)
            throws NetworkErrorException
    {
        Bundle reply = new Bundle();
        
        
        // TBD: 
        // Use requiredFeatures to populate the data below????
        
        
//        // TBD:
//        //Intent i = new Intent(mContext, BaseHome.class);   // ?????
//        Intent i = new Intent(mContext, BaseAccountActivity.class);   // ?????
//        i.setAction("ms.airy.lib.family.ui.sync.LOGIN");        /// ?????
//        i.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);
//        reply.putParcelable(AccountManager.KEY_INTENT, i);
        
        return reply;
    }

    @Override
    public Bundle confirmCredentials(AccountAuthenticatorResponse arg0,
            Account arg1, Bundle arg2) throws NetworkErrorException
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Bundle editProperties(AccountAuthenticatorResponse arg0, String arg1)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Bundle getAuthToken(AccountAuthenticatorResponse arg0, Account arg1,
            String arg2, Bundle arg3) throws NetworkErrorException
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getAuthTokenLabel(String arg0)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Bundle hasFeatures(AccountAuthenticatorResponse arg0, Account arg1,
            String[] arg2) throws NetworkErrorException
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Bundle updateCredentials(AccountAuthenticatorResponse arg0,
            Account arg1, String arg2, Bundle arg3)
            throws NetworkErrorException
    {
        // TODO Auto-generated method stub
        return null;
    }

}

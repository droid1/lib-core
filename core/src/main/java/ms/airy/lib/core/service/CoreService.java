package ms.airy.lib.core.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.IBinder;

// For serving predefined core  
public class CoreService extends IntentService
{
    private static final String CORE_SERVICE = "AeryMobile.lib.core.service.CoreService";

    public CoreService()
    {
        super(CORE_SERVICE);
    }

    
    @Override
    protected void onHandleIntent(Intent intent)
    {
        // TBD:

        // [1] "predefined" intents
        
        
        // [2] Other intents
        
        
        // [3] Generic implementation
        
        
    }

    @Override
    public IBinder onBind(Intent intent)
    {
        // TBD...
        return null;
    }


    
    @Override
    public void onCreate()
    {
        // TODO Auto-generated method stub
        super.onCreate();
    }

    @Override
    public void onDestroy()
    {
        // TODO Auto-generated method stub
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        // TODO Auto-generated method stub
        return super.onStartCommand(intent, flags, startId);
    }

}

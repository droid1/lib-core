package ms.airy.lib.core.status.client.task;

import android.os.AsyncTask;

import ms.airy.lib.core.status.client.StatusMessageFetchClient;
import ms.airy.lib.core.status.client.StatusMessageSendClient;
import ms.airy.lib.core.status.client.factory.StatusClientFactory;
import ms.airy.lib.core.status.common.StatusMessage;

import java.util.List;


/**
 */
public class AsyncStatusSendMessageTask extends AsyncTask<Object, Void, Long>
{
    private final AsyncTaskCallback callback;
    public AsyncStatusSendMessageTask(AsyncTaskCallback callback)
    {
        this.callback = callback;
    }

    // Store input params during processing
    private int type;
    private String message;



    @Override
    protected Long doInBackground(Object... params)
    {
        type = (Integer) params[0];
        message = (String) params[1];

        StatusMessageSendClient client = StatusClientFactory.getInstance().getSendClient();

        Long id = client.sendMessage(type, message);

        return id;
    }

    @Override
    protected void onPostExecute(Long id)
    {
        // super.onPostExecute(aLong);

        this.callback.onSendMessage(id, type, message);
    }




    // Callback interface
    public static interface AsyncTaskCallback
    {
        void onSendMessage(long id, int type, String message);
    }

}

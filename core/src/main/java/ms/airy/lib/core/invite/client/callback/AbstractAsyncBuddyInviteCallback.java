package ms.airy.lib.core.invite.client.callback;

import java.util.List;

import ms.airy.lib.core.invite.client.AsyncInviteCallback;
import ms.airy.lib.core.invite.common.InviteInfo;
import ms.airy.lib.core.invite.common.RecipientInfo;


public abstract class AbstractAsyncBuddyInviteCallback implements AsyncInviteCallback
{

    @Override
    public void onSendInvite(long invitedId, String recipient)
    {
        if(Log.I) Log.i("AbstractAsyncBuddyInviteCallback.onSendInvite(). invitedId = " + invitedId + "; recipient = " + recipient);

    }

    @Override
    public void onGetInviteInfo(InviteInfo inviteInfo, long inviteId)
    {
        if(Log.I) Log.i("AbstractAsyncBuddyInviteCallback.onGetInviteInfo(). inviteInfo = " + inviteInfo + "; inviteId = " + inviteId);

    }

    @Override
    public void onGetInvites(List<Long> invites, int status)
    {
        if(Log.I) Log.i("AbstractAsyncBuddyInviteCallback.onGetInvites(). status = " + status);

    }

    @Override
    public void onGetRecipientInfo(RecipientInfo recipientInfo, String recipient)
    {
        if(Log.I) Log.i("AbstractAsyncBuddyInviteCallback.onGetRecipientInfo(). recipientInfo = " + recipientInfo + "; recipient = " + recipient);

    }

    @Override
    public void onGetRecipients(List<String> recipients, int status)
    {
        if(Log.I) Log.i("AbstractAsyncBuddyInviteCallback.onGetRecipients(). status = " + status);

    }
}

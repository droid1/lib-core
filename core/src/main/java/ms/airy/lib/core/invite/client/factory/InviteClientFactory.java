package ms.airy.lib.core.invite.client.factory;


import ms.airy.lib.core.invite.client.BuddyInviteClient;
import ms.airy.lib.core.invite.client.impl.MiniBuddyInviteClient;
import ms.airy.lib.core.invite.client.impl.MockBuddyInviteClient;

/**
 */
public class InviteClientFactory
{
    // temporary
    private static final int CLIENT_MOCK = 1;
    private static final int CLIENT_MINI = 2;
    // ...

    private static InviteClientFactory INSTANCE = null;

    public static InviteClientFactory getInstance()
    {
        if(INSTANCE == null) {
            INSTANCE = new InviteClientFactory();
        }
        return INSTANCE;
    }

    // mock or mini
    private int type;

    private BuddyInviteClient inviteClient = null;
    private InviteClientFactory()
    {
        // TBD:
        type = CLIENT_MOCK;
        // ...
    }

    public void useMockClient()
    {
        type = CLIENT_MOCK;
        inviteClient = null;
    }
    public void useMiniClient()
    {
        type = CLIENT_MINI;
        inviteClient = null;
    }



    public BuddyInviteClient getInviteClient()
    {
        if(inviteClient == null) {
            switch(type) {
                case CLIENT_MOCK:
                default:
                    inviteClient = new MockBuddyInviteClient();
                    break;
                case CLIENT_MINI:
                    inviteClient = new MiniBuddyInviteClient();
                    break;
            }
        }
        return inviteClient;
    }



}

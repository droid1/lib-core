package ms.airy.lib.core.util;

import java.util.Random;


/**
 */
public final class IdUtil
{
    private IdUtil() {}

    private static Random sRNG = null;
    private static Random getRandom()
    {
        if(sRNG == null) {
            sRNG = new Random(System.currentTimeMillis());
        }
        return sRNG;
    }

    // temporary.
    // Note: assuming the rng is truely random,
    // there is roughly one out of 90000 chance of collision for five digit version, etc..
    private static final long MIN_ID_03 = 100L;
    private static final long MAX_ID_03 = 1000L;
    private static final long MIN_ID_04 = 1000L;
    private static final long MAX_ID_04 = 10000L;
    private static final long MIN_ID_05 = 10000L;
    private static final long MAX_ID_05 = 100000L;
    private static final long MIN_ID_07 = 1000000L;
    private static final long MAX_ID_07 = 10000000L;
    private static final long MIN_ID_10 = 1000000000L;
    private static final long MAX_ID_10 = 10000000000L;
    private static final long MIN_ID_15 = 100000000000000L;
    private static final long MAX_ID_15 = 1000000000000000L;

    // TBD:
    public static long generateRandomId()
    {
        return generateRandomId10();
    }

    public static long generateRandomId03()
    {
        return generateRandomId(MIN_ID_03, MAX_ID_03);
    }
    public static long generateRandomId04()
    {
        return generateRandomId(MIN_ID_04, MAX_ID_04);
    }
    public static long generateRandomId05()
    {
        return generateRandomId(MIN_ID_05, MAX_ID_05);
    }
    public static long generateRandomId07()
    {
        return generateRandomId(MIN_ID_07, MAX_ID_07);
    }

    public static long generateRandomId10()
    {
        return generateRandomId(MIN_ID_10, MAX_ID_10);
    }
    public static long generateRandomId15()
    {
        return generateRandomId(MIN_ID_15, MAX_ID_15);
    }

    // min; inclusive, max: exclusive.
    public static long generateRandomId(long min, long max)
    {
        // Temporary
        long r = (Math.abs(getRandom().nextLong()) % (max - min) ) + min;
        return r;
    }



}

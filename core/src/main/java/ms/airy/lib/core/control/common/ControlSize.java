package ms.airy.lib.core.control.common;


import java.util.HashMap;
import java.util.Map;

// "Size" of a custom control.
// Just constants.
// The actual "size" of a control is to be determined by each app.
public final class ControlSize
{
    // TBD...
    public static final String SIZE_TINY = "tiny";
    public static final String SIZE_SMALL = "small";
    public static final String SIZE_MEDIUM = "medium";
    public static final String SIZE_LARGE = "large";
    public static final String SIZE_XLARGE = "xlarge";
    public static final String SIZE_XXLARGE = "xxlarge";
    public static final String SIZE_GIGANTIC = "gigantic";
    // ...

    // Special consts.
    // (1) Default: Equivalent to not specifying the value.
    // App should have the "default" value.
    public static final String SIZE_DEFAULT = "default";
    // (2) Ues the parent value. (similar to "match_parent" layout param)
    public static final String SIZE_PARENT = "parent";
    // ...


    // static consts only.
    private ControlSize() {}


    // for efficiency...
    private static final Map<String, String> decrMap = new HashMap<String,String>();
    static {
        decrMap.put(SIZE_TINY, SIZE_TINY);
        decrMap.put(SIZE_SMALL, SIZE_TINY);
        decrMap.put(SIZE_MEDIUM, SIZE_SMALL);
        decrMap.put(SIZE_LARGE, SIZE_MEDIUM);
        decrMap.put(SIZE_XLARGE, SIZE_LARGE);
        decrMap.put(SIZE_XXLARGE, SIZE_XLARGE);
        decrMap.put(SIZE_GIGANTIC, SIZE_XXLARGE);
    }


    public static boolean isValidSize(String size)
    {
        // tbd: what about default, parent ????
        return decrMap.containsKey(size);
    }
    public static boolean isValidValue(String size)
    {
        if(decrMap.containsKey(size)
                || (SIZE_DEFAULT.equals(size) || SIZE_PARENT.equals(size))) {
            return true;
        } else {
            return false;
        }
    }


    public static int compareSizes(String sizeOne, String sizeTwo)
    {
        if(!isValidSize(sizeOne) && !isValidSize(sizeTwo)) {
            return 0;
        } else {
            if(!isValidSize(sizeOne)) {
                return -1;
            } else if(!isValidSize(sizeTwo)) {
                return 1;
            } else {
                if(sizeOne.equals(SIZE_TINY)) {
                    if(sizeTwo.equals(SIZE_TINY)) {
                        return 0;
                    } else {
                        return -1;
                    }
                } else if(sizeOne.equals(SIZE_SMALL)) {
                    if(sizeTwo.equals(SIZE_TINY)) {
                        return 1;
                    } else if(sizeTwo.equals(SIZE_SMALL)) {
                        return 0;
                    } else {
                        return -1;
                    }
                } else if(sizeOne.equals(SIZE_MEDIUM)) {
                    if(sizeTwo.equals(SIZE_TINY) || sizeTwo.equals(SIZE_SMALL)) {
                        return 1;
                    } else if(sizeTwo.equals(SIZE_MEDIUM)) {
                        return 0;
                    } else {
                        return -1;
                    }
                } else if(sizeOne.equals(SIZE_LARGE)) {
                    if(sizeTwo.equals(SIZE_TINY) || sizeTwo.equals(SIZE_SMALL) || sizeTwo.equals(SIZE_MEDIUM)) {
                        return 1;
                    } else if(sizeTwo.equals(SIZE_LARGE)) {
                        return 0;
                    } else {
                        return -1;
                    }
                } else if(sizeOne.equals(SIZE_XLARGE)) {
                    if(sizeTwo.equals(SIZE_TINY) || sizeTwo.equals(SIZE_SMALL) || sizeTwo.equals(SIZE_MEDIUM) || sizeTwo.equals(SIZE_LARGE)) {
                        return 1;
                    } else if(sizeTwo.equals(SIZE_XLARGE)) {
                        return 0;
                    } else {
                        return -1;
                    }
                } else if(sizeOne.equals(SIZE_XXLARGE)) {
                    if(sizeTwo.equals(SIZE_TINY) || sizeTwo.equals(SIZE_SMALL) || sizeTwo.equals(SIZE_MEDIUM) || sizeTwo.equals(SIZE_LARGE) || sizeTwo.equals(SIZE_XLARGE)) {
                        return 1;
                    } else if(sizeTwo.equals(SIZE_XXLARGE)) {
                        return 0;
                    } else {
                        return -1;
                    }
                } else {  // if(sizeOne.equals(SIZE_GIGANTIC))
                    if(sizeTwo.equals(SIZE_TINY) || sizeTwo.equals(SIZE_SMALL) || sizeTwo.equals(SIZE_MEDIUM) || sizeTwo.equals(SIZE_LARGE) || sizeTwo.equals(SIZE_XLARGE) || sizeTwo.equals(SIZE_XXLARGE)) {
                        return 1;
                    } else {  // if(sizeTwo.equals(SIZE_GIGANTIC))
                        return 0;
                    }
                }
            }
        }
    }


    public static String getOneSizeSmaller(String size)
    {
        return decrMap.get(size);
    }

    public static String getTwoSizeSmaller(String size)
    {
//        String oneSizeSmaller = decrMap.get(size);
//        if(isValid(oneSizeSmaller)) {
//            return decrMap.get(oneSizeSmaller);
//        } else {
//            // ?????
//        }
        return decrMap.get(decrMap.get(size));
    }
    public static String getThreeSizeSmaller(String size)
    {
        return decrMap.get(decrMap.get(decrMap.get(size)));
    }
    public static String getFourSizeSmaller(String size)
    {
        return decrMap.get(decrMap.get(decrMap.get(decrMap.get(size))));
    }


    // TBD
    // getOneSizeBigger(), etc... ???
    // ..


}

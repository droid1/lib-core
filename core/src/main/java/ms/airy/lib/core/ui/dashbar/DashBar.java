package ms.airy.lib.core.ui.dashbar;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.drawable.Drawable;
import android.view.View;


// Substitute for ActionBar....
public class DashBar
{
    // ????
    //Context mContext;
    
    // ....
    Activity mParent;
    
    // The ui part
    //private DashBarFragment fragment;

    // The ui part
    private DashBarView mView;

    
    // TBD: Do we need this????
    // Maybe just use the data in LinearLayout ?????
            
    // "Home" ????
    private NavItem mHome;
    
    // "Up"  ????
    //private NavItem up;
    
    // "Home" ????
    private NavItem mMenu;

    
    // "Action items"  ???? 
    //private NavItemList mItems;
    private List<NavItem> mItems;
    
    
    // TBD
    private boolean mIncludeHomeButton = true;
    private boolean mIncludeMenuButton = true;
    // ...
    
    
    // Client sets this flag to indicate that the all items have been added.
    private boolean mInitialized = false;
    

    // TBD: .....
    // Change context to parent activity
    public DashBar(Activity parent)
    {
        // ....
        mParent = parent;
        //mContext = context;
        mView = new DashBarView(mParent);

        // .... 
        resetNavItems();
        
    }
    
    // ????
    protected void resetNavItems()
    {
        //mItems = new NavItemList();
        mItems = new ArrayList<NavItem>();
        // TBD: Need to remove all navButton in mView as well.......
        // ...

        if(mIncludeHomeButton) {
            Drawable homeIcon = null;   // TBD
            NavTarget homeTarget = new NavTarget(NavType.HOME);  // TBD
            NavItem home = new NavItem(mParent, "home", "Home", homeIcon, homeTarget);
            setHome(home);
        }
        
        if(mIncludeMenuButton) {
            Drawable menuIcon = null;   // TBD
            NavTarget menuTarget = new NavTarget(NavType.MENU);  // TBD
            NavItem menu = new NavItem(mParent, "menu", "Menu", menuIcon, menuTarget);
            setMenu(menu);
        }
    
        // ???
        mInitialized = false;
    }

    public boolean isInitialized()
    {
        return mInitialized;
    }
    public void setInitialized()
    {
        setInitialized(true);
    }
    public void setInitialized(boolean initialized)
    {
        this.mInitialized = initialized;
    }

    
    public DashBarView getView()
    {
        if(Log.D) Log.d("getView()");

        return mView;
    }

    // TBD: Should we expose this????
    private void setView(DashBarView view)
    {
        if(Log.D) Log.d("setView()");

        this.mView = view;
        // TBD:
        // Update all other attributes????
        
        
        // ?????
        mView.invalidate();
    }

    // ???
    private void refreshView()
    {
        // TBD:
        // based on the current mItems
        // update the linearlayout...
        // ....

        // ?????
        mView.invalidate();
    }

    
    
    
    private void doNavTarget(NavTarget target)
    {
        if(Log.D) Log.d("doNavTarget()");

        if(target != null) {
            NavType type = target.getType();
            if(Log.D) Log.d("navType = " + type);

            if(type == NavType.HOME) {
                // ???
                Intent intent = null;
                //try {
                    intent = mParent.getPackageManager().getLaunchIntentForPackage(mParent.getPackageName());
                //} catch(NameNotFoundException ex) {
                //    // ignore
                //    if(Log.I) Log.i("Default intent not found. ", ex);
                //}
                
                if(intent == null) {
                    if(Log.I) Log.i("Default intent not found. ");

                    intent = new Intent(Intent.ACTION_MAIN);
                    intent.addCategory(Intent.CATEGORY_LAUNCHER);
                    //intent.addCategory(Intent.CATEGORY_DEFAULT);
                    //intent.setPackage(mParent.getPackageName());  // Note: Main/home activity might be in a different package than application package.
                }
                
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mParent.startActivity(intent);
                
                // ????
            } else if(type == NavType.UP) {
                // ???
                
            } else if(type == NavType.MENU) {
                // ????
                mParent.openOptionsMenu();
                // ???
            } else {
                if(type == NavType.FRAGMENT) {
                    // TBD: 
                    Fragment fragment = target.getFragment();
                    // ????
                    // ....
                } else if(type == NavType.ACTIVITY) {
                    Class<?> activity = target.getActivity();
                    if(activity != null) {
                        Intent intent = new Intent(mParent, activity);
                        mParent.startActivity(intent); // ???
                    } else {
                        // ????
                    }
                } else if(type == NavType.INTENT) {
                    Intent intent = target.getIntent();
                    if(intent != null) {
                        mParent.startActivity(intent); // ???
                    } else {
                        // ????
                    }
                } else if(type == NavType.CALLBACK) {
                    NavCallback callback = target.getCallbak();
                    if(callback != null) {
                        // TBD: ....
                        callback.doNav();
                    } else {
                        // ????
                    }
                } else {
                    // ???
                }
            }
        } else {
            // ignore ????
            // ....
        }
    }
    
    
    public void addNavItem(final NavItem item)
    {
        if(Log.D) Log.d("addNavItem()");
        mItems.add(item);
        NavButton button = item.getNavButton();
        button.setNavListener(new NavListener() {
            public void onClick(View v)
            {
                // TBD:
                NavTarget target = item.getTarget();
                DashBar.this.doNavTarget(target);
            }            
        });
        mView.addNavButton(button);
    }
    public void addNavItems(final NavItem... items)
    {
        if(Log.D) Log.d("addNavItems()");
        //mItems.addAll(Arrays.asList(items));
        // mView.???
        for(final NavItem i : items) {
            mItems.add(i);
            NavButton button = i.getNavButton();
            button.setNavListener(new NavListener() {
                public void onClick(View v)
                {
                    // TBD:
                    NavTarget target = i.getTarget();
                    DashBar.this.doNavTarget(target);
                }            
            });
            mView.addNavButton(button);
        }
    }
    public void addNavItem(int location, final NavItem item)
    {
        if(Log.D) Log.d("addNavItem(): location = " + location);
        mItems.add(location, item);
        NavButton button = item.getNavButton();
        button.setNavListener(new NavListener() {
            public void onClick(View v)
            {
                // TBD:
                NavTarget target = item.getTarget();
                DashBar.this.doNavTarget(target);
            }            
        });
        mView.addNavButton(location, button);
    }
    public void addNavItems(int location, final NavItem... items)
    {
        if(Log.D) Log.d("addNavItems(): location = " + location);
        //mItems.addAll(location, Arrays.asList(items));
        // mView.????
        for(int n=items.length-1; n>=0; n--) {   // Does this make sense?????
            final NavItem item = items[n];
            mItems.add(location, item);
            NavButton button = item.getNavButton();
            button.setNavListener(new NavListener() {
                public void onClick(View v)
                {
                    // TBD:
                    NavTarget target = item.getTarget();
                    DashBar.this.doNavTarget(target);
                }            
            });
            mView.addNavButton(location, button);
        }
    }
    
    // TBD:
    //public void removeNavItem(String tag) {}
    //public void removeNavItems(String... tags) {}
    
    
    
    public NavItem getHome()
    {
        if(Log.D) Log.d("getHome()");

        return mHome;
    }
    public void setHome(final NavItem home)
    {
        if(Log.D) Log.d("setHome()");

        this.mHome = home;
        NavButton button = mHome.getNavButton();
        button.setNavListener(new NavListener() {
            public void onClick(View v)
            {
                // TBD:
                NavTarget target = DashBar.this.mHome.getTarget();
                DashBar.this.doNavTarget(target);
            }            
        });
        mView.setHomeButton(button);
    }

    public NavItem getMenu()
    {
        if(Log.D) Log.d("getMenu()");

        return mMenu;
    }
    public void setMenu(final NavItem menu)
    {
        if(Log.D) Log.d("setMenu()");

        this.mMenu = menu;
        NavButton button = mMenu.getNavButton();
        button.setNavListener(new NavListener() {
            public void onClick(View v)
            {
                // TBD:
                NavTarget target = DashBar.this.mMenu.getTarget();
                DashBar.this.doNavTarget(target);
            }
        });
        mView.setMenuButton(button);
    }

    
//    public List<NavItem> getItemList()
//    {
//        if(Log.D) Log.d("getItemList()");
//
//        return mItems;
//    }
//    // ???? 
//    private void setItemList(List<NavItem> itemList)
//    {
//        if(Log.D) Log.d("setItemList()");
//
//        this.mItems = itemList;
//        // TBD: Update the view...
//        // ....
//    }

    

    
//
//    public DashBarFragment getFragment()
//    {
//        return fragment;
//    }
//
//
//    public void setFragment(DashBarFragment fragment)
//    {
//        this.fragment = fragment;
//    }

    
    
    
}

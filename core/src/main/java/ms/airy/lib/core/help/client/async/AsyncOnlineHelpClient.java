package ms.airy.lib.core.help.client.async;

import ms.airy.lib.core.help.client.AsyncHelpCallback;
import ms.airy.lib.core.help.client.AsyncHelpClient;
import ms.airy.lib.core.help.client.task.AsyncHelpGetSectionContentTask;


/**
 */
public class AsyncOnlineHelpClient implements AsyncHelpClient,
        AsyncHelpGetSectionContentTask.AsyncTaskCallback
{
    private final AsyncHelpCallback callback;
//    private final AsyncHelpGetSectionContentTask asyncHelpGetSectionContentTask;

    public AsyncOnlineHelpClient(AsyncHelpCallback callback)
    {
        this.callback = callback;
//        this.asyncHelpGetSectionContentTask = new AsyncHelpGetSectionContentTask(this);
    }


    @Override
    public void getSectionContent(long sectionId)
    {
        if(Log.I) Log.i("AsyncOnlineHelpClient.getSectionContent() called with sectionId = " + sectionId);

        (new AsyncHelpGetSectionContentTask(this)).execute(sectionId);
    }


    //////////////////////////////////////
    // AsyncHelpGetSectionContentTask.AsyncTaskCallback interface

    @Override
    public void onGetSectionContent(long sectinId, String content)
    {
        callback.onGetSectionContent(sectinId, content);
    }

}

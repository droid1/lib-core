package ms.airy.lib.core.dialog;

import android.app.DialogFragment;


public interface ActionableAlertDialogHost
{
    DialogFragment addActionable(ActionableAlertDialogListener actionable);
}

package ms.airy.lib.core.phaser;

import ms.airy.lib.core.phaser.common.Feature;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


// TBD:
// ...
// Note:
// We have this dual implementations
// (1) A client app can use a list of Feature objects:
//           A Feature contain all the necessary information
//           regarding disabled/enbaled, user state dependency, etc.
// (2) A client app use use a list of feature codes:
//           The feature's phaser information is stored in each attr's Helper singletons...
// .....
// This was not intentional. During the implementation, we changed the design...
// For now, only option (1) - FeatureManager is implemented...
// ...
public final class FeatureManager
{
    // Current phase;
    private final int currentPhase;

    // FeatureCode -> Feature
    private final Map<String, Feature> features;


//    private FeatureManager()
//    {
//        features = new HashMap<String, Feature>();
//        init();
//    }
//    private void init()
//    {
//        // TBD:
//        // Add all rank- phase- dependent features here ????
//        // ....
//        // ....
////
////        // ...
////        Feature f1 = new Feature("feature-001");
////        features.put(f1.getCode(), f1);
////        // etc..
////
//    }
//
//    // Singleton
//    private static final class FeatureManagerHolder
//    {
//        private static final FeatureManager INSTANCE = new FeatureManager();
//    }
//    public static FeatureManager getInstance()
//    {
//        return FeatureManagerHolder.INSTANCE;
//    }


    // Ctor...
    public FeatureManager(int currentPhase)
    {
        this.currentPhase = currentPhase;
        features = new HashMap<String, Feature>();
    }


    public int getCurrentPhase()
    {
        return currentPhase;
    }
//    public void setCurrentPhase(int currentPhase)
//    {
//        this.currentPhase = currentPhase;
//    }


    public Set<Feature> getFeatures()
    {
        return new HashSet<Feature>(features.values());
    }


    // Add or update
    public void addPhasedFeature(Feature feature)
    {
        features.put(feature.getCode(), feature);
    }
    // Enable the feature completely...
    public void removePhasedFeature(String featureCode)
    {
        features.remove(featureCode);
    }


    public boolean isFeatureAvailable(String featureCode)
    {
        if(! features.containsKey(featureCode)) {
            return true;
        }
        Feature feature = features.get(featureCode);
        if(! feature.isIncludedInPhase(getCurrentPhase())) {
            return false;
        }
        return feature.isFeatureAvailable();
    }
    public boolean isFeatureAvailable(String featureCode, int level)
    {
        if(! features.containsKey(featureCode)) {
            return true;
        }
        Feature feature = features.get(featureCode);
        if(! feature.isIncludedInPhase(getCurrentPhase())) {
            return false;
        }
        return feature.isFeatureAvailable(level);
    }
    public boolean isFeatureAvailable(String featureCode, int level, long userState, long userRole, long userBadge)
    {
        if(! features.containsKey(featureCode)) {
            return true;
        }
        Feature feature = features.get(featureCode);
        if(! feature.isIncludedInPhase(getCurrentPhase())) {
            return false;
        }
        return feature.isFeatureAvailable(level, userState, userRole, userBadge);
    }
    public boolean isFeatureAvailable(String featureCode, int level, long userState, long userRole, long userBadge, int userRank)
    {
        if(! features.containsKey(featureCode)) {
            return true;
        }
        Feature feature = features.get(featureCode);
        if(! feature.isIncludedInPhase(getCurrentPhase())) {
            return false;
        }
        return feature.isFeatureAvailable(level, userState, userRole, userBadge, userRank);
    }


    // TBD:...
    public boolean isFeatureAvailableForUser(String featureCode, String userId)
    {
        // TBD:
        // Get feature from the feature map
        // get user object (or, the user's, state, role, badge, etc...)
        // then call Feature's convenience functions...
        // ....


        return true;
    }



}

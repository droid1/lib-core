package ms.airy.lib.core.notification.handler;

import android.app.Notification;


/**
 */
public interface NotificationEventCallback
{
    void processScheduledNotificationEvent(int notificationId, Notification notification);
}

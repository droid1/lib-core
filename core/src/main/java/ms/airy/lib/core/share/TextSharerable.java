package ms.airy.lib.core.share;

import java.io.Serializable;


// To be implemented by the parent activity (or, possibly, by other sibling fragments, or beans, etc.).
public interface TextSharerable extends Serializable
{
    // Note that the shareable text might be time-dependent.
    // These methods may return different values each time.

    // If returns null, subject is not used.
    String getShareSubject();
    // This cannot return null.
    String getShareText();
}

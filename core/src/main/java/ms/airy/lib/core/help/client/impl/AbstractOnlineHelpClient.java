package ms.airy.lib.core.help.client.impl;

import ms.airy.lib.core.help.client.OnlineHelpClient;


/**
 */
public abstract class AbstractOnlineHelpClient implements OnlineHelpClient
{
    // TBD:
    private String helpWebServiceEndpoint;

    public AbstractOnlineHelpClient()
    {
        this(null);  // ????
    }
    protected AbstractOnlineHelpClient(String helpWebServiceEndpoint)
    {
        this.helpWebServiceEndpoint = helpWebServiceEndpoint;
    }

    public String getHelpWebServiceEndpoint()
    {
        return helpWebServiceEndpoint;
    }
    protected void setHelpWebServiceEndpoint(String helpWebServiceEndpoint)
    {
        this.helpWebServiceEndpoint = helpWebServiceEndpoint;
        // TBD:
        // reset the miniclient...
        // ...
    }


}

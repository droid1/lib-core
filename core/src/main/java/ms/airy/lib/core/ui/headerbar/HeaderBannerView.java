package ms.airy.lib.core.ui.headerbar;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

public class HeaderBannerView extends LinearLayout
{
    // ....
    private TextView mTitleView = null;
    private TextView mCaptionView = null;

    public HeaderBannerView(Context context)
    {
        super(context);
        init();
        //...
    }

    public HeaderBannerView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
        //...
    }

    public HeaderBannerView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        init();
        //...
    }

    
    
    private void init()
    {
        if(Log.D) Log.d("init()");

        // TBD
        setOrientation(VERTICAL);
        setMinimumWidth(200);  // temporary
        // ...

        mTitleView = new TextView(getContext());
        mCaptionView = new TextView(getContext());

        
        // Redo the layout...
        layoutChildViews();

    }
    
    
    // TBD:
    private void layoutChildViews()
    {
        if(Log.D) Log.d("layoutChildViews()");
        
        
        // [0] Remove all views first.
        removeAllViews();

        // [1] Title
        if(mTitleView != null) {
            LinearLayout.LayoutParams homeParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT); // ????
            addView(mTitleView, homeParams);
        }        

        // [2] Caption
        if(mCaptionView != null) {
            LinearLayout.LayoutParams homeParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT); // ????
            addView(mCaptionView, homeParams);
        }
        
        // ?????
        invalidate();
    }
    
    
    public CharSequence getTitle()
    {
        return mTitleView.getText();
    }
    public void setTitle(CharSequence title)
    {
        mTitleView.setText(title);
    }

    public CharSequence getCaption()
    {
        return mCaptionView.getText();
    }
    public void setCaption(CharSequence caption)
    {
        mCaptionView.setText(caption);
    }

    
}

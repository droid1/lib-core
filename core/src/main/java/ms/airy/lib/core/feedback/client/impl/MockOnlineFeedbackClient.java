package ms.airy.lib.core.feedback.client.impl;

import ms.airy.lib.core.feedback.client.OnlineFeedbackClient;


/**
 */
public class MockOnlineFeedbackClient implements OnlineFeedbackClient
{
    public MockOnlineFeedbackClient()
    {
    }

    @Override
    public void sendFeedback(long surveyEntryId, int answer, String comment)
    {
        if(Log.I) Log.i("MockOnlineFeedbackClient.sendFeedback(). surveyEntryId = " + surveyEntryId + "; answer = " + answer + "; comment = " + comment);

    }

}

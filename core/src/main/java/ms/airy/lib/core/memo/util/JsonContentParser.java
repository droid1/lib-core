package ms.airy.lib.core.memo.util;

import ms.airy.lib.core.memo.common.MemoInfo;
import ms.airy.lib.core.memo.common.MemoType;
import ms.airy.lib.core.util.IdUtil;

import org.miniclient.json.LiteJsonParser;
import org.miniclient.json.MiniClientJsonException;
import org.miniclient.json.parser.MiniClientJsonParser;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;
import java.util.Map;


/**
 */
public final class JsonContentParser
{
    private JsonContentParser() {}


    public static MemoInfo buildMemoInfo(InputStream inputStream)
    {
        MemoInfo memoInfo = null;

        Map<String,Object> jsonRoot = parseJsonContent(inputStream);
        if(jsonRoot != null) {
            memoInfo = buildMemoInfo(jsonRoot);
        }

        return memoInfo;
    }


    private static Map<String,Object> parseJsonContent(InputStream inputStream)
    {
        Map<String,Object> jsonRoot = null;
        LiteJsonParser jsonParser = new MiniClientJsonParser();
        Reader reader = new InputStreamReader(inputStream);
        try {
            // ???
            jsonRoot = (Map<String,Object>) ((Map<?,?>) jsonParser.parse(reader));
            if(Log.D) {
                Log.d("Parsed: jsonRoot = " + jsonRoot);
            }
        } catch (MiniClientJsonException e) {
            Log.e("Failed to parse the json content.", e);
        } catch (IOException e) {
            Log.e("Error while parsing the json content.", e);
        }

        return jsonRoot;
    }

    private static MemoInfo buildMemoInfo(Map<String,Object> jsonRoot)
    {
        return null;
    }


}

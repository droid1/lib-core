package ms.airy.lib.core.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.Random;
 
public class CoreUtil
{
	private CoreUtil() {}
	 
    private static String convertToHex(byte[] data)
    {
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < data.length; i++) {
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                if ((0 <= halfbyte) && (halfbyte <= 9))
                    buf.append((char) ('0' + halfbyte));
                else
                    buf.append((char) ('a' + (halfbyte - 10)));
                halfbyte = data[i] & 0x0F;
            } while(two_halfs++ < 1);
        }
        return buf.toString();
    }

    public static String SHA1(String text) 
        throws NoSuchAlgorithmException, UnsupportedEncodingException
    {
		MessageDigest md;
		md = MessageDigest.getInstance("SHA-1");
		byte[] sha1hash = new byte[40];
		md.update(text.getBytes("iso-8859-1"), 0, text.length());
		sha1hash = md.digest();
		return convertToHex(sha1hash);
    }



    // TBD: Move to a singleton class where user-specific info (e.g., locale) can be used.
    public static String generateDateString(int year, int month, int day)
    {
        // TBD: Get the user's date format from Locale (or, from pref.)
        // Locale is preferred.
        // ?? what happens to the stored dates if the user changes the prefs? (or, even locale) 
        String dateStr = new StringBuilder()
        .append(month + 1).append("/") // Month is 0 based so add 1
        .append(day).append("/")
        .append(year).toString();
        return dateStr;
    }

    // [year, month, day]
    public static int[] parseDateString(String dateStr)
    {
        String[] parts = null;
        
        if(dateStr != null) {
            try {
                parts = dateStr.split("/");
            } catch(Exception ex) {
                // ignore...
                if(Log.W) Log.w("Date string parse error. ", ex);
            }
        }
        
        int[] dateInt = new int[3];
        
        // Default value. (today)
        final Calendar c = Calendar.getInstance();
        dateInt[0] = c.get(Calendar.YEAR);
        dateInt[1] = c.get(Calendar.MONTH);
        dateInt[2] = c.get(Calendar.DAY_OF_MONTH);

        if(parts == null || parts.length < 2) {
            // Use just today.
            if(Log.W) Log.w("Date string format is not recognized. Using today.");
        } else {
            if(parts.length > 2) {  // What if it's > 3?
                try {
                    dateInt[0] = Integer.parseInt(parts[2]);
                    if(dateInt[0] < 100) {
                        dateInt[0] += 2000;  // In case the year is written with the last two decimals only.
                    }
                } catch(NumberFormatException ex) {
                    // ignore
                    if(Log.V) Log.v("Year not recognized", ex);
                }
            }
            try {
                dateInt[1] = Integer.parseInt(parts[0]) - 1;
            } catch(NumberFormatException ex) {
                // ignore
                if(Log.V) Log.v("Month not recognized", ex);
            }
            try {
                dateInt[2] = Integer.parseInt(parts[1]);
            } catch(NumberFormatException ex) {
                // ignore
                if(Log.V) Log.v("Day not recognized", ex);
            }           
        }
        
        return dateInt;
    }
    
    
    
    // TBD
    public static String createDeviceId()
    {
        String deviceId = null;
        
        // Unique string...
        // temporary
        Random RNG = new Random();
        long r = RNG.nextLong();
        String text = (new Date()).toString() + Long.toString(r);
        try {
            deviceId = CoreUtil.SHA1(text);
        } catch(Exception ex) {
            if(Log.W) Log.w("Failed to create a unique device Id through SHA-1 hash.", ex);
            // What to do???
            //deviceId = System.getString(mContext.getContentResolver(), android.Provider.Settings.System.ANDROID_ID);
            Formatter formatter = new Formatter();
            deviceId = formatter.format("%40s", Long.toString(r)).toString();  // ???
            /*
            <uses-permission android:name="android.permission.READ_PHONE_STATE"></uses-permission>
            TelephonyManager mTelephonyMgr = TelephonyManager)getSystemService(TELEPHONY_SERVICE); 
            String imei = mTelephonyMgr.getDeviceId(); // Requires READ_PHONE_STATE
            String phoneNumber=mTelephonyMgr.getLine1Number(); // Requires READ_PHONE_STATE
            String softwareVer = mTelephonyMgr.getDeviceSoftwareVersion(); // Requires READ_PHONE_STATE
            String simSerial = mTelephonyMgr.getSimSerialNumber(); // Requires READ_PHONE_STATE
            String subscriberId = mTelephonyMgr.getSubscriberId(); // Requires READ_PHONE_STATE
            */
        }
        
        return deviceId;
    }
    
}

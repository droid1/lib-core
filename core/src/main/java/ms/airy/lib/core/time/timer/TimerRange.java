package ms.airy.lib.core.time.timer;


/**
 */
public class TimerRange
{
    // Count down timer.
    // That is, it starts from max and counts down to min.
    // Note that max/min are NOT time (as in unit epoch time).
    // Although it's up to the client,
    // it's good to use some time kind of time (millis) based values.
    // Then, getDuration(), for example, returns the duration in millis, etc.

    // min <= max
    // the current val should be generally min <= current val <= max,
    //    but, not always, since min/max can be updated.

    // TBD: Make min/max final ???
    private long max;
    private long min;
    private long threshold;    // min <= threshold < max. val below threhold is considered "bad", critical, etc...
    private long val;   // current value

    public TimerRange(long max)
    {
        this(max, 0L);   // max should be a positive number...
    }
    public TimerRange(long max, long min)
    {
        this(max, min, min);  // min == threshold mean there is no threshold...
    }
    public TimerRange(long max, long min, long threshold)
    {
        // TBD: Validate min/max ???
        this.min = min;
        this.max = max;
        setThreshold(threshold);
        reset();
    }


    public final long getMax()
    {
        return max;
    }
    public void setMax(long max)
    {
        this.max = max;
        if(min > max) {
            Log.w("New max, " + max + ", is smaller than the current min, " + min + ". Resetting the min.");
            this.min = max;  // ???
        }
        setThreshold(this.threshold);
    }

    public final long getMin()
    {
        return min;
    }
    public void setMin(long min)
    {
        this.min = min;
        if(min > max) {
            Log.w("New min, " + min + ", is bigger than the current max, " + max + ". Resetting the max.");
            this.max = min;  // ???
        }
        setThreshold(this.threshold);
    }

    public final long getThreshold()
    {
        return threshold;
    }
    public final long setThreshold(long threshold)
    {
        if(min > threshold) {
            this.threshold = min;
        } else if(max < threshold) {
            this.threshold = max;
        } else {
            this.threshold = threshold;
        }
        return this.threshold;
    }

    public final long getVal()
    {
        return val;
    }
    public long setVal(long val)
    {
        if(min > val) {
            this.val = min;
        } else if(max < val) {
            this.val = max;
        } else {
            this.val = val;
        }
        return this.val;
    }


    public final float getFraction()
    {
        return ((float) (this.val - this.min)) / (this.max - this.min);
    }

    public final long getDuration()
    {
        return (this.max - this.min);
    }


    public final boolean isBelowThreshold()
    {
        return (val < threshold);
    }
    public final boolean isAboveThreshold()
    {
        return (val > threshold);
    }


    // Rewind.
    public final void reset()
    {
        this.val = max;
    }

    // Reset the timer range.
    public final void reset(long max)
    {
        reset(max, this.min);
    }
    public final void reset(long max, long min)
    {
        reset(max, min, min);
    }
    public final void reset(long max, long min, long threshold)
    {
        this.max = max;
        this.min = min;
        setThreshold(threshold);
        reset();
    }

    // End the timer.
    public final void finish()
    {
        this.val = this.min;
    }


}

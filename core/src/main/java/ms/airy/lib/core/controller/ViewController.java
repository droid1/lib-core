package ms.airy.lib.core.controller;

import android.content.Context;
import android.view.View;


// tbd
public interface ViewController
{
    View addEventListeners(Context context, View rootView);
    View refreshViewData(Context context, View rootView);
    // etc....
}

package ms.airy.lib.core.helper;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import ms.airy.lib.core.common.CommonConstants;
import ms.airy.lib.core.prefs.PrefsHelper;


// Cf. /nofitication/NotificationHelper...
public class NotificationHelper
{
    // temporary
    protected static final int RUNTIME_OS_VERSION = android.os.Build.VERSION.SDK_INT;

	// context
	private Context mContext;

	// System/User preferences
	private PrefsHelper mPrefs;
   
    // Lazy initialization.
    private PrefsHelper getPrefsHelper() {
    	if(mPrefs == null) {
            mPrefs = new PrefsHelper(mContext);
    	}
    	return mPrefs;
    }

	public NotificationHelper(Context context)
	{
		mContext = context;
	}

	// To be removed.... ????
	// tmeporary
	public void setNotification(int notificationId, CharSequence tickerText, Long timeMillis)
	{
		// TBD:
		//     Read user's prefs first.
		
		// Notification service.
		NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
		
//		if(tickerText == null) {
//			// TBD
//			tickerText = "Memoroid";
//		}
		if(timeMillis == null) {
			timeMillis = System.currentTimeMillis() + 10L * 1000L;  // 10 seconds from now....
		}

        // ????
		Notification notification = new Notification(android.R.drawable.ic_notification_overlay, tickerText, timeMillis);
//        Notification notification = new Notification.Builder(mContext)
//                .setContentText(contentText)    // ???
//                .setContentTitle(contentTitle)   // ???
//                .setTicker(tickerText)         // ???
//                .setSmallIcon(android.R.drawable.ic_notification_overlay)
//                .setWhen(timeMillis)
//                .build();

		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		
		Context appContext = mContext.getApplicationContext();  // ????
		boolean toNotify = false;
		CharSequence contentTitle = null;
		CharSequence contentText = null;
		Intent notificationIntent = null;
		switch(notificationId) {
		// ????
		case CommonConstants.NOTIFICATION_ID_MEMO:
			toNotify = true;
			contentTitle = "My note notification";
			contentText = "Note is due!";
			//notificationIntent = new Intent(mContext, MemoList.class);
            //notificationIntent = new Intent(CommonConstants.ACTION_NEW_MEMO);
			break;
		default:
			if(Log.W) Log.w("notificationId not recognized: " + notificationId);
			break;
		}
		
		if(toNotify == true) {
		    // ????
		    //PendingIntent contentIntent = PendingIntent.getActivity(mContext.getApplicationContext(), 0, notificationIntent, 0);
            PendingIntent contentIntent = PendingIntent.getActivity(mContext, 0, notificationIntent, 0);
			notification.setLatestEventInfo(appContext, contentTitle, contentText, contentIntent);
		
			notificationManager.notify(notificationId, notification);
		} else {
			if(Log.I) Log.i("Notification not set.");
		}
	}

	
	
	// TBD:
	// This only works > 3.0 ???
	// ...
	
	public void setNotification(NotificationStruct notificationStruct)
	{
//	    if(notificationStruct == null) {
//	        // throw exception?
//	    }
	    int id = notificationStruct.getId();
	    if(id <= 0) {
            // Something's wrong.
            if(Log.W) Log.w("notificationId is invalid: " + id);
            return;
	    }
	    
	    
	    // TBD:
	    // Validate the notificationId, e.g., against registry/hash table...
	    // ...
	    
	    
	    int icon = notificationStruct.getIcon();
	    if(icon <= 0) {
	        // TBD: Use the predefined icon for the particular notificationId, if any.
	        // Otherwise, just use default icon.
	        icon = android.R.drawable.ic_notification_overlay;
	    }
	    
	    long now = System.currentTimeMillis();
	    long time = notificationStruct.getTime();
	    if(time < now) {
	        time = now; // ????
	    }

	    // TBD:
	    
//	    Notification.Builder builder = new Notification.Builder(mContext)
//        .setSmallIcon(icon)
//        .setWhen(time)
//        .setTicker(notificationStruct.getTickerText())
//        .setContentTitle(notificationStruct.getContentTitle())
//        .setContentText(notificationStruct.getContentText());

//	    Intent notificationIntent = null;
//	    String action = notificationStruct.getContentAction();
//	    if(action != null) {
//	        // TBD: Validate the action, e.g., against the registry/hash table...
//	        // ....
//	        
//	        Uri uri = notificationStruct.getContentUri();
//	        notificationIntent = new Intent(action, uri);
//	        PendingIntent contentIntent = PendingIntent.getActivity(mContext, 0, notificationIntent, 0);
//	        builder = builder.setContentIntent(contentIntent);
//	    } else {
//            if(Log.I) Log.i("No intent has been set because action is null.");	        
//	    }
//
//	    Notification notification = builder.getNotification();
	    
//	    // Notification service.
//        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
//        notificationManager.notify(id, notification);
	}



}

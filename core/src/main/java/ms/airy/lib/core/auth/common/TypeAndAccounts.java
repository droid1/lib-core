package ms.airy.lib.core.auth.common;

import android.accounts.Account;


// Primarily to be used as a return value from type/account selection methods.
public class TypeAndAccounts
{
    private final int type;
    private Account[] accounts;

    public TypeAndAccounts(int type)
    {
        this(type, null);
    }
    public TypeAndAccounts(int type, Account[] accounts)
    {
        this.type = type;
        this.accounts = accounts;
    }

    public int getType()
    {
        return type;
    }

    public Account[] getAccounts()
    {
        return accounts;
    }
    public void setAccounts(Account[] accounts)
    {
        this.accounts = accounts;
    }

    public String[] getAccountNames()
    {
        String[] names = new String[accounts.length];
        for (int i = 0; i < names.length; i++) {
            names[i] = accounts[i].name;
        }
        return names;
    }

    public int getSize()
    {
        int size = 0;
        if(accounts != null) {
            size = accounts.length;
        }
        return size;
    }
}

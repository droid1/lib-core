package ms.airy.lib.core.status.client.async;

import ms.airy.lib.core.status.client.AsyncStatusSendCallback;
import ms.airy.lib.core.status.client.AsyncStatusSendClient;
import ms.airy.lib.core.status.client.impl.*;
import ms.airy.lib.core.status.client.task.AsyncStatusSendMessageTask;
import ms.airy.lib.core.status.client.task.AsyncStatusSendStatusMessageTask;
import ms.airy.lib.core.status.common.StatusMessage;


/**
 */
public class AsyncStatusMessageSendClient implements AsyncStatusSendClient,
        AsyncStatusSendMessageTask.AsyncTaskCallback,
        AsyncStatusSendStatusMessageTask.AsyncTaskCallback
{
    private final AsyncStatusSendCallback callback;
    private final AsyncStatusSendMessageTask asyncStatusSendMessageTask;
    private final AsyncStatusSendStatusMessageTask asyncStatusSendStatusMessageTask;


    public AsyncStatusMessageSendClient(AsyncStatusSendCallback callback)
    {
        this.callback = callback;
        asyncStatusSendMessageTask = new AsyncStatusSendMessageTask(this);
        asyncStatusSendStatusMessageTask = new AsyncStatusSendStatusMessageTask(this);
    }


    @Override
    public void sendMessage(int type, String message)
    {
        if(Log.I) Log.i("AsyncStatusMessageSendClient.sendMessage() type = " + type + "; message = " + message);

        asyncStatusSendMessageTask.execute(type, message);
    }

    @Override
    public void sendStatusMessage(StatusMessage statusMessage)
    {
        if(Log.I) Log.i("AsyncStatusMessageSendClient.sendStatusMessage() statusMessage = " + statusMessage);

        asyncStatusSendStatusMessageTask.execute(statusMessage);
    }



    //////////////////////////////////////
    // AsyncStatusSendMessageTask.AsyncTaskCallback interface

    @Override
    public void onSendMessage(long id, int type, String message)
    {
        callback.onSendMessage(id, type, message);
    }


    //////////////////////////////////////
    // AsyncStatusSendStatusMessageTask.AsyncTaskCallback interface

    @Override
    public void onSendStatusMessage(StatusMessage statusMessage)
    {
        callback.onSendStatusMessage(statusMessage);
    }


}

package ms.airy.lib.core.feedback.client.async;

import ms.airy.lib.core.feedback.client.AsyncFeedbackCallback;
import ms.airy.lib.core.feedback.client.AsyncFeedbackClient;
import ms.airy.lib.core.feedback.client.task.AsyncFeedbackSendFeedbackTask;


/**
 */
public class AsyncOnlineFeedbackClient implements AsyncFeedbackClient,
        AsyncFeedbackSendFeedbackTask.AsyncTaskCallback
{
    private final AsyncFeedbackCallback callback;
//    private final AsyncFeedbackSendFeedbackTask asyncFeedbackSendFeedbackTask;

    public AsyncOnlineFeedbackClient(AsyncFeedbackCallback callback)
    {
        this.callback = callback;
//        this.asyncFeedbackSendFeedbackTask = new AsyncFeedbackSendFeedbackTask(this);
    }


    @Override
    public void sendFeedback(long surveyEntryId, int answer, String comment)
    {
        if(Log.I) Log.i("AsyncOnlineFeedbackClient.sendFeedback(). surveyEntryId = " + surveyEntryId + "; answer = " + answer + "; comment = " + comment);

        (new AsyncFeedbackSendFeedbackTask(this)).execute(surveyEntryId, answer, comment);
    }



    //////////////////////////////////////
    // AsyncFeedbackSendFeedbackTask.AsyncTaskCallback interface

    @Override
    public void onSendFeedback(long surveyEntryId, int answer, String comment)
    {
        callback.onSendFeedback(surveyEntryId, answer, comment);
    }


}

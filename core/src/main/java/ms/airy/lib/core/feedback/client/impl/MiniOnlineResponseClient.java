package ms.airy.lib.core.feedback.client.impl;

import org.miniclient.ApiUserClient;
import org.miniclient.RestApiException;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ms.airy.lib.core.feedback.client.OnlineResponseClient;
import ms.airy.lib.core.feedback.common.ResponseEntry;


public class MiniOnlineResponseClient extends AbstractOnlineFeedbackClient implements OnlineResponseClient
{

    private ApiUserClient miniFeedbackWebClient = null;

    public MiniOnlineResponseClient()
    {
        this((String) null);
    }
    public MiniOnlineResponseClient(String feedbackWebServiceEndpoint)
    {
        super(feedbackWebServiceEndpoint);
        this.miniFeedbackWebClient = MiniClientBuilder.getInstance().getFeedbackWebClient(feedbackWebServiceEndpoint);
    }
    public MiniOnlineResponseClient(ApiUserClient miniFeedbackWebClient)
    {
        super(miniFeedbackWebClient.getResourceBaseUrl());
        this.miniFeedbackWebClient = miniFeedbackWebClient;
    }

    public ApiUserClient getMiniFeedbackWebClient()
    {
        return miniFeedbackWebClient;
    }
    protected void setMiniFeedbackWebClient(String feedbackWebServiceEndpoint)
    {
        super.setFeedbackWebServiceEndpoint(feedbackWebServiceEndpoint);
        this.miniFeedbackWebClient = MiniClientBuilder.getInstance().getFeedbackWebClient(feedbackWebServiceEndpoint);
    }
    protected void setMiniFeedbackWebClient(ApiUserClient miniFeedbackWebClient)
    {
        super.setFeedbackWebServiceEndpoint(miniFeedbackWebClient.getResourceBaseUrl());
        this.miniFeedbackWebClient = miniFeedbackWebClient;
    }

    @Override
    protected void setFeedbackWebServiceEndpoint(String feedbackWebServiceEndpoint)
    {
        super.setFeedbackWebServiceEndpoint(feedbackWebServiceEndpoint);
        this.miniFeedbackWebClient = MiniClientBuilder.getInstance().getFeedbackWebClient(feedbackWebServiceEndpoint);
    }



    @Override
    public ResponseEntry getResponse(long responseId)
    {
        if(Log.I) Log.i("MiniOnlineResponseClient.getResponse(). responseId = " + responseId);

        Object result = null;
        try {
            // TBD:
            Map<String,Object> params = new HashMap<String, Object>();
            params.put("responseId", responseId);  // ???
            List<Object> objs = miniFeedbackWebClient.list(params);
            if(objs != null && objs.size() > 0) {
                result = objs.get(0);   // ???
            }
        } catch (RestApiException e) {
            Log.e("Failed to get response for responseId = " + responseId, e);
        } catch (IOException e) {
            Log.e("IO Exception while fetching response for responseId = " + responseId, e);
        }

        ResponseEntry responseEntry = null;
        if(result != null) {
            // TBD:
            // convert the map to responseEntry
            // ...
        }

        return responseEntry;
    }

    @Override
    public List<Long> getNewResponses()
    {
        if(Log.I) Log.i("MiniOnlineResponseClient.getNewResponses().");

        // TBD:
        // ...
        
        return null;
    }

}

package ms.airy.lib.core.control;

import android.content.Context;
import android.util.AttributeSet;


// TBD:
// Control that has a fixed aspect ratio...
// ...

public abstract class FixedShapeControl extends AnamorphicControl
{
    // tbd:
    // aspect ratio ???
//     private boolean aspectRatioFixed = false;  // --> Just use (aspectRatio == null).
    // note that we define aspect ratio as a string/constant, "w:h", not as a float.
    // This value is for axis == "horz"  (NOT "vert", as is more reasonable for a phone device)
    //   When the device is in "vert", we need to "flip" this value.
    private String aspectRatio;
    // ???
    // Note that if aspectAxisDependent == false,
    //     then we use the same (or, flipped/inverted, to be more precise) aspect ratios
    //     for vert and horz.
    // if aspectAxisDependent == true,
    //     then we can potentially use different aspect ratios for vert and horz.
    // This is however way too complicated...
    // For now, just use a single fixed value for horz and vert (flipped).
//    private boolean aspectAxisDependent;
//    private final Map<Integer, String> axisAspectRatioMap = new HashMap<Integer, String>();
    // ???
//    private boolean aspectOrientationDependent;
//    private final Map<Integer, String> orientationAspectRatioMap = new HashMap<Integer, String>();
    // ???

    public FixedShapeControl(Context context)
    {
        super(context);
        initControl(context, null, 0);
    }

    public FixedShapeControl(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        initControl(context, attrs, 0);
    }

    public FixedShapeControl(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        initControl(context, attrs, defStyle);
    }
    // tbd
    private void initControl(Context context, AttributeSet attrs, int defStyle)
    {
        // ...
        aspectRatio = null;

    }


    // tbd
    // Use the example: FixedShapeFrameLayout...
    // ...



}

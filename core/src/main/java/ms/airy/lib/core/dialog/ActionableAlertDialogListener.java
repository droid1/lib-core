package ms.airy.lib.core.dialog;


/**
 * To be used for "AlertDialog" based dialogs with positive/negative buttons.
 * E.g., ok/cancel, yes/no, etc....
 * Note that all Actionable dialogFragments require
 *    the parent activities implement ActionableAlertDialog/ActionableChoiceDialog.
 * This also means that
 *    Actionable dialogFragments can only be shown from (known/pre-selected) activities.
 * On the other hand,
 *    Simple dialogFragments do not require their parent activities implement Actionable interface.
 *    It can be implemented by any class (including non-activity classes).
*/
public interface ActionableAlertDialogListener extends ActionableDialogListener
{
    // "Negative" action. Could be used as "neutral" as well.
    // e.g., Cancel, Dismiss, Ignore, ...
    // In the three-button version (ActionaableResponseDialog),
    //    which action is "negative" and "neutral" may depend on the individual implementation. ???
    void onNegativeClick(String tag);
}

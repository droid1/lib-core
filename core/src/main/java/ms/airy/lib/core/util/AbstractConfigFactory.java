package ms.airy.lib.core.util;

import ms.airy.lib.core.common.Config;

public abstract class AbstractConfigFactory
{
    public abstract Config getConfig();
}

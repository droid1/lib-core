package ms.airy.lib.core.help.content;


/**
 */
public final class LocalLoaderUtil extends CommonLoaderUtil
{
    private LocalLoaderUtil() {}



    // Help file name: help-011.html  --> Section id: 11.
    public static long getSectionIdFromFileName(String fileName)
    {
        if(fileName == null || !fileName.endsWith(".html")) {
            return 0L;
        }
        int i1 = fileName.lastIndexOf(".");
        if(i1 == -1) {
            i1 = fileName.length();  // No suffix/extension.
        }
        int i2 = fileName.lastIndexOf("-");
        if(i2 == -1) {
            return 0L;   // ???
        }
        String idStr = fileName.substring(i2+1, i1);
        long sectionId = 0L;
        try {
            sectionId = Long.parseLong(idStr);
        } catch(NumberFormatException e) {
            // ignore
            Log.w("Invalid section Id: " + idStr, e);
        }
        return sectionId;
    }


}

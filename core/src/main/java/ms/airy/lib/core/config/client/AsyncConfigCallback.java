package ms.airy.lib.core.config.client;


public interface AsyncConfigCallback
{
//    void onGetProperty(String value, String key);
//    void onGetProperty(String value, String key, String defValue);
//    void onSetProperty(String key, String value);
    void onGetProperty(Object value, String key);
    void onSetProperty(boolean result, String key, Object value);
}

package ms.airy.lib.core.help.client.factory;

import ms.airy.lib.core.help.client.OnlineHelpClient;
import ms.airy.lib.core.help.client.impl.MiniOnlineHelpClient;
import ms.airy.lib.core.help.client.impl.MockOnlineHelpClient;


/**
 */
public class HelpClientFactory
{
    // temporary
    private static final int CLIENT_MOCK = 1;
    private static final int CLIENT_MINI = 2;
    // ...

    private static HelpClientFactory INSTANCE = null;

    public static HelpClientFactory getInstance()
    {
        if(INSTANCE == null) {
            INSTANCE = new HelpClientFactory();
        }
        return INSTANCE;
    }

    // mock or mini
    private int type;

    private OnlineHelpClient client = null;
    private HelpClientFactory()
    {
        // TBD:
        type = CLIENT_MOCK;
        // ...
    }

    public void useMockClient()
    {
        type = CLIENT_MOCK;
        client = null;
    }
    public void useMiniClient()
    {
        type = CLIENT_MINI;
        client = null;
    }

    public OnlineHelpClient getClient()
    {
        if(client == null) {
            switch(type) {
                case CLIENT_MOCK:
                default:
                    client = new MockOnlineHelpClient();
                    break;
                case CLIENT_MINI:
                    client = new MiniOnlineHelpClient();
                    break;
            }
        }
        return client;
    }


}

package ms.airy.lib.core.auth.helper;

import android.accounts.Account;
import android.app.Activity;
import android.content.Context;

import java.util.HashSet;
import java.util.Set;

import ms.airy.lib.core.auth.common.AccountType;
import ms.airy.lib.core.auth.config.AppAuthConfigManager;
import ms.airy.lib.core.auth.oauth2.OAuth2AuthCallback;
import ms.airy.lib.core.auth.oauth2.OAuth2Helper;
import ms.airy.lib.core.auth.prefs.AccountPrefsStoreManager;
import ms.airy.lib.core.auth.util.OAuth2ScopeUtil;


// TBD:
// To be used for account auth management.
// AuthHelper is implemented as a singleton.
//   --> Does this make sense???
//   because we need to use callbacks, we may end up with a lot (old/stale) callbacks...
public final class AuthHelper implements OAuth2AuthCallback
{
    private static AuthHelper INSTANCE = null;
    public static AuthHelper getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new AuthHelper(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;

    private AuthHelper(Context context)
    {
        this.context = context;
        OAuth2Helper.getInstance(context).addOAuth2AuthCallback(this);
    }



    // temporary.
    // we assume, for now, that as long as we have non-null authToken for the account,
    // we are authorized.
    // TBD: Check the authTime as well????
    // ...
    // To re-iterate,
    //   The return value of true for "isAuthorized()" functions deos not mean the account is currently authorized.
    //   The auth might have expired, or the user may have been revoked the auth or removed the account from the device, etc.

    public boolean isAccountAuthorized(int type, String name)
    {
        // temporary
        String authToken = AccountPrefsStoreManager.getInstance(context).getAuthToken(type, name);
        return (authToken != null && !authToken.isEmpty());
    }
    public boolean isAccountAuthorized(int type, String name, Set<String> requiredScopeSet)
    {
        // temporary
        String authToken = AccountPrefsStoreManager.getInstance(context).getAuthToken(type, name);
        if(authToken == null || authToken.isEmpty()) {
            return false;
        }
        Set<String> authScopeSet = AccountPrefsStoreManager.getInstance(context).getAuthScopeSet(type, name);
        return (OAuth2ScopeUtil.isScopeSufficient(authScopeSet, requiredScopeSet));
    }

    public boolean isMainAccountAuthorized(int type)
    {
        String name = AccountHelper.getInstance(context).getMainAccountName(type);
        if(name == null) {
            // No main account.
            return false;
        }
        String authToken = AccountPrefsStoreManager.getInstance(context).getAuthToken(type, name);
        return (authToken != null && !authToken.isEmpty());
    }
    public boolean isMainAccountAuthorized(int type, Set<String> requiredScopeSet)
    {
        String name = AccountHelper.getInstance(context).getMainAccountName(type);
        if(name == null) {
            // No main account.
            return false;
        }
        String authToken = AccountPrefsStoreManager.getInstance(context).getAuthToken(type, name);
        if(authToken == null || authToken.isEmpty()) {
            return false;
        }
        Set<String> authScopeSet = AccountPrefsStoreManager.getInstance(context).getAuthScopeSet(type, name);
        return (OAuth2ScopeUtil.isScopeSufficient(authScopeSet, requiredScopeSet));
    }

    public boolean isPrincipalAccountAuthorized()
    {
        int type = AppAuthConfigManager.getInstance(context).getApplicationAuthConfig().getPrincipalAccountType();
        if(AccountTypeUtil.isValid(type)) {
            return isMainAccountAuthorized(type);
        } else {
            Log.w("Invalid principal account type = " + type);
            return false;
        }
    }
    public boolean isPrincipalAccountAuthorized(Set<String> requiredScopeSet)
    {
        int type = AppAuthConfigManager.getInstance(context).getApplicationAuthConfig().getPrincipalAccountType();
        if(AccountTypeUtil.isValid(type)) {
            return isMainAccountAuthorized(type, requiredScopeSet);
        } else {
            Log.w("Invalid principal account type = " + type);
            return false;
        }
    }


    public boolean isPrimaryAccountAuthorized()
    {
        int type = AccountHelper.getInstance(context).getPrimaryAccountType();
        String name = AccountHelper.getInstance(context).getPrimaryAccountName();
        if(name == null) {
            // No primary account.
            return false;
        }
        String authToken = AccountPrefsStoreManager.getInstance(context).getAuthToken(type, name);
        return (authToken != null && !authToken.isEmpty());
    }
    public boolean isPrimaryAccountAuthorized(Set<String> requiredScopeSet)
    {
        int type = AccountHelper.getInstance(context).getPrimaryAccountType();
        String name = AccountHelper.getInstance(context).getPrimaryAccountName();
        if(name == null) {
            // No primary account.
            return false;
        }
        String authToken = AccountPrefsStoreManager.getInstance(context).getAuthToken(type, name);
        if(authToken == null || authToken.isEmpty()) {
            return false;
        }
        Set<String> authScopeSet = AccountPrefsStoreManager.getInstance(context).getAuthScopeSet(type, name);
        return (OAuth2ScopeUtil.isScopeSufficient(authScopeSet, requiredScopeSet));
    }


    // TBD
    public boolean isMainGoogleAccountAuthorized()
    {
        return isMainAccountAuthorized(AccountType.TYPE_GOOGLE);
    }
    public boolean isMainGoogleAccountAuthorized(Set<String> requiredScopeSet)
    {
        return isMainAccountAuthorized(AccountType.TYPE_GOOGLE, requiredScopeSet);
    }


    // TBD:
    // It returns null if no auth has been attempted before,
    //    or if the last attempt has been failed.
    public String getLastAuthFailureReason(int type, String name)
    {
        return AccountPrefsStoreManager.getInstance(context).getAuthFailureReason(type, name);
    }


    // TBD:
    // The following methods return true if auth has been attempted
    //     regardless of the auth has succeeded or not.
    // Note that when the user explicitly unauthorises,
    //     we remove the records from the Shared Prefs,
    //     and hence these methods will return false.

    public boolean isAuthAttemptedForAccount(int type, String name)
    {
        long authTime = AccountPrefsStoreManager.getInstance(context).getAuthTime(type, name);
        return (authTime > 0L);
    }
    public boolean isAuthAttemptedForMainAccount(int type)
    {
        String name = AccountHelper.getInstance(context).getMainAccountName(type);
        return isAuthAttemptedForAccount(type, name);
    }
    public boolean isAuthAttemptedForPrincipalAccount()
    {
        int type = AppAuthConfigManager.getInstance(context).getApplicationAuthConfig().getPrincipalAccountType();
        if(AccountTypeUtil.isValid(type)) {
            return isAuthAttemptedForMainAccount(type);
        } else {
            Log.w("Invalid principal account type = " + type);
            return false;
        }
    }
    public boolean isAuthAttemptedForPrimaryAccount()
    {
        Account account = AccountHelper.getInstance(context).getPrimaryAccount();
        if(account == null) {
            return false;
        }
        return isAuthAttemptedForAccount(AccountTypeUtil.getAccountType(account.type), account.name);
    }



    // TBD
    // Logins are async methods, whereas logouts are synchronous...

    // login
    public void authorizeForPrimaryAccount(Set<String> authScopeSet, boolean notifyAuthFailure)
    {
        Account account = AccountHelper.getInstance(context).getPrimaryAccount();
        if(account != null) {
            authorizeForAccount(AccountTypeUtil.getAccountType(account.type), account.name, authScopeSet, notifyAuthFailure);
        } else {
            // ???
        }
    }
    public void authorizeForPrimaryAccount(Set<String> authScopeSet, Activity activity)
    {
        Account account = AccountHelper.getInstance(context).getPrimaryAccount();
        if(account != null) {
            authorizeForAccount(AccountTypeUtil.getAccountType(account.type), account.name, authScopeSet, activity);
        } else {
            // ???
        }
    }

    // logout
    public void unauthorizeForPrimaryAccount()
    {
        Account account = AccountHelper.getInstance(context).getPrimaryAccount();
        if(account != null) {
            unauthorizeForAccount(AccountTypeUtil.getAccountType(account.type), account.name);
        } else {
            // ???
        }
    }


    // login
    public void authorizeForPrincipalAccount(Set<String> authScopeSet, boolean notifyAuthFailure)
    {
        int type = AppAuthConfigManager.getInstance(context).getApplicationAuthConfig().getPrincipalAccountType();
        if(AccountTypeUtil.isValid(type)) {
            authorizeForMainAccount(type, authScopeSet, notifyAuthFailure);
        } else {
            Log.w("Invalid principal account type = " + type);
        }
    }
    public void authorizeForPrincipalAccount(Set<String> authScopeSet, Activity activity)
    {
        int type = AppAuthConfigManager.getInstance(context).getApplicationAuthConfig().getPrincipalAccountType();
        if(AccountTypeUtil.isValid(type)) {
            authorizeForMainAccount(type, authScopeSet, activity);
        } else {
            Log.w("Invalid principal account type = " + type);
        }
    }

    // logout
    public void unauthorizeForPrincipalAccount()
    {
        int type = AppAuthConfigManager.getInstance(context).getApplicationAuthConfig().getPrincipalAccountType();
        if(AccountTypeUtil.isValid(type)) {
            unauthorizeForMainAccount(type);
        } else {
            Log.w("Invalid principal account type = " + type);
        }
    }


    // login
    public void authorizeForMainAccount(int type, Set<String> authScopeSet, boolean notifyAuthFailure)
    {
        String name = AccountHelper.getInstance(context).getMainAccountName(type);
        authorizeForAccount(type, name, authScopeSet, notifyAuthFailure);
    }
    public void authorizeForMainAccount(int type, Set<String> authScopeSet, Activity activity)
    {
        String name = AccountHelper.getInstance(context).getMainAccountName(type);
        authorizeForAccount(type, name, authScopeSet, activity);
    }

    // logout
    public void unauthorizeForMainAccount(int type)
    {
        String name = AccountHelper.getInstance(context).getMainAccountName(type);
        unauthorizeForAccount(type, name);
    }


    // login
    public void authorizeForAccount(int type, String name, Set<String> authScopeSet, Activity activity)
    {
        authorizeForAccount(type, name, authScopeSet, activity, false);
    }
    public void authorizeForAccount(int type, String name, Set<String> authScopeSet, Activity activity, boolean invalidateFirst)
    {
        OAuth2Helper.getInstance(context).authorizeForAccount(type, name, authScopeSet, activity, invalidateFirst);
    }
//    public void authorizeForAccount(int type, String name, Set<String> authScopeSet)
//    {
//        authorizeForAccount(type, name, authScopeSet, false);
//    }
    public void authorizeForAccount(int type, String name, Set<String> authScopeSet, boolean notifyAuthFailure)
    {
        authorizeForAccount(type, name, authScopeSet, notifyAuthFailure, false);
    }
    public void authorizeForAccount(int type, String name, Set<String> authScopeSet, boolean notifyAuthFailure, boolean invalidateFirst)
    {
        OAuth2Helper.getInstance(context).authorizeForAccount(type, name, authScopeSet, notifyAuthFailure, invalidateFirst);
    }

    // logout
    public void unauthorizeForAccount(int type, String name)
    {
        // ????

        // remove auth token from the prefs
        // invalidate auth token in AcccountManager cache...
        AccountPrefsStoreManager.getInstance(context).removeAccountAuthToken(type, name);
        OAuth2Helper.getInstance(context).unauthorizeForAccount(type, name);
    }



    ////////////////////////////////////////////////////
    // OAuth2AuthCallback interface

    @Override
    public void onOAuth2AuthSuccess(int type, String name, String authToken, String authScopes, long authTime)
    {
        if(Log.I) Log.i("AuthHelper.onOAuth2AuthSuccess() type = " + type + "; name = " + name + "; authToken = " + authToken + "; authScopes = " + authScopes + "; authTime = " + authTime);

        // TBD:
        AccountPrefsStoreManager.getInstance(context).setAccountAuthToken(type, name, authToken, authScopes, authTime);
        // ...

        for(AuthResultCallback c : callbacks) {
            c.onAuthorizationSuccess(type, name, authToken, authScopes, authTime);
        }
    }

    @Override
    public void onOAuth2AuthFailure(int type, String name, String reason)
    {
        if(Log.I) Log.i("AuthHelper.onOAuth2AuthFailure() type = " + type + "; name = " + name + "; reason = " + reason);

        // TBD:
        // ????
        // AccountPrefsStoreManager.getInstance(context).removeAccountAuthToken(type, name);
        // long authFailureTime = 0L;   // ???
        long authFailureTime = System.currentTimeMillis();  // ???
        AccountPrefsStoreManager.getInstance(context).setAccountAuthToken(type, name, authFailureTime, reason);
        // ....

        for (AuthResultCallback c : callbacks) {
            c.onAuthorizationFailure(type, name, reason);
        }
    }



    ////////////////////////////////////////////////////
    // To support async login/logout...


    // We allow multiple callbacks, but most likely, the client only needs one.
    private final Set<AuthResultCallback> callbacks = new HashSet<AuthResultCallback>();
    public final void addAuthResultCallback(AuthResultCallback callback)
    {
        callbacks.add(callback);
    }
    // TBD:
    // Allow remove callbacks???
    //


}

package ms.airy.lib.core.receiver;


// TBD:
public final class ClassPackageUtil
{
    private ClassPackageUtil() {}

    // Returns the package name from the full path class name.
    public static String getPackageName(String className)
    {
        int lidx = className.lastIndexOf('.');
        String packageName = className.substring(0, lidx);
        return packageName;
    }

}

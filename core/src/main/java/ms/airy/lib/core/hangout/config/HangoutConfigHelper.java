package ms.airy.lib.core.hangout.config;

import android.content.Context;

import ms.airy.lib.core.config.ConfigMaster;
import ms.airy.lib.core.util.IdUtil;


public final class HangoutConfigHelper
{
    // Config values are defined in res/values/config_hangout.xml.
    // Note that local config values can be overwritten by those from the remote config server.

    // temporary
    // config. string (cron expression)
    private static final String KEY_HANGOUT_SCHEDULE = "airy.hangout.schedule.expression";
    // This is specific to this module (hangout)
    // To add some randomness between different modules which produce cron-based notifications.
    // (5,000~10,000 millsecond range should suffice depending on how accurate the nofication should be.
    //  Note that negative value means, delay.)
    // This is to be combined with the user pref notification lead time.
    private static final String KEY_HANGOUT_CRON_LEADMILLIS = "airy.hangout.schedule.cron.leadmillis";
    // Which is better???
    // private static final long DEFAULT_CRON_LEADMILLIS = 0L;
    private static final long DEFAULT_CRON_LEADMILLIS = IdUtil.generateRandomId04();
    // ...

    private static HangoutConfigHelper INSTANCE = null;
    public static HangoutConfigHelper getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new HangoutConfigHelper(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;

    private final ConfigMaster configMaster;
    private String scheduleCronExpression = null;
    private Long cronLeadMillis = null;

    private HangoutConfigHelper(Context context)
    {
        this.context = context;
        this.configMaster = ConfigMaster.getInstance(context);
    }


    public String getHangoutScheduleExpression()
    {
        if(scheduleCronExpression == null) {
            scheduleCronExpression = configMaster.getStringProperty(KEY_HANGOUT_SCHEDULE);
        }
        // what to do if scheduleCronExpression == null, at this point ????
        return scheduleCronExpression;
    }

    public long getHangoutCronLeadMillis()
    {
        if(cronLeadMillis == null) {
            cronLeadMillis = configMaster.getLongProperty(KEY_HANGOUT_CRON_LEADMILLIS);
            if(cronLeadMillis == null) { // ???
                // What to do???
                cronLeadMillis = DEFAULT_CRON_LEADMILLIS;
            }
        }
        return cronLeadMillis;
    }


}

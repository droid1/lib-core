package ms.airy.lib.core.invite.client.impl;

import java.util.List;

import ms.airy.lib.core.invite.client.BuddyInviteClient;
import ms.airy.lib.core.invite.common.InviteInfo;
import ms.airy.lib.core.invite.common.RecipientInfo;


public class MockBuddyInviteClient implements BuddyInviteClient
{
    public MockBuddyInviteClient()
    {
    }

    @Override
    public long sendInvite(String recipient)
    {
        if(Log.I) Log.i("MockBuddyInviteClient.sendInvite(). recipient = " + recipient);

        return 0;
    }

    @Override
    public InviteInfo getInviteInfo(long inviteId)
    {
        if(Log.I) Log.i("MockBuddyInviteClient.getInviteInfo(). inviteId = " + inviteId);

        return null;
    }

    @Override
    public List<Long> getInvites(int status)
    {
        if(Log.I) Log.i("MockBuddyInviteClient.getInvites(). status = " + status);

        return null;
    }

    @Override
    public RecipientInfo getRecipientInfo(String recipient)
    {
        if(Log.I) Log.i("MockBuddyInviteClient.getRecipientInfo(). recipient = " + recipient);

        return null;
    }

    @Override
    public List<String> getRecipients(int status)
    {
        if(Log.I) Log.i("MockBuddyInviteClient.getRecipients(). status = " + status);

        return null;
    }

}

package ms.airy.lib.core.feedback.client.task;

import android.os.AsyncTask;

import ms.airy.lib.core.feedback.client.OnlineSurveyClient;
import ms.airy.lib.core.feedback.client.factory.FeedbackClientFactory;
import ms.airy.lib.core.feedback.common.SurveyEntry;


/**
 */
public class AsyncSurveyGetSurveyTask extends AsyncTask<Long, Void, SurveyEntry>
{

    private final AsyncTaskCallback callback;
    public AsyncSurveyGetSurveyTask(AsyncTaskCallback callback)
    {
        this.callback = callback;
    }


    // Store input params during processing
    private long id;

    @Override
    protected SurveyEntry doInBackground(Long... params)
    {
        id = params[0];

        OnlineSurveyClient client = FeedbackClientFactory.getInstance().getSurveyClient();

        SurveyEntry surveyEntry = client.getSurvey(id);

        return surveyEntry;
    }


    @Override
    protected void onPostExecute(SurveyEntry surveyEntry)
    {
        // super.onPostExecute(aLong);

        this.callback.onGetSurvey(surveyEntry, id);
    }





    // Callback interface
    public static interface AsyncTaskCallback
    {
        void onGetSurvey(SurveyEntry surveyEntry, long id);
    }

}

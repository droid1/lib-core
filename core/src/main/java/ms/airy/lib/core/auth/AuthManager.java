package ms.airy.lib.core.auth;

import android.accounts.Account;
import android.content.Context;

import java.util.HashMap;
import java.util.Map;

import ms.airy.lib.core.auth.common.AccountType;
import ms.airy.lib.core.auth.common.UserAccount;
import ms.airy.lib.core.auth.helper.AccountHelper;
import ms.airy.lib.core.auth.helper.AccountTypeUtil;
import ms.airy.lib.core.user.UserManager;


public final class AuthManager
{
    private static AuthManager INSTANCE = null;
    public static AuthManager getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new AuthManager(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;

    // "Cache"
    // TBD: how to invalidate this???
    // If the primary account changes, we need to invalidate this....
    //private UserAccount primaryUserAccount = null;
    // This is better. (Note that UserAccount is immutable.)
    // ??? Is this really necessary????
    private final Map<String, UserAccount> userAccountMap = new HashMap<String, UserAccount>();


    private AuthManager(Context context)
    {
        this.context = context;
    }


    // tbd
    public UserAccount getUserAccount(int type, String name)
    {
        Account account = AccountHelper.getInstance(context).getAccount(type, name);
        if(account == null) {
            // return null;
            // ????
            type = AccountType.TYPE_NONE;
            name = null;
            // ????
        }
        String id = UserManager.getInstance(context).getUserDeviceId();
        String key = UserAccount.getUniqueKey(id, type, name);
        if(userAccountMap.get(key) == null) {
            UserAccount userAccount = new UserAccount(id, type, name);
            // UserAccount primaryUserAccount = UserAccount.create(key);
            userAccountMap.put(key, userAccount);
        }
        return userAccountMap.get(key);
    }

    public UserAccount getPrimaryUserAccount()
    {
        Account primaryAccount = AccountHelper.getInstance(context).getPrimaryAccount();
        if(primaryAccount == null) {
            return null;  // ???
        }

        String id = UserManager.getInstance(context).getUserDeviceId();
        int type = AccountTypeUtil.getAccountType(primaryAccount.type);
        String name = primaryAccount.name;
        String key = UserAccount.getUniqueKey(id, type, name);
        if(userAccountMap.get(key) == null) {
            UserAccount primaryUserAccount = new UserAccount(id, type, name);
            // UserAccount primaryUserAccount = UserAccount.create(key);
            userAccountMap.put(key, primaryUserAccount);
        }
        return userAccountMap.get(key);
    }



    // TBD

    // ....
    // Log in
    // Log out
    // ???


}

package ms.airy.lib.core.cron;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;


/**
 */
public final class CronConstants
{
    // CronTab field indices.
    public static final int MINUTES = 0;
    public static final int HOURS = 1;
    public static final int DAYS_OF_MONTH = 2;
    public static final int MONTHS = 3;
    public static final int DAYS_OF_WEEK = 4;
    public static final int YEARS = 5;
    public static final int NUM_FIELDS = 6;

    // Field value ranges.
    // Range == min ~ max (inclusive on both ends)
    // TBD: This is very confusing.
    //      Some fields are 0-index based, and some fields are 1 based.
    // Note that th standard cron expressions and Java calendar has one "annoying" differences.
    // In cron, month runs from 1 to 12,
    //      whereas in Java Calendar, months is indexed from 0 to 11.
    public static final int MIN_MINUTES = 0;
    public static final int MAX_MINUTES = 59;
    public static final int MIN_HOURS = 0;
    public static final int MAX_HOURS = 23;
    public static final int MIN_DAYS_OF_MONTH = 1;  // 1-based!!!
    public static final int MAX_DAYS_OF_MONTH = 31;
    public static final int MIN_MONTHS = 1;   // 1->0 == January
    public static final int MAX_MONTHS = 12;  // 12->11 == December
    public static final int MIN_DAYS_OF_WEEK = 0;
    public static final int MAX_DAYS_OF_WEEK = 6; // Sun = 0 or 7
    public static final int MIN_YEARS = 2000;
    public static final int MAX_YEARS = 2020;
    public static final int THIS_YEAR = Calendar.getInstance().get(Calendar.YEAR);
    public static final int[][] RANGES = {
         {MIN_MINUTES, MAX_MINUTES},
         {MIN_HOURS, MAX_HOURS},
         {MIN_DAYS_OF_MONTH, MAX_DAYS_OF_MONTH},
         {MIN_MONTHS, MAX_MONTHS},
         {MIN_DAYS_OF_WEEK, MAX_DAYS_OF_WEEK},
         //{MIN_YEARS, MAX_YEARS}
         //{THIS_YEAR, MAX_YEARS}   // ???
         {THIS_YEAR, (THIS_YEAR + 10 < MAX_YEARS) ? THIS_YEAR + 10 : MAX_YEARS}   // ???
    };

    // String constants for Months and Days of Week.
    public static final Map<String, Integer> sMapMonths = new HashMap<String, Integer>();
    public static final Map<String, Integer> sMapDaysOfWeek = new HashMap<String, Integer>();

    static {
        sMapMonths.put("JAN", 1);
        sMapMonths.put("FEB", 2);
        sMapMonths.put("MAR", 3);
        sMapMonths.put("APR", 4);
        sMapMonths.put("MAY", 5);
        sMapMonths.put("JUN", 6);
        sMapMonths.put("JUL", 7);
        sMapMonths.put("AUG", 8);
        sMapMonths.put("SEP", 9);
        sMapMonths.put("OCT", 10);
        sMapMonths.put("NOV", 11);
        sMapMonths.put("DEC", 12);
    }

    static {
        sMapDaysOfWeek.put("SUN", 0);
        sMapDaysOfWeek.put("MON", 1);
        sMapDaysOfWeek.put("TUE", 2);
        sMapDaysOfWeek.put("WED", 3);
        sMapDaysOfWeek.put("THU", 4);
        sMapDaysOfWeek.put("FRI", 5);
        sMapDaysOfWeek.put("SAT", 6);
    }

}

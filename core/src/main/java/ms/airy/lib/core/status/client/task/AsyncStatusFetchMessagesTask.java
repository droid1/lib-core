package ms.airy.lib.core.status.client.task;


import android.os.AsyncTask;

import ms.airy.lib.core.status.client.StatusMessageFetchClient;
import ms.airy.lib.core.status.client.factory.StatusClientFactory;

import java.util.List;

/**
 */
public class AsyncStatusFetchMessagesTask extends AsyncTask<Long, Void, List<Long>>
{
    private final AsyncTaskCallback callback;
    public AsyncStatusFetchMessagesTask(AsyncTaskCallback callback)
    {
        this.callback = callback;
    }

    // Store input params during processing
    private long since;


    @Override
    protected List<Long> doInBackground(Long... params)
    {
        since = params[0];

        StatusMessageFetchClient client = StatusClientFactory.getInstance().getFetchClient();

        List<Long> ids = client.fetchMessages(since);

        return ids;
    }

    @Override
    protected void onPostExecute(List<Long> ids)
    {
        // super.onPostExecute(aLong);

        this.callback.onFetchMessages(ids, since);
    }


    // Callback interface
    public static interface AsyncTaskCallback
    {
        void onFetchMessages(List<Long> ids, long since);
    }

}

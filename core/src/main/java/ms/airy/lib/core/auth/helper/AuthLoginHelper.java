package ms.airy.lib.core.auth.helper;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;

import java.util.HashSet;
import java.util.Set;

import ms.airy.lib.core.dialog.ActionableAssertDialogListener;
import ms.airy.lib.core.dialog.stock.SimpleResponseDialogFragment;


// TBD:
// It can be used with or without dialog.
public final class AuthLoginHelper implements ActionableAssertDialogListener
{
    // temporary
    private static final String DIALOG_TITLE = "Login";
    private static final String DIALOG_TAG = "auth_login_dialog";

    private static AuthLoginHelper INSTANCE = null;
    public static AuthLoginHelper getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new AuthLoginHelper(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;
    
    // Login Message
    private String message;

    // TBD:
    // target account type and name ???
    // ....


    private AuthLoginHelper(Context context)
    {
        this(context, null);
    }
    private AuthLoginHelper(Context context, AuthLoginCallback callback)
    {
        this.context = context;
        if(callback != null) {
            addAuthLoginCallback(callback);
        }
        
        // TBD:
        // Load the message ...
        // temporary
        message = "Log in to ...";   // + Account Type...
        // ...
    }


    public void startLoginFlow(Activity activity)
    {
        // TBD ...
        // ...

        showAccountPickerDialog(activity);
    }

    private void showAccountPickerDialog(Activity activity)
    {
        if(activity == null) {
            if(context instanceof Activity) {
                activity = (Activity) context;
            }
        }
        if(activity == null) {
            Log.e("activity is null. Cannot display login dialog.");
            return;   // ??
        }
        // temporary
        // positive + neutral buttons only.
        DialogFragment fragment = SimpleResponseDialogFragment.newInstance(
                DIALOG_TITLE,
                message,
                null,
                "",
                null
        ).addActionable(this);
        fragment.show(activity.getFragmentManager(), DIALOG_TAG);
    }




    //////////////////////////////////////////
    // ActionableAssertDialogListener interface

    @Override
    public void onPositiveClick(String tag)
    {
        if(DIALOG_TAG.equals(tag)) {
            for (AuthLoginCallback c : callbacks) {
                c.onLoginFlowCanceled();
            }
        } else {
            // Ignore.
        }
    }

    @Override
    public void onNegativeClick(String tag)
    {
        // Ignore.
        // No negative button is displayed.
    }

    @Override
    public void onNeutralClick(String tag)
    {
        if(DIALOG_TAG.equals(tag)) {
            
            // TBD:
            // Initiate the login..
            // ...
            
            for (AuthLoginCallback c : callbacks) {
                c.onLoginFlowStarted();
            }
        } else {
            // Ignore.
        }
    }




    //////////////////////////////////////////
    // For "relaying" ActionableAssertDialogListener methods ....

    public static interface AuthLoginCallback
    {
        // void onAuthLogined(Account account);
        void onLoginFlowStarted();
        void onLoginFlowCanceled();
    }

    // We allow multiple callbacks, but most likely, the client only needs one.
    private final Set<AuthLoginCallback> callbacks = new HashSet<AuthLoginCallback>();
    public final void addAuthLoginCallback(AuthLoginCallback callback)
    {
        callbacks.add(callback);
    }

}

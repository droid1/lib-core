package ms.airy.lib.core.ui.headerbar;

public enum ActionType
{
    HOME,
    UP,    // ???
    MENU,  // ???
    FRAGMENT,
    ACTIVITY,
    INTENT,
    CALLBACK

}

package ms.airy.lib.core.phaser.feature;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import ms.airy.lib.core.phaser.common.Feature;
import ms.airy.lib.core.phaser.common.Phase;


// TBD:
// FeatureMap data should be stored to/from persistent storage such as config.
// Just using a singleton may not be sufficient.....
public final class FeatureHelper
{
    // FeatureCode -> Disabled flag.
    // For now, we only store disabled features.
    private final Map<String, Boolean> features;

    public FeatureHelper()
    {
        features = new HashMap<String, Boolean>();
        init();
    }
    private void init()
    {
        // TBD:
        // Add all predefined phase- dependent features here ????
        // ....
    }


    // Singleton
    private static final class FeatureHelperHolder
    {
        private static final FeatureHelper INSTANCE = new FeatureHelper();
    }
    public static FeatureHelper getInstance()
    {
        return FeatureHelperHolder.INSTANCE;
    }


//    public void addDisabledFeature(String featureCode)
//    {
//        features.put(featureCode, false);
//    }
    public void disableFeature(String featureCode)
    {
        features.put(featureCode, false);
    }
    public void enableFeature(String featureCode)
    {
        features.remove(featureCode);
    }

    public Set<String> getDisabledFeatures()
    {
        // features contain only disabled features..
        return features.keySet();
    }



}

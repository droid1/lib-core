package ms.airy.lib.core.feedback.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import ms.airy.lib.core.feedback.manager.FeedbackManager;


/**
 * Example usage:
 * In Activity.onCreate()
 *   fragmentTransaction.add(ContextSensitiveSurveyFragment.newInstance(surveyId, surveyTitle), fragmentTag);
 */
public class FeedbackSurveyFragment extends Fragment
{
    // Long
    public static final String ARG_SURVEY_ID = "feedback_survey_id";
    // String
    public static final String ARG_SURVEY_TITLE = "feedback_survey_title";
    // ...

    // TBD:
    public static FeedbackSurveyFragment newInstance(long surveyId)
    {
        return newInstance(surveyId, null);
    }
    public static FeedbackSurveyFragment newInstance(long surveyId, String surveyTitle)
    {
        FeedbackSurveyFragment fragment = new FeedbackSurveyFragment();
        Bundle args = new Bundle();
        args.putLong(ARG_SURVEY_ID, surveyId);
        if(surveyTitle != null && !surveyTitle.isEmpty()) {
            args.putString(ARG_SURVEY_TITLE, surveyTitle);
        }
        fragment.setArguments(args);
        return fragment;
    }
    protected static FeedbackSurveyFragment addIntArgument(FeedbackSurveyFragment fragment, String key, int val)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        args.putInt(key, val);
        return fragment;
    }
    protected static FeedbackSurveyFragment addLongArgument(FeedbackSurveyFragment fragment, String key, long val)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        args.putLong(key, val);
        return fragment;
    }
    protected static FeedbackSurveyFragment addStringArgument(FeedbackSurveyFragment fragment, String key, String val)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        args.putString(key, val);
        return fragment;
    }
    public FeedbackSurveyFragment()
    {
    }


    // Each survey fragment is associated with a surveyId.
    private long surveyId = 0L;
    // Default survey dialog title can be overwritten if surveyTitle is set.
    private String surveyTitle = null;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        // tbd
        setRetainInstance(true);
        // ...

        Bundle args = getArguments();
        if(args != null) {
            surveyId = args.getLong(ARG_SURVEY_ID);
            surveyTitle = args.getString(ARG_SURVEY_TITLE);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
//        TextView textView = new TextView(getActivity());
//        textView.setText(R.string.hello_blank_fragment);
//        return textView;
        return null;
    }


    // temporary
    private static final int MENU_ITEM_SHOW_SPECIAL_SURVEY = 114381;
    private static final int MENU_ITEM_SHOW_DAILY_SURVEY = 114382;
    private static final int MENU_ITEM_SHOW_PAGE_SURVEY = 114383;
    private static final int MENU_ITEM_SHOW_FEEDBACK_DIALOG = 114384;

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        // TBD: Put the survey menu at the bottom? How?
        // TBD: Put the menu label ("survey") in the resource file.
        // TBD: Show this only if the user has not seen the survey or has not answered it yet, etc..
        // TBD: Show only one, starting from the top. If any is to be shown, ignore the following ones.
        //      the only exception is general feedback dialog.
        //      it is always shown if no other high-priority survey is to show.
        //     (what about page survey? do we allow multiple answers for the same page survey????)

        MenuItem item1 = menu.add(Menu.NONE, MENU_ITEM_SHOW_SPECIAL_SURVEY, Menu.NONE, "Special Survey");
        // item1.setIcon(android.R.drawable.ic_menu_help);

        MenuItem item2 = menu.add(Menu.NONE, MENU_ITEM_SHOW_DAILY_SURVEY, Menu.NONE, "Daily Quenstionnaire");
        // item2.setIcon(android.R.drawable.ic_menu_help);

        MenuItem item3 = menu.add(Menu.NONE, MENU_ITEM_SHOW_PAGE_SURVEY, Menu.NONE, "Page Feedback");
        // item3.setIcon(android.R.drawable.ic_menu_help);

        MenuItem item4 = menu.add(Menu.NONE, MENU_ITEM_SHOW_FEEDBACK_DIALOG, Menu.NONE, "General Feedback");
        //item4.setIcon(android.R.drawable.ic_menu_help);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch(item.getItemId()) {
            case MENU_ITEM_SHOW_SPECIAL_SURVEY:
                showSpecialSurvey();
                return true;
            case MENU_ITEM_SHOW_DAILY_SURVEY:
                showDailySurvey();
                return true;
            case MENU_ITEM_SHOW_PAGE_SURVEY:
                showPageSurvey();
                return true;
            case MENU_ITEM_SHOW_FEEDBACK_DIALOG:
                showFeedbackDialog();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showSpecialSurvey()
    {
        // TBD:
        // Get the special survey id, from remote server if any.
        // check if this servey has been answered or otherwise processed.
        long specialSurveyId = 123L; // temporary
        String specialSurveyTitle = null;   // tbd
        boolean suc = FeedbackManager.getInstance(getActivity()).showSurvey(getActivity(), specialSurveyId, specialSurveyTitle);
        // ToastSurveyer.getInstance(getActivity()).showToast("suc = " + suc);
        if(Log.I) Log.i("showSurvey(). suc = " + suc);
    }

    private void showDailySurvey()
    {
        // TBD:
        // Get the daily survey id, from remote server if any.
        // check if this servey has been answered or otherwise processed.
        long dailySurveyId = 124L; // temporary
        String dailySurveyTitle = null;   // tbd
        boolean suc = FeedbackManager.getInstance(getActivity()).showSurvey(getActivity(), dailySurveyId, dailySurveyTitle);
        // ToastSurveyer.getInstance(getActivity()).showToast("suc = " + suc);
        if(Log.I) Log.i("showSurvey(). suc = " + suc);
    }

    private void showPageSurvey()
    {
        boolean suc = FeedbackManager.getInstance(getActivity()).showSurvey(getActivity(), surveyId, surveyTitle);
        // ToastSurveyer.getInstance(getActivity()).showToast("suc = " + suc);
        if(Log.I) Log.i("showSurvey(). suc = " + suc);
    }

    private void showFeedbackDialog()
    {
        // TBD:
        long generalFeedbackId = 111L;
        String generalFeedbackTitle = null;   // ???
        boolean suc = FeedbackManager.getInstance(getActivity()).showSurvey(getActivity(), generalFeedbackId, generalFeedbackTitle);
        // ToastSurveyer.getInstance(getActivity()).showToast("suc = " + suc);
        if(Log.I) Log.i("showSurvey(). suc = " + suc);
    }


}

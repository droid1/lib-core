package ms.airy.lib.core.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;


public abstract class BaseActiveFragment extends Fragment implements ActiveFragment
{
    // private static final String CHANNEL = "base";

    private FragmentContainer fragmentContainer;
    private String channel = null;
    // private List<String> channels = new ArrayList<String>();


    public BaseActiveFragment()
    {
        Log.i("CTOR: BaseActiveFragment()");
    }


    // newInstance() ???
    // ....



    protected FragmentContainer getFragmentContainer()
    {
        return fragmentContainer;
    }

    public String getChannel()
    {
        return channel;
    }
    protected void setChannel(String channel)
    {
        this.channel = channel;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return null;
    }


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        Activity activity = getActivity();
        if(activity instanceof FragmentContainer) {
            fragmentContainer = (FragmentContainer) activity;
        } else {
            // ???
        }

    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }


    @Override
    public void onStart()
    {
        super.onStart();
        if(fragmentContainer != null) {
            fragmentContainer.registerFragment(this, getChannel());
        }
    }

    @Override
    public void onStop()
    {
        if(fragmentContainer != null) {
            fragmentContainer.unregisterFragment(this);
        }
        super.onStop();
    }



    @Override
    public boolean processContainerMessages(FragmentContainer container, String channel, List<FragmentMessage> messages)
    {
        // TBD:
        return false;
    }

}

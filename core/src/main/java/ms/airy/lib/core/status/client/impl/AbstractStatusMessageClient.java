package ms.airy.lib.core.status.client.impl;


/**
 */
public abstract class AbstractStatusMessageClient 
{
    // TBD:
    private String statusWebServiceEndpoint;

    public AbstractStatusMessageClient()
    {
        this(null);  // ????
    }
    protected AbstractStatusMessageClient(String statusWebServiceEndpoint)
    {
        this.statusWebServiceEndpoint = statusWebServiceEndpoint;
    }

    public String getStatusWebServiceEndpoint()
    {
        return statusWebServiceEndpoint;
    }
    protected void setStatusWebServiceEndpoint(String statusWebServiceEndpoint)
    {
        this.statusWebServiceEndpoint = statusWebServiceEndpoint;
        // TBD:
        // reset the miniclient...
        // ...
    }


}

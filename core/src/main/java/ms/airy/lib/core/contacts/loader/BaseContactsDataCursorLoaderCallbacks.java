package ms.airy.lib.core.contacts.loader;

import android.app.LoaderManager;
import android.content.Context;
import android.content.Loader;
import android.database.Cursor;
import android.widget.CursorAdapter;

import ms.airy.lib.core.contacts.projection.ContactsDataProjectionRegistry;


public abstract class BaseContactsDataCursorLoaderCallbacks implements LoaderManager.LoaderCallbacks<Cursor>
{
    private final Context context;

    private final CursorAdapter cursorAdapter;
    private final String selectQuery;
    private final String[] projection;


    public BaseContactsDataCursorLoaderCallbacks(Context context, CursorAdapter cursorAdapter, String selectQuery)
    {
        this(context, cursorAdapter, selectQuery, null);
    }
    public BaseContactsDataCursorLoaderCallbacks(Context context, CursorAdapter cursorAdapter, String selectQuery, String[] projection)
    {
        this.context = context;
        this.cursorAdapter = cursorAdapter;
        this.selectQuery = selectQuery;
        if(projection != null) {
            this.projection = projection;
        } else {
            // ???
            this.projection = ContactsDataProjectionRegistry.getDefaultProjection();
        }
    }




    protected Context getContext()
    {
        return context;
    }

    protected CursorAdapter getCursorAdapter()
    {
        return cursorAdapter;
    }

    protected String getSelectQuery()
    {
        return selectQuery;
    }

    protected String[] getProjection()
    {
        return projection;
    }


    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data)
    {
        Log.i("onLoadFinished() called.");

        if(Log.I) Log.i("data = " + data.toString());
        int cnt = data.getCount();
        if(Log.I) Log.i("count = " + cnt);

        cursorAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader loader)
    {
        Log.i("onLoaderReset() called.");
        cursorAdapter.swapCursor(null);
    }

}

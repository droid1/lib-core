package ms.airy.lib.core.common.util;

import java.util.HashMap;
import java.util.Map;

import ms.airy.lib.core.common.ImageSize;
import ms.airy.lib.core.common.ItemContentLength;


public final class ItemSizeUtil
{
    private ItemSizeUtil() {}


    // for efficiency...
    private static final Map<String, String> directFlagSizeMap = new HashMap<String,String>();
    static {
        directFlagSizeMap.put(ItemContentLength.TINY, ImageSize.SIZE_TINY);
        directFlagSizeMap.put(ItemContentLength.SHORT, ImageSize.SIZE_SMALL);
        directFlagSizeMap.put(ItemContentLength.MEDIUM, ImageSize.SIZE_MEDIUM);
        directFlagSizeMap.put(ItemContentLength.LONG, ImageSize.SIZE_LARGE);
        directFlagSizeMap.put(ItemContentLength.EXTRA, ImageSize.SIZE_XLARGE);
    }
    private static final Map<String, String> listDefaultFlagSizeMap = new HashMap<String,String>();
    static {
        listDefaultFlagSizeMap.put(ItemContentLength.TINY, ImageSize.SIZE_TINY);
        listDefaultFlagSizeMap.put(ItemContentLength.SHORT, ImageSize.SIZE_SMALL);
        listDefaultFlagSizeMap.put(ItemContentLength.MEDIUM, ImageSize.SIZE_SMALL);  // <-- Note this.
        listDefaultFlagSizeMap.put(ItemContentLength.LONG, ImageSize.SIZE_MEDIUM);
        listDefaultFlagSizeMap.put(ItemContentLength.EXTRA, ImageSize.SIZE_LARGE);
    }
    private static final Map<String, String> upwardFlagSizeMap = new HashMap<String,String>();
    static {
        upwardFlagSizeMap.put(ItemContentLength.TINY, ImageSize.SIZE_SMALL);
        upwardFlagSizeMap.put(ItemContentLength.SHORT, ImageSize.SIZE_MEDIUM);
        upwardFlagSizeMap.put(ItemContentLength.MEDIUM, ImageSize.SIZE_LARGE);
        upwardFlagSizeMap.put(ItemContentLength.LONG, ImageSize.SIZE_XLARGE);
        upwardFlagSizeMap.put(ItemContentLength.EXTRA, ImageSize.SIZE_XXLARGE);
    }
    private static final Map<String, String> doubleUpwardFlagSizeMap = new HashMap<String,String>();
    static {
        doubleUpwardFlagSizeMap.put(ItemContentLength.TINY, ImageSize.SIZE_MEDIUM);
        doubleUpwardFlagSizeMap.put(ItemContentLength.SHORT, ImageSize.SIZE_LARGE);
        doubleUpwardFlagSizeMap.put(ItemContentLength.MEDIUM, ImageSize.SIZE_XLARGE);
        doubleUpwardFlagSizeMap.put(ItemContentLength.LONG, ImageSize.SIZE_XXLARGE);
        doubleUpwardFlagSizeMap.put(ItemContentLength.EXTRA, ImageSize.SIZE_XXLARGE);
    }
    private static final Map<String, String> downwardFlagSizeMap = new HashMap<String,String>();
    static {
        downwardFlagSizeMap.put(ItemContentLength.TINY, ImageSize.SIZE_MINI);
        downwardFlagSizeMap.put(ItemContentLength.SHORT, ImageSize.SIZE_TINY);
        downwardFlagSizeMap.put(ItemContentLength.MEDIUM, ImageSize.SIZE_SMALL);
        downwardFlagSizeMap.put(ItemContentLength.LONG, ImageSize.SIZE_MEDIUM);
        downwardFlagSizeMap.put(ItemContentLength.EXTRA, ImageSize.SIZE_LARGE);
    }


    public static String getFlagSizeViaDirectMapping(String contentLength)
    {
        if(directFlagSizeMap.containsKey(contentLength)) {
            return directFlagSizeMap.get(contentLength);
        } else {
            return ImageSize.SIZE_UNKNOWN;
        }
    }

    public static String getFlagSizeViaListDefaultMapping(String contentLength)
    {
        if(listDefaultFlagSizeMap.containsKey(contentLength)) {
            return listDefaultFlagSizeMap.get(contentLength);
        } else {
            return ImageSize.SIZE_UNKNOWN;
        }
    }

    public static String getFlagSizeViaUpwardMapping(String contentLength)
    {
        if(upwardFlagSizeMap.containsKey(contentLength)) {
            return upwardFlagSizeMap.get(contentLength);
        } else {
            return ImageSize.SIZE_UNKNOWN;
        }
    }

    public static String getFlagSizeViaDoubleUpwardMapping(String contentLength)
    {
        if(doubleUpwardFlagSizeMap.containsKey(contentLength)) {
            return doubleUpwardFlagSizeMap.get(contentLength);
        } else {
            return ImageSize.SIZE_UNKNOWN;
        }
    }

    public static String getFlagSizeViaDownwardMapping(String contentLength)
    {
        if(downwardFlagSizeMap.containsKey(contentLength)) {
            return downwardFlagSizeMap.get(contentLength);
        } else {
            return ImageSize.SIZE_UNKNOWN;
        }
    }


}

package ms.airy.lib.core.feedback.manager;

import android.content.Context;

import ms.airy.lib.core.feedback.common.SurveyEntry;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;


public final class SurveyHelper
{
    private static SurveyHelper INSTANCE = null;
    public static SurveyHelper getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new SurveyHelper(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Aplication context
    private final Context context;

    // Remote feedback service URL
    private URL feedbackServiceURL = null;


    private SurveyHelper(Context context)
    {
        this.context = context;
    }


    // TBD:
    // Load online surveys to the SurveyReqistry
    // check user's preferences to see
    //    if a certain survey entry has already been shown to the user,
    //    what the user's response was (e.g., "dismissed: don't show me again, show me later, etc." or "answered", etc.)
    // Manage "Today's survey".
    // etc...
    // ....

}

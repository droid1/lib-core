package ms.airy.lib.core.auth.oauth2;


public interface OAuth2AuthCallback
{
    void onOAuth2AuthSuccess(int type, String name, String authToken, String authScopes, long authTime);
    void onOAuth2AuthFailure(int type, String name, String reason);
}

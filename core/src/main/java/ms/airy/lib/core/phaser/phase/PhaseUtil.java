package ms.airy.lib.core.phaser.phase;


/**
 */
public final class PhaseUtil
{
    private PhaseUtil() {}

    // temporary
//    public static boolean isPhasedFeatureIncluded(int currentPhase, int phase)
//    {
//        return true;
//    }

    public static boolean isPhaseIncluded(int phase, Integer minPhase, Integer maxPhase)
    {
        return ((minPhase == null || minPhase <= phase) && (maxPhase == null || maxPhase > phase));
    }

}

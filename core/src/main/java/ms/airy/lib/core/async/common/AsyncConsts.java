package ms.airy.lib.core.async.common;


public final class AsyncConsts
{
    // temporary
    public static final String ACTION_ASYNC_RESULT_POST = "airy.action.ASYNC_RESULT_POST";
    // this is necessary to restrict broadcasters to handle AsyncRunner messages.
    public static final String PERMISSION_ASYNC_RUNNER = "airy.permission.ASYNC_RUNNER";
    // ...

}

package ms.airy.lib.core.location;

import android.content.Context;
import android.location.Location;

import ms.airy.lib.core.location.common.TrackingFrequency;
import ms.airy.lib.core.location.prefs.LocationPrefsStoreManager;
import ms.airy.lib.core.location.scheduler.LocationTrackerHeartBeat;


// tbd...
public final class LocationTrackingManager
{
    private static LocationTrackingManager INSTANCE = null;
    public static LocationTrackingManager getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new LocationTrackingManager(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;


    private LocationTrackingManager(Context context)
    {
        this.context = context;
    }



    // TBD:
    // ....


    public Location getCurrentLocation(long minStaleMillis)
    {
        // tbd:

        // (1) try the processed location db table
        // if it's too old
        // (2) then try locationManager.getLastKnownLocation (or, call its wrapper)
        // if it's too old
        // (3) then do one time check
        // ...


        return null;
    }


}

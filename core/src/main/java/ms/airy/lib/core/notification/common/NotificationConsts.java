package ms.airy.lib.core.notification.common;


// TBD:
public final class NotificationConsts
{
    // temporary
    public static final String ACTION_SCHEDULED_NOTIFICATION = "airy.action.SCHEDULED_NOFIFICATION";
    // ....

    public static final String KEY_EXTRA_NOTIFICATION_TAG = "airy.extra.key.notification_tag";
    public static final String KEY_EXTRA_NOTIFICATION_ID = "airy.extra.key.notification_id";
    public static final String KEY_EXTRA_NOTIFICATION = "airy.extra.key.notification";
    // ...



    private NotificationConsts() {}
}

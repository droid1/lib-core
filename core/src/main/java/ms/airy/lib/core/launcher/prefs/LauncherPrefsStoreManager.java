package ms.airy.lib.core.launcher.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;
import java.util.Map;


// TBD:
// This is not about "user preferences".
// It is about SharedPreferences storage.
// It stores the app-related global user settings.
public final class LauncherPrefsStoreManager
{
    // temporary
    private static final String SP_FILENAME_SUFFIX = ".launcher_prefs";
//    private static final String KEY_LAST_STARTUP_TIME = "last.launcher.time";
    // ...

    private static LauncherPrefsStoreManager INSTANCE = null;
    public static LauncherPrefsStoreManager getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new LauncherPrefsStoreManager(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;

    // Separate Shared Preferences file.
    private final String sharedPrefsFile;
    private final SharedPreferences sharedPreferences;

    // "Cache"
    private Long lastLauncherTime = null;


    private LauncherPrefsStoreManager(Context context)
    {
        this.context = context;
        sharedPrefsFile = context.getPackageName() + SP_FILENAME_SUFFIX;
        sharedPreferences = context.getSharedPreferences(sharedPrefsFile, Context.MODE_PRIVATE);
    }
    public String getSharedPrefsFile()
    {
        return sharedPrefsFile;
    }


//    public long getLastLauncherTime()
//    {
//        if(this.lastLauncherTime == null) {
//            long lastLauncherTime = sharedPreferences.getLong(KEY_LAST_STARTUP_TIME, 0L);
//            if (ms.airy.lib.core.launcher.prefs.Log.D) ms.airy.lib.core.launcher.prefs.Log.d("LauncherPrefsStoreManager: lastLauncherTime = " + lastLauncherTime);
//            this.lastLauncherTime = lastLauncherTime;
//        }
//        return this.lastLauncherTime;
//    }

//    public void setLastLauncherTime(long lastLauncherTime)
//    {
//        setLastLauncherTime(lastLauncherTime, false);
//    }
//    public void setLastLauncherTime(long lastLauncherTime, boolean force)
//    {
//        if(force || this.lastLauncherTime == null || this.lastLauncherTime != lastLauncherTime) {
//            this.lastLauncherTime = lastLauncherTime;
//            SharedPreferences.Editor editor = sharedPreferences.edit();
//            editor.putLong(KEY_LAST_STARTUP_TIME, lastLauncherTime);
//            editor.commit();
//        }
//    }



    // For debugging/testing purposes
    protected void removeAllPrefs()
    {
        ms.airy.lib.core.launcher.prefs.Log.w("======== LauncherPrefsStoreManager.removeAllPrefs()");

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }


}

package ms.airy.lib.core.share.util;

import android.content.Intent;


public final class TextShareUtil
{
    private TextShareUtil() {}

    public static Intent buildShareIntent(String shareText)
    {
        return buildShareIntent(shareText, null);
    }
    public static Intent buildShareIntent(String shareText, String shareSubject)
    {
        if (Log.I) Log.i("buildShareIntent(). shareText = " + shareText);

        Intent sendIntent = null;
        if(shareText == null || shareText.isEmpty()) {
            // What to do ????
        } else {
            sendIntent = new Intent(Intent.ACTION_SEND);
            sendIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);  // ????
            sendIntent.putExtra(Intent.EXTRA_TEXT, shareText);
            if(shareSubject != null && !shareSubject.isEmpty()) {
                sendIntent.putExtra(Intent.EXTRA_SUBJECT, shareSubject);
            }
            sendIntent.setType("text/plain");   // ????
        }

        return sendIntent;
    }
}

package ms.airy.lib.core.auth.config;

import java.util.List;
import java.util.Set;


public interface ApplicationAuthConfig
{
    boolean hasPrincipalAccountType();
    int getPrincipalAccountType();

    List<Integer> getAplicationTypeList();
    Set<String> getScopeSet(int accountType);

    boolean isPrimaryAccountRequired();
    boolean isPrincipalAccountRequired();

    boolean isAutoSelectAccount();
    boolean isAutoSelectAccountIfOne();

    boolean isApplicationAuthDisabled();

    boolean isAuthRequired();
    boolean isPrimaryAccountAuthRequired();
    boolean isPrimaryAccountAuthRequired(Set<String> authScopes);
    boolean isPrincipalAccountAuthRequired();
    boolean isPrincipalAccountAuthRequired(Set<String> authScopes);
    boolean isGoogleAccountAuthRequired();
    boolean isGoogleAccountAuthRequired(Set<String> authScopes);
    int getAutomaticAuthTries();

    boolean isAuthOptionsMenuDisabled();
    boolean includeAuthorizeOptionsMenu();
    boolean includeUnauthorizeOptionsMenu();
    boolean includeAccountConsoleMenu();

}

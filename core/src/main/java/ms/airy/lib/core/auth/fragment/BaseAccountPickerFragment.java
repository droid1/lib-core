package ms.airy.lib.core.auth.fragment;

import android.accounts.AccountManager;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;


// TBD:
// Not currently being used. Just use the dialg for now...
public class BaseAccountPickerFragment extends Fragment
{
    private static final int RC_ACCOUNT_PICK = 1233;

    private String accountName = null;

    public BaseAccountPickerFragment()
    {
    }

    public String getAccountName()
    {
        return accountName;
    }
    protected void unsetAccountName()
    {
        accountName = null;
    }

    // Async methods.
    // The client needs to call getAccountName() after calling pickAccount().
    public void pickAccount(String type)
    {
        pickAccount(new String[]{type});
    }
    public void pickAccount(String[] types)
    {
        unsetAccountName();
        Intent intent = AccountManager.newChooseAccountIntent(null, null, types,
                false, null, null, null, null);
        startActivityForResult(intent, RC_ACCOUNT_PICK);
    }



    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // tbd
        setRetainInstance(true);
        // ...
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
//        TextView textView = new TextView(getActivity());
//        textView.setText(R.string.hello_blank_fragment);
//        return textView;
        return null;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
    }


    @Override
    public void onStart()
    {
        super.onStart();
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }

    @Override
    public void onPause()
    {
        super.onPause();
    }

    @Override
    public void onStop()
    {
        super.onStop();
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(requestCode == RC_ACCOUNT_PICK) {
            if(resultCode == Activity.RESULT_OK) {
                accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
            } else {
                // ???
                if(Log.I) Log.i("onActivityResult(). resultCode = " + resultCode);
            }
        } else {
            // ???
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


}

package ms.airy.lib.core.feedback.client;

import java.util.List;

import ms.airy.lib.core.feedback.common.ResponseEntry;


// TBD:
public interface AsyncResponseClient
{
    void getResponse(long responseId);
    void getNewResponses();
}

package ms.airy.lib.core.ui.headerbar;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;

public class ActionTarget
{
    
    // TBD: How to encapsulate "fragment" or "activity/intent" info????
    // Use type for now: type = "fragment", "activity", or "intent".
    // If type == "fragment", the headerbar is reused.
    // Otherwise, a new activity (possibly, with or without headerbar) is invoked.
    private ActionType type;
    private Fragment fragment = null;
    private Class<?> activity = null;
    private Intent intent = null;
    private ActionCallback callback = null;
    

    // ????
    public ActionTarget()
    {
        this(ActionType.HOME);
    }
    public ActionTarget(ActionType type)
    {
        this.type = type; // ????
    }
    public ActionTarget(Fragment fragment)
    {
        this.fragment = fragment;
        this.type = ActionType.FRAGMENT;
    }
    public ActionTarget(Class<?> activity)
    {
        this.activity = activity;
        this.type = ActionType.ACTIVITY;
    }
    public ActionTarget(Intent intent)
    {
        this.intent = intent;
        this.type = ActionType.INTENT;
    }
    public ActionTarget(ActionCallback callback)
    {
        this.callback = callback;
        this.type = ActionType.CALLBACK;
    }

    

    public ActionType getType()
    {
        return type;
    }
    // ?????
    private void setType(ActionType type)
    {
        this.type = type;
    }

    
    public Fragment getFragment()
    {
        return fragment;
    }
    public void setFragment(Fragment fragment)
    {
        this.fragment = fragment;
        this.type = ActionType.FRAGMENT;  // ????
    }

    public Class<?> getActivity()
    {
        return activity;
    }
    public void setActivity(Class<?> activity)
    {
        this.activity = activity;
        this.type = ActionType.ACTIVITY;  // ????
    }

    public Intent getIntent()
    {
        return intent;
    }
    public void setIntent(Intent intent)
    {
        this.intent = intent;
        this.type = ActionType.INTENT;  // ????
    }
    public void setUpIntent(Intent intent)   // ?????
    {
        this.intent = intent;
        this.type = ActionType.UP;  // ????
    }

    public ActionCallback getCallback()
    {
        return callback;
    }
    public void setCallback(ActionCallback callback)
    {
        this.callback = callback;
        this.type = ActionType.CALLBACK;
    }
    

}

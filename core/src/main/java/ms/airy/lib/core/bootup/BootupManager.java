package ms.airy.lib.core.bootup;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;

import java.util.List;

import ms.airy.lib.core.startup.prefs.StartupPrefsStoreManager;
import ms.airy.lib.core.startup.registry.JobRegistryManager;
import ms.airy.lib.core.startup.registry.StartupJobRegistry;
import ms.airy.lib.core.startup.task.BackgroundStartupTask;
import ms.airy.lib.core.startup.task.ForegroundStartupTask;
import ms.airy.lib.core.startup.task.StartupTask;
import ms.airy.lib.core.app.VersionManager;


// TBD:
public final class BootupManager
{
    private static BootupManager INSTANCE = null;
    public static BootupManager getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new BootupManager(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;

    private BootupManager(Context context)
    {
        this.context = context;
    }

    // TBD:
    // The purpose of this class is
    // to start some required/desired processing when the device boots up
    // like starting a persistent service
    //      creating long term/recurring alarms
    //      creating future notifications (for self)
    // etc...

    // Use "android.intent.action.BOOT_COMPLETED" ??? on BroadcaseReceiver???
    // <uses-permission android:name="android.permission.RECEIVE_BOOT_COMPLETED" />
    // ...
    // <action android:name="android.intent.action.BOOT_COMPLETED" />
    // <action android:name="android.intent.action.QUICKBOOT_POWERON" />
    // ....

    // Note:
    // We will not receive ACTION_BOOT_COMPLETED broadcast if the app is installed on SD card...
    // ...
    // Listen to ACTION_EXTERNAL_APPLICATIONS_AVAILABLE  intent???
    // ....

}

package ms.airy.lib.core.dialog.stock;

import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.DialogInterface;

import java.util.HashSet;
import java.util.Set;

import ms.airy.lib.core.R;
import ms.airy.lib.core.dialog.ActionableAssertDialogHost;
import ms.airy.lib.core.dialog.ActionableAssertDialogListener;
import ms.airy.lib.core.dialog.base.BasicCustomDialogFragment;


// "Custom" dialog does not seem to work....
// We really need a way to set values in custom view
// and retrieve the user inputted data, etc...
// ....
// --> Just copy this or BasicCustomDialogFragment in app's module
// ....
public class SimpleCustomDialogFragment extends BasicCustomDialogFragment implements ActionableAssertDialogHost
{
    public static SimpleCustomDialogFragment newInstance(CharSequence title, int layoutResId)
    {
        SimpleCustomDialogFragment fragment = new SimpleCustomDialogFragment();
        addArguments(fragment, title, layoutResId);
        return fragment;
    }
    public static SimpleCustomDialogFragment newInstance(int titleResId, int layoutResId)
    {
        SimpleCustomDialogFragment fragment = new SimpleCustomDialogFragment();
        addArguments(fragment, titleResId, layoutResId);
        return fragment;
    }
    public SimpleCustomDialogFragment()
    {
    }


    ////////////////////////////////////////////////////
    // ActionableAssertDialog interface callbacks

    private final Set<ActionableAssertDialogListener> actionables = new HashSet<ActionableAssertDialogListener>();
    @Override
    public DialogFragment addActionable(ActionableAssertDialogListener actionable)
    {
        actionables.add(actionable);
        return this;
    }


    @Override
    protected AlertDialog.Builder buildDialogBuilder()
    {
        AlertDialog.Builder dialogBuilder = super.buildDialogBuilder();

        dialogBuilder.setPositiveButton(R.string.basic_custom_dialog_ok,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int whichButton) {
                        for(ActionableAssertDialogListener a : actionables) {
                            a.onPositiveClick(SimpleCustomDialogFragment.this.getTag());
                        }
                    }
                }
        );
        dialogBuilder.setNegativeButton(R.string.basic_custom_dialog_dismiss,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int whichButton) {
                        for(ActionableAssertDialogListener a : actionables) {
                            a.onNegativeClick(SimpleCustomDialogFragment.this.getTag());
                        }
                    }
                }
        );
        dialogBuilder.setNeutralButton(R.string.basic_custom_dialog_cancel,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int whichButton) {
                        for(ActionableAssertDialogListener a : actionables) {
                            a.onNeutralClick(SimpleCustomDialogFragment.this.getTag());
                        }
                    }
                }
        );

        return dialogBuilder;
    }



}

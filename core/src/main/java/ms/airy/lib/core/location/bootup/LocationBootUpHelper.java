package ms.airy.lib.core.location.bootup;

import android.content.Context;

import ms.airy.lib.core.location.LocationTrackingScheduler;


public final class LocationBootUpHelper
{
    private static LocationBootUpHelper INSTANCE = null;
    public static LocationBootUpHelper getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new LocationBootUpHelper(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;


    private LocationBootUpHelper(Context context)
    {
        this.context = context;
    }


    // TBD:
    // ....


    public void prepareLocationModule()
    {
        // TBD:
        LocationTrackingScheduler.getInstance(context).scheduleLocationTracking(false);
        LocationTrackingScheduler.getInstance(context).scheduleLocationProcessing();
    }


}

package ms.airy.lib.core.feedback.common;

import java.io.Serializable;


/**
 * It corresponds to a question in questionnaire.
 */
public class SurveyEntry implements Serializable
{
    private final long id;     // > 0L.
    private String title;      // optional
    private String question;   // can be null. E.g, user's general comment/feedback.
    // private long createdTime;  // In case this survey has been downloaded from online feedback server..
    // TBD:
    // this info needs to be stored (e.g., in SharedPreferences)
    // so that the user is not shown the same survey again and again.
    // private boolean shownToUser;
    // private int userResponse;      // dismissed, answered, etc...
    // ....


    public SurveyEntry(long id)
    {
        this(id, null, null);
    }
    public SurveyEntry(long id, String title, String question)
    {
        this.id = id;
        this.title = title;
        this.question = question;
    }


    public long getId()
    {
        return id;
    }

    public String getTitle()
    {
        return title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getQuestion()
    {
        return question;
    }
    public void setQuestion(String question)
    {
        this.question = question;
    }

    // For debugging...
    @Override
    public String toString()
    {
        return "SurveyEntry{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", question='" + question + '\'' +
                '}';
    }
}

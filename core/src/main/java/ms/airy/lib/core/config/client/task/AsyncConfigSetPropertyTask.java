package ms.airy.lib.core.config.client.task;

import android.os.AsyncTask;

import ms.airy.lib.core.config.client.OnlineConfigClient;
import ms.airy.lib.core.config.client.factory.ConfigClientFactory;


/**
 */
public class AsyncConfigSetPropertyTask extends AsyncTask<Object, Void, Boolean>
{
    private final AsyncTaskCallback callback;

    public AsyncConfigSetPropertyTask(AsyncTaskCallback callback)
    {
        this.callback = callback;
    }

    // Store input params during processing...
    private String key;
    private Object value;

    @Override
    protected Boolean doInBackground(Object... params)
    {
        key = (String) params[0];
        value = params[1];

        OnlineConfigClient client = ConfigClientFactory.getInstance().getConfigClient();

        Boolean result = client.setProperty(key, value);

        return result;
    }

    @Override
    protected void onPostExecute(Boolean result)
    {
        this.callback.onSetProperty(result, key, value);
    }



    // Callback interface
    public static interface AsyncTaskCallback
    {
        void onSetProperty(Boolean result, String key, Object value);
    }

}

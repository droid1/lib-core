package ms.airy.lib.core.ui.dashbar;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.LinearLayout;

// http://developer.android.com/guide/topics/ui/custom-components.html
public class NavButtonLayout extends LinearLayout
{

    // ....
    private List<NavButton> mButtons = null;
    // ....
    
    
    public NavButtonLayout(Context context)
    {
        super(context);
        init();
        //...
    }

    public NavButtonLayout(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
        //...
    }

    public NavButtonLayout(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        init();
        //...
    }

    
    
    private void init()
    {
        if(Log.D) Log.d("init()");

        // TBD
        setOrientation(VERTICAL);
        setMinimumWidth(250);  // temporary
        // ...
        
        // ...
        mButtons = new ArrayList<NavButton>();
        // ...
        
        // Redo the layout...
        layoutNavButtons();
    }
    
    
    // Redo the layout.
    // TBD: Incremental update???
    //      (e.g., when a button is added, it really need not existing buttons, etc.)
    // TBD: Use preferred height to include only X number of buttons.
    private void layoutNavButtons()
    {
        if(Log.D) Log.d("layoutNavButtons()");

        // [1] Remove all buttons.
        removeAllViews();
        
        // [2] Add them again.
        //     TBD: How do we know how many we can add/????
        //LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(getLayoutParams());  // ????
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT); // ????
        for(NavButton b : mButtons) {
            // TBD: Check if the button is "visible", etc. ?????
            // b.getVisibility()...
            this.addView(b, params);
        }
        
    }

    
    public void addNavButton(NavButton button)
    {
        if(Log.D) Log.d("addNavButton()");
        mButtons.add(button);
        layoutNavButtons();        
    }
    public void addNavButtons(NavButton... buttons)
    {
        if(Log.D) Log.d("addNavButtons()");
        
        mButtons.addAll(Arrays.asList(buttons));
//        for(NavButton b : buttons) {
//            mButtons.add(b);
//        }
        layoutNavButtons();        
    }
    public void addNavButton(int location, NavButton button)
    {
        if(Log.D) Log.d("addNavButton(): location = " + location);
        
        mButtons.add(location, button);
        layoutNavButtons();        
    }
    public void addNavButtons(int location, NavButton... buttons)
    {
        if(Log.D) Log.d("addNavButtons(): location = " + location);
        
        mButtons.addAll(location, Arrays.asList(buttons));
//      for(NavButton b : buttons) {  // TBD: Need to do it backward....
//          mButtons.add(location, b);
//      }
        layoutNavButtons();        
    }
    
    // TBD:
    //public void removeNavButton(String tag) {}
    //public void removeNavButtons(String... tags) {}
    
    
    
    
    @Override
    public int getOrientation()
    {
        if(Log.D) Log.d("getOrientation()");

        // TODO Auto-generated method stub
        return super.getOrientation();
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        if(Log.D) Log.d("onDraw()");

        // TODO Auto-generated method stub
        super.onDraw(canvas);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b)
    {
        if(Log.D) Log.d("onLayout()");

        // TODO Auto-generated method stub
        super.onLayout(changed, l, t, r, b);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        if(Log.D) Log.d("onMeasure()");

        // TODO Auto-generated method stub
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    public void setOrientation(int orientation)
    {
        if(Log.D) Log.d("setOrientation()");

        // TODO Auto-generated method stub
        super.setOrientation(orientation);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if(Log.D) Log.d("onKeyDown()");

        // TODO Auto-generated method stub
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyShortcut(int keyCode, KeyEvent event)
    {
        if(Log.D) Log.d("onKeyShortcut()");

        // TODO Auto-generated method stub
        return super.onKeyShortcut(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event)
    {
        if(Log.D) Log.d("onKeyUp()");

        // TODO Auto-generated method stub
        return super.onKeyUp(keyCode, event);
    }
    
    
    
}

package ms.airy.lib.core.analytics;


public interface ScreenViewTracker
{
    // Start/resume tracking.
    void startSession();

    // End/pause tracking.
    void endSession();

    // Send explicitly screenView with a predefined screen name.
    void sendScreenView();

    // etc...

}

package ms.airy.lib.core.ui.image;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import ms.airy.lib.core.R;
import ms.airy.lib.core.ui.common.UIConsts;


public class FullSizeImageViewFragment extends Fragment
{
    public static FullSizeImageViewFragment newInstance()
    {
        return newInstance(0);
    }

    public static FullSizeImageViewFragment newInstance(int imageResId)
    {
        FullSizeImageViewFragment fragment = new FullSizeImageViewFragment();
        Bundle extra = new Bundle();
        if (imageResId > 0) {
            extra.putInt(UIConsts.EXTRA_FULL_SIZE_IMAGE_RESID, imageResId);
        }
        fragment.setArguments(extra);
        return fragment;
    }

    private int imageResId = 0;

    public FullSizeImageViewFragment()
    {
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

//        // Enable the menu
//        setHasOptionsMenu(true);
//        // tbd
//        setRetainInstance(true);

        Bundle args = getArguments();
        if (args != null) {
            this.imageResId = args.getInt(UIConsts.EXTRA_FULL_SIZE_IMAGE_RESID, 0);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_full_size_image_view, container, false);

        // This is called in onResume().
        // but, seems like it needs to be called here as well.
        rootView = refreshRootView(inflater.getContext(), rootView);

        return rootView;
    }


    @Override
    public void onResume()
    {
        super.onResume();

        // ????
        View rootView = refreshRootView(getActivity(), getView());
        // rootView.invalidate();
    }

    // temporary
    private View refreshRootView(Context context, View rootView)
    {
        if(this.imageResId > 0) {
            ImageView mainImageView = (ImageView) rootView.findViewById(R.id.full_size_image_view_image);
            if(mainImageView != null) {
                if(Log.I) Log.i("Image being set to: imageResId = " + imageResId);
                mainImageView.setImageResource(this.imageResId);
            } else {
                // this cannot happen.
            }
        } else {
            // ignore
            Log.i("Image resource not set.");
        }


        return rootView;
    }

}

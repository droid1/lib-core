package ms.airy.lib.core.help.ui;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import ms.airy.lib.core.R;
import ms.airy.lib.core.help.common.QuestionInfo;
import ms.airy.lib.core.help.helper.QuestionInfoRegistry;


// temporary
// Client/appn needs to override this or create a new activity:
// (1) Populate the questionInfo list in QuestionInfoRegistry, and
// (2) Add QuestionInfoListFragment.
public class QuestionInfoListActivity extends Activity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_info_list);

        if (savedInstanceState == null) {
            FragmentTransaction transaction = getFragmentManager().beginTransaction();

            // TBD:
            // Is there a better way?
            List<QuestionInfo> questionInfos = new ArrayList<QuestionInfo>();
            // Populate questionInfos here.
            // ....
            QuestionInfoRegistry.getInstance(this).setQuestionInfos(questionInfos);

            // QuestionInfoListFragment uses QuestionInfoRegistry questionInfo list implicitly.
            QuestionInfoListFragment questionInfoListFragment = QuestionInfoListFragment.newInstance();
            transaction.add(R.id.container, questionInfoListFragment);

            transaction.commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.question_info_list, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}

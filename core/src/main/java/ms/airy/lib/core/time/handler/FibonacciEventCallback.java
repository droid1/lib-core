package ms.airy.lib.core.time.handler;

/**
 */
public interface FibonacciEventCallback
{
    void doDelayedProcessing();
}

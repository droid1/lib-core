package ms.airy.lib.core.ui.headerbar;

import ms.airy.lib.core.common.GUID;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;


// Modeled after ActionBar.Tab.
public class HeaderItem
{
    // temporary
    private static final int DEFAULT_BUTTON_WIDTH = 160;   // in pixels
    private static final int DEFAULT_BUTTON_HEIGHT = 100;   // in pixels
    private static final int DEFAULT_ICON_WIDTH = 100;   // in pixels
    private static final int DEFAULT_ICON_HEIGHT = 80;   // in pixels
    
    
    // Context
    private Context mContext;
    
    // id/idTag
    // Should be unique within a given HeaderBar (or, across an app????)
    private String idTag;
    

    // Menu icon. TBD: Use int (resId) ??????
    private Drawable icon;
    
    // Position/Index ????
    //private int position;
    
    // label
    private String label;

    // State????  Normal, Focused, Pressed, ....
    private String state;
    
    
    // "View" ....
    private HeaderButton button;
    
    
    
    // If true, the headerbar nav list will be replaced. --> Use ActionTarget.type
    //private boolean fullNav;

    
    // ...  ??? 
    private ActionTarget target;
    
   
    // .... Store the parent?????
    //private HeaderItemList parent;
    
    
    // getter/setter ????
    private int mButtonWidth = DEFAULT_BUTTON_WIDTH;
    private int mButtonHeight = DEFAULT_BUTTON_HEIGHT;
    private int mIconWidth = DEFAULT_ICON_WIDTH;
    private int mIconHeight = DEFAULT_ICON_HEIGHT;

    
    
    public HeaderItem(Context context)
    {
        this(context, GUID.generateShortString());
    }
    public HeaderItem(Context context, String idTag)
    {
        this(context, idTag, "");  // ?????
    }
    public HeaderItem(Context context, String idTag, String label)
    {
        this(context, idTag, label, null);  // ?????
    }
    public HeaderItem(Context context, String idTag, String label, Drawable icon)
    {
        this(context, idTag, label, icon, new ActionTarget());  // ?????
    }
    public HeaderItem(Context context, String idTag, String label, Drawable icon, ActionTarget target)
    {
        mContext = context;
        this.idTag = idTag;
        
        // ????
        //AttributeSet attrs = null;
        button = new HeaderButton(mContext, idTag);
        button.setText(label);
        button.setIcon(icon);
        // set other attributes, etc...
        
        // TBD
        button.setMinWidth(getButtonWidth());
        button.setMaxWidth(getButtonWidth());
        button.setMinHeight(getButtonHeight());
        button.setMaxHeight(getButtonHeight());
        // ....
        
        // .....
        this.target = target;
        
        // TBD: ...
        // ....
        
    }

    
    // TBD: Setters????
    private int getButtonWidth()
    {
        return mButtonWidth;
    }
    private int getButtonHeight()
    {
        return mButtonHeight;
    }
    private int getIconWidth()
    {
        return mIconWidth;
    }
    private int getIconHeight()
    {
        return mIconHeight;
    }
    
    
    public HeaderButton getHeaderButton()
    {
        return button;
    }

    

    public String getIdTag()
    {
        return idTag;
    }

    public void setIdTag(String idTag)
    {
        this.idTag = idTag;
        button.setIdTag(idTag);
    }


    public Drawable getIcon()
    {
        return icon;
    }

    public void setIcon(Drawable icon)
    {
        this.icon = icon;
        
        // TBD: Move the icon to the left side.. ????
        if(icon != null) {
            // ??? 
            // TBD: Do this in HeaderButton????
            int left = (int) (getButtonWidth() - getIconWidth())/2;
            int top = 20;
            int right = left + getIconWidth();
            int bottom = top + getIconHeight();
            icon.setBounds(left, top, right, bottom);
            
            
            button.setIcon(icon);
        } else {
            // ????
        }
    }



    public String getLabel()
    {
        return label;
    }

    public void setLabel(String label)
    {
        this.label = label;
        // ????
        button.setText(label);
    }



    public ActionTarget getTarget()
    {
        return target;
    }

    public void setTarget(ActionTarget target)
    {
        this.target = target;
    }

    
    
}

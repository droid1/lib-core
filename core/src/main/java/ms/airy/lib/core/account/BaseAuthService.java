package ms.airy.lib.core.account;

import android.accounts.AbstractAccountAuthenticator;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;


public class BaseAuthService extends Service
{
    private static AbstractAccountAuthenticator sAccountAuthenticator;

    public BaseAuthService()
    {
        super();
    }

    
    @Override
    public IBinder onBind(Intent intent)
    {
        IBinder ret = null;
        if (intent.getAction().equals(android.accounts.AccountManager.ACTION_AUTHENTICATOR_INTENT)) {
            ret = getAuthenticator().getIBinder();
        }
        return ret;
    }

    private AbstractAccountAuthenticator getAuthenticator()
    {
        if (sAccountAuthenticator == null) {
            sAccountAuthenticator = new BaseAccountAuthenticator(this);
        }
        return sAccountAuthenticator;
    }

}

package ms.airy.lib.core.control;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import ms.airy.lib.core.time.timer.TimerState;
import ms.airy.lib.core.time.timer.TimerRange;
import ms.airy.lib.core.time.timer.CountDownEventListener;
import ms.airy.lib.core.time.timer.FlexibleCountDownTimer;

import ms.airy.lib.core.control.util.CountDownUtil;
import ms.airy.lib.core.R;

import java.util.HashSet;
import java.util.Set;


/**
 * CountDown "text" timer.
 * It displays the counting down clock, or the time remaining, as text (as opposed to as a progress bar)
 */
public class CountDownTimerButton extends Button implements FlexibleCountDownTimer.CountDownListener  // , View.OnClickListener
{
    // disabled -> enabled onFinish.
    public static final int MODE_ENABLE_ON_FINISH = 1;
    // enabled -> disabled onFinish
    public static final int MODE_DISABLE_ON_FINISH = 2;

    private int mode;

    // tbd:
    // text vs. progress bar???
    // Or both???
    // ...

    // "master flag"
    // this is separate from View.isEnabled()...
    private boolean controlDisabled;

    // timer state
    private int state;

    // Timer range, and the current value.
    private TimerRange timerRange = null;

    // Drives the count-down animation.
    private FlexibleCountDownTimer countDownTimer = null;

    // Number of digits below the second level in the count-down time.
    private int countDownTimeDecimal;

    // Colors
    private int mColorBackgroundNormal;    // Background of the control
    private int mColorBackgroundWarning;   // Background,  when val < threshold.
    private int mColorTextNormal;    // Count-down text.
    private int mColorTextWarning;   // Count-down text, when val < threshold.

    // ???
    private volatile boolean thresholdCrossHandled = false;


    public CountDownTimerButton(Context context)
    {
        super(context);
        initControl(context, null, 0);
    }
    public CountDownTimerButton(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        initControl(context, attrs, 0);
    }
    public CountDownTimerButton(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        initControl(context, attrs, defStyle);
    }

    private void initControl(Context context, AttributeSet attrs, int defStyle)
    {
        if(Log.D) Log.d("initControl(): attrs = " + attrs);

        setFocusable(false); // For display only. No user input.

        // temporary
        // state = TimerState.UNKNOWN;
        // mode = MODE_ENABLE_ON_FINISH;

        if(attrs != null) {
            TypedArray a0 = context.getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.CountDownTimerButton,
                    0, 0);
            try {
                mode = a0.getInt(R.styleable.CountDownTimerButton_countDownTimerButtonMode, MODE_ENABLE_ON_FINISH);
            } finally {
                a0.recycle();
            }
            TypedArray a1 = context.getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.TimerRange,
                    0, 0);
            try {
//                // hack
//                int imin = a1.getInt(R.styleable.TimerRange_timerRangeMin, 0);
//                int imax = a1.getInt(R.styleable.TimerRange_timerRangeMax, imin + 60);         // ???
//                int ithreshold = a1.getInt(R.styleable.TimerRange_timerRangeThreshold, imin);  // ???
//                long min = imin * 1000L;
//                long max = imax * 1000L;
//                long threshold = ithreshold * 1000L;
                int min = a1.getInt(R.styleable.TimerRange_timerRangeMin, 0);
                int max = a1.getInt(R.styleable.TimerRange_timerRangeMax, min + 60000);         // ???
                int threshold = a1.getInt(R.styleable.TimerRange_timerRangeThreshold, min);  // ???
                timerRange = new TimerRange(max, min, threshold);
                // ...
            } finally {
                a1.recycle();
            }
            TypedArray a2 = context.getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.BaseControl,
                    0, 0);
            try {
                int c1 = a2.getColor(R.styleable.BaseControl_backgroundColorNormal, Color.rgb(172, 172, 172));
                setColorBackgroundNormal(c1);
//                int c2 = a.getColor(R.styleable.BaseControl_backgroundColorWarning, Color.rgb(255, 255, 255));
                int defC2 = Color.rgb(255, 255, 255);
                if(c1 != Color.rgb(172, 172, 172)) {  // In case c1 has been set but not c2, then just use c1 for c2.
                    defC2 = c1;
                }
                int c2 = a2.getColor(R.styleable.BaseControl_backgroundColorWarning, defC2);
                setColorBackgroundWarning(c2);
                int c3 = a2.getColor(R.styleable.BaseControl_textColorNormal, Color.rgb(127, 127, 255));
                setColorTextNormal(c3);
//                int c4 = a.getColor(R.styleable.BaseControl_textColorWarning, Color.rgb(255, 127, 127));
                int defC4 = Color.rgb(255, 127, 127);
                if(c3 != Color.rgb(127, 127, 255)) {  // In case c3 has been set but not c4, then just use c3 for c4.
                    defC4 = c3;
                }
                int c4 = a2.getColor(R.styleable.BaseControl_textColorWarning, defC4);
                setColorTextWarning(c4);

            } finally {
                a2.recycle();
            }
            TypedArray a3 = context.getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.CountDownTimer,
                    0, 0);
            try {
                countDownTimeDecimal = a3.getInt(R.styleable.CountDownTimer_countDownTimerDecimal, 1);
            } finally {
                a3.recycle();
            }
        } else {
            // Use the default axisOrientationDependent, axis-orientation mapping, etc...
            // ...
            mode = MODE_ENABLE_ON_FINISH;

            // ???? What's the best "default" value???
            timerRange = new TimerRange(10000L);

            countDownTimeDecimal = 1;

            // Default colors.
            setColorBackgroundNormal(Color.rgb(172, 172, 172));
            setColorBackgroundWarning(Color.rgb(255, 255, 255));
            setColorTextNormal(Color.rgb(127, 127, 255));
            setColorTextWarning(Color.rgb(255, 127, 127));
        }

        // ???
        state = TimerState.INITIALIZED;

        // ????
//        CharSequence text = formatCountDownTime(timerRange.getMax());
//        super.setText(text);
//
//        setTextColor(mColorTextNormal);
//        // TBD: Need to figure out how to set active/pressed state color, etc.
//        // setBackgroundColor(mColorBackgroundNormal);

        // setTextSize(24.0f);

//        // ???
//        super.setOnClickListener(this);
//

        // ???
        if(mode == MODE_ENABLE_ON_FINISH) {
            this.setEnabled(false);
        } else if(mode == MODE_DISABLE_ON_FINISH) {
            this.setEnabled(true);
        }

        // ???
        initTimer(true);
    }


    protected void initTimer()
    {
        initTimer(false);
    }
    protected void initTimer(boolean force)
    {
        // TBD:
        // At this point,
        // timerRanger should not be null...
        // How to enforce that????

        // timerRange.reset();  // Make val == timerMax. Here? or Below?
        if(force || countDownTimer == null) {
            timerRange.reset();  // Make val == timerMax. Here? or Above?
            countDownTimer = new FlexibleCountDownTimer(timerRange, 100L);
            countDownTimer.addCountDownListener(this);
        }

        // ???
        if(countDownTimer == null) {
            setState(TimerState.INITIALIZED);
        } else {
            // Keep the current state???
            // ????
        }

        // ????
//        CharSequence text = formatTextWithCountDownTime(timerRange.getMax());
        CharSequence text = formatTextWithCountDownTime(timerRange.getVal());
        super.setText(text);

        // ???
        refreshTimer();
    }

    // ???
    private void refreshTimer()
    {
        // ???
        if(timerRange.isBelowThreshold()) {
            thresholdCrossHandled = true;
            setTextColor(mColorTextWarning);
//            // TBD: Need to figure out how to set active/pressed state color, etc.
//            setBackgroundColor(mColorBackgroundWarning);
        } else {
            thresholdCrossHandled = false;
            setTextColor(mColorTextNormal);
//            // TBD: Need to figure out how to set active/pressed state color, etc.
//            setBackgroundColor(mColorBackgroundNormal);
        }


        // ???
        if(controlDisabled == true) {
            this.setEnabled(false);
        } else {
            // ???
            if (mode == MODE_ENABLE_ON_FINISH) {
                if (state == TimerState.FINISHED) {
                    this.setEnabled(true);
                } else {
                    this.setEnabled(false);
                }
            } else if (mode == MODE_DISABLE_ON_FINISH) {
                if (state == TimerState.FINISHED) {
                    this.setEnabled(false);
                } else {
                    this.setEnabled(true);
                }
            }
        }

    }


    public void update()
    {
        update(false);
    }
    public void update(boolean force)
    {
        update(force, false);
    }
    public void update(boolean force, boolean layout)
    {
        if(force || layout || isDirty() ) {   // layout==true implies force==true.
            if(layout) {
                requestLayout();
            }
            invalidate();
            // setDirty(false);  // ???
        } else {
            // ignore
        }
    }


    public int getMode()
    {
        return mode;
    }
    public void setMode(int mode)
    {
        this.mode = mode;
    }

    public boolean isControlDisabled()
    {
        return controlDisabled;
    }
    public void setControlDisabled(boolean controlDisabled)
    {
        this.controlDisabled = controlDisabled;
        if(controlDisabled == true) {
            // ???
            // this.setEnabled(false);
        } else {
            // what to do ???
            // refresh the "enabled" state...
            // ...
        }
    }

    public int getState()
    {
        return state;
    }
    public void setState(int state)
    {
        this.state = state;
    }

    public TimerRange getTimerRange()
    {
        return timerRange;
    }
    public void setTimerRange(TimerRange timerRange)
    {
        this.timerRange = timerRange;
        // ???
        initTimer(true);
        // update(true);    // ???
        // ???
    }
    public void resetTimerRange(long max)
    {
        if(this.timerRange != null) {
            this.timerRange.reset(max);
        } else {
            // ????
            this.timerRange = new TimerRange(max);
        }
        // ???
        initTimer(true);
        // update(true);    // ???
        // ???
    }
    public void resetTimerRange(long max, long min)
    {
        if(this.timerRange != null) {
            this.timerRange.reset(max, min);
        } else {
            // ????
            this.timerRange = new TimerRange(max, min);
        }
        // ???
        initTimer(true);
        // update(true);    // ???
        // ???
    }
    public void resetTimerRange(long max, long min, long threshold)
    {
        if(this.timerRange != null) {
            this.timerRange.reset(max, min, threshold);
        } else {
            // ????
            this.timerRange = new TimerRange(max, min, threshold);
        }
        // ???
        initTimer(true);
        // update(true);    // ???
    }

//    public FlexibleCountDownTimer getCountDownTimer()
//    {
//        return countDownTimer;
//    }
//    public void setCountDownTimer(FlexibleCountDownTimer countDownTimer)
//    {
//        this.countDownTimer = countDownTimer;
//    }


    public int getCountDownTimeDecimal()
    {
        return countDownTimeDecimal;
    }
    public void setCountDownTimeDecimal(int countDownTimeDecimal)
    {
        this.countDownTimeDecimal = countDownTimeDecimal;
    }


    public int getColorBackgroundNormal()
    {
        return mColorBackgroundNormal;
    }
    public void setColorBackgroundNormal(int colorBackgroundNormal)
    {
        this.mColorBackgroundNormal = colorBackgroundNormal;
    }

    public int getColorBackgroundWarning()
    {
        return mColorBackgroundWarning;
    }
    public void setColorBackgroundWarning(int colorBackgroundWarning)
    {
        this.mColorBackgroundWarning = colorBackgroundWarning;
    }

    public int getColorTextNormal()
    {
        return mColorTextNormal;
    }
    public void setColorTextNormal(int colorTextNormal)
    {
        this.mColorTextNormal = colorTextNormal;
    }

    public int getColorTextWarning()
    {
        return mColorTextWarning;
    }
    public void setColorTextWarning(int colorTextWarning)
    {
        this.mColorTextWarning = colorTextWarning;
    }





    // TBD:
    // Overriding super class methods....

//    @Override
//    public void setText(CharSequence text, BufferType type)
//    {
//        String textWithTime = text + " in ";
//        super.setText(textWithTime, type);
//    }






    // TBD:
    // Timer life cycle methods....
    // ...



    // Name? init or reset?
    // this control can be re-used for multiple count-downs...
    // so, reset seems more appropriate...
    public void reset()
    {
        reset(TimerState.INITIALIZED);
    }
    public void reset(int state)
    {
        // Timer can be reset/initialized regardless of the current state.
        // Just initialize the timer with the current/default values.
        initTimer(true);
        setState(state);
        // update();   // ???
    }
    public void reset(long max)
    {
        resetTimerRange(max);
        reset();
    }
    public void reset(long max, long min)
    {
        resetTimerRange(max, min);
        reset();
    }
    public void reset(long max, long min, long threshold)
    {
        resetTimerRange(max, min, threshold);
        reset();
    }

    public void restart()
    {
        reset();
        start();
    }
    public void restart(long max)
    {
        reset(max);
        start();
    }
    public void restart(long max, long min)
    {
        reset(max, min);
        start();
    }
    public void restart(long max, long min, long threshold)
    {
        reset(max, min, threshold);
        start();
    }


    public void start()
    {
        if(getState() != TimerState.INITIALIZED) {
            // throw new IllegalStateException("Timer cannot be started when it has not been initialized.");
            Log.w("Timer cannot be started when it has not been initialized. Current state = " + getState());
            return;
        }

        if(countDownTimer != null) {
            countDownTimer.start();
        } else {
            // ???
            Log.w("countDownTimer is null. Cannot startCycle().");
        }

        // TBD
        setState(TimerState.RUNNING);
        // update();   // ???
    }
    public void pause()
    {
        pause(-1L);
    }
    public void pause(long extent)
    {
        if(extent >= 0L) {
            // Hack. We are using this as to re-create a snapshot of the timer...
            // bypass the state check...
        } else {
            if(getState() != TimerState.RUNNING) {
                // throw new IllegalStateException("Timer cannot be paused when it's not running.");
                Log.w("Timer cannot be paused when it's not running. Current state = " + getState());
                return;
            }
        }

        if(countDownTimer != null) {
            countDownTimer.pause(extent);
            // reset this timerControl's val???
            //   (note: extent could be -1 at this point.)
            // the caller should take care of this before calling pause(extent)...
        } else {
            // ???
            Log.w("countDownTimer is null. Cannot pause().");
        }

        // TBD:
        setState(TimerState.PAUSED);
        // update();   // ???
    }
    public void resume()
    {
        resume(-1L);
    }
    public void resume(long extent)
    {
        if(extent >= 0L) {
            // Hack. We are using this to re-create a snapshot of the timer...
            // --> bypass the state check...
        } else {
            if (getState() != TimerState.PAUSED) {
                // throw new IllegalStateException("Timer cannot be resumed when it has not been paused.");
                Log.w("Timer cannot be resumed when it has not been paused. Current state = " + getState());
                return;
            }
        }

        if(countDownTimer != null) {
            countDownTimer.resume(extent);
            // reset this timerControl's val???
            //   (note: extent could be -1 at this point.)
            // the caller should take care of this before calling resume(extent)...
        } else {
            // ???
            Log.w("countDownTimer is null. Cannot resume().");
        }

        // TBD:
        setState(TimerState.RUNNING);
        // update();   // ???
    }

    public void stop()
    {
        if(getState() != TimerState.RUNNING && getState() != TimerState.PAUSED) {
            // throw new IllegalStateException("Timer cannot be stopped when it's not running.");
            Log.w("Timer cannot be stopped when it's not running or paused. Current state = " + getState());
            return;
        }

        if(countDownTimer != null) {
            countDownTimer.stop();
        } else {
            // ???
            Log.w("countDownTimer is null. Cannot stopCycle().");
        }

        // TBD:
        setState(TimerState.STOPPED);
        // update();   // ???
    }

    public void finish()
    {
//        if(getRepeats() != RUNNING && getRepeats() != PAUSED && getRepeats() != STOPPED) {
//            // throw new IllegalStateException("Timer cannot be finished when it's not running/paused/stopped.");
//            Log.w("Timer cannot be finished when it's not running/paused/stopped. Current state = " + getRepeats());
//            return;
//        }

        if(countDownTimer != null) {
            countDownTimer.finish();
        } else {
            // ???
            Log.w("countDownTimer is null. Cannot finish().");
        }

        // TBD:
        setState(TimerState.FINISHED);
        // update();   // ???
    }



//    ////////
//    // OnClickListener interface
//
//    @Override
//    public void onClick(View v)
//    {
//        if (Log.I) Log.i("onClick(): v = " + v);
//
//        // Do this only if mode == MODE_DISABLE_ON_FINISH ???
//        if(isEnabled()) {
//
//            if(state == TimerState.RUNNING) {
//                pause();
//            } else if(state == TimerState.PAUSED) {
//                resume();
//            } else {
//                // ....
//            }
//
//        } else {
//            Log.d("Button is currently disabled.");
//        }
//    }


    ////////
    // CountDownListener interface.

    @Override
    public void onTick(long millisUntilFinished)
    {
        if (Log.I) Log.i("onTick(): millisUntilFinished = " + millisUntilFinished);

        // ???
        long time = timerRange.getMin() + millisUntilFinished;
        CharSequence text = formatTextWithCountDownTime(time);
        super.setText(text);

        // Call this only once when the value cross from above threshold to below threshold??
        // ???
        timerRange.setVal(time);
        if(thresholdCrossHandled == false) {
            if (timerRange.isBelowThreshold()) {
                // if(time < timerRange.getThreshold()) {
                setTextColor(mColorTextWarning);
//                // TBD: Need to figure out how to set active/pressed state color, etc.
//                setBackgroundColor(mColorBackgroundWarning);

                // Broadcast the event.
                for (CountDownEventListener listener : listeners) {
                    listener.onThresholdCross(this);
                }

                thresholdCrossHandled = true;
            }
        }

        // ???
        update(true);
    }

    @Override
    public void onFinish()
    {
        if (Log.I) Log.i("onFinish(): now = " + System.currentTimeMillis());

        // ???
        CharSequence text = formatTextWithCountDownTime(timerRange.getMin());
        super.setText(text);

        if(mode == MODE_ENABLE_ON_FINISH) {
            this.setEnabled(true);
        } else if(mode == MODE_DISABLE_ON_FINISH) {
            this.setEnabled(false);
        }

        // ???
        setState(TimerState.FINISHED);
        update(true);

        // Broadcast the event.
        for(CountDownEventListener listener : listeners) {
            listener.onFinish(this);
        }
    }


    // temporary
    private CharSequence formatTextWithCountDownTime(long time)
    {
        return CountDownUtil.addCountDownTime(super.getText(), time, countDownTimeDecimal);
    }



    // TBD:
    // How to save/restore countDownTimer ????
    // ....

    @Override
    public Parcelable onSaveInstanceState()
    {
        Log.d("PlayerTimerControl.onSaveInstanceState() Called.");

        Parcelable superState = super.onSaveInstanceState();
        SavedState savedState = new SavedState(superState);
        int currentState = getState();
        // Log.w(">>>>>>> onSaveInstanceState() currentState = " + currentState);
        savedState.setControlDisabled(controlDisabled);
        savedState.setState(currentState);
        savedState.setTimerText(super.getText().toString());
        savedState.setTimerMax(getTimerRange().getMax());
        savedState.setTimerMin(getTimerRange().getMin());
        savedState.setTimerThreshold(getTimerRange().getThreshold());
        savedState.setTimerValue(getTimerRange().getVal());
        return savedState;
    }

    @Override
    public void onRestoreInstanceState(Parcelable state)
    {
        Log.d("PlayerTimerControl.onRestoreInstanceState() Called.");

        SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(savedState.getSuperState());
        boolean controlDisabled = savedState.isControlDisabled();
        int controlState = savedState.getState();
        String timerText = savedState.getTimerText();
        long timerMax = savedState.getTimerMax();
        long timerMin = savedState.getTimerMin();
        long timerThreshold = savedState.getTimerThreshold();
        long timerValue = savedState.getTimerValue();
        // ???
        resetTimerRange(timerMax, timerMin, timerThreshold);
        // ???
        initTimer(true);

        // Log.w(">>>>>>> onSaveInstanceState() controlState = " + controlState);
        setState(controlState);
        super.setText(timerText);
        getTimerRange().setVal(timerValue);
        long ext = timerValue - getTimerRange().getMin();
        // ???
        // TBD: Keep the state???
        if(controlState == TimerState.RUNNING) {
            pause(ext);   // this calls setRepeats(PAUSED)...
            resume();     // this calls setRepeats(RUNNING) ???
        } else if(controlState == TimerState.PAUSED) {
            pause(ext);   // Hack. This is needed because we do not properly save/restore countDownTimer...
        } else {
            // pause(ext);   // ???
            // Or, just keep the current state???
        }
        // ???
        refreshTimer();
    }

    protected static class SavedState extends BaseSavedState implements Parcelable
    {
        private boolean controlDisabled;
        private int state;
        private String timerText;
        private long timerMax;
        private long timerMin;
        private long timerThreshold;
        private long timerValue;
        // etc...

        public SavedState(Parcelable superState)
        {
            super(superState);
        }
        public SavedState(Parcel source)
        {
            super(source);
            controlDisabled = (source.readInt() == 0) ? false : true;
            state = source.readInt();
            timerText = source.readString();
            timerMax = source.readLong();
            timerMin = source.readLong();
            timerThreshold = source.readLong();
            timerValue = source.readLong();
        }


        public boolean isControlDisabled()
        {
            return controlDisabled;
        }
        public void setControlDisabled(boolean controlDisabled)
        {
            this.controlDisabled = controlDisabled;
        }

        public int getState()
        {
            return state;
        }
        public void setState(int state)
        {
            this.state = state;
        }

        public String getTimerText()
        {
            return timerText;
        }
        public void setTimerText(String timerText)
        {
            this.timerText = timerText;
        }

        public long getTimerMax()
        {
            return timerMax;
        }
        public void setTimerMax(long timerMax)
        {
            this.timerMax = timerMax;
        }

        public long getTimerMin()
        {
            return timerMin;
        }
        public void setTimerMin(long timerMin)
        {
            this.timerMin = timerMin;
        }

        public long getTimerThreshold()
        {
            return timerThreshold;
        }
        public void setTimerThreshold(long timerThreshold)
        {
            this.timerThreshold = timerThreshold;
        }

        public long getTimerValue()
        {
            return timerValue;
        }
        public void setTimerValue(long timerValue)
        {
            this.timerValue = timerValue;
        }


        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            @Override
            public SavedState createFromParcel(Parcel source)
            {
                return new SavedState(source);
            }
            @Override
            public SavedState[] newArray(int size)
            {
                return new SavedState[size];
            }
        };

        @Override
        public void writeToParcel(Parcel dest, int flags)
        {
            super.writeToParcel(dest, flags);
            dest.writeInt(controlDisabled == true ? 1 : 0);
            dest.writeInt(state);
            dest.writeString(timerText);
            dest.writeLong(timerMax);
            dest.writeLong(timerMin);
            dest.writeLong(timerThreshold);
            dest.writeLong(timerValue);
        }


        // For debugging...
        @Override
        public String toString()
        {
            return "SavedState{" +
                    "controlDisabled=" + controlDisabled +
                    "state=" + state +
                    "timerText=" + timerText +
                    "timerMax=" + timerMax +
                    "timerMin=" + timerMin +
                    "timerThreshold=" + timerThreshold +
                    "timerValue=" + timerValue +
                    '}';
        }
    }








    private final Set<CountDownEventListener> listeners = new HashSet<CountDownEventListener>();

    public void addCountDownEventListener(CountDownEventListener listener)
    {
        if(listener != null) {
            listeners.add(listener);
        }
    }


}

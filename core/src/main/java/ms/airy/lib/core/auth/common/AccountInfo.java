package ms.airy.lib.core.auth.common;

import java.io.Serializable;

import ms.airy.lib.core.util.IdUtil;


// Unfortunately, we've created a number of similar classes,
//   which need to be refactored at some point.
// This class is for UI.
// It corresponds to a row in the account selection/management table.
// The table shows rows of "main" accounts, and their basic infos.
public class AccountInfo implements Serializable
{
    // "columns"
    private boolean primary;
    private final int accountType;    // We can display String, or an icon.
    private String accountName;
    private boolean authorized;  // if true, show "log out" button. If false, show "log in" button.
//    private String authToken;
//    private long authTime;


    public AccountInfo(int accountType)
    {
        this(accountType, null);   // ???
    }
    public AccountInfo(int accountType, String accountName)
    {
        this(accountType, accountName, false);
    }
    public AccountInfo(int accountType, String accountName, boolean authorized)
    {
        this.primary = false;
        this.accountType = accountType;
        this.accountName = accountName;
        this.authorized = authorized;
    }
//    public AccountInfo(int accountType, String accountName)
//    {
//        this(accountType, accountName, null);
//    }
//    public AccountInfo(int accountType, String accountName, String authToken)
//    {
//        this(accountType, accountName, authToken, 0L);
//    }
//    public AccountInfo(int accountType, String accountName, String authToken, long authTime)
//    {
//        this.primary = false;
//        this.accountType = accountType;
//        this.accountName = accountName;
//        this.authToken = authToken;
//        this.authTime = authTime;
//    }


    // temporary
    // Why do we need id???
    private volatile long id = IdUtil.generateRandomId05();
    public long getId()
    {
//        if(id == 0L) {
//        // (1) id changes every time accountName changes. Not good.
//        long result = accountType;
//        result = 31 * result + (accountName != null ? accountName.hashCode() : 0);
//        id = result;
//        // (2) Just use a random id.
//        id = IdUtil.generateRandomId05();
//        }
        return id;
    }


    public boolean isPrimary()
    {
        return primary;
    }
    public void setPrimary(boolean primary)
    {
        this.primary = primary;
    }
    public void setPrimary()
    {
        setPrimary(true);
    }

    public int getAccountType()
    {
        return accountType;
    }

    public String getAccountName()
    {
        return accountName;
    }
    public void setAccountName(String accountName)
    {
        setAccountName(accountName, false);
    }
    public void setAccountName(String accountName, boolean authorized)
    {
        if(accountName != null && accountName.equals(this.accountName)) {
            // Do nothing.
        } else {
            this.accountName = accountName;
            setAuthorized(authorized);
        }
    }
//    public void setAccountName(String accountName)
//    {
//        if(accountName != null && accountName.equals(this.accountName)) {
//            // Do nothing.
//        } else {
//            this.accountName = accountName;
//            setUnauthorized();
//        }
//    }
//    public void setAccountName(String accountName, String authToken, long authTime)
//    {
//        this.accountName = accountName;
//        this.authToken = authToken;
//        this.authTime = authTime;
//    }

    public boolean isAuthorized()
    {
        return authorized;
    }
    public void setAuthorized(boolean authorized)
    {
        this.authorized = authorized;
    }
    public void setAuthorized()
    {
        setAuthorized(true);
    }
    public void setUnauthorized()
    {
        setAuthorized(false);
    }

//    public boolean isAuthorized()
//    {
//        return (authToken != null);
//    }
//    public void setAuthorized(String authToken)
//    {
//        setAuthorized(authToken, System.currentTimeMillis());
//    }
//    public void setAuthorized(String authToken, long authTime)
//    {
//        this.authToken = authToken;
//        this.authTime = authTime;
//    }
//    public void setUnauthorized()
//    {
//        this.authToken = null;
//        this.authTime = 0L;
//    }

}

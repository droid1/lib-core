package ms.airy.lib.core.startup.job;

import ms.airy.lib.core.startup.job.condition.TaskCondition;
import ms.airy.lib.core.startup.task.ForegroundStartupTask;
import ms.airy.lib.core.startup.task.StartupTask;


public class BaseStartupJob implements StartupJob
{
    private final TaskCondition condition;
    private final StartupTask task;
    private int priority;
    private boolean backgroundJob;


    public BaseStartupJob(TaskCondition condition, StartupTask task)
    {
        this(condition, task, 0);
    }
    public BaseStartupJob(TaskCondition condition, StartupTask task, int priority)
    {
        this(condition, task, priority, false);
    }
    public BaseStartupJob(TaskCondition condition, StartupTask task, int priority, boolean backgroundJob)
    {
        this.condition = condition;
        this.task = task;
        this.priority = priority;
        setBackgroundJob(backgroundJob);
    }


    @Override
    public TaskCondition getCondition()
    {
        return condition;
    }

    @Override
    public StartupTask getTask()
    {
        return task;
    }


    @Override
    public int getPriority()
    {
        return priority;
    }
    public void setPriority(int priority)
    {
        this.priority = priority;
    }

    @Override
    public boolean isBackgroundJob()
    {
        return backgroundJob;
    }
    protected void setBackgroundJob(boolean backgroundJob)
    {
        // Note that background tasks can be run either foreground or background.
        //  On the other hand, foreground tasks can be only run on foreground.
        if(this.task instanceof ForegroundStartupTask) {
            this.backgroundJob = false;
        } else {
            this.backgroundJob = backgroundJob;
        }
    }

}

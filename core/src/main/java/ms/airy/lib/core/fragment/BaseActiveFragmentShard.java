package ms.airy.lib.core.fragment;

import java.util.List;

/**
 * Created by harry on 2/26/14.
 */
public abstract class BaseActiveFragmentShard implements ActiveFragment
{
    private final FragmentContainer fragmentContainer;
    private String channel = null;
    // private List<String> channels = new ArrayList<String>();

    public BaseActiveFragmentShard(FragmentContainer fragmentContainer)
    {
        this.fragmentContainer = fragmentContainer;
    }


    public String getChannel()
    {
        return channel;
    }
    protected void setChannel(String channel)
    {
        this.channel = channel;
    }


    protected FragmentContainer getFragmentContainer()
    {
        return fragmentContainer;
    }
//    protected void setFragmentContainer(FragmentContainer fragmentContainer)
//    {
//        this.fragmentContainer = fragmentContainer;
//    }


    @Override
    public boolean processContainerMessages(FragmentContainer container, String channel, List<FragmentMessage> messages)
    {
        return false;
    }

}

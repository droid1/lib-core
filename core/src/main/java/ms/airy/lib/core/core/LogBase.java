package ms.airy.lib.core.core;

import android.os.SystemClock;


// Base class for Log classes.
public abstract class LogBase
{
    // Log logTag. Immutable.
    private final String logTag;
    // Logger-specific Log level.
    // It can disable low-level logging,
    // but it cannot enable high-level which is disabled through system/device settings.
    private final int logLevel;

    protected LogBase(String tag)
    {
        this(tag, Config.LOGLEVEL);
    }
    protected LogBase(String tag, int level)
    {
        this.logTag = tag;
        this.logLevel = level;
    }

    protected final String getLogTag()
    {
        return logTag;
    }
//    protected final void setLogTag(String tag)
//    {
//        this.logTag = tag;
//    }


    protected final int getLogLevel()
    {
        return logLevel;
    }
//    protected final void setLogLevel(int level)
//    {
//        this.logLevel = level;
//    }


    // Returns true if the given log level is loggable in the current settings.
    protected final boolean isLoggable(int level)
    {
        return (getLogLevel() <= level) && android.util.Log.isLoggable(getLogTag(), level);
    }


    // Time reference.
    // Only used in V and D logging.
    private static long getTimestamp()
    {
        // long t = System.currentTimeMillis();    // truncate ???
        long t = SystemClock.uptimeMillis();
        return t;
    }

    protected final void logV(String logMe)
    {
        android.util.Log.v(getLogTag(), getTimestamp() + ": " + logMe);
    }
    protected final void logV(String logMe, Throwable ex)
    {
        android.util.Log.v(getLogTag(), getTimestamp() + ": " + logMe, ex);
    }

    protected final void logD(String logMe)
    {
        android.util.Log.d(getLogTag(), getTimestamp() + ": " + logMe);
    }
    protected final void logD(String logMe, Throwable ex)
    {
        android.util.Log.d(getLogTag(), getTimestamp() + ": " + logMe, ex);
    }

    protected final void logI(String logMe)
    {
        android.util.Log.i(getLogTag(), logMe);
    }
    protected final void logI(String logMe, Throwable ex)
    {
        android.util.Log.i(getLogTag(), logMe, ex);
    }

    protected final void logW(String logMe)
    {
        android.util.Log.w(getLogTag(), logMe);
    }
    protected final void logW(String logMe, Throwable ex)
    {
        android.util.Log.w(getLogTag(), logMe, ex);
    }

    protected final void logE(String logMe)
    {
        android.util.Log.e(getLogTag(), logMe);
    }
    protected final void logE(String logMe, Throwable ex)
    {
        android.util.Log.e(getLogTag(), logMe, ex);
    }

}

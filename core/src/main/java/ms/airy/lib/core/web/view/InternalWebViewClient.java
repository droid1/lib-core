package ms.airy.lib.core.web.view;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;


// temporary
// Not being used...
public class InternalWebViewClient extends WebViewClient
{
    private final Context context;
    public InternalWebViewClient(Context context)
    {
        // ???
        this.context = (context != null) ? context.getApplicationContext() : null;
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url)
    {
        // TBD:
        // I'm not sure if this really makes sense.....
        if (Uri.parse(url).isRelative()) {
            // (1) This is my web site, so do not override; let my WebView load the page
            return false;
        }
        if(context != null) { // ???
            // (2) Otherwise, the link is not for a page on my site, so launch another Activity that handles URLs
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            context.startActivity(intent);
            return true;
        }
        // (3) ??? Can this happen ???
        return false;
    }

}
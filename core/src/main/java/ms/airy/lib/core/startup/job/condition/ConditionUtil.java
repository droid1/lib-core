package ms.airy.lib.core.startup.job.condition;

import java.util.ArrayList;
import java.util.List;



public final class ConditionUtil
{
    private ConditionUtil() {}

    // TBD:
    // Use StartupConditionHelper.filterTasks().
    public static List<TaskCondition> filterConditions(List<TaskCondition> inList, boolean isFirstStartup, int startupCounterForVersion)
    {
        if(inList == null || inList.isEmpty()) {
            return inList;
        }

        List<TaskCondition> outList = new ArrayList<TaskCondition>();
        for(TaskCondition tc : inList) {
            if(tc == null) { // ???
                continue;
            }
            if(tc.isOnlyForFirstStartup()) {
                if(isFirstStartup) {
                    outList.add(tc);
                }
            } else {
                if(tc.getThresholdStartupCounter() <= startupCounterForVersion
                        // && tc.getThresholdStartupCounter() + 2 > startupCounterForVersion  // ???
                        ) {
                    outList.add(tc);
                }
            }
        }

        return outList;
    }


}

package ms.airy.lib.core.feedback.client.callback;

import ms.airy.lib.core.feedback.client.AsyncSurveyCallback;
import ms.airy.lib.core.feedback.common.SurveyEntry;


/**
 */
public abstract class AbstractAsyncOnlineSurveyCallback implements AsyncSurveyCallback
{
    @Override
    public void onGetSpecialSurvey(SurveyEntry surveyEntry)
    {

    }

    @Override
    public void onGetDailySurvey(SurveyEntry surveyEntry, long time)
    {

    }

    @Override
    public void onGetSurvey(SurveyEntry surveyEntry, long id)
    {

    }

}

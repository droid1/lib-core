package ms.airy.lib.core.device.screen;

import android.content.Context;
import android.content.res.Configuration;


public final class ScreenSizeHelper
{
    private static ScreenSizeHelper INSTANCE = null;

    public static ScreenSizeHelper getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new ScreenSizeHelper(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // We use the application context.
    private final Context context;

    private ScreenSizeHelper(Context context)
    {
        this.context = context;
    }


    // temporary
    // cf. http://developer.android.com/guide/practices/screens-distribution.html
    public boolean isRunningOnTablet()
    {
        // ????
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;

//        boolean xlarge = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == 4);
//        boolean large = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE);
//        return (xlarge || large);
    }

    // Returns one of
    // 1: Configuration.SCREENLAYOUT_SIZE_SMALL
    // 2: Configuration.SCREENLAYOUT_SIZE_NORMAL
    // 3: Configuration.SCREENLAYOUT_SIZE_LARGE
    // 4: Configuration.SCREENLAYOUT_SIZE_XLARGE
    public int getScreenSize()
    {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK);
    }


}

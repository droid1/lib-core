package ms.airy.lib.core.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


// Base class.
// Each Activity may use a different subclass of FragmentContainerSupportFragment.
public abstract class FragmentContainerSupportFragment extends Fragment implements FragmentContainer
{
    private final Set<Fragment> globalQueue = new HashSet<Fragment>();
    private final Map<String, Set<Fragment>> channelQueue = new HashMap<String, Set<Fragment>>();

    public FragmentContainerSupportFragment()
    {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return null;
    }


    protected Set<Fragment> getGlobalQueue()
    {
        return globalQueue;
    }

    protected Map<String, Set<Fragment>> getChannelQueue()
    {
        return channelQueue;
    }


    @Override
    public boolean registerFragment(Fragment fragment, String channel)
    {
        if(Log.D) Log.d("registerFragment() fragment = " + fragment + "; channel = " + channel);

        if(channel == null || channel.isEmpty()) {
            globalQueue.add(fragment);
        } else {
            Set<Fragment> set = channelQueue.get(channel);
            if(set == null) {
                set = new HashSet<Fragment>();
                channelQueue.put(channel, set);
            }
            set.add(fragment);
        }
        return true;
    }

    @Override
    public boolean unregisterFragment(Fragment fragment)
    {
        if(Log.D) Log.d("unregisterFragment() fragment = " + fragment);

        globalQueue.remove(fragment);
        for(String channel : channelQueue.keySet()) {
            Set<Fragment> set = channelQueue.get(channel);
            if(set != null) {
                set.remove(fragment);
            }
        }
        return true;
    }

    @Override
    public boolean processFragmentMessages(Fragment sender, String channel, List<FragmentMessage> messages)
    {
        if(Log.D) {
            Log.d("processFragmentMessages() sender = " + sender + "; channel = " + channel);
            if(messages != null && !messages.isEmpty()) {
                for(FragmentMessage fm : messages) {
                    Log.d("fm = " + fm);
                }
            }
        }

        // TBD:
        // [1] Process whatever is needed in tha activity.
        // ...

        // Then:
        // [2] Call all ActiveFragments.
        boolean suc = relayFragmentMessages(sender, channel, messages);

        // temporary
        return false;
    }

    protected boolean relayFragmentMessages(Fragment sender, String channel, List<FragmentMessage> messages)
    {
        if(channel == null || channel.isEmpty()) {
            for(Fragment f : globalQueue) {
                if(f instanceof ActiveFragment) {
                    ((ActiveFragment) f).processContainerMessages((FragmentContainer) getActivity(), channel, messages);
                } else {
                    // ignore...
                }
            }
        } else {
            Set<Fragment> set = channelQueue.get(channel);
            if(set != null && !set.isEmpty()) {
                for(Fragment f : set) {
                    if(f instanceof ActiveFragment) {
                        ((ActiveFragment) f).processContainerMessages((FragmentContainer) getActivity(), channel, messages);
                    } else {
                        // ignore...
                    }
                }
            }
        }

        // temporary
        return false;
    }

}

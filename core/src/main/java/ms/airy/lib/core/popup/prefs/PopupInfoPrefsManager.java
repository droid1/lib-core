package ms.airy.lib.core.popup.prefs;

import android.content.Context;
import android.content.SharedPreferences;


// TBD:
public final class PopupInfoPrefsManager
{
    // temporary
    private static final String SP_FILENAME_SUFFIX = ".popup_info_prefs";
    private static final String KEY_COUNTER_SUFFIX = ".counter";
    private static final String KEY_RESPONSE_SUFFIX = ".response";
    private static final String KEY_ACTION_TIME_SUFFIX = ".actiontime";
    // ...


    private static PopupInfoPrefsManager INSTANCE = null;
    public static PopupInfoPrefsManager getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new PopupInfoPrefsManager(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;

    // Separate Shared Preferences file to store "popup info" related data.
    private final String sharedPrefsFile;
    private final SharedPreferences sharedPreferences;

    private PopupInfoPrefsManager(Context context)
    {
        this.context = context;
        sharedPrefsFile = context.getPackageName() + SP_FILENAME_SUFFIX;
        sharedPreferences = context.getSharedPreferences(sharedPrefsFile, Context.MODE_PRIVATE);
    }
    public String getSharedPrefsFile()
    {
        return sharedPrefsFile;
    }

//    protected String getSharedPrefsFile()
//    {
//        return this.sharedPrefsFile;
//    }

    private static String getCounterKey(String keyRoot)
    {
        return keyRoot + KEY_COUNTER_SUFFIX;
    }
    private static String getResponseKey(String keyRoot)
    {
        return keyRoot + KEY_RESPONSE_SUFFIX;
    }
    private static String getActionTimeKey(String keyRoot)
    {
        return keyRoot + KEY_ACTION_TIME_SUFFIX;
    }


    public int getCounter(String keyRoot)
    {
        return getCounter(keyRoot, 0);
    }
    public int getCounter(String keyRoot, int defValue)
    {
        return sharedPreferences.getInt(getCounterKey(keyRoot), defValue);
    }
    public void setCounter(String keyRoot, int value)
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(getCounterKey(keyRoot), value);
        editor.commit();
    }
    public int increaseCounter(String keyRoot)
    {
        int counter = getCounter(keyRoot);
        setCounter(keyRoot, ++counter);
        return counter;
    }

    public int getResponse(String keyRoot)
    {
        // Valid values for response are -1, 0, 1.
        // TBD: Use a different default value????
        return getResponse(keyRoot, 0);
    }
    public int getResponse(String keyRoot, int defValue)
    {
        return sharedPreferences.getInt(getResponseKey(keyRoot), defValue);
    }
    public void setResponse(String keyRoot, int value)
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(getResponseKey(keyRoot), value);
        editor.commit();
    }

    public long getActionTime(String keyRoot)
    {
        return getActionTime(keyRoot, 0L);
    }
    public long getActionTime(String keyRoot, long defValue)
    {
        return sharedPreferences.getLong(getActionTimeKey(keyRoot), defValue);
    }
    public void setActionTime(String keyRoot, long value)
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(getActionTimeKey(keyRoot), value);
        editor.commit();
    }
    public void setActionTime(String keyRoot)
    {
        long now = System.currentTimeMillis();
        setActionTime(keyRoot, now);
    }


    // We could check either counter or actionTime.
    public boolean isPopupShown(String keyRoot)
    {
        if(getCounter(keyRoot) > 0) {  // or if(getActionTime(keyRoot) > 0L)
            return true;
        } else {
            return false;
        }
    }


    // For debugging/testing purposes
    protected void removePrefs(String keyRoot)
    {
        Log.w("======== PopupInfoPrefsManager.removePrefs() keyRoot = " + keyRoot);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(getCounterKey(keyRoot));
        editor.remove(getResponseKey(keyRoot));
        editor.remove(getActionTimeKey(keyRoot));
        editor.commit();
    }
    protected void removeAllPrefs()
    {
        Log.w("======== PopupInfoPrefsManager.removeAllPrefs()");

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }


}

package ms.airy.lib.core.feedback.client.async;

import ms.airy.lib.core.feedback.client.AsyncSurveyCallback;
import ms.airy.lib.core.feedback.client.AsyncSurveyClient;
import ms.airy.lib.core.feedback.client.task.AsyncSurveyGetDailySurveyTask;
import ms.airy.lib.core.feedback.client.task.AsyncSurveyGetSpecialSurveyTask;
import ms.airy.lib.core.feedback.client.task.AsyncSurveyGetSurveyTask;
import ms.airy.lib.core.feedback.common.SurveyEntry;


/**
 */
public class AsyncOnlineSurveyClient implements AsyncSurveyClient,
        AsyncSurveyGetSpecialSurveyTask.AsyncTaskCallback,
        AsyncSurveyGetDailySurveyTask.AsyncTaskCallback,
        AsyncSurveyGetSurveyTask.AsyncTaskCallback
{
    private final AsyncSurveyCallback callback;
//    private final AsyncSurveyGetSpecialSurveyTask asyncSurveyGetSpecialSurveyTask;
//    private final AsyncSurveyGetDailySurveyTask asyncSurveyGetDailySurveyTask;
//    private final AsyncSurveyGetSurveyTask asyncSurveyGetSurveyTask;

    public AsyncOnlineSurveyClient(AsyncSurveyCallback callback)
    {
        this.callback = callback;
//        this.asyncSurveyGetSpecialSurveyTask = new AsyncSurveyGetSpecialSurveyTask(this);
//        this.asyncSurveyGetDailySurveyTask = new AsyncSurveyGetDailySurveyTask(this);
//        this.asyncSurveyGetSurveyTask = new AsyncSurveyGetSurveyTask(this);
    }


    @Override
    public void getSpecialSurvey()
    {
        Log.i("AsyncOnlineSurveyClient.getSpecialSurvey().");

        (new AsyncSurveyGetSpecialSurveyTask(this)).execute();
    }

    @Override
    public void getDailySurvey(long time)
    {
        if(Log.I) Log.i("AsyncOnlineSurveyClient.getDailySurvey(). time = " + time);

        (new AsyncSurveyGetDailySurveyTask(this)).execute(time);
    }

    @Override
    public void getSurvey(long id)
    {
        if(Log.I) Log.i("AsyncOnlineSurveyClient.getSurvey(). id = " + id);

        (new AsyncSurveyGetSurveyTask(this)).execute(id);
    }



    //////////////////////////////////////
    // AsyncSurveyGetSpecialSurveyTask.AsyncTaskCallback interface

    @Override
    public void onGetSpecialSurvey(SurveyEntry surveyEntry)
    {
        callback.onGetSpecialSurvey(surveyEntry);
    }


    //////////////////////////////////////
    // AsyncSurveyGetDailySurveyTask.AsyncTaskCallback interface

    @Override
    public void onGetDailySurvey(SurveyEntry surveyEntry, long time)
    {
        callback.onGetDailySurvey(surveyEntry, time);
    }


    //////////////////////////////////////
    // AsyncSurveyGetSurveyTask.AsyncTaskCallback interface

    @Override
    public void onGetSurvey(SurveyEntry surveyEntry, long id)
    {
        callback.onGetSurvey(surveyEntry, id);
    }


}

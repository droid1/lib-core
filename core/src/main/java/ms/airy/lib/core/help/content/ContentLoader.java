package ms.airy.lib.core.help.content;


/**
 */
public interface ContentLoader
{
    String getContent(long sectionId);
}

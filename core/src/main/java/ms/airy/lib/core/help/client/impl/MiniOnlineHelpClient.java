package ms.airy.lib.core.help.client.impl;

import ms.airy.lib.core.help.client.OnlineHelpClient;

import org.miniclient.ApiUserClient;
import org.miniclient.RestApiException;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 */
public class MiniOnlineHelpClient extends AbstractOnlineHelpClient implements OnlineHelpClient
{
    private ApiUserClient miniHelpWebClient = null;

    public MiniOnlineHelpClient()
    {
        this((String) null);   // ????
    }
    public MiniOnlineHelpClient(String helpWebServiceEndpoint)
    {
        // this(helpWebServiceEndpoint, MiniClientBuilder.getInstance().getHelpWebClient(helpWebServiceEndpoint));
        super(helpWebServiceEndpoint);
        this.miniHelpWebClient = MiniClientBuilder.getInstance().getHelpWebClient(helpWebServiceEndpoint);
    }
//    private MiniOnlineHelpClient(String helpWebServiceEndpoint, ApiUserClient miniHelpWebClient)
//    {
//        // ????
//        super(helpWebServiceEndpoint);
//        this.miniHelpWebClient = miniHelpWebClient;
//    }
    public MiniOnlineHelpClient(ApiUserClient miniHelpWebClient)
    {
        super(miniHelpWebClient.getResourceBaseUrl());   // ???
        this.miniHelpWebClient = miniHelpWebClient;
    }

    public ApiUserClient getMiniHelpWebClient()
    {
        return miniHelpWebClient;
    }
    protected void setMiniHelpWebClient(String helpWebServiceEndpoint)
    {
        super.setHelpWebServiceEndpoint(helpWebServiceEndpoint);
        this.miniHelpWebClient = MiniClientBuilder.getInstance().getHelpWebClient(helpWebServiceEndpoint);
    }
    protected void setMiniHelpWebClient(ApiUserClient miniHelpWebClient)
    {
        super.setHelpWebServiceEndpoint(miniHelpWebClient.getResourceBaseUrl());   // ???
        this.miniHelpWebClient = miniHelpWebClient;
    }

    @Override
    protected void setHelpWebServiceEndpoint(String helpWebServiceEndpoint)
    {
        super.setHelpWebServiceEndpoint(helpWebServiceEndpoint);
        this.miniHelpWebClient = MiniClientBuilder.getInstance().getHelpWebClient(helpWebServiceEndpoint);
    }


    @Override
    public String getSectionContent(long sectionId)
    {
        Log.d("MiniOnlineHelpClient.getSectionContent(). sectionId = " + sectionId);

        // TBD: ...

        Object section = null;
        try {
            // TBD:
            // section = miniHelpWebClient.get(Long.toString(sectionId));   /// ???

            Map<String,Object> params = new HashMap<String, Object>();
            params.put("sectionId", sectionId);  // ???
            List<Object> sections = miniHelpWebClient.list(params);
            if(sections != null && sections.size() > 0) {
                section = sections.get(0);   // ???
            }
        } catch (RestApiException e) {
            Log.e("Failed to get section for id = " + sectionId, e);
        } catch (IOException e) {
            Log.e("IO Exception while fetching section for id = " + sectionId, e);
        }

        // ...

        String content = null;
        if(section != null) {
            if(section instanceof Map<?,?>) {
                // ???
                content = (String) ((Map<String,Object>) ((Map<?,?>) section)).get("content");
            }
        }

        return content;
    }

}

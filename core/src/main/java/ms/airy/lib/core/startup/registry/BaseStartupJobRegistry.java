package ms.airy.lib.core.startup.registry;

import java.util.ArrayList;
import java.util.List;

import ms.airy.lib.core.startup.job.StartupJob;


// TBD:
public class BaseStartupJobRegistry implements StartupJobRegistry
{
    private final List<StartupJob> startupJobs = new ArrayList<StartupJob>();

    public BaseStartupJobRegistry()
    {
    }

    // Should return non-null list (even if it's empty).
    @Override
    public List<StartupJob> getStartupJobs()
    {
        return startupJobs;
    }

    @Override
    public void addJob(StartupJob job)
    {
        startupJobs.add(job);
    }


}

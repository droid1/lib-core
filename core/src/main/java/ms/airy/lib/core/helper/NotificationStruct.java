package ms.airy.lib.core.helper;

import android.content.Intent;
import android.net.Uri;

public class NotificationStruct
{
    // TBD: Add more fields????
    private int id;     // NotificationId
    private int icon;   // Resource id
    private long time;
    private CharSequence tickerText;
    private CharSequence contentTitle;
    private CharSequence contentText;
    private String contentAction;
    private Uri contentUri;
    // TBD:
    //private Intent intent;
    // ....
    

    public NotificationStruct(int id)
    {
        this(id, 0, 0L, null, null, null, null, null);
    }

    public NotificationStruct(int id, int icon, long time, CharSequence tickerText, 
            CharSequence contentTitle, CharSequence contentText,String contentAction, Uri contentUri)
    {
        this.id = id;
        this.icon = icon;
        this.time = time;
        this.tickerText = tickerText;
        this.contentTitle = contentTitle;
        this.contentText = contentText;
        this.contentAction = contentAction;
        this.contentUri = contentUri;
    }



    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }


    public int getIcon()
    {
        return icon;
    }

    public void setIcon(int icon)
    {
        this.icon = icon;
    }


    public long getTime()
    {
        return time;
    }

    public void setTime(long time)
    {
        this.time = time;
    }


    public CharSequence getTickerText()
    {
        return tickerText;
    }

    public void setTickerText(CharSequence tickerText)
    {
        this.tickerText = tickerText;
    }


    public CharSequence getContentTitle()
    {
        return contentTitle;
    }

    public void setContentTitle(CharSequence contentTitle)
    {
        this.contentTitle = contentTitle;
    }

    
    public CharSequence getContentText()
    {
        return contentText;
    }

    public void setContentText(CharSequence contentText)
    {
        this.contentText = contentText;
    }

    
    public String getContentAction()
    {
        return contentAction;
    }

    public void setContentAction(String contentAction)
    {
        this.contentAction = contentAction;
    }
    

    public Uri getContentUri()
    {
        return contentUri;
    }

    public void setContentUri(Uri contentUri)
    {
        this.contentUri = contentUri;
    }

    
    @Override
    public String toString()
    {
        return "NotificationStruct [id=" + id + ", icon=" + icon + ", time="
                + time + ", tickerText=" + tickerText + ", contentTitle="
                + contentTitle + ", contentText=" + contentText
                + ", contentAction=" + contentAction + ", contentUri="
                + contentUri + "]";
    }

    
    
}

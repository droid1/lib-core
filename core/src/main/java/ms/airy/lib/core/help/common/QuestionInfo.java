package ms.airy.lib.core.help.common;

import android.content.Context;


// An item in the the "question list" as well as in the "question and answer" page.
// (Use case: FAQs, How-to docs, quick guides, etc.)
public class QuestionInfo
{
    // Unique Id within a list. > 0L.
    private final long id;

    // Custom icons??? (as a header in the list, or in the answer page....)
    // private int questionIconResId;  // small
    // private int answerIconResId;    // large

    // Title. (Plain text)
    private String questionText;
    private int questionResId;

    // Description (HTML)
    private String answerText;
    private int answerResId;

    // External reference url???
    // private String referenceUrl;

    // tbd:
    // inline images?? (e.g., html?) for screenshots, etc. ???
    // ...

    // (Optional) synopsis/summary, or "short answer".
    // (or, alternatively, this field can be used as a "long question".)
    // private String summaryText;
    // private int summaryResId;


    // Whether the user has viewed this particular QandA item...
    // private boolean viewed;   // Or "isNew" flag...
    // --> In order for this to work,
    //     (1) We need a unique/persitent uuid (the id of an item might change from release to release, etc.),
    //     (2) and, the persistent storage (e.g., SharedPrefs) to keep track of this...
    // ....


    public QuestionInfo(long id)
    {
        this(id, null, null);
    }
    public QuestionInfo(long id, String questionText, String answerText)
    {
        this.id = id;
        // TBD: Check if text != null and non-empty ???
        this.questionText = questionText;
        this.answerText = answerText;
        this.questionResId = 0;
        this.answerResId = 0;
    }
    public QuestionInfo(long id, int questionResId, int answerResId)
    {
        this.id = id;
        // TBD: Check if res ids are > 0 ???
        this.questionResId = questionResId;
        this.answerResId = answerResId;
        this.questionText = null;
        this.answerText = null;
    }

    public final long getId()
    {
        return id;
    }

    // Note:
    //      Texts, if both text and ids are set, take precedence over Res ids.
    // Also, Q and A are always used as a pair.
    // ...

    public String getQuestion(Context context)
    {
        if(questionText != null && !questionText.isEmpty()) {
            return questionText;
        } else if(questionResId > 0) {
            if(context != null) {
                return context.getResources().getString(questionResId);
            }
        }
        return null;
    }
    public String getAnswer(Context context)
    {
        // Note that we are checking questionText not answerText.
        // (e.g. if answerText is set, but questionText is not set (e.g., bacause questionResId is used),
        //       then answerResId is used.)
        if(questionText != null && !questionText.isEmpty()) {
            return answerText;  // this can be null or empty.
        } else if(answerResId > 0) {
            if(context != null) {
                return context.getResources().getString(answerResId);
            }
        }
        return null;
    }


    public final String getQuestionText()
    {
        return questionText;
    }
    public final String getAnswerText()
    {
        return answerText;
    }
    public final void setQuestionAndAnswerText(String questionText, String answerText)
    {
        // TBD: Check if text != null and non-empty ???
        this.questionText = questionText;
        this.answerText = answerText;
        this.questionResId = 0;
        this.answerResId = 0;
    }

    public final int getQuestionResId()
    {
        return questionResId;
    }
    public final int getAnswerResId()
    {
        return answerResId;
    }
    public final void setQuestionAndAnswerResId(int questionResId, int answerResId)
    {
        // TBD: Check if res ids are > 0 ???
        this.questionResId = questionResId;
        this.answerResId = answerResId;
        this.questionText = null;
        this.answerText = null;
    }


    // For debugging...
    @Override
    public String toString()
    {
        return "QuestionInfo{" +
                "id=" + id +
                ", questionText='" + questionText + '\'' +
                ", questionResId=" + questionResId +
                ", answerText='" + answerText + '\'' +
                ", answerResId=" + answerResId +
                '}';
    }

}

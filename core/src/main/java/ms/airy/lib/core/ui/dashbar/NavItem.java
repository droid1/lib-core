package ms.airy.lib.core.ui.dashbar;

import ms.airy.lib.core.common.GUID;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;


// Modeled after ActionBar.Tab.
public class NavItem
{
    // temporary
    private static final int DEFAULT_BUTTON_WIDTH = 240;   // in pixels
    private static final int DEFAULT_BUTTON_HEIGHT = 120;   // in pixels
    private static final int DEFAULT_ICON_WIDTH = 180;   // in pixels
    private static final int DEFAULT_ICON_HEIGHT = 100;   // in pixels
    
    
    // Context
    private Context mContext;
    
    // id/idTag
    // Should be unique within a given DashBar (or, across an app????)
    private String idTag;
    

    // Menu icon. TBD: Use int (resId) ??????
    private Drawable icon;
    
    // Position/Index ????
    //private int position;
    
    // label
    private String label;

    // State????  Normal, Focused, Pressed, ....
    private String state;
    
    
    // "View" ....
    private NavButton button;
    
    
    
    // If true, the dashbar nav list will be replaced. --> Use NavTarget.type
    //private boolean fullNav;

    
    // ...  ??? 
    private NavTarget target;
    
   
    // .... Store the parent?????
    //private NavItemList parent;
    
    
    // getter/setter ????
    private int mButtonWidth = DEFAULT_BUTTON_WIDTH;
    private int mButtonHeight = DEFAULT_BUTTON_HEIGHT;
    private int mIconWidth = DEFAULT_ICON_WIDTH;
    private int mIconHeight = DEFAULT_ICON_HEIGHT;

    
    
    public NavItem(Context context)
    {
        this(context, GUID.generateShortString());
    }
    public NavItem(Context context, String idTag)
    {
        this(context, idTag, "");  // ?????
    }
    public NavItem(Context context, String idTag, String label)
    {
        this(context, idTag, label, null);  // ?????
    }
    public NavItem(Context context, String idTag, String label, Drawable icon)
    {
        this(context, idTag, label, icon, new NavTarget());  // ?????
    }
    public NavItem(Context context, String idTag, String label, Drawable icon, NavTarget target)
    {
        mContext = context;
        this.idTag = idTag;
        
        // ????
        //AttributeSet attrs = null;
        button = new NavButton(mContext, idTag);
        button.setText(label);
        button.setIcon(icon);
        // set other attributes, etc...
        
        // TBD
        button.setMinWidth(getButtonWidth());
        button.setMaxWidth(getButtonWidth());
        button.setMinHeight(getButtonHeight());
        button.setMaxHeight(getButtonHeight());
        // ....
        
        // .....
        this.target = target;
        
        // TBD: ...
        // ....
        
    }

    
    // TBD: Setters????
    private int getButtonWidth()
    {
        return mButtonWidth;
    }
    private int getButtonHeight()
    {
        return mButtonHeight;
    }
    private int getIconWidth()
    {
        return mIconWidth;
    }
    private int getIconHeight()
    {
        return mIconHeight;
    }
    
    
    public NavButton getNavButton()
    {
        return button;
    }

    

    public String getIdTag()
    {
        return idTag;
    }

    public void setIdTag(String idTag)
    {
        this.idTag = idTag;
        button.setIdTag(idTag);
    }


    public Drawable getIcon()
    {
        return icon;
    }

    public void setIcon(Drawable icon)
    {
        this.icon = icon;
        
        if(icon != null) {
            // ??? 
            // TBD: Do this in NavButton????
            int left = (int) (getButtonWidth() - getIconWidth())/2;
            int top = 20;
            int right = left + getIconWidth();
            int bottom = top + getIconHeight();
            icon.setBounds(left, top, right, bottom);
            
            
            button.setIcon(icon);
        } else {
            // ????
        }
    }



    public String getLabel()
    {
        return label;
    }

    public void setLabel(String label)
    {
        this.label = label;
        // ????
        button.setText(label);
    }



    public NavTarget getTarget()
    {
        return target;
    }

    public void setTarget(NavTarget target)
    {
        this.target = target;
    }

    
    
}

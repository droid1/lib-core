package ms.airy.lib.core.phaser.common;


// TBD: Do we need this?
//      Just use integer ????
public final class Rank
{
    // User Rank definitions.
    // note: 0 is an invalid value (with special meaning)....
    // public static final int R00 = 0;
    public static final int R01 = 1;
    public static final int R02 = 2;
    public static final int R03 = 3;
    public static final int R04 = 4;
    public static final int R05 = 5;
    // ...

    // Inclusive
    public static final int MIN = R01;
    // Exclusive
    public static final int MAX = R05 + 1;

    // Static constants/methods only.
    private Rank() {}


    public static boolean isValid(int rank)
    {
        return (rank >= MIN) && (rank < MAX);
    }


    public static final class Range
    {
        private Integer min;   // Inclusive
        private Integer max;   // Exclusive

        public Range(Integer min, Integer max)
        {
            this.min = min;
            this.max = max;
        }

        public Integer getMin()
        {
            return min;
        }
        public Integer getMax()
        {
            return max;
        }
        public Integer setMax(Integer max)
        {
            Integer cur = this.max;
            this.max = max;
            return cur;
        }
    };

}

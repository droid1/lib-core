package ms.airy.lib.core.toast;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


// Static class version of ToastHelper.
// Either should be fine.
//     The only difference is that ToastHelper uses the global application context.
// Show one toast at a time. Also, clean up the toast, if any, when the app closes.
//   Toast toast = Toast.makeText(context, toastText, Toast.LENGTH_LONG);
//   ToastManager.showToast(toast);
public class ToastManager extends ToastUtil
{
    // Only one toast at a time.
    private static Toast sToast = null;

    private ToastManager() {}

    public static void showToast(Context context, CharSequence text)
    {
        showToast(context, text, Toast.LENGTH_SHORT);
    }
    public static void showToast(Context context, int resId)
    {
        showToast(context, resId, Toast.LENGTH_SHORT);
    }
    
    public static void showToast(Context context, CharSequence text, int duration)
    {
        showToast(context, text, duration, DEFAULT_GRAVITY, DEFAULT_XOFFSET, DEFAULT_YOFFSET);
    }
    public static void showToast(Context context, int resId, int duration)
    {
        showToast(context, resId, duration, DEFAULT_GRAVITY, DEFAULT_XOFFSET, DEFAULT_YOFFSET);
    }

    public static void showToast(Context context, CharSequence text, int duration, int gravity, int xOffset, int yOffset)
    {
        if(Log.V) Log.v("showToast() called with text = " + text);
        Toast toast = Toast.makeText(context, text, duration);
        toast.setGravity(gravity, xOffset, yOffset);
        showToast(toast, false);
    }
    public static void showToast(Context context, CharSequence text, int duration, int gravity, int xOffset, int yOffset, int textColor)
    {
        if(Log.V) Log.v("showToast() called with text = " + text);
        Toast toast = Toast.makeText(context, text, duration);
        toast.setGravity(gravity, xOffset, yOffset);
        View toastView = toast.getView();
        if(toastView != null) {
            TextView v = (TextView) toastView.findViewById(android.R.id.message);
            if (v != null) {
                v.setTextColor(textColor);
            }
        }
        showToast(toast, false);
    }
    public static void showToast(Context context, CharSequence text, int duration, int gravity, int xOffset, int yOffset, int textColor, int backgroundColor)
    {
        if(Log.V) Log.v("showToast() called with text = " + text);
        Toast toast = Toast.makeText(context, text, duration);
        toast.setGravity(gravity, xOffset, yOffset);
        View toastView = toast.getView();
        if(toastView != null) {
            TextView v = (TextView) toastView.findViewById(android.R.id.message);
            if (v != null) {
                v.setTextColor(textColor);
                v.setBackgroundColor(backgroundColor);
                // tbd
                Drawable bgDrawable = toastView.getBackground();
                if(bgDrawable != null && bgDrawable instanceof ShapeDrawable) {
                    ((ShapeDrawable) bgDrawable).getPaint().setColor(backgroundColor);
                } else if(bgDrawable != null && bgDrawable instanceof GradientDrawable) {
                    ((GradientDrawable) bgDrawable).setColor(backgroundColor);
                } else {
                    toastView.setBackgroundColor(backgroundColor);
                }
                // tbd
            }
        }
        showToast(toast, false);
    }

    public static void showToast(Context context, int resId, int duration, int gravity, int xOffset, int yOffset)
    {
        if(Log.V) Log.v("showToast() called with resId = " + resId);
        Toast toast = Toast.makeText(context, resId, duration);
        toast.setGravity(gravity, xOffset, yOffset);
        showToast(toast, false);
    }
    public static void showToast(Context context, int resId, int duration, int gravity, int xOffset, int yOffset, int textColor)
    {
        if(Log.V) Log.v("showToast() called with resId = " + resId);
        Toast toast = Toast.makeText(context, resId, duration);
        toast.setGravity(gravity, xOffset, yOffset);
        View toastView = toast.getView();
        if(toastView != null) {
            TextView v = (TextView) toastView.findViewById(android.R.id.message);
            if (v != null) {
                v.setTextColor(textColor);
            }
        }
        showToast(toast, false);
    }
    public static void showToast(Context context, int resId, int duration, int gravity, int xOffset, int yOffset, int textColor, int backgroundColor)
    {
        if(Log.V) Log.v("showToast() called with resId = " + resId);
        Toast toast = Toast.makeText(context, resId, duration);
        toast.setGravity(gravity, xOffset, yOffset);
        View toastView = toast.getView();
        if(toastView != null) {
            TextView v = (TextView) toastView.findViewById(android.R.id.message);
            if (v != null) {
                v.setTextColor(textColor);
                v.setBackgroundColor(backgroundColor);
                // tbd
                Drawable bgDrawable = toastView.getBackground();
                if(bgDrawable != null && bgDrawable instanceof ShapeDrawable) {
                    ((ShapeDrawable) bgDrawable).getPaint().setColor(backgroundColor);
                } else if(bgDrawable != null && bgDrawable instanceof GradientDrawable) {
                    ((GradientDrawable) bgDrawable).setColor(backgroundColor);
                } else {
                    toastView.setBackgroundColor(backgroundColor);
                }
                // tbd
            }
        }
        showToast(toast, false);
    }


    public static void showToast(Toast toast)
    {
        showToast(toast, true);
    }

    public static void showToast(Toast toast, boolean forceDefault)
    {
        if(Log.V) Log.v("showToast() called with toast = " + toast + "; forceDefault = " + forceDefault);
        // ???? 
        // TBD: Either remove the exiting one and show the new one immediately
        //      or, wait until the previous one is done......
//        if(sToast != null) {
//            sToast.cancel();
//            sToast = null;
//        }
        if(forceDefault) {
            toast.setGravity(DEFAULT_GRAVITY, DEFAULT_XOFFSET, DEFAULT_YOFFSET);
        }
        if(sToast != null) {
            int gravity = sToast.getGravity();
            int xoffset = sToast.getXOffset();
            int yoffset = sToast.getYOffset();
            toast.setGravity(gravity, xoffset, yoffset + YDELTA);
            //toast.setGravity(toast.getGravity(), toast.getXOffset(), yoffset + YDELTA);
        }
        sToast = toast;
        toast.show();
    }

    public static void cancelToast()
    {
        if(Log.V) Log.v("cancelToast() called.");
        if(sToast != null) {
            sToast.cancel();
        }
        sToast = null;
    }

}

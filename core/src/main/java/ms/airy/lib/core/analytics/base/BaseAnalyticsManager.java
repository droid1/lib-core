package ms.airy.lib.core.analytics.base;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import ms.airy.lib.core.analytics.CustomEventTracker;
import ms.airy.lib.core.analytics.ScreenViewTracker;
import ms.airy.lib.core.analytics.config.AnalyticsConfigHelper;
import ms.airy.lib.core.analytics.manager.AnalyticsManager;


// TBD:
public abstract class BaseAnalyticsManager implements AnalyticsManager, ScreenViewTracker, CustomEventTracker
{
    // Application context.
    private Context context;

//    private ScreenViewTracker screenViewTracker = null;
//    private CustomEventTracker customEventTracker = null;

    private List<ScreenViewTracker> screenViewTrackers = null;
    private List<CustomEventTracker> customEventTrackers = null;

    // "cache"
    private final boolean trackingEnabled;
    private final boolean amazonEnabled;
    private final boolean googleEnabled;


//    public BaseAnalyticsManager()
//    {
//        this(null, null);
//    }
//    public BaseAnalyticsManager(ScreenViewTracker screenViewTracker)
//    {
//        this(screenViewTracker, null);
//    }
//    public BaseAnalyticsManager(ScreenViewTracker screenViewTracker, CustomEventTracker customEventTracker)
//    {
//        this.screenViewTracker = screenViewTracker;
//        this.customEventTracker = customEventTracker;
//    }

    public BaseAnalyticsManager(Context context)
    {
        this.context = context.getApplicationContext();

        // config vars. cached.
        trackingEnabled = AnalyticsConfigHelper.getInstance(context).isAnalyticsEnabled();
        amazonEnabled = AnalyticsConfigHelper.getInstance(context).isAmazonAnalyticsEnabled();
        googleEnabled = AnalyticsConfigHelper.getInstance(context).isGoogleAnalyticsEnabled();

        // Trackers delay-initialized...
        // ....
    }

    protected final Context getContext()
    {
        return context;
    }

    @Override
    public boolean isTrackingEnabled()
    {
        return trackingEnabled;
    }

    // TBD:
    public final boolean isAmazonEnabled()
    {
        return (trackingEnabled && amazonEnabled);
    }
    public final boolean isGoogleEnabled()
    {
        return (trackingEnabled && googleEnabled);
    }


    protected final List<ScreenViewTracker> screenViewTrackers()
    {
        return screenViewTrackers;
    }
    protected final void screenViewTrackers(List<ScreenViewTracker> screenViewTrackers)
    {
        this.screenViewTrackers = screenViewTrackers;
    }
    public List<ScreenViewTracker> getScreenViewTrackers()
    {
        if(screenViewTrackers == null) {
            screenViewTrackers = new ArrayList<ScreenViewTracker>();
        }
        return screenViewTrackers;
    }

    protected final List<CustomEventTracker> customEventTrackers()
    {
        return customEventTrackers;
    }
    protected final void customEventTrackers(List<CustomEventTracker> customEventTrackers)
    {
        this.customEventTrackers = customEventTrackers;
    }
    public List<CustomEventTracker> getCustomEventTrackers()
    {
        if(customEventTrackers == null) {
            customEventTrackers = new ArrayList<CustomEventTracker>();
        }
        return customEventTrackers;
    }


    // For dependency injenction
    // TBD:

    protected void addScreenViewTracker(ScreenViewTracker screenViewTracker)
    {
        if(screenViewTrackers == null) {
            // Note: If we do not add "default" trackers (e.g., as defined in a subclass),
            //       and just create a new list, then
            //       depending on the usage, this can be potentially an issue.
            //       As a workaround, we need to call (overridden) getScreenViewTrackers()...
            // screenViewTrackers = new ArrayList<ScreenViewTracker>();
            // TBD: Is this safe???
            screenViewTrackers = getScreenViewTrackers();
        }
        screenViewTrackers.add(screenViewTracker);
    }

    protected void addCustomEventTracker(CustomEventTracker customEventTracker)
    {
        if(customEventTrackers == null) {
            // Note: If we do not add "default" trackers (e.g., as defined in a subclass),
            //       and just create a new list, then
            //       depending on the usage, this can be potentially an issue.
            //       As a workaround, we need to call (overridden) getCustomEventTrackers()...
            // customEventTrackers = new ArrayList<CustomEventTracker>();
            // TBD: Is this safe???
            customEventTrackers = getCustomEventTrackers();
        }
        customEventTrackers.add(customEventTracker);
    }



    ///////////////////////////////////////////////////////////////////////////////
    // AnalyticsManager interface

//    @Override
//    public ScreenViewTracker getScreenViewTracker()
//    {
//        return screenViewTracker;
//    }
//    public void setScreenViewTracker(ScreenViewTracker screenViewTracker)
//    {
//        this.screenViewTracker = screenViewTracker;
//    }
//
//    @Override
//    public CustomEventTracker getCustomEventTracker()
//    {
//        return customEventTracker;
//    }
//    public void setCustomEventTracker(CustomEventTracker customEventTracker)
//    {
//        this.customEventTracker = customEventTracker;
//    }



    ///////////////////////////////////////////////////////////////////////////////
    // ScreenViewTracker interface

    @Override
    public void startSession()
    {
        if(isTrackingEnabled()) {
//        if(screenViewTracker != null) {
//            screenViewTracker.startSession();
//        }
            for (ScreenViewTracker tracker : getScreenViewTrackers()) {
                tracker.startSession();
            }
        } else {
            Log.d("BaseAnalyticsManager.startSession() - tracking not enabled.");
        }
    }

    @Override
    public void endSession()
    {
        if(isTrackingEnabled()) {
//        if(screenViewTracker != null) {
//            screenViewTracker.endSession();
//        }
            for (ScreenViewTracker tracker : getScreenViewTrackers()) {
                tracker.endSession();
            }
        } else {
            Log.d("BaseAnalyticsManager.endSession() - tracking not enabled.");
        }
    }

    @Override
    public void sendScreenView()
    {
        if(isTrackingEnabled()) {
//        if(screenViewTracker != null) {
//            screenViewTracker.sendScreenView();
//        }
            for (ScreenViewTracker tracker : getScreenViewTrackers()) {
                tracker.sendScreenView();
            }
        } else {
            Log.d("BaseAnalyticsManager.sendScreenView() - tracking not enabled.");
        }
    }


    ///////////////////////////////////////////////////////////////////////////////
    // CustomEventTracker interface




    // For debugging....
    @Override
    public String toString()
    {
        return "BaseAnalyticsManager{" +
                "trackingEnabled=" + trackingEnabled +
                ", amazonEnabled=" + amazonEnabled +
                ", googleEnabled=" + googleEnabled +
                '}';
    }

}

package ms.airy.lib.core.config.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import ms.airy.lib.core.config.AbstractReadWriteConfigManager;


public class PrefsConfigManager extends AbstractReadWriteConfigManager
{
    // temporary
    private static final String SP_FILENAME_SUFFIX = ".config_prefs";
    // ...

    // TBD:
    // Singleton???
    private static PrefsConfigManager INSTANCE = null;
    public static PrefsConfigManager getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new PrefsConfigManager(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Separate Shared Preferences file.
    private final String sharedPrefsFile;
    private final SharedPreferences sharedPreferences;

    protected PrefsConfigManager(Context context)
    {
        super(context);
        sharedPrefsFile = getContext().getPackageName() + SP_FILENAME_SUFFIX;
        sharedPreferences = context.getSharedPreferences(sharedPrefsFile, Context.MODE_PRIVATE);
    }
    public String getSharedPrefsFile()
    {
        return sharedPrefsFile;
    }

    // Note that the sharedPreferences can be "shared" with other components of the app.
    //   (hence, we do not "cache" the value here...)
    public SharedPreferences getSharedPreferences()
    {
        return sharedPreferences;
    }


    // TBD:
    // This implementation does not really make sense (e.g., very inefficient)
    // Is there a better way???

    @Override
    public Object get(String key)
    {
        Object val = null;
        if(sharedPreferences.contains(key)) {
            try {
                // Check the string type first!
                val = getString(key);
            } catch (ClassCastException e1) {
                try {
                    val = getBoolean(key);
                } catch (ClassCastException e2) {
                    try {
                        val = getInt(key);
                    } catch (ClassCastException e3) {
                        try {
                            val = getLong(key);
                        } catch (ClassCastException e4) {
                            try {
                                val = getFloat(key);
                            } catch (ClassCastException e5) {
                                // ???
                                // This cannot happen since the value should be one of these types...
                                Log.e("Failed to find a value for key = " + key);
                            }
                        }
                    }
                }
            }
        } else {
            // Ignore
        }
        return val;
    }

    @Override
    public Object get(String key, Object defVal)
    {
        Object val = get(key);
        if(val != null) {
            return val;
        } else {
            return defVal;
        }
    }

    @Override
    public String getString(String key)
    {
        return sharedPreferences.getString(key, null);
    }

    @Override
    public String getString(String key, String defVal)
    {
        String val = getString(key);
        if(val != null) {
            return val;
        } else {
            return defVal;
        }
    }


    public Boolean getBoolean(String key)
    {
        if(sharedPreferences.contains(key)) {
            return sharedPreferences.getBoolean(key, false);
        } else {
            return null;
        }
    }
    public Integer getInt(String key)
    {
        if(sharedPreferences.contains(key)) {
            return sharedPreferences.getInt(key, 0);
        } else {
            return null;
        }
    }
    public Long getLong(String key)
    {
        if(sharedPreferences.contains(key)) {
            return sharedPreferences.getLong(key, 0L);
        } else {
            return null;
        }
    }
    public Float getFloat(String key)
    {
        if(sharedPreferences.contains(key)) {
            return sharedPreferences.getFloat(key, 0f);
        } else {
            return null;
        }
    }



    @Override
    public boolean set(String key, Object val)
    {
        if(val instanceof Boolean) {
            setBoolean(key, (Boolean) val);
        } else if(val instanceof Integer) {
            setInt(key, (Integer) val);
        } else if(val instanceof Long) {
            setLong(key, (Long) val);
        } else if(val instanceof Float) {
            setFloat(key, (Float) val);
        } else if(val instanceof String) {
            setString(key, (String) val);
        } else {
            // ????
            return false;
        }
        return true;
    }

    @Override
    public boolean setString(String key, String val)
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, val);
        editor.commit();
        return true;
    }

    public boolean setBoolean(String key, boolean val)
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, val);
        editor.commit();
        return true;
    }
    public boolean setInt(String key, int val)
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, val);
        editor.commit();
        return true;
    }
    public boolean setLong(String key, long val)
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(key, val);
        editor.commit();
        return true;
    }
    public boolean setFloat(String key, float val)
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putFloat(key, val);
        editor.commit();
        return true;
    }

}


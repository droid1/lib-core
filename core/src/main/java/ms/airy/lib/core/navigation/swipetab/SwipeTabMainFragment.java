package ms.airy.lib.core.navigation.swipetab;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ms.airy.lib.core.R;

/**
 * A placeholder fragment...
 */
public class SwipeTabMainFragment extends Fragment
{
    /**
     * The fragment argument representing the tab/section id.
     */
    private static final String ARG_SECTION_ID = "section_id";

    /**
     * The fragment argument representing the tab/section title.
     */
    private static final String ARG_SECTION_TITLE = "section_title";

    /**
     * Returns a new instance of this fragment for the given section id.
     */
    public static SwipeTabMainFragment newInstance(int sectionId)
    {
        return newInstance(sectionId, null);   // ???
    }
    public static SwipeTabMainFragment newInstance(int sectionId, String sectionTitle)
    {
        SwipeTabMainFragment fragment = new SwipeTabMainFragment();

        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_ID, sectionId);
        if(sectionTitle != null) {
            args.putString(ARG_SECTION_TITLE, sectionTitle);
        }
        fragment.setArguments(args);
        return fragment;
    }

    public SwipeTabMainFragment()
    {
        // TBD..
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.sample_fragment_swipe_tab_main, container, false);
        TextView textView = (TextView) rootView.findViewById(R.id.section_label);
        // temporary
        Integer sectionId = getArguments().getInt(ARG_SECTION_ID);
        if(sectionId != null) {
            textView.setText(Integer.toString(sectionId));
        } else {
            // ???
            textView.setText("0");
        }
        // temporary
        return rootView;
    }

}

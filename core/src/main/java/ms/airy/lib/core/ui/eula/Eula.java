package ms.airy.lib.core.ui.eula;

import java.io.BufferedInputStream;
import java.io.InputStream;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import ms.airy.lib.core.prefs.PrefsHelper;


public class Eula extends Activity 
{
	private final static int MENU_QUIT = 1041;

    // System/User preferences
    private PrefsHelper mPrefs;

    // Lazy initialization.
    private PrefsHelper getPrefsHelper()
    {
        if(mPrefs == null) {
            mPrefs = new PrefsHelper(this);
        }
        return mPrefs;
    }
    
    // Form controls.
    private TextView mTextEula;
    private Button mButtonAccept;
    private Button mButtonCancel;
    

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        
        // TBD:
        // Programtically create layout, buttons....
        // ...
        
//        setContentView(R.layout.eula_view);
//        
//        mTextEula = (TextView) findViewById(R.id.eula_view_text);
//        mButtonAccept = (Button) findViewById(R.id.eula_view_button_accept);
//        mButtonCancel = (Button) findViewById(R.id.eula_view_button_cancel);
//        
//        if(mTextEula != null) {
//            // TBD:
//            // Display the eula text
//            
//
//            try {
//                InputStream is = null;
//                try {
//                    //String filename = "eula_2011a";
//                    //is = getAssets().open(filename);
//                    is = getResources().openRawResource(R.raw.eula_2011a);
//                    
//                    //BufferedInputStream input =  new BufferedInputStream(is);
//                    
//                    // TBD
//                    int MAX_SIZE = 100000;  // ???
//                    int size = is.available();
//                    if(size > MAX_SIZE) {
//                        if(Log.W) Log.w("Eula file size is too big, size = " + size + " bytes. It is truncated at " + MAX_SIZE);
//                        size = MAX_SIZE;
//                    }
//                    byte[] buffer = new byte[size];
//                    is.read(buffer);
//                    
//                    // TBD:
//                    String eula = new String(buffer);
//                    if(Log.D) Log.d("EULA read: " + eula);
//                    // TBD:
//                    
//                    
//                } finally {
//                    if(is != null) {
//                        is.close();
//                    }
//                }
//            } catch(Resources.NotFoundException nfx) {
//                // ...
//                if(Log.W) Log.w("Failed to open EULA file.", nfx);
//            } catch(Exception ex) {
//                // ...
//                if(Log.W) Log.w("Error while trying to open EULA file.", ex);
//            }
//            // ....
//            
//            
//
//        } else {
//            // This cannot happen. Just bail out... ???
//            if(Log.W) Log.w( "Eula text cannot be displayed. Bailing out.");
//            // ...
//        }

        if(mButtonAccept != null) {
            mButtonAccept.setOnClickListener(new View.OnClickListener() {
                //@Override  // ???
                public void onClick(View v) {
                    if(Log.I) Log.i( "Eula.mButtonAccept Clicked.");
                    // TBD:
                    getPrefsHelper().setEulaAccepted();    // TBD: temporarily commented out.
                    boolean eulaAccepted = getPrefsHelper().isEulaAccepted();
                    if(Log.I) Log.i( "eulaAccepted = " + eulaAccepted);
                    
                    // TBD:
                    setResult(RESULT_OK);
                    finish();
                }
            });
        }
        if(mButtonCancel != null) {
            mButtonCancel.setOnClickListener(new View.OnClickListener() {
                //@Override  // ???
                public void onClick(View v) {
                    if(Log.I) Log.i( "Eula.mButtonCancel Clicked.");
                    // TBD: Really no need to do anything when Cancel is pressed.
                    // Double checking.
                    boolean eulaAccepted = getPrefsHelper().isEulaAccepted();
                    if(!eulaAccepted) { 
                        // Display message, and
                        // Close the app (including eula activity).
                        // ...
                        setResult(RESULT_CANCELED);
                        finish();
                    } else {
                        // this cannot happen.
                        if(Log.W) Log.w( "It appears that Eula has already been accepted.");
                        setResult(RESULT_OK);
                        finish();
                    }
//                    // TBD: For debugging purposes.
//                    getPrefsHelper().setEulaAccepted(false);    // TBD: temporary. 
//                    setResult(RESULT_CANCELED);
//                    finish();
//                    // TBD: For debugging purposes.
                }
            });
        }
    }
    
	
	@Override
    public void onBackPressed()
    {
        // TBD
	    // Do "cancel" here
        super.onBackPressed();
    }


    @Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
    	//return super.onCreateOptionsMenu(menu);
	    menu.add(0, MENU_QUIT, 41, "Quit")
		// .setIcon(android.R.drawable.ic_menu_close_clear_cancel)
		.setAlphabeticShortcut('q');
	    return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
	    switch (item.getItemId()) {
	    case MENU_QUIT:
	        doQuit();
	        return true;
	    }
	    return super.onOptionsItemSelected(item);
	}
 
    private void doQuit()
    {
		finish();
    }
    
}

package ms.airy.lib.core.auth.config;

import android.content.Context;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import ms.airy.lib.core.auth.common.AccountType;


// TBD:
// Is this AbstractFactory???
public class AppAuthConfigManager
{
    private static AppAuthConfigManager INSTANCE = null;
    public static AppAuthConfigManager getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new AppAuthConfigManager(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;

    // DI.
    private ApplicationAuthConfig authConfig = null;

    protected AppAuthConfigManager(Context context)
    {
        this.context = context;

        // temporary
        this.authConfig = BaseApplicationAuthConfig.getInstance(context);
        // ...
    }


    public ApplicationAuthConfig getApplicationAuthConfig()
    {
        return authConfig;
    }
    public void setApplicationAuthConfig(ApplicationAuthConfig authConfig)
    {
        this.authConfig = authConfig;
    }

}

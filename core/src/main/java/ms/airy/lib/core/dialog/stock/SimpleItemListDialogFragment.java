package ms.airy.lib.core.dialog.stock;

import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import ms.airy.lib.core.R;
import ms.airy.lib.core.dialog.ActionableAssertDialogHost;
import ms.airy.lib.core.dialog.ActionableAssertDialogListener;
import ms.airy.lib.core.dialog.ActionableChoiceDialogHost;
import ms.airy.lib.core.dialog.ActionableChoiceDialogListener;
import ms.airy.lib.core.dialog.base.BasicItemListDialogFragment;

import java.util.HashSet;
import java.util.Set;


// http://developer.android.com/guide/topics/ui/dialogs.html
public class SimpleItemListDialogFragment extends BasicItemListDialogFragment implements ActionableAssertDialogHost, ActionableChoiceDialogHost
{
    // Note:
    // by default (for appropriate ctors),
    // single choice list includes only the neutral button, and
    // multi choice list includes positve and neutral buttons.

    public static SimpleItemListDialogFragment newSingleChoiceInstance(CharSequence title, CharSequence[] itemsArray)
    {
        return newSingleChoiceInstance(title, itemsArray, "", "", null);
    }
    public static SimpleItemListDialogFragment newSingleChoiceInstance(CharSequence title, CharSequence[] itemsArray, CharSequence positiveLabel, CharSequence negativeLabel, CharSequence neutralLabel)
    {
        return newInstance(title, false, itemsArray, null, positiveLabel, negativeLabel, neutralLabel);
    }
    public static SimpleItemListDialogFragment newMultipleChoiceInstance(CharSequence title, CharSequence[] itemsArray, boolean[] selectedItems)
    {
        return newMultipleChoiceInstance(title, itemsArray, selectedItems, null, "", null);
    }
    public static SimpleItemListDialogFragment newMultipleChoiceInstance(CharSequence title, CharSequence[] itemsArray, boolean[] selectedItems, CharSequence positiveLabel, CharSequence negativeLabel, CharSequence neutralLabel)
    {
        return newInstance(title, true, itemsArray, selectedItems, positiveLabel, negativeLabel, neutralLabel);
    }

    public static SimpleItemListDialogFragment newSingleChoiceInstance(int titleResId, int itemsResId)
    {
        return newSingleChoiceInstance(titleResId, itemsResId, -1, -1, 0);
    }
    public static SimpleItemListDialogFragment newSingleChoiceInstance(int titleResId, int itemsResId, int positiveLabelResId, int negativeLabelResId, int neutralLabelResId)
    {
        return newInstance(titleResId, false, itemsResId, null, positiveLabelResId, negativeLabelResId, neutralLabelResId);
    }
    public static SimpleItemListDialogFragment newMultipleChoiceInstance(int titleResId, int itemsResId, boolean[] selectedItems)
    {
        return newMultipleChoiceInstance(titleResId, itemsResId, selectedItems, 0, -1, 0);
    }
    public static SimpleItemListDialogFragment newMultipleChoiceInstance(int titleResId, int itemsResId, boolean[] selectedItems, int positiveLabelResId, int negativeLabelResId, int neutralLabelResId)
    {
        return newInstance(titleResId, true, itemsResId, selectedItems, positiveLabelResId, negativeLabelResId, neutralLabelResId);
    }

    public static SimpleItemListDialogFragment newInstance(CharSequence title, boolean multipleChoice, CharSequence[] itemsArray, boolean[] selectedItems)
    {
        SimpleItemListDialogFragment fragment = new SimpleItemListDialogFragment();
        addArguments(fragment, title, multipleChoice, itemsArray, selectedItems);
        return fragment;
    }
    public static SimpleItemListDialogFragment newInstance(CharSequence title, boolean multipleChoice, CharSequence[] itemsArray, boolean[] selectedItems, CharSequence positiveLabel, CharSequence negativeLabel, CharSequence neutralLabel)
    {
        SimpleItemListDialogFragment fragment = newInstance(title, multipleChoice, itemsArray, selectedItems);
        addButtonLabelArguments(fragment, positiveLabel, negativeLabel, neutralLabel);
        return fragment;
    }

    public static SimpleItemListDialogFragment newInstance(int titleResId, boolean multipleChoice, int itemsResId, boolean[] selectedItems)
    {
        SimpleItemListDialogFragment fragment = new SimpleItemListDialogFragment();
        addArguments(fragment, titleResId, multipleChoice, itemsResId, selectedItems);
        return fragment;
    }
    public static SimpleItemListDialogFragment newInstance(int titleResId, boolean multipleChoice, int itemsResId, boolean[] selectedItems, int positiveLabelResId, int negativeLabelResId, int neutralLabelResId)
    {
        SimpleItemListDialogFragment fragment =  newInstance(titleResId, multipleChoice, itemsResId, selectedItems);
        addButtonLabelArguments(fragment, positiveLabelResId, negativeLabelResId, neutralLabelResId);
        return fragment;
    }

    // label==null means, use the resource. label=="" means, do not display the button.
    protected static DialogFragment addButtonLabelArguments(DialogFragment fragment, CharSequence positiveLabel, CharSequence negativeLabel, CharSequence neutralLabel)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        if(positiveLabel != null) {
            args.putCharSequence(ARG_POSITIVE_LABEL, positiveLabel);
        }
        if(negativeLabel != null) {
            args.putCharSequence(ARG_NEGATIVE_LABEL, negativeLabel);
        }
        if(neutralLabel != null) {
            args.putCharSequence(ARG_NEUTRAL_LABEL, neutralLabel);
        }
        return fragment;
    }
    // labelResId==-1 means, Do not include the button.
    protected static DialogFragment addButtonLabelArguments(DialogFragment fragment, int positiveLabelResId, int negativeLabelResId, int neutralLabelResId)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        args.putInt(ARG_POSITIVE_LABEL_RESID, positiveLabelResId);
        args.putInt(ARG_NEGATIVE_LABEL_RESID, negativeLabelResId);
        args.putInt(ARG_NEUTRAL_LABEL_RESID, neutralLabelResId);
        return fragment;
    }

    public SimpleItemListDialogFragment()
    {
    }


    ////////////////////////////////////////////////////
    // ActionableChoiceDialog interface callbacks

    private final Set<ActionableChoiceDialogListener> actionables = new HashSet<ActionableChoiceDialogListener>();
    @Override
    public DialogFragment addActionable(ActionableChoiceDialogListener actionable)
    {
        actionables.add(actionable);
        return this;
    }
    @Override
    public DialogFragment addActionable(ActionableAssertDialogListener actionable)
    {
        // ????
        return addActionable((ActionableChoiceDialogListener) actionable);
    }


    private DialogInterface.OnClickListener positiveListener =
            new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int whichButton)
                {
                    for(ActionableChoiceDialogListener a : actionables) {
                        a.onPositiveClick(SimpleItemListDialogFragment.this.getTag());
                    }
                }
            };
    private DialogInterface.OnClickListener negativeListener =
            new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int whichButton)
                {
                    for(ActionableChoiceDialogListener a : actionables) {
                        a.onNegativeClick(SimpleItemListDialogFragment.this.getTag());
                    }
                }
            };
    private DialogInterface.OnClickListener neutralListener =
            new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int whichButton)
                {
                    for(ActionableChoiceDialogListener a : actionables) {
                        a.onNeutralClick(SimpleItemListDialogFragment.this.getTag());
                    }
                }
            };
    private DialogInterface.OnMultiChoiceClickListener multiChoiceClickListener =
            new DialogInterface.OnMultiChoiceClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which, boolean isChecked)
                {
                    for(ActionableChoiceDialogListener a : actionables) {
                        a.onDialogItemClick(SimpleItemListDialogFragment.this.getTag(), which, isChecked);
                    }
                }
            };
    private DialogInterface.OnClickListener singleChoiceClickListener =
            new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    for(ActionableChoiceDialogListener a : actionables) {
                        a.onDialogItemClick(SimpleItemListDialogFragment.this.getTag(), which, true);
                    }
                }
            };


    @Override
    protected AlertDialog.Builder buildDialogBuilder()
    {
        Bundle args = getArguments();
        // boolean itemsInRes = args.containsKey(ARG_ITEMS_RESID) && (args.getInt(ARG_ITEMS_RESID) > 0);
        CharSequence[] itemsArray = args.getCharSequenceArray(ARG_ITEMS_ARRAY);
        int itemsResId = args.getInt(ARG_ITEMS_RESID);
        boolean[] selectedItems = args.getBooleanArray(ARG_SELECTED_ITEMS);

        CharSequence positiveLabel = getArguments().getCharSequence(ARG_POSITIVE_LABEL);
        int positiveLabelResId = getArguments().getInt(ARG_POSITIVE_LABEL_RESID);
        CharSequence negativeLabel = getArguments().getCharSequence(ARG_NEGATIVE_LABEL);
        int negativeLabelResId = getArguments().getInt(ARG_NEGATIVE_LABEL_RESID);
        CharSequence neutralLabel = getArguments().getCharSequence(ARG_NEUTRAL_LABEL);
        int neutralLabelResId = getArguments().getInt(ARG_NEUTRAL_LABEL_RESID);

        AlertDialog.Builder dialogBuilder = super.buildDialogBuilder();

        if(isMultipleChoice()) {
            if(itemsArray != null) {
                dialogBuilder.setMultiChoiceItems(itemsArray, selectedItems, multiChoiceClickListener);
            } else {
                if(itemsResId > 0) {
                    dialogBuilder.setMultiChoiceItems(itemsResId, selectedItems, multiChoiceClickListener);
                }
            }
        } else {
            if(itemsArray != null) {
                dialogBuilder.setItems(itemsArray, singleChoiceClickListener);
            } else {
                if(itemsResId > 0) {
                    dialogBuilder.setItems(itemsResId, singleChoiceClickListener);
                }
            }
        }

        if(positiveLabel != null) {
            if(positiveLabel.length() > 0) {
                dialogBuilder.setPositiveButton(positiveLabel, positiveListener);
            }
        } else {
            int posId = positiveLabelResId;
            if (posId == 0) {
                posId = R.string.basic_itemlist_dialog_ok;
            }
            if (posId >= 0) {
                dialogBuilder.setPositiveButton(posId, positiveListener);
            }
        }
        if(negativeLabel != null) {
            if(negativeLabel.length() > 0) {
                dialogBuilder.setNegativeButton(negativeLabel, negativeListener);
            }
        } else {
            int negId = negativeLabelResId;
            if(negId == 0) {
                negId = R.string.basic_itemlist_dialog_dismiss;
            }
            if(negId >= 0) {
                dialogBuilder.setNegativeButton(negId, negativeListener);
            }
        }
        if(neutralLabel != null) {
            if(neutralLabel.length() > 0) {
                dialogBuilder.setNeutralButton(neutralLabel, neutralListener);
            }
        } else {
            int neuId = neutralLabelResId;
            if(neuId == 0) {
                neuId = R.string.basic_itemlist_dialog_cancel;
            }
            if(neuId >= 0) {
                dialogBuilder.setNeutralButton(neuId, neutralListener);
            }
        }

        return dialogBuilder;
    }


}

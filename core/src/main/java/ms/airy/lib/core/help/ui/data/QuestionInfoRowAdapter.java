package ms.airy.lib.core.help.ui.data;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.List;

import ms.airy.lib.core.R;
import ms.airy.lib.core.common.ImageSize;
import ms.airy.lib.core.common.ItemContentLength;
import ms.airy.lib.core.common.util.ItemSizeUtil;
import ms.airy.lib.core.help.common.QuestionInfo;


public class QuestionInfoRowAdapter extends BaseAdapter implements ListAdapter
{
    private final Context context;
    private List<? extends QuestionInfo> questionInfoList = null;


    public QuestionInfoRowAdapter(Context context, List<? extends QuestionInfo> questionInfoList)
    {
        this.context = context;
        // tbd: check questionInfoList is not null!!!
        this.questionInfoList = questionInfoList;
    }


    // ???
    public QuestionInfo getQuestionInfo(long id)
    {
        // if(questionInfoList != null) {
        for(QuestionInfo m : questionInfoList) {
            if(id == m.getId()) {
                return m;
            }
        }
        // }
        return null;
    }



    @Override
    public int getCount()
    {
        return questionInfoList.size();
    }

    @Override
    public Object getItem(int position)
    {
        return questionInfoList.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return questionInfoList.get(position).getId();
    }


    // what is this???
    private static class ViewHolder {
        View firstTopLevelChild;
//        ImageView customBulletImageView;
        View mainContentView;
        TextView questionTextView;
        TextView summaryTextView;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        // ???
        View rowView = null;
        ViewHolder holder = new ViewHolder();
        if(convertView != null) {
            Log.i("convertView is being re-used...");
            rowView = convertView;
            holder = (ViewHolder) convertView.getTag();
        } else {

            LayoutInflater inflater = null;
            if(context instanceof Activity) {
                inflater = ((Activity) context).getLayoutInflater();
            } else {
                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }

            // rowView = inflater.inflate(layoutResId, parent);   // ???
            rowView = inflater.inflate(R.layout.listview_row_question_info_list_view, null);
            holder.firstTopLevelChild = rowView.findViewById(R.id.questioninfo_question_info);
//            holder.customBulletImageView = (ImageView) rowView.findViewById(R.id.questioninfo_main_logo);

            holder.mainContentView = rowView.findViewById(R.id.questioninfo_main_content);
            holder.questionTextView = (TextView) rowView.findViewById(R.id.questioninfo_question);
            // holder.summaryTextView = (TextView) rowView.findViewById(R.id.questioninfo_summary);
            // ...

            rowView.setTag(holder);
        }


        // TBD:
        String contentLength = ItemContentLength.SHORT;   // ???
        if(holder.firstTopLevelChild != null) {
            contentLength = (String) holder.firstTopLevelChild.getTag();
        }
        String flagSize = null;
        if(ItemContentLength.isValid(contentLength)) {
            flagSize = ItemSizeUtil.getFlagSizeViaDirectMapping(contentLength);
            // flagSize = ItemSizeUtil.getFlagSizeViaListDefaultMapping(contentLength);
        } else {
            // ???
            Log.i(">>>>>>>>>>>>>>>>>>> tag (for contentLength/flagSize) not found!");
            contentLength = ItemContentLength.SHORT;
            flagSize = ImageSize.SIZE_SMALL;
        }
        if(Log.D) Log.d(">>>>>>>>>>>>>>>>>>> contentLength = " + contentLength + "; flagSize = " + flagSize);


        QuestionInfo questionInfo = questionInfoList.get(position);
        long itemId = questionInfo.getId();

//        if(holder.customBulletImageView != null) {
//
//            // TBD:
//            holder.customBulletImageView.setVisibility(View.INVISIBLE);
//            // ...
//
////            int customBulletResId = questionInfo.getImageResId(flagSize);
////            if (customBulletResId == 0) {
////                // Use default image ????
////                customBulletResId = QuestionInfoLogoUtil.getDefaultQuestionInfoLogoResourceId(flagSize);
////            }
////            if (customBulletResId > 0) {
////                holder.customBulletImageView.setVisibility(View.VISIBLE);
////                holder.customBulletImageView.setImageResource(customBulletResId);
////            } else {
////                // ???
////                holder.customBulletImageView.setVisibility(View.INVISIBLE);
////            }
//        }

        if(holder.mainContentView != null) {
            String questionStr = questionInfo.getQuestion(context);
            if(holder.questionTextView != null) {
                String questionText = "";
                if (questionStr != null) {
                    questionText = (position + 1) + ". " + questionStr;
                } else {
                    // ???
                    questionText = (position + 1) + ". ";
                }
                holder.questionTextView.setText(questionText);
            }
//            String summaryStr = questionInfo.getSummary(context);
//            if(holder.summaryTextView != null) {
//                if (summaryStr != null) {
//                    holder.summaryTextView.setText(summaryStr);
//                } else {
//                    // ???
//                    holder.summaryTextView.setText("");
//                }
//            }
        }

        return rowView;
    }

}

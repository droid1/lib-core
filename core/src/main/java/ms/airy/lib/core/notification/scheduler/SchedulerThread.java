package ms.airy.lib.core.notification.scheduler;

import android.app.Notification;
import android.content.Context;
import android.os.HandlerThread;
import android.os.Looper;

import ms.airy.lib.core.notification.NotificationProcessor;
import ms.airy.lib.core.notification.common.NotificationAndTimeStruct;
import ms.airy.lib.core.notification.handler.NotificationHandler;

import java.util.HashSet;
import java.util.Set;


/**
*/
public class SchedulerThread extends HandlerThread
{
    // TBD:
    // Is it safe to keep the reference to the handler?
    private NotificationHandler notificationHandler = null;

    // ???
    // Keep notifications here and schedule them at thread start time.
    private final Set<NotificationAndTimeStruct> notificationSet = new HashSet<NotificationAndTimeStruct>();

    private Context context;
    public SchedulerThread(Context context, String name)
    {
        super(name);
        this.context = context.getApplicationContext();
    }

    // TBD:
    // Call this before start() ???
    // TBD: Allow scheduling multiple notifications???
    public void scheduleNotification(Notification notification, long notificationTime)
    {
        notificationSet.add(new NotificationAndTimeStruct(notification, notificationTime));
    }
    public void scheduleNotification(int notificationId, Notification notification, long notificationTime)
    {
        notificationSet.add(new NotificationAndTimeStruct(notificationId, notification, notificationTime));
    }
    public void scheduleNotifications(Set<NotificationAndTimeStruct> notificationTimePairs)
    {
        // TBD:
        // Enforce the rule: Set the same notificationId for all notificationTimePairs???
        notificationSet.addAll(notificationTimePairs);
    }

    @Override
    public synchronized void start()
    {
        super.start();

        // TBD:
        // Move this at the beginning of run() ???
        Looper looper = this.getLooper();
        if(looper != null) {
            notificationHandler = new NotificationHandler(looper);
            long now = System.currentTimeMillis();

            for(NotificationAndTimeStruct pair : notificationSet) {
                int notiId = pair.getNotificationId();
                Notification notification = pair.getNotification();
                long notificationTime = pair.getScheduledTime();
                long delay = notificationTime - now;
                notificationHandler.scheduleDelayedNotification(notiId, notification, delay);
            }

            notificationHandler.addNotificationEventCallback(NotificationProcessor.getInstance(context));

        } else {
            // ????
            Log.w("Looper is null. Cannot proceed.");
        }
    }

    @Override
    public void run()
    {
        if(notificationHandler == null) {
            // ???
            Log.w("notificationHandler is null.");
            // return;   // ???  --> It seems like we cannot return without calling super.run().
        }

        super.run();
    }

}

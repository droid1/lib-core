package ms.airy.lib.core.startup.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ms.airy.lib.core.startup.StartupManager;
import ms.airy.lib.core.startup.job.StartupJob;
import ms.airy.lib.core.startup.job.stock.PrimaryAccountJob;
import ms.airy.lib.core.startup.registry.BaseStartupJobRegistry;
import ms.airy.lib.core.startup.registry.StartupJobRegistry;
import ms.airy.lib.core.util.IdUtil;
import ms.airy.lib.core.app.VersionManager;


// TBD:
// To be included in the app's launcher activity.
// This fragment checks, for example, if this is the first time the user has opened the app, etc..
// ...
public class BaseStartupCheckFragment extends Fragment
{
    // int
    protected static final String ARG_VERSION_CODE = "base_startup_version_code";
    // ...



    public BaseStartupCheckFragment()
    {
    }


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // tbd
        setRetainInstance(true);
        // ...
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        return null;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        // ...

    }


    @Override
    public void onStart()
    {
        super.onStart();

        // TBD:
        // Note that onStart() will be called multiple times during the lifetime of the launcher activiey.
        // isFirstStartup() should return true only for the first time....
        // ????

        boolean isFirstStartup = StartupManager.getInstance(getActivity()).isFirstStartup();
        if(isFirstStartup) {
            // Do post-install task
            // ..
        }

        // tbd
//        // get this from the arg???
//        int versionCode = VersionManager.getInstance(getActivity()).getVersionCode();
//        int versionStartupCounter = StartupManager.getInstance(getActivity()).getStartupCounter(versionCode);
        int versionStartupCounter = StartupManager.getInstance(getActivity()).getStartupCounter();
        if(versionStartupCounter == 0) {
            // Do version upgrade task (using AsyncTask)
            // ....
        }


        // tbd
        // general startup tasks...

        // temporary
        StartupManager startupManager = StartupManager.getInstance(getActivity());
        StartupJobRegistry registry = new BaseStartupJobRegistry();

        long taskId = IdUtil.generateRandomId05();
        StartupJob job = new PrimaryAccountJob(getActivity(), taskId);
        registry.addJob(job);

        startupManager.initialize(registry);
        startupManager.doStartupProcessing(getActivity());
        // ....



        // TBD:
        // update the lastStarupTime
        // increase the versionStartupCounter..
        // etc...

    }

    @Override
    public void onResume()
    {
        super.onResume();
    }

    @Override
    public void onPause()
    {
        super.onPause();
    }

    @Override
    public void onStop()
    {
        super.onStop();
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
    }


}

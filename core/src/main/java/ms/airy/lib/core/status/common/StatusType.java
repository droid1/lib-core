package ms.airy.lib.core.status.common;


/**
 */
public class StatusType
{
    // TBD:
    public static final int TYPE_ACTIVITY = (0x1 << 0);
    // ....
    public static final int TYPE_CUSTOM1 = (0x1 << 10);
    public static final int TYPE_CUSTOM2 = (0x1 << 11);
    public static final int TYPE_CUSTOM3 = (0x1 << 12);
    // ....

    private StatusType() {}
}

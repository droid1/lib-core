package ms.airy.lib.core.auth.common;

import org.minijson.JsonCompatible;
import org.minijson.JsonSerializable;
import org.minijson.builder.JsonBuilder;
import org.minijson.builder.JsonBuilderException;
import org.minijson.builder.impl.SimpleJsonBuilder;

import java.io.IOException;
import java.io.Serializable;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import ms.airy.lib.core.auth.helper.AccountTypeUtil;
import ms.airy.lib.core.user.UserManager;
import ms.airy.lib.core.user.prefs.UserDevicePrefsStoreManager;


// Note: UserAccount will "uniquely" identify a user/device/player on the server.
// See the comments in UserDevicePrefsStoreManager ....
// UserAccount comprises two parts: userDeviceId and the "account".
// This object may be passed to the app (e.g., for the purposes of identifying players in the multi-player game, etc.)
// The server/app may decide to use one or both of these parts (or, neither).
// --> Hence (for security reasons) UserAccount is merely an "id" not an "auth" token....
public final class UserAccount implements Serializable, JsonCompatible, JsonSerializable
{
    private final String userDeviceId;
    private final int accountType;
    private final String accountName;

    public UserAccount(String userDeviceId)
    {
        this(userDeviceId, AccountType.TYPE_NONE, null);
    }
    public UserAccount(String userDeviceId, int accountType, String accountName)
    {
        // userDeviceId should always be valid.
        // tbd: how to ensure that???
        this.userDeviceId = userDeviceId;
        this.accountType = accountType;
        this.accountName = accountName;
    }


    public String getUserDeviceId()
    {
        return userDeviceId;
    }

    public int getAccountType()
    {
        return accountType;
    }

    public String getAccountName()
    {
        return accountName;
    }

    public boolean isAccountValid()
    {
        return AccountTypeUtil.isValid(this.accountType);
    }


    // Note that this is public, to be used for serialization/de-serialization.
    // This "contract" should be honored for both server and client sides...
    public static String getUniqueKey(String id)
    {
        // ???
        return getUniqueKey(id, AccountType.TYPE_NONE, null);
    }
    public static String getUniqueKey(String id, int type, String name)
    {
        StringBuilder sb = new StringBuilder();
        // id should always be valid....
        sb.append(id).append(":");
        // if(AccountTypeUtil.isValid(type)) {
            sb.append(type);  // Type can be 0 == TYPE_NONE
        // }
        sb.append(":");
        if(name != null) {
            sb.append(name);
        }
        return sb.toString();
    }


    // "Serialization"
    @Override
    public String toString()
    {
        return getUniqueKey(this.userDeviceId, this.accountType, this.accountName);
    }

    // "Deserialization"
    public static UserAccount create(String str)
    {
        // TBD: validate arg?
        String[] arr = str.split(":", 3);
        // assert arr != null && arr.length == 3 ????
        String id = arr[0];
        int type = Integer.parseInt(arr[1]);
        String name = arr[2];
        return new UserAccount(id, type, name);
    }



    //////////////////////////////////////////////////////////
    // JsonCompatible interface

    @Override
    public Object toJsonStructure() throws JsonBuilderException
    {
        return toJsonStructure(1);
    }

    @Override
    public Object toJsonStructure(int depth) throws JsonBuilderException
    {
        // ???
        // This throws java.lang.NoClassDefFoundError: java.beans.Introspector ???
//        JsonStructureBuilder jsonStructureBuilder = new SimpleJsonBuilder();
//        Object obj = jsonStructureBuilder.buildJsonStructure(this, depth);
//        return obj;

        // Independent of depth (always 1).
        Map<String,String> structure = new HashMap<String, String>();
        structure.put("id", this.userDeviceId);
        // if(AccountTypeUtil.isValid(this.accountType)) {   // ????
            structure.put("type", AccountTypeUtil.getTypeName(this.accountType));
        // }
        if(this.accountName != null) {  // ???
            structure.put("name", this.accountName);
        }
        return structure;
    }


    //////////////////////////////////////////////////////////
    // JsonSerializable interface

    @Override
    public String toJsonString() throws JsonBuilderException
    {
        // ???
        JsonBuilder jsonBuilder = new SimpleJsonBuilder();
        // ???
        // This throws java.lang.NoClassDefFoundError: java.beans.Introspector ???
        // It would not have worked anyway.....
//        String json = jsonBuilder.build(this);
        // ...
        // We need to specify how to serialize this object.
        String json = jsonBuilder.build(toJsonStructure());
        return json;
    }

    @Override
    public void writeJsonString(Writer writer) throws IOException, JsonBuilderException
    {
        writer.write(toJsonString());
    }

}

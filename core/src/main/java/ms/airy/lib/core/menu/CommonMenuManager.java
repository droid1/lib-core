package ms.airy.lib.core.menu;

import android.content.Context;


// TBD:
// For including common menu items across different activities...
public final class CommonMenuManager
{
    private static CommonMenuManager INSTANCE = null;
    public static CommonMenuManager getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new CommonMenuManager(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;


    private CommonMenuManager(Context context)
    {
        this.context = context;
    }

    // TBD

    // ....


}

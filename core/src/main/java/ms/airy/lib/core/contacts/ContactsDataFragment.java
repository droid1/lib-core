package ms.airy.lib.core.contacts;

import android.app.Fragment;
import android.app.LoaderManager;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.SimpleCursorAdapter;

import ms.airy.lib.core.contacts.loader.DefaultContactsDataCursorLoaderCallbacks;


// TBD:
// ....
public class ContactsDataFragment extends Fragment
{
    // id == 0
    private CursorAdapter baseContactsCursorAdapter = null;
    // id == 1
    private CursorAdapter emailContactsCursorAdapter = null;

    public ContactsDataFragment()
    {
    }


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // Enable the menu
        setHasOptionsMenu(true);

        // tbd
        setRetainInstance(true);
        // ...
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        return null;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);


        // ????

        String select = "((" + ContactsContract.Contacts.DISPLAY_NAME + " NOTNULL) AND ("
        + ContactsContract.Contacts.HAS_PHONE_NUMBER + "=1) AND ("
        + ContactsContract.Contacts.DISPLAY_NAME + " != '' ))";

        baseContactsCursorAdapter = new SimpleCursorAdapter(getActivity(),
                android.R.layout.simple_list_item_2, null,
                new String[] { ContactsContract.Contacts.DISPLAY_NAME_PRIMARY,
                        ContactsContract.Contacts.CONTACT_STATUS },
                new int[] { android.R.id.text1, android.R.id.text2 }, 0);

//        setListAdapter(baseContactsCursorAdapter);

        LoaderManager.LoaderCallbacks<Cursor> loaderCallbacks = new DefaultContactsDataCursorLoaderCallbacks(getActivity(), baseContactsCursorAdapter, select);
        getLoaderManager().initLoader(0, null, loaderCallbacks);





    }


    @Override
    public void onStart()
    {
        super.onStart();
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }

    @Override
    public void onPause()
    {
        super.onPause();
    }

    @Override
    public void onStop()
    {
        super.onStop();
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {


        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {


        return super.onOptionsItemSelected(item);
    }

}

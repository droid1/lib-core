package ms.airy.lib.core.config.client;


public interface OnlineConfigClient
{
//    String getProperty(String key);
//    String getProperty(String key, String defValue);
//    void setProperty(String key, String value);
    Object getProperty(String key);
    boolean setProperty(String key, Object value);
}

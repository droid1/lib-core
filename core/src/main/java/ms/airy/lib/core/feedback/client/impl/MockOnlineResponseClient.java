package ms.airy.lib.core.feedback.client.impl;

import java.util.List;

import ms.airy.lib.core.feedback.client.OnlineResponseClient;
import ms.airy.lib.core.feedback.common.ResponseEntry;


public class MockOnlineResponseClient implements OnlineResponseClient
{

    @Override
    public ResponseEntry getResponse(long responseId)
    {
        if(Log.I) Log.i("MockOnlineResponseClient.getResponse(). responseId = " + responseId);

        return null;
    }

    @Override
    public List<Long> getNewResponses()
    {
        if(Log.I) Log.i("MockOnlineResponseClient.getNewResponses().");

        return null;
    }

}

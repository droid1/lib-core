package ms.airy.lib.core.cron;

import java.util.Calendar;
import java.util.TimeZone;


/**
 */
public final class CronTimeUtil
{
    private CronTimeUtil() {}


    // Note:
    // If we need to use timezone at all, it should be UTC. For now...
    // ....

    // m: 1-12
    public static boolean isValidDate(int y, int m, int d)
    {
        try {
            Calendar c = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
            c.setLenient(false);
            c.set(y, m-1, d);   // 0 for January
            c.getTime();        // To trigger the exception if the date is invalid...
        } catch(IllegalArgumentException e) {
            return false;
        }
        return true;
    }

    // m: 1-12
    // day of week: 0-6
    // returns -1 if isValidDate(y,m,d)==false;
    public static int getDayOfWeek(int y, int m, int d)
    {
        int dayOfWeek = -1;
        try {
            Calendar c = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
            c.setLenient(false);
            c.set(y, m-1, d);   // 0 for January
            dayOfWeek = c.get(Calendar.DAY_OF_WEEK);   // Calendar.SUNDAY==0
        } catch(IllegalArgumentException e) {
            // ignore
        }
        return dayOfWeek;
    }

    // m: 1-12
    public static long getTime(int y, int m, int d, int hr, int mn)
    {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        // cal.clear();    // ???
        cal.set(y, m-1, d, hr, mn);
        long time = cal.getTimeInMillis();
        return time;
    }

}

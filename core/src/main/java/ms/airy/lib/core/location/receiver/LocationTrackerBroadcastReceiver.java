package ms.airy.lib.core.location.receiver;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;


// TBD:
public class LocationTrackerBroadcastReceiver extends WakefulBroadcastReceiver
{
    @Override
    public void onReceive(Context context, Intent intent)
    {
        // TBD:
        // Use GcmReceiverShards ????
        // For now, all handling is delegated to the Service...
        // ....

        // Explicitly specify the service.
        ComponentName comp = new ComponentName(context.getPackageName(),
                BaseLocationTrackerIntentService.class.getName());
        // Start the service, keeping the device awake while it is launching.
        startWakefulService(context, (intent.setComponent(comp)));
        setResultCode(Activity.RESULT_OK);
    }
}

package ms.airy.lib.core.startup.job;

import ms.airy.lib.core.startup.job.condition.TaskCondition;
import ms.airy.lib.core.startup.task.StartupTask;


public class BaseForegroundStartupJob extends BaseStartupJob implements ForegroundStartupJob
{

    public BaseForegroundStartupJob(TaskCondition condition, StartupTask task)
    {
        super(condition, task);
    }
    public BaseForegroundStartupJob(TaskCondition condition, StartupTask task, int priority)
    {
        super(condition, task, priority);
    }
    public BaseForegroundStartupJob(TaskCondition condition, StartupTask task, int priority, boolean backgroundJob)
    {
        super(condition, task, priority, backgroundJob);
    }



}

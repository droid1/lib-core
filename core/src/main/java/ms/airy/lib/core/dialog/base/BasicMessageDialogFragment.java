package ms.airy.lib.core.dialog.base;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;


// http://developer.android.com/guide/topics/ui/dialogs.html
public class BasicMessageDialogFragment extends BaseDialogFragment
{
    private static final String ARG_MESSAGE = "msssage_dialog_message";
    private static final String ARG_MESSAGE_RESID = "msssage_dialog_message_resid";


    // Factory methods.
    public static BasicMessageDialogFragment newInstance(CharSequence title, CharSequence message)
    {
        BasicMessageDialogFragment fragment = new BasicMessageDialogFragment();
        addArguments(fragment, title, message);
        return fragment;
    }
    public static BasicMessageDialogFragment newInstance(int titleResId, int messageResId)
    {
        BasicMessageDialogFragment fragment = new BasicMessageDialogFragment();
        addArguments(fragment, titleResId, messageResId);
        return fragment;
    }
    public BasicMessageDialogFragment()
    {
    }

    protected static DialogFragment addArguments(DialogFragment fragment, CharSequence title, CharSequence message)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        if(title != null && title.length() > 0) {
            args.putCharSequence(ARG_TITLE, title);
        }
        if(message != null && message.length() > 0) {
            args.putCharSequence(ARG_MESSAGE, message);
        }
        return fragment;
    }
    protected static DialogFragment addArguments(DialogFragment fragment, int titleResId, int messageResId)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        if(titleResId > 0) {  // ???
            args.putInt(ARG_TITLE_RESID, titleResId);
        }
        if(messageResId > 0) {  // ???
            args.putInt(ARG_MESSAGE_RESID, messageResId);
        }
        return fragment;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        AlertDialog.Builder dialogBuilder = buildDialogBuilder();
        return dialogBuilder.create();
    }

    @Override
    protected AlertDialog.Builder buildDialogBuilder()
    {
        CharSequence title = getArguments().getCharSequence(ARG_TITLE);
        int titleResId = getArguments().getInt(ARG_TITLE_RESID);
        CharSequence message = getArguments().getCharSequence(ARG_MESSAGE);
        int messageResId = getArguments().getInt(ARG_MESSAGE_RESID);

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());

        dialogBuilder.setIcon(android.R.drawable.alert_dark_frame);

        if(title != null && title.length() > 0) {
            dialogBuilder.setTitle(title);
        } else {
            if (titleResId > 0) {  // ???
                dialogBuilder.setTitle(titleResId);
            }
        }
        if(message != null && message.length() > 0) {
            dialogBuilder.setMessage(message);
        } else {
            if (messageResId > 0) {  // ???
                dialogBuilder.setMessage(messageResId);
            }
        }

        // No buttons.

        return dialogBuilder;
    }


}

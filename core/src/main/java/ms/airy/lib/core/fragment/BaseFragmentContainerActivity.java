package ms.airy.lib.core.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;



public abstract class BaseFragmentContainerActivity extends Activity implements FragmentContainer
{
    private final Set<Fragment> globalQueue = new HashSet<Fragment>();
    private final Map<String, Set<Fragment>> channelQueue = new HashMap<String, Set<Fragment>>();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        // TBD
        //setContentView(R.layout.activity_sample_fragment_container);
	/*
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new BaseFragmentContainerFragment())
                    .commit();
        }
	*/
    }


    protected Set<Fragment> getGlobalQueue()
    {
        return globalQueue;
    }

    protected Map<String, Set<Fragment>> getChannelQueue()
    {
        return channelQueue;
    }


    @Override
    public boolean registerFragment(Fragment fragment, String channel)
    {
        if(Log.D) Log.d("registerFragment() fragment = " + fragment + "; channel = " + channel);

        if(channel == null || channel.isEmpty()) {
            globalQueue.add(fragment);
        } else {
            Set<Fragment> set = channelQueue.get(channel);
            if(set == null) {
                set = new HashSet<Fragment>();
                channelQueue.put(channel, set);
            }
            set.add(fragment);
        }
        return true;
    }

    @Override
    public boolean unregisterFragment(Fragment fragment)
    {
        if(Log.D) Log.d("unregisterFragment() fragment = " + fragment);

        globalQueue.remove(fragment);
        for(String channel : channelQueue.keySet()) {
            Set<Fragment> set = channelQueue.get(channel);
            if(set != null) {
                set.remove(fragment);
            }
        }
        return true;
    }

    @Override
    public boolean processFragmentMessages(Fragment sender, String channel, List<FragmentMessage> messages)
    {
        if(Log.D) {
            Log.d("processFragmentMessages() sender = " + sender + "; channel = " + channel);
            if(messages != null && !messages.isEmpty()) {
                for(FragmentMessage fm : messages) {
                    Log.d("fm = " + fm);
                }
            }
        }

        // TBD:
        // [1] Process whatever is needed in tha activity.
        // ...

        // Then:
        // [2] Call all ActiveFragments.
        boolean suc = relayFragmentMessages(sender, channel, messages);

        // temporary
        return false;
    }

    protected boolean relayFragmentMessages(Fragment sender, String channel, List<FragmentMessage> messages)
    {
        if(channel == null || channel.isEmpty()) {
            for(Fragment f : globalQueue) {
                if(f instanceof ActiveFragment) {
                    ((ActiveFragment) f).processContainerMessages((FragmentContainer) this, channel, messages);
                } else {
                    // ignore...
                }
            }
        } else {
            Set<Fragment> set = channelQueue.get(channel);
            if(set != null && !set.isEmpty()) {
                for(Fragment f : set) {
                    if(f instanceof ActiveFragment) {
                        ((ActiveFragment) f).processContainerMessages((FragmentContainer) this, channel, messages);
                    } else {
                        // ignore...
                    }
                }
            }
        }

        // temporary
        return false;
    }



    /**
     * A placeholder fragment containing a simple view.
     */
    public static class BaseFragmentContainerFragment extends Fragment
    {

        public BaseFragmentContainerFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
	    //            View rootView = inflater.inflate(R.layout.fragment_sample_fragment_container, container, false);
	    //            return rootView;
	    return null;
        }
    }
}

package ms.airy.lib.core.common;

import java.util.HashSet;
import java.util.Set;


public final class ItemContentLength
{
    public static final String TINY = "tiny";
    public static final String SHORT = "short";
    public static final String MEDIUM = "medium";
    public static final String LONG = "long";
    public static final String EXTRA = "extra";

    private ItemContentLength() {}

    // for efficiency...
    private static final Set<String> lengthMap = new HashSet<String>();
    static {
        lengthMap.add(TINY);
        lengthMap.add(SHORT);
        lengthMap.add(MEDIUM);
        lengthMap.add(LONG);
        lengthMap.add(EXTRA);
    }

    public static boolean isValid(String itemContentLength)
    {
        return lengthMap.contains(itemContentLength);
    }

}

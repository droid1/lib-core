package ms.airy.lib.core.cache;

import java.util.HashMap;
import java.util.Map;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;

import ms.airy.lib.core.helper.NotificationHelper;
import ms.airy.lib.core.helper.NotificationStruct;
import ms.airy.lib.core.toast.ToastManager;

// "MemCache" service.
// Currently, it's only an imitation.
// Cannot really be used as a generic cache service
//     unless you know you have only a finite number of objects to be cached.
// See the comments below.
public class MemCacheService extends Service
{
    // tbd... Move these to CommonConstants or other AeryXXX class.
    public static final String MEMCACHE_SERVICE = "AeryMobile.lib.core.cache.MemCacheService";    // ?????
    private static final int MEMCACHE_SERVICE_NOTIFICATION = 111;
    
    // temporary
    private static final String CACHE_KEY_VALUE = "value";
    private static final String CACHE_KEY_EXPIRATION = "expiration";
    
    
    private NotificationHelper mNotificationHelper;
    private NotificationStruct mServiceNotification;

    // TBD: Use "soft references" or "weak references" ????
    // { key => { 'value' => value, 'expiration' => expiration } }
    private Map<String, Map<String, Object>> mCache = null;
    
    
    // TBD: How to check if a service is running or not??????
    // temporary
    // Need to save this to a persistent storage????
    private boolean isRunning = false;
    // temporary
    
    
    // temporary
    public boolean isRunning()
    {
        return isRunning;
    }
    
    
    private void initCache()
    {
        initCache(false);
    }
    private void initCache(boolean clean)
    {
        if(mCache == null || clean) {
            mCache = new HashMap<String, Map<String, Object>>();            
        }
    }
    
    
    // TBD:
    // [1] Need to implement a method
    //       which runs on a regular interval
    //       and 
    //       cleans up expired objects...
    // [2] Need to make the max size of mCache finite.
    //       Otherwise, this may grow too large.    --> This is very important.
    // [2a] Then, use the Least used object algorithm
    //       to recycle through the available "slots"....

    
    public void putObject(String key, Object value)
    {
        putObject(key, value, 0L);
    }
    
    // ttl in milliseconds..
    // ttl==0 or ttl==-1 means no expiration....
    public void putObject(String key, Object value, long ttl)
    {
        if(Log.I) Log.i("putObject() called with key = " + key + "; value = " + value + "; ttl = " + ttl);
        
        initCache();            

        // TBD: Check first if the key/obj already exists???
        // If so, .... do what ?????
        
        Map<String, Object> obj = new HashMap<String, Object>(2);
        obj.put(CACHE_KEY_VALUE, value);
        if(ttl > 0L) {
            long now = System.currentTimeMillis();
            Long expiration = new Long(now + ttl);            
            obj.put(CACHE_KEY_EXPIRATION, expiration);
        }
        // else no expiration
        
        mCache.put(key, obj);
    }
    
    public void removeObject(String key)
    {
        if(Log.I) Log.i("removeObject() called with key = " + key);

        initCache();            
        mCache.remove(key);        
    }

    public Object getObject(String key)
    {
        if(Log.I) Log.i("getObject() called with key = " + key);

        initCache();            
        if(mCache.containsKey(key)) {
            Map<String, Object> obj = mCache.get(key);
            Object val = obj.get(CACHE_KEY_VALUE);
            Long expiration = null;
            if(obj.containsKey(CACHE_KEY_EXPIRATION)) {
                expiration = (Long) obj.get(CACHE_KEY_EXPIRATION);
                if(expiration != null) {
                    long now = System.currentTimeMillis();
                    if(expiration.longValue() > now) {  // expired
                        removeObject(key);
                        return null;
                    }
                }
            }
            return val;
        } else {
            return null;
        }
    }
    

    
    
    
    // ?????????????
    private boolean isMyServiceRunning()
    {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (MemCacheService.class.getCanonicalName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
    // ?????????????

    
    @Override
    public void onCreate()
    {
        if(Log.D) Log.d("onCreate() called.");
        super.onCreate();
        
        initCache();            

        // Initialize the Notification Helper member var.
        mNotificationHelper = new NotificationHelper(this);
        mServiceNotification = new NotificationStruct(MEMCACHE_SERVICE_NOTIFICATION);
        
        mServiceNotification.setTickerText("cache service started");
        mServiceNotification.setContentTitle("Cache Service");
        mServiceNotification.setContentText("MemCache service running...");
        //mServiceNotification.setContentAction(Intent.ACTION_VIEW);  // ????
        
        
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        if(Log.D) Log.d("onStartCommand() called. flags = " + flags + "; startId = " + startId);
        super.onStartCommand(intent, flags, startId);

        // ...
        
        
        
        // temporary
        isRunning = true;
        // temporary
        
        
        
        // Debugging....
        CharSequence text = "onStartCommand() called";
        int duration = Toast.LENGTH_LONG;
        ToastManager.showToast(this, text, duration);

        
        // Notification.
        mNotificationHelper.setNotification(mServiceNotification);
        
        return START_REDELIVER_INTENT;
    }

    @Override
    public void onDestroy()
    {
        if(Log.D) Log.d("onDestory() called.");

        
        // temporary
        isRunning = false;
        // temporary

        
        // Notification. ?????
        //mNotificationHelper.setNotification(mServiceNotification);

        super.onDestroy();

    }


    @Override
    public IBinder onBind(Intent arg0)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onRebind(Intent intent)
    {
        // TODO Auto-generated method stub
        super.onRebind(intent);
    }

    @Override
    public boolean onUnbind(Intent intent)
    {
        // TODO Auto-generated method stub
        return super.onUnbind(intent);
    }


}

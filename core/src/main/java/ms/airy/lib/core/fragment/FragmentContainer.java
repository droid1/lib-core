package ms.airy.lib.core.fragment;

import android.app.Fragment;

import java.util.List;

/**
 * Can be implemented by Activity.
 */
public interface FragmentContainer
{
    // boolean registerFragment(Fragment fragment);
    boolean registerFragment(Fragment fragment, String channel);   // channel==null means "global" channel.
    // boolean registerFragment(Fragment fragment, List<String> channels);
    boolean unregisterFragment(Fragment fragment);
    // boolean unregisterFragment(Fragment fragment, boolean includeAllChannels);
    // boolean unregisterFragment(Fragment fragment, String channel);

    boolean processFragmentMessages(Fragment sender, String channel, List<FragmentMessage> messages);
    // boolean processFragmentMessages(Fragment sender, List<String> channels, List<String> messages);

}

package ms.airy.lib.core.donate.prefs;

import android.content.Context;
import android.content.SharedPreferences;


// TBD:
// This is not about "user preferences".
// It is about SharedPreferences storage.
// It stores the app-related global user settings.
public final class DonatePrefsStoreManager
{
    // temporary
    private static final String SP_FILENAME_SUFFIX = ".donate_prefs";
//    private static final String KEY_LAST_STARTUP_TIME = "last.donate.time";
    // ...

    private static DonatePrefsStoreManager INSTANCE = null;
    public static DonatePrefsStoreManager getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new DonatePrefsStoreManager(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;

    // Separate Shared Preferences file.
    private final String sharedPrefsFile;
    private final SharedPreferences sharedPreferences;

    // "Cache"
    private Long lastDonateTime = null;


    private DonatePrefsStoreManager(Context context)
    {
        this.context = context;
        sharedPrefsFile = context.getPackageName() + SP_FILENAME_SUFFIX;
        sharedPreferences = context.getSharedPreferences(sharedPrefsFile, Context.MODE_PRIVATE);
    }
    public String getSharedPrefsFile()
    {
        return sharedPrefsFile;
    }


//    public long getLastDonateTime()
//    {
//        if(this.lastDonateTime == null) {
//            long lastDonateTime = sharedPreferences.getLong(KEY_LAST_STARTUP_TIME, 0L);
//            if (ms.airy.lib.core.donate.prefs.Log.D) ms.airy.lib.core.donate.prefs.Log.d("DonatePrefsStoreManager: lastDonateTime = " + lastDonateTime);
//            this.lastDonateTime = lastDonateTime;
//        }
//        return this.lastDonateTime;
//    }

//    public void setLastDonateTime(long lastDonateTime)
//    {
//        setLastDonateTime(lastDonateTime, false);
//    }
//    public void setLastDonateTime(long lastDonateTime, boolean force)
//    {
//        if(force || this.lastDonateTime == null || this.lastDonateTime != lastDonateTime) {
//            this.lastDonateTime = lastDonateTime;
//            SharedPreferences.Editor editor = sharedPreferences.edit();
//            editor.putLong(KEY_LAST_STARTUP_TIME, lastDonateTime);
//            editor.commit();
//        }
//    }



    // For debugging/testing purposes
    protected void removeAllPrefs()
    {
        Log.w("======== DonatePrefsStoreManager.removeAllPrefs()");

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }


}

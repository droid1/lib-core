package ms.airy.lib.core.popup.analytics;

import android.content.Context;


// TBD:
public final class PopupAnalyticsManager
{
    private static PopupAnalyticsManager INSTANCE = null;
    public static PopupAnalyticsManager getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new PopupAnalyticsManager(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;


    private PopupAnalyticsManager(Context context)
    {
        this.context = context;
    }





}

package ms.airy.lib.core.dialog.stock;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

import ms.airy.lib.core.R;
import ms.airy.lib.core.dialog.ActionableNoticeDialogListener;
import ms.airy.lib.core.dialog.base.BasicMessageDialogFragment;


// http://developer.android.com/guide/topics/ui/dialogs.html
public class ActionableConfirmDialogFragment extends BasicMessageDialogFragment
{
    //    private ActionableNoticeDialog mActivity = null;

    // Factory methods.
    public static ActionableConfirmDialogFragment newInstance(CharSequence title, CharSequence message)
    {
        ActionableConfirmDialogFragment fragment = new ActionableConfirmDialogFragment();
        addArguments(fragment, title, message);
        return fragment;
    }
    public static ActionableConfirmDialogFragment newInstance(int titleResId, int messageResId)
    {
        ActionableConfirmDialogFragment fragment = new ActionableConfirmDialogFragment();
        addArguments(fragment, titleResId, messageResId);
        return fragment;
    }
    public ActionableConfirmDialogFragment()
    {
    }


    @Override
    protected AlertDialog.Builder buildDialogBuilder()
    {
        AlertDialog.Builder dialogBuilder = super.buildDialogBuilder();

        dialogBuilder.setPositiveButton(R.string.basic_confirm_dialog_ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        ((ActionableNoticeDialogListener) getActivity()).onPositiveClick(ActionableConfirmDialogFragment.this.getTag());
                    }
                }
        )
        .setNeutralButton(R.string.basic_confirm_dialog_cancel,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        ((ActionableNoticeDialogListener) getActivity()).onNeutralClick(ActionableConfirmDialogFragment.this.getTag());
                    }
                }
        );

        return dialogBuilder;
    }


    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try {
            ActionableNoticeDialogListener mActivity = (ActionableNoticeDialogListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement ActionableNoticeDialog");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
//        mActivity = null;
    }


}

package ms.airy.lib.core.dialog.stock;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

import ms.airy.lib.core.R;
import ms.airy.lib.core.dialog.ActionableNoticeDialogListener;
import ms.airy.lib.core.dialog.base.BasicTimerDialogFragment;


public class ActionableTimerDialogFragment extends BasicTimerDialogFragment
{
//    private ActionableNoticeDialog mActivity = null;

    public static ActionableTimerDialogFragment newInstance(CharSequence title)
    {
        ActionableTimerDialogFragment fragment = new ActionableTimerDialogFragment();
        addArguments(fragment, title);
        return fragment;
    }
    public static ActionableTimerDialogFragment newInstance(CharSequence title, long timerMax, long timerMin, long timerDelta)
    {
        ActionableTimerDialogFragment fragment = new ActionableTimerDialogFragment();
        addArguments(fragment, title);
        addArguments(fragment, timerMax, timerMin, timerDelta);
        return fragment;
    }
    public static ActionableTimerDialogFragment newInstance(CharSequence title, int layoutResId)
    {
        ActionableTimerDialogFragment fragment = new ActionableTimerDialogFragment();
        addArguments(fragment, title, layoutResId);
        return fragment;
    }
    public static ActionableTimerDialogFragment newInstance(CharSequence title, int layoutResId, long timerMax, long timerMin, long timerDelta)
    {
        ActionableTimerDialogFragment fragment = new ActionableTimerDialogFragment();
        addArguments(fragment, title, layoutResId);
        addArguments(fragment, timerMax, timerMin, timerDelta);
        return fragment;
    }
    public static ActionableTimerDialogFragment newInstance(int titleResId)
    {
        ActionableTimerDialogFragment fragment = new ActionableTimerDialogFragment();
        addArguments(fragment, titleResId);
        return fragment;
    }
    public static ActionableTimerDialogFragment newInstance(int titleResId, long timerMax, long timerMin, long timerDelta)
    {
        ActionableTimerDialogFragment fragment = new ActionableTimerDialogFragment();
        addArguments(fragment, titleResId);
        addArguments(fragment, timerMax, timerMin, timerDelta);
        return fragment;
    }
    public static ActionableTimerDialogFragment newInstance(int titleResId, int layoutResId)
    {
        ActionableTimerDialogFragment fragment = new ActionableTimerDialogFragment();
        addArguments(fragment, titleResId, layoutResId);
        return fragment;
    }
    public static ActionableTimerDialogFragment newInstance(int titleResId, int layoutResId, long timerMax, long timerMin, long timerDelta)
    {
        ActionableTimerDialogFragment fragment = new ActionableTimerDialogFragment();
        addArguments(fragment, titleResId, layoutResId);
        addArguments(fragment, timerMax, timerMin, timerDelta);
        return fragment;
    }
    public ActionableTimerDialogFragment()
    {
    }


    protected AlertDialog.Builder buildDialogBuilder()
    {
        AlertDialog.Builder dialogBuilder = super.buildDialogBuilder();

        // TBD:
        // How to make the count down continue
        // even after the dialog is dismissed?????

        dialogBuilder.setPositiveButton(R.string.basic_timer_dialog_ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //.Continue countdown
                        // --> This does not work. When the dialog is dismissed, the timer stops...
                        // ...

                        ((ActionableNoticeDialogListener) getActivity()).onPositiveClick(ActionableTimerDialogFragment.this.getTag());
                    }
                }
        );
//        dialogBuilder.setNeutralButton(R.string.basic_timer_dialog_cancel,
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int whichButton) {
//                        // Cancel countdown
//                        // ActionableTimerDialogFragment.this.stopTimerAndProgress();   // ?????
//                        // ...
//
//                        ((ActionableNoticeDialog) getActivity()).onNeutralClick(ActionableTimerDialogFragment.this.getTag());
//                    }
//                }
//        );

        return dialogBuilder;
    }


    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try {
            ActionableNoticeDialogListener mActivity = (ActionableNoticeDialogListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement ActionableNoticeDialog");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
//        mActivity = null;
    }


}

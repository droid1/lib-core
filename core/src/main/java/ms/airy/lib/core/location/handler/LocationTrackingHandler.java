package ms.airy.lib.core.location.handler;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;

import java.util.HashSet;
import java.util.Set;

import ms.airy.lib.core.location.common.TrackingFrequency;
import ms.airy.lib.core.location.tracking.LocationTrackingHelper;


/**
 * For scheduling location tracking at a later time.
 */
public class LocationTrackingHandler extends Handler
{
    // temporary
    private static final int LOCATION_TRACKING_SCHEDULE_EVENT = 2135;
    private static final int LOCATION_TRACKING_CANCEL_EVENT = 2137;


    // Application context.
    private final Context context;
    public LocationTrackingHandler(Context context)
    {
        this.context = context.getApplicationContext();
        init();
    }
    // TBD:
    //     do we need callback? (LocationHandler is a pretty specialized class.)
    public LocationTrackingHandler(Context context, Callback callback)
    {
        super(callback);
        this.context = context.getApplicationContext();
        init();
    }
    public LocationTrackingHandler(Context context, Looper looper)
    {
        super(looper);
        this.context = context.getApplicationContext();
        init();
    }
    // TBD: do we need callback?
    public LocationTrackingHandler(Context context, Looper looper, Callback callback)
    {
        super(looper, callback);
        this.context = context.getApplicationContext();
        init();
    }
    private void init()
    {
        // TBD:
    }


    public void cancelDelayedLocationTracking()
    {
        sendLocationTrackingMessage(LOCATION_TRACKING_CANCEL_EVENT, TrackingFrequency.NONE, 0L);
    }

    public void scheduleDelayedLocationTracking(int trackingFrequency, long delay)
    {
        sendLocationTrackingMessage(LOCATION_TRACKING_SCHEDULE_EVENT, trackingFrequency, delay);
    }

    public void sendLocationTrackingMessage(int what, int trackingFrequency, long delay)
    {
        Message msg = new Message();
        msg.what = what;
        msg.arg1 = trackingFrequency;  // this is not really needed...
        sendLocationTrackingMessage(msg, delay);
    }
    public void sendLocationTrackingMessage(Message msg, long delay)
    {
        // Uses system uptime (not unix epoch time).
        long locationScheduledTime = SystemClock.uptimeMillis() + delay;
        sendMessageAtTime(msg, locationScheduledTime);
    }


    @Override
    public void handleMessage(Message msg)
    {
        switch(msg.what) {
            case LOCATION_TRACKING_SCHEDULE_EVENT:
                // tbd:
                int trackingFrequency = msg.arg1;   // this is not really needed...
                startLocationTracking(trackingFrequency);
                // return; // ???
                break;
            case LOCATION_TRACKING_CANCEL_EVENT:
                // tbd:
                stopLocationTracking();
                // return; // ???
                break;
            default:
                // TBD:....
                Log.i("Unrecognized message: what = " + msg.what);
        }
        super.handleMessage(msg);
    }

    // TBD: get trackingFrequency from prefs???
    private void startLocationTracking(int trackingFrequency)
    {
        if(Log.D) Log.d("startLocationTracking() trackingFrequency = " + trackingFrequency);


        // tbd
        // ...
        // ???
        LocationTrackingHelper.getInstance(context).startLocationTracking(true, true);
        // ....


//        for(LocationTrackingEventCallback callback : locationEventCallbacks) {
//            callback.startScheduledLocationTracking();
//        }
    }

    private void stopLocationTracking()
    {
        Log.d("stopLocationTracking()");




//        for(LocationTrackingEventCallback callback : locationEventCallbacks) {
//            callback.cancelScheduledLocationTracking();
//        }
    }


//    // TBD:
//    // Event callbacks
//    private final Set<LocationTrackingEventCallback> locationEventCallbacks = new HashSet<LocationTrackingEventCallback>();
//    protected final Set<LocationTrackingEventCallback> getLocationEventCallbacks()
//    {
//        return locationEventCallbacks;
//    }
//
//    // This needs to be called by the "client"
//    public final void addLocationEventCallback(LocationTrackingEventCallback locationEventCallback)
//    {
//        locationEventCallbacks.add(locationEventCallback);
//    }


}

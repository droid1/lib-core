package ms.airy.lib.core.config.client.async;

import ms.airy.lib.core.config.client.AsyncConfigCallback;
import ms.airy.lib.core.config.client.AsyncConfigClient;
import ms.airy.lib.core.config.client.task.AsyncConfigGetPropertyTask;
import ms.airy.lib.core.config.client.task.AsyncConfigSetPropertyTask;


/**
 */
public class AsyncOnlineConfigClient implements AsyncConfigClient,
        AsyncConfigGetPropertyTask.AsyncTaskCallback,
        AsyncConfigSetPropertyTask.AsyncTaskCallback
{
    private final AsyncConfigCallback callback;
//    private final AsyncConfigGetPropertyTask asyncConfigGetPropertyTask;
//    private final AsyncConfigSetPropertyTask asyncConfigSetPropertyTask;

    public AsyncOnlineConfigClient(AsyncConfigCallback callback)
    {
        this.callback = callback;
//        asyncConfigGetPropertyTask = new AsyncConfigGetPropertyTask(this);
//        asyncConfigSetPropertyTask = new AsyncConfigSetPropertyTask(this);
    }

    @Override
    public void getProperty(String key)
    {
        if(Log.I) Log.i("AsyncOnlineConfigClient.getProperty() key = " + key);

        (new AsyncConfigGetPropertyTask(this)).execute(key);
    }

    @Override
    public void setProperty(String key, Object value)
    {
        if(Log.I) Log.i("AsyncOnlineConfigClient.setProperty() key = " + key + "; value = " + value);

        (new AsyncConfigSetPropertyTask(this)).execute(key, value);
    }



    //////////////////////////////////////
    // AsyncConfigGetPropertyTask.AsyncTaskCallback interface

    @Override
    public void onGetProperty(Object value, String key)
    {
        callback.onGetProperty(value, key);
    }

    //////////////////////////////////////
    // AsyncConfigSetPropertyTask.AsyncTaskCallback interface

    @Override
    public void onSetProperty(Boolean result, String key, Object value)
    {
        callback.onSetProperty(result, key, value);
    }

}

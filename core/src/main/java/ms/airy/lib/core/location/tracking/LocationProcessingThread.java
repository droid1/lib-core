package ms.airy.lib.core.location.tracking;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;


// Not being used...
public class LocationProcessingThread extends HandlerThread
{
    // TBD:
    // Is it safe to keep the reference to the handler?
    private Handler locationHandler = null;

    private Context context;
    public LocationProcessingThread(Context context, String name)
    {
        super(name);
        this.context = context.getApplicationContext();
    }

  
    @Override
    public synchronized void start()
    {
        super.start();

        // TBD:
        // Move this at the beginning of run() ???
        Looper looper = this.getLooper();
        if(looper != null) {


        } else {
            // ????
            Log.w("Looper is null. Cannot proceed.");
        }
    }

    @Override
    public void run()
    {
        if(locationHandler == null) {
            // ???
            Log.w("locationHandler is null.");
            // return;   // ???  --> It seems like we cannot return without calling super.run().
        }

        super.run();
    }

}

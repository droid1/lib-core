package ms.airy.lib.core.feedback.client;

import ms.airy.lib.core.feedback.common.SurveyEntry;


public interface AsyncSurveyClient
{
    void getSpecialSurvey();
    void getDailySurvey(long time);
    void getSurvey(long id);
}

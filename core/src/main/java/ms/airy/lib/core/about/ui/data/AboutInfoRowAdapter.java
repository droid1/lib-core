package ms.airy.lib.core.about.ui.data;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.List;

import ms.airy.lib.core.R;
import ms.airy.lib.core.about.common.AboutInfo;
import ms.airy.lib.core.about.ui.util.AboutInfoLogoUtil;
import ms.airy.lib.core.common.ImageSize;
import ms.airy.lib.core.common.ItemContentLength;
import ms.airy.lib.core.common.util.ItemSizeUtil;


public class AboutInfoRowAdapter extends BaseAdapter implements ListAdapter
{
    private final Context context;
    private List<? extends AboutInfo> aboutInfoList = null;


    public AboutInfoRowAdapter(Context context, List<? extends AboutInfo> aboutInfoList)
    {
        this.context = context;
        // tbd: check aboutInfoList is not null!!!
        this.aboutInfoList = aboutInfoList;
    }


    // ???
    public AboutInfo getAboutInfo(long id)
    {
        // if(aboutInfoList != null) {
        for(AboutInfo m : aboutInfoList) {
            if(id == m.getId()) {
                return m;
            }
        }
        // }
        return null;
    }



    @Override
    public int getCount()
    {
        return aboutInfoList.size();
    }

    @Override
    public Object getItem(int position)
    {
        return aboutInfoList.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return aboutInfoList.get(position).getId();
    }


    // what is this???
    private static class ViewHolder {
        View firstTopLevelChild;
        ImageView mainLogoImageView;
        View mainContentView;
        TextView titleTextView;
        TextView descriptionTextView;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        // ???
        View rowView = null;
        ViewHolder holder = new ViewHolder();
        if(convertView != null) {
            Log.i("convertView is being re-used...");
            rowView = convertView;
            holder = (ViewHolder) convertView.getTag();
        } else {

            LayoutInflater inflater = null;
            if(context instanceof Activity) {
                inflater = ((Activity) context).getLayoutInflater();
            } else {
                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }

            // rowView = inflater.inflate(layoutResId, parent);   // ???
            rowView = inflater.inflate(R.layout.listview_row_about_info_list_view, null);
            holder.firstTopLevelChild = rowView.findViewById(R.id.aboutinfo_about_info);
            holder.mainLogoImageView = (ImageView) rowView.findViewById(R.id.aboutinfo_main_logo);

            holder.mainContentView = rowView.findViewById(R.id.aboutinfo_main_content);
            holder.titleTextView = (TextView) rowView.findViewById(R.id.aboutinfo_title);
            holder.descriptionTextView = (TextView) rowView.findViewById(R.id.aboutinfo_description);
            // ...

            rowView.setTag(holder);
        }


        // TBD:
        String contentLength = ItemContentLength.SHORT;   // ???
        if(holder.firstTopLevelChild != null) {
            contentLength = (String) holder.firstTopLevelChild.getTag();
        }
        String flagSize = null;
        if(ItemContentLength.isValid(contentLength)) {
            flagSize = ItemSizeUtil.getFlagSizeViaDirectMapping(contentLength);
            // flagSize = ItemSizeUtil.getFlagSizeViaListDefaultMapping(contentLength);
        } else {
            // ???
            Log.i(">>>>>>>>>>>>>>>>>>> tag (for contentLength/flagSize) not found!");
            contentLength = ItemContentLength.SHORT;
            flagSize = ImageSize.SIZE_SMALL;
        }
        if(Log.D) Log.d(">>>>>>>>>>>>>>>>>>> contentLength = " + contentLength + "; flagSize = " + flagSize);


        AboutInfo aboutInfo = aboutInfoList.get(position);
        long itemId = aboutInfo.getId();

        if(holder.mainLogoImageView != null) {
            int mainLogoResId = aboutInfo.getImageResId(flagSize);
            if (mainLogoResId == 0) {
                // Use default image ????
                mainLogoResId = AboutInfoLogoUtil.getDefaultAboutInfoLogoResourceId(flagSize);
            }
            if (mainLogoResId > 0) {
                holder.mainLogoImageView.setVisibility(View.VISIBLE);
                holder.mainLogoImageView.setImageResource(mainLogoResId);
            } else {
                // ???
                holder.mainLogoImageView.setVisibility(View.INVISIBLE);
            }
        }

        if(holder.mainContentView != null) {
            String titleStr = aboutInfo.getTitle();
            if(holder.titleTextView != null) {
                if (titleStr != null) {
                    holder.titleTextView.setText(titleStr);
                } else {
                    // ???
                    holder.titleTextView.setText("");
                }
            }
            String descriptionStr = aboutInfo.getDescription();
            if(holder.descriptionTextView != null) {
                if (descriptionStr != null) {
                    holder.descriptionTextView.setText(descriptionStr);
                } else {
                    // ???
                    holder.descriptionTextView.setText("");
                }
            }
        }

        return rowView;
    }

}

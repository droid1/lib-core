package ms.airy.lib.core.about.ui.util;

import ms.airy.lib.core.R;
import ms.airy.lib.core.common.ImageSize;


public final class AboutInfoLogoUtil
{
    private AboutInfoLogoUtil() {}

    public static int getDefaultAboutInfoLogoResourceId(String logoSize)
    {
        if(ImageSize.SIZE_TINY.equals(logoSize)) {
            return R.drawable.default_about_info_logo_tiny;
        } else if(ImageSize.SIZE_SMALL.equals(logoSize)) {
                return R.drawable.default_about_info_logo_small;
        } else if(ImageSize.SIZE_MEDIUM.equals(logoSize)) {
            return R.drawable.default_about_info_logo_medium;
        } else if(ImageSize.SIZE_LARGE.equals(logoSize)) {
            return R.drawable.default_about_info_logo_large;
        } else if(ImageSize.SIZE_XLARGE.equals(logoSize)) {
            return R.drawable.default_about_info_logo_xlarge;
        } else {
            // ???
            return R.drawable.default_about_info_logo_small;
        }
    }

}

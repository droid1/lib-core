package ms.airy.lib.core.status.client.async;

import ms.airy.lib.core.status.client.AsyncStatusFetchCallback;
import ms.airy.lib.core.status.client.AsyncStatusFetchClient;
import ms.airy.lib.core.status.client.task.AsyncStatusFetchMessagesTask;
import ms.airy.lib.core.status.client.task.AsyncStatusFetchNewMessagesTask;
import ms.airy.lib.core.status.client.task.AsyncStatusFetchStatusMessageTask;
import ms.airy.lib.core.status.common.StatusMessage;

import java.util.List;


/**
 */
public class AsyncStatusMessageFetchClient implements AsyncStatusFetchClient,
        AsyncStatusFetchStatusMessageTask.AsyncTaskCallback,
        AsyncStatusFetchNewMessagesTask.AsyncTaskCallback,
        AsyncStatusFetchMessagesTask.AsyncTaskCallback
{
    private final AsyncStatusFetchCallback callback;
//    private final AsyncStatusFetchStatusMessageTask asyncStatusFetchStatusMessageTask;
//    private final AsyncStatusFetchNewMessagesTask asyncStatusFetchNewMessagesTask;
//    private final AsyncStatusFetchMessagesTask asyncStatusFetchMessagesTask;

    public AsyncStatusMessageFetchClient(AsyncStatusFetchCallback callback)
    {
        this.callback = callback;
//        asyncStatusFetchStatusMessageTask = new AsyncStatusFetchStatusMessageTask(this);
//        asyncStatusFetchNewMessagesTask = new AsyncStatusFetchNewMessagesTask(this);
//        asyncStatusFetchMessagesTask = new AsyncStatusFetchMessagesTask(this);
    }

    @Override
    public void fetchStatusMessage(long id)
    {
        if(Log.I) Log.i("AsyncStatusMessageFetchClient.fetchStatusMessage() id = " + id);

        (new AsyncStatusFetchStatusMessageTask(this)).execute(id);
    }

    @Override
    public void fetchNewMessages()
    {
        Log.i("AsyncStatusMessageFetchClient.fetchNewMessages()");

        (new AsyncStatusFetchNewMessagesTask(this)).execute();
    }

    @Override
    public void fetchMessages(long since)
    {
        if(Log.I) Log.i("AsyncStatusMessageFetchClient.fetchMessages() since = " + since);

        (new AsyncStatusFetchMessagesTask(this)).execute(since);
    }


    //////////////////////////////////////
    // AsyncStatusFetchStatusMessageTask.AsyncTaskCallback interface

    @Override
    public void onFetchStatusMessage(StatusMessage statusMessage, long id)
    {
        callback.onFetchStatusMessage(statusMessage, id);
    }


    //////////////////////////////////////
    // AsyncStatusFetchNewMessagesTask.AsyncTaskCallback interface

    @Override
    public void onFetchNewMessages(List<Long> ids)
    {
        callback.onFetchNewMessages(ids);
    }


    //////////////////////////////////////
    // AsyncStatusFetchMessagesTask.AsyncTaskCallback interface

    @Override
    public void onFetchMessages(List<Long> ids, long since)
    {
        callback.onFetchMessages(ids, since);
    }

}

package ms.airy.lib.core.phaser.badge;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import ms.airy.lib.core.phaser.common.Feature;
import ms.airy.lib.core.phaser.common.Badge;
import ms.airy.lib.core.phaser.feature.FeatureHelper;


// TBD:
// FeatureMap data should be stored to/from persistent storage such as config.
// Just using a singleton is not sufficient.....
public final class BadgeHelper
{

    // Map of feature Id -> { badge bit }.
    // FeatureMap is open ended. That is,
    // Features not in this map are supported for all badges.
    private final Map<String, Long> featureMap;

    public BadgeHelper()
    {
        featureMap = new HashMap<String, Long>();
        init();
    }
    private void init()
    {
        // TBD:
        // Populate the featureMap....
        // ....

//        Set<Feature> features = FeatureHelper.getInstance().getFeatures();
//        if(features != null && !features.isEmpty()) {
//            for(Feature f : features) {
//                featureMap.put(f.getCode(), f.getBadge());
//            }
//        }
    }


    // Singleton
    private static final class BadgeHelperHolder
    {
        private static final BadgeHelper INSTANCE = new BadgeHelper();
    }
    public static BadgeHelper getInstance()
    {
        return BadgeHelperHolder.INSTANCE;
    }


    // add or update...
    public void addFeature(String featureCode, long badge)
    {
        featureMap.put(featureCode, badge);
    }


    public long getBadge(String featureCode)
    {
        if(! featureMap.containsKey(featureCode)) {
            return Badge.BADGE_NONE;
        } else {
            return featureMap.get(featureCode);
        }
    }


    // TBD: Need to double-check this logic...
    public boolean isFeatureAvailable(String featureCode, long badge)
    {
        if(! featureMap.containsKey(featureCode)) {
            return true;
        }
        long featureBadge = getBadge(featureCode);
        if(featureBadge == Badge.BADGE_NONE ||
                (featureBadge & badge) == featureBadge ) {
            return true;
        } else {
            return false;
        }
    }


}

package ms.airy.lib.core.help.client;


/**
 */
public interface OnlineHelpClient
{
    String getSectionContent(long sectionId);
}

package ms.airy.lib.core.help.content;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.webkit.WebView;

import ms.airy.lib.core.help.common.HelpSection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Local help files are stored under /assets/help directory.
 */
public class LocalContentLoader implements ContentLoader
{
    private static final String HELP_DIR = "help";

    private static LocalContentLoader INSTANCE = null;
    public static LocalContentLoader getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new LocalContentLoader(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // We use the application context.
    private final Context context;
    protected LocalContentLoader(Context context)
    {
        this.context = context;
    }

    protected Context getContext()
    {
        return this.context;
    }


    // This is needed to prefetch online help content.
    // The id's of the local files are likely the valid id's for online sections as well...
    private long[] idArray = null;
    public long[] getSectionIds()
    {
        if(idArray == null) {
            List<Long> idList = new ArrayList<Long>();
            try {
                Resources resources = getContext().getResources();
                AssetManager assetManager = resources.getAssets();
                String[] files = new String[0];
                files = assetManager.list(HELP_DIR);
                if (files != null && files.length > 0) {
                    for (String f : files) {
                        if (Log.D) Log.d(">>>>>>>>> f = " + f);
                        long sectionId = LocalLoaderUtil.getSectionIdFromFileName(f);
                        if (sectionId > 0L) {
                            idList.add(sectionId);
                        }
                    }
                }
                // ???
                //idArray = idList.toArray(new long[]{});
                int size = idList.size();
                idArray = new long[size];
                for(int i=0; i<size; i++) {
                    idArray[i] = idList.get(i);
                }
            } catch (IOException e) {
                Log.e("Failed to list help files.", e);
            }
        }

        return idArray;
    }

    // TBD:
    // just load local html file in webview???
    // e.g., webView.loadUrl("file:///android_asset/help/filename.html");
    // ...


    @Override
    public String getContent(long sectionId)
    {
        return getSectionMap().get(sectionId);
    }

    // TBD:
    // Load all html files into a map:

//    private Map<Long, HelpSection> sectionMap = null;
    private Map<Long, String> sections = null;

//    public Map<Long, HelpSection> getSectionTitleMap()
//    {
//        if(sectionMap == null) {
//            loadHelpFiles();
//        }
//        return sectionMap;
//    }
    public Map<Long, String> getSectionMap()
    {
        if(sections == null) {
            loadHelpFiles();
        }
        return sections;
    }

    protected void loadHelpFiles()
    {
//        sectionMap = new HashMap<Long, HelpSection>();
        sections = new HashMap<Long, String>();
        try {
            Resources resources = getContext().getResources();
            AssetManager assetManager = resources.getAssets();
            String[] files = assetManager.list(HELP_DIR);
            if(Log.I) {
                Log.i(">>>>>>>>> files = " + files);
                if(files != null) {
                    Log.i(">>>>>>>>> files = " + files.length);
                }
            }

            if(files != null && files.length > 0) {
                for(String f : files) {
                    if(Log.I) Log.i(">>>>>>>>> f = " + f);

                    long sectionId = LocalLoaderUtil.getSectionIdFromFileName(f);
                    if(sectionId == 0L) {
                        Log.w("Invalid help file name: " + f);
                        continue;
                    }
                    try {
                        String fpath = HELP_DIR + "/" + f;
                        InputStream inputStream = assetManager.open(fpath);
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        StringBuilder sb = new StringBuilder();
                        String line;
                        while((line = bufferedReader.readLine()) != null) {
                            sb.append(line).append("\n");
                        }
                        String content = sb.toString();

                        // TBD:
                        // title ???

//                        HelpSection helpSection = new HelpSection();
//                        helpSection.setContent(content);
//                        sectionMap.put(sectionId, helpSection);
                        sections.put(sectionId, content);
                        if(Log.I) Log.i("Section loaded: sectionId = " + sectionId + ": content = " + content);

                    } catch (IOException e) {
                        Log.e("Failed to load help file: " + f, e);
                    }
                }
            }
        } catch (IOException e) {
            Log.e("Failed to load help files.", e);
        }
    }




}

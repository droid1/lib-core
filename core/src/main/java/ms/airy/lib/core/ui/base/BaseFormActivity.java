package ms.airy.lib.core.ui.base;

import ms.airy.lib.core.common.Config;
import ms.airy.lib.core.ui.dashbar.DashBar;
import ms.airy.lib.core.util.ConfigFactoryManager;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;


// TBD: Not working yet.
// TBD: Use use BaseActivity...
public class BaseFormActivity extends BaseActivity
{

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        
        
    }
    
    
    @Override
    protected void onStart()
    {
        super.onStart();
        
    }

    
    @Override
    protected void onDestroy()
    {
        super.onDestroy();

    }


    
    @Override
	public boolean onCreateOptionsMenu(Menu menu)
    {
		super.onCreateOptionsMenu(menu);
        
        
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.base_form, menu);

        
	    return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
	    switch (item.getItemId()) {
	    
//	    case R.id.menu_base_form_main:
//	        doMain();
//	        return true;
//	    case R.id.menu_base_form_search:
//	        // doSearch();
//	        return true;
//	    case R.id.menu_base_form_help:
//	        // doHelp();
//	        return true;
//	    case R.id.menu_base_form_close:
//	        // doClose();
//	        return true;

	    default:
	        return super.onOptionsItemSelected(item);
	    }
	}




    // temporary
    private void doMain()
    {
        // TBD: Is this save or cancel....?
        // TBD: Or, cancel all data change but save/keep the view-related states (e.g., full-screen)???
        
        // TBD
        if(Log.D) Log.d("doMain() called.");
//        //Intent intent = new Intent(this, MemoroidMain.class);
//        Intent intent = new Intent(CommonConstants.ACTION_APP_MAIN);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);   // this with "singleTop" unwinds the activity stack.
//        startActivity(intent);
//        finish();
    }

 
}
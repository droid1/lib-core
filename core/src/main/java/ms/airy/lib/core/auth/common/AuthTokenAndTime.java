package ms.airy.lib.core.auth.common;

import android.accounts.Account;

import java.io.Serializable;


// Primarily to be used as a return value from auth methods.
// --> Just use AccountAuthToken???
public class AuthTokenAndTime implements Serializable
{
    private String authToken;
    private long authTime;     // last auth time.

    public AuthTokenAndTime()
    {
    }
    public AuthTokenAndTime(String authToken, long authTime)
    {
        this.authToken = authToken;
        this.authTime = authTime;
    }

    public String getAuthToken()
    {
        return authToken;
    }
    public void setAuthToken(String authToken)
    {
        this.authToken = authToken;
    }

    public long getAuthTime()
    {
        return authTime;
    }
    public void setAuthTime(long authTime)
    {
        this.authTime = authTime;
    }

    // For debugging.
    @Override
    public String toString()
    {
        return "AuthTokenAndTime{" +
                "authToken='" + authToken + '\'' +
                ", authTime=" + authTime +
                '}';
    }

}

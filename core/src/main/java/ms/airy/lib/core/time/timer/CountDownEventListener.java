package ms.airy.lib.core.time.timer;

import android.view.View;


/**
*/
// Primarily to relay the FlexibleCountDownTimer.CountDownListener events...
public interface CountDownEventListener
{
    // The View arg is necessary in case we are using multiple timers...
    void onThresholdCross(View view);
    void onFinish(View view);
}

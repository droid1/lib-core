package ms.airy.lib.core.registry;


// TBD:
// These constants (except for a few shared ones)
// should really be defined in each separate module.....
public class Manifest
{
    private Manifest() {}

    // TBD....
    // NotificatioIDs ....
    // Valid id should be > 0.
    //public static final int NOTID_FAMILY_ABC = 1001;
    // ....
   
    

    ///////////////////////////////////////////////////////////////////
    // Common / Shared
    ///////////////////////////////////////////////////////////////////

    
    public static final int NOTIFICATIONID_CORE_ABC = 1001;

    
    // temporary. Intent. (Or,string?) (Used primarily to set the UP button action.)
    public static final String EXTRA_CORE_UP_INTENT = "ms.airy.lib.core.extra.UP_INTENT";
    public static final String EXTRA_CORE_CALLER_INTENT = "ms.airy.lib.core.extra.CALLER_INTENT";
    
    // temporary. Boolean.
    public static final String EXTRA_CORE_USE_DASHBAR = "ms.airy.lib.core.extra.USE_DASHBAR";
    public static final String EXTRA_CORE_USE_HEADERBAR = "ms.airy.lib.core.extra.USE_HEADERBAR";
    

    ///////////////////////////////////////////////////////////////////
    // User / Family
    ///////////////////////////////////////////////////////////////////
    
    
    public static final String AUTHORITIES_FAMILY_USER = "ms.airy.lib.family.user";
    public static final String AUTHORITIES_FAMILY_DEVICE = "ms.airy.lib.family.device";
    public static final String AUTHORITIES_FAMILY_MODULE = "ms.airy.lib.family.module";

    public static final String MIMIETYPE_FAMILY_LIST_USER = "vnd.android.cursor.dir/vnd.tellyfamily.user.user";
    public static final String MIMIETYPE_FAMILY_ITEM_USER = "vnd.android.cursor.item/vnd.tellyfamily.user.user";
    public static final String MIMIETYPE_FAMILY_LIST_FAMILY = "vnd.android.cursor.dir/vnd.tellyfamily.user.family";
    public static final String MIMIETYPE_FAMILY_ITEM_FAMILY = "vnd.android.cursor.item/vnd.tellyfamily.user.family";
    public static final String MIMIETYPE_FAMILY_LIST_FAMILYMEMBER = "vnd.android.cursor.dir/vnd.tellyfamily.user.familymember";
    public static final String MIMIETYPE_FAMILY_ITEM_FAMILYMEMBER = "vnd.android.cursor.item/vnd.tellyfamily.user.familymember";    
    public static final String MIMIETYPE_FAMILY_LIST_TELLYDEVICE = "vnd.android.cursor.dir/vnd.tellyfamily.device.tellydevice";
    public static final String MIMIETYPE_FAMILY_ITEM_TELLYDEVICE = "vnd.android.cursor.item/vnd.tellyfamily.device.tellydevice";
    public static final String MIMIETYPE_FAMILY_LIST_MOBILEDEVICE = "vnd.android.cursor.dir/vnd.tellyfamily.device.mobiledevice";
    public static final String MIMIETYPE_FAMILY_ITEM_MOBILEDEVICE = "vnd.android.cursor.item/vnd.tellyfamily.device.mobiledevice";


    // temporary (Used with FamilyBar)
    public static final String ACTION_FAMILY_FAMILY_GRID = "ms.airy.lib.family.action.FAMILY_GRID";
    public static final String ACTION_FAMILY_FAMILY_UPDATE = "ms.airy.lib.family.action.FAMILY_UPDATE";
    public static final String ACTION_FAMILY_FAMILYMEMBER_ADD = "ms.airy.lib.family.action.FAMILYMEMBER_ADD";

    
    public static final String ACTION_FAMILY_NEW = "ms.airy.lib.family.action.NEW";
    public static final String ACTION_FAMILY_EDIT = "ms.airy.lib.family.action.EDIT";
    public static final String ACTION_FAMILY_VIEW = "ms.airy.lib.family.action.VIEW";

    public static final String ACTION_FAMILY_NEW_FAMILY = "ms.airy.lib.family.action.NEW_FAMILY";
    public static final String ACTION_FAMILY_EDIT_FAMILY = "ms.airy.lib.family.action.EDIT_FAMILY";
    public static final String ACTION_FAMILY_VIEW_FAMILY = "ms.airy.lib.family.action.VIEW_FAMILY";

    public static final String ACTION_FAMILY_NEW_FAMILYMEMBER = "ms.airy.lib.family.action.NEW_FAMILYMEMBER";
    public static final String ACTION_FAMILY_EDIT_FAMILYMEMBER = "ms.airy.lib.family.action.EDIT_FAMILYMEMBER";
    public static final String ACTION_FAMILY_VIEW_FAMILYMEMBER = "ms.airy.lib.family.action.VIEW_FAMILYMEMBER";
    
    

    ///////////////////////////////////////////////////////////////////
    // Memo / Message
    ///////////////////////////////////////////////////////////////////


    
    
    ///////////////////////////////////////////////////////////////////
    // Calendar
    ///////////////////////////////////////////////////////////////////

    
    
    
    
}

package ms.airy.lib.core.navigation.swipetab;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class SwipeTabSectionsPagerAdapter extends FragmentPagerAdapter
{
    private Activity swipeTabMainActivity;
    private List<SwipeTabPageInfo> pageInfos;

    public SwipeTabSectionsPagerAdapter(FragmentManager fm, Activity swipeTabMainActivity)
    {
        this(fm, swipeTabMainActivity, null);
    }
    public SwipeTabSectionsPagerAdapter(FragmentManager fm, Activity swipeTabMainActivity, List<SwipeTabPageInfo> pageInfos)
    {
        super(fm);
        this.swipeTabMainActivity = swipeTabMainActivity;
        if(pageInfos != null) {  // what if it's empty() ????
            this.pageInfos = pageInfos;
        } else {
            this.pageInfos = new ArrayList<SwipeTabPageInfo>();
        }
//        // temporary
//        this.pageInfos = new AbstractSwipeTabPageRegistry().getPageInfos();
    }

    public List<SwipeTabPageInfo> getPageInfos()
    {
        return pageInfos;
    }
    public void setPageInfos(List<SwipeTabPageInfo> pageInfos)
    {
        this.pageInfos = pageInfos;
    }

    @Override
    public Fragment getItem(int position)
    {
        // getItem is called to instantiate the fragment for the given page.
        // Return a SwipeTabMainFragment.
        // return SwipeTabMainFragment.newInstance(position + 1);

        return this.pageInfos.get(position).getFragment();
    }

    @Override
    public int getCount()
    {
        // Show 3 total pages.
        // return 3;

        return this.pageInfos.size();
    }

    @Override
    public CharSequence getPageTitle(int position)
    {
        if(position >= this.pageInfos.size()) {
            return null;
        }
        String title = this.pageInfos.get(position).getTitle();
        return title;

//
//        Locale l = Locale.getDefault();
//        switch (position) {
//            case 0:
//                return swipeTabMainActivity.getString(R.string.title_section1).toUpperCase(l);
//            case 1:
//                return swipeTabMainActivity.getString(R.string.title_section2).toUpperCase(l);
//            case 2:
//                return swipeTabMainActivity.getString(R.string.title_section3).toUpperCase(l);
//        }
//        return null;
    }
}

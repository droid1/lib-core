package ms.airy.lib.core.contacts.loader;

import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.widget.CursorAdapter;

import ms.airy.lib.core.contacts.projection.ContactsDataProjectionRegistry;


// Note:
// http://developer.android.com/training/contacts-provider/retrieve-details.html
public class DefaultEmailDataCursorLoaderCallbacks extends BaseContactsDataCursorLoaderCallbacks
{
    // tbd
    private String lookupKey;

    private static String emailQuery = ContactsContract.Data.LOOKUP_KEY + " = ?"
            + " AND " +
            ContactsContract.Data.MIMETYPE + " = " +
            "'" + ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE + "'";

    public DefaultEmailDataCursorLoaderCallbacks(Context context, CursorAdapter cursorAdapter, String lookupKey)
    {
        this(context, cursorAdapter, lookupKey, ContactsDataProjectionRegistry.getEmailProjection());
    }
    public DefaultEmailDataCursorLoaderCallbacks(Context context, CursorAdapter cursorAdapter, String lookupKey, String[] projection)
    {
        super(context, cursorAdapter, emailQuery, projection);
        this.lookupKey = lookupKey;
    }


    public String getLookupKey()
    {
        return lookupKey;
    }
    public void setLookupKey(String lookupKey)
    {
        this.lookupKey = lookupKey;
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args)
    {
        Log.w("onCreateLoader() called with id = " + id);



        Uri baseUri = ContactsContract.Contacts.CONTENT_URI;

        // ???
        Uri emailUri = ContactsContract.CommonDataKinds.Email.CONTENT_URI;


        // Now create and return a CursorLoader that will take care of
        // creating a Cursor for the data being displayed.
//        String select = "((" + ContactsContract.Contacts.DISPLAY_NAME + " NOTNULL) AND ("
//                + ContactsContract.Contacts.HAS_PHONE_NUMBER + "=1) AND ("
//                + ContactsContract.Contacts.DISPLAY_NAME + " != '' ))";

        return new CursorLoader(getContext(), baseUri,
                getProjection(), getSelectQuery(), null,
                ContactsContract.Contacts.DISPLAY_NAME_PRIMARY + " COLLATE LOCALIZED ASC");
    }



}

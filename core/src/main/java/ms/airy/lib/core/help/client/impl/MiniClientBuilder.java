package ms.airy.lib.core.help.client.impl;

import org.miniclient.ApiUserClient;
import org.miniclient.impl.AbstractApiUserClient;
import org.miniclient.impl.base.DefaultApiUserClient;


// TBD:
public final class MiniClientBuilder
{
    // temporary
    private static final String DEF_HELP_WEBSERVICE_ENDPOINT = "http://www.helpstoa.com/v1/help";
    // ...

    private String helpWebServiceEndpoint = null;
    private AbstractApiUserClient helpWebClient = null;

    public MiniClientBuilder()
    {
        helpWebServiceEndpoint = DEF_HELP_WEBSERVICE_ENDPOINT;
    }

    // Singleton
    private static final class MiniClientBuilderHolder
    {
        private static final MiniClientBuilder INSTANCE = new MiniClientBuilder();
    }
    public static MiniClientBuilder getInstance()
    {
        return MiniClientBuilderHolder.INSTANCE;
    }


    public String getHelpWebServiceEndpoint()
    {
        return helpWebServiceEndpoint;
    }
    public void setHelpWebServiceEndpoint(String helpWebServiceEndpoint)
    {
        this.helpWebServiceEndpoint = helpWebServiceEndpoint;
        if(this.helpWebServiceEndpoint == null) {   // TBD: Validate the URL ???
            this.helpWebServiceEndpoint = DEF_HELP_WEBSERVICE_ENDPOINT;
        }
        helpWebClient = null;  // reset.
    }


    public ApiUserClient getHelpWebClient(String helpWebServiceEndpoint)
    {
        if(! this.helpWebServiceEndpoint.equals(helpWebServiceEndpoint)) {
            setHelpWebServiceEndpoint(helpWebServiceEndpoint);
        }
        return getHelpWebClient();
    }
    public ApiUserClient getHelpWebClient()
    {
        if(this.helpWebClient == null) {
            // TBD: UserCredential ????
            this.helpWebClient = new DefaultApiUserClient(this.helpWebServiceEndpoint);
            // TBD: Other configs.???
        }
        return this.helpWebClient;
    }

}

package ms.airy.lib.core.fragment.listener;

import android.view.KeyEvent;


// This is needed to "relay" the parent activity's keyboard-related events to its child fragments.
public interface KeyboardEventListener extends
        KeyDownEventListener,
        KeyLongPressEventListener,
        KeyMultipleEventListener,
        KeyShortcutEventListener,
        KeyUpEventListener
{
//    boolean onKeyDown(int keyCode, KeyEvent event);
//    boolean onKeyLongPress(int keyCode, KeyEvent event);
//    boolean onKeyMultiple(int keyCode, int repeatCount, KeyEvent event);
//    boolean onKeyShortcut(int keyCode, KeyEvent event);
//    boolean onKeyUp(int keyCode, KeyEvent event);

}

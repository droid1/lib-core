package ms.airy.lib.core.feedback.client.impl;


/**
 */
public abstract class AbstractOnlineFeedbackClient 
{
    // TBD:
    private String feedbackWebServiceEndpoint;

    public AbstractOnlineFeedbackClient()
    {
        this(null);  // ????
    }
    protected AbstractOnlineFeedbackClient(String feedbackWebServiceEndpoint)
    {
        this.feedbackWebServiceEndpoint = feedbackWebServiceEndpoint;
    }

    public String getFeedbackWebServiceEndpoint()
    {
        return feedbackWebServiceEndpoint;
    }
    protected void setFeedbackWebServiceEndpoint(String feedbackWebServiceEndpoint)
    {
        this.feedbackWebServiceEndpoint = feedbackWebServiceEndpoint;
        // TBD:
        // reset the miniclient...
        // ...
    }


}

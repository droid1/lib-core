package ms.airy.lib.core.notification.receiver;

import android.app.Activity;
import android.app.Notification;
import android.content.Context;
import android.content.Intent;

import ms.airy.lib.core.notification.NotificationHelper;
import ms.airy.lib.core.notification.common.NotificationConsts;
import ms.airy.lib.core.receiver.AbstractBroadcastReceiverShard;
import ms.airy.lib.core.receiver.BroadcastReceiverShard;


/**
 * BroadcastReceiver.onReceive() delegation.
 */
public class NotificationHandlerShard extends AbstractBroadcastReceiverShard implements BroadcastReceiverShard
{
    public NotificationHandlerShard()
    {
//        this(null);
    }
//    public NotificationHandlerShard(Activity parentActivity)
//    {
//        // parentActivity could be null...
//        super(parentActivity);
//    }


    @Override
    public void onReceive(Context context, Intent intent)
    {
        Log.i("NotificationHandlerShard.onReceive().");


        // intent filter ???
        // ....
        String action = intent.getAction();
        if(action == null || !action.equals(NotificationConsts.ACTION_SCHEDULED_NOTIFICATION)) {
            // bail out???
            if(Log.I) Log.i("Unhandled intent: action = " + action);
            return;
        }


        // then
        // create notification...

        // TBD:
        int notificationId = intent.getIntExtra(NotificationConsts.KEY_EXTRA_NOTIFICATION_ID, 0);
        Notification notification = intent.getParcelableExtra(NotificationConsts.KEY_EXTRA_NOTIFICATION);
        // ???

        String tag = intent.getStringExtra(NotificationConsts.KEY_EXTRA_NOTIFICATION_TAG);
        // ...

        NotificationHelper notificationHelper;
        if(tag != null) {
            notificationHelper = NotificationHelper.getInstance(context, tag);
        } else {
            notificationHelper = NotificationHelper.getInstance(context);
        }

        notificationHelper.showNotification(notificationId, notification);

    }


}

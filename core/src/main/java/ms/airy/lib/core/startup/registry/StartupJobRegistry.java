package ms.airy.lib.core.startup.registry;

import java.util.List;

import ms.airy.lib.core.startup.job.StartupJob;


public interface StartupJobRegistry
{
    // TBD
    // Should return non-null list.
    List<StartupJob> getStartupJobs();

    // ???
    void addJob(StartupJob job);

}

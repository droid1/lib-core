package ms.airy.lib.core.toast;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


/**
 * Singleton version of ToastManager.
 * Either ToastHelper or ToastManager should be fine.
 * The only difference is that ToastHelper uses the global application context
 * (and, it can store other global state).
 */
public class ToastHelper extends ToastUtil
{
    private static ToastHelper INSTANCE = null;

    public static ToastHelper getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new ToastHelper(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Only one toast at a time.
    private Toast mToast = null;

    // For simple customization.
    // (Using custom view is TBD. Likely will use a different class.)
    // TBD: is there a good "invalid" color value that can represent "unset" values???
    // For now, just use a boolean flag.
    private boolean useCustomColors;
    private boolean useBackgroundColor;
    private int textColor;          // TBD: Use color resourceId???
    private int backgroundColor;


    // Using application context.
    // TBD: Is this a good idea???
    private Context context;
    protected ToastHelper(Context context)
    {
        this(context, false, false, 0, 0);  // 0?
    }
    protected ToastHelper(Context context, int textColor)
    {
        this(context, true, false, textColor, 0);   // 0?
    }
    protected ToastHelper(Context context, int textColor, int backgroundColor)
    {
        this(context, true, true, textColor, backgroundColor);
    }
    private ToastHelper(Context context, boolean useCustomColors, boolean useBackgroundColor, int textColor, int backgroundColor)
    {
        super();
        this.context = context;
        this.useCustomColors = useCustomColors;
        this.useBackgroundColor = useBackgroundColor;
        this.textColor = textColor;
        this.backgroundColor = backgroundColor;
    }

    protected Context getContext()
    {
        return context;
    }

    // If we set the background color, we lose the oval shape of the toast.
    //    for now, we will likely just use the custom text color.
    public void setCustomTextColor(int textColor)
    {
        this.useCustomColors = true;
        this.useBackgroundColor = false;
        this.textColor = textColor;
    }
    public void setCustomColors(int textColor, int backgroundColor)
    {
        this.useCustomColors = true;
        this.useBackgroundColor = true;
        this.textColor = textColor;
        this.backgroundColor = backgroundColor;
    }
    public void unsetCustomColors()
    {
        this.useCustomColors = false;
        this.useBackgroundColor = false;
    }


    public void showToast(CharSequence text)
    {
        showToast(text, Toast.LENGTH_SHORT);
    }
    public void showToast(int resId)
    {
        showToast(resId, Toast.LENGTH_SHORT);
    }

    public void showToast(CharSequence text, int duration)
    {
        showToast(text, duration, DEFAULT_GRAVITY, DEFAULT_XOFFSET, DEFAULT_YOFFSET);
    }
    public void showToast(int resId, int duration)
    {
        showToast(resId, duration, DEFAULT_GRAVITY, DEFAULT_XOFFSET, DEFAULT_YOFFSET);
    }

    public void showToast(CharSequence text, int duration, int gravity, int xOffset, int yOffset)
    {
        if(Log.V) Log.v("showToast() called with text = " + text);
        Toast toast = Toast.makeText(this.context, text, duration);
        toast.setGravity(gravity, xOffset, yOffset);
        if(useCustomColors) {
            View toastView = toast.getView();
            if(toastView != null) {
                TextView v = (TextView) toastView.findViewById(android.R.id.message);
                if (v != null) {
                    v.setTextColor(textColor);
                    if(useBackgroundColor) {
                        v.setBackgroundColor(backgroundColor);
                        // tbd
                        Drawable bgDrawable = toastView.getBackground();
                        if(bgDrawable != null && bgDrawable instanceof ShapeDrawable) {
                            ((ShapeDrawable) bgDrawable).getPaint().setColor(backgroundColor);
                        } else if(bgDrawable != null && bgDrawable instanceof GradientDrawable) {
                            ((GradientDrawable) bgDrawable).setColor(backgroundColor);
                        } else {
                            toastView.setBackgroundColor(backgroundColor);
                        }
                        // tbd
                    }
                }
            }
        }
        showToast(toast, false);
    }
    public void showToast(int resId, int duration, int gravity, int xOffset, int yOffset)
    {
        if(Log.V) Log.v("showToast() called with resId = " + resId);
        Toast toast = Toast.makeText(this.context, resId, duration);
        toast.setGravity(gravity, xOffset, yOffset);
        if(useCustomColors) {
            View toastView = toast.getView();
            if(toastView != null) {
                TextView v = (TextView) toastView.findViewById(android.R.id.message);
                if (v != null) {
                    v.setTextColor(textColor);
                    if(useBackgroundColor) {
                        v.setBackgroundColor(backgroundColor);
                        // tbd
                        Drawable bgDrawable = toastView.getBackground();
                        if(bgDrawable != null && bgDrawable instanceof ShapeDrawable) {
                            ((ShapeDrawable) bgDrawable).getPaint().setColor(backgroundColor);
                        } else if(bgDrawable != null && bgDrawable instanceof GradientDrawable) {
                            ((GradientDrawable) bgDrawable).setColor(backgroundColor);
                        } else {
                            toastView.setBackgroundColor(backgroundColor);
                        }
                        // tbd
                    }
                }
            }
        }
        showToast(toast, false);
    }


    public void showToast(Toast toast)
    {
        showToast(toast, true);
    }

    public void showToast(Toast toast, boolean forceDefault)
    {
        if(Log.V) Log.v("showToast() called with toast = " + toast + "; forceDefault = " + forceDefault);
        // ????
        // TBD: Either remove the exiting one and show the new one immediately
        //      or, wait until the previous one is done......
//        if(mToast != null) {
//            mToast.cancel();
//            mToast = null;
//        }
        if(forceDefault) {
            toast.setGravity(DEFAULT_GRAVITY, DEFAULT_XOFFSET, DEFAULT_YOFFSET);
        }
        if(mToast != null) {
            int gravity = mToast.getGravity();
            int xoffset = mToast.getXOffset();
            int yoffset = mToast.getYOffset();
            toast.setGravity(gravity, xoffset, yoffset + YDELTA);
            //toast.setGravity(toast.getGravity(), toast.getXOffset(), yoffset + YDELTA);
        }
        mToast = toast;
        toast.show();
    }

    public void cancelToast()
    {
        if(Log.V) Log.v("cancelToast() called.");
        if(mToast != null) {
            mToast.cancel();
        }
        mToast = null;
    }


}

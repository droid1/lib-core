package ms.airy.lib.core.feedback.client.task;

import android.os.AsyncTask;

import ms.airy.lib.core.feedback.client.OnlineFeedbackClient;
import ms.airy.lib.core.feedback.client.factory.FeedbackClientFactory;


/**
 */
public class AsyncFeedbackSendFeedbackTask extends AsyncTask<Object, Void, Void>
{
    private final AsyncTaskCallback callback;

    public AsyncFeedbackSendFeedbackTask(AsyncTaskCallback callback)
    {
        this.callback = callback;
    }

    // Store input params during processing...
    private long surveyEntryId;
    private int answer;
    private String comment;

    @Override
    protected Void doInBackground(Object... params)
    {
        surveyEntryId = (Long) params[0];
        answer = (Integer) params[1];
        comment = (String) params[2];

        OnlineFeedbackClient client = FeedbackClientFactory.getInstance().getFeedbackClient();

        client.sendFeedback(surveyEntryId, answer, comment);

        return null;
    }

    @Override
    protected void onPostExecute(Void result)
    {
        // super.onPostExecute(aLong);

        this.callback.onSendFeedback(surveyEntryId, answer, comment);
    }



    // Callback interface
    public static interface AsyncTaskCallback
    {
        void onSendFeedback(long surveyEntryId, int answer, String comment);
    }

}

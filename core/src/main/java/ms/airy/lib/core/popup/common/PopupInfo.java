package ms.airy.lib.core.popup.common;

import java.util.List;


// TBD: conditional popup?
//    e.g., show it only if the user is not logged on, etc..
// ....
public class PopupInfo
{
//    private final long id;    // id can change every time the app is loaded, but that's ok.
    private final String prefKey;   // Prefs key. TBD: In case of dynamic content, use different keys for different content????
    private String category;  // Arbitrary string/tag. Fragment may query popups using category... ????
    private int priority;     // higher number means higher priority. We show no more than one popup at any given time. based on the priority (and popup type precedence).
    private int popupType;    // Splash, Eula, Change logs, Quick Help, Notice, etc...
    private int actionType;   // ??? yes/no, ok, ok/cancel, ok/cancel/ignore, accept/decline, accept/decline/later, etc..  --> determined by the popup type???
    // private int redirectMode; // forward? or back? upon positive action. Always back upon negative action.
    private String contentType;     // text or html??? --> Just use fixed values based on popupType ???
    // private boolean dynamicContent;   // If true, content can dynamically change, like ads.... How???
    private String title;
    private String content;
    private List<String> items;   // single/multiple choice list.
    private String positiveLabel;
    private String negativeLabel;
    private String neutralLabel;
    // private Intent positiveIntent;
    // private Intent negativeIntent;
    // private Intent neutralIntent;
    private int maxRepeats;      // 0 is invalid. 1: no "repeat", -1: no limit. maxRepeats is a hard limit. if counter>=maxRepeats this popup will never show again. counter<maxRepeats does not mean it will be shown, however.
    private int refractorySecs;  // if the popup was shown less than refractorySecs ago, do not show it again.
    // private boolean useAnalytics; // if true, the user's response is logged to GA.  --> Just use global setting, not popup-specific value.
    // ....
    // private boolean shown;    // whether this popup has been shown at least once to the user. -->Just use counter? to indicate how many times shown before?
    private int counter;         // counter > 0 means it has already been shown to the user.
    // private String response;     // popup-specific. accepted, declined, canceled, etc.....
    private int response;        // 1: positive, -1: negative, 0: neutral/unanswered. In case of "list", list items are "bit flagged", e.g., 1,2,4,8, etc. Respoonse is the sum of selected items.
    // private long displayTime;   // last display time.
    private long actionTime;     // last display/action time.
    // tbd: active time window? e.g., from mm/dd hh:mm to mm/dd hh:mm, otherwise do not show?
    // etc...


//    public PopupInfo()
//    {
//        this(IdUtil.generateRandomId05());
//    }
//    public PopupInfo(long id)
//    {
//        this.id = id;
//    }
    public PopupInfo(String prefKey)
    {
        this.prefKey = prefKey;
    }


//    public long getId()
//    {
//        return id;
//    }

    public String getPrefKey()
    {
        return prefKey;
    }
//    public void setPrefKey(String prefKey)
//    {
//        this.prefKey = prefKey;
//    }

    public String getCategory()
    {
        return category;
    }
    public void setCategory(String category)
    {
        this.category = category;
    }

    public int getPriority()
    {
        return priority;
    }
    public void setPriority(int priority)
    {
        this.priority = priority;
    }

    public int getPopupType()
    {
        return popupType;
    }
    public void setPopupType(int popupType)
    {
        this.popupType = popupType;
    }

    public int getActionType()
    {
        return actionType;
    }
    public void setActionType(int actionType)
    {
        this.actionType = actionType;
    }

    public String getContentType()
    {
        return contentType;
    }
    public void setContentType(String contentType)
    {
        this.contentType = contentType;
    }

    public String getTitle()
    {
        return title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getContent()
    {
        return content;
    }
    public void setContent(String content)
    {
        this.content = content;
    }

    public List<String> getItems()
    {
        return items;
    }
    public void setItems(List<String> items)
    {
        this.items = items;
    }

    public String getPositiveLabel()
    {
        return positiveLabel;
    }
    public void setPositiveLabel(String positiveLabel)
    {
        this.positiveLabel = positiveLabel;
    }

    public String getNegativeLabel()
    {
        return negativeLabel;
    }
    public void setNegativeLabel(String negativeLabel)
    {
        this.negativeLabel = negativeLabel;
    }

    public String getNeutralLabel()
    {
        return neutralLabel;
    }
    public void setNeutralLabel(String neutralLabel)
    {
        this.neutralLabel = neutralLabel;
    }

    public int getMaxRepeats()
    {
        return maxRepeats;
    }
    public void setMaxRepeats(int maxRepeats)
    {
        this.maxRepeats = maxRepeats;
    }

    public int getRefractorySecs()
    {
        return refractorySecs;
    }
    public void setRefractorySecs(int refractorySecs)
    {
        this.refractorySecs = refractorySecs;
    }

    public int getCounter()
    {
        return counter;
    }
    public void setCounter(int counter)
    {
        this.counter = counter;
    }

    public int getResponse()
    {
        return response;
    }
    public void setResponse(int response)
    {
        this.response = response;
    }

    public long getActionTime()
    {
        return actionTime;
    }
    public void setActionTime(long actionTime)
    {
        this.actionTime = actionTime;
    }


    // temporary
    public final boolean isShowable()
    {
        return isShowable(this);
    }

    // temporary
    // This method works only after all fields have been filled in, including counters, etc..
    public static boolean isShowable(PopupInfo info)
    {
        int counter = info.getCounter();
        if(counter == 0) {
            return true;
        }
        int maxRepeats = info.getMaxRepeats();
        if(maxRepeats > 0 && counter >= maxRepeats) {
            return false;
        }

        int response = info.getResponse();
        if(response != 0) {
            // ??? user already answered. no more showing of this popup....
            return false;
        }

//        // temporary
//        int popupType = info.getPopupType();
//        switch(popupType) {
//            case PopupType.PT_NOTICE:
//            case PopupType.PT_QUICK_HELP:
//            case PopupType.PT_AD:
//            case PopupType.PT_SPLASH:
//                int refractorySecs = info.getRefractorySecs();
//                long actionTime = info.getActionTime();
//                long now = System.currentTimeMillis();
//                if(actionTime + refractorySecs*1000L < now) {
//                    return true;
//                }
//                break;
//            default:
//                // do nothing..
//                break;
//        }

        // temporary
        int refractorySecs = info.getRefractorySecs();
        if(refractorySecs == 0) {
            return true;
        }
        long actionTime = info.getActionTime();
        long now = System.currentTimeMillis();
        if(actionTime + refractorySecs*1000L < now) {
            return true;
        }

        return false;
    }



    // For debugging...
    @Override
    public String toString()
    {
        return "PopupInfo{" +
                // "id=" + id +
                "prefKey='" + prefKey + '\'' +
                ", category='" + category + '\'' +
                ", priority=" + priority +
                ", popupType=" + popupType +
                ", actionType=" + actionType +
                ", contentType='" + contentType + '\'' +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", items='" + items + '\'' +
                ", positiveLabel='" + positiveLabel + '\'' +
                ", negativeLabel='" + negativeLabel + '\'' +
                ", neutralLabel='" + neutralLabel + '\'' +
                ", maxRepeats=" + maxRepeats +
                ", refractorySecs=" + refractorySecs +
                ", counter=" + counter +
                ", response=" + response +
                ", actionTime=" + actionTime +
                '}';
    }

}

package ms.airy.lib.core.config.client.impl;

import ms.airy.lib.core.config.client.OnlineConfigClient;


/**
 */
public class MockOnlineConfigClient implements OnlineConfigClient
{

    @Override
    public Object getProperty(String key)
    {
        if(Log.I) Log.i("MockOnlineConfigClient.getProperty() key = " + key);

        return null;
    }

    @Override
    public boolean setProperty(String key, Object value)
    {
        if(Log.I) Log.i("MockOnlineConfigClient.setProperty() key = " + key + "; value = " + value);

        return false;
    }

}

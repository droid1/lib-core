package ms.airy.lib.core.controller;

import android.content.Context;
import android.view.View;


// tbd...
public abstract class BaseViewController implements ViewController
{
//    private final Context context;
//    public BaseViewController(Context context)
//    {
//        this.context = context;
//    }


    protected BaseViewController()
    {
    }

    @Override
    public View addEventListeners(Context context, View rootView)
    {
        Log.d("BsseViewController.addEventListeners()");

        return rootView;
    }

    @Override
    public View refreshViewData(Context context, View rootView)
    {
        Log.d("BsseViewController.refreshViewData()");

        return rootView;
    }
}

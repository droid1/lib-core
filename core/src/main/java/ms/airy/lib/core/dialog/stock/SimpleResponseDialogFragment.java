package ms.airy.lib.core.dialog.stock;

import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import ms.airy.lib.core.R;
import ms.airy.lib.core.dialog.ActionableAssertDialogHost;
import ms.airy.lib.core.dialog.ActionableAssertDialogListener;
import ms.airy.lib.core.dialog.base.BasicMessageDialogFragment;

import java.util.HashSet;
import java.util.Set;


// http://developer.android.com/guide/topics/ui/dialogs.html
public class SimpleResponseDialogFragment extends BasicMessageDialogFragment implements ActionableAssertDialogHost
{
    // Use default button labels (includes all three buttons).
    public static SimpleResponseDialogFragment newInstance(CharSequence title, CharSequence message)
    {
        SimpleResponseDialogFragment fragment = new SimpleResponseDialogFragment();
        addArguments(fragment, title, message);
        return fragment;
    }
    public static SimpleResponseDialogFragment newInstance(CharSequence title, CharSequence message, CharSequence positiveLabel, CharSequence negativeLabel, CharSequence neutralLabel)
    {
        SimpleResponseDialogFragment fragment = newInstance(title, message);
        addButtonLabelArguments(fragment, positiveLabel, negativeLabel, neutralLabel);
        return fragment;
    }
    // Use default button labels (includes all three buttons).
    public static SimpleResponseDialogFragment newInstance(int titleResId, int messageResId)
    {
        SimpleResponseDialogFragment fragment = new SimpleResponseDialogFragment();
        addArguments(fragment, titleResId, messageResId);
        return fragment;
    }
    public static SimpleResponseDialogFragment newInstance(int titleResId, int messageResId, int positiveLabelResId, int negativeLabelResId, int neutralLabelResId)
    {
        SimpleResponseDialogFragment fragment =  newInstance(titleResId, messageResId);
        addButtonLabelArguments(fragment, positiveLabelResId, negativeLabelResId, neutralLabelResId);
        return fragment;
    }

    // label==null means, use the resource. label=="" means, do not display the button.
    protected static DialogFragment addButtonLabelArguments(DialogFragment fragment, CharSequence positiveLabel, CharSequence negativeLabel, CharSequence neutralLabel)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        if(positiveLabel != null) {
            args.putCharSequence(ARG_POSITIVE_LABEL, positiveLabel);
        }
        if(negativeLabel != null) {
            args.putCharSequence(ARG_NEGATIVE_LABEL, negativeLabel);
        }
        if(neutralLabel != null) {
            args.putCharSequence(ARG_NEUTRAL_LABEL, neutralLabel);
        }
        return fragment;
    }
    // labelResId==-1 means, Do not include the button.
    protected static DialogFragment addButtonLabelArguments(DialogFragment fragment, int positiveLabelResId, int negativeLabelResId, int neutralLabelResId)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        args.putInt(ARG_POSITIVE_LABEL_RESID, positiveLabelResId);
        args.putInt(ARG_NEGATIVE_LABEL_RESID, negativeLabelResId);
        args.putInt(ARG_NEUTRAL_LABEL_RESID, neutralLabelResId);
        return fragment;
    }

    public SimpleResponseDialogFragment()
    {
    }


    ////////////////////////////////////////////////////
    // ActionableAssertDialog interface callbacks

    private final Set<ActionableAssertDialogListener> actionables = new HashSet<ActionableAssertDialogListener>();
    @Override
    public DialogFragment addActionable(ActionableAssertDialogListener actionable)
    {
        actionables.add(actionable);
        return this;
    }


    private DialogInterface.OnClickListener positiveListener =
            new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int whichButton)
                {
                    for(ActionableAssertDialogListener a : actionables) {
                        a.onPositiveClick(SimpleResponseDialogFragment.this.getTag());
                    }
                }
            };
    private DialogInterface.OnClickListener negativeListener =
            new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int whichButton)
                {
                    for(ActionableAssertDialogListener a : actionables) {
                        a.onNegativeClick(SimpleResponseDialogFragment.this.getTag());
                    }
                }
            };
    private DialogInterface.OnClickListener neutralListener =
            new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int whichButton)
                {
                    for(ActionableAssertDialogListener a : actionables) {
                        a.onNeutralClick(SimpleResponseDialogFragment.this.getTag());
                    }
                }
            };

    @Override
    protected AlertDialog.Builder buildDialogBuilder()
    {
        CharSequence positiveLabel = getArguments().getCharSequence(ARG_POSITIVE_LABEL);
        int positiveLabelResId = getArguments().getInt(ARG_POSITIVE_LABEL_RESID);
        CharSequence negativeLabel = getArguments().getCharSequence(ARG_NEGATIVE_LABEL);
        int negativeLabelResId = getArguments().getInt(ARG_NEGATIVE_LABEL_RESID);
        CharSequence neutralLabel = getArguments().getCharSequence(ARG_NEUTRAL_LABEL);
        int neutralLabelResId = getArguments().getInt(ARG_NEUTRAL_LABEL_RESID);

        AlertDialog.Builder dialogBuilder = super.buildDialogBuilder();

        if(positiveLabel != null) {
            if(positiveLabel.length() > 0) {
                dialogBuilder.setPositiveButton(positiveLabel, positiveListener);
            }
        } else {
            int posId = positiveLabelResId;
            if(posId == 0) {
                posId = R.string.basic_response_dialog_positive;
            }
            if(posId >= 0) {
                dialogBuilder.setPositiveButton(posId, positiveListener);
            }
        }
        if(negativeLabel != null) {
            if(negativeLabel.length() > 0) {
                dialogBuilder.setNegativeButton(negativeLabel, negativeListener);
            }
        } else {
            int negId = negativeLabelResId;
            if(negId == 0) {
                negId = R.string.basic_response_dialog_negative;
            }
            if(negId >= 0) {
                dialogBuilder.setNegativeButton(negId, negativeListener);
            }
        }
        if(neutralLabel != null) {
            if(neutralLabel.length() > 0) {
                dialogBuilder.setNeutralButton(neutralLabel, neutralListener);
            }
        } else {
            int neuId = neutralLabelResId;
            if(neuId == 0) {
                neuId = R.string.basic_response_dialog_neutral;
            }
            if(neuId >= 0) {
                dialogBuilder.setNeutralButton(neuId, neutralListener);
            }
        }

        return dialogBuilder;
    }


}

package ms.airy.lib.core.device.screen;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.view.Surface;

import ms.airy.lib.core.toast.ToastHelper;


public final class ScreenOrientationLockHelper
{

    private static ScreenOrientationLockHelper INSTANCE = null;

    public static ScreenOrientationLockHelper getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new ScreenOrientationLockHelper(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Unfortunately, we cannot keep activity here.
    // It'll become stale.
    // Either we make HelpManager non-singleton and instantiate in onCreate() every time,
    //      or, pass the relevant activity in showHelp(), etc...
    // We choose the latter option...
//    // This is needed to get the fragment manager.
//    private final Activity activity;
    // We use the application context.
    private final Context context;

    private ScreenOrientationLockHelper(Context context)
    {
        this.context = context;
    }


    public void lockScreenOrientation(Activity activity)
    {
//        // TBD:
//        int orientation = activity.getResources().getConfiguration().orientation;
//        // int orientation = activity.getWindowManager().getDefaultDisplay().getRotation();
//        if(Log.I) Log.i(">>>> Current orientation = " + orientation);
//
//        // temporary
//        int rotation;
//        if(orientation == Configuration.ORIENTATION_PORTRAIT) {
//            rotation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
//        } else if(orientation == Configuration.ORIENTATION_LANDSCAPE) {
//            rotation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
//        } else {
//            // ???
//            rotation = ActivityInfo.SCREEN_ORIENTATION_SENSOR;
//        }
//
//        activity.setRequestedOrientation(rotation);

        // TBD:
        // This may NOT work on a device whose "natural" orientation is landscape....
        // ....
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        switch(rotation) {
            case Surface.ROTATION_0:
                activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                break;
            case Surface.ROTATION_90:
                activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                break;
            case Surface.ROTATION_180:
                activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT);
                break;
            case Surface.ROTATION_270:
                activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
                break;
        }

//        ToastHelper.getInstance(activity).showToast("Screen orientation locked.");
    }
    public void unlockScreenOrientation(Activity activity)
    {
        // activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        // activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
        activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
//        ToastHelper.getInstance(activity).showToast("Screen orientation unlocked.");
    }


}

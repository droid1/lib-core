package ms.airy.lib.core.config;

import android.content.Context;

import java.net.URL;


// Not being used.
// (to be used as a "cache" in ConfigMaster...)
public final class ConfigRegistry
{
    private static ConfigRegistry INSTANCE = null;
    public static ConfigRegistry getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new ConfigRegistry(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;


    private ConfigRegistry(Context context)
    {
        this.context = context;
    }


    // TBD:
    // ....


}

package ms.airy.lib.core.auth.oauth2;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerFuture;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;

import java.util.HashSet;
import java.util.Set;

import ms.airy.lib.core.auth.helper.AccountHelper;
import ms.airy.lib.core.auth.helper.AccountTypeUtil;
import ms.airy.lib.core.auth.prefs.AccountPrefsStoreManager;
import ms.airy.lib.core.auth.util.OAuth2ScopeUtil;


// TBD:
// Despite the package name, the classes in this package are not specific to "OAuth2"
// It's Android AccountManager helper classes....
public final class OAuth2Helper implements OAuth2AuthCallback
{
    private static OAuth2Helper INSTANCE = null;
    public static OAuth2Helper getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new OAuth2Helper(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;

    private OAuth2Helper(Context context)
    {
        this.context = context;
    }



    // login
    public void authorizeForAccount(int type, String name, Set<String> authScopeSet, Activity activity)
    {
        authorizeForAccount(type, name, authScopeSet, activity, false);
    }
    public void authorizeForAccount(int type, String name, Set<String> authScopeSet, Activity activity, boolean invalidateFirst)
    {
        Account account = AccountHelper.getInstance(context).getAccount(type, name);
        if(account == null) {
            Log.w("Account does not exist for type = " + type + "; name = " + name);
            // TBD: Throw exception?
            return;
        }

        String authToken = AccountPrefsStoreManager.getInstance(context).getAuthToken(type, name);
        if(authToken != null) {
            Set<String> currentAuthScopeSet = AccountPrefsStoreManager.getInstance(context).getAuthScopeSet(type, name);
            if(invalidateFirst == true
                    || ! OAuth2ScopeUtil.isScopeSufficient(currentAuthScopeSet, authScopeSet)) {  // Is this necessary??
                AccountManager.get(context).invalidateAuthToken(AccountTypeUtil.getTypeString(type), authToken);
            }
        }

        String scopes = OAuth2ScopeUtil.generateScopeString(authScopeSet);
        DefaultAccountAuthCallback callback = new DefaultAccountAuthCallback(this, type, name, scopes);

        // ???
        // Handler handler = null;
        Handler handler = createHandlerInThread(type + name);

        // What to do with future???
        // At least check isCanceled() ???
        AccountManagerFuture<Bundle> future = AccountManager.get(context).getAuthToken(account, scopes, null, activity, callback, handler);
        // ...
    }
    public void authorizeForAccount(int type, String name, Set<String> authScopeSet)
    {
        authorizeForAccount(type, name, authScopeSet, false);
    }
    public void authorizeForAccount(int type, String name, Set<String> authScopeSet, boolean notifyAuthFailure)
    {
        authorizeForAccount(type, name, authScopeSet, notifyAuthFailure, false);
    }
    public void authorizeForAccount(int type, String name, Set<String> authScopeSet, boolean notifyAuthFailure, boolean invalidateFirst)
    {
        Account account = AccountHelper.getInstance(context).getAccount(type, name);
        if(account == null) {
            Log.w("Account does not exist for type = " + type + "; name = " + name);
            // TBD: Throw exception?
            return;
        }

        String authToken = AccountPrefsStoreManager.getInstance(context).getAuthToken(type, name);
        if(authToken != null) {
            Set<String> currentAuthScopeSet = AccountPrefsStoreManager.getInstance(context).getAuthScopeSet(type, name);
            if(invalidateFirst == true
                    || ! OAuth2ScopeUtil.isScopeSufficient(currentAuthScopeSet, authScopeSet)) {  // Is this necessary??
                AccountManager.get(context).invalidateAuthToken(AccountTypeUtil.getTypeString(type), authToken);
            }
        }

        String scopes = OAuth2ScopeUtil.generateScopeString(authScopeSet);
        DefaultAccountAuthCallback callback = new DefaultAccountAuthCallback(this, type, name, scopes);

        // ???
        // Handler handler = null;
        Handler handler = createHandlerInThread(type + name);

        // What to do with future???
        // At least check isCanceled() ???
        AccountManagerFuture<Bundle> future = AccountManager.get(context).getAuthToken(account, scopes, null, notifyAuthFailure, callback, handler);
        // ...
    }

    // logout
    public void unauthorizeForAccount(int type, String name)
    {
        String authToken = AccountPrefsStoreManager.getInstance(context).getAuthToken(type, name);
        if(authToken != null) {
            AccountManager.get(context).invalidateAuthToken(AccountTypeUtil.getTypeString(type), authToken);
        } else {
            // Nothing to do...
        }
    }


    // TBD:
    private static Handler createHandlerInThread(String threadName)
    {
        Handler handler = null;
        HandlerThread thread = new HandlerThread(threadName);
        thread.start();
        if(thread.isAlive()) {
            Looper looper = thread.getLooper();
            if(looper != null) {
                handler = new Handler(looper);
                Log.d("createHandler(). Non-null handler created.");
            } else {
                Log.i("createHandler(). Looper is null.");
            }
        } else {
            Log.i("createHandler(). Handler thread is not started yet.");
        }
        return handler;
    }


    ////////////////////////////////////////////////
    // OAuth2AuthCallback interface
    // Note that these methods merely "relay" the auth result events to other callbacks, for now.
    // TBD: ...
    // Also, note:
    //       Does making OAuth2Helper a singleton make sense????
    // ...

    @Override
    public void onOAuth2AuthSuccess(int type, String name, String authToken, String authScopes, long authTime)
    {
        if(Log.I) Log.i("OAuth2Helper.onOAuth2AuthSuccess() type = " + type + "; name = " + name + "; authToken = " + authToken + "; authScopes = " + authScopes + "; authTime = " + authTime);

        // TBD:
        // Do something here and relay the events...
        // ...
        for(OAuth2AuthCallback c : callbacks) {
            c.onOAuth2AuthSuccess(type, name, authToken, authScopes, authTime);
        }
    }

    @Override
    public void onOAuth2AuthFailure(int type, String name, String reason)
    {
        if(Log.I) Log.i("OAuth2Helper.onOAuth2AuthFailure() type = " + type + "; name = " + name + "; reason = " + reason);

        // TBD:
        // Do something here and relay the events...
        // ...
        for(OAuth2AuthCallback c : callbacks) {
            c.onOAuth2AuthFailure(type, name, reason);
        }
    }


    ////////////////////////////////////////////////
    // To support async login/logout...

    // We allow multiple callbacks, but most likely, the client only needs one.
    private final Set<OAuth2AuthCallback> callbacks = new HashSet<OAuth2AuthCallback>();
    public final void addOAuth2AuthCallback(OAuth2AuthCallback callback)
    {
        callbacks.add(callback);
    }


}

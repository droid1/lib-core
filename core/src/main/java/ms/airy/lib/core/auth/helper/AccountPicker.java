package ms.airy.lib.core.auth.helper;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;

import ms.airy.lib.core.auth.common.AccountType;
import ms.airy.lib.core.dialog.ActionableChoiceDialogListener;
import ms.airy.lib.core.dialog.stock.SimpleItemListDialogFragment;

import java.util.Arrays;


// TBD:
// It can be used with or without dialog.
public class AccountPicker implements ActionableChoiceDialogListener
{
    // temporary
    private static final String DIALOG_TITLE = "Select Account";
    private static final String DEFAULT_DIALOG_TAG = "account_picker_dialog";

    // Application context
    private final Context context;

    // Callback
    private AccountPickerCallback callback;

    // Note that an instance of AccountPicker cannot reused for different accountTypes
    //    although it can be used multiple times with the same accountType.
    //   (Cf. AccountPickHelper. WIP)
    private final int accountType;
    private String[] accountNames = null;

    // This is needed to support a callback from multiple AccountPickers.
    // private String tag;

    // TBD:
    // Dialog title?
    // ...

    public AccountPicker(Context context)
    {
        this(context, null);
    }
    public AccountPicker(Context context, AccountPickerCallback callback)
    {
        // Note that using accountType == AccountType.TYPE_NONE
        //           uses the "default" account type.
        this(context, callback, AccountType.TYPE_NONE);
    }
    public AccountPicker(Context context, AccountPickerCallback callback, int accountType)
//    {
//        this(context, callback, accountType, null);
//    }
//    public AccountPicker(Context context, AccountPickerCallback callback, int accountType, String tag)
    {
        this.context = context;
        this.callback = callback;
        if(accountType == AccountType.TYPE_NONE) {
            this.accountType = AccountHelper.getInstance(context).getDefaultAccountType();
        } else {
            this.accountType = accountType;
        }
//        if(tag == null) {
//            this.tag = DEFAULT_DIALOG_TAG;
//        } else {
//            this.tag = tag;
//        }
    }


    public void setCallback(AccountPickerCallback callback)
    {
        this.callback = callback;
    }

//    // If necessary, use a different tag for each pickAccount() call.
//    public String getTag()
//    {
//        return tag;
//    }
//    public void setTag(String tag)
//    {
//        if(tag == null) {
//            this.tag = DEFAULT_DIALOG_TAG;
//        } else {
//            this.tag = tag;
//        }
//    }


    // Async methods.
    // The method names should really have been
    //    letUserPickAccount() or initiatePickingAccount(), or something like that.
    // The caller should provide AccountPickCallback implementation.

    // Note the unusual way this.tag is set.
    public void pickAccount()
    {
        pickAccount(null);
    }
    public void pickAccount(Activity activity)
    {
        pickAccount(activity, null);
    }
    public void pickAccount(Activity activity, String tag)
    {
        if(accountNames == null) {
            accountNames = AccountHelper.getInstance(context).getAccountNames(this.accountType);
        }
        if(tag == null) {
            tag = DEFAULT_DIALOG_TAG;
        }
        showAccountPickerDialog(activity, tag);
    }

    private void showAccountPickerDialog(Activity activity, String tag)
    {
        if(activity == null) {
            if(context instanceof Activity) {
                activity = (Activity) context;
            }
        }
        if(activity == null) {
            Log.e("activity is null. Cannot display account picker dialog.");
            return;   // ??
        }
        // temporary
        // neutral button only.
//        DialogFragment fragment = SimpleItemListDialogFragment.newSingleChoiceInstance(title_resid,
//                list_resid, -1, -1, 0)
//                .addActionable(this);
        DialogFragment fragment = SimpleItemListDialogFragment.newSingleChoiceInstance(DIALOG_TITLE,
                this.accountNames, "", "", null)
                .addActionable(this);
        fragment.show(activity.getFragmentManager(), tag);
    }



    //////////////////////////////////////////
    // ActionableChoiceDialogListener interface

    @Override
    public void onDialogItemClick(String tag, int which, boolean isChecked)
    {
        // isChecked is always true for singleChoice dialog.
        if(Log.I) Log.i("AccountPickerHelper.onDialogItemClick() tag = " + tag + "; which = " + which + "; isChecked = " + isChecked);

//        if(this.tag.equals(tag)) {
            String name = null;
            if(accountNames != null) {
                if(accountNames.length > which) {
                    name = accountNames[which];
                } else {
                    // ???
                    Log.w("Invalid index: which = " + which + "; accountNames = " + Arrays.toString(accountNames));
                }
            } else {
                // ???
                Log.w("accountNames array is null.");
            }
            if(callback != null) {
                callback.onAccountPicked(tag, accountType, name);
            }
//        } else {
//            // ignore
//        }
    }

    @Override
    public void onPositiveClick(String tag)
    {
    }

    @Override
    public void onNegativeClick(String tag)
    {
    }

    @Override
    public void onNeutralClick(String tag)
    {
//        if(this.tag.equals(tag)) {
            if(callback != null) {
                callback.onAccountPickCanceled(tag, accountType);
            }
//        } else {
//            // Ignore.
//        }
    }


    //////////////////////////////////////////
    // For "relaying" ActionableChoiceDialog.onDialogItemClick() ....

    public static interface AccountPickerCallback
    {
        // void onAccountPicked(Account account);
        // void onAccountPicked(int which);
        void onAccountPicked(String tag, int type, String name);
        void onAccountPickCanceled(String tag, int type);
    }


}

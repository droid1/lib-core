package ms.airy.lib.core.hangout.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ms.airy.lib.core.R;


/**
 */
public class BaseHangoutPrefsFragment extends Fragment
{


    // contains two configs
    // whether to be notified
    // lead time



    public BaseHangoutPrefsFragment()
    {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_base_hangout_prefs, container, false);
        return rootView;
    }

}

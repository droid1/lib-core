package ms.airy.lib.core.list.sample;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

import ms.airy.lib.core.R;
import ms.airy.lib.core.list.DynamicListView;
import ms.airy.lib.core.list.StableArrayAdapter;


public class SampleDynamicListViewActivity extends Activity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample_dynamic_list_view);

        ArrayList<String> mCheeseList = new ArrayList<String>();
//        for (int i = 0; i < Cheeses.sCheeseStrings.length; ++i) {
//            mCheeseList.add(Cheeses.sCheeseStrings[i]);
//        }

        StableArrayAdapter adapter = new StableArrayAdapter(this, R.layout.sample_dynamic_list_text_view, mCheeseList);
        DynamicListView listView = (DynamicListView) findViewById(R.id.listview);

        listView.setCheeseList(mCheeseList);
        listView.setAdapter(adapter);
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    }
}

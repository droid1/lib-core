package ms.airy.lib.core.popup.helper;

import android.content.Context;

import ms.airy.lib.core.popup.common.PopupInfo;
import ms.airy.lib.core.popup.common.PopupType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


// TBD:
// Dynamically download the new popups (and, content) from a remote server????
// ....
public final class PopupRegistry implements LocalPopupLoadCallback, PopupInfoProcessingCallback
{
    private static PopupRegistry INSTANCE = null;
    public static PopupRegistry getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new PopupRegistry(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;

//    // Remote popup service URL
//    private URL popupServiceURL = null;


    // Note:
    // All these maps and "index" lists are "built once and never updated",
    //     during the lifetime of this singleton.
    // The content, popupInfo objects, however, may change as users interact with popups...


    // All popup list for this release.
//    private Map<Long, PopupInfo> allPopups = null;
    private Map<String, PopupInfo> allPopups = null;


    // "Master lists"
    private final Set<String> allCategories = new HashSet<String>();
    private final Set<Integer> allPopupTypes = new HashSet<Integer>();


    // "Filter" maps

    // (1) category -> whether any "new" popup exists for the given category.
    // false means false.
    // true may mean, however, false as well as true
    //    (since the popupInfos may have been updated since we built this list).
    private final Map<String, Boolean> filterMapForCategory = new HashMap<String, Boolean>();

    // (2) popupType -> whether any "new" popup exists for the given category.
    private final Map<Integer, Boolean> filterMapForPopupType = new HashMap<Integer, Boolean>();

    // (3) category/popupType -> whether any "new" popup exists for the given category.
    private final Map<String, Boolean> filterMapForCategoryAndType = new HashMap<String, Boolean>();


    // "Index" lists.

    // (0) Includes all prefKeys that are "showable". It is both "filter" and "index".
    private final List<String> indexMapForAll = new ArrayList<String>();

    // (1) category -> list of prefKeys (reversely sorted in terms of popupType/precedence and then priority).
    private final Map<String, List<String>> indexMapForCategory = new HashMap<String, List<String>>();

    // (2) popupType -> list of prefKeys (reversely sorted in terms of popupType/precedence and then priority).
    private final Map<Integer, List<String>> indexMapForPopupType = new HashMap<Integer, List<String>>();

    // (3) category+popupType -> list of prefKeys (reversely sorted in terms of popupType/precedence and then priority).
    private final Map<String, List<String>> indexMapForCategoryAndType = new HashMap<String, List<String>>();


    private static String generateIndexKey(String category, int popupType)
    {
        return category + "---" + popupType;
    }


    // ??
    // --> This does not make sense...
    // Popups not shown to the user.
    // Reversely sorted in terms of priority.
//    private final List<Long> newPopups = new ArrayList<Long>();

    // Set this to true if all popups are loaded and checked against prefs.
    // if PopupRegistry.isReady() == false, then no popups are shown.
    private boolean ready;

    // Set this to true if all popups are shown to the user.
    // private boolean shownAll;

    private PopupRegistry(Context context)
    {
        this.context = context;
        ready = false;
        // shownAll = false;

        // call this automatically???
        initialize();
    }

    // TBD:
    private void initialize()
    {
        startLoadingPopupContent();
    }

    public boolean isReady()
    {
        return ready;
    }
    public void setReady()
    {
        // Dummy method.
        // Do nothing. Just to call the initialize() method.
    }
//    public void setReady(boolean ready)
//    {
//        this.ready = ready;
//    }

    // public boolean isShownAll()
    // {
    //     return shownAll;
    // }
//    public void setShownAll(boolean shownAll)
//    {
//        this.shownAll = shownAll;
//    }


    // For debugging/testing. Normally, the client would not need the whole map.
    public Map<String, PopupInfo> getPopupInfos()
    {
        if(! ready) {
            return null;
        }
        return allPopups;
    }

    public PopupInfo getPopupInfo(String prefKey)
    {
        if(! ready) {
            return null;
        }
        return allPopups.get(prefKey);
    }

    public boolean hasShowablePopups()
    {
        if(! ready) {
            return false;
        }
        return (! indexMapForAll.isEmpty());
    }
    public List<String> getShowablePopups()
    {
        if(! ready) {
            return null;
        }
        return indexMapForAll;
    }


    public boolean hasShowablePopups(String category)
    {
        if(! ready) {
            return false;
        }
        if(category == null || category.isEmpty()) {
            category = CAT_NULL;
        }
        if(indexMapForCategory.get(category) == null) {
            return false;
        }
        return (! indexMapForCategory.get(category).isEmpty());
    }
    public boolean hasShowablePopups(int popupType)
    {
        if(! ready) {
            return false;
        }
        if(indexMapForPopupType.get(popupType) == null) {
            return false;
        }
        return (! indexMapForPopupType.get(popupType).isEmpty());
    }
    public boolean hasShowablePopups(String category, int popupType)
    {
        if(! ready) {
            return false;
        }
        if(category == null || category.isEmpty()) {
            category = CAT_NULL;
        }
        if(indexMapForCategoryAndType.get(generateIndexKey(category, popupType)) == null) {
            return false;
        }
        return (! indexMapForCategoryAndType.get(generateIndexKey(category, popupType)).isEmpty());
    }

    public List<String> getShowablePopups(String category)
    {
        if(! ready) {
            return null;
        }
        if(category == null || category.isEmpty()) {
            category = CAT_NULL;
        }
        return indexMapForCategory.get(category);
    }
    public List<String> getShowablePopups(int popupType)
    {
        if(! ready) {
            return null;
        }
        return indexMapForPopupType.get(popupType);
    }
    public List<String> getShowablePopups(String category, int popupType)
    {
        if(! ready) {
            return null;
        }
        if(category == null || category.isEmpty()) {
            category = CAT_NULL;
        }
        return indexMapForCategoryAndType.get(generateIndexKey(category, popupType));
    }


    // async methods

    private void startLoadingPopupContent()
    {
        // ???
        AsyncLocalPopupLoadTask popupLoadTask = new AsyncLocalPopupLoadTask(context, this);
        popupLoadTask.execute();
    }
    private void startProcessingPopupContent()
    {
        AsyncPopupInfoProcessingTask popupInfoTask = new AsyncPopupInfoProcessingTask(context, this);
        popupInfoTask.execute(allPopups);
    }


    // Make these async????

    // temporary
    // Is null/empty category allowed???
    private static final String CAT_NULL = "__NULL__";
    private void buildMasterLists()
    {
        for(String k : allPopups.keySet()) {
            PopupInfo info = allPopups.get(k);
            String category = info.getCategory();
            if(category == null || category.isEmpty()) {
                category = CAT_NULL;
            }
            int popupType = info.getPopupType();
            allCategories.add(category);
            allPopupTypes.add(popupType);
        }
    }

    private void buildFilterMaps()
    {
        for(String c : allCategories) {
            filterMapForCategory.put(c, false);
        }
        for(int t : allPopupTypes) {
            filterMapForPopupType.put(t, false);
        }
        for(String c : allCategories) {
            for(int t : allPopupTypes) {
                filterMapForCategoryAndType.put(generateIndexKey(c, t), false);
            }
        }

        for(String k : allPopups.keySet()) {
            PopupInfo info = allPopups.get(k);
            String category = info.getCategory();
            if(category == null || category.isEmpty()) {
                category = CAT_NULL;
            }
            int popupType = info.getPopupType();
            // checking one is good for all three.  ??? --> actually, NO.
//            if(filterMapForCategory.get(category) == true) {
//                continue;
//            }
            if(info.isShowable()) {
                filterMapForCategory.put(category, true);
                filterMapForPopupType.put(popupType, true);
                filterMapForCategoryAndType.put(generateIndexKey(category, popupType), true);
            }
        }
    }


    // temporary
    // higher precedence/priority comes first (that is, reversely sorted).
    private Comparator<String> keyComparator = new Comparator<String>()
    {
        @Override
        public int compare(String lhs, String rhs)
        {
            PopupInfo lhsInfo = allPopups.get(lhs);
            PopupInfo rhsInfo = allPopups.get(rhs);

            if(lhsInfo == null && rhsInfo == null) {
                return 0;
            } else if(lhsInfo == null) {
                return 1;
            } else if(rhsInfo == null) {
                return -1;
            } else {
                int lhsType = lhsInfo.getPopupType();
                int rhsType = rhsInfo.getPopupType();
                if(PopupType.getPrecedence(lhsType) > PopupType.getPrecedence(rhsType)) {
                    return -1;
                } else if(PopupType.getPrecedence(lhsType) < PopupType.getPrecedence(rhsType)) {
                    return 1;
                } else {
                    int lhsPriority = lhsInfo.getPriority();
                    int rhsPriority = rhsInfo.getPriority();
                    return (lhsPriority > rhsPriority) ? -1 :
                            ((lhsPriority < rhsPriority) ? 1 : 0);
                }
            }
        }
    };

    private void buildIndexMaps()
    {
        // (0)
        for(String k : allPopups.keySet()) {
            PopupInfo popupInfo = allPopups.get(k);
            String prefKey = popupInfo.getPrefKey();
            if (popupInfo.isShowable()) {
                indexMapForAll.add(prefKey);
            }
        }
        // Sort the list reversely in terms of precdence/priority.
        Collections.sort(indexMapForAll, keyComparator);

        // (1)
        for(String c : allCategories) {
            if(filterMapForCategory.get(c) == false) {
                indexMapForCategory.put(c, null);   // ???
                continue;
            }
            List<String> list = new ArrayList<String>();
            for(String k : allPopups.keySet()) {
                PopupInfo popupInfo = allPopups.get(k);
                String prefKey = popupInfo.getPrefKey();
                String category = popupInfo.getCategory();
                if(category == null || category.isEmpty()) {
                    category = CAT_NULL;
                }
                if(c.equals(category)) {
                    if (popupInfo.isShowable()) {
                        list.add(prefKey);
                    }
                }
            }
            // Sort the list reversely in terms of precdence/priority.
            Collections.sort(list, keyComparator);
            indexMapForCategory.put(c, list);
        }

        // (2)
        for(int t : allPopupTypes) {
            if(filterMapForPopupType.get(t) == false) {
                indexMapForPopupType.put(t, null);   // ???
                continue;
            }
            List<String> list = new ArrayList<String>();
            for(String k : allPopups.keySet()) {
                PopupInfo popupInfo = allPopups.get(k);
                String prefKey = popupInfo.getPrefKey();
                int popupType = popupInfo.getPopupType();
                if(t == popupType) {
                    if (popupInfo.isShowable()) {
                        list.add(prefKey);
                    }
                }
            }
            // Sort the list reversely in terms of precdence/priority.
            Collections.sort(list, keyComparator);
            indexMapForPopupType.put(t, list);
        }

        // (3)
        for(String c : allCategories) {
            for(int t : allPopupTypes) {
                String ct = generateIndexKey(c, t);
                if(filterMapForCategoryAndType.get(ct) == false) {
                    indexMapForCategoryAndType.put(ct, null);  // ???
                    continue;
                }

                List<String> list = new ArrayList<String>();
                for(String k : allPopups.keySet()) {
                    PopupInfo popupInfo = allPopups.get(k);
                    String prefKey = popupInfo.getPrefKey();
                    String category = popupInfo.getCategory();
                    if(category == null || category.isEmpty()) {
                        category = CAT_NULL;
                    }
                    int popupType = popupInfo.getPopupType();
                    String catType = generateIndexKey(category, popupType);
                    if(ct.equals(catType)) {
                        if (popupInfo.isShowable()) {
                            list.add(prefKey);
                        }
                    }
                }
                // Sort the list reversely in terms of precdence/priority.
                Collections.sort(list, keyComparator);
                indexMapForCategoryAndType.put(ct, list);
            }
        }
    }


    ////////////////////////////////////////////////////////
    // LocalPopupLoadCallback interface

    @Override
    public void localPopupContentLoaded(Map<String,PopupInfo> popupMap)
    {
        Log.i("PopupRegistry.localPopupContentLoaded()");

        // TBD: copy/clone it??
        allPopups = popupMap;

        // ready = true;   // ???

        startProcessingPopupContent();
    }

    ////////////////////////////////////////////////////////
    // PopupInfoProcessingCallback interface

    @Override
    public void popupInfoProcessed(Map<String, PopupInfo> popupMap)
    {
        Log.i("PopupRegistry.popupInfoProcessed()");

        // TBD: copy/clone it??
        allPopups = popupMap;

        // Make these async????
        buildMasterLists();
        buildFilterMaps();
        buildIndexMaps();

        ready = true;   // ???
    }

}

package ms.airy.lib.core.dialog.stock;

import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.DialogInterface;

import ms.airy.lib.core.R;
import ms.airy.lib.core.dialog.ActionableNoticeDialogHost;
import ms.airy.lib.core.dialog.ActionableNoticeDialogListener;
import ms.airy.lib.core.dialog.base.BasicMessageDialogFragment;

import java.util.HashSet;
import java.util.Set;


// http://developer.android.com/guide/topics/ui/dialogs.html
public class SimpleConfirmDialogFragment extends BasicMessageDialogFragment implements ActionableNoticeDialogHost
{
    // Factory methods.
    public static SimpleConfirmDialogFragment newInstance(CharSequence title, CharSequence message)
    {
        SimpleConfirmDialogFragment fragment = new SimpleConfirmDialogFragment();
        addArguments(fragment, title, message);
        return fragment;
    }
    public static SimpleConfirmDialogFragment newInstance(int titleResId, int messageResId)
    {
        SimpleConfirmDialogFragment fragment = new SimpleConfirmDialogFragment();
        addArguments(fragment, titleResId, messageResId);
        return fragment;
    }
    public SimpleConfirmDialogFragment()
    {
    }


    ////////////////////////////////////////////////////
    // ActionableNoticeDialog interface callbacks

    private final Set<ActionableNoticeDialogListener> actionables = new HashSet<ActionableNoticeDialogListener>();
    @Override
    public DialogFragment addActionable(ActionableNoticeDialogListener actionable)
    {
        actionables.add(actionable);
        return this;
    }


    @Override
    protected AlertDialog.Builder buildDialogBuilder()
    {
        AlertDialog.Builder dialogBuilder = super.buildDialogBuilder();

        dialogBuilder.setPositiveButton(R.string.basic_confirm_dialog_ok,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int whichButton) {
                        for(ActionableNoticeDialogListener a : actionables) {
                            a.onPositiveClick(SimpleConfirmDialogFragment.this.getTag());
                        }
                    }
                }
        )
        .setNeutralButton(R.string.basic_confirm_dialog_cancel,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int whichButton) {
                        for(ActionableNoticeDialogListener a : actionables) {
                            a.onNeutralClick(SimpleConfirmDialogFragment.this.getTag());
                        }
                    }
                }
        );

        return dialogBuilder;
    }


}

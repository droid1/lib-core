package ms.airy.lib.core.receiver;

import android.content.Context;
import android.content.Intent;


/**
 * BroadcastReceiver.onReceive() delegates.
 */
public interface BroadcastReceiverShard
{
    void onReceive(Context context, Intent intent);
}

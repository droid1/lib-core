package ms.airy.lib.core.feedback.client.impl;

import org.miniclient.ApiUserClient;
import org.miniclient.impl.AbstractApiUserClient;
import org.miniclient.impl.base.DefaultApiUserClient;


// TBD:
public final class MiniClientBuilder
{
    // temporary
    private static final String DEF_FEEDBACK_WEBSERVICE_ENDPOINT = "http://www.feedstoa.com/v1/feedback";
    // ...

    private String feedbackWebServiceEndpoint = null;
    private AbstractApiUserClient feedbackWebClient = null;

    public MiniClientBuilder()
    {
        feedbackWebServiceEndpoint = DEF_FEEDBACK_WEBSERVICE_ENDPOINT;
    }

    // Singleton
    private static final class MiniClientBuilderHolder
    {
        private static final MiniClientBuilder INSTANCE = new MiniClientBuilder();
    }
    public static MiniClientBuilder getInstance()
    {
        return MiniClientBuilderHolder.INSTANCE;
    }


    public String getFeedbackWebServiceEndpoint()
    {
        return feedbackWebServiceEndpoint;
    }
    public void setFeedbackWebServiceEndpoint(String feedbackWebServiceEndpoint)
    {
        this.feedbackWebServiceEndpoint = feedbackWebServiceEndpoint;
        if(this.feedbackWebServiceEndpoint == null) {   // TBD: Validate the URL ???
            this.feedbackWebServiceEndpoint = DEF_FEEDBACK_WEBSERVICE_ENDPOINT;
        }
        feedbackWebClient = null;  // reset.
    }


    public ApiUserClient getFeedbackWebClient(String feedbackWebServiceEndpoint)
    {
        if(! this.feedbackWebServiceEndpoint.equals(feedbackWebServiceEndpoint)) {
            setFeedbackWebServiceEndpoint(feedbackWebServiceEndpoint);
        }
        return getFeedbackWebClient();
    }
    public ApiUserClient getFeedbackWebClient()
    {
        if(this.feedbackWebClient == null) {
            // TBD: UserCredential ????
            this.feedbackWebClient = new DefaultApiUserClient(this.feedbackWebServiceEndpoint);
            // TBD: Other configs.???
        }
        return this.feedbackWebClient;
    }

}

package ms.airy.lib.core.dialog.base;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import ms.airy.lib.core.time.timer.FlexibleCountDownTimer;
import ms.airy.lib.core.time.timer.TimerRange;


// Timer (or, reverse progress bar) dialog.
// ....
// TBD:
//      vs. Android ProgressDialog???
// http://developer.android.com/guide/topics/ui/dialogs.html
// http://developer.android.com/reference/android/app/AlertDialog.html
public class BasicTimerDialogFragment extends BaseDialogFragment implements FlexibleCountDownTimer.CountDownListener
{
    // Layout containing progress bar
    // progress bar is the only element (or, at least the first one) in the customView.
    private static final String ARG_LAYOUT_RESID = "timer_dialog_layout_resid";

    // In milliseconds...
    private static final String ARG_TIMER_MAX = "timer_dialog_timer_max";
    private static final String ARG_TIMER_MIN = "timer_dialog_timer_min";
    private static final String ARG_TIMER_DELTA = "timer_dialog_timer_delta";

    // temporary
    private static final long DEF_TIMER_MAX = 5000L;
    private static final long DEF_TIMER_MIN = 0L;
    private static final long DEF_TIMER_DELTA = 250L;
    // ...


    /*
Progress bar layout example:
   <ProgressBar
    android:id="@+id/progressbar"
    style="@android:style/Widget.ProgressBar.Horizontal"
    android:max="1000"
    android:progress="1000"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    />
 */
    private View customView = null;
//    private ViewGroup customView = null;
    private ProgressBar progressBar = null;

    // This timer is tied to the progress bar..
    private FlexibleCountDownTimer countDownTimer = null;

    // heck
    private boolean startTimerOnStart = false;


    public static BasicTimerDialogFragment newInstance(CharSequence title)
    {
        BasicTimerDialogFragment fragment = new BasicTimerDialogFragment();
        addArguments(fragment, title);
        return fragment;
    }
    public static BasicTimerDialogFragment newInstance(CharSequence title, long timerMax, long timerMin, long timerDelta)
    {
        BasicTimerDialogFragment fragment = new BasicTimerDialogFragment();
        addArguments(fragment, title);
        addArguments(fragment, timerMax, timerMin, timerDelta);
        return fragment;
    }
    public static BasicTimerDialogFragment newInstance(CharSequence title, int layoutResId)
    {
        BasicTimerDialogFragment fragment = new BasicTimerDialogFragment();
        addArguments(fragment, title, layoutResId);
        return fragment;
    }
    public static BasicTimerDialogFragment newInstance(CharSequence title, int layoutResId, long timerMax, long timerMin, long timerDelta)
    {
        BasicTimerDialogFragment fragment = new BasicTimerDialogFragment();
        addArguments(fragment, title, layoutResId);
        addArguments(fragment, timerMax, timerMin, timerDelta);
        return fragment;
    }
    public static BasicTimerDialogFragment newInstance(int titleResId)
    {
        BasicTimerDialogFragment fragment = new BasicTimerDialogFragment();
        addArguments(fragment, titleResId);
        return fragment;
    }
    public static BasicTimerDialogFragment newInstance(int titleResId, long timerMax, long timerMin, long timerDelta)
    {
        BasicTimerDialogFragment fragment = new BasicTimerDialogFragment();
        addArguments(fragment, titleResId);
        addArguments(fragment, timerMax, timerMin, timerDelta);
        return fragment;
    }
    public static BasicTimerDialogFragment newInstance(int titleResId, int layoutResId)
    {
        BasicTimerDialogFragment fragment = new BasicTimerDialogFragment();
        addArguments(fragment, titleResId, layoutResId);
        return fragment;
    }
    public static BasicTimerDialogFragment newInstance(int titleResId, int layoutResId, long timerMax, long timerMin, long timerDelta)
    {
        BasicTimerDialogFragment fragment = new BasicTimerDialogFragment();
        addArguments(fragment, titleResId, layoutResId);
        addArguments(fragment, timerMax, timerMin, timerDelta);
        return fragment;
    }
    public BasicTimerDialogFragment()
    {
    }

    protected static DialogFragment addArguments(DialogFragment fragment, CharSequence title)
    {
        return addArguments(fragment, title, 0);
    }
    protected static DialogFragment addArguments(DialogFragment fragment, CharSequence title, int layoutResId)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        if(title != null && title.length() > 0) {
            args.putCharSequence(ARG_TITLE, title);
        }
        if(layoutResId > 0) {
            args.putInt(ARG_LAYOUT_RESID, layoutResId);
        }
        return fragment;
    }
    protected static DialogFragment addArguments(DialogFragment fragment, int titleResId)
    {
        return addArguments(fragment, titleResId, 0);
    }
    protected static DialogFragment addArguments(DialogFragment fragment, int titleResId, int layoutResId)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        if(titleResId > 0) {  // ???
            args.putInt(ARG_TITLE_RESID, titleResId);
        }
        if(layoutResId > 0) {
            args.putInt(ARG_LAYOUT_RESID, layoutResId);
        }
        return fragment;
    }
    protected static DialogFragment addArguments(DialogFragment fragment, long timerMax, long timerMin, long timerDelta)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        if(timerMax > 0) {  // ???
            args.putLong(ARG_TIMER_MAX, timerMax);
        }
        if(timerMin >= 0) {  // ???
            args.putLong(ARG_TIMER_MIN, timerMin);
        }
        if(timerDelta > 0) {  // ???
            args.putLong(ARG_TIMER_DELTA, timerDelta);
        }
        return fragment;
    }


    // ???
    protected ProgressBar getProgressBar()
    {
        return progressBar;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        // heck
        if(savedInstanceState == null) { // First time
            startTimerOnStart = true;
        } else {
            // Do not start the timer...
        }

        // TBD:
        // keep the current value/progress,
        // and on configuration change (e.g, port vs. horz),
        // restart the timer (if currently in progress) from the current value...
        // (For now, if the device orientation changes, for instance, the timer stops.)
        // ....

        AlertDialog.Builder dialogBuilder = buildDialogBuilder();
        return dialogBuilder.create();
    }

    @Override
    protected AlertDialog.Builder buildDialogBuilder()
    {
        CharSequence title = getArguments().getCharSequence(ARG_TITLE);
        int titleResId = getArguments().getInt(ARG_TITLE_RESID);
        int layoutResId = getArguments().getInt(ARG_LAYOUT_RESID);
        if(layoutResId > 0) {
            // Log.w(">>>>>>>>>>>>>>>> layoutResId = " + layoutResId);

            // customView = getActivity().findViewById(layoutResId);
            LayoutInflater inflater = getActivity().getLayoutInflater();
            customView = inflater.inflate(layoutResId, null);
            // Log.w(">>>>>>>>>>>>>>>> customView = " + customView);

            progressBar = (ProgressBar) ((ViewGroup) customView).getChildAt(0);
            // progressBar = (ProgressBar) ((ViewGroup) customView).findViewWithTag("progressbar");
            // ...
//            // TBD: How to specify/pre-define the progress bar id.
//            progressBar = (ProgressBar) customView.findViewById(1);   /// ????
//            // ...
            // progressBar = (ProgressBar) getActivity().findViewById(layoutResId);
            // ...
        } else {
            Log.i("Layout not provided.");
            customView = new FrameLayout(getActivity());
            FrameLayout.LayoutParams cvlp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            // cvlp.setMargins(10,25,10,25);   // ???
            ((FrameLayout) customView).setLayoutParams(cvlp);

            progressBar = new ProgressBar(getActivity(), null, android.R.attr.progressBarStyleHorizontal);
            progressBar.setPadding(10,25,10,25);
            // progressBar.setMinimumHeight(60);
            // progressBar.setMinimumWidth(60);
            ViewGroup.LayoutParams pblp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            progressBar.setLayoutParams(pblp);
            ((FrameLayout) customView).addView(progressBar);


            // ...
//            progressBar = new ProgressBar(getActivity(), null, android.R.attr.progressBarStyleHorizontal);

            // ...
        }

        long timerMax = getArguments().getLong(ARG_TIMER_MAX);
        long timerMin = getArguments().getLong(ARG_TIMER_MIN);
        long timerDelta = getArguments().getLong(ARG_TIMER_DELTA);
        if(timerMax <= 0L) {
            timerMax = DEF_TIMER_MAX;
        }
        if(timerMin < 0L) {
            timerMin = DEF_TIMER_MIN;
        }
        if(timerDelta <= 0L) {
            timerDelta = DEF_TIMER_DELTA;
        }
        TimerRange range = new TimerRange(timerMax, timerMin);
        countDownTimer = new FlexibleCountDownTimer(range, timerDelta, this);
        // progressBar.setMax((int) (timerMax - timerMin));   // ????
        progressBar.setMax((int) timerMax);
        progressBar.setProgress((int) timerMin);   // ???  (See the comment above regarding startTimerOnStart)
        // ...

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());

        dialogBuilder.setIcon(android.R.drawable.alert_dark_frame);
        if(title != null && title.length() > 0) {
            dialogBuilder.setTitle(title);
        } else {
            if (titleResId > 0) {  // ???
                dialogBuilder.setTitle(titleResId);
            }
        }
        if(customView != null) {
//          if(progressBar != null) {
            // ????
              dialogBuilder.setView(customView);
//              dialogBuilder.setView(progressBar);

        }

        return dialogBuilder;
    }


    @Override
    public void onStart()
    {
        super.onStart();

        // TBD:
        // Start the timer.

        // temporary
        if(startTimerOnStart) {
            startTimerAndProgress();
        }
        // ...

    }

    @Override
    public void onStop()
    {
        // TBD:
        // Stop the timer. (Or, just pause it?)

        // ???
        stopTimerAndProgress();
        // ...

        super.onStop();
    }



    protected void startTimerAndProgress()
    {
        Log.d("startTimerAndProgress()");

        // temporary
//        countDownTimer.reset();
        countDownTimer.restart();
        progressBar.setProgress((int) countDownTimer.getRange().getMax());
        // ...
    }

    // ???
    protected void pauseTimerAndProgress()
    {
        Log.d("pauseTimerAndProgress()");

    }

    protected void stopTimerAndProgress()
    {
        Log.d("stopTimerAndProgress()");

        // temporary
        countDownTimer.finish();  // ???
        progressBar.setProgress((int) countDownTimer.getRange().getMin());   // ???
        // ...
    }


    ///////////////////////////////////////////////////////////
    // FlexibleCountDownTimer.CountDownListener interface

    @Override
    public void onTick(long millisUntilFinished)
    {
        if(Log.I) Log.i("onTick(). millisUntilFinished = " + millisUntilFinished);

        // ???
        long progress = countDownTimer.getRange().getMin() + millisUntilFinished;
        countDownTimer.getRange().setVal(progress);
        progressBar.setProgress((int) progress);

    }

    @Override
    public void onFinish()
    {
        Log.i("onFinish()");

        // ???
        stopTimerAndProgress();
        // ...

    }

}

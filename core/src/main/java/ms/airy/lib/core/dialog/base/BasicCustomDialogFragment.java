package ms.airy.lib.core.dialog.base;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;


// http://developer.android.com/guide/topics/ui/dialogs.html
// http://developer.android.com/reference/android/app/AlertDialog.html
public class BasicCustomDialogFragment extends BaseDialogFragment
{
    private static final String ARG_LAYOUT_RESID = "custom_dialog_layout_resid";


    private int layoutId = 0;
    private View dialogView = null;

    public static BasicCustomDialogFragment newInstance(CharSequence title, int layoutResId)
    {
        BasicCustomDialogFragment fragment = new BasicCustomDialogFragment();
        addArguments(fragment, title, layoutResId);
        return fragment;
    }
    public static BasicCustomDialogFragment newInstance(int titleResId, int layoutResId)
    {
        BasicCustomDialogFragment fragment = new BasicCustomDialogFragment();
        addArguments(fragment, titleResId, layoutResId);
        return fragment;
    }
    public BasicCustomDialogFragment()
    {
    }

    protected static DialogFragment addArguments(DialogFragment fragment, CharSequence title, int layoutResId)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        if(title != null && title.length() > 0) {
            args.putCharSequence(ARG_TITLE, title);
        }
        if(layoutResId > 0) {
            args.putInt(ARG_LAYOUT_RESID, layoutResId);
        }
        return fragment;
    }
    protected static DialogFragment addArguments(DialogFragment fragment, int titleResId, int layoutResId)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        if(titleResId > 0) {  // ???
            args.putInt(ARG_TITLE_RESID, titleResId);
        }
        if(layoutResId > 0) {
            args.putInt(ARG_LAYOUT_RESID, layoutResId);
        }
        return fragment;
    }


    // Could this be null?
    // When is onCreateDialog() called?
    // Upon show()?
    // Then, this method should be called after show()...
    // but, then what happens if the dialog has been dismissed????
    // --> this (and, getView()) seems always null....
    public View getDialogView()
    {
        return dialogView;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        AlertDialog.Builder dialogBuilder = buildDialogBuilder();
        return dialogBuilder.create();
    }

    @Override
    protected AlertDialog.Builder buildDialogBuilder()
    {
        CharSequence title = getArguments().getCharSequence(ARG_TITLE);
        int titleResId = getArguments().getInt(ARG_TITLE_RESID);
        int layoutResId = getArguments().getInt(ARG_LAYOUT_RESID);
        if(layoutResId > 0) {
            this.layoutId = layoutResId;
            // dialogView ...
        } else {
            // ???
            // Is this an error????
            // Use the default layout????
            Log.i("Custom layout not set.");
            // ...
        }

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());

        dialogBuilder.setIcon(android.R.drawable.alert_dark_frame);
        if(title != null && title.length() > 0) {
            dialogBuilder.setTitle(title);
        } else {
            if (titleResId > 0) {  // ???
                dialogBuilder.setTitle(titleResId);
            }
        }

        LayoutInflater inflater = getActivity().getLayoutInflater();
        if(layoutResId > 0) {
            // ????
            dialogView = inflater.inflate(layoutId, null);
            dialogBuilder.setView(dialogView);
            // ...
        } else {
            // ????
        }

        return dialogBuilder;
    }

}

package ms.airy.lib.core.feedback.client.task;

import android.os.AsyncTask;

import ms.airy.lib.core.feedback.client.OnlineSurveyClient;
import ms.airy.lib.core.feedback.client.factory.FeedbackClientFactory;
import ms.airy.lib.core.feedback.common.SurveyEntry;

/**
 */
public class AsyncSurveyGetSpecialSurveyTask extends AsyncTask<Void, Void, SurveyEntry>
{
    private final AsyncTaskCallback callback;

    public AsyncSurveyGetSpecialSurveyTask(AsyncTaskCallback callback)
    {
        this.callback = callback;
    }


    @Override
    protected SurveyEntry doInBackground(Void... params)
    {
        OnlineSurveyClient client = FeedbackClientFactory.getInstance().getSurveyClient();

        SurveyEntry surveyEntry = client.getSpecialSurvey();

        return surveyEntry;
    }

    @Override
    protected void onPostExecute(SurveyEntry surveyEntry)
    {
        // super.onPostExecute(aLong);

        this.callback.onGetSpecialSurvey(surveyEntry);
    }


    // Callback interface
    public static interface AsyncTaskCallback
    {
        void onGetSpecialSurvey(SurveyEntry surveyEntry);
    }

}

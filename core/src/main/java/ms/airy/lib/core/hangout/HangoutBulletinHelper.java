package ms.airy.lib.core.hangout;

import android.content.Context;

import ms.airy.lib.core.hangout.config.HangoutConfigHelper;
import ms.airy.lib.core.hangout.prefs.HangoutPrefsStoreManager;
import ms.airy.lib.core.cron.CronExpressionParser;


public final class HangoutBulletinHelper
{
    private static HangoutBulletinHelper INSTANCE = null;
    public static HangoutBulletinHelper getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new HangoutBulletinHelper(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;

    // tbd
    private final CronExpressionParser cronExpressionParser;


    private HangoutBulletinHelper(Context context)
    {
        this.context = context;
        String expression = HangoutConfigHelper.getInstance(context).getHangoutScheduleExpression();
        if(expression == null || expression.isEmpty()) {  // ???
            this.cronExpressionParser = null;
        } else {
            this.cronExpressionParser = new CronExpressionParser(expression);
        }
    }


    public boolean isNotifyEnabled()
    {
        return HangoutPrefsStoreManager.getInstance(context).isNotifyEnabled();
    }
    public void enableNotify()
    {
        HangoutPrefsStoreManager.getInstance(context).setNotifyEnabled(true);
    }
    public void disableNotify()
    {
        HangoutPrefsStoreManager.getInstance(context).setNotifyEnabled(false);
    }

    public int getNotifyLeadTime()
    {
        return HangoutPrefsStoreManager.getInstance(context).getNofityLeadTime();
    }
    public void setNotifyLeadTime(int notifyLeadTime)
    {
        HangoutPrefsStoreManager.getInstance(context).setNofityLeadTime(notifyLeadTime);
    }


    // tbd:
    public long getNextHangoutTime()
    {
        if(cronExpressionParser == null) {
            return 0L;  // ???
        }
        Long t = cronExpressionParser.getNextCronTime();   // Can this be null???
        if(Log.V) Log.v("HangoutScheduleHelper.getNextHangoutTime() time = " + t);
        if(t == null) {
            return 0L;
        }
        return t;
    }

    public long getNextNotifyTime()
    {
        if(isNotifyEnabled() == false || cronExpressionParser == null) {
            return 0L;   // ???
        }

        long deltaMillis = -1000L * getNotifyLeadTime();  // Note the minus sign.
        long shiftMillis = -1L * HangoutConfigHelper.getInstance(context).getHangoutCronLeadMillis();

        Long t = cronExpressionParser.getNextCronTimeShifted(deltaMillis + shiftMillis);
        // Can t be null???
        if(Log.V) Log.v("HangoutScheduleHelper.getNextNotifyTime() time = " + t);
        if(t == null) {
            return 0L;
        }
        return t;
    }


}

package ms.airy.lib.core.ui.headerbar;

// To be used as an ActionTarget...
public interface ActionCallback
{
    // TBD: Return an object?
    //      Include a "generic" argument?
    // ....
    void doAction();

}

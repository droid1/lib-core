package ms.airy.lib.core.invite.client.task;

import android.os.AsyncTask;

import ms.airy.lib.core.invite.client.BuddyInviteClient;
import ms.airy.lib.core.invite.client.factory.InviteClientFactory;
import ms.airy.lib.core.invite.common.RecipientInfo;


/**
 */
public class AsyncBuddyGetRecipientInfoTask extends AsyncTask<String, Void, RecipientInfo>
{
    private final AsyncTaskCallback callback;
    public AsyncBuddyGetRecipientInfoTask(AsyncTaskCallback callback)
    {
        this.callback = callback;
    }

    // Store input params during processing
    private String recipient;


    @Override
    protected RecipientInfo doInBackground(String... params)
    {
        recipient = (String) params[0];

        BuddyInviteClient client = InviteClientFactory.getInstance().getInviteClient();

        RecipientInfo recipientInfo = client.getRecipientInfo(recipient);

        return recipientInfo;
    }

    @Override
    protected void onPostExecute(RecipientInfo recipientInfo)
    {
        // super.onPostExecute(aLong);

        this.callback.onGetRecipientInfo(recipientInfo, recipient);
    }




    // Callback interface
    public static interface AsyncTaskCallback
    {
        void onGetRecipientInfo(RecipientInfo recipientInfo, String recipient);
    }

}

package ms.airy.lib.core.popup.helper;

import ms.airy.lib.core.popup.common.PopupInfo;

import java.util.Map;


/**
*/
public interface LocalPopupLoadCallback
{
    void localPopupContentLoaded(Map<String,PopupInfo> popupMap);
}

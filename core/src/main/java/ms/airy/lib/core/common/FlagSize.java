package ms.airy.lib.core.common;

import java.util.HashMap;
import java.util.Map;


// "Enum"
public final class FlagSize
{
    private FlagSize() {};

    // for efficiency...
    private static final Map<String, String> decrMap = new HashMap<String,String>();
    static {
        decrMap.put(ImageSize.SIZE_MINI, ImageSize.SIZE_MINI);
        decrMap.put(ImageSize.SIZE_TINY, ImageSize.SIZE_MINI);
        decrMap.put(ImageSize.SIZE_SMALL, ImageSize.SIZE_TINY);
        decrMap.put(ImageSize.SIZE_MEDIUM, ImageSize.SIZE_SMALL);
        decrMap.put(ImageSize.SIZE_LARGE, ImageSize.SIZE_MEDIUM);
        decrMap.put(ImageSize.SIZE_XLARGE, ImageSize.SIZE_LARGE);
        decrMap.put(ImageSize.SIZE_XXLARGE, ImageSize.SIZE_XLARGE);
    }


    public static boolean isValid(String size)
    {
        return decrMap.containsKey(size);
    }

    public static String getOneSizeSmaller(String size)
    {
        return decrMap.get(size);
    }

    public static String getTwoSizeSmaller(String size)
    {
//        String oneSizeSmaller = decrMap.get(size);
//        if(isValid(oneSizeSmaller)) {
//            return decrMap.get(oneSizeSmaller);
//        } else {
//            // ?????
//        }
        return decrMap.get(decrMap.get(size));
    }
    public static String getThreeSizeSmaller(String size)
    {
        return decrMap.get(decrMap.get(decrMap.get(size)));
    }
    public static String getFourSizeSmaller(String size)
    {
        return decrMap.get(decrMap.get(decrMap.get(decrMap.get(size))));
    }


}

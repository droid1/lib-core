package ms.airy.lib.core.launcher;

import android.content.Context;


// TBD:
// For using different launcher activity based on various conditions.
// e.g., user prefs, time and location, etc.....
// ...
// Vs. Startup ????
// ...
public final class ConditionalLauncherManager
{
    private static ConditionalLauncherManager INSTANCE = null;
    public static ConditionalLauncherManager getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new ConditionalLauncherManager(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;


    private ConditionalLauncherManager(Context context)
    {
        this.context = context;
    }

    // TBD

    // ....


}

package ms.airy.lib.core.popup.util;

import ms.airy.lib.core.popup.common.PopupInfo;
import ms.airy.lib.core.popup.common.PopupType;
import ms.airy.lib.core.util.IdUtil;

import org.miniclient.json.LiteJsonParser;
import org.miniclient.json.MiniClientJsonException;
import org.miniclient.json.parser.MiniClientJsonParser;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;
import java.util.Map;


/**
 */
public final class JsonContentParser
{
    private JsonContentParser() {}


    public static PopupInfo buildPopupInfo(InputStream inputStream)
    {
        PopupInfo popupInfo = null;

        Map<String,Object> jsonRoot = parseJsonContent(inputStream);
        if(jsonRoot != null) {
            popupInfo = buildPopupInfo(jsonRoot);
        }

        return popupInfo;
    }


    private static Map<String,Object> parseJsonContent(InputStream inputStream)
    {
        Map<String,Object> jsonRoot = null;
        LiteJsonParser jsonParser = new MiniClientJsonParser();
        Reader reader = new InputStreamReader(inputStream);
        try {
            // ???
            jsonRoot = (Map<String,Object>) ((Map<?,?>) jsonParser.parse(reader));
            if(Log.D) {
                Log.d("Parsed: jsonRoot = " + jsonRoot);
            }
        } catch (MiniClientJsonException e) {
            Log.e("Failed to parse the json content.", e);
        } catch (IOException e) {
            Log.e("Error while parsing the json content.", e);
        }

        return jsonRoot;
    }

    private static PopupInfo buildPopupInfo(Map<String,Object> jsonRoot)
    {
//        long id = 0L;
//        Object objId = jsonRoot.get("id");
//        if(objId != null) {
//            if(objId instanceof Long) {
//                id = (Long) objId;
//            } else if (objId instanceof String) {   // ???
//                id = Long.parseLong((String) objId);
//            } else {
//                // ???
//            }
//        }
//        if(id == 0L) {
//            id = IdUtil.generateRandomId05();   // ???
//        }
//        PopupInfo popupInfo = new PopupInfo(id);

        String prefKey = null;
        Object objKey = jsonRoot.get("prefKey");
        if(objKey != null && objKey instanceof String) {
            prefKey = (String) objKey;
        } else {
            Log.w("Invalid prefKey: " + objKey);
            // prefKey is required.
            // what to do???
            return null;
        }

        PopupInfo popupInfo = new PopupInfo(prefKey);

        Object v1 = null;

        v1 = jsonRoot.get("category");
        if(v1 != null && v1 instanceof String) {
            String category = (String) v1;
            popupInfo.setCategory(category);
        } else {
            Log.w("Invalid category: " + v1);
            // category is optional.. ????
        }

        v1 = jsonRoot.get("priority");
        if(v1 != null && v1 instanceof Number) {
            int priority = 0;
            if(v1 instanceof Long) {
                priority = (int) ((long) ((Long) v1));
            } else if(v1 instanceof Integer) {
                priority = (int) ((Integer) v1);
            } else {
                Log.w("Invalid type: priority = " + v1);
            }
            popupInfo.setPriority(priority);
        } else {
            Log.w("Invalid priority: " + v1);
            // --> priority = 0???
        }

        int popupType = 0;
        v1 = jsonRoot.get("popupType");
        if(v1 != null && v1 instanceof Number) {
            if(v1 instanceof Long) {
                popupType = (int) ((long) ((Long) v1));
            } else if(v1 instanceof Integer) {
                popupType = (int) ((Integer) v1);
            } else {
                Log.w("Invalid type: popupType = " + v1);
            }
        } else {
            // ???
            popupType = PopupType.getDefaultType();
            // ????
            Log.w("Invalid popupType: " + v1 + ". Using the default value: " + popupType);
        }
        popupInfo.setPopupType(popupType);

        v1 = jsonRoot.get("actionType");
        if(v1 != null && v1 instanceof Number) {
            int actionType = 0;
            if(v1 instanceof Long) {
                actionType = (int) ((long) ((Long) v1));
            } else if(v1 instanceof Integer) {
                actionType = (int) ((Integer) v1);
            } else {
                Log.w("Invalid type: actionType = " + v1);
            }
            popupInfo.setActionType(actionType);
        } else {
            Log.w("Invalid actionType: " + v1);
            popupInfo.setActionType(PopupType.getDefaultActionType(popupType));
        }

        v1 = jsonRoot.get("contentType");
        if(v1 != null && v1 instanceof String) {
            String contentType = (String) v1;
            popupInfo.setContentType(contentType);
        } else {
            Log.w("Invalid contentType: " + v1);
            popupInfo.setContentType(PopupType.getDefaultContentType(popupType));
        }

        v1 = jsonRoot.get("title");
        if(v1 != null && v1 instanceof String) {
            String title = (String) v1;
            popupInfo.setTitle(title);
        } else {
            Log.w("Invalid title: " + v1);
        }

        // Depending on the content type,
        // either content or items is used, but not both...

        v1 = jsonRoot.get("content");
        if(v1 != null && v1 instanceof String) {
            String content = (String) v1;
            popupInfo.setContent(content);
        } else {
            Log.w("Invalid content: " + v1);
        }

        v1 = jsonRoot.get("items");
        if(v1 != null && v1 instanceof List<?>) {
            List<String> items = (List<String>) ((List<?>) v1);
            popupInfo.setItems(items);
        } else {
            Log.w("Invalid items: " + v1);
        }


        v1 = jsonRoot.get("positiveLabel");
        if(v1 != null && v1 instanceof String) {
            String positiveLabel = (String) v1;
            popupInfo.setPositiveLabel(positiveLabel);
        } else {
            Log.w("Invalid positiveLabel: " + v1);
        }

        v1 = jsonRoot.get("negativeLabel");
        if(v1 != null && v1 instanceof String) {
            String negativeLabel = (String) v1;
            popupInfo.setNegativeLabel(negativeLabel);
        } else {
            Log.w("Invalid negativeLabel: " + v1);
        }

        v1 = jsonRoot.get("neutralLabel");
        if(v1 != null && v1 instanceof String) {
            String neutralLabel = (String) v1;
            popupInfo.setNeutralLabel(neutralLabel);
        } else {
            Log.w("Invalid neutralLabel: " + v1);
        }

        int maxRepeats = 0;  // 0 is an invalid value.
        v1 = jsonRoot.get("maxRepeats");
        if(v1 != null && v1 instanceof Number) {
            if(v1 instanceof Long) {
                maxRepeats = (int) ((long) ((Long) v1));
            } else if(v1 instanceof Integer) {
                maxRepeats = (int) ((Integer) v1);
            } else {
                Log.w("Invalid type: maxRepeats = " + v1);
            }
        } else {
            Log.w("Invalid maxRepeats: " + v1);
        }
        if(maxRepeats == 0) {
            maxRepeats = PopupType.getDefaultMaxRepeats(popupType);
        }
        popupInfo.setMaxRepeats(maxRepeats);

        v1 = jsonRoot.get("refractorySecs");
        if(v1 != null && v1 instanceof Number) {
            int refractorySecs = 0;
            if(v1 instanceof Long) {
                refractorySecs = (int) ((long) ((Long) v1));
            } else if(v1 instanceof Integer) {
                refractorySecs = (int) ((Integer) v1);
            } else {
                Log.w("Invalid type: refractorySecs = " + v1);
            }
            popupInfo.setRefractorySecs(refractorySecs);
        } else {
            Log.w("Invalid refractorySecs: " + v1);
            popupInfo.setRefractorySecs(PopupType.getDefaultRefractorySecs(popupType));
        }

        if(Log.I) Log.i(">>>>>>>>>>>>> popupInfo = " + popupInfo);
        return popupInfo;
    }


}

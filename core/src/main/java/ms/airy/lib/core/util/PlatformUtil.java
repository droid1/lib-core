package ms.airy.lib.core.util;

import android.content.Context;

// TBD:
// Android platform-related utility functions...
public class PlatformUtil
{
    // temporary
    private static final String FEATURE_GOOGLE_TV = "com.google.android.tv";
    
    // Prevents instantiation...
    private PlatformUtil() {}

    // ...

    

    // TBD....
    
    public static boolean isGoogleTV(Context context)
    {
        return context.getPackageManager().hasSystemFeature(FEATURE_GOOGLE_TV);
    }

    // 5~6"
    public static boolean isSmallTablet(Context context)
    {
        // temporary
        return false;
    }

    // 9~10"
    public static boolean isLargeTablet(Context context)
    {
        // temporary
        return false;
    }

    public static boolean isTablet(Context context)
    {
        // temporary
        return isSmallTablet(context) || isLargeTablet(context);
    }

    public static boolean isPhone(Context context)
    {
        // temporary
        return !(isTablet(context) || isGoogleTV(context));
    }


}

package ms.airy.lib.core.control.common;


// "Predefined" aspect ratios.
// An app is not limited to the set of values here.
public class AspectRatio
{
    // Note that the w, h in "w:h" need not be integers...
    // Always use w >= h ???
    public static final String SQUARE = "1:1";
//    public static final String STANDARD = "3:2";
//    public static final String GOLDEN_RATIO = "1.618:1";
    public static final String GOLDEN_RATIO_HORZ = "1.618:1";
    public static final String GOLDEN_RATIO_VERT = "1:1.618";
    public static final String STANDARD_HORZ = "3:2";
    public static final String STANDARD_VERT = "2:3";

    public static final String INDEXCARD_3x5 = "5:3";  // note 3 by 5 card has 3 inch height, 5 inch width.
    public static final String INDEXCARD_4x6 = "3:2";
    public static final String INDEXCARD_5x8 = "8:5";

    public static final String TV_STANDARD = "4:3";
    public static final String TV_WIDESCREEN = "16:9";
    public static final String CINEMASCOPE = "2.40:1";  // ???
    // ...


    // Static consts. only.
    protected AspectRatio() {}

}

package ms.airy.lib.core.location.common;


// To be used in user preference settings.
public final class LocationAccuracy
{
    public static final int UNKNOWN = 0;   // ???
//    public static final int COARSE = (0x1 << 0);
//    public static final int FINE = (0x1 << 1);
    // LOW/MEDIUM correspond to COARSE...
    public static final int LOW = 1;
    public static final int MEDIUM = 2;
    public static final int HIGH = 3;
    // ...

    private LocationAccuracy() {}

    // temporary
    public static int getDefaultAccuracy()
    {
        return MEDIUM;
    }
}

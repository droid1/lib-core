package ms.airy.lib.core.location.tracking;

import android.content.Context;
import android.location.Criteria;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ms.airy.lib.core.location.common.LocationAccuracy;
import ms.airy.lib.core.location.common.PowerRequirement;
import ms.airy.lib.core.location.common.TrackingFrequency;
import ms.airy.lib.core.location.data.LocationDataHelper;
import ms.airy.lib.core.location.prefs.LocationPrefsStoreManager;


public final class LocationTrackingHelper
{
    private static LocationTrackingHelper INSTANCE = null;
    public static LocationTrackingHelper getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new LocationTrackingHelper(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;
    // Is it safe to keep the reference of LocationManager?
    private LocationManager locationManager = null;
    private boolean trackingEnabled;
    private int trackingFrequency;
    private int locationAccuracy;
    private int powerRequirement;
    private Criteria updateCriteria;
    // private long minUpdateMillis;    
    // private String trackingCronExpression;
    // private long defaultTrackingDuration;
    
    // TBD:
    private List<String> availableProviders = null;
    // private List<String> enabledProviders = null;

    // TBD:
    // This needs to be updated in GpsStatus.Listener.onGpsStatusChanged()
    private GpsStatus gpsStatus = null;
    // ..

    // TBD:
    // foreground vs background update???
    // ...

    // Status???
    private boolean trackingInForeground;   // active tracking 
    private boolean trackingInBackground;   // passive tracking...
    // ...

    // TBD:
    private LocationListener locationListener = null;


    private LocationTrackingHelper(Context context)
    {
        this.context = context;

        trackingInForeground = false;
        trackingInBackground = false;

        trackingEnabled = LocationPrefsStoreManager.getInstance(context).isLocationTrackingEnabled();
        trackingFrequency = LocationPrefsStoreManager.getInstance(context).getLocationTrackingFrequency();
        locationAccuracy = LocationPrefsStoreManager.getInstance(context).getLocationTrackingAccuracy();
        powerRequirement = LocationPrefsStoreManager.getInstance(context).getTrackingPowerRequirement();

        // if(trackingEnabled == true)   // ???
        // Can we re-use criteria??
        updateCriteria = new Criteria();
        // TBD: Fine/High accuracy requires FINE permission.
        updateCriteria.setAccuracy( locationAccuracy == LocationAccuracy.HIGH
                ? Criteria.ACCURACY_FINE : Criteria.ACCURACY_COARSE);   // ???
        // Power
        if(powerRequirement == PowerRequirement.NONE) {
            updateCriteria.setPowerRequirement(Criteria.NO_REQUIREMENT);
        } else {
            updateCriteria.setPowerRequirement(
                    powerRequirement == PowerRequirement.HIGH ?
                            Criteria.POWER_HIGH :
                            (powerRequirement == PowerRequirement.MEDIUM ?
                                    Criteria.POWER_MEDIUM :
                                    Criteria.POWER_LOW)
            );
        }
        // etc...

        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        // locationManager.addGpsStatusListener(this);

        // ...
        // Note that availableProviders will not change (at least in the lifetime of this singleton)
        // whereas enabledProviders may change over time...
        availableProviders = locationManager.getAllProviders();
        // this actually does not make sense since we are not running locationListener all the time....
        // enabledProviders = locationManager.getProviders(true);
        // ...

        // ????
        locationListener = new DefaultLocationListener(context);

    }



    // TBD:
    // ....



    public void checkLastKnownLocation(boolean runInForeground)
    {
        // ????
        String provider;
        if(runInForeground) {
            provider = LocationManager.GPS_PROVIDER;
            // provider = locationManager.getBestProvider(updateCriteria, false);
        } else {
            provider = locationManager.getBestProvider(updateCriteria, true);
        }

        // ???
        boolean lastLocationRelativelyNew = false;
        Location lastLocation = locationManager.getLastKnownLocation(provider);
        if(lastLocation != null) {
            // tbd: check accuracy as well???
            long lastTime = lastLocation.getTime();
            long now = System.currentTimeMillis();
            // tbd: this should depend on the frequency!!!
            final long delta = 5 * 60 * 60 * 1000L;   // five mins.
            if((now - lastTime) < delta) {
                lastLocationRelativelyNew = true;
            }
        }

        // TBD:
        if(lastLocation != null) {
            // tbd:
            // Store it in DB.
            storeRawLocationData(lastLocation);
        }

        // TBD:
        // start one time or recurring location checking
        // based on lastLocationRelativelyNew ???
        if(lastLocationRelativelyNew == true) {


        }
        // ...

    }


    public void doOneTimeLocationCheck(boolean runInForeground)
    {
        if(trackingEnabled == false) {
            Log.w("Location tracking is not enabled.");
            return;
        }

        // TBD:
        // Check the GPS status and
        // if the gps is not currently enabled,
        //    then do not use FINE accuracy, just for one time location check...
        // <-- Does this make sense???
        // ..

        String provider;
        if(runInForeground) {
            provider = locationManager.getBestProvider(updateCriteria, false);
//            if(locationAccuracy == LocationAccuracy.HIGH) {
//                provider = LocationManager.GPS_PROVIDER;
//            } else {
//                provider = LocationManager.NETWORK_PROVIDER;
//            }
        } else {
            provider = locationManager.getBestProvider(updateCriteria, true);
//            provider = LocationManager.PASSIVE_PROVIDER;
        }


    }

    // TBD:
    // Make it thread safe????
    public void startLocationTracking(boolean runInForeground)
    {
        startLocationTracking(runInForeground, false);
    }
    public void startLocationTracking(boolean runInForeground, boolean runContinuously)
    {
        if(trackingEnabled == false) {
            Log.w("Location tracking is not enabled.");
            return;
        }

        if(runInForeground) {
            if(trackingInBackground) {
                // TBD:
                // Stop the currently running background tracking.
                // ...
            }
            if(trackingInForeground) {
                // TBD:
                // Just let the current run continue???
                // ...
            }
        } else {
            if(trackingInForeground) {
                // Do not start background tracking...
            }
            if(trackingInBackground) {
                // TBD:
                // Just let the current run continue???
                // ...
            }
        }

        String trackingCronExpression = TrackingFrequency.getUpdateCronExpression(trackingFrequency, runInForeground);
        long defaultTrackingDuration = TrackingFrequency.getUpdateDuration(trackingFrequency, runInForeground);
        long minUpdateMillis = TrackingFrequency.getMinUpdateMillis(trackingFrequency, runInForeground);

//        String provider;
//        if(runInForeground) {
//            provider = locationManager.getBestProvider(updateCriteria, false);
////            if(locationAccuracy == LocationAccuracy.HIGH) {
////                provider = LocationManager.GPS_PROVIDER;
////            } else {
////                provider = LocationManager.NETWORK_PROVIDER;
////            }
//        } else {
//            provider = locationManager.getBestProvider(updateCriteria, true);
////            provider = LocationManager.PASSIVE_PROVIDER;
//        }



        // TBD:
//        locationManager.requestLocationUpdates(provider, minUpdateMillis, 0.0f, locationListener);

        if(runInForeground) {
            for(String p : availableProviders) {
                locationManager.requestLocationUpdates(p, minUpdateMillis, 0.0f, locationListener);
            }
        } else {
            for(String p : locationManager.getProviders(true)) {
                locationManager.requestLocationUpdates(p, minUpdateMillis, 0.0f, locationListener);
            }
        }





        // Reset the flags...
        if(runInForeground) {
            trackingInForeground = true;
            trackingInBackground = false;
        } else {
            trackingInForeground = false;
            trackingInBackground = true;
        }

    }

    public void stopLocationTracking()
    {
        // tbd:
        // foreground vs. background???


        locationManager.removeUpdates(locationListener);


        // Reset the flags...
        trackingInForeground = false;
        trackingInBackground = false;
    }


    // ???
    public void storeRawLocationData(Location location)
    {
        Log.w("LocationTrackingHelper.storeRawLocationData() called with location = " + location);

        // tbd
        LocationDataHelper.getInstance(context).storeLocation(location);


        // TBD:
        // Do this in a separate thread???
        // ...
        processLocationData();
        // ...
    }

    // ???
    private void processLocationData()
    {
        // ?????

        // tbd
        // ..


        // tbd: how often are we to upload data????
        uploadLocationData();
    }
    // ???
    private void uploadLocationData()
    {
        // ?????

    }




    private static final class DefaultLocationListener implements LocationListener
    {
        private final Context context;
        private DefaultLocationListener(Context context)
        {
            this.context = context;
        }

        @Override
        public void onLocationChanged(Location location)
        {
            if(Log.I) Log.i("onLocationChanged() location = " + location);

            LocationTrackingHelper.getInstance(context).storeRawLocationData(location);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras)
        {
            // what to do???
            if(Log.I) Log.i("onStatusChanged() provider = " + provider + "; status = " + status);
        }

        @Override
        public void onProviderEnabled(String provider)
        {
            // what to do???
            if(Log.I) Log.i("onProviderEnabled() provider = " + provider);
        }

        @Override
        public void onProviderDisabled(String provider)
        {
            // what to do???
            if(Log.I) Log.i("onProviderDisabled() provider = " + provider);
        }
    }

}

package ms.airy.lib.core.status.client.task;

import android.os.AsyncTask;

import ms.airy.lib.core.status.client.StatusMessageFetchClient;
import ms.airy.lib.core.status.client.factory.StatusClientFactory;
import ms.airy.lib.core.status.common.StatusMessage;

import java.util.List;


/**
 */
public class AsyncStatusFetchNewMessagesTask extends AsyncTask<Void, Void, List<Long>>
{
    private final AsyncTaskCallback callback;
    public AsyncStatusFetchNewMessagesTask(AsyncTaskCallback callback)
    {
        this.callback = callback;
    }


    // Store input params during processing


    @Override
    protected List<Long> doInBackground(Void... params)
    {
        StatusMessageFetchClient client = StatusClientFactory.getInstance().getFetchClient();

        List<Long> ids = client.fetchNewMessages();

        return ids;
    }

    @Override
    protected void onPostExecute(List<Long> ids)
    {
        // super.onPostExecute(aLong);

        this.callback.onFetchNewMessages(ids);
    }



    // Callback interface
    public static interface AsyncTaskCallback
    {
        void onFetchNewMessages(List<Long> ids);
    }

}

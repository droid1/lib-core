package ms.airy.lib.core.status.client.task;

import android.os.AsyncTask;

import ms.airy.lib.core.status.client.StatusMessageSendClient;
import ms.airy.lib.core.status.client.factory.StatusClientFactory;
import ms.airy.lib.core.status.common.StatusMessage;


/**
 */
public class AsyncStatusSendStatusMessageTask extends AsyncTask<StatusMessage, Void, Void>
{
    private final AsyncTaskCallback callback;
    public AsyncStatusSendStatusMessageTask(AsyncTaskCallback callback)
    {
        this.callback = callback;
    }

    // Store input params during processing
    private StatusMessage statusMessage;


    @Override
    protected Void doInBackground(StatusMessage... params)
    {
        statusMessage = (StatusMessage) params[0];

        StatusMessageSendClient client = StatusClientFactory.getInstance().getSendClient();

        client.sendStatusMessage(statusMessage);

        return null;
    }

    @Override
    protected void onPostExecute(Void result)
    {
        // super.onPostExecute(aLong);

        this.callback.onSendStatusMessage(statusMessage);
    }




    // Callback interface
    public static interface AsyncTaskCallback
    {
        void onSendStatusMessage(StatusMessage statusMessage);
    }

}

package ms.airy.lib.core.dialog.base;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;


// http://developer.android.com/guide/topics/ui/dialogs.html
public class BasicItemListDialogFragment extends BaseDialogFragment
{
    // Note that title/itemsArray takes precedence over titleResId/itemsResId (e.g, if both are specified)
    //     (althought the current API does not allow specifying both at the moment...)
    protected static final String ARG_MULTIPLE_CHOICE = "itemlist_dialog_multiple_choice";
    protected static final String ARG_ITEMS_RESID = "itemlist_dialog_items_resid";
    protected static final String ARG_ITEMS_ARRAY = "itemlist_dialog_items_array";
    protected static final String ARG_SELECTED_ITEMS = "itemlist_dialog_selected_items";


    // temporary
    private boolean multipleChoice;


    // TBD:
    // Why CharSequence[] ???
    // Just use String[] ????
    // ...


    public static BasicItemListDialogFragment newSingleChoiceInstance(CharSequence title, CharSequence[] itemsArray)
    {
        return newInstance(title, false, itemsArray);
    }
    public static BasicItemListDialogFragment newMultipleChoiceInstance(CharSequence title, CharSequence[] itemsArray, boolean[] selectedItems)
    {
        return newInstance(title, true, itemsArray, selectedItems);
    }
    public static BasicItemListDialogFragment newInstance(CharSequence title, boolean multipleChoice, CharSequence[] itemsArray)
    {
        return newInstance(title, multipleChoice, itemsArray, null);
    }

    public static BasicItemListDialogFragment newSingleChoiceInstance(int titleResId, int itemsResId)
    {
        return newInstance(titleResId, false, itemsResId);
    }
    public static BasicItemListDialogFragment newMultipleChoiceInstance(int titleResId, int itemsResId, boolean[] selectedItems)
    {
        return newInstance(titleResId, true, itemsResId, selectedItems);
    }
    public static BasicItemListDialogFragment newInstance(int titleResId, boolean multipleChoice, int itemsResId)
    {
        return newInstance(titleResId, multipleChoice, itemsResId, null);
    }


    public static BasicItemListDialogFragment newInstance(CharSequence title, boolean multipleChoice, CharSequence[] itemsArray, boolean[] selectedItems)
    {
        BasicItemListDialogFragment fragment = new BasicItemListDialogFragment();
        addArguments(fragment, title, multipleChoice, itemsArray, selectedItems);
        return fragment;
    }
    public static BasicItemListDialogFragment newInstance(int titleResId, boolean multipleChoice, int itemsResId, boolean[] selectedItems)
    {
        BasicItemListDialogFragment fragment = new BasicItemListDialogFragment();
        addArguments(fragment, titleResId, multipleChoice, itemsResId, selectedItems);
        return fragment;
    }
    public BasicItemListDialogFragment()
    {
        // Default false?
        multipleChoice = false;
    }

    protected static DialogFragment addArguments(DialogFragment fragment, CharSequence title, boolean multipleChoice, CharSequence[] itemsArray, boolean[] selectedItems)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        if(title != null && title.length() > 0) {
            args.putCharSequence(ARG_TITLE, title);
        }
        args.putBoolean(ARG_MULTIPLE_CHOICE, multipleChoice);
        if(itemsArray != null) {
            args.putCharSequenceArray(ARG_ITEMS_ARRAY, itemsArray);
        }
        if(selectedItems != null) {
            args.putBooleanArray(ARG_SELECTED_ITEMS, selectedItems);
        }
        return fragment;
    }
    protected static DialogFragment addArguments(DialogFragment fragment, int titleResId, boolean multipleChoice, int itemsResId, boolean[] selectedItems)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        if(titleResId > 0) {  // ???
            args.putInt(ARG_TITLE_RESID, titleResId);
        }
        args.putBoolean(ARG_MULTIPLE_CHOICE, multipleChoice);
        if(itemsResId > 0) {
            args.putInt(ARG_ITEMS_RESID, itemsResId);
        }
        if(selectedItems != null) {
            args.putBooleanArray(ARG_SELECTED_ITEMS, selectedItems);
        }
        return fragment;
    }


    protected boolean isMultipleChoice()
    {
        return multipleChoice;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        AlertDialog.Builder dialogBuilder = buildDialogBuilder();
        return dialogBuilder.create();
    }

    @Override
    protected AlertDialog.Builder buildDialogBuilder()
    {
        Bundle args = getArguments();
        if(args == null) {
            // ????
        }
        CharSequence title = args.getCharSequence(ARG_TITLE);
        int titleResId = args.getInt(ARG_TITLE_RESID);
        multipleChoice = args.getBoolean(ARG_MULTIPLE_CHOICE);
//        // boolean itemsInRes = args.containsKey(ARG_ITEMS_RESID) && (args.getInt(ARG_ITEMS_RESID) > 0);
//        CharSequence[] itemsArray = args.getCharSequenceArray(ARG_ITEMS_ARRAY);
//        int itemsResId = args.getInt(ARG_ITEMS_RESID);
//        boolean[] selectedItems = args.getBooleanArray(ARG_SELECTED_ITEMS);

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());

        dialogBuilder.setIcon(android.R.drawable.alert_dark_frame);
        if(title != null && title.length() > 0) {
            dialogBuilder.setTitle(title);
        } else {
            if (titleResId > 0) {  // ???
                dialogBuilder.setTitle(titleResId);
            }
        }

//        if(multipleChoice) {
//            DialogInterface.OnMultiChoiceClickListener onMultiChoiceClickListener =
//                    new DialogInterface.OnMultiChoiceClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which, boolean isChecked)
//                        {
//                            ((ActionableChoiceDialog) getActivity()).onDialogItemClick(BasicItemListDialogFragment.this.getTag(), which, isChecked);
//                        }
//                    };
//            if(itemsArray != null) {
//                dialogBuilder.setMultiChoiceItems(itemsArray, selectedItems, onMultiChoiceClickListener);
//            } else {
//                if(itemsResId > 0) {
//                    dialogBuilder.setMultiChoiceItems(itemsResId, selectedItems, onMultiChoiceClickListener);
//                }
//            }
//        } else {
//            DialogInterface.OnClickListener onSingleChoiceClickListener =
//                    new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which)
//                        {
//                            ((ActionableChoiceDialog) getActivity()).onDialogItemClick(BasicItemListDialogFragment.this.getTag(), which, true);
//                        }
//                    };
//            if(itemsArray != null) {
//                dialogBuilder.setItems(itemsArray, onSingleChoiceClickListener);
//            } else {
//                if(itemsResId > 0) {
//                    dialogBuilder.setItems(itemsResId, onSingleChoiceClickListener);
//                }
//            }
//        }
//
//        // Single-choice list does not require "OK".
//        if(multipleChoice) {
//            dialogBuilder.setPositiveButton(R.string.basic_itemlist_dialog_ok,
//                    new DialogInterface.OnClickListener()
//                    {
//                        public void onClick(DialogInterface dialog, int whichButton)
//                        {
//                            ((ActionableChoiceDialog) getActivity()).onPositiveClick(BasicItemListDialogFragment.this.getTag());
//                        }
//                    }
//            );
//        }
//        dialogBuilder.setNegativeButton(R.string.basic_itemlist_dialog_cancel,
//                new DialogInterface.OnClickListener()
//                {
//                    public void onClick(DialogInterface dialog, int whichButton)
//                    {
//                        ((ActionableChoiceDialog) getActivity()).onNegativeClick(BasicItemListDialogFragment.this.getTag());
//                    }
//                }
//        );

        return dialogBuilder;
    }


}

package ms.airy.lib.core.config.cloud;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;
import java.util.Map;

import ms.airy.lib.core.config.AbstractConfigManager;
import ms.airy.lib.core.config.AbstractReadWriteConfigManager;
import ms.airy.lib.core.config.client.AsyncConfigCallback;
import ms.airy.lib.core.config.client.AsyncConfigClient;
import ms.airy.lib.core.config.client.async.AsyncOnlineConfigClient;


// Although we mostly use this as read-only config (only for "global" configs),
// it's still a read-write config.
public class CloudConfigManager extends AbstractReadWriteConfigManager implements AsyncConfigCallback
{
    // REST client.
    private AsyncConfigClient client = null;

    // "Cache"
    // Note that the cache can become stale
    //    because the remote service data might be updated without using this ConfigManager object.
    private final Map<String, Object> propsMap = new HashMap<String, Object>();

    public CloudConfigManager(Context context)
    {
        super(context);
    }

    public AsyncConfigClient getClient()
    {
        if(client == null) {
            client = new AsyncOnlineConfigClient(this);
        }
        return client;
    }

    @Override
    public Object get(String key)
    {
        // We do not allow null value entry.
        if(propsMap.get(key) != null) {
            return propsMap.get(key);
        }
        // async call
        getClient().getProperty(key);
        // return null, for now.
        // next time get(key) is called, the cache will likely have been filled in with a new value.
        return null;
    }

    @Override
    public Object get(String key, Object defVal)
    {
        Object val = get(key);
        if(val != null) {
            return val;
        } else {
            return defVal;
        }
    }

    @Override
    public String getString(String key)
    {
        String val = null;
        Object obj = get(key);
        if(obj != null && obj instanceof String) {  // TBD: Or, use toString() ???
            val = (String) obj;
        }
        return val;
    }

    @Override
    public String getString(String key, String defVal)
    {
        String val = getString(key);
        if(val != null) {
            return val;
        } else {
            return defVal;
        }
    }

    @Override
    public boolean set(String key, Object val)
    {
        // Cache the value now?
        // Or, wait until the callback???
        getClient().setProperty(key, val);
        // Which to return, true or false?
        return false;
    }

    @Override
    public boolean setString(String key, String val)
    {
        return set(key, val);
    }


    //////////////////////////////////////////////////////
    // AsyncConfigCallback interface

    @Override
    public void onGetProperty(Object value, String key)
    {
        // We ignore the null value.
        // TBD: should we allow null value ????
        if(value != null) {
            if(Log.I) Log.i("CloudConfigManager.onGetProperty(). key = " + key + "; value = " + value);
            propsMap.put(key, value);
        } else {
            // ???
            Log.w("CloudConfigManager.onGetProperty(). Null value returned for key = " + key);
        }
        // Heck??? to save memory? (This is not thread-safe!!!)
        client = null;
    }

    @Override
    public void onSetProperty(boolean result, String key, Object value)
    {
        if(result == true) {
            if(Log.I) Log.i("CloudConfigManager.onSetProperty(). key = " + key + "; value = " + value);
            propsMap.put(key, value);
        } else {
            // ???
            Log.w("CloudConfigManager.onSetProperty(). Failed for key = " + key);
        }
        // Heck??? to save memory? (This is not thread-safe!!!)
        client = null;
    }

}


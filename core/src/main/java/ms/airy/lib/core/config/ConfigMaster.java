package ms.airy.lib.core.config;

import android.content.Context;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ms.airy.lib.core.config.cloud.CloudConfigManager;
import ms.airy.lib.core.config.common.ConfigType;
import ms.airy.lib.core.config.json.JsonConfigManager;
import ms.airy.lib.core.config.prefs.PrefsConfigManager;
import ms.airy.lib.core.config.props.PropertiesConfigManager;
import ms.airy.lib.core.config.res.ResourceConfigManager;


public class ConfigMaster
{
    // "Convention over config"
    // These configs are needed to "bootstrap" configs.
    public static final String RES_KEY_CONFIG_REMOTE_ENABLED = "config.remote.enabled";
    public static final String RES_KEY_CONFIG_TYPES_ENABLED = "config.types.enabled";
    public static final String RES_KEY_CONFIG_FILE_JSON = "config.file.json";
    public static final String RES_KEY_CONFIG_FILE_PROPS = "config.file.props";
    // File under /assets dir. Default, if RES_KEY_CONFIG_FILE_JSON is not set.
    public static final String RES_FILE_NAME_JSON = "app_config.json";
    // Under /assets dir. Default, if RES_KEY_CONFIG_FILE_PROPS is not set.
    public static final String RES_FILE_NAME_PROPS = "app_config.properties";
    // ...

    private static ConfigMaster INSTANCE = null;
    public static ConfigMaster getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new ConfigMaster(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;

    // temporary
    private final boolean remoteConfigEnabled;
    private final int configTypeMask;
    private CloudConfigManager cloudConfigManager = null;
    private PrefsConfigManager prefsConfigManager = null;
    private ResourceConfigManager resourceConfigManager = null;
    private JsonConfigManager jsonConfigManager = null;
    private PropertiesConfigManager propertiesConfigManager = null;


    private ConfigMaster(Context context)
    {
        this.context = context;

        int resIdRemoteEnabled = context.getResources().getIdentifier(RES_KEY_CONFIG_REMOTE_ENABLED, "boolean", context.getPackageName());
        if(resIdRemoteEnabled > 0) {
            this.remoteConfigEnabled = context.getResources().getBoolean(resIdRemoteEnabled);
        } else {
            this.remoteConfigEnabled = true;  // ???
        }
        int resIdConfigTypes = context.getResources().getIdentifier(RES_KEY_CONFIG_TYPES_ENABLED, "integer", context.getPackageName());
        if(resIdConfigTypes > 0) {
            this.configTypeMask = context.getResources().getInteger(resIdConfigTypes);
        } else {
            this.configTypeMask = ConfigType.TYPE_PREFS | ConfigType.TYPE_RES | ConfigType.TYPE_JSON | ConfigType.TYPE_PROPS;
        }

        if(this.remoteConfigEnabled) {
            cloudConfigManager = new CloudConfigManager(context);
        }
        if(ConfigType.isPrefsConfigEnabled(this.configTypeMask)) {
            // Singleton??
            prefsConfigManager = PrefsConfigManager.getInstance(context);
        }
        if(ConfigType.isResourceConfigEnabled(this.configTypeMask)) {
            resourceConfigManager = new ResourceConfigManager(context);
        }
        if(ConfigType.isJsonConfigEnabled(configTypeMask)) {
            int resIdJsonFile = context.getResources().getIdentifier(RES_KEY_CONFIG_FILE_JSON, "string", context.getPackageName());
            if(resIdJsonFile > 0) {
                jsonConfigManager = new JsonConfigManager(context, resIdJsonFile);
            } else {
                jsonConfigManager = new JsonConfigManager(context, RES_FILE_NAME_JSON);
            }
        }
        if(ConfigType.isPropertiesConfigEnabled(configTypeMask)) {
            int resIdPropsFile = context.getResources().getIdentifier(RES_KEY_CONFIG_FILE_PROPS, "string", context.getPackageName());
            if(resIdPropsFile > 0) {
                propertiesConfigManager = new PropertiesConfigManager(context, resIdPropsFile);
            } else {
                propertiesConfigManager = new PropertiesConfigManager(context, RES_FILE_NAME_PROPS);
            }
        }
    }


//    public boolean isRemoteConfigEnabled()
//    {
//        return remoteConfigEnabled;
//    }
    public boolean isCloudConfigEnabled()
    {
        return (cloudConfigManager != null);
    }

//    protected int getConfigTypeMask()
//    {
//        return configTypeMask;
//    }
    public boolean isPrefsConfigEnabled()
    {
        return (prefsConfigManager != null);
    }
    public boolean isResourceConfigEnabled()
    {
        return (resourceConfigManager != null);
    }
    public boolean isJsonConfigEnabled()
    {
        return (jsonConfigManager != null);
    }
    public boolean isPropertiesConfigEnabled()
    {
        return (propertiesConfigManager != null);
    }


    // "cache"
    // Note that because of the way we implement these config classes,
    //     we cannot have an entry with null value.
    // It's hard to be consistent in all different classes, but
    // Null value may be considered to be equivalent with not having the entry...
    // --> Need to check this... tbd.
    private final Map<String, Object> propertyMap = new HashMap<String, Object>();


    public Object getProperty(String key)
    {
        if(propertyMap.containsKey(key)) {
            // This could be null. ???
            return propertyMap.get(key);
        }

        // [1] remote first...
        // Remote server can be used only for global/read-only configs
        // because it's checked first.
        if(cloudConfigManager != null) {
            Object obj = cloudConfigManager.get(key);
            if(obj != null) {
                propertyMap.put(key, obj);
                return obj;
            }
        }

        // [2] then shared preferences,
        //     which is the only read-write config.
        if(prefsConfigManager != null) {
            Object obj = prefsConfigManager.get(key);
            if(obj != null) {
                propertyMap.put(key, obj);
                return obj;
            }
        }


        // [*] TBD:
        // User-specific read-write remote config???
        // ....


        // [3] Then local/read-only.
        // Note the order.

        if(resourceConfigManager != null) {
            Object obj = resourceConfigManager.get(key);

            // Temporary
            // Log.w(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> key = " + key + "; val = " + obj);

            if(obj != null) {
                propertyMap.put(key, obj);
                return obj;
            }
        }

        if(jsonConfigManager != null) {
            Object obj = jsonConfigManager.get(key);
            if(obj != null) {
                propertyMap.put(key, obj);
                return obj;
            }
        }

        if(propertiesConfigManager != null) {
            Object obj = propertiesConfigManager.get(key);
            if(obj != null) {
                propertyMap.put(key, obj);
                return obj;
            }
        }

        return null;
    }

    public boolean setProperty(String key, Object value)
    {
        // Remote configs can be read-write in theory,
        // But, since we use it as the high priority global config,
        //      we can only use it as read-only config.
        // The only read-write config is
        // shared preferences, for now.

        boolean suc = prefsConfigManager.set(key, value);
        if(suc) {
            propertyMap.put(key, value);
            return true;
        } else {
            // ????
        }

        // etc. ....

        return false;
    }



    // Convenience methods.

    public String getStringProperty(String key)
    {
        String val = null;
        Object obj = getProperty(key);
        if(obj instanceof String) {
            val = (String) obj;
        }
        return val;
    }

    public Boolean getBooleanProperty(String key)
    {
        Boolean val = null;
        Object obj = getProperty(key);

        // Temporary
        // Log.w(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> key = " + key + "; val = " + obj);

        if(obj instanceof Boolean) {
            val = (Boolean) obj;
        }
        return val;
    }

    public Integer getIntProperty(String key)
    {
        Integer val = null;
        Object obj = getProperty(key);
        if(obj instanceof Integer) {
            val = (Integer) obj;
        }
        return val;
    }

    public Long getLongProperty(String key)
    {
        Long val = null;
        Object obj = getProperty(key);
        if(obj instanceof Long) {
            val = (Long) obj;
        } else if(obj instanceof Integer) {
            // ResConfig, for example, does not have <long> resource type.
            // Long values are stored in <integer> as well.
            val = (long) (int) (Integer) obj;   // what ????
        }
        return val;
    }

    public Float getFloatProperty(String key)
    {
        Float val = null;
        Object obj = getProperty(key);
        if(obj instanceof Float) {
            val = (Float) obj;
        } else if(obj instanceof Double) {
            // ?????
            // We consider float and double types equivalent for the purposes of config.
            // (I think..., No config managers currently support double types, though...)
            val = (float) (double) (Double) obj;
        }
        return val;
    }


}

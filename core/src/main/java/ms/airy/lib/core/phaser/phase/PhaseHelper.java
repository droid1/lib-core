package ms.airy.lib.core.phaser.phase;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import ms.airy.lib.core.phaser.common.Feature;
import ms.airy.lib.core.phaser.common.Phase;
import ms.airy.lib.core.phaser.feature.FeatureHelper;


// TBD:
// FeatureMap data should be stored to/from persistent storage such as config.
// Just using a singleton may not be sufficient.....
public final class PhaseHelper
{
    // Map of feature Id -> { min phase (inclusive), max (exclusive) }.
    // Min cannot be null, whereas max can be null, meaning there is no max.
    // FeatureMap is open ended. That is,
    // Features not in this map are supported for all phases >= minphase = 0;
    private final Map<String, Phase.Range> featureMap;

    public PhaseHelper()
    {
        featureMap = new HashMap<String, Phase.Range>();
        init();
    }
    private void init()
    {
        // TBD:
        // Populate the featureMap....
        // ....

//        Set<Feature> features = FeatureHelper.getInstance().getFeatures();
//        if(features != null && !features.isEmpty()) {
//            for(Feature f : features) {
//                featureMap.put(f.getCode(), new Phase.Range(f.getMinPhase(), f.getMaxPhase()));
//            }
//        }
    }


    // Singleton
    private static final class PhaseHelperHolder
    {
        private static final PhaseHelper INSTANCE = new PhaseHelper();
    }
    public static PhaseHelper getInstance()
    {
        return PhaseHelperHolder.INSTANCE;
    }


    public void addFeature(String featureCode, Integer minPhase, Integer maxPhase)
    {
        featureMap.put(featureCode, new Phase.Range(minPhase, maxPhase));
    }

    // Inclusive.
    public int getMinPhase(String featureCode)
    {
        if(! featureMap.containsKey(featureCode)) {
            return Phase.P01;
        } else {
            return featureMap.get(featureCode).getMin();
        }
    }
    // Exclusive. ????
    public int getMaxPhase(String featureCode)
    {
        if(! featureMap.containsKey(featureCode)) {
            return Phase.FINAL;
        } else {
            Integer max = featureMap.get(featureCode).getMax();
            if(max == null) {
                return Phase.FINAL;
            } else {
                return max;
            }
        }
    }
    public boolean hasMaxPhase(String featureCode)
    {
        if(featureMap.containsKey(featureCode)) {
            if(featureMap.get(featureCode).getMax() != null) {
                return true;
            }
        }
        return false;
    }

    public boolean isFeatureAvailable(String featureCode, int phase)
    {
        if(! featureMap.containsKey(featureCode)) {
            return true;
        }
        return (getMinPhase(featureCode) <= phase && getMaxPhase(featureCode) > phase);
    }


}

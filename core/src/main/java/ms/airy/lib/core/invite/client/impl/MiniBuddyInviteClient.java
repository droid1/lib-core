package ms.airy.lib.core.invite.client.impl;

import org.miniclient.ApiUserClient;
import org.miniclient.RestApiException;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ms.airy.lib.core.invite.client.BuddyInviteClient;
import ms.airy.lib.core.invite.common.InviteInfo;
import ms.airy.lib.core.invite.common.RecipientInfo;
import ms.airy.lib.core.util.IdUtil;


public class MiniBuddyInviteClient extends AbstractBuddyInviteClient implements BuddyInviteClient
{
    private ApiUserClient miniInviteWebClient = null;
    private ApiUserClient miniRecipientWebClient = null;

    public MiniBuddyInviteClient()
    {
        this((String) null, (String) null);   // ???
    }
    public MiniBuddyInviteClient(String inviteWebServiceEndpoint, String recipientWebServiceEndpoint)
    {
        super(inviteWebServiceEndpoint);
        this.miniInviteWebClient = MiniClientBuilder.getInstance().getInviteWebClient(inviteWebServiceEndpoint);
        this.miniRecipientWebClient = MiniClientBuilder.getInstance().getRecipientWebClient(recipientWebServiceEndpoint);
    }
    public MiniBuddyInviteClient(ApiUserClient miniInviteWebClient, ApiUserClient miniRecipientWebClient)
    {
        super(miniInviteWebClient.getResourceBaseUrl());
        this.miniInviteWebClient = miniInviteWebClient;
        this.miniRecipientWebClient = miniRecipientWebClient;
    }

    public ApiUserClient getMiniInviteWebClient()
    {
        return miniInviteWebClient;
    }
    protected void setMiniInviteWebClient(String inviteWebServiceEndpoint)
    {
        super.setInviteWebServiceEndpoint(inviteWebServiceEndpoint);
        this.miniInviteWebClient = MiniClientBuilder.getInstance().getInviteWebClient(inviteWebServiceEndpoint);
    }
    protected void setMiniInviteWebClient(ApiUserClient miniInviteWebClient)
    {
        super.setInviteWebServiceEndpoint(miniInviteWebClient.getResourceBaseUrl());
        this.miniInviteWebClient = miniInviteWebClient;
    }

    public ApiUserClient getMiniRecipientWebClient()
    {
        return miniRecipientWebClient;
    }
    protected void setMiniRecipientWebClient(String recipientWebServiceEndpoint)
    {
        this.miniRecipientWebClient = MiniClientBuilder.getInstance().getRecipientWebClient(recipientWebServiceEndpoint);
    }
    protected void setMiniRecipientWebClient(ApiUserClient miniRecipientWebClient)
    {
        this.miniRecipientWebClient = miniRecipientWebClient;
    }


    @Override
    protected void setInviteWebServiceEndpoint(String inviteWebServiceEndpoint)
    {
        super.setInviteWebServiceEndpoint(inviteWebServiceEndpoint);
        this.miniInviteWebClient = MiniClientBuilder.getInstance().getInviteWebClient(inviteWebServiceEndpoint);
    }

    protected void setRecipientWebServiceEndpoint(String recipientWebServiceEndpoint)
    {
        this.miniRecipientWebClient = MiniClientBuilder.getInstance().getRecipientWebClient(recipientWebServiceEndpoint);
    }


    @Override
    public long sendInvite(String recipient)
    {
        if(Log.I) Log.i("MiniBuddyInviteClient.sendInvite(). recipient = " + recipient);

        long id = IdUtil.generateRandomId();
        try {
            // TBD:
            long inviteTime = System.currentTimeMillis();
            InviteInfo inviteInfo = new InviteInfo(id, recipient, inviteTime);
            inviteInfo = (InviteInfo) miniInviteWebClient.create(inviteInfo);
            // ...
        } catch (RestApiException e) {
            Log.e("Failed to send invite to " + recipient, e);
        } catch (IOException e) {
            Log.e("IO Exception while sending invite to " + recipient, e);
        }
        return id;
    }

    @Override
    public InviteInfo getInviteInfo(long inviteId)
    {
        if(Log.I) Log.i("MiniBuddyInviteClient.getInviteInfo(). inviteId = " + inviteId);

        InviteInfo inviteInfo = null;
        try {
            // TBD:
            Map<String,Object> params = new HashMap<String, Object>();
            params.put("inviteId", inviteId);  // ???
            List<Object> objs = miniInviteWebClient.list(params);
            if(objs != null && objs.size() > 0) {
                // ???
                inviteInfo = (InviteInfo) objs.get(0);
            }
        } catch (RestApiException e) {
            Log.e("Failed to get invite for " + inviteId, e);
        } catch (IOException e) {
            Log.e("IO Exception while fetching invite for " + inviteId, e);
        }

        return inviteInfo;
    }

    @Override
    public List<Long> getInvites(int status)
    {
        if(Log.I) Log.i("MiniBuddyInviteClient.getInvites(). status = " + status);

        Object result = null;
        try {
            // TBD:
            Map<String,Object> params = new HashMap<String, Object>();
            params.put("status", status);  // ???
            List<Object> objs = miniInviteWebClient.list(params);
            if(objs != null && objs.size() > 0) {
                // ....
            }
        } catch (RestApiException e) {
            Log.e("Failed to get invites", e);
        } catch (IOException e) {
            Log.e("IO Exception while fetching invites", e);
        }

        List<Long> ids = null;
        if(result != null) {
            // TBD
            // ...
        }

        return ids;
    }

    @Override
    public RecipientInfo getRecipientInfo(String recipient)
    {
        if(Log.I) Log.i("MiniBuddyInviteClient.getRecipientInfo(). recipient = " + recipient);

        RecipientInfo recipientInfo = null;
        try {
            // TBD:
            Map<String,Object> params = new HashMap<String, Object>();
            params.put("recipient", recipient);  // ???
            List<Object> objs = miniRecipientWebClient.list(params);
            if(objs != null && objs.size() > 0) {
                // ???
                recipientInfo = (RecipientInfo) objs.get(0);
            }
        } catch (RestApiException e) {
            Log.e("Failed to get recipient for " + recipient, e);
        } catch (IOException e) {
            Log.e("IO Exception while fetching recipient for " + recipient, e);
        }

        return recipientInfo;
    }

    @Override
    public List<String> getRecipients(int status)
    {
        if(Log.I) Log.i("MiniBuddyInviteClient.getRecipients(). status = " + status);

        Object result = null;
        try {
            // TBD:
            Map<String,Object> params = new HashMap<String, Object>();
            params.put("status", status);  // ???
            List<Object> objs = miniRecipientWebClient.list(params);
            if(objs != null && objs.size() > 0) {
                // ....
            }
        } catch (RestApiException e) {
            Log.e("Failed to get recipients", e);
        } catch (IOException e) {
            Log.e("IO Exception while fetching recipients", e);
        }

        List<String> recipients = null;
        if(result != null) {
            // TBD
            // ...
        }

        return recipients;
    }

}

package ms.airy.lib.core.time.timer;

import android.os.CountDownTimer;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


/**
 * FlexibleCountDownTimer is just a wrapper around CountDownTimer.
*/
public class FlexibleCountDownTimer implements Serializable
{
    public static final long DEFAULT_COUNTDOWN_INTERVAL = 100L;

    // This TimerRange should use millisecond values for max/min, etc...
    private final TimerRange range;
    private final long delta;     // count-down interval in millis.
    private CountDownTimer timer;
    private long extent;    // remaining time. millisUntilFinished.

    public FlexibleCountDownTimer(TimerRange range)
    {
        this(range, DEFAULT_COUNTDOWN_INTERVAL);
    }
    public FlexibleCountDownTimer(TimerRange range, long delta)
    {
        this(range, delta, null);
    }
    public FlexibleCountDownTimer(TimerRange range, long delta, CountDownListener countDownListener)
    {
        // TBD: range cannot be null.
        this.range = range;
        this.delta = delta;
        this.timer = null;    // ???
        // extent = range.getDuration();
        if(countDownListener != null) {
            addCountDownListener(countDownListener);
        }
    }


    public final TimerRange getRange()
    {
        return range;
    }

    public final long getDelta()
    {
        return delta;
    }
//    public final void setDelta(long delta)
//    {
//        // This does not affect the already created timer...
//        this.delta = delta;
//    }

    public final long getExtent()
    {
        return extent;
    }


    public void start()
    {
        if(timer != null) {
            Log.w("Timer already exists. Cannot start another one.");
            return;
        }
        // synchronized (range) {
            range.setVal(range.getMax());
        // }
        extent = range.getDuration();
        startTimer(extent);
    }
    public void restart()
    {
        timer = null;  // ???
        start();
    }
    public void reset()
    {
        timer = null;  // ???
    }

    private void createTimer(long duration)
    {
        // Looks like I misunderstood the arg, millisInFuture...
        // long millisInFuture = System.currentTimeMillis() + duration;  --> Wrong.
        long millisInFuture = duration;   // Correct.
        // ...
        synchronized (this) {
            timer = new CountDownTimer(millisInFuture, delta)
            {
                @Override
                public void onTick(long millisUntilFinished)
                {
                    if (Log.D) Log.d("onTick(): millisUntilFinished = " + millisUntilFinished);
                    extent = millisUntilFinished;
                    // synchronized (range) {
                    range.setVal(range.getMin() + extent);
                    // }
                    for(CountDownListener listener : listeners) {
                        listener.onTick(millisUntilFinished);
                    }
                }
                @Override
                public void onFinish()
                {
                    if (Log.D) Log.d("onFinish(): now = " + System.currentTimeMillis());
                    finish();   // ???
                    for(CountDownListener listener : listeners) {
                        listener.onFinish();
                    }
                }
            };
        }
    }
    private void startTimer(long duration)
    {
        synchronized (this) {
            // if(timer == null) {
                createTimer(duration);
                timer.start();
            // } else {
            //     Log.w("Timer already exists. Cannot startCycle another one.");
            // }
        }
    }

    public void pause()
    {
        pause(-1L);
    }
    public void pause(long extent)
    {
        if(timer != null) {
            if(extent >= 0L) {
                this.extent = extent;
            }
            timer.cancel();
        } else {
            if(extent >= 0L) {
                this.extent = extent;
                createTimer(extent);
                // timer.cancel();
            } else {
                Log.i("Timer has not been started. Cannot pause it.");
            }
        }
    }

    public void resume()
    {
        resume(-1L);
    }
    public void resume(long extent)
    {
        if(timer != null) {
            if(extent >= 0L) {
                this.extent = extent;
            }
            // We just create a new timer with the current extent value (not the whole duration).
            // timer = null;
            startTimer(this.extent);
        } else {
            if(extent >= 0L) {
                this.extent = extent;
                createTimer(extent);
                // timer.cancel();
            } else {
                Log.i("Timer has not been paused. Cannot resume it.");
            }
        }
    }


    public void stop()
    {
        stopTimer();
    }
    public void finish()
    {
        extent = 0L;
        stopTimer();
    }
    private void stopTimer()
    {
        if(timer != null) {
            synchronized (timer) {
                timer.cancel();
                timer = null;
                // synchronized (range) {
                    range.setVal(range.getMin() + extent);
                // }
            }
        } else {
            Log.i("Timer has not been started. Cannot stopCycle it.");
        }
    }



    // Event listener.
    public static interface CountDownListener
    {
        // long getId();  // We need some kind of id if we are to support removal.
        void onTick(long millisUntilFinished);
        void onFinish();
    }

    private final Set<CountDownListener> listeners = new HashSet<CountDownListener>();

    public void addCountDownListener(CountDownListener countDownListener)
    {
        if(countDownListener != null) {
            listeners.add(countDownListener);
        }
    }
    // No way to remove existing listeners...
    // But, that's ok because a timer is a short-lived object anyway...

}

package ms.airy.lib.core.startup.task;

import ms.airy.lib.core.util.IdUtil;


public abstract class AbstractStartupTask implements StartupTask
{
    private final long id;

    public AbstractStartupTask()
    {
        this(IdUtil.generateRandomId05());
    }
    public AbstractStartupTask(long id)
    {
        this.id = id;
    }

    @Override
    public long getId()
    {
        return id;
    }


    // TBD:
    // ...

}

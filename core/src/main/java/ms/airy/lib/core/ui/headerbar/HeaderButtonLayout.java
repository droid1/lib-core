package ms.airy.lib.core.ui.headerbar;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.LinearLayout;

// http://developer.android.com/guide/topics/ui/custom-components.html
public class HeaderButtonLayout extends LinearLayout
{

    // ....
    private List<HeaderButton> mButtons = null;
    // ....
    
    
    public HeaderButtonLayout(Context context)
    {
        super(context);
        init();
        //...
    }

    public HeaderButtonLayout(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
        //...
    }

    public HeaderButtonLayout(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        init();
        //...
    }

    
    
    private void init()
    {
        if(Log.D) Log.d("init()");

        // TBD
        setOrientation(HORIZONTAL);
        setMinimumHeight(120);  // temporary
        // ...
        
        // ...
        mButtons = new ArrayList<HeaderButton>();
        // ...
        
        // Redo the layout...
        layoutHeaderButtons();
    }
    
    
    // Redo the layout.
    // TBD: Incremental update???
    //      (e.g., when a button is added, it really need not existing buttons, etc.)
    // TBD: Use preferred height to include only X number of buttons.
    private void layoutHeaderButtons()
    {
        if(Log.D) Log.d("layoutHeaderButtons()");

        // [1] Remove all buttons.
        removeAllViews();
        
        // [2] Add them again.
        //     TBD: How do we know how many we can add/????
        //LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(getLayoutParams());  // ????
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT); // ????
        for(HeaderButton b : mButtons) {
            // TBD: Check if the button is "visible", etc. ?????
            // b.getVisibility()...
            this.addView(b, params);
        }
        
    }

    
    public void addHeaderButton(HeaderButton button)
    {
        if(Log.D) Log.d("addHeaderButton()");
        mButtons.add(button);
        layoutHeaderButtons();        
    }
    public void addHeaderButtons(HeaderButton... buttons)
    {
        if(Log.D) Log.d("addHeaderButtons()");
        
        mButtons.addAll(Arrays.asList(buttons));
//        for(HeaderButton b : buttons) {
//            mButtons.add(b);
//        }
        layoutHeaderButtons();        
    }
    public void addHeaderButton(int location, HeaderButton button)
    {
        if(Log.D) Log.d("addHeaderButton(): location = " + location);
        
        mButtons.add(location, button);
        layoutHeaderButtons();        
    }
    public void addHeaderButtons(int location, HeaderButton... buttons)
    {
        if(Log.D) Log.d("addHeaderButtons(): location = " + location);
        
        mButtons.addAll(location, Arrays.asList(buttons));
//      for(HeaderButton b : buttons) {  // TBD: Need to do it backward....
//          mButtons.add(location, b);
//      }
        layoutHeaderButtons();        
    }
    
    // TBD:
    //public void removeHeaderButton(String tag) {}
    //public void removeHeaderButtons(String... tags) {}
    
    // TBD:
    public void removeAllHeaderButtons()
    {
        mButtons = new ArrayList<HeaderButton>();
        removeAllViews();
    }
    
    
    @Override
    public int getOrientation()
    {
        if(Log.D) Log.d("getOrientation()");

        // TODO Auto-generated method stub
        return super.getOrientation();
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        if(Log.D) Log.d("onDraw()");

        // TODO Auto-generated method stub
        super.onDraw(canvas);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b)
    {
        if(Log.D) Log.d("onLayout()");

        // TODO Auto-generated method stub
        super.onLayout(changed, l, t, r, b);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        if(Log.D) Log.d("onMeasure()");

        // TODO Auto-generated method stub
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    public void setOrientation(int orientation)
    {
        if(Log.D) Log.d("setOrientation()");

        // TODO Auto-generated method stub
        super.setOrientation(orientation);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if(Log.D) Log.d("onKeyDown()");

        // TODO Auto-generated method stub
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyShortcut(int keyCode, KeyEvent event)
    {
        if(Log.D) Log.d("onKeyShortcut()");

        // TODO Auto-generated method stub
        return super.onKeyShortcut(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event)
    {
        if(Log.D) Log.d("onKeyUp()");

        // TODO Auto-generated method stub
        return super.onKeyUp(keyCode, event);
    }
    
    
    
}

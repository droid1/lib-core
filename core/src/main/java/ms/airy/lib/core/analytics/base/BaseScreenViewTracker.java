package ms.airy.lib.core.analytics.base;

import ms.airy.lib.core.analytics.ScreenViewTracker;


public abstract class BaseScreenViewTracker implements ScreenViewTracker
{
    @Override
    public void startSession()
    {

    }

    @Override
    public void endSession()
    {

    }

    @Override
    public void sendScreenView()
    {

    }

}

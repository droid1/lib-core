package ms.airy.lib.core.control.common;


import android.content.res.Configuration;

/**
 * Integer pair.
 * To be used as a key for control axis mapping.
 */
public final class OrientationLocationPair
{
    public final static OrientationLocationPair PortTop = new OrientationLocationPair(Configuration.ORIENTATION_PORTRAIT, LayoutLocation.TOP);
    public final static OrientationLocationPair PortBottom = new OrientationLocationPair(Configuration.ORIENTATION_PORTRAIT, LayoutLocation.BOTTOM);
    public final static OrientationLocationPair PortTopBottom = new OrientationLocationPair(Configuration.ORIENTATION_PORTRAIT, LayoutLocation.TOP_BOTTOM);
    public final static OrientationLocationPair PortLeft = new OrientationLocationPair(Configuration.ORIENTATION_PORTRAIT, LayoutLocation.LEFT);
    public final static OrientationLocationPair PortRight = new OrientationLocationPair(Configuration.ORIENTATION_PORTRAIT, LayoutLocation.RIGHT);
    public final static OrientationLocationPair PortLeftRigth = new OrientationLocationPair(Configuration.ORIENTATION_PORTRAIT, LayoutLocation.LEFT_RIGHT);

    public final static OrientationLocationPair LandTop = new OrientationLocationPair(Configuration.ORIENTATION_LANDSCAPE, LayoutLocation.TOP);
    public final static OrientationLocationPair LandBottom = new OrientationLocationPair(Configuration.ORIENTATION_LANDSCAPE, LayoutLocation.BOTTOM);
    public final static OrientationLocationPair LandTopBottom = new OrientationLocationPair(Configuration.ORIENTATION_LANDSCAPE, LayoutLocation.TOP_BOTTOM);
    public final static OrientationLocationPair LandLeft = new OrientationLocationPair(Configuration.ORIENTATION_LANDSCAPE, LayoutLocation.LEFT);
    public final static OrientationLocationPair LandRight = new OrientationLocationPair(Configuration.ORIENTATION_LANDSCAPE, LayoutLocation.RIGHT);
    public final static OrientationLocationPair LandLeftRigth = new OrientationLocationPair(Configuration.ORIENTATION_LANDSCAPE, LayoutLocation.LEFT_RIGHT);

    public static OrientationLocationPair getPair(int orientation, int location)
    {
        switch(orientation) {
            case Configuration.ORIENTATION_PORTRAIT:
            default:
                switch(location) {
                    case LayoutLocation.TOP:
                        return PortTop;
                    case LayoutLocation.BOTTOM:
                    default:
                        return PortBottom;
                    case LayoutLocation.LEFT:
                        return PortLeft;
                    case LayoutLocation.RIGHT:
                        return PortRight;
                }
            case Configuration.ORIENTATION_LANDSCAPE:
                switch(location) {
                    case LayoutLocation.TOP:
                        return LandTop;
                    case LayoutLocation.BOTTOM:
                    default:
                        return LandBottom;
                    case LayoutLocation.LEFT:
                        return LandLeft;
                    case LayoutLocation.RIGHT:
                        return LandRight;
                }
        }
    }


    private final int orientation;
    private final int location;

    private OrientationLocationPair(int orientation, int location)
    {
        this.orientation = orientation;
        this.location = location;
    }

    public int getOrientation()
    {
        return orientation;
    }

    public int getLocation()
    {
        return location;
    }

}

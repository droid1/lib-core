package ms.airy.lib.core.gesture;

/**
 * Created by harry on 3/16/14.
 */
public interface SwipeGestureListener
{
    void onSwipeRightward();
    void onSwipeLeftward();
    void onSwipeUpward();
    void onSwipeDownward();
}

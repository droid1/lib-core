package ms.airy.lib.core.about.ui;

import android.app.Fragment;
import android.app.ListFragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import ms.airy.lib.core.R;
import ms.airy.lib.core.about.common.AboutConsts;
import ms.airy.lib.core.about.common.AboutInfo;
import ms.airy.lib.core.about.helper.AboutInfoRegistry;
import ms.airy.lib.core.about.ui.data.AboutInfoRowAdapter;
import ms.airy.lib.core.common.ImageSize;
import ms.airy.lib.core.toast.ToastHelper;
import ms.airy.lib.core.ui.common.UIConsts;
import ms.airy.lib.core.ui.image.FullSizeImageViewActivity;


public class AboutInfoListFragment extends ListFragment
{
    public static AboutInfoListFragment newInstance()
    {
        AboutInfoListFragment fragment = new AboutInfoListFragment();
        return fragment;
    }

    private List<AboutInfo> aboutInfoList = null;
    private AboutInfoRowAdapter adapter = null;
    public AboutInfoListFragment()
    {
    }


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // Enable the menu
        setHasOptionsMenu(true);
//        // tbd
//        setRetainInstance(true);

        Bundle args = getArguments();
        if (args != null) {
            // ????
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
//        View rootView = inflater.inflate(R.layout.fragment_about_info_list, container, false);
//        return rootView;
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        if(aboutInfoList == null) {   // ???
            aboutInfoList = AboutInfoRegistry.getInstance(getActivity()).getAboutInfos();
        }
        adapter = new AboutInfoRowAdapter(getActivity(), aboutInfoList);
        setListAdapter(adapter);

        // Temporary
        this.setEmptyText("Info Not Available");    // ???
        // Temporary


        this.getListView().setLongClickable(true);
        getListView().setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
        {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id)
            {
                if(Log.I) Log.i("onItemLongClick() position = " + position + "; id = " + id);

                AboutInfo aboutInfo = (AboutInfo) adapter.getItem(position);
                if(aboutInfo != null) {
                    // int imgResId = aboutInfo.getImageResId();
                    // int imgResId = aboutInfo.getImageResId(ImageSize.SIZE_SMALL);
                    int imgResId = aboutInfo.getImageResId(ImageSize.SIZE_LARGE);
                    if(imgResId > 0) {
                        if(Log.I) Log.i("onItemLongClick() imgResId = " + imgResId);

                        // TBD:
                        Intent intent = new Intent(getActivity(), FullSizeImageViewActivity.class);
                        intent.putExtra(UIConsts.EXTRA_FULL_SIZE_IMAGE_RESID, imgResId);
                        startActivity(intent);

                        return true;
                    }
                }
                return false;
            }
        });

        
    }

//    @Override
//    public void onResume()
//    {
//        super.onResume();
//        // This is not needed for our purposes..
//        if(adapter != null) {
//            adapter.notifyDataSetChanged();
//        }
//    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id)
    {
        // super.onListItemClick(l, v, position, id);
        if(Log.D) Log.d("onListItemClick(). position = " + position + "; id = " + id);

//        AboutInfoRowAdapter adapter = (AboutInfoRowAdapter) l.getAdapter();
        AboutInfo aboutInfo = (AboutInfo) adapter.getItem(position);
        processAboutInfo(aboutInfo);

    }


    private void processAboutInfo(AboutInfo aboutInfo)
    {
        // temporary
        long itemId = aboutInfo.getId();
        if(Log.I) Log.i(">>>>>>> onListItemClick(). itemId = " + itemId);
        // ....

        if(itemId > 0L) {
            Intent intent = null;
            String detailUrl = aboutInfo.getDetailUrl();
            if(detailUrl != null && !detailUrl.isEmpty()) {  // tbd: isValid()
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(detailUrl));
            } else {
                String detailAction = aboutInfo.getDetailAction();
                if(detailAction != null && !detailAction.isEmpty()) {  // tbd: isValid()
                    intent = new Intent();
                    intent.setAction(detailAction);
                    String detailExtra = aboutInfo.getDetailExtra();
                    if(detailExtra != null && !detailExtra.isEmpty()) {  // tbd: isValid()
                        intent.putExtra(AboutConsts.KEY_EXTRA_ARG, detailExtra);
                    }
                } else {
                    // ???
                }
            }
            if(intent != null) {
                getActivity().startActivity(intent);
            }
        } else {
            // ???
            // ToastHelper.getInstance(getActivity()).showToast("Invalid item.");
        }
    }


    // temporary
    private int getMenuItemId(long aboutInfoId)
    {
        final int MENU_ITEM_BASE = 123551;
        return (int) (MENU_ITEM_BASE + aboutInfoId);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        // final List<AboutInfo> aboutInfoList = AboutInfoRegistry.getInstance(getActivity()).getAboutInfos();
        if(aboutInfoList != null && !aboutInfoList.isEmpty()) {
            for(AboutInfo info : aboutInfoList) {
                int miId = getMenuItemId(info.getId());
                String miLabel = (info.getTitle() != null) ? info.getTitle() : "";
                menu.add(Menu.NONE, miId, Menu.NONE, miLabel);
            }
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int miId = item.getItemId();

        // final List<AboutInfo> aboutInfoList = AboutInfoRegistry.getInstance(getActivity()).getAboutInfos();
        if(aboutInfoList != null && !aboutInfoList.isEmpty()) {
            for(AboutInfo info : aboutInfoList) {
                if(miId == getMenuItemId(info.getId())) {
                    processAboutInfo(info);
                    return true;
                }
            }
        }

        return super.onOptionsItemSelected(item);
    }


}

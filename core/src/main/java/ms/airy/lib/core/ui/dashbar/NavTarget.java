package ms.airy.lib.core.ui.dashbar;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;

public class NavTarget
{
    
    // TBD: How to encapsulate "fragment" or "activity/intent" info????
    // Use type for now: type = "fragment", "activity", or "intent".
    // If type == "fragment", the dashbar is reused.
    // Otherwise, a new activity (possibly, with or without dashbar) is invoked.
    private NavType type;
    private Fragment fragment = null;
    private Class<?> activity = null;
    private Intent intent = null;
    private NavCallback callbak = null;
    

    // ????
    public NavTarget()
    {
        this(NavType.HOME);
    }
    public NavTarget(NavType type)
    {
        this.type = type; // ????
    }
    public NavTarget(Fragment fragment)
    {
        this.fragment = fragment;
        this.type = NavType.FRAGMENT;
    }
    public NavTarget(Class<?> activity)
    {
        this.activity = activity;
        this.type = NavType.ACTIVITY;
    }
    public NavTarget(Intent intent)
    {
        this.intent = intent;
        this.type = NavType.INTENT;
    }
    public NavTarget(NavCallback callback)
    {
        this.callbak = callback;
        this.type = NavType.CALLBACK;
    }


    public NavType getType()
    {
        return type;
    }
    // ?????
    private void setType(NavType type)
    {
        this.type = type;
    }

    public Fragment getFragment()
    {
        return fragment;
    }
    public void setFragment(Fragment fragment)
    {
        this.fragment = fragment;
        this.type = NavType.FRAGMENT;  // ????
    }

    public Class<?> getActivity()
    {
        return activity;
    }
    public void setActivity(Class<?> activity)
    {
        this.activity = activity;
        this.type = NavType.ACTIVITY;  // ????
    }

    public Intent getIntent()
    {
        return intent;
    }
    public void setIntent(Intent intent)
    {
        this.intent = intent;
        this.type = NavType.INTENT;  // ????
    }

    public NavCallback getCallbak()
    {
        return callbak;
    }
    public void setCallbak(NavCallback callbak)
    {
        this.callbak = callbak;
        this.type = NavType.CALLBACK;
    }


}

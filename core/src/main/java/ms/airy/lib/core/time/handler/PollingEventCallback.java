package ms.airy.lib.core.time.handler;


/**
 */
public interface PollingEventCallback
{
    void processPollingEvent();
}

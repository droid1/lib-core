package ms.airy.lib.core.phaser.role;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import ms.airy.lib.core.phaser.common.Feature;
import ms.airy.lib.core.phaser.common.Role;
import ms.airy.lib.core.phaser.feature.FeatureHelper;


// TBD:
// Parent control???
// ...

// TBD:
// FeatureMap data should be stored to/from persistent storage such as config.
// Just using a singleton is not sufficient.....
public final class RoleHelper
{

    // Map of feature Id -> { role bit }.
    // FeatureMap is open ended. That is,
    // Features not in this map are supported for all roles.
    private final Map<String, Long> featureMap;

    public RoleHelper()
    {
        featureMap = new HashMap<String, Long>();
        init();
    }
    private void init()
    {
        // TBD:
        // Populate the featureMap....
        // ....

//        Set<Feature> features = FeatureHelper.getInstance().getFeatures();
//        if(features != null && !features.isEmpty()) {
//            for(Feature f : features) {
//                featureMap.put(f.getCode(), f.getRole());
//            }
//        }
    }


    // Singleton
    private static final class RoleHelperHolder
    {
        private static final RoleHelper INSTANCE = new RoleHelper();
    }
    public static RoleHelper getInstance()
    {
        return RoleHelperHolder.INSTANCE;
    }


    // add or update...
    public void addFeature(String featureCode, long role)
    {
        featureMap.put(featureCode, role);
    }


    public long getRole(String featureCode)
    {
        if(! featureMap.containsKey(featureCode)) {
            return Role.ROLE_NONE;
        } else {
            return featureMap.get(featureCode);
        }
    }


    // TBD: Need to double-check this logic...
    public boolean isFeatureAvailable(String featureCode, long role)
    {
        if(! featureMap.containsKey(featureCode)) {
            return true;
        }
        long featureRole = getRole(featureCode);
        if(featureRole == Role.ROLE_NONE ||
                (featureRole & role) == featureRole ) {
            return true;
        } else {
            return false;
        }
    }


}

package ms.airy.lib.core.fragment.listener;

import android.view.MotionEvent;


// This is needed to "relay" the parent activity's event to its child fragments.
public interface TouchEventListener
{
    boolean onTouchEvent(MotionEvent event);

}

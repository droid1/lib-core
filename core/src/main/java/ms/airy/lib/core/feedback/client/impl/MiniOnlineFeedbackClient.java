package ms.airy.lib.core.feedback.client.impl;

import ms.airy.lib.core.feedback.client.OnlineFeedbackClient;

import org.miniclient.ApiUserClient;
import org.miniclient.RestApiException;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 */
public class MiniOnlineFeedbackClient extends AbstractOnlineFeedbackClient implements OnlineFeedbackClient
{
    private ApiUserClient miniFeedbackWebClient = null;

    public MiniOnlineFeedbackClient()
    {
        this((String) null);
    }
    public MiniOnlineFeedbackClient(String feedbackWebServiceEndpoint)
    {
        super(feedbackWebServiceEndpoint);
        this.miniFeedbackWebClient = MiniClientBuilder.getInstance().getFeedbackWebClient(feedbackWebServiceEndpoint);
    }
    public MiniOnlineFeedbackClient(ApiUserClient miniFeedbackWebClient)
    {
        super(miniFeedbackWebClient.getResourceBaseUrl());
        this.miniFeedbackWebClient = miniFeedbackWebClient;
    }

    public ApiUserClient getMiniFeedbackWebClient()
    {
        return miniFeedbackWebClient;
    }
    protected void setMiniFeedbackWebClient(String feedbackWebServiceEndpoint)
    {
        super.setFeedbackWebServiceEndpoint(feedbackWebServiceEndpoint);
        this.miniFeedbackWebClient = MiniClientBuilder.getInstance().getFeedbackWebClient(feedbackWebServiceEndpoint);
    }
    protected void setMiniFeedbackWebClient(ApiUserClient miniFeedbackWebClient)
    {
        super.setFeedbackWebServiceEndpoint(miniFeedbackWebClient.getResourceBaseUrl());
        this.miniFeedbackWebClient = miniFeedbackWebClient;
    }

    @Override
    protected void setFeedbackWebServiceEndpoint(String feedbackWebServiceEndpoint)
    {
        super.setFeedbackWebServiceEndpoint(feedbackWebServiceEndpoint);
        this.miniFeedbackWebClient = MiniClientBuilder.getInstance().getFeedbackWebClient(feedbackWebServiceEndpoint);
    }


    @Override
    public void sendFeedback(long surveyEntryId, int answer, String comment)
    {
        if(Log.I) Log.i("MiniOnlineFeedbackClient.sendFeedback(). surveyEntryId = " + surveyEntryId + "; answer = " + answer + "; comment = " + comment);

        // ???
        try {
            // TBD:
            Map<String,Object> params = new HashMap<String, Object>();
            params.put("surveyEntryId", surveyEntryId);  // ???
            params.put("answer", answer);  // ???
            params.put("comment", comment);  // ???
            miniFeedbackWebClient.create(params);
        } catch (RestApiException e) {
            Log.e("Failed to get send feedback for surveyEntryId = " + surveyEntryId, e);
        } catch (IOException e) {
            Log.e("IO Exception while sending feedback for surveyEntryId = " + surveyEntryId, e);
        }

    }

}

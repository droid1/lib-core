package ms.airy.lib.core.popup;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;

import ms.airy.lib.core.dialog.ActionableAssertDialogHost;
import ms.airy.lib.core.dialog.ActionableAssertDialogListener;
import ms.airy.lib.core.dialog.ActionableChoiceDialogListener;
import ms.airy.lib.core.dialog.stock.SimpleItemListDialogFragment;
import ms.airy.lib.core.dialog.stock.SimpleResponseDialogFragment;
import ms.airy.lib.core.dialog.stock.SimpleWebViewDialogFragment;
import ms.airy.lib.core.popup.common.ActionType;
import ms.airy.lib.core.popup.common.ContentType;
import ms.airy.lib.core.popup.common.PopupInfo;
import ms.airy.lib.core.popup.helper.PopupRegistry;
import ms.airy.lib.core.popup.prefs.PopupInfoPrefsManager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


// Popup types: Splash, Eula, Change logs, Quick Help, Notice, etc...
// "Popup framework" is rather similar to those of help and feedback,
// but unlike help/feedback, popup dialogs are displayed automatically
//     (e.g., when the user opens an app or opens an activity, etc.).
// In Help/Feedback, the dialogs are displayed in response to user's explicit menu actions.
public final class PopupManager implements ActionableAssertDialogListener, ActionableChoiceDialogListener
{
    private static PopupManager INSTANCE = null;
    public static PopupManager getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new PopupManager(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;


    private PopupManager(Context context)
    {
        this.context = context;
    }


    public boolean isReady()
    {
        return PopupRegistry.getInstance(context).isReady();
    }

    public boolean hasShowablePopups()
    {
        if(! PopupRegistry.getInstance(context).isReady()) {
            return false;
        }
        return PopupRegistry.getInstance(context).hasShowablePopups();
    }
    public boolean hasShowablePopups(String category)
    {
        if(! PopupRegistry.getInstance(context).isReady()) {
            return false;
        }
        return PopupRegistry.getInstance(context).hasShowablePopups(category);
    }
    public boolean hasShowablePopups(int popupType)
    {
        if(! PopupRegistry.getInstance(context).isReady()) {
            return false;
        }
        return PopupRegistry.getInstance(context).hasShowablePopups(popupType);
    }
    public boolean hasShowablePopups(String category, int popupType)
    {
        if(! PopupRegistry.getInstance(context).isReady()) {
            return false;
        }
        return PopupRegistry.getInstance(context).hasShowablePopups(category, popupType);
    }


    public PopupInfo getThisPopup(String prefKey)
    {
        if(! PopupRegistry.getInstance(context).isReady()) {
            return null;
        }
        return PopupRegistry.getInstance(context).getPopupInfo(prefKey);
    }


    // TBD:
    // Returns the highest priority popup, if any.
    // Note that the PopupRegistry data might have become stale since maps/lists were built...

    public PopupInfo getPopup()
    {
        if(hasShowablePopups() == false) {
            return null;
        }
        List<String> popups = PopupRegistry.getInstance(context).getShowablePopups();
        if(popups == null || popups.isEmpty()) {
            return null;
        }
        // List is sorted with the highest precedence/priority on top.
        PopupInfo popupInfo = null;
        for(String prefKey : popups) {
            PopupInfo info = PopupRegistry.getInstance(context).getPopupInfo(prefKey);
            if(info.isShowable()) {
                popupInfo = info;
                break;
            }
        }

        return popupInfo;
    }
    public PopupInfo getPopup(String category)
    {
        // TBD:
        // if category==null, just use getPopup() ???
        if(hasShowablePopups(category) == false) {
            return null;
        }
        List<String> popups = PopupRegistry.getInstance(context).getShowablePopups(category);
        if(popups == null || popups.isEmpty()) {
            return null;
        }
        // List is sorted with the highest precedence/priority on top.
        PopupInfo popupInfo = null;
        for(String prefKey : popups) {
            PopupInfo info = PopupRegistry.getInstance(context).getPopupInfo(prefKey);
            if(info.isShowable()) {
                popupInfo = info;
                break;
            }
        }

        return popupInfo;
    }
    public PopupInfo getPopup(int popupType)
    {
        // TBD:
        // if popupType==0, just use getPopup() ???
        if(hasShowablePopups(popupType) == false) {
            return null;
        }
        List<String> popups = PopupRegistry.getInstance(context).getShowablePopups(popupType);
        if(popups == null || popups.isEmpty()) {
            return null;
        }
        // List is sorted with the highest precedence/priority on top.
        PopupInfo popupInfo = null;
        for(String prefKey : popups) {
            PopupInfo info = PopupRegistry.getInstance(context).getPopupInfo(prefKey);
            if(info.isShowable()) {
                popupInfo = info;
                break;
            }
        }

        return popupInfo;
    }
    public PopupInfo getPopup(String category, int popupType)
    {
        // TBD:
        // Ditto.
        if(hasShowablePopups(category, popupType) == false) {
            return null;
        }
        List<String> popups = PopupRegistry.getInstance(context).getShowablePopups(category, popupType);
        if(popups == null || popups.isEmpty()) {
            return null;
        }
        // List is sorted with the highest precedence/priority on top.
        PopupInfo popupInfo = null;
        for(String prefKey : popups) {
            PopupInfo info = PopupRegistry.getInstance(context).getPopupInfo(prefKey);
            if(info.isShowable()) {
                popupInfo = info;
                break;
            }
        }

        return popupInfo;
    }



    // TBD:
    // Returns true, if popup is to be shown. (We have no way to tell if popup was actually displayed)
    // Returns false, if popup is not to be shown.
    public boolean showThisPopup(Activity activity, String prefKey)
    {
        PopupInfo popupInfo = getThisPopup(prefKey);
        if(popupInfo == null) {
            Log.w("Popup not found for prefKey = " + prefKey);
            return false;
        }
        return showPopupDialog(activity, popupInfo);
    }

    public boolean showPopup(Activity activity)
    {
        PopupInfo popupInfo = getPopup();
        if(popupInfo == null) {
            Log.w("Popup not found.");
            return false;
        }
        return showPopupDialog(activity, popupInfo);
    }
    public boolean showPopup(Activity activity, String category)
    {
        PopupInfo popupInfo = getPopup(category);
        if(popupInfo == null) {
            Log.w("Popup not found for category = " + category);
            return false;
        }
        return showPopupDialog(activity, popupInfo);
    }
    public boolean showPopup(Activity activity, int popupType)
    {
        PopupInfo popupInfo = getPopup(popupType);
        if(popupInfo == null) {
            Log.w("Popup not found for popupType = " + popupType);
            return false;
        }
        return showPopupDialog(activity, popupInfo);
    }
    public boolean showPopup(Activity activity, String category, int popupType)
    {
        PopupInfo popupInfo = getPopup(category, popupType);
        if(popupInfo == null) {
            Log.w("Popup not found for category = " + category + "; popupType = " + popupType);
            return false;
        }
        return showPopupDialog(activity, popupInfo);
    }

    // heck?
    // this seems to be the only (or, best) way to keep track of multiple choice items.
    // prefKey -> selected items array.
    // Note that, for popups, the input selectedItems is always none.
    private final Map<String, boolean[]> selectedItemsMap = new HashMap<String, boolean[]>();

    // Always returns true, for now.
    private boolean showPopupDialog(Activity activity, PopupInfo popupInfo)
    {
        String prefKey = popupInfo.getPrefKey();
        String title = popupInfo.getTitle();
        String contentType = popupInfo.getContentType();
        String content = popupInfo.getContent();
        List<String> itemList = popupInfo.getItems();
        String positiveLabel = popupInfo.getPositiveLabel();
        String negativeLabel = popupInfo.getNegativeLabel();
        String neutralLabel = popupInfo.getNeutralLabel();
        int actionType = popupInfo.getActionType();

        String posiButton = "";   // empty string means, do not include the button.
        if(ActionType.includingPositive(actionType)) {
            posiButton = positiveLabel;
        }
        String negaButton = "";
        if(ActionType.includingNegative(actionType)) {
            negaButton = negativeLabel;
        }
        String neutButton = "";
        if(ActionType.includingNeutral(actionType)) {
            neutButton = neutralLabel;
        }

        if(ContentType.isMultiChoiceList(contentType)) {
            if (itemList != null && !itemList.isEmpty()) {
                int size = itemList.size();
                boolean[] selItems = new boolean[size];
                for(int i=0; i<size; i++) {
                    selItems[i] = false;
                }
                selectedItemsMap.put(prefKey, selItems);
            } else {
                // this cannot happen...
            }
        } else {
            // heck. Null selItems means the content type is not multi-choice list.
            selectedItemsMap.put(prefKey, null);
        }

        // TBD:
        DialogFragment fragment = null;
        if(ContentType.isHtml(contentType)) {
            // Use web dialog.
            fragment = SimpleWebViewDialogFragment.newInstance(title, 0, content, posiButton, negaButton, neutButton);
        } else if(ContentType.isList(contentType)) {
            String[] itemArray = new String[]{};
            if(itemList != null) {   // ????
                itemArray = itemList.toArray(new String[]{});
            }
            if(ContentType.isSingleChoiceList(contentType)) {
                fragment = SimpleItemListDialogFragment.newSingleChoiceInstance(title, itemArray, posiButton, negaButton, neutButton);
            } else {
                fragment = SimpleItemListDialogFragment.newMultipleChoiceInstance(title, itemArray, null, posiButton, negaButton, neutButton);
            }
        } else {
            // use message dialog.
            fragment = SimpleResponseDialogFragment.newInstance(title, content, posiButton, negaButton, neutButton);
        }
        // ???
        ((ActionableAssertDialogHost) fragment).addActionable(this);
        // ...

        fragment.show(activity.getFragmentManager(), prefKey);
        return true;
    }



    ////////////////////////////////////////////////////////////////
    // ActionableAssertDialogListener interface.

    @Override
    public void onPositiveClick(String tag)
    {
        if(Log.I) Log.i("PopupManager.onPositiveClick() tag = " + tag);

        String prefKey = tag;
        boolean[] selItems = selectedItemsMap.get(prefKey);
        if(selItems != null) {
            // multi-choice list.
            processResponse(prefKey, convertItemSelectionToResponse(selItems));
        } else {
            // ??? what if the content was a single choice list???
            // We should not generally include positive button for single-choice list.
            // But, if positive button is included, it is really considered neutral (e.g., cancel)
            // hence the response really should be 0 not 1.
            PopupInfo popupInfo = getThisPopup(prefKey);
            String contentType = popupInfo.getContentType();
            if(ContentType.isSingleChoiceList(contentType)) {
                processResponse(prefKey, 0);
            } else {
                processResponse(prefKey, 1);
            }
        }
    }

    @Override
    public void onNegativeClick(String tag)
    {
        if(Log.I) Log.i("PopupManager.onNegativeClick() tag = " + tag);

        processResponse(tag, -1);
    }

    @Override
    public void onNeutralClick(String tag)
    {
        if(Log.I) Log.i("PopupManager.onNeutralClick() tag = " + tag);

        processResponse(tag, 0);
    }


    ////////////////////////////////////////////////////////////////
    // ActionableChoiceDialogListener interface.

    @Override
    public void onDialogItemClick(String tag, int which, boolean isChecked)
    {
        if(Log.I) Log.i("PopupManager.onDialogItemClick() tag = " + tag + "; which = " + which);


        String prefKey = tag;
//        PopupInfo popupInfo = getThisPopup(prefKey);
//        String contentType = popupInfo.getContentType();
//        boolean isMulti = ContentType.isMultiChoiceList(contentType);
//
//        if(isMulti) {
//            boolean[] selItems = selectedItemsMap.get(prefKey);
//            // ...
//        }

        boolean[] selItems = selectedItemsMap.get(prefKey);
        if(selItems != null) {
            selItems[which] = isChecked;
        } else {
            // single choice. onItemClick is equivalent to OK.
            processResponse(prefKey, convertItemSelectionToResponse(which));
        }
    }


    private void processResponse(String prefKey, int response)
    {
        if(Log.I) Log.i("PopupManager.processResponse() prefKey= " + prefKey + "; response = " + response);

        // (1) Update the SharedPreferences....
        PopupInfoPrefsManager.getInstance(context).increaseCounter(prefKey);
        PopupInfoPrefsManager.getInstance(context).setResponse(prefKey, response);
        PopupInfoPrefsManager.getInstance(context).setActionTime(prefKey);

        // (2) Update the popupInfo (which is essentially a "cache")
        PopupInfo popupInfo = getThisPopup(prefKey);
        popupInfo.setCounter(PopupInfoPrefsManager.getInstance(context).getCounter(prefKey));
        // popupInfo.setResponse(response);
        popupInfo.setResponse(PopupInfoPrefsManager.getInstance(context).getResponse(prefKey));
        popupInfo.setActionTime(PopupInfoPrefsManager.getInstance(context).getActionTime(prefKey));
    }



    // temporary

    // this works when 0 <= which < 32
    private static int convertItemSelectionToResponse(int which)
    {
        return (0x1 << which);
    }
    private static int convertItemSelectionToResponse(boolean[] selectedItems)
    {
        int response = 0;
        int size = selectedItems.length;
        for(int i=0; i<size; i++) {
            if(selectedItems[i] == true) {
                response += (0x1 << i);
            }
        }
        return response;
    }

}


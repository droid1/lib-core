package ms.airy.lib.core.control;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;

import java.util.HashMap;
import java.util.Map;

import ms.airy.lib.core.control.common.ControlAxis;


// This is (almost) the same as AnamorphicCompositeControl
// Since we cannot use multiple inheritance, we have one base class for each common layout...
public abstract class AnamorphicRelativeLayout extends BaseCustomRelativeLayout
{
    // Button direction. horz or vert.
    private /* volatile */ int mControlAxis;

    // Master flag.
    // If true, then the control axis is orientation dependent,
    //         that is, whenever the device orientation changes the control axis is updated according to orientationAxisMap.
    // If false, then the control asix is NOT dependent on the device orientation.
    // This should be really read-only.
    // Needs to be set during the initialization/construction, and after that, this should not be changed....
    private boolean axisOrientationDependent;

    // Default mapping.
    // Used only if axisOrientationDependent==true.
    private final Map<Integer, Integer> orientationAxisMap = new HashMap<Integer, Integer>();


    public AnamorphicRelativeLayout(Context context)
    {
        super(context);
        initControl();
    }
    public AnamorphicRelativeLayout(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        initControl();
    }
    public AnamorphicRelativeLayout(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        initControl();
    }


    // Note that it's not a good idea to call virtual/non-final method in ctor....
    private void initControl()
    {
        // Default true???
        // True is convenient in case this control is used stand-alone..
        // If this control is part of a parent control, then
        // it's better to let the parent to set the axis, and hence false is better.
        axisOrientationDependent = false;

        // Default orientation-axis mapping.
        orientationAxisMap.put(Configuration.ORIENTATION_PORTRAIT, ControlAxis.AXIS_HORIZONTAL);
        orientationAxisMap.put(Configuration.ORIENTATION_LANDSCAPE, ControlAxis.AXIS_VERTICAL);

        // ???
        mControlAxis = ControlAxis.AXIS_HORIZONTAL;
        // mControlAxis = getAxisForOrientation(getOrientation());
        // ???
    }






    @Override
    protected void resetOrientation()
    {
        super.resetOrientation();

        if(axisOrientationDependent) {
            // TBD:
            mControlAxis = getAxisForOrientation(getOrientation());
            // ...
        } else {
            // Do not change mControlAxis.
        }
    }


    public boolean isAxisOrientationDependent()
    {
        return axisOrientationDependent;
    }
    public void setAxisOrientationDependent(boolean axisOrientationDependent)
    {
        this.axisOrientationDependent = axisOrientationDependent;
    }

    public int getControlAxis()
    {
        return mControlAxis;
    }
    public void setControlAxis(int controlAxis)
    {
        this.mControlAxis = controlAxis;
    }

    protected Map<Integer, Integer> getOrientationAxisMap()
    {
        return orientationAxisMap;
    }
    protected void setOrientationAxis(int orientation1, int axis1)
    {
        orientationAxisMap.put(orientation1, axis1);
    }
    protected void setOrientationAxis(int orientation1, int axis1, int orientation2, int axis2)
    {
        orientationAxisMap.put(orientation1, axis1);
        orientationAxisMap.put(orientation2, axis2);
    }

    public int getAxisForOrientation(int orientation)
    {
        return orientationAxisMap.get(orientation);
    }
    public int setControlAxisForOrientation()
    {
        return setControlAxisForOrientation(getOrientation());
    }
    public int setControlAxisForOrientation(int orientation)
    {
        setControlAxis(orientationAxisMap.get(orientation));
        return mControlAxis;
    }


    @Override
    protected Parcelable onSaveInstanceState()
    {
        Log.d("onSaveInstanceState() Called.");
        Parcelable superState = super.onSaveInstanceState();
        SavedState savedState = new SavedState(superState);
        // ...
        return savedState;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state)
    {
        Log.d("onRestoreInstanceState() Called.");
        SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(savedState.getSuperState());
        // ...

        // ???
        if(axisOrientationDependent) {
            mControlAxis = getAxisForOrientation(getOrientation());
        }
        // ???
    }


    // temporary
    protected static class SavedState extends BaseCustomRelativeLayout.SavedState implements Parcelable
    {
        // axisOrientationDependent ???
        // controlAxis ???
        // etc...

        public SavedState(Parcelable superState)
        {
            super(superState);
        }
        public SavedState(Parcel source)
        {
            super(source);
        }

        public static final Creator<SavedState> CREATOR = new Creator<SavedState>() {
            @Override
            public SavedState createFromParcel(Parcel source)
            {
                return new SavedState(source);
            }
            @Override
            public SavedState[] newArray(int size)
            {
                return new SavedState[size];
            }
        };

        @Override
        public void writeToParcel(Parcel dest, int flags)
        {
            super.writeToParcel(dest, flags);
        }

        // For debugging...
        @Override
        public String toString()
        {
            return "SavedState{" +
                    '}';
        }
    }

}

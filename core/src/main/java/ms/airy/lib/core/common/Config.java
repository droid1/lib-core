package ms.airy.lib.core.common;


// Make it a singleton?
// Or, implement some sort of factory methods????
// Note: Config is read-only (at least, for now).
public class Config
{
    public Config()
    {
        // ....
    }
    
    
    //    // Log level.
    //    public static int LOGLEVEL = android.util.Log.VERBOSE;
    //    //static final int LOGLEVEL = android.util.Log.DEBUG;
    //    //static final int LOGLEVEL = android.util.Log.INFO;

    
    // "Default" values
    // ...
    private static boolean USE_GA = false;
    private static boolean USE_HEADERBAR = true;
    private static boolean USE_DASHBAR = true;
    
    
    // Make it a singleton????
    
    
    
    // Read config.xml and overwrite the default values...
    //  how to do this ????
    // .....
       
    
    
    // TBD: Convenience functions ???
    // getInt(key, default_value)
    // getString(key, default_value)
    // ....
    
    
    
    
    // Common config vars...
    
    public boolean isUseGa()
    {
        return USE_GA;
    }
    
    public boolean isUseHeaderBar()
    {
        return USE_HEADERBAR;
    }
    
    public boolean isUseDashBar()
    {
        return USE_DASHBAR;
    }
    
    

}

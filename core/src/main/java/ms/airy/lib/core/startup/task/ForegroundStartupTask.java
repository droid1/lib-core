package ms.airy.lib.core.startup.task;

import android.app.Activity;


// Marker interface (for asynctasks)
public interface ForegroundStartupTask extends StartupTask
{
    // if fg task, then call this.
    // Note that activity is needed (not just Context)
    //     to interact with UI.
    void run(Activity activity);   // foreground version ???

}

package ms.airy.lib.core.config.client.factory;

import ms.airy.lib.core.config.client.OnlineConfigClient;
import ms.airy.lib.core.config.client.impl.MiniOnlineConfigClient;
import ms.airy.lib.core.config.client.impl.MockOnlineConfigClient;


/**
 */
public class ConfigClientFactory
{
    // temporary
    private static final int CLIENT_MOCK = 1;
    private static final int CLIENT_MINI = 2;
    // ...

    private static ConfigClientFactory INSTANCE = null;

    public static ConfigClientFactory getInstance()
    {
        if(INSTANCE == null) {
            INSTANCE = new ConfigClientFactory();
        }
        return INSTANCE;
    }

    // mock or mini
    private int type;

    private OnlineConfigClient configClient = null;
    private ConfigClientFactory()
    {
        // TBD:
        type = CLIENT_MOCK;
        // ...
    }

    public void useMockClient()
    {
        type = CLIENT_MOCK;
        configClient = null;
    }
    public void useMiniClient()
    {
        type = CLIENT_MINI;
        configClient = null;
    }


    public OnlineConfigClient getConfigClient()
    {
        if(configClient == null) {
            switch(type) {
                case CLIENT_MOCK:
                default:
                    configClient = new MockOnlineConfigClient();
                    break;
                case CLIENT_MINI:
                    configClient = new MiniOnlineConfigClient();
                    break;
            }
        }
        return configClient;
    }


}

package ms.airy.lib.core.asset;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;


/**
 * Files are stored under /assets/_subdir_ directory.
 */
public class FileContentLoader
{
    // private static final String DEF_SUBDIR = "files";

    // We use the application context.
    private final Context context;
    // Subdir under /assets.
    // No leading or trailing slashes.
    // (e.g., "abc/xyz" --> assets/abc/xyz/_files_)
    private String subdir;

    public FileContentLoader(Context context)
    {
        this(context, null);
    }
    public FileContentLoader(Context context, String subdir)
    {
        this.context = context.getApplicationContext();
        this.subdir = subdir;
    }

    protected Context getContext()
    {
        return this.context;
    }

    public String getSubDir()
    {
        return subdir;
    }
    public void setSubDir(String subdir)
    {
        this.subdir = subdir;
        contentMap = null;    // ???
    }


    // TBD:
    // Load all files into a map:
    private Map<String, String> contentMap = null;

    public Map<String, String> getFileContentMap()
    {
        if(contentMap == null) {
            loadAllFiles();
        }
        return contentMap;
    }

    private void loadAllFiles()
    {
        contentMap = new HashMap<String, String>();
        try {
            Resources resources = getContext().getResources();
            AssetManager assetManager = resources.getAssets();

            String dir = "";
            if(subdir != null && !subdir.isEmpty()) {
                dir = subdir + "/";
            }
            String[] files = null;
            if(subdir != null && !subdir.isEmpty()) {
                files = assetManager.list(subdir);
            } else {
                // ????
                files = assetManager.list("");
            }
            if(Log.D) {
                Log.d(">>>>>>>>> files = " + files);
                if(files != null) {
                    Log.d(">>>>>>>>> files = " + files.length);
                }
            }

            if(files != null && files.length > 0) {
                for(String f : files) {
                    if(Log.D) Log.d(">>>>>>>>> f = " + f);

                    try {
                        String fpath = dir + f;
                        InputStream inputStream = assetManager.open(fpath);
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        StringBuilder sb = new StringBuilder();
                        String line;
                        while((line = bufferedReader.readLine()) != null) {
                            sb.append(line).append("\n");
                        }
                        String content = sb.toString();
                        contentMap.put(f, content);
                        if(Log.I) Log.i("Section loaded: file = " + f + ": content = " + content);

                    } catch (IOException e) {
                        Log.e("Failed to load file: " + f, e);
                    }
                }
            }
        } catch (IOException e) {
            Log.e("Failed to load files.", e);
        }
    }


}

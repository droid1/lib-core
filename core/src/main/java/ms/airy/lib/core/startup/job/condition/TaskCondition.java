package ms.airy.lib.core.startup.job.condition;


// TBD:
// Task conditions should be read from config???
public interface TaskCondition
{
    // If true, other conditions are not relevant.
    boolean isOnlyForFirstStartup();

    // Null value means, Not used.
    // Min/max, both ends inclusive, unless null.

    // Does version-code based conditions make sense??
    //    since this will be released with a particular version.
//    Integer getMinimumVersionCode();
//    Integer getMaximumVersionCode();

    // Does not make sense to have a range.
//    Integer getMinimumCounter();
//    Integer getMaximumCounter();

    // The task is triggered when the counter (for this version) reaches a certain number.
    // 0 means, run the task when the new version is released.
    // TBD: Multiple conditions so that the same task can run more than once?????
    int getThresholdStartupCounter();   // for the current version.

}

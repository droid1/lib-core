package ms.airy.lib.core.common;

import java.util.HashSet;
import java.util.Set;


public class TermType
{
    private TermType() {}

    // TBD
    public static final String TERM_MONTHLY = "monthly";
    public static final String TERM_WEEKLY = "weekly";
    public static final String TERM_DAILY = "daily";
    public static final String TERM_HOURLY = "hourly";
    public static final String TERM_CUMULATIVE = "cumulative";
    public static final String TERM_CURRENT = "current";
    
    private static final Set<String> sTypes = new HashSet<String>();
    static {
        sTypes.add(TERM_MONTHLY);
        sTypes.add(TERM_WEEKLY);
        sTypes.add(TERM_DAILY);
        sTypes.add(TERM_HOURLY);
        sTypes.add(TERM_CUMULATIVE);
        sTypes.add(TERM_CURRENT);
    }
    
    public static boolean isValid(String type)
    {
        if(sTypes.contains(type)) {
            return true;
        } else {
            return false;
        }
    }
    
    public static String getDefaultType()
    {
        // temporary
        //return TERM_CURRENT;
        return TERM_HOURLY;
    }
}

package ms.airy.lib.core.phaser.level;

import ms.airy.lib.core.phaser.common.Level;

import java.util.HashMap;
import java.util.Map;


// TBD:
// FeatureMap data should be stored to/from persistent storage such as config.
// Just using a singleton may not be sufficient.....
public final class LevelHelper
{

    // Map of feature Id -> { min Level (inclusive), max (exclusive) }.
    // Min cannot be null, whereas max can be null, meaning there is no max.
    // FeatureMap is open ended. That is,
    // Features not in this map are supported for all Levels >= minLevel = 0;
    private final Map<String, Level.Range> featureMap;

    public LevelHelper()
    {
        featureMap = new HashMap<String, Level.Range>();
        init();
    }
    private void init()
    {
        // TBD:
        // Populate the featureMap....
        // ....

//        Set<Feature> features = FeatureHelper.getInstance().getFeatures();
//        if(features != null && !features.isEmpty()) {
//            for(Feature f : features) {
//                featureMap.put(f.getCode(), new Level.Range(f.getMinLevel(), f.getMaxLevel()));
//            }
//        }
    }


    // Singleton
    private static final class LevelHelperHolder
    {
        private static final LevelHelper INSTANCE = new LevelHelper();
    }
    public static LevelHelper getInstance()
    {
        return LevelHelperHolder.INSTANCE;
    }

    
    public void addFeature(String featureCode, Integer minLevel, Integer maxLevel)
    {
        featureMap.put(featureCode, new Level.Range(minLevel, maxLevel));
    }

    // Inclusive.
    public int getMinLevel(String featureCode)
    {
        if(! featureMap.containsKey(featureCode)) {
            return Level.L01;
        } else {
            return featureMap.get(featureCode).getMin();
        }
    }
    // Exclusive. ????
    public int getMaxLevel(String featureCode)
    {
        if(! featureMap.containsKey(featureCode)) {
            return Level.MAX;
        } else {
            Integer max = featureMap.get(featureCode).getMax();
            if(max == null) {
                return Level.MAX;
            } else {
                return max;
            }
        }
    }
    public boolean hasMaxLevel(String featureCode)
    {
        if(featureMap.containsKey(featureCode)) {
            if(featureMap.get(featureCode).getMax() != null) {
                return true;
            }
        }
        return false;
    }

    public boolean isFeatureAvailable(String featureCode, int Level)
    {
        if(! featureMap.containsKey(featureCode)) {
            return true;
        }
        return (getMinLevel(featureCode) <= Level && getMaxLevel(featureCode) > Level);
    }


}

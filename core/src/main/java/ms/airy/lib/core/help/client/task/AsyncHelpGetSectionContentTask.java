package ms.airy.lib.core.help.client.task;

import android.os.AsyncTask;

import ms.airy.lib.core.help.client.OnlineHelpClient;
import ms.airy.lib.core.help.client.factory.HelpClientFactory;


/**
 */
public class AsyncHelpGetSectionContentTask extends AsyncTask<Long, Void, String>
{
    private final AsyncTaskCallback callback;
    public AsyncHelpGetSectionContentTask(AsyncTaskCallback callback)
    {
        this.callback = callback;
    }

    // Store this during processing...
    private long sectionId = 0L;

    @Override
    protected String doInBackground(Long... params)
    {
        sectionId = params[0];

        OnlineHelpClient client = HelpClientFactory.getInstance().getClient();

        String content = client.getSectionContent(sectionId);

        return content;
    }

    @Override
    protected void onPostExecute(String content)
    {
        // super.onPostExecute(aLong);

        this.callback.onGetSectionContent(sectionId, content);

    }



    // Callback interface
    public static interface AsyncTaskCallback
    {
        void onGetSectionContent(long sectionId, String content);
    }

}

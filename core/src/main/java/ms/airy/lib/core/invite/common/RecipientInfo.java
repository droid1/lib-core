package ms.airy.lib.core.invite.common;

import java.util.ArrayList;
import java.util.List;


public class RecipientInfo
{
    // PK
    private final String email;   // tbd: could this be like twitter handle, etc. as well ????
    private String name;    // tbd: first name/last name, nickname, etc. ???

    // ???
    // Allow multiple invites per recipient??
    private final List<Long> invites = new ArrayList<Long>();

    // etc...


    public RecipientInfo(String email)
    {
        this(email, null);
    }

    public RecipientInfo(String email, String name)
    {
        this.email = email;
        this.name = name;
    }

    public String getEmail()
    {
        return email;
    }

    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }


    // TBD:
    // add/remove invites, etc...
    // ...


    @Override
    public String toString()
    {
        return "RecipientInfo{" +
                "email='" + email + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}

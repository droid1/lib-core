package ms.airy.lib.core.config.props;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;

import ms.airy.lib.core.config.AbstractConfigManager;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


/**
 * Note: We store the custom properties file either in the /res/raw or in the /assets directory.
 *       Although the config file is not exactly an resource/asset,
 *       unfortunately this seems to be the only place where we can put an arbitrary file.
 */
public class PropertiesConfigManager extends AbstractConfigManager
{
    // Config files under /assets/config
    private static final String CONFIG_DIR = "config";

    // Properties file under the /res/raw directory.
    // It should be passed in, in the form of R.raw._name_.
    private int resourceId;
    // Properties file under the /assets directory. File name.
    // (Could be a path, e.g., abc/xyz.properties. ???)
    private String fileName;
    // Lazy initialized.
    private Properties properties = null;


    public PropertiesConfigManager(Context context)
    {
        this(context, null);
    }
    public PropertiesConfigManager(Context context, int resourceId)
    {
        this(context, resourceId, null);   // ???
    }
    public PropertiesConfigManager(Context context, String fileName)
    {
        this(context, 0, fileName);   // ???
    }
    private PropertiesConfigManager(Context context, int resourceId, String fileName)
    {
        super(context);
        // ResourceId (under /res/raw) over fileName (under /assets/config) takes precedence if both are specified.
        this.resourceId = resourceId;
        this.fileName = fileName;
        // loadProperties();
    }

    private void loadProperties()
    {
        if(resourceId > 0) {
            loadResourceProperties();
        } else if(fileName != null) {
            loadAssetProperties();
        } else {
            // ???
            Log.e("No valid properties file specified.");
        }
        if(Log.V) {
            if(properties != null) {
                Log.v("properties loaded: " + properties);
            }
        }
    }
    private void loadResourceProperties()
    {
        try {
            Resources resources = getContext().getResources();
            InputStream rawResource = resources.openRawResource(resourceId);
            properties = new Properties();
            properties.load(rawResource);
        } catch (Resources.NotFoundException e) {
            Log.e("Failed to find the resource properties file: " + resourceId, e);
        } catch (IOException e) {
            Log.e("Failed to load the resource properties file: " + resourceId, e);
        }
    }
    private void loadAssetProperties()
    {
        try {
            Resources resources = getContext().getResources();
            AssetManager assetManager = resources.getAssets();
            InputStream inputStream = assetManager.open(CONFIG_DIR + "/" + fileName);
            properties = new Properties();
            properties.load(inputStream);
        } catch (IOException e) {
            Log.e("Failed to load the asset properties file: " + fileName, e);
        }
    }


    public int getResourceId()
    {
        return resourceId;
    }
    public void setResourceId(int resourceId)
    {
        this.resourceId = resourceId;
        this.properties = null;    // ???
    }

    public String getFileName()
    {
        return fileName;
    }
    public void setFileName(String fileName)
    {
        this.fileName = fileName;
        this.properties = null;    // ???
    }

    protected Properties getProperties()
    {
        return properties;
    }


    @Override
    public Object get(String key)
    {
        return getString(key);
    }
    @Override
    public Object get(String key, Object defVal)
    {
        Object val = get(key);
        if(val == null) {
            val = defVal;
        }
        return val;
    }

    @Override
    public String getString(String key)
    {
        if(properties == null) {
            loadProperties();
        }
        String val = null;
        if(properties != null) {
            val = properties.getProperty(key);
        }
        return val;
    }
    @Override
    public String getString(String key, String defVal)
    {
        if(properties == null) {
            loadProperties();
        }
        String val = null;
        if(properties != null) {
            val = properties.getProperty(key, defVal);
        }
        return val;
    }

}


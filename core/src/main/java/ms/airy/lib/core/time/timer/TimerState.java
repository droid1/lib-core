package ms.airy.lib.core.time.timer;


/**
 *
 */
public class TimerState
{
    // temporary
    public final static int UNKNOWN = 0;   // ???
    public final static int INITIALIZED = 1;  // Not started. Ready. val == max.
    public final static int RUNNING = 2;   // Started, Counting down. val != max
    public final static int PAUSED = 3;    // Started. But, paused. min < val < max
    public final static int STOPPED = 4;   // Timer not finished, but stopped. val != min ??Same as Paused??
    public final static int FINISHED = 5;  // Finished. val == min.
}

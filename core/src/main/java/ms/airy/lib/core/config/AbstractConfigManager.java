package ms.airy.lib.core.config;

import android.content.Context;


/**
 */
public abstract class AbstractConfigManager
{
    // We use the application context.
    private final Context context;

    protected AbstractConfigManager(Context context)
    {
        this.context = context.getApplicationContext();
    }

    protected Context getContext()
    {
        return this.context;
    }

    public abstract Object get(String key);
    public abstract Object get(String key, Object defVal);
    public abstract String getString(String key);
    public abstract String getString(String key, String defVal);

    // TBD:
    // Different types?
    // int, boolean, float, etc... ???
    // ....

}

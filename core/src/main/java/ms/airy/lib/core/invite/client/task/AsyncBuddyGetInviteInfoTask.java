package ms.airy.lib.core.invite.client.task;

import android.os.AsyncTask;

import ms.airy.lib.core.invite.client.BuddyInviteClient;
import ms.airy.lib.core.invite.client.factory.InviteClientFactory;
import ms.airy.lib.core.invite.common.InviteInfo;
import ms.airy.lib.core.invite.common.RecipientInfo;


/**
 */
public class AsyncBuddyGetInviteInfoTask extends AsyncTask<Long, Void, InviteInfo>
{
    private final AsyncTaskCallback callback;
    public AsyncBuddyGetInviteInfoTask(AsyncTaskCallback callback)
    {
        this.callback = callback;
    }

    // Store input params during processing
    private long inviteId;


    @Override
    protected InviteInfo doInBackground(Long... params)
    {
        inviteId = (Long) params[0];

        BuddyInviteClient client = InviteClientFactory.getInstance().getInviteClient();

        InviteInfo inviteInfo = client.getInviteInfo(inviteId);

        return inviteInfo;
    }

    @Override
    protected void onPostExecute(InviteInfo inviteInfo)
    {
        // super.onPostExecute(aLong);

        this.callback.onGetInviteInfo(inviteInfo, inviteId);
    }




    // Callback interface
    public static interface AsyncTaskCallback
    {
        void onGetInviteInfo(InviteInfo inviteInfo, long inviteId);
    }

}

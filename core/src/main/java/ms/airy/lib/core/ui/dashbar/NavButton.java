package ms.airy.lib.core.ui.dashbar;

import android.content.Context;
import android.content.res.Resources.NotFoundException;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.Button;

import ms.airy.lib.core.common.GUID;

// TBD:
// If i use ImageButton, i don't know how to add label.
// If i use Button, at least "background" image can be added.
// TBD:
// Distinguish "selected" and "focused" (or, hover) state 
// Add a support for "selector" resource including these states....
public class NavButton extends Button  // extends ImageButton???
{
    // temporary
    protected static final int RUNTIME_OS_VERSION = android.os.Build.VERSION.SDK_INT;
    
    // Id/Tag.
    private String mIdTag = null;
    

    public NavButton(Context context)
    {
        this(context, GUID.generateShortString());
    }
    public NavButton(Context context, String idTag)
    {
        super(context);
        init(idTag);
        //...
    }

    public NavButton(Context context, AttributeSet attrs)
    {
        this(context, attrs, GUID.generateShortString());
    }
    public NavButton(Context context, AttributeSet attrs, String idTag)
    {
        super(context, attrs);
        init(idTag);
        //...
    }

    public NavButton(Context context, AttributeSet attrs, int defStyle)
    {
        this(context, attrs, defStyle, GUID.generateShortString());
    }
    public NavButton(Context context, AttributeSet attrs, int defStyle, String idTag)
    {
        super(context, attrs, defStyle);
        init(idTag);
        //...
    }

    private void init(String idTag)
    {
        if(Log.D) Log.d("init()");
        mIdTag = idTag;
        // ...
    }

    
    public String getIdTag()
    {
        return mIdTag;
    }
    public void setIdTag(String idTag)
    {
        this.mIdTag = idTag;
    }

    
    
    // ???????
    private void setIcon(int resId)
    {
        //setBackgroundResource(resId);
        
        try {
            Drawable icon = getResources().getDrawable(resId);
            setIcon(icon);
        } catch(NotFoundException ex) {
            // ignore ???
            if(Log.W) Log.w("Failed to set icon.", ex);
        }
    }
    public void setIcon(Drawable icon)
    {
        if(icon != null) {
            // temporary
            // does this work???
//            if(RUNTIME_OS_VERSION < android.os.Build.VERSION_CODES.JELLY_BEAN) {
//                setBackgroundDrawable(icon);
//            } else {
//                setBackground(icon);
//            }
            setBackgroundDrawable(icon);
            // ????
            //setCompoundDrawables(left, top, right, bottom);
            //setCompoundDrawables(icon, null, null, null);
            
        } else {
            // ????
        }

        // ???
        invalidate();
    }

    
    
    
    
    
    
    // temporary
    public void setNavListener(NavListener l)
    {
        setOnClickListener(l);
    }
    
    
    
    
    
    
    
    @Override
    public void setOnClickListener(OnClickListener l)
    {
        // TODO Auto-generated method stub
        super.setOnClickListener(l);
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        if(Log.D) Log.d("onDraw()");

        // TODO Auto-generated method stub
        super.onDraw(canvas);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        if(Log.D) Log.d("onMeasure()");

        // TODO Auto-generated method stub
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    
}


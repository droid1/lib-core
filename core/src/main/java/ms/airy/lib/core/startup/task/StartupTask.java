package ms.airy.lib.core.startup.task;


// Startup task. To be run either in foreground or background.
public interface StartupTask
{
    // To uniquely identify the task
    //    so that we do not run the same task multiple times....
    long getId();

    // TBD:
    // ...

}

package ms.airy.lib.core.invite.common;


// TBD:
// To keep track of the invite status...
public class InviteInfo
{
    private final long id;
    private String recipient;
    private long inviteTime;
    private long expirationTime;
    private int status;
    // etc...


    public InviteInfo(long id)
    {
        this(id, null, 0L);
    }
    public InviteInfo(long id, String recipient, long inviteTime)
    {
        this.id = id;
        this.recipient = recipient;
        this.inviteTime = inviteTime;
        this.expirationTime = 0L;
        this.status = InviteStatus.STATUS_CREATED;  // ???
    }

    public long getId()
    {
        return id;
    }

    public String getRecipient()
    {
        return recipient;
    }
    public void setRecipient(String recipient)
    {
        this.recipient = recipient;
    }

    public long getInviteTime()
    {
        return inviteTime;
    }
    public void setInviteTime(long inviteTime)
    {
        this.inviteTime = inviteTime;
    }

    public long getExpirationTime()
    {
        return expirationTime;
    }
    public void setExpirationTime(long expirationTime)
    {
        this.expirationTime = expirationTime;
    }

    public int getStatus()
    {
        return status;
    }
    public void setStatus(int status)
    {
        this.status = status;
    }


    @Override
    public String toString()
    {
        return "InviteInfo{" +
                "id=" + id +
                ", recipient='" + recipient + '\'' +
                ", inviteTime=" + inviteTime +
                ", expirationTime=" + expirationTime +
                ", status=" + status +
                '}';
    }
}

package ms.airy.lib.core.auth.fragment;

import android.accounts.Account;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.Set;

import ms.airy.lib.core.auth.common.AccountType;
import ms.airy.lib.core.auth.config.AppAuthConfigManager;
import ms.airy.lib.core.auth.config.ApplicationAuthConfig;
import ms.airy.lib.core.auth.helper.AccountHelper;
import ms.airy.lib.core.auth.helper.AccountPicker;
import ms.airy.lib.core.auth.helper.AuthHelper;
import ms.airy.lib.core.auth.helper.AuthResultCallback;
import ms.airy.lib.core.auth.ui.BaseAccountConsoleActivity;


// TBD:
// To be included in all activities that require auth.
public class BaseAuthSupportFragment extends Fragment implements AccountPicker.AccountPickerCallback, AuthResultCallback
{
    // TBD:
    // Override the config values using the args????

    // boolean
    // protected static final String ARG_AUTH_REQUIRED = "base_auth_required";
    protected static final String ARG_AUTH_DISABLED = "base_auth_disabled";
    protected static final String ARG_SHOW_PRIMARY_PICKER_MENU = "base_auth_show_primary_picker";
    protected static final String ARG_SHOW_PRINCIPAL_PICKER_MENU = "base_auth_show_principle_picker";
    protected static final String ARG_SHOW_PRIMARY_LOGIN_MENU = "base_auth_show_primary_login";
    protected static final String ARG_SHOW_PRIMARY_LOGOUT_MENU = "base_auth_show_primary_logout";
    protected static final String ARG_SHOW_PRINCIPAL_LOGIN_MENU = "base_auth_show_principal_login";
    protected static final String ARG_SHOW_PRINCIPAL_LOGOUT_MENU = "base_auth_show_principal_logout";
    protected static final String ARG_SHOW_CONSOLE_MENU = "base_auth_show_console";
    // ...



    public BaseAuthSupportFragment()
    {
    }


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // Enable the menu
        setHasOptionsMenu(true);

        // tbd
        setRetainInstance(true);
        // ...
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
//        TextView textView = new TextView(getActivity());
//        textView.setText(R.string.hello_blank_fragment);
//        return textView;
        return null;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        // Set the authHelper callback.
        AuthHelper.getInstance(getActivity()).addAuthResultCallback(this);

    }


    @Override
    public void onStart()
    {
        super.onStart();
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }

    @Override
    public void onPause()
    {
        super.onPause();
    }

    @Override
    public void onStop()
    {
        super.onStop();
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
    }



    // temporary
    private static final int MENU_ITEM_PICK_PRIMARY_ACCOUNT = 214831;
    private static final int MENU_ITEM_PICK_PRINCIPAL_ACCOUNT = 214832;
    private static final int MENU_ITEM_AUTHORIZE_PRIMARY = 214841;
    private static final int MENU_ITEM_UNAUTHORIZE_PRIMARY = 214842;
    private static final int MENU_ITEM_AUTHORIZE_PRINCIPAL = 214851;
    private static final int MENU_ITEM_UNAUTHORIZE_PRINCIPAL = 214852;
    private static final int MENU_ITEM_SHOW_ACCOUNT_CONSOLE = 214861;

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        // TBD: Put the menu labels in the resource file.
        // TBD: Icons.

        ApplicationAuthConfig authConfig = AppAuthConfigManager.getInstance(getActivity()).getApplicationAuthConfig();

        // Show one picker at a time. If primary account picker item is shown, the rest are ignored, for now.
        if(authConfig.isPrimaryAccountRequired()
                && ! AccountHelper.getInstance(getActivity()).hasPrimaryAccount()) {
            int size = AccountHelper.getInstance(getActivity()).getSizeForDefaultAccountType();
            if(size == 0) {
                // Nothing to select
                // ???
            } else {
                if(authConfig.isAutoSelectAccount()
                        || (size == 1 && authConfig.isAutoSelectAccountIfOne())) {
                    // auto select.
                    // this is not the best place to do this.
                    // we will need to do it in Startup tasks...
                    // ...
                } else {
                    MenuItem item1 = menu.add(Menu.NONE, MENU_ITEM_PICK_PRIMARY_ACCOUNT, Menu.NONE, "Select Account");
                    // item1.setIcon(android.R.drawable.ic_menu_help);
                }
            }
        } else {
            if(authConfig.isPrincipalAccountRequired()
                    && ! AccountHelper.getInstance(getActivity()).hasPrincipalAccount()) {
                int size = AccountHelper.getInstance(getActivity()).getSizeForPrincipalAccountType();
                if(size == 0) {
                    // Nothing to select
                    // ???
                } else {
                    if(authConfig.isAutoSelectAccount()
                            || (size == 1 && authConfig.isAutoSelectAccountIfOne())) {
                        // auto select.
                        // this is not the best place to do this.
                        // we will need to do it in Startup tasks...
                        // ...
                    } else {
                        MenuItem item1 = menu.add(Menu.NONE, MENU_ITEM_PICK_PRINCIPAL_ACCOUNT, Menu.NONE, "Select Account");
                        // item1.setIcon(android.R.drawable.ic_menu_help);
                    }
                }
            } else {
                // TBD:
                // check the next one, etc...
                // ...
            }
        }

        if(authConfig.isApplicationAuthDisabled() == false) {
            if (authConfig.isAuthOptionsMenuDisabled() == false) {
                // TBD check auth state for primary account? Or, google account ???
                // Or, for the main account of the app's principal account type?

                // Kind of complicated. But...
                //     if the app has principal account type,
                //        then the auth menus will be used for the principal account, if any.
                //           If the user does not have a principal account, the menus will not be shown.
                //     If not,
                //        then the auth menus will be used for the primary account type.
                // Note that primary and principal accounts might be the same for certain users...
                if (authConfig.hasPrincipalAccountType()) {
                    Account account = AccountHelper.getInstance(getActivity()).getPrincipalAccount();
                    if (account != null) {
                        if(AuthHelper.getInstance(getActivity()).isPrincipalAccountAuthorized()) {
                            if(authConfig.includeUnauthorizeOptionsMenu()) {
                                // Include the logout menu here.
                                MenuItem item1 = menu.add(Menu.NONE, MENU_ITEM_UNAUTHORIZE_PRINCIPAL, Menu.NONE, "Log out");
                                // item1.setIcon(android.R.drawable.ic_menu_help);
                            }
                        } else {
                            if(authConfig.includeAuthorizeOptionsMenu()) {
                                // Include the login menu here.
                                MenuItem item1 = menu.add(Menu.NONE, MENU_ITEM_AUTHORIZE_PRINCIPAL, Menu.NONE, "Log in");
                                // item1.setIcon(android.R.drawable.ic_menu_help);
                            }
                        }
                    }
                } else {
                    Account account = AccountHelper.getInstance(getActivity()).getPrimaryAccount();
                    if (account != null) {
                        if(AuthHelper.getInstance(getActivity()).isPrimaryAccountAuthorized()) {
                            if(authConfig.includeUnauthorizeOptionsMenu()) {
                                // Include the logout menu here.
                                MenuItem item1 = menu.add(Menu.NONE, MENU_ITEM_UNAUTHORIZE_PRIMARY, Menu.NONE, "Log out");
                                // item1.setIcon(android.R.drawable.ic_menu_help);
                            }
                        } else {
                            if(authConfig.includeAuthorizeOptionsMenu()) {
                                // Include the login menu here.
                                MenuItem item1 = menu.add(Menu.NONE, MENU_ITEM_AUTHORIZE_PRIMARY, Menu.NONE, "Log in");
                                // item1.setIcon(android.R.drawable.ic_menu_help);
                            }
                        }
                    }
                }

                if(authConfig.includeAccountConsoleMenu()) {
                    // Include the console menu here
                    MenuItem item1 = menu.add(Menu.NONE, MENU_ITEM_SHOW_ACCOUNT_CONSOLE, Menu.NONE, "Manage Accounts");
                    // item1.setIcon(android.R.drawable.ic_menu_help);
                }
            }
        }

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch(item.getItemId()) {
            case MENU_ITEM_PICK_PRIMARY_ACCOUNT:
                showPrimaryAccountPicker();
                return true;
            case MENU_ITEM_PICK_PRINCIPAL_ACCOUNT:
                showPrincipalAccountPicker();
                return true;
            case MENU_ITEM_AUTHORIZE_PRIMARY:
                doAuthorizeForPrimaryAccount();
                return true;
            case MENU_ITEM_UNAUTHORIZE_PRIMARY:
                doUnauthorizeForPrimaryAccount();
                return true;
            case MENU_ITEM_AUTHORIZE_PRINCIPAL:
                doAuthorizeForPrincipalAccount();
                return true;
            case MENU_ITEM_UNAUTHORIZE_PRINCIPAL:
                doUnauthorizeForPrincipalAccount();
                return true;
            case MENU_ITEM_SHOW_ACCOUNT_CONSOLE:
                showAccountConsoleActivity();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }



    // TBD:

    // Temporary
    private static final String TAG_PICK_PRIMARY_ACCOUNT = "pick_primary";
    private static final String TAG_PICK_PRINCIPAL_ACCOUNT = "pick_principal";


    private void showPrimaryAccountPicker()
    {
        int type = AccountHelper.getInstance(getActivity()).getDefaultAccountType();
        showAccountPicker(type, TAG_PICK_PRIMARY_ACCOUNT);
    }

    private void showPrincipalAccountPicker()
    {
        int type = AppAuthConfigManager.getInstance(getActivity()).getApplicationAuthConfig().getPrincipalAccountType();
        showAccountPicker(type, TAG_PICK_PRINCIPAL_ACCOUNT);
    }

    private void doAuthorizeForPrimaryAccount()
    {
        int accountType = AccountHelper.getInstance(getActivity()).getPrimaryAccountType();
        Set<String> authScopes = AppAuthConfigManager.getInstance(getActivity()).getApplicationAuthConfig().getScopeSet(accountType);
        AuthHelper.getInstance(getActivity()).authorizeForPrimaryAccount(authScopes, getActivity());
    }

    private void doUnauthorizeForPrimaryAccount()
    {
        AuthHelper.getInstance(getActivity()).unauthorizeForPrimaryAccount();
    }

    private void doAuthorizeForPrincipalAccount()
    {
        int accountType = AccountHelper.getInstance(getActivity()).getPrincipalAccountType();
        Set<String> authScopes = AppAuthConfigManager.getInstance(getActivity()).getApplicationAuthConfig().getScopeSet(accountType);
        AuthHelper.getInstance(getActivity()).authorizeForPrincipalAccount(authScopes, getActivity());
    }

    private void doUnauthorizeForPrincipalAccount()
    {
        AuthHelper.getInstance(getActivity()).unauthorizeForPrincipalAccount();
    }

    private void showAccountConsoleActivity()
    {
        // TBD:
        // Use a generic intent??? e.g., OPEN_ACCOUNT_MANAGEMENT_CONSOLE...
        Intent intent = new Intent(getActivity(), BaseAccountConsoleActivity.class);
        startActivity(intent);
    }



    private void showAccountPicker(int accountType, String tag)
    {
        AccountPicker accountPickHelper = new AccountPicker(getActivity(), this, accountType);
        accountPickHelper.pickAccount(getActivity(), tag);
    }

    /////////////////////////////////////////////////
    // AccountPicker.AccountPickerCallback interface

    @Override
    public void onAccountPicked(String tag, int type, String name)
    {
        if(Log.I) Log.i("BaseAuthSupportFragment.onAccountPicked() tag = " + tag + "; type = " + type + "; name = " + name);

        // TBD: Validate args?
        if(TAG_PICK_PRIMARY_ACCOUNT.equals(tag)) {
            AccountHelper.getInstance(getActivity()).setPrimaryAccount(type, name);
        } else if(TAG_PICK_PRINCIPAL_ACCOUNT.equals(tag)) {
            AccountHelper.getInstance(getActivity()).setPrincipalAccount(name);
        } else {
            // ignore
        }
    }

    @Override
    public void onAccountPickCanceled(String tag, int type)
    {
        if(Log.I) Log.i("BaseAuthSupportFragment.onAccountPickCanceled(). tag = " + tag + "; type = " + type);

        // Do nothing..
    }


    /////////////////////////////////////////////////
    // AuthResultCallback interface

    @Override
    public void onAuthorizationSuccess(int type, String name, String authToken, String authScopes, long authTime)
    {
        if(Log.I) Log.i("BaseAuthSupportFragment.onAuthorizationSuccess(). type = " + type + "; name = " + name + "; authScopes =" +  authScopes + "; authTime = " + authTime);

        // ???
        // This crashes if we use a different thread handler...
        // adapter.notifyDataSetChanged();
        // ...
    }

    @Override
    public void onAuthorizationFailure(int type, String name, String reason)
    {
        if(Log.I) Log.i("BaseAuthSupportFragment.onAuthorizationFailure(). type = " + type + "; name = " + name + "; reason = " + reason);

        // ???
        // This crashes if we use a different thread handler...
        // adapter.notifyDataSetChanged();
        // ...
    }

}

package ms.airy.lib.core.time.handler;

import android.os.Looper;

import ms.airy.lib.core.time.common.PollingSchedule;

import java.util.ArrayList;
import java.util.List;


/**
 * Polling using Fibonacci sequence delays....
 */
public class FibonacciPollingHandler extends PollingHandler
{
    // Initial delay intervals.
    // In milli seconds.
    private long delay1 = 1000L;
    private long delay2 = 1000L;

    public FibonacciPollingHandler()
    {
        init();
    }
    // TBD: do we need callback?
    public FibonacciPollingHandler(Callback callback)
    {
        super(callback);
        init();
    }
    public FibonacciPollingHandler(Looper looper)
    {
        super(looper);
        init();
    }
    // TBD: do we need callback?
    public FibonacciPollingHandler(Looper looper, Callback callback)
    {
        super(looper, callback);
        init();
    }
    private void init()
    {
        setPollingSchedule(PollingSchedule.FIBONACCI);
    }



    public final void setInitialDelays(long delay1, long delay2)
    {
        if(Log.D) Log.d("setPollingIntervalAndDelta() called with delay1 = " + delay1 + " & delay2 = " + delay2);
        this.delay1 = delay1;
        this.delay2 = delay2;
    }


    // Note the name of the methods.
    // The "fetch" method is not a "getter", and it is not idempotent.
    // Calling these methods changes the internal state of the handler.
    @Override
    protected long[] fetchNextDelays(int size)
    {
        long[] delays = computeNextDelays(size, this.delay1, this.delay2);
        if(delays != null && delays.length > 0) {
            this.delay1 = this.delay2;
            this.delay2 = delays[0];    // note: the first element of the returned arrays is the same as delay2.
        }
        return delays;
    }
    @Override
    protected long[] fetchNextDelays(long duration)
    {
        long[] delays = computeNextDelays(duration, this.delay1, this.delay2);
        if(delays != null && delays.length > 0) {
            this.delay1 = this.delay2;
            this.delay2 = delays[0];
        }
        return delays;
    }
    @Override
    protected long[] computeNextDelays(int size)
    {
        return computeNextDelays(size, this.delay1, this.delay2);
    }
    @Override
    protected long[] computeNextDelays(long duration)
    {
        return computeNextDelays(duration, this.delay1, this.delay2);
    }
    private long[] computeNextDelays(int size, long delay1, long delay2)
    {
        long[] delays = new long[size];

        long prev1 = delay1;
        long prev2 = delay2;
        for (int i = 0; i < size; i++) {
            delays[i] = prev1 + prev2;
            prev1 = prev2;
            prev2 = delays[i];
        }

        return delays;
    }
    private long[] computeNextDelays(long duration, long delay1, long delay2)
    {
        List<Long> list = new ArrayList<Long>();
        long totalDelay = 0;
        int cnt = 0;
        long prev1 = delay1;
        long prev2 = delay2;
        while(totalDelay < duration && cnt < 10000) {   // arbitrary cutoff to prevent infinite loop.
            long delay = prev1 + prev2;
            prev1 = prev2;
            prev2 = delay;
            list.add(delay);
            totalDelay += delay;
            cnt++;
        }
        // Long[] arr = list.toArray(new Long[]{});
        long[] delays = new long[list.size()];
        for (int i = 0; i < list.size(); i++) {
            delays[i] = list.get(i);
        }
        return delays;
    }


}

package ms.airy.lib.core.popup.common;


/**
 */
public final class ActionType
{
    // ???
    public static final int AT_UNKNOWN = 0;

    // New constants.

    public static final int AT_POSITIVE = 1;
    public static final int AT_NEGATIVE = 2;
    public static final int AT_NEUTRAL = 4;
    public static final int AT_POSITIVE_NEGATIVE = 3;
    public static final int AT_POSITIVE_NEUTRAL = 5;
    public static final int AT_NEGATIVE_NEUTRAL = 6;
    public static final int AT_POSITIVE_NEGATIVE_NEUTRAL = 7;

    public static boolean includingPositive(int actionType)
    {
        return ((actionType & AT_POSITIVE) != 0);
    }
    public static boolean includingNegative(int actionType)
    {
        return ((actionType & AT_NEGATIVE) != 0);
    }
    public static boolean includingNeutral(int actionType)
    {
        return ((actionType & AT_NEUTRAL) != 0);
    }


    // Old constants.
//    // positive: ok/dismiss.
//    public static final int AT_OK = 1;
//    // positive: ok, neutral: cancel/dismiss.
//    public static final int AT_OK_CANCEL = 2;
//    // positive: ok, negative: cancel, neutral: remind me later.
//    public static final int AT_OK_CANCEL_LATER = 3;
//    // positive: ok, negative: cancel, neutral: ignore.
//    public static final int AT_OK_CANCEL_IGNORE = 4;
//    // positive: yes/accept, negative: no/decline.
//    public static final int AT_YES_NO = 5;
//    // positive: yes/accept, negative: no/decline, neutral: remind me later.
//    public static final int AT_YES_NO_LATER = 6;
//    // positive: yes/accept, negative: no/decline, neutral: ignore.
//    public static final int AT_YES_NO_IGNORE = 7;
//    // positive: next, neutral: dismiss.
//    public static final int AT_NEXT_CANCEL = 8;
//    // positive: next, neutral: dismiss, neutral: do not show this again.
//    public static final int AT_NEXT_CANCEL_IGNORE = 9;
//    // etc...


    private ActionType() {}


}

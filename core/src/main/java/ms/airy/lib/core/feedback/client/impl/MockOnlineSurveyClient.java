package ms.airy.lib.core.feedback.client.impl;

import ms.airy.lib.core.feedback.client.OnlineSurveyClient;
import ms.airy.lib.core.feedback.common.SurveyEntry;


/**
 */
public class MockOnlineSurveyClient implements OnlineSurveyClient
{

    public MockOnlineSurveyClient()
    {
    }

    @Override
    public SurveyEntry getSpecialSurvey()
    {
        Log.i("MockOnlineSurveyClient.getSpecialSurvey().");

        return null;
    }

    @Override
    public SurveyEntry getDailySurvey(long time)
    {
        if(Log.I) Log.i("MockOnlineSurveyClient.getDailySurvey(). time = " + time);

        return null;
    }

    @Override
    public SurveyEntry getSurvey(long id)
    {
        if(Log.I) Log.i("MockOnlineSurveyClient.getSurvey(). id = " + id);

        return null;
    }
}

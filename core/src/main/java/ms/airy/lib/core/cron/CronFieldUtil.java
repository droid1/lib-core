package ms.airy.lib.core.cron;


/**
 * Utility methods for "cron fields"
 * Cron fields are integer/long bit masks,
 * which are to be used instead of Navigable Set.
 */
public final class CronFieldUtil
{
    private static final long ALL_ONES = ~0x0L;

    private CronFieldUtil() {}


    public static boolean contains(int fieldIndex, long bitmask, int val)
    {
        if(fieldIndex == CronConstants.YEARS) {
            if ((bitmask & (0x1L << (val - CronConstants.MIN_YEARS))) != 0) {
                return true;
            }
        } else {
            if ((bitmask & (0x1L << val)) != 0) {
                return true;
            }
        }
        return false;
    }

    public static long add(int fieldIndex, long bitmask, int val)
    {
        int bit;
        if(fieldIndex == CronConstants.YEARS) {
            bit = (0x1 << (val - CronConstants.MIN_YEARS));
        } else {
            bit = (0x1 << val);
        }
        return (bitmask | bit);
    }
    public static long add(int fieldIndex, long bitmask, int rangeStart, int rangeEnd, int step)
    {
        if(fieldIndex == CronConstants.YEARS) {
            for (int i = rangeStart; i <= rangeEnd; i++) {
                if((i%step) == 0) {
                    int bit = (0x1 << (i - CronConstants.MIN_YEARS));
                    bitmask |= bit;
                }
            }
        } else {
            for (int i = rangeStart; i <= rangeEnd; i++) {
                if((i%step) == 0) {
                    int bit = (0x1 << i);
                    bitmask |= bit;
                }
            }
        }
        return bitmask;
    }
    public static long add(int fieldIndex, long bitmask, int rangeStart, int rangeEnd, int step, int modulo)
    {
        int bits = 0;
        if(fieldIndex == CronConstants.YEARS) {
            for (int i = rangeStart; i <= rangeEnd; i++) {
                if((i%step) == 0) {
                    int bit = (0x1 << ((i - CronConstants.MIN_YEARS) % modulo));  // ???
                    bitmask |= bit;
                }
            }
        } else {
            for (int i = rangeStart; i <= rangeEnd; i++) {
                if((i%step) == 0) {
                    int bit = (0x1 << (i % modulo));
                    bitmask |= bit;
                }
            }
        }
        return bitmask;
    }

    // End, included. ???? (end: -1, 0, 1, 2, ... ???)
    public static long head(int fieldIndex, long bitmask, int end)
    {
        long mask;
        if(fieldIndex == CronConstants.YEARS) {
            mask = ~(ALL_ONES << (end + 1 - CronConstants.MIN_YEARS));
        } else {
            mask = ~(ALL_ONES << (end + 1));
        }
        bitmask &= mask;
        return bitmask;
    }

    // Start, included.
    public static long tail(int fieldIndex, long bitmask, int start)
    {
        long mask;
        if(fieldIndex == CronConstants.YEARS) {
            mask = (ALL_ONES << (start - CronConstants.MIN_YEARS));
        } else {
            mask = (ALL_ONES << start);
        }
        bitmask &= mask;
        return bitmask;
    }

    public static int[] getValuesAscending(int fieldIndex, long bitmask)
    {
        // ???
        int maxSize = CronConstants.RANGES[fieldIndex][1] - CronConstants.RANGES[fieldIndex][0] + 1;
        int[] array = new int[maxSize];

        int counter = 0;
        for(int i=CronConstants.RANGES[fieldIndex][0]; i<=CronConstants.RANGES[fieldIndex][1]; i++) {
            if(fieldIndex == CronConstants.YEARS) {
                if((bitmask & (0x1L << (i - CronConstants.MIN_YEARS))) != 0) {
                    array[counter++] = i;
                }
            } else {
                if((bitmask & (0x1L << i)) != 0) {
                    array[counter++] = i;
                }
            }
        }
        int[] fields = new int[counter];
        System.arraycopy(array, 0, fields, 0, counter);

        return fields;
    }

    public static int[] getValuesDescending(int fieldIndex, long bitmask)
    {
        // ???
        int maxSize = CronConstants.RANGES[fieldIndex][1] - CronConstants.RANGES[fieldIndex][0] + 1;
        int[] array = new int[maxSize];

        int counter = 0;
        for(int i=CronConstants.RANGES[fieldIndex][1]; i>=CronConstants.RANGES[fieldIndex][0]; i--) {
            if(fieldIndex == CronConstants.YEARS) {
                if((bitmask & (0x1L << (i - CronConstants.MIN_YEARS))) != 0) {
                    array[counter++] = i;
                }
            } else {
                if((bitmask & (0x1L << i)) != 0) {
                    array[counter++] = i;
                }
            }
        }
        int[] fields = new int[counter];
        System.arraycopy(array, 0, fields, 0, counter);

        return fields;
    }

}

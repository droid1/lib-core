package ms.airy.lib.core.feedback.client;

import java.util.List;

import ms.airy.lib.core.feedback.common.ResponseEntry;


// TBD:
public interface OnlineResponseClient
{
    ResponseEntry getResponse(long responseId);
    // ResponseEntry getFeedbackResponse(long feedbackId);
    // List<ResponseEntry> getNewResponses();
    List<Long> getNewResponses();
}

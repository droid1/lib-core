package ms.airy.lib.core.startup.job.condition;


public class BaseTaskCondition implements TaskCondition
{
    // if true, other conditions are ignored.
    private final boolean onlyForFirstStartup;
    // 0, by default.
    // Note that (true, X) is not equivalent to (false, 0), in general.
    private final int thresholdCounter;


    public BaseTaskCondition()
    {
        this(true);
    }

    public BaseTaskCondition(boolean onlyForFirstStartup)
    {
        this(onlyForFirstStartup, 0);
    }

    public BaseTaskCondition(boolean onlyForFirstStartup, int thresholdCounter)
    {
        this.onlyForFirstStartup = onlyForFirstStartup;
        this.thresholdCounter = thresholdCounter;
    }

    public boolean isOnlyForFirstStartup()
    {
        return onlyForFirstStartup;
    }
//    public void setOnlyForFirstStartup(boolean onlyForFirstStartup)
//    {
//        this.onlyForFirstStartup = onlyForFirstStartup;
//    }

    public int getThresholdStartupCounter()
    {
        return thresholdCounter;
    }
//    public void setThresholdCounter(int thresholdCounter)
//    {
//        this.thresholdCounter = thresholdCounter;
//    }

}

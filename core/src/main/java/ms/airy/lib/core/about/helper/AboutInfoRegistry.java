package ms.airy.lib.core.about.helper;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import ms.airy.lib.core.about.common.AboutInfo;


// Heck
// Used mainly to pass a list of AboutInfo beans to the fragment....
public class AboutInfoRegistry
{
    private static AboutInfoRegistry INSTANCE = null;
    public static AboutInfoRegistry getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new AboutInfoRegistry(context!= null ? context.getApplicationContext() : null);  // ???
        } else {
            if(INSTANCE.getContext() == null && context != null) {  // ???
                // TBD:
                // Need to "clone" the existing singleton ????
                INSTANCE = new AboutInfoRegistry(context.getApplicationContext());  // ???
            }
        }
        return INSTANCE;
    }

    private final Context context;
    private AboutInfoRegistry(Context context)
    {
        this.context = context;
    }
    protected final Context getContext()
    {
        return context;
    }

    // temporary
    private final List<AboutInfo> aboutInfos = new ArrayList<AboutInfo>();

    // This should be called every time a new list is created since it's a global singleton.
    public void reset()
    {
        this.aboutInfos.clear();
    }
    public void addAboutInfo(AboutInfo aboutInfo)
    {
        this.aboutInfos.add(aboutInfo);
    }
    public void setAboutInfos(List<AboutInfo> aboutInfos)
    {
        this.aboutInfos.clear();
        this.aboutInfos.addAll(aboutInfos);
    }

    public List<AboutInfo> getAboutInfos()
    {
        return this.aboutInfos;
    }


    // tbd
    // ....


}

package ms.airy.lib.core.alert;

import android.content.Context;


// Registry of "alarm times"
public final class AlertEventRegistry
{
    private static AlertEventRegistry INSTANCE = null;
    public static AlertEventRegistry getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new AlertEventRegistry(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;


    private AlertEventRegistry(Context context)
    {
        this.context = context;
    }


    // TBD:
    // ....


}

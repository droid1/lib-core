package ms.airy.lib.core.dialog;


// Base interface for all "actionable" dialogs...
public interface ActionableDialogListener
{
    // "Positive" action. Confirm, Accept, Do, etc..
    void onPositiveClick(String tag);
}

package ms.airy.lib.core.feedback.client.callback;

import java.util.List;

import ms.airy.lib.core.feedback.client.AsyncResponseCallback;
import ms.airy.lib.core.feedback.common.ResponseEntry;


public abstract class AbstractAsyncOnlineResponseCallback implements AsyncResponseCallback
{

    @Override
    public void onGetResponse(ResponseEntry response, long responseId)
    {
        if(Log.I) Log.i("AbstractAsyncOnlineResponseCallback.onGetResponse(). responseId = " + responseId);

    }

    @Override
    public void onGetNewResponses(List<Long> respones)
    {
        if(Log.I) Log.i("AbstractAsyncOnlineResponseCallback.onGetNewResponses().");

    }

}

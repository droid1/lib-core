package ms.airy.lib.core.status.client.impl;

import ms.airy.lib.core.status.client.StatusMessageFetchClient;
import ms.airy.lib.core.status.common.StatusMessage;

import org.miniclient.ApiUserClient;
import org.miniclient.RestApiException;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 */
public class MiniStatusMessageFetchClient extends AbstractStatusMessageClient implements StatusMessageFetchClient
{
    private ApiUserClient miniStatusWebClient = null;

    public MiniStatusMessageFetchClient()
    {
        this((String) null);
    }
    public MiniStatusMessageFetchClient(String statusWebServiceEndpoint)
    {
        super(statusWebServiceEndpoint);
        this.miniStatusWebClient = MiniClientBuilder.getInstance().getStatusWebClient(statusWebServiceEndpoint);
    }
    public MiniStatusMessageFetchClient(ApiUserClient miniStatusWebClient)
    {
        super(miniStatusWebClient.getResourceBaseUrl());
        this.miniStatusWebClient = miniStatusWebClient;
    }

    public ApiUserClient getMiniStatusWebClient()
    {
        return miniStatusWebClient;
    }
    protected void setMiniStatusWebClient(String statusWebServiceEndpoint)
    {
        super.setStatusWebServiceEndpoint(statusWebServiceEndpoint);
        this.miniStatusWebClient = MiniClientBuilder.getInstance().getStatusWebClient(statusWebServiceEndpoint);
    }
    protected void setMiniStatusWebClient(ApiUserClient miniStatusWebClient)
    {
        super.setStatusWebServiceEndpoint(miniStatusWebClient.getResourceBaseUrl());
        this.miniStatusWebClient = miniStatusWebClient;
    }

    @Override
    protected void setStatusWebServiceEndpoint(String statusWebServiceEndpoint)
    {
        super.setStatusWebServiceEndpoint(statusWebServiceEndpoint);
        this.miniStatusWebClient = MiniClientBuilder.getInstance().getStatusWebClient(statusWebServiceEndpoint);
    }


    @Override
    public StatusMessage fetchStatusMessage(long id)
    {
        if(Log.I) Log.i("MiniStatusMessageFetchClient.fetchStatusMessage() id = " + id);

        Object result = null;
        try {
            // TBD:
            Map<String,Object> params = new HashMap<String, Object>();
            params.put("id", id);  // ???
            List<Object> objs = miniStatusWebClient.list(params);
            if(objs != null && objs.size() > 0) {
                result = objs.get(0);   // ???
            }
        } catch (RestApiException e) {
            Log.e("Failed to get message for id = " + id, e);
        } catch (IOException e) {
            Log.e("IO Exception while fetching message for id = " + id, e);
        }

        StatusMessage statusMessage = null;
        if(result != null) {
            // TBD
            // ...
        }

        return statusMessage;
    }

    @Override
    public List<Long> fetchNewMessages()
    {
        Log.i("MiniStatusMessageFetchClient.fetchNewMessages()");

        Object result = null;
        try {
            // TBD:
            Map<String,Object> params = new HashMap<String, Object>();
            // params.put("new", true);  // ???
            List<Object> objs = miniStatusWebClient.list(params);
            if(objs != null && objs.size() > 0) {
                // ....
            }
        } catch (RestApiException e) {
            Log.e("Failed to get messages", e);
        } catch (IOException e) {
            Log.e("IO Exception while fetching messages", e);
        }

        List<Long> ids = null;
        if(result != null) {
            // TBD
            // ...
        }

        return ids;
    }

    @Override
    public List<Long> fetchMessages(long since)
    {
        if(Log.I) Log.i("MiniStatusMessageFetchClient.fetchMessages() since = " + since);

        Object result = null;
        try {
            // TBD:
            Map<String,Object> params = new HashMap<String, Object>();
            params.put("since", since);  // ???
            List<Object> objs = miniStatusWebClient.list(params);
            if(objs != null && objs.size() > 0) {
                // ....
            }
        } catch (RestApiException e) {
            Log.e("Failed to get messages for since = " + since, e);
        } catch (IOException e) {
            Log.e("IO Exception while fetching messages for since = " + since, e);
        }

        List<Long> ids = null;
        if(result != null) {
            // TBD
            // ...
        }

        return ids;
    }
}

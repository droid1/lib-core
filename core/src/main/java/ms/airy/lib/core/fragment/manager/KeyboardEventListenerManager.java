package ms.airy.lib.core.fragment.manager;

import android.app.Fragment;
import android.view.KeyEvent;

import ms.airy.lib.core.fragment.listener.KeyDownEventListener;
import ms.airy.lib.core.fragment.listener.KeyLongPressEventListener;
import ms.airy.lib.core.fragment.listener.KeyShortcutEventListener;
import ms.airy.lib.core.fragment.listener.KeyMultipleEventListener;
import ms.airy.lib.core.fragment.listener.KeyUpEventListener;

import java.util.ArrayList;
import java.util.List;


public final class KeyboardEventListenerManager extends EventListenerManager
{

    public KeyboardEventListenerManager()
    {
    }

    // Returning false means the parent activity still need to handle the event.
    // Returning true means that the parent activity need not, and should not,
    //    further handle the event.
    // If at least one of the fragments return true, it returns true
    //    without processing the rest of the fragments.

    public boolean handleKeyDownEvent(int keyCode, KeyEvent event)
    {
        for(Fragment f : getFragments()) {
            if(f instanceof KeyDownEventListener) {
                if(((KeyDownEventListener) f).onKeyDown(keyCode, event) ) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean handleKeyLongPressEvent(int keyCode, KeyEvent event)
    {
        for(Fragment f : getFragments()) {
            if(f instanceof KeyLongPressEventListener) {
                if(((KeyLongPressEventListener) f).onKeyLongPress(keyCode, event) ) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean handleKeyMultipleEvent(int keyCode, int repeatCount, KeyEvent event)
    {
        for(Fragment f : getFragments()) {
            if(f instanceof KeyMultipleEventListener) {
                if(((KeyMultipleEventListener) f).onKeyMultiple(keyCode, repeatCount, event) ) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean handleKeyShortcutEvent(int keyCode, KeyEvent event)
    {
        for(Fragment f : getFragments()) {
            if(f instanceof KeyShortcutEventListener) {
                if(((KeyShortcutEventListener) f).onKeyShortcut(keyCode, event) ) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean handleKeyUpEvent(int keyCode, KeyEvent event)
    {
        for(Fragment f : getFragments()) {
            if(f instanceof KeyUpEventListener) {
                if(((KeyUpEventListener) f).onKeyUp(keyCode, event) ) {
                    return true;
                }
            }
        }
        return false;
    }

}

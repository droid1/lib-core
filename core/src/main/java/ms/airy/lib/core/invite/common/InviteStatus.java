package ms.airy.lib.core.invite.common;


public final class InviteStatus
{
    public static final int STATUS_CREATED = 0;  // ??
    public static final int STATUS_SENT = (0x1 << 0);
    public static final int STATUS_RECEIVED = (0x1 << 1);
    public static final int STATUS_ACCEPTED = (0x1 << 2);
    public static final int STATUS_DECLINED = (0x1 << 3);
    public static final int STATUS_IGNORED = (0x1 << 4);
    public static final int STATUS_EXPIRED = (0x1 << 5);
    // ...

    private InviteStatus() {}

    public static boolean isOpen(int status)
    {
        return (status == STATUS_SENT
                || status == STATUS_RECEIVED);
    }

    public static boolean isClosed(int status)
    {
        return (status == STATUS_ACCEPTED
                || status == STATUS_DECLINED
                || status == STATUS_IGNORED
                || status == STATUS_EXPIRED);
    }

}

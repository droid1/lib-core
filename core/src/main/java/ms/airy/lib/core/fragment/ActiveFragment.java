package ms.airy.lib.core.fragment;


import android.app.Fragment;

import java.util.List;

/**
 * Can be implemented by a Fragment.
 */
public interface ActiveFragment
{
    boolean processContainerMessages(FragmentContainer container, String channel, List<FragmentMessage> messages);

}

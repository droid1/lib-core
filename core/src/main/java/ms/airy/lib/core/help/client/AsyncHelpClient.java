package ms.airy.lib.core.help.client;


/**
 */
public interface AsyncHelpClient
{
    void getSectionContent(long sectionId);
}

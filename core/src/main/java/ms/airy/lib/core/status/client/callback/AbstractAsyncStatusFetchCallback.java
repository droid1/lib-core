package ms.airy.lib.core.status.client.callback;

import ms.airy.lib.core.status.client.AsyncStatusFetchCallback;
import ms.airy.lib.core.status.common.StatusMessage;

import java.util.List;


/**
 */
public abstract class AbstractAsyncStatusFetchCallback implements AsyncStatusFetchCallback
{

    @Override
    public void onFetchStatusMessage(StatusMessage statusMessage, long id)
    {
        if(Log.I) Log.i("AbstractAsyncStatusFetchCallback.onFetchStatusMessage() statusMessage = " + statusMessage + "; id = " + id);

    }

    @Override
    public void onFetchNewMessages(List<Long> ids)
    {
        Log.i("AbstractAsyncStatusFetchCallback.onFetchNewMessages().");

    }

    @Override
    public void onFetchMessages(List<Long> ids, long since)
    {
        if(Log.I) Log.i("AbstractAsyncStatusFetchCallback.onFetchMessages() since = " + since);

    }

}

package ms.airy.lib.core.startup.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;
import java.util.Map;


// TBD:
// This is not about "user preferences".
// It is about SharedPreferences storage.
// It stores the app-related global user settings.
public final class StartupPrefsStoreManager
{
    // temporary
    private static final String SP_FILENAME_SUFFIX = ".startup_prefs";
    private static final String KEY_LAST_STARTUP_TIME = "last.startup.time";
    private static final String KEY_SUFFIX_STARTUP_COUNTER = ".startup.counter";
    // ...

    // TBD:
    private static final String KEY_SUFFIX_TASK_COUNTER = ".task.counter";
    // ...

    private static StartupPrefsStoreManager INSTANCE = null;
    public static StartupPrefsStoreManager getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new StartupPrefsStoreManager(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;

    // Separate Shared Preferences file.
    private final String sharedPrefsFile;
    private final SharedPreferences sharedPreferences;

    // "Cache"
    private Long lastStartupTime = null;
    // versionId -> counter.
    private final Map<Integer, Integer> startupCounterMap = new HashMap<Integer, Integer>();
    // taskId -> counter.
    private final Map<Long, Integer> runCounterMap = new HashMap<Long, Integer>();


    private StartupPrefsStoreManager(Context context)
    {
        this.context = context;
        sharedPrefsFile = context.getPackageName() + SP_FILENAME_SUFFIX;
        sharedPreferences = context.getSharedPreferences(sharedPrefsFile, Context.MODE_PRIVATE);
    }
    public String getSharedPrefsFile()
    {
        return sharedPrefsFile;
    }


    private static String getPrefKeyPrefixForVersion(int versionCode)
    {
        // temporary
        return "version-" + versionCode;
    }
    private static String getVersionCounterKey(int versionCode)
    {
        // temporary
        return getPrefKeyPrefixForVersion(versionCode) + KEY_SUFFIX_STARTUP_COUNTER;
    }

    public int getStartupCounterForVersion(int versionCode)
    {
        int count;
        if(startupCounterMap.get(versionCode) != null) {
            // Note the implications of this instance cache...
            count = startupCounterMap.get(versionCode);
        } else {
            count = sharedPreferences.getInt(getVersionCounterKey(versionCode), 0);
            if(Log.D) Log.d("StartupPrefsStoreManager: versionCode = " + versionCode + "; count = " + count);
            startupCounterMap.put(versionCode, count);
        }
        return count;
    }
    public boolean isFirstStartupForVersion(int versionCode)
    {
        int count = getStartupCounterForVersion(versionCode);
        boolean isFirstStartup = (count == 0);

//        // temporary
//        // When is the best time to call this ???
//        increaseStartupCounterForVersion(versionCode);
//        // temporary

        return isFirstStartup;
    }

    public void setStartupCounterForVersion(int versionCode, int count)
    {
        setStartupCounterForVersion(versionCode, count, false);
    }
    public void setStartupCounterForVersion(int versionCode, int count, boolean force)
    {
        if(force || startupCounterMap.get(versionCode) == null || startupCounterMap.get(versionCode) != count) {
            startupCounterMap.put(versionCode, count);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt(getVersionCounterKey(versionCode), count);
            editor.commit();
        }
    }
    public void increaseStartupCounterForVersion(int versionCode)
    {
        int count = getStartupCounterForVersion(versionCode);
        setStartupCounterForVersion(versionCode, ++count);
    }



    // Note: This can be called only at the beginning of the app/startup.
    // Once setLastStartupTime() is called, getLastStartupTime() will not return the "last" time.
    // Rather it'll return the "current' startup time...
    public long getLastStartupTime()
    {
        // Note the implications of this instance cache...
        if(this.lastStartupTime == null) {
            long lastStartupTime = sharedPreferences.getLong(KEY_LAST_STARTUP_TIME, 0L);
            if (Log.D) Log.d("StartupPrefsStoreManager: lastStartupTime = " + lastStartupTime);
            this.lastStartupTime = lastStartupTime;
        }
        return this.lastStartupTime;
    }
    public boolean isFirstStartup()
    {
        long lastStartupTime = getLastStartupTime();
        boolean isFirstStartup = (lastStartupTime == 0L);

//        // temporary
//        // When is the best time to call this ???
//        // By who????
//        setLastStartupTime();
//        // temporary

        return isFirstStartup;
    }

    public void setLastStartupTime()
    {
        setLastStartupTime(System.currentTimeMillis());
    }
    public void setLastStartupTime(long lastStartupTime)
    {
        setLastStartupTime(lastStartupTime, false);
    }
    public void setLastStartupTime(long lastStartupTime, boolean force)
    {
        if(force || this.lastStartupTime == null || this.lastStartupTime != lastStartupTime) {
            this.lastStartupTime = lastStartupTime;
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putLong(KEY_LAST_STARTUP_TIME, lastStartupTime);
            editor.commit();
        }
    }


    private static String getPrefKeyPrefixForTask(long taskId)
    {
        // temporary
        return "task-" + taskId;
    }
    private static String getTaskCounterKey(long taskId)
    {
        // temporary
        return getPrefKeyPrefixForTask(taskId) + KEY_SUFFIX_TASK_COUNTER;
    }


    public int getTaskRunCounter(long taskId)
    {
        int count;
        if(runCounterMap.get(taskId) != null) {
            count = runCounterMap.get(taskId);
        } else {
            count = sharedPreferences.getInt(getTaskCounterKey(taskId), 0);
            if(Log.D) Log.d("StartupPrefsStoreManager: taskId = " + taskId + "; count = " + count);
            runCounterMap.put(taskId, count);
        }
        return count;
    }
    public boolean isTaskNew(long taskId)
    {
        int count = getTaskRunCounter(taskId);
        return (count == 0);
    }

    public void setTaskRunCounter(long taskId, int count)
    {
        setTaskRunCounter(taskId, count, false);
    }
    public void setTaskRunCounter(long taskId, int count, boolean force)
    {
        if(force || runCounterMap.get(taskId) == null || runCounterMap.get(taskId) != count) {
            runCounterMap.put(taskId, count);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt(getTaskCounterKey(taskId), count);
            editor.commit();
        }
    }
    public void increaseTaskRunCounter(long taskId)
    {
        int count = getTaskRunCounter(taskId);
        setTaskRunCounter(taskId, ++count);
    }


    // For debugging/testing purposes
    protected void removePrefsForVersion(int versionCode)
    {
        Log.w("======== StartupPrefsStoreManager.removePrefsForVersion(). versionCode = " + versionCode);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(getVersionCounterKey(versionCode));
        editor.commit();
    }
    protected void removePrefsForTask(long taskId)
    {
        Log.w("======== StartupPrefsStoreManager.removePrefsForTask(). taskId = " + taskId);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(getTaskCounterKey(taskId));
        editor.commit();
    }
    protected void removeAllPrefs()
    {
        Log.w("======== StartupPrefsStoreManager.removeAllPrefs()");

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }


}

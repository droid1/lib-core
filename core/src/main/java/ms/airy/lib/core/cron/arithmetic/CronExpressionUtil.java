package ms.airy.lib.core.cron.arithmetic;

import ms.airy.lib.core.cron.CronExpressionParser;


// TBD
public final class CronExpressionUtil
{
    private CronExpressionUtil() {}


    // Note: This will not work.
    // A CronExpression shifted is, in general, not a cron expression.
    // (I think, however, that
    //     List<cron expression> can be shifted to another List<cron expression>
    //     although there will not be in general one-to-one match
    //     between the expressions in the two lists......)
    // ...
    // For now,
    // just use CronExpressionParser.getNextTimeShifted()...
    public String shiftCronTime(String cronExpression, int minutes)
    {
        CronExpressionParser parser = new CronExpressionParser(cronExpression);

        // ????


        return null;
    }


}

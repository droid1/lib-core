package ms.airy.lib.core.help.common;

import java.io.Serializable;


/**
 */
public class TimestampSectionPair implements Serializable
{
    private HelpSection helpSection;
    private long expirationTime;      // Null means it has not expired. 0L means it has expired.
    // origin? online vs local ???

    public TimestampSectionPair()
    {
        this(null);
    }
    public TimestampSectionPair(HelpSection helpSection)
    {
        this(helpSection, 0L);
    }
    public TimestampSectionPair(HelpSection helpSection, long expirationTime)
    {
        this.helpSection = helpSection;
        this.expirationTime = expirationTime;
    }

    public HelpSection getHelpSection()
    {
        return helpSection;
    }
    public void setHelpSection(HelpSection helpSection)
    {
        this.helpSection = helpSection;
    }

    public long getExpirationTime()
    {
        return expirationTime;
    }
    public void setExpirationTime(long expirationTime)
    {
        this.expirationTime = expirationTime;
    }


    // For debugging...
    @Override
    public String toString()
    {
        return "TimestampSectionPair{" +
                "helpSection=" + helpSection +
                ", expirationTime=" + expirationTime +
                '}';
    }

}

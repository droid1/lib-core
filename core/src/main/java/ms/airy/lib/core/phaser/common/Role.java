package ms.airy.lib.core.phaser.common;


// set of "binary" user roles.
public final class Role
{
    // User's "state".
    // "Two-Bit" masks. 0x1==presence, 0x2==absence of a trait, for each pair of bits.
    //                  0x00 means don't care. 0x11 is invalid.
    public static final long USER_ADMIN = 0x1 << 0;
    public static final long NOT_USER_ADMIN = 0x1 << 1;
    // etc..

    // All bits off.
    public static final long ROLE_NONE = 0;


    // static methods only.
    private Role() {}


}

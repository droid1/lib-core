package ms.airy.lib.core.status.common;

import ms.airy.lib.core.util.IdUtil;


// Status,
//   Used for both sending and receiving.
public class StatusMessage
{
    private final long id;
    // private String title;    // optional
    // private String sender;   // me in case of sending status.
    // private String recipient;   // targeted status messsage???
    private final int type;        // status type ???
    private String message;        // TBD: max length????
    private MetaData metaData;
    // private long createdTime;   // ???
    // private long receivedTime;   // ???

    public StatusMessage()
    {
        this(StatusType.TYPE_ACTIVITY);
    }

    public StatusMessage(int type)
    {
        this(type, null);
    }
    public StatusMessage(int type, String message)
    {
        this(type, message, null);
    }
    public StatusMessage(int type, String message, MetaData metaData)
    {
        this(IdUtil.generateRandomId(), type, message, metaData);
    }

    public StatusMessage(String message)
    {
        this(message, null);
    }
    public StatusMessage(String message, MetaData metaData)
    {
        this(IdUtil.generateRandomId(), StatusType.TYPE_ACTIVITY, message, metaData);
    }

    public StatusMessage(long id)
    {
        this(id, StatusType.TYPE_ACTIVITY);
    }
    public StatusMessage(long id, int type)
    {
        this(id, type, null);
    }

    public StatusMessage(long id, String message)
    {
        this(id, message, null);
    }
    public StatusMessage(long id, String message, MetaData metaData)
    {
        this(id, StatusType.TYPE_ACTIVITY, message, metaData);
    }

    public StatusMessage(long id, int type, String message)
    {
        this(id, type, message, null);
    }
    public StatusMessage(long id, int type, String message, MetaData metaData)
    {
        this.id = id;
        this.type = type;
        this.message = message;
        this.metaData = metaData;
    }


    public long getId()
    {
        return id;
    }

    public int getType()
    {
        return type;
    }

    public String getMessage()
    {
        return message;
    }
    public void setMessage(String message)
    {
        this.message = message;
    }

    public MetaData getMetaData()
    {
        return metaData;
    }
    public void setMetaData(MetaData metaData)
    {
        this.metaData = metaData;
    }


    // For debugging....
    @Override
    public String toString()
    {
        return "StatusMessage{" +
                "id=" + id +
                ", type=" + type +
                ", message='" + message + '\'' +
                ", metaData=" + metaData +
                '}';
    }


    // Detail information about this status message.
    // e.g., subtype (e.g., match win, etc.), origin, sender, timestamp, etc...
    public static final class MetaData
    {
        // TBD:

    }

}

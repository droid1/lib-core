package ms.airy.lib.core.startup.task;

import android.content.Context;

import java.io.Serializable;


public class BaseStartupTask extends AbstractStartupTask implements StartupTask, Serializable
{
    private final Context context;

    public BaseStartupTask(Context context)
    {
        super();
        this.context = context;
    }
    public BaseStartupTask(Context context, long id)
    {
        super(id);
        this.context = context;
    }

    public Context getContext()
    {
        return context;
    }

    // TBD:
    // ...

}

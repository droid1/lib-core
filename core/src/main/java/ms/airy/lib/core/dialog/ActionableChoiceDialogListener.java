package ms.airy.lib.core.dialog;


/**
 * To be used for Single choice or multiple choice item list dialog.,
*/
public interface ActionableChoiceDialogListener extends ActionableAssertDialogListener  // ActionableAlertDialog
{
    // In case of a single choice list, isChecked is always true.
    void onDialogItemClick(String tag, int which, boolean isChecked);
}

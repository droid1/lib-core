package ms.airy.lib.core.auth.ui;

import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.List;
import java.util.Set;

import ms.airy.lib.core.R;
import ms.airy.lib.core.auth.common.AccountInfo;
import ms.airy.lib.core.auth.common.AccountType;
import ms.airy.lib.core.auth.config.AppAuthConfigManager;
import ms.airy.lib.core.auth.helper.AccountHelper;
import ms.airy.lib.core.auth.helper.AccountInfoListHelper;
import ms.airy.lib.core.auth.helper.AccountPicker;
import ms.airy.lib.core.auth.helper.AccountTypeUtil;
import ms.airy.lib.core.auth.helper.AuthHelper;
import ms.airy.lib.core.auth.helper.AuthResultCallback;
import ms.airy.lib.core.auth.ui.data.AccountInfoRowAdapter;
import ms.airy.lib.core.toast.ToastHelper;


public class BaseAccountConsoleFragment extends ListFragment implements AccountPicker.AccountPickerCallback, AuthResultCallback, AccountInfoRowAdapter.AccountInfoRowEventCallback
{


    // tbd

    // tbd: just use getListAdapter() ????
    private AccountInfoRowAdapter adapter = null;

    public BaseAccountConsoleFragment()
    {
    }


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState)
    {
//        // here vs. onActivityCreated() ???
//        List<AccountInfo> accountInfoList = AccountInfoListHelper.getInstance(inflater.getContext()).getAccountInfoList();
//        adapter = new AccountInfoRowAdapter(inflater.getContext(), accountInfoList, R.layout.list_item_account_console_account_info);
//        adapter.addAccountInfoRowEventCallback(this);
//        setListAdapter(adapter);

        // Set the authHelper callback here ???
        // AuthHelper.getInstance(inflater.getContext()).addAuthResultCallback(this);

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        // here vs. onCreateView() ???
        List<AccountInfo> accountInfoList = AccountInfoListHelper.getInstance(getActivity()).getAccountInfoList();
        adapter = new AccountInfoRowAdapter(getActivity(), accountInfoList, R.layout.list_item_account_console_account_info);
        adapter.addAccountInfoRowEventCallback(this);
        setListAdapter(adapter);

        // Set the authHelper callback.
        AuthHelper.getInstance(getActivity()).addAuthResultCallback(this);

    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id)
    {
        // super.onListItemClick(l, v, position, id);
        Log.w("onListItemClick(). position = " + position + "; id = " + id);

//        AccountInfoRowAdapter adapter = (AccountInfoRowAdapter) l.getAdapter();
        AccountInfo accountInfo = (AccountInfo) adapter.getItem(position);

        int accountType = accountInfo.getAccountType();
        Log.w("accountType = " + accountType);
        showAccountPicker(accountType);
    }


    // Note that, in this use case,
    //   accountPicker dialog tag is not needed since accountType is sufficient
    //   to distinguish the origin of the callback events.
    private void showAccountPicker(int accountType)
    {
        AccountPicker accountPicker = new AccountPicker(getActivity(), this, accountType);
        accountPicker.pickAccount(getActivity());
    }


    /////////////////////////////////////////////////
    // AccountPickHelper.AccountPickCallback interface

    @Override
    public void onAccountPicked(String tag, int type, String name)
    {
        if(Log.I) Log.i("BaseAccountConsoleFragment.onAccountPicked() tag = " + tag + "; type = " + type + "; name = " + name);

        // Main account selected.
        if(AccountTypeUtil.isValid(type)) {
            if(name != null && !name.isEmpty() && !name.equals(AccountType.NAME_NONE)) {

                String oldName = AccountHelper.getInstance(getActivity()).getMainAccountName(type);
                if(name.equals(oldName)) {
                    // No need to change anything..
                } else {

                    // set the new main account.
                    AccountHelper.getInstance(getActivity()).setMainAccount(type, name);

                    // Update the ui.
                    // ????
//                    AccountInfo accountInfo = (AccountInfo) adapter.getItem(position);
                    AccountInfo accountInfo = adapter.getAccountInfo(type, oldName);
                    accountInfo.setAccountName(name);
                    accountInfo.setUnauthorized();     // ????
                    // ???
                    // invalidate();

                    // ???
                    adapter.notifyDataSetChanged();
                    // ???
                }
            } else {
                // ???
                // what to do??
            }
        } else {
            // This cannot happen...
        }
    }

    @Override
    public void onAccountPickCanceled(String tag, int type)
    {
        if(Log.I) Log.i("BaseAccountConsoleFragment.onAccountPickCanceled(). tag = " + tag + "; type = " + type);

        // Do nothing..
    }


    /////////////////////////////////////////////////
    // AuthResultCallback interface

    @Override
    public void onAuthorizationSuccess(int type, String name, String authToken, String authScopes, long authTime)
    {
        if(Log.I) Log.i("BaseAccountConsoleFragment.onAuthorizationSuccess(). type = " + type + "; name = " + name + "; authScopes =" +  authScopes + "; authTime = " + authTime);

        // ???
        // This crashes if we use a different thread handler...
        // adapter.notifyDataSetChanged();
        // ...
    }

    @Override
    public void onAuthorizationFailure(int type, String name, String reason)
    {
        if(Log.I) Log.i("BaseAccountConsoleFragment.onAuthorizationFailure(). type = " + type + "; name = " + name + "; reason = " + reason);

        // ???
        // This crashes if we use a different thread handler...
        // adapter.notifyDataSetChanged();
        // ...
    }



    /////////////////////////////////////////////////
    // AccountInfoRowAdapter.AccountInfoRowEventCallback interface

    @Override
    public void onPrimaryRadioClick(int position)
    {
        Log.w("BaseAccountConsoleFragment.onPrimaryRadioClick() position = " + position);

        for(int i=0; i<adapter.getCount(); i++) {
            AccountInfo ai = (AccountInfo) adapter.getItem(i);
            ai.setPrimary(false);
        }
        // ???
        AccountInfo accountInfo = (AccountInfo) adapter.getItem(position);
        accountInfo.setPrimary(true);

        String accountName = accountInfo.getAccountName();
        AccountHelper.getInstance(getActivity()).setPrimaryAccount(accountInfo.getAccountType(), accountName);

        // ???
        adapter.notifyDataSetChanged();
        // ???

        String accountNameForDisplay = AccountTypeUtil.getTypeName(accountInfo.getAccountType()) + ": " + accountName;
        ToastHelper.getInstance(getActivity()).showToast("Primary account changed to " + accountNameForDisplay);
//      // ToastManager.showToast(getActivity(), "Primary account changed to " + accountNameForDisplay);

    }
    // TBD:
    private void onPrimaryRadioClick(int accountType, String accountName)
    {
        Log.w("BaseAccountConsoleFragment.onPrimaryRadioClick() accountType = " + accountType + "; accountName = " + accountName);


        AccountHelper.getInstance(getActivity()).setPrimaryAccount(accountType, accountName);

        for(int i=0; i<adapter.getCount(); i++) {
            AccountInfo ai = (AccountInfo) adapter.getItem(i);
            ai.setPrimary(false);
        }
        // ???
        AccountInfo accountInfo = adapter.getAccountInfo(accountType, accountName);
        accountInfo.setPrimary(true);

        // ???
        adapter.notifyDataSetChanged();
        // ???
    }

    @Override
    public void onAuthButtonClick(int position, boolean authRequested)
    {
        Log.w("BaseAccountConsoleFragment.onAuthButtonClick() position = " + position + "; authRequested = " + authRequested);

        // ???
        AccountInfo accountInfo = (AccountInfo) adapter.getItem(position);
        int accountType = accountInfo.getAccountType();
        String accountName = accountInfo.getAccountName();
        String accountNameForDisplay = AccountTypeUtil.getTypeName(accountInfo.getAccountType()) + ": " + accountName;

        if(authRequested) {
            // TBD:
            // get default auth scopes from config, etc..

            Set<String> authScopes = AppAuthConfigManager.getInstance(getActivity()).getApplicationAuthConfig().getScopeSet(accountType);
            // Validate?
            if(authScopes == null) {
                // ????
            }
            // ....
            // Note that the callback needs to be set across all rows.
            // AuthHelper.getInstance(getActivity()).addAuthResultCallback(this);

            // ???
            // AuthHelper.getInstance(getActivity()).authorizeForAccount(accountType, accountName, authScopes);
            AuthHelper.getInstance(getActivity()).authorizeForAccount(accountType, accountName, authScopes, getActivity());
            // ???
        } else {
            // AuthHelper.getInstance(getActivity()).addAuthResultCallback(this);
            AuthHelper.getInstance(getActivity()).unauthorizeForAccount(accountType, accountName);
        }

    }
    // TBD:
    private void onAuthButtonClick(int accountType, String accountName, boolean authRequested)
    {
        Log.w("BaseAccountConsoleFragment.onAuthButtonClick() accountType = " + accountType + "; accountName = " + accountName + "; authRequested = " + authRequested);

        // ???
        // ...
    }

}

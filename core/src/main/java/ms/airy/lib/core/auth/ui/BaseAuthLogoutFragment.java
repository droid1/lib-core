package ms.airy.lib.core.auth.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ms.airy.lib.core.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class BaseAuthLogoutFragment extends Fragment
{

    public BaseAuthLogoutFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_base_auth_logout, container, false);
        return rootView;
    }
}

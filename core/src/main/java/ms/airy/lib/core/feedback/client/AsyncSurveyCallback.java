package ms.airy.lib.core.feedback.client;

import ms.airy.lib.core.feedback.common.SurveyEntry;


public interface AsyncSurveyCallback
{
    void onGetSpecialSurvey(SurveyEntry surveyEntry);
    void onGetDailySurvey(SurveyEntry surveyEntry, long time);
    void onGetSurvey(SurveyEntry surveyEntry, long id);
}

package ms.airy.lib.core.config.client.impl;


/**
 */
public abstract class AbstractOnlineConfigClient 
{
    // TBD:
    private String configWebServiceEndpoint;

    public AbstractOnlineConfigClient()
    {
        this(null);  // ????
    }
    protected AbstractOnlineConfigClient(String configWebServiceEndpoint)
    {
        this.configWebServiceEndpoint = configWebServiceEndpoint;
    }

    public String getConfigWebServiceEndpoint()
    {
        return configWebServiceEndpoint;
    }
    protected void setConfigWebServiceEndpoint(String configWebServiceEndpoint)
    {
        this.configWebServiceEndpoint = configWebServiceEndpoint;
        // TBD:
        // reset the miniclient...
        // ...
    }


}

package ms.airy.lib.core.invite.client;

import java.util.List;

import ms.airy.lib.core.invite.common.InviteInfo;
import ms.airy.lib.core.invite.common.RecipientInfo;


public interface BuddyInviteClient
{
    // Returns the invite id.
    // "recipient" is tha email address of the invitee.
    long sendInvite(String recipient);

    // Returns the invite status/data.
    InviteInfo getInviteInfo(long inviteId);

    // Update invites?
    // ....
    // Cancel invites?
    // void cancelInvite(long inviteId);

    // Returns the invite list of the users.
//    List<Long> getAllInvites();
//    List<Long> getOpenInvites();
    List<Long> getInvites(int status);

//    // ??? One invite per recipient?
//    // List<Long> getInvitesForRecipient(String recipient);
//    InviteInfo getInviteInfoForRecipient(String recipient);

    // Returns the recipient data.
    RecipientInfo getRecipientInfo(String recipient);

//    // Returns all recipients of this user.
//    List<String> getAllRecipients();
//    // Returns the list of recipients who have accepted the invites.
//    List<String> getAcceptedRecipients();
//    // Returns the list of recipients who have declined the invites.
//    List<String> getDeclinedRecipients();
//    // Returns the list of recipients who have open invites invites.
//    List<String> getOpenRecipients();

    // Returns all recipients of this user, with the given status: accepted, open closed.
    List<String> getRecipients(int status);

    // etc..
    // ...
}

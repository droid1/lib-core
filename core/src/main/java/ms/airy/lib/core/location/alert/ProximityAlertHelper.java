package ms.airy.lib.core.location.alert;

import android.content.Context;
import android.location.LocationManager;

import ms.airy.lib.core.location.prefs.LocationPrefsStoreManager;


//
public final class ProximityAlertHelper
{
    private static ProximityAlertHelper INSTANCE = null;
    public static ProximityAlertHelper getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new ProximityAlertHelper(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;
    // Is it safe to keep the reference of LocationManager?
    private LocationManager locationManager = null;

    private ProximityAlertHelper(Context context)
    {
        this.context = context;
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
    }



    // TBD:
    // ....


    public void addProximityAlert()
    {

    }



}

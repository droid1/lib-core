package ms.airy.lib.core.auth.ui.data;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ms.airy.lib.core.R;
import ms.airy.lib.core.auth.common.AccountInfo;
import ms.airy.lib.core.auth.common.AccountType;
import ms.airy.lib.core.auth.config.AppAuthConfigManager;
import ms.airy.lib.core.auth.helper.AccountTypeUtil;
import ms.airy.lib.core.auth.helper.AuthHelper;
import ms.airy.lib.core.dialog.ActionableNoticeDialogListener;
import ms.airy.lib.core.dialog.stock.SimpleConfirmDialogFragment;


public class AccountInfoRowAdapter extends BaseAdapter implements ListAdapter
{
//    // The field names are from the AccountInfo class.
//    public static final String[] FIELDS = new String[]{"primary", "accountType", "accountName"};

//    public AccountInfoRowAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to)
//    {
//        super(context, data, resource, from, to);
//    }


    private final Context context;
    private List<? extends AccountInfo> accountInfoList = null;
    private int layoutResId;

    public AccountInfoRowAdapter(Context context, List<? extends AccountInfo> data)
    {
        this(context, data, 0);
    }

    public AccountInfoRowAdapter(Context context, List<? extends AccountInfo> data, int layoutResId)
    {
        this.context = context;
        accountInfoList = data;
        if (layoutResId == 0) {
            this.layoutResId = R.layout.list_item_account_console_account_info;
        } else {
            this.layoutResId = layoutResId;
        }
    }

    // ???
    // Returns the first AccountInfo (and, it should be the only one) that has the same type/name.
    public AccountInfo getAccountInfo(int type, String name)
    {
        // temporary
        // tbd: Can accountInfoList be null?
        for(AccountInfo ai : accountInfoList) {
            if(ai.getAccountType() == type
                    && ((name == null && ai.getAccountName() == null) || (name != null && name.equals(ai.getAccountName())))) {
                return ai;
            }
        }
        return null;
    }

    @Override
    public int getCount()
    {
        return accountInfoList.size();
    }

    @Override
    public Object getItem(int position)
    {
        return accountInfoList.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return accountInfoList.get(position).getId();
    }


    // what is this???
    private static class ViewHolder {
        RadioButton rbPrimary;
        TextView tvAccountType;
        TextView tvAccountName;
        Button bnAuth;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        // ???
        View rowView = null;
        ViewHolder holder = new ViewHolder();
        if(convertView != null) {
            Log.i("convertView is being re-used...");
            rowView = convertView;
            holder = (ViewHolder) convertView.getTag();
        } else {

            LayoutInflater inflater = null;
            if(context instanceof Activity) {
                inflater = ((Activity) context).getLayoutInflater();
            } else {
                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }

            // rowView = inflater.inflate(layoutResId, parent);   // ???
            rowView = inflater.inflate(layoutResId, null);
            holder.rbPrimary = (RadioButton) rowView.findViewById(R.id.account_info_is_primary);
            holder.tvAccountType = (TextView) rowView.findViewById(R.id.account_info_account_type);
            holder.tvAccountName = (TextView) rowView.findViewById(R.id.account_info_account_name);
            holder.bnAuth = (Button) rowView.findViewById(R.id.account_info_account_auth);

            // TBD:
            // Add button click listeners???
            // ....

            rowView.setTag(holder);
        }


        AccountInfo accountInfo = accountInfoList.get(position);
        int accountType = accountInfo.getAccountType();
        String accountName = accountInfo.getAccountName();
        if(accountName == null) {    // No "main" account set for this accountType...
            accountName = "";   // ??

            // temporary
            accountName = AccountType.NAME_NONE;
            // temporary
        }

        if(accountName == null
                || accountName.isEmpty()
                || accountName.equals(AccountType.NAME_NONE)    // temporary
                ) {
            holder.rbPrimary.setEnabled(false);
            holder.rbPrimary.setChecked(false);
        } else {
            holder.rbPrimary.setEnabled(true);
            if(accountInfo.isPrimary()) {
                holder.rbPrimary.setChecked(true);
            } else {
                holder.rbPrimary.setChecked(false);
            }

            // temporary
            holder.rbPrimary.setOnClickListener(new PrimaryRadioClickListener(context, callbacks, position));
//            holder.rbPrimary.setOnClickListener(new PrimaryRadioClickListener(callbacks, accountType, accountName));
            // temporary
        }

        String typeName = AccountTypeUtil.getTypeName(accountType);
        if(typeName == null) {
            typeName = "";   // ??
        }
        holder.tvAccountType.setText(typeName);

        holder.tvAccountName.setText(accountName);

        // TBD:
        boolean authDisabled = AppAuthConfigManager.getInstance(context).getApplicationAuthConfig().isApplicationAuthDisabled();
        if(authDisabled == true) {
            // Hide the auth buttons..
            // Or,
            // Hide the whole column...
            // ...
            // This should be done somewhere else
            // ...
        }
        // ....

        if(accountName == null
                || accountName.isEmpty()
                || accountName.equals(AccountType.NAME_NONE)    // temporary
                ) {
            holder.bnAuth.setEnabled(false);
            holder.bnAuth.setVisibility(View.INVISIBLE);
        } else {
            holder.bnAuth.setEnabled(true);
            holder.bnAuth.setVisibility(View.VISIBLE);

            boolean useDialog = false;

            boolean isAuthorized = accountInfo.isAuthorized();
            if(isAuthorized) {
                // temporary
                holder.bnAuth.setBackgroundColor(Color.GRAY);
                // ...
                holder.bnAuth.setText("Log out");   // TBD
            } else {
                boolean attemptedBefore = AuthHelper.getInstance(context).isAuthAttemptedForAccount(accountType, accountName);
                if(attemptedBefore) {   // attempted and failed.
                    // temporary
                    holder.bnAuth.setBackgroundColor(Color.RED);
                    // ...

                    // Show previous message, if any???
                    String failureReason = AuthHelper.getInstance(context).getLastAuthFailureReason(accountType, accountName);
                    if(failureReason != null && !failureReason.isEmpty()) {
                        // ???
                    } else {
                        // ???
                    }

                    // ???
                    useDialog = true;
                    // Or, just use the "activity" version of the getAuthToken() ????
                    // ....
                } else {
                    // temporary
                    holder.bnAuth.setBackgroundColor(Color.WHITE);
                    // ...

                    // ???
                    useDialog = false;
                }

                holder.bnAuth.setText("Log in");    // TBD
            }

            // temporary
            holder.bnAuth.setOnClickListener(new AuthButtonClickListener(context, callbacks, position, !isAuthorized, useDialog));
//            holder.bnAuth.setOnClickListener(new AuthButtonClickListener(callbacks, accountType, accountName, !isAuthorized));
            // temporary
        }

        return rowView;
    }



    //////////////////////////////////////////
    // Event handlers

    private static class PrimaryRadioClickListener implements View.OnClickListener
    {
        private final Context context;
        private final Set<AccountInfoRowEventCallback> callbacks;
        private final int position;
//        private int accountType;
//        private String accountName;

        private PrimaryRadioClickListener(Context context,
                                          Set<AccountInfoRowEventCallback> callbacks,
                                          int position)
        {
            this.context = context;
            this.callbacks = callbacks;
            this.position = position;
        }
//        private PrimaryRadioClickListener(Set<AccountInfoRowEventCallback> callbacks,
//                                          int accountType, String accountName)
//        {
//            this.callbacks = callbacks;
//            this.accountType = accountType;
//            this.accountName = accountName;
//        }

        @Override
        public void onClick(View v)
        {
            Log.d("PrimaryRadioClickListener.onClick().");

//            ToastHelper.getInstance(context).showToast("Changing primary account...");
//            // ToastManager.showToast(context, "Changing primary account...");

            for (AccountInfoRowEventCallback c : callbacks) {
                c.onPrimaryRadioClick(position);
//                c.onPrimaryRadioClick(accountType, accountName);
            }
        }
    }

    private static class AuthButtonClickListener implements View.OnClickListener
    {
        // temporary
        private static final String AUTH_DIALOG_TAG = "auth_dialog";

        private final Context context;
        private final Set<AccountInfoRowEventCallback> callbacks;
        private final int position;
//        private int accountType;
//        private String accountName;
        private boolean authRequested;
        private boolean useDialog;

        private AuthButtonClickListener(Context context,
                                        Set<AccountInfoRowEventCallback> callbacks,
                                        int position,
                                        boolean authRequested,
                                        boolean useDialog)
        {
            this.context = context;
            this.callbacks = callbacks;
            this.position = position;
            this.authRequested = authRequested;
            this.useDialog = useDialog;
        }
//        private AuthButtonClickListener(Context context, Set<AccountInfoRowEventCallback> callbacks,
//                                        int accountType, String accountName, boolean authRequested, boolean useDialog)
//        {
//            this.callbacks = callbacks;
//            this.accountType = accountType;
//            this.accountName = accountName;
//            this.authRequested = authRequested;
//            this.useDialog = useDialog;
//        }

        @Override
        public void onClick(View v)
        {
            Log.d("AuthButtonClickListener.onClick().");

            if(useDialog && (context instanceof Activity)) {
                // TBD:
                // Use "standard" auth/login dialog???
                // ...

                // temporary
                String title = "Auth Dialog";
                // String message = "Please authorize for " + AccountType.getTypeName(accountType);
                String message = "Please authorize.";
                // ....
                DialogFragment fragment = SimpleConfirmDialogFragment.newInstance(title, message)
                        .addActionable(new ActionableNoticeDialogListener()
                        {
                            @Override
                            public void onNeutralClick(String tag) {}
                            @Override
                            public void onPositiveClick(String tag)
                            {
                                for (AccountInfoRowEventCallback c : callbacks) {
                                    c.onAuthButtonClick(position, authRequested);
//                                    c.onAuthButtonClick(accountType, accountName, authRequested);
                                }
                            }
                        });
                fragment.show(((Activity) context).getFragmentManager(), "auth_dialog");
            } else {
                for (AccountInfoRowEventCallback c : callbacks) {
                    c.onAuthButtonClick(position, authRequested);
//                    c.onAuthButtonClick(accountType, accountName, authRequested);
                }
            }
        }
    }





    //////////////////////////////////////////
    // For "relaying" ActionableChoiceDialog.onDialogItemClick() ....

    public static interface AccountInfoRowEventCallback
    {
//        void onPrimaryRadioClick(int accountType, String accountName);
//        void onAuthButtonClick(int accountType, String accountName, boolean authRequested);
        void onPrimaryRadioClick(int position);
        void onAuthButtonClick(int position, boolean authRequested);
    }

    // We allow multiple callbacks, but most likely, the client only needs one.
    private final Set<AccountInfoRowEventCallback> callbacks = new HashSet<AccountInfoRowEventCallback>();
    public final void addAccountInfoRowEventCallback(AccountInfoRowEventCallback callback)
    {
        callbacks.add(callback);
    }


}

package ms.airy.lib.core.cron;


/**
 * Simple wrapper around IllegalArgumentException.
 * Place holder for now...
*/
public final class CronExpressionParseException extends IllegalArgumentException
{
    private static final long serialVersionUID = 1L;

    public CronExpressionParseException()
    {
        super();
    }
    public CronExpressionParseException(String message, Throwable cause)
    {
        super(message, cause);
    }
    public CronExpressionParseException(String s)
    {
        super(s);
    }
    public CronExpressionParseException(Throwable cause)
    {
        super(cause);
    }

}

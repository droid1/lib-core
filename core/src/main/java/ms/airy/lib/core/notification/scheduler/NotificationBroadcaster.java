package ms.airy.lib.core.notification.scheduler;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import java.util.Date;
import java.util.Set;

import ms.airy.lib.core.cron.MultiCronHelper;
import ms.airy.lib.core.notification.NotificationBuilder;
import ms.airy.lib.core.notification.common.NotificationAndTimeStruct;
import ms.airy.lib.core.notification.common.NotificationConsts;
import ms.airy.lib.core.util.IdUtil;


// TBD:
// how to schedule notifications when the app has not run since the last reboot????
// ???
// Do this at bootup???
// Use "android.intent.action.BOOT_COMPLETED" ??? on BroadcastReceiver???
// <uses-permission android:name="android.permission.RECEIVE_BOOT_COMPLETED" />
// ...
// --> Well, the first statement is not entirely true
// since we have BroadcastReceiver, any notifications sent from other devices or clouds
//    will create notifications on this device....
// But, this "framework" of posting deplayed notifications will not work
//    unless the app has started at least once
//    so that we have a chance to schedule notifications...
// ....

public final class NotificationBroadcaster
{
    private static NotificationBroadcaster INSTANCE = null;
    public static NotificationBroadcaster getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new NotificationBroadcaster(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // To set the notification tag/id
    private String notificationTag;    // ???
//    private int notificationId;
    // Application context.
    private final Context context;
    // AlarmManager
    private final AlarmManager alarmManager;

    private NotificationBroadcaster(Context context)
    {
        this.notificationTag = null;
//        this.notificationId = 0;
        this.context = context;
        this.alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    }


    public String getNotificationTag()
    {
        return notificationTag;
    }
    public void setNotificationTag(String notificationTag)
    {
        this.notificationTag = notificationTag;
    }
    public void resetNotificationTag()
    {
        this.notificationTag = null;
    }


    // TBD:

    // jitter, if > 0, is used to set the "window", (notificationTime, notificationTime + jitter).
    // Note that jitter is non-negative.
    // If negative/advanced jitter is needed, modify the notificationTime.
    public void scheduleNotification(int requestCode, Class<? extends Activity> clss, String contentTitle, String contentText, int smallIconRes, long notificationTime)
    {
        scheduleNotification(requestCode, clss, contentTitle, contentText, smallIconRes, notificationTime, 0L);
    }
    public void scheduleNotification(int requestCode, Class<? extends Activity> clss, String contentTitle, String contentText, int smallIconRes, long notificationTime, long jitter)
    {
        Notification notification = NotificationBuilder.buildNotification(context, requestCode, clss, contentTitle, contentText, smallIconRes);
        scheduleNotification(notification, notificationTime, jitter);
    }

    public void scheduleNotification(PendingIntent contentIntent, String contentTitle, String contentText, int smallIconRes, long notificationTime)
    {
        scheduleNotification(contentIntent, contentTitle, contentText, smallIconRes, notificationTime, 0L);
    }
    public void scheduleNotification(PendingIntent contentIntent, String contentTitle, String contentText, int smallIconRes, long notificationTime, long jitter)
    {
        Notification notification = NotificationBuilder.buildNotification(context, contentIntent, contentTitle, contentText, smallIconRes);
        scheduleNotification(notification, notificationTime, jitter);
    }

    public void scheduleNotification(Notification notification, long notificationTime)
    {
        scheduleNotification(notification, notificationTime, 0L);
    }
    public void scheduleNotification(Notification notification, long notificationTime, long jitter)
    {
        scheduleNotification(0, notification, notificationTime, jitter);  // ???
    }
    public void scheduleNotification(int notificationId, Notification notification, long notificationTime)
    {
        scheduleNotification(notificationId, notification, notificationTime, 0L);
    }
    // "The" method.
    public void scheduleNotification(int notificationId, Notification notification, long notificationTime, long jitter)
    {
        if(Log.I) Log.i(">>>>>>>>> scheduleNotification(). notificationId = " + notificationId + "; jitter = " + jitter);


        // TBD:
        // check first if any notification alarm (at around the same time) has been created???
        // how to do this???


        // Is this necessary???
        long now = System.currentTimeMillis();
        if(notificationTime < now) {
            // ???
            Log.w("Notification time is in the past: notificationTime = " + notificationTime);
            return;
        }
        // ???

        // Scheduled notification uses the current time as the noti ID, if the id is not set.
        // This can potentially prevent duplicate notifications from being created.
        // And, possibly, allow us to cancel future notifications (e.g., using tag+id)...
        // --> Does this make sense???
        if(notificationId == 0) {
            notificationId = (int) (notificationTime / 1000L);
        }
        // ....

        // TBD:
        // This is necessary to support multiple alarms (e.g., repeated notifications from a cron.)
        // If you use the same requestCode, you'll see only one alarm (the last one).
        // We also tie the requestCode with the notificationTime so that
        //    if multiple requests (e.g., with repeated notifs) are made,
        //    then it might help avoid duplicate alarms.
        // (The problem is that if two different sources/modules (e.g., hangout and rps party)
        //    use similar cron expressions, they end up having the same second alarms.
        //  To avoid this, to add some random, but fixed, shifts to the notification times.
        //  Since the cron is defined down to minutes, +-30 second range should suffice.
        //  The module shift can be combined with the user pref shift/delayMillis....)
        //int requestCode = (int) IdUtil.generateRandomId05();  // ???
        int requestCode = (int) (notificationTime / 1000L);
        // ...

        // Notification intent, to be sent to the broadcast receiver...
        Intent intent = new Intent(NotificationConsts.ACTION_SCHEDULED_NOTIFICATION);

//        // TBD:
//        Uri uri = Uri.parse("notification:" + IdUtil.generateRandomId05());  // ???
//        intent.setData(uri);
//        // ...

        // How to "attach" the notification to the intent???
        // ...
        intent.putExtra(NotificationConsts.KEY_EXTRA_NOTIFICATION_ID, notificationId);
        intent.putExtra(NotificationConsts.KEY_EXTRA_NOTIFICATION, notification);
        if(this.notificationTag != null) {
            // Note: we use whatever is the current tag value for this singleton...
            // TBD: Does this make sense????
            intent.putExtra(NotificationConsts.KEY_EXTRA_NOTIFICATION_TAG, this.notificationTag);
        }
        // ...

        // PendingIntent broadcastIntent = PendingIntent.getBroadcast(context, requestCode, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        PendingIntent broadcastIntent = PendingIntent.getBroadcast(context, requestCode, intent, 0);

        // TBD:
        // This works only if target sdk >= 19.
//        if(jitter == 0L) {
//            this.alarmManager.setExact(AlarmManager.RTC, notificationTime, broadcastIntent);
//        } else {
//            this.alarmManager.setWindow(AlarmManager.RTC, notificationTime, jitter, broadcastIntent);
//        }
        this.alarmManager.set(AlarmManager.RTC, notificationTime, broadcastIntent);
        // this.alarmManager.set(AlarmManager.RTC_WAKEUP, notificationTime, broadcastIntent);
        if(Log.I) Log.i(">>>>>>>>>>>>> Alarm set: notificationTime = " + new Date(notificationTime).toString());
        // TBD:
    }


    public void scheduleNotifications(Set<NotificationAndTimeStruct> notificationTimePairs)
    {
        scheduleNotifications(notificationTimePairs, 0L);
    }
    // TBD: Allow different jitters for different pairs???
    public void scheduleNotifications(Set<NotificationAndTimeStruct> notificationTimePairs, long jitter)
    {
        long now = System.currentTimeMillis();
        for(NotificationAndTimeStruct pair : notificationTimePairs) {
            int id = pair.getNotificationId();
            Notification notification = pair.getNotification();
            long time = pair.getScheduledTime();
            if(time < now) {
                // ???
                continue;
            }
            scheduleNotification(id, notification, time, jitter);
        }
    }


    // temporary
    private static final int CRON_REPEATS = 10;

    // Note that all notifications in the cron repeats
    //           share the same notification ID.
    // delayMillis > 0 ==> delayed.
    // delayMillis < 0 ==> advanced.
    public void scheduleNotification(Notification notification, String cronExpression)
    {
        scheduleNotification(notification, cronExpression, 0L);
    }
    public void scheduleNotification(Notification notification, String cronExpression, long delayMillis)
    {
        scheduleNotification(notification, cronExpression, delayMillis, 0L);
    }
    public void scheduleNotification(Notification notification, String cronExpression, long delayMillis, long jitter)
    {
        scheduleNotification(0, notification, cronExpression, delayMillis, jitter);
    }
    public void scheduleNotification(int notificationId, Notification notification, String cronExpression)
    {
        scheduleNotification(notificationId, notification, cronExpression, 0L);
    }
    public void scheduleNotification(int notificationId, Notification notification, String cronExpression, long delayMillis)
    {
        scheduleNotification(notificationId, notification, cronExpression, delayMillis, 0L);
    }
    public void scheduleNotification(int notificationId, Notification notification, String cronExpression, long delayMillis, long jitter)
    {
        scheduleNotification(notificationId, notification, cronExpression, delayMillis, jitter, CRON_REPEATS);
    }
    public void scheduleNotification(Notification notification, String cronExpression, int repeats)
    {
        scheduleNotification(notification, cronExpression, 0L, repeats);
    }
    public void scheduleNotification(Notification notification, String cronExpression, long delayMillis, int repeats)
    {
        scheduleNotification(notification, cronExpression, delayMillis, 0L, repeats);
    }
    public void scheduleNotification(Notification notification, String cronExpression, long delayMillis, long jitter, int repeats)
    {
        scheduleNotification(0, notification, cronExpression, delayMillis, jitter, repeats);
    }
    public void scheduleNotification(int notificationId, Notification notification, String cronExpression, int repeats)
    {
        scheduleNotification(notificationId, notification, cronExpression, 0L, repeats);
    }
    public void scheduleNotification(int notificationId, Notification notification, String cronExpression, long delayMillis, int repeats)
    {
        scheduleNotification(notificationId, notification, cronExpression, delayMillis, 0L, repeats);
    }
    public void scheduleNotification(int notificationId, Notification notification, String cronExpression, long delayMillis, long jitter, int repeats)
    {
        scheduleNotification(notificationId, notification, cronExpression, delayMillis, jitter, repeats, false);
    }
    public void scheduleNotification(Notification notification, String cronExpression, int repeats, boolean useDifferentIds)
    {
        scheduleNotification(notification, cronExpression, 0L, repeats, useDifferentIds);
    }
    public void scheduleNotification(Notification notification, String cronExpression, long delayMillis, int repeats, boolean useDifferentIds)
    {
        scheduleNotification(notification, cronExpression, delayMillis, 0L, repeats, useDifferentIds);
    }
    public void scheduleNotification(Notification notification, String cronExpression, long delayMillis, long jitter, int repeats, boolean useDifferentIds)
    {
        scheduleNotification(0, notification, cronExpression, delayMillis, jitter, repeats, useDifferentIds);
    }
    public void scheduleNotification(int notificationId, Notification notification, String cronExpression, int repeats, boolean useDifferentIds)
    {
        scheduleNotification(notificationId, notification, cronExpression, 0L, repeats, useDifferentIds);
    }
    public void scheduleNotification(int notificationId, Notification notification, String cronExpression, long delayMillis, int repeats, boolean useDifferentIds)
    {
        scheduleNotification(notificationId, notification, cronExpression, delayMillis, 0L, repeats, useDifferentIds);
    }
    public void scheduleNotification(int notificationId, Notification notification, String cronExpression, long delayMillis, long jitter, int repeats, boolean useDifferentIds)
    {
        if(Log.I) Log.i(">>>>>>>>> scheduleNotification(). notificationId = " + notificationId + "; cronExpression = " + cronExpression + "; delayMillis = " + delayMillis + "; jitter = " + jitter + "; repeats = " + repeats + "; useDifferentIds = " + useDifferentIds);

        // ???
        int notiId = 0;
        if(notificationId > 0) {
            notiId = notificationId;
        } else {
            // temporary
            notiId = (int) IdUtil.generateRandomId10();
        }

        long nextTime = System.currentTimeMillis();
        for(int i=0; i<repeats; i++) {
            Log.i("i = " + i);

//            nextTime = MultiCronHelper.getInstance().getNextCronTime(cronExpression, nextTime + 1000L);
            nextTime = MultiCronHelper.getInstance().getNextCronTimeShifted(cronExpression, nextTime + 1000L, delayMillis);

            if(Log.I) Log.i(">>>>>>>>> scheduleNotification(). nextTime = " + nextTime);

            // "Clone" notifications?
            // ???

            // By default, we use the same notification Id for all notifications repeated...
            if(useDifferentIds) {
                scheduleNotification(notiId++, notification, nextTime, jitter);
            } else {
                scheduleNotification(notiId, notification, nextTime, jitter);
            }
        }
    }

}

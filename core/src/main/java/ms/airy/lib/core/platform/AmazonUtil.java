package ms.airy.lib.core.platform;

import android.app.Activity;
import android.app.Application;
import android.os.Build;


public final class AmazonUtil
{
    private AmazonUtil() {}


    // These methods are useful
    // when a single APK is distributed to both Google Play and Android Appstore
    // ...

    public static boolean isAmazonDevice()
    {
        boolean amazonDevice = Build.MANUFACTURER.equalsIgnoreCase("amazon");
        return amazonDevice;
    }

    public static boolean isfromAmazonStore(Activity activity)
    {
        final Application application = activity.getApplication();
        String installerName = application.getPackageManager().getInstallerPackageName(application.getPackageName());
        boolean fromAmazonStore = installerName != null && installerName.toLowerCase().contains(".amazon.");
        return fromAmazonStore;
    }

    // Name ???
    public static boolean isAmazonSupported(Activity activity)
    {
        return (isAmazonDevice() || isfromAmazonStore(activity));
    }

}

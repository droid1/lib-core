package ms.airy.lib.core.async.listener;


public interface AsyncRunnerListener
{
    void onResultSuccess(String tag, Object result);
    void onResultFailure(String tag, String message);
}

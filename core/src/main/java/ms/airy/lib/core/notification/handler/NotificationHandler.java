package ms.airy.lib.core.notification.handler;

import android.app.Notification;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;

import java.util.HashSet;
import java.util.Set;


/**
 * For scheduling notifications at a later time.
 */
public class NotificationHandler extends Handler
{
    // temporary
    private static final int NOTIFICATION_SCHEDULE_EVENT = 1923;
    private static final int DEFAULT_NOTIFICATION_ID = 0;

//    // TBD:
//    private long notificationScheduledTime;


    public NotificationHandler()
    {
        init();
    }
    // TBD:
    //     do we need callback? (NotificationHandler is a pretty specialized class.)
    public NotificationHandler(Callback callback)
    {
        super(callback);
        init();
    }
    public NotificationHandler(Looper looper)
    {
        super(looper);
        init();
    }
    // TBD: do we need callback?
    public NotificationHandler(Looper looper, Callback callback)
    {
        super(looper, callback);
        init();
    }
    private void init()
    {
    }


    // ???
    public void scheduleDelayedNotification(Notification notification, long delay)
    {
        scheduleDelayedNotification(DEFAULT_NOTIFICATION_ID, notification, delay);
    }

    public void scheduleDelayedNotification(int notiId, Notification notification, long delay)
    {
        scheduleDelayedNotification(NOTIFICATION_SCHEDULE_EVENT, notiId, notification, delay);
    }

    public void scheduleDelayedNotification(int what, int notificationId, Notification notification, long delay)
    {
        Message msg = new Message();
        msg.what = what;  // ??? Do we allow what other than NOTIFICATION_SCHEDULE_EVENT ????
        msg.arg1 = notificationId;
        msg.obj = notification;
        scheduleDelayedNotification(msg, delay);
    }

    public void scheduleDelayedNotification(Message msg, long delay)
    {
        // Uses system uptime (not unix epoch time).
        long notificationScheduledTime = SystemClock.uptimeMillis() + delay;
        sendMessageAtTime(msg, notificationScheduledTime);
    }


    @Override
    public void handleMessage(Message msg)
    {
        switch(msg.what) {
            case NOTIFICATION_SCHEDULE_EVENT:
                int notificationId = msg.arg1;
                Notification notification = (Notification) msg.obj;
                doNotificationEventProcessing(notificationId, notification);
                // return; // ???
                break;
            default:
                // TBD:....
                Log.i("Unrecognized message: what = " + msg.what);
        }
        super.handleMessage(msg);
    }

    private void doNotificationEventProcessing(int notificationId, Notification notification)
    {
        Log.d("doNotificationEventProcessing()");

        for(NotificationEventCallback callback : notificationEventCallbacks) {
            callback.processScheduledNotificationEvent(notificationId, notification);
        }
    }


    // TBD:
    // Event callbacks
    private final Set<NotificationEventCallback> notificationEventCallbacks = new HashSet<NotificationEventCallback>();
    protected final Set<NotificationEventCallback> getNotificationEventCallbacks()
    {
        return notificationEventCallbacks;
    }

    // This needs to be called by the "client"
    public final void addNotificationEventCallback(NotificationEventCallback notificationEventCallback)
    {
        notificationEventCallbacks.add(notificationEventCallback);
    }


}

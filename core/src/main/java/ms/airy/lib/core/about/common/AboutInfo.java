package ms.airy.lib.core.about.common;

import android.content.Context;

import ms.airy.lib.core.common.ImageSize;


// An item in the the "about list"
public class AboutInfo
{
    // Unique Id within a list. > 0L.
    private final long id;

    // Image/Logo required ????
    private int imageResId;      // Logo image. Default size.

    // Title also required ????
    private String title = null;
    private int titleResId = 0;

    // Optional (?)
    private String description = null;
    private int descriptionResId = 0;

    // Either external URL or action, but not both.
    // Note that the about list does not have "detail" page.
    // Each item opens up the external link or a different activity.
    // At least one is required.
    // private String label;             // label for the url/action (for display) ?????
    private String detailUrl = null;         // Item link.
    private String detailAction = null;      // Item link.
    private String detailExtra = null;       // If action is used. Extra might be needed ???? We can support only one key, for now.

    // Different size logos.
    // Note that these are "optional" and sizes are really arbitrary.
    // But, the "standard" size means
    // (Cf. ImageSize class)
    //           m     h    xh
    // tiny     64    96   128
    // small    96   144   192
    // medium  128   192   256
    // large   256   384   512
    // xlarge  512   768  1024
    // ...
    private int tinyImageResId = 0;
    private int smallImageResId = 0;
    private int mediumImageResId = 0;
    private int largeImageResId = 0;
    private int xlargeImageResId = 0;

    public AboutInfo(long id)
    {
        this(id, 0);
    }
    public AboutInfo(long id, int imageResId)
    {
        this(id, imageResId, null);
    }
    public AboutInfo(long id, int imageResId, String title)
    {
        this(id, imageResId, title, null, null, null);
    }
    public AboutInfo(long id, int imageResId, int titleResId)
    {
        this(id, imageResId, titleResId, 0, null, null);
    }
    public AboutInfo(long id, int imageResId, String title, String description, String detailUrl)
    {
        this.id = id;
        this.imageResId = imageResId;
        this.title = title;
        this.description = description;
        this.detailUrl = detailUrl;
    }
    public AboutInfo(long id, int imageResId, int titleResId, int descriptionResId, String detailUrl)
    {
        this.id = id;
        this.imageResId = imageResId;
        this.titleResId = titleResId;
        this.descriptionResId = descriptionResId;
        this.detailUrl = detailUrl;
    }
    public AboutInfo(long id, int imageResId, String title, String description, String detailAction, String detailExtra)
    {
        this.id = id;
        this.imageResId = imageResId;
        this.title = title;
        this.description = description;
        this.detailAction = detailAction;
        this.detailExtra = detailExtra;
    }
    public AboutInfo(long id, int imageResId, int titleResId, int descriptionResId, String detailAction, String detailExtra)
    {
        this.id = id;
        this.imageResId = imageResId;
        this.titleResId = titleResId;
        this.descriptionResId = descriptionResId;
        this.detailAction = detailAction;
        this.detailExtra = detailExtra;
    }


    public final long getId()
    {
        return id;
    }

    public final int getImageResId()
    {
        return imageResId;
    }
    public final void setImageResId(int imageResId)
    {
        this.imageResId = imageResId;
    }

    public final String getTitle()
    {
        return getTitle(null);
    }
    public String getTitle(Context context)
    {
        if(title != null && !title.isEmpty()) {
            return title;
        } else if(titleResId > 0) {
            if(context != null) {
                return context.getResources().getString(titleResId);
            }
        }
        return null;
    }

    public final void setTitle(String title)
    {
        this.title = title;
        this.titleResId = 0;
    }
    public final void setTitle(int titleResId)
    {
        this.title = null;
        this.titleResId = titleResId;
    }

    public final String getDescription()
    {
        return getDescription(null);
    }
    public String getDescription(Context context)
    {
        if(description != null && !description.isEmpty()) {
            return description;
        } else if(descriptionResId > 0) {
            if(context != null) {
                return context.getResources().getString(descriptionResId);
            }
        }
        return null;
    }

    public final void setDescription(String description)
    {
        this.description = description;
        this.descriptionResId = 0;
    }
    public final void setDescription(int descriptionResId)
    {
        this.description = null;
        this.descriptionResId = descriptionResId;
    }


    public final String getDetailUrl()
    {
        return detailUrl;
    }
    public final void setDetailUrl(String detailUrl)
    {
        this.detailUrl = detailUrl;
        // ????
        if(detailUrl != null) {
            detailAction = null;
            detailExtra = null;
        }
    }

    public final String getDetailAction()
    {
        return detailAction;
    }
    public final String getDetailExtra()
    {
        return detailExtra;
    }
    public final void setDetailAction(String detailAction)
    {
       setDetailAction(detailAction, null);
    }
    public final void setDetailAction(String detailAction, String detailExtra)
    {
        this.detailAction = detailAction;
        this.detailExtra = detailExtra;
        // ????
        if(detailAction != null) {
            detailUrl = null;
        }
    }

//    // temporary
//    public final String getDetailLink()
//    {
//        // Return either url or action ???
//    }


    public final void setTinyImageResId(int tinyImageResId)
    {
        this.tinyImageResId = tinyImageResId;
    }
    public final void setSmallImageResId(int smallImageResId)
    {
        this.smallImageResId = smallImageResId;
    }
    public final void setMediumImageResId(int mediumImageResId)
    {
        this.mediumImageResId = mediumImageResId;
    }
    public final void setLargeImageResId(int largeImageResId)
    {
        this.largeImageResId = largeImageResId;
    }
    public final void setXLargeImageResId(int xlargeImageResId)
    {
        this.xlargeImageResId = xlargeImageResId;
    }
    public final void setImageResIds(int smallImageResId, int mediumImageResId, int largeImageResId)
    {
        this.smallImageResId = smallImageResId;
        this.mediumImageResId = mediumImageResId;
        this.largeImageResId = largeImageResId;
    }
    public final void setImageResIds(int tinyImageResId, int smallImageResId, int mediumImageResId, int largeImageResId, int xlargeImageResId)
    {
        this.tinyImageResId = tinyImageResId;
        this.smallImageResId = smallImageResId;
        this.mediumImageResId = mediumImageResId;
        this.largeImageResId = largeImageResId;
        this.xlargeImageResId = xlargeImageResId;
    }

    public final int getImageResId(String logoSize)
    {
        if(ImageSize.SIZE_TINY.equals(logoSize)) {
            if(tinyImageResId > 0) {
                return tinyImageResId;
            }
        } else if(ImageSize.SIZE_SMALL.equals(logoSize)) {
            if(smallImageResId > 0) {
                return smallImageResId;
            }
        } else if(ImageSize.SIZE_MEDIUM.equals(logoSize)) {
            if(mediumImageResId > 0) {
                return mediumImageResId;
            }
        } else if(ImageSize.SIZE_LARGE.equals(logoSize)) {
            if(largeImageResId > 0) {
                return largeImageResId;
            }
        } else if(ImageSize.SIZE_XLARGE.equals(logoSize)) {
            if(xlargeImageResId > 0) {
                return xlargeImageResId;
            }
        }
        // ???
        return imageResId;
    }


    // For debugging...
    @Override
    public String toString()
    {
        return "AboutInfo{" +
                "id=" + id +
                ", imageResId=" + imageResId +
                ", title='" + title + '\'' +
                ", titleResId=" + titleResId +
                ", description='" + description + '\'' +
                ", descriptionResId=" + descriptionResId +
                ", detailUrl='" + detailUrl + '\'' +
                ", detailAction='" + detailAction + '\'' +
                ", detailExtra='" + detailExtra + '\'' +
                ", tinyImageResId=" + tinyImageResId +
                ", smallImageResId=" + smallImageResId +
                ", mediumImageResId=" + mediumImageResId +
                ", largeImageResId=" + largeImageResId +
                ", xlargeImageResId=" + xlargeImageResId +
                '}';
    }
}

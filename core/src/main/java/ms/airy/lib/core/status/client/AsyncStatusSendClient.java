package ms.airy.lib.core.status.client;

import ms.airy.lib.core.status.common.StatusMessage;


/**
 */
public interface AsyncStatusSendClient
{
    void sendMessage(int type, String message);
    // void sendMessage(long id, int type, String message);
    void sendStatusMessage(StatusMessage statusMessage);
}

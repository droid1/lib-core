package ms.airy.lib.core.common;

// Color is represented as an int.
// http://developer.android.com/reference/android/graphics/Color.html
// http://www.w3schools.com/cssref/css_colornames.asp
// TBD: use string????
public class Color // extends android.graphics.Color
{
    // TBD:
    // Define NULL_COLOR ????
    // Currently, Android.graphics.Color.Transparent (#00000000) is used as null color....
    // ...
    
    // Color value (according to android.graphics.Color implementation)
    private int value;
    private String colorString = null;  // "cache"  format: "#aarrggbb" or "#rrggbb" if alpha==ff
    
    public Color()
    {
        value = android.graphics.Color.BLACK; // ???
    }
    public Color(int value)
    {
        this.value = value;
    }
    public Color(int red, int green, int blue)
    {
        value = android.graphics.Color.rgb(red, green, blue);
    }
    public Color(int alpha, int red, int green, int blue)
    {
        value = android.graphics.Color.argb(alpha, red, green, blue);
    }
    public Color(String colorString)
    {
        if(colorString == null || colorString.isEmpty()) {
            throw new IllegalArgumentException("Error - Empty color string.");
        }

        try {
            value = android.graphics.Color.parseColor(colorString.toLowerCase());
        } catch (IllegalArgumentException ex) {
            // TBD: Include all CSS color names....
            
            String colorHex = null;
            if(colorString.equalsIgnoreCase("AliceBlue")) {
                colorHex = "#FFF0F8FF";
            } else if(colorString.equalsIgnoreCase("AntiqueWhite")) { 
                colorHex = "#FFFAEBD7";
            } else if(colorString.equalsIgnoreCase("Aquamarine")) { 
                colorHex = "#FF7FFFD4";
            } else if(colorString.equalsIgnoreCase("Azure")) { 
                colorHex = "#FFF0FFFF";
            } else if(colorString.equalsIgnoreCase("Beige")) { 
                colorHex = "#FFF5F5DC";
            } else if(colorString.equalsIgnoreCase("Bisque")) { 
                colorHex = "#FFFFE4C4";
            } else if(colorString.equalsIgnoreCase("BlanchedAlmond")) { 
                colorHex = "#FFFFEBCD";
            } else if(colorString.equalsIgnoreCase("BlueViolet")) { 
                colorHex = "#FF8A2BE2";
            } else if(colorString.equalsIgnoreCase("Brown")) { 
                colorHex = "#FFA52A2A";
            } else if(colorString.equalsIgnoreCase("BurlyWood")) { 
                colorHex = "#FFDEB887";
            } else if(colorString.equalsIgnoreCase("CadetBlue")) { 
                colorHex = "#FF5F9EA0";
            } else if(colorString.equalsIgnoreCase("Chartreuse")) { 
                colorHex = "#FF7FFF00";
            } else if(colorString.equalsIgnoreCase("Chocolate")) { 
                colorHex = "#FFD2691E";
            } else if(colorString.equalsIgnoreCase("Coral")) { 
                colorHex = "#FFFF7F50";
            } else if(colorString.equalsIgnoreCase("CornflowerBlue")) { 
                colorHex = "#FF6495ED";
            } else if(colorString.equalsIgnoreCase("Cornsilk")) { 
                colorHex = "#FFFFF8DC";
            } else if(colorString.equalsIgnoreCase("Crimson")) { 
                colorHex = "#FFDC143C";
            } else if(colorString.equalsIgnoreCase("DarkBlue")) { 
                colorHex = "#FF00008B";
            } else if(colorString.equalsIgnoreCase("DarkCyan")) { 
                colorHex = "#FF008B8B";
            } else if(colorString.equalsIgnoreCase("DarkGoldenRod")) { 
                colorHex = "#FFB8860B";
            } else if(colorString.equalsIgnoreCase("DarkGrey")) { 
                colorHex = "#FFA9A9A9";
            } else if(colorString.equalsIgnoreCase("DarkGreen")) { 
                colorHex = "#FF006400";
            } else if(colorString.equalsIgnoreCase("DarkKhaki")) { 
                colorHex = "#FFBDB76B";
            } else if(colorString.equalsIgnoreCase("DarkMagenta")) { 
                colorHex = "#FF8B008B";
            } else if(colorString.equalsIgnoreCase("DarkOliveGreen")) { 
                colorHex = "#FF556B2F";
            } else if(colorString.equalsIgnoreCase("Darkorange")) { 
                colorHex = "#FFFF8C00";
            } else if(colorString.equalsIgnoreCase("DarkOrchid")) { 
                colorHex = "#FF9932CC";
            } else if(colorString.equalsIgnoreCase("DarkRed")) { 
                colorHex = "#FF8B0000";
            } else if(colorString.equalsIgnoreCase("DarkSalmon")) { 
                colorHex = "#FFE9967A";
            } else if(colorString.equalsIgnoreCase("DarkSeaGreen")) { 
                colorHex = "#FF8FBC8F";
            } else if(colorString.equalsIgnoreCase("DarkSlateBlue")) { 
                colorHex = "#FF483D8B";
            } else if(colorString.equalsIgnoreCase("DarkSlateGrey")) { 
                colorHex = "#FF2F4F4F";
            } else if(colorString.equalsIgnoreCase("DarkTurquoise")) { 
                colorHex = "#FF00CED1";
            } else if(colorString.equalsIgnoreCase("DarkViolet")) { 
                colorHex = "#FF9400D3";
            } else if(colorString.equalsIgnoreCase("DeepPink")) { 
                colorHex = "#FFFF1493";
            } else if(colorString.equalsIgnoreCase("DeepSkyBlue")) { 
                colorHex = "#FF00BFFF";
            } else if(colorString.equalsIgnoreCase("DimGrey")) { 
                colorHex = "#FF696969";
            } else if(colorString.equalsIgnoreCase("DodgerBlue")) { 
                colorHex = "#FF1E90FF";
            } else if(colorString.equalsIgnoreCase("FireBrick")) { 
                colorHex = "#FFB22222";
            } else if(colorString.equalsIgnoreCase("FloralWhite")) { 
                colorHex = "#FFFFFAF0";
            } else if(colorString.equalsIgnoreCase("ForestGreen")) { 
                colorHex = "#FF228B22";
            } else if(colorString.equalsIgnoreCase("Gainsboro")) { 
                colorHex = "#FFDCDCDC";
            } else if(colorString.equalsIgnoreCase("GhostWhite")) { 
                colorHex = "#FFF8F8FF";
            } else if(colorString.equalsIgnoreCase("Gold")) { 
                colorHex = "#FFFFD700";
            } else if(colorString.equalsIgnoreCase("GoldenRod")) { 
                colorHex = "#FFDAA520";
            } else if(colorString.equalsIgnoreCase("Grey")) { 
                colorHex = "#FF808080";
            } else if(colorString.equalsIgnoreCase("Green")) { 
                colorHex = "#FF008000";
            } else if(colorString.equalsIgnoreCase("GreenYellow")) { 
                colorHex = "#FFADFF2F";
            } else if(colorString.equalsIgnoreCase("HoneyDew")) { 
                colorHex = "#FFF0FFF0";
            } else if(colorString.equalsIgnoreCase("HotPink")) { 
                colorHex = "#FFFF69B4";
            } else if(colorString.equalsIgnoreCase("IndianRed ")) { 
                colorHex = "#FFCD5C5C";
            } else if(colorString.equalsIgnoreCase("Indigo ")) { 
                colorHex = "#FF4B0082";
            } else if(colorString.equalsIgnoreCase("Ivory")) { 
                colorHex = "#FFFFFFF0";
            } else if(colorString.equalsIgnoreCase("Khaki")) { 
                colorHex = "#FFF0E68C";
            } else if(colorString.equalsIgnoreCase("Lavender")) { 
                colorHex = "#FFE6E6FA";
            } else if(colorString.equalsIgnoreCase("LavenderBlush")) { 
                colorHex = "#FFFFF0F5";
            } else if(colorString.equalsIgnoreCase("LawnGreen")) { 
                colorHex = "#FF7CFC00";
            } else if(colorString.equalsIgnoreCase("LemonChiffon")) { 
                colorHex = "#FFFFFACD";
            } else if(colorString.equalsIgnoreCase("LightBlue")) { 
                colorHex = "#FFADD8E6";
            } else if(colorString.equalsIgnoreCase("LightCoral")) { 
                colorHex = "#FFF08080";
            } else if(colorString.equalsIgnoreCase("LightCyan")) { 
                colorHex = "#FFE0FFFF";
            } else if(colorString.equalsIgnoreCase("LightGoldenRodYellow")) { 
                colorHex = "#FFFAFAD2";
            } else if(colorString.equalsIgnoreCase("LightGrey")) { 
                colorHex = "#FFD3D3D3";
            } else if(colorString.equalsIgnoreCase("LightGreen")) { 
                colorHex = "#FF90EE90";
            } else if(colorString.equalsIgnoreCase("LightPink")) { 
                colorHex = "#FFFFB6C1";
            } else if(colorString.equalsIgnoreCase("LightSalmon")) { 
                colorHex = "#FFFFA07A";
            } else if(colorString.equalsIgnoreCase("LightSeaGreen")) { 
                colorHex = "#FF20B2AA";
            } else if(colorString.equalsIgnoreCase("LightSkyBlue")) { 
                colorHex = "#FF87CEFA";
            } else if(colorString.equalsIgnoreCase("LightSlateGrey")) { 
                colorHex = "#FF778899";
            } else if(colorString.equalsIgnoreCase("LightSteelBlue")) { 
                colorHex = "#FFB0C4DE";
            } else if(colorString.equalsIgnoreCase("LightYellow")) { 
                colorHex = "#FFFFFFE0";
            } else if(colorString.equalsIgnoreCase("LimeGreen")) { 
                colorHex = "#FF32CD32";
            } else if(colorString.equalsIgnoreCase("Linen")) { 
                colorHex = "#FFFAF0E6";
            } else if(colorString.equalsIgnoreCase("Maroon")) { 
                colorHex = "#FF800000";
            } else if(colorString.equalsIgnoreCase("MediumAquaMarine")) { 
                colorHex = "#FF66CDAA";
            } else if(colorString.equalsIgnoreCase("MediumBlue")) { 
                colorHex = "#FF0000CD";
            } else if(colorString.equalsIgnoreCase("MediumOrchid")) { 
                colorHex = "#FFBA55D3";
            } else if(colorString.equalsIgnoreCase("MediumPurple")) { 
                colorHex = "#FF9370D8";
            } else if(colorString.equalsIgnoreCase("MediumSeaGreen")) { 
                colorHex = "#FF3CB371";
            } else if(colorString.equalsIgnoreCase("MediumSlateBlue")) { 
                colorHex = "#FF7B68EE";
            } else if(colorString.equalsIgnoreCase("MediumSpringGreen")) { 
                colorHex = "#FF00FA9A";
            } else if(colorString.equalsIgnoreCase("MediumTurquoise")) { 
                colorHex = "#FF48D1CC";
            } else if(colorString.equalsIgnoreCase("MediumVioletRed")) { 
                colorHex = "#FFC71585";
            } else if(colorString.equalsIgnoreCase("MidnightBlue")) { 
                colorHex = "#FF191970";
            } else if(colorString.equalsIgnoreCase("MintCream")) { 
                colorHex = "#FFF5FFFA";
            } else if(colorString.equalsIgnoreCase("MistyRose")) { 
                colorHex = "#FFFFE4E1";
            } else if(colorString.equalsIgnoreCase("Moccasin")) { 
                colorHex = "#FFFFE4B5";
            } else if(colorString.equalsIgnoreCase("NavajoWhite")) { 
                colorHex = "#FFFFDEAD";
            } else if(colorString.equalsIgnoreCase("Navy")) { 
                colorHex = "#FF000080";
            } else if(colorString.equalsIgnoreCase("OldLace")) { 
                colorHex = "#FFFDF5E6";
            } else if(colorString.equalsIgnoreCase("Olive")) { 
                colorHex = "#FF808000";
            } else if(colorString.equalsIgnoreCase("OliveDrab")) { 
                colorHex = "#FF6B8E23";
            } else if(colorString.equalsIgnoreCase("Orange")) { 
                colorHex = "#FFFFA500";
            } else if(colorString.equalsIgnoreCase("OrangeRed")) { 
                colorHex = "#FFFF4500";
            } else if(colorString.equalsIgnoreCase("Orchid")) { 
                colorHex = "#FFDA70D6";
            } else if(colorString.equalsIgnoreCase("PaleGoldenRod")) { 
                colorHex = "#FFEEE8AA";
            } else if(colorString.equalsIgnoreCase("PaleGreen")) { 
                colorHex = "#FF98FB98";
            } else if(colorString.equalsIgnoreCase("PaleTurquoise")) { 
                colorHex = "#FFAFEEEE";
            } else if(colorString.equalsIgnoreCase("PaleVioletRed")) { 
                colorHex = "#FFD87093";
            } else if(colorString.equalsIgnoreCase("PapayaWhip")) { 
                colorHex = "#FFFFEFD5";
            } else if(colorString.equalsIgnoreCase("PeachPuff")) { 
                colorHex = "#FFFFDAB9";
            } else if(colorString.equalsIgnoreCase("Peru")) { 
                colorHex = "#FFCD853F";
            } else if(colorString.equalsIgnoreCase("Pink")) { 
                colorHex = "#FFFFC0CB";
            } else if(colorString.equalsIgnoreCase("Plum")) { 
                colorHex = "#FFDDA0DD";
            } else if(colorString.equalsIgnoreCase("PowderBlue")) { 
                colorHex = "#FFB0E0E6";
            } else if(colorString.equalsIgnoreCase("Purple")) { 
                colorHex = "#FF800080";
            } else if(colorString.equalsIgnoreCase("RosyBrown")) { 
                colorHex = "#FFBC8F8F";
            } else if(colorString.equalsIgnoreCase("RoyalBlue")) { 
                colorHex = "#FF4169E1";
            } else if(colorString.equalsIgnoreCase("SaddleBrown")) { 
                colorHex = "#FF8B4513";
            } else if(colorString.equalsIgnoreCase("Salmon")) { 
                colorHex = "#FFFA8072";
            } else if(colorString.equalsIgnoreCase("SandyBrown")) { 
                colorHex = "#FFF4A460";
            } else if(colorString.equalsIgnoreCase("SeaGreen")) { 
                colorHex = "#FF2E8B57";
            } else if(colorString.equalsIgnoreCase("SeaShell")) { 
                colorHex = "#FFFFF5EE";
            } else if(colorString.equalsIgnoreCase("Sienna")) { 
                colorHex = "#FFA0522D";
            } else if(colorString.equalsIgnoreCase("Silver")) { 
                colorHex = "#FFC0C0C0";
            } else if(colorString.equalsIgnoreCase("SkyBlue")) { 
                colorHex = "#FF87CEEB";
            } else if(colorString.equalsIgnoreCase("SlateBlue")) { 
                colorHex = "#FF6A5ACD";
            } else if(colorString.equalsIgnoreCase("SlateGrey")) { 
                colorHex = "#FF708090";
            } else if(colorString.equalsIgnoreCase("Snow")) { 
                colorHex = "#FFFFFAFA";
            } else if(colorString.equalsIgnoreCase("SpringGreen")) { 
                colorHex = "#FF00FF7F";
            } else if(colorString.equalsIgnoreCase("SteelBlue")) { 
                colorHex = "#FF4682B4";
            } else if(colorString.equalsIgnoreCase("Tan")) { 
                colorHex = "#FFD2B48C";
            } else if(colorString.equalsIgnoreCase("Teal")) { 
                colorHex = "#FF008080";
            } else if(colorString.equalsIgnoreCase("Thistle")) { 
                colorHex = "#FFD8BFD8";
            } else if(colorString.equalsIgnoreCase("Tomato")) { 
                colorHex = "#FFFF6347";
            } else if(colorString.equalsIgnoreCase("Turquoise")) { 
                colorHex = "#FF40E0D0";
            } else if(colorString.equalsIgnoreCase("Violet")) { 
                colorHex = "#FFEE82EE";
            } else if(colorString.equalsIgnoreCase("Wheat")) { 
                colorHex = "#FFF5DEB3";
            } else if(colorString.equalsIgnoreCase("WhiteSmoke")) { 
                colorHex = "#FFF5F5F5";
            } else if(colorString.equalsIgnoreCase("YellowGreen")) { 
                colorHex = "#FF9ACD32";
            }

            if(colorHex != null) {
                value = android.graphics.Color.parseColor(colorHex);
            } else {
                //throw ex;
                throw new IllegalArgumentException("Error - Invalid color string.", ex);
            }
        }
    }

    public int getValue()
    {
        return value;
    }

    // Returns the color name if defined.
    // Otherwise return the color string, e.g., "#aarrggbb" or "#rrggbb". 
    public String getStringValue()
    {
        return toString();
    }
    
    
    // Creates a color from colorString.
    public static Color fromColorString(String colorString)
    {
        return new Color(colorString);
    }


    // Returns the color string in a format "#aarrggbb".
    public static String toColorString(int value)
    {
        // (1) Break the value int into four parts.
        // (2) Convert each part to hex number.
        // (3) Combine the string.

        int alpha = android.graphics.Color.alpha(value);
        int red = android.graphics.Color.red(value);
        int green = android.graphics.Color.green(value);
        int blue = android.graphics.Color.blue(value);
        
        StringBuffer sb = new StringBuffer();
        sb.append("#");
        if(alpha == 255) {
            // Skip. ????
        } else {
            sb.append(String.format("%02x", alpha));
        }
        sb.append(String.format("%02x", red));
        sb.append(String.format("%02x", green));
        sb.append(String.format("%02x", blue));

        return sb.toString();
    }


    // Returns the if the value is not a predefined color.
    public String toColorName()
    {
        String name = null;
        switch(value) {
        case android.graphics.Color.BLACK:
            name = "Black";
            break;
        case android.graphics.Color.BLUE:
            name = "Blue";
            break;
        case android.graphics.Color.CYAN:
            name = "Cyan";
            break;
        case android.graphics.Color.DKGRAY:
            name = "DarkGray";
            break;
        case android.graphics.Color.GRAY:
            name = "Gray";
            break;
        case android.graphics.Color.GREEN:
            name = "Green";
            break;
        case android.graphics.Color.LTGRAY:
            name = "LightGray";
            break;
        case android.graphics.Color.MAGENTA:
            name = "Magenta";
            break;
        case android.graphics.Color.RED:
            name = "Red";
            break;
        case android.graphics.Color.TRANSPARENT:
            name = "Transparent";
            break;
        case android.graphics.Color.WHITE:
            name = "White";
            break;
        case android.graphics.Color.YELLOW:
            name = "Yellow";
            break;

        // 147 colors defined by CSS
        case 0xFFF0F8FF:
            name = "AliceBlue"; 
            break;
        case 0xFFFAEBD7:
            name = "AntiqueWhite"; 
            break;
//        case 0xFF00FFFF:
//            name = "Aqua"; 
//            break;
        case 0xFF7FFFD4:
            name = "Aquamarine"; 
            break;
        case 0xFFF0FFFF:
            name = "Azure"; 
            break;
        case 0xFFF5F5DC:
            name = "Beige"; 
            break;
        case 0xFFFFE4C4:
            name = "Bisque"; 
            break;
//        case 0xFF000000:
//            name = "Black"; 
//            break;
        case 0xFFFFEBCD:
            name = "BlanchedAlmond"; 
            break;
//        case 0xFF0000FF:
//            name = "Blue"; 
//            break;
        case 0xFF8A2BE2:
            name = "BlueViolet"; 
            break;
        case 0xFFA52A2A:
            name = "Brown"; 
            break;
        case 0xFFDEB887:
            name = "BurlyWood"; 
            break;
        case 0xFF5F9EA0:
            name = "CadetBlue"; 
            break;
        case 0xFF7FFF00:
            name = "Chartreuse"; 
            break;
        case 0xFFD2691E:
            name = "Chocolate"; 
            break;
        case 0xFFFF7F50:
            name = "Coral"; 
            break;
        case 0xFF6495ED:
            name = "CornflowerBlue"; 
            break;
        case 0xFFFFF8DC:
            name = "Cornsilk"; 
            break;
        case 0xFFDC143C:
            name = "Crimson"; 
            break;
//        case 0xFF00FFFF:
//            name = "Cyan"; 
//            break;
        case 0xFF00008B:
            name = "DarkBlue"; 
            break;
        case 0xFF008B8B:
            name = "DarkCyan"; 
            break;
        case 0xFFB8860B:
            name = "DarkGoldenRod"; 
            break;
//        case 0xFFA9A9A9:
//            name = "DarkGray"; 
//            break;
        case 0xFFA9A9A9:
            name = "DarkGrey"; 
            break;
        case 0xFF006400:
            name = "DarkGreen"; 
            break;
        case 0xFFBDB76B:
            name = "DarkKhaki"; 
            break;
        case 0xFF8B008B:
            name = "DarkMagenta"; 
            break;
        case 0xFF556B2F:
            name = "DarkOliveGreen"; 
            break;
        case 0xFFFF8C00:
            name = "Darkorange"; 
            break;
        case 0xFF9932CC:
            name = "DarkOrchid"; 
            break;
        case 0xFF8B0000:
            name = "DarkRed"; 
            break;
        case 0xFFE9967A:
            name = "DarkSalmon"; 
            break;
        case 0xFF8FBC8F:
            name = "DarkSeaGreen"; 
            break;
        case 0xFF483D8B:
            name = "DarkSlateBlue"; 
            break;
//        case 0xFF2F4F4F:
//            name = "DarkSlateGray"; 
//            break;
        case 0xFF2F4F4F:
            name = "DarkSlateGrey"; 
            break;
        case 0xFF00CED1:
            name = "DarkTurquoise"; 
            break;
        case 0xFF9400D3:
            name = "DarkViolet"; 
            break;
        case 0xFFFF1493:
            name = "DeepPink"; 
            break;
        case 0xFF00BFFF:
            name = "DeepSkyBlue"; 
            break;
//        case 0xFF696969:
//            name = "DimGray"; 
//            break;
        case 0xFF696969:
            name = "DimGrey"; 
            break;
        case 0xFF1E90FF:
            name = "DodgerBlue"; 
            break;
        case 0xFFB22222:
            name = "FireBrick"; 
            break;
        case 0xFFFFFAF0:
            name = "FloralWhite"; 
            break;
        case 0xFF228B22:
            name = "ForestGreen"; 
            break;
//        case 0xFFFF00FF:
//            name = "Fuchsia"; 
//            break;
        case 0xFFDCDCDC:
            name = "Gainsboro"; 
            break;
        case 0xFFF8F8FF:
            name = "GhostWhite"; 
            break;
        case 0xFFFFD700:
            name = "Gold"; 
            break;
        case 0xFFDAA520:
            name = "GoldenRod"; 
            break;
//        case 0xFF808080:
//            name = "Gray"; 
//            break;
        case 0xFF808080:
            name = "Grey"; 
            break;
        case 0xFF008000:
            name = "Green"; 
            break;
        case 0xFFADFF2F:
            name = "GreenYellow"; 
            break;
        case 0xFFF0FFF0:
            name = "HoneyDew"; 
            break;
        case 0xFFFF69B4:
            name = "HotPink"; 
            break;
        case 0xFFCD5C5C:
            name = "IndianRed "; 
            break;
        case 0xFF4B0082:
            name = "Indigo "; 
            break;
        case 0xFFFFFFF0:
            name = "Ivory"; 
            break;
        case 0xFFF0E68C:
            name = "Khaki"; 
            break;
        case 0xFFE6E6FA:
            name = "Lavender"; 
            break;
        case 0xFFFFF0F5:
            name = "LavenderBlush"; 
            break;
        case 0xFF7CFC00:
            name = "LawnGreen"; 
            break;
        case 0xFFFFFACD:
            name = "LemonChiffon"; 
            break;
        case 0xFFADD8E6:
            name = "LightBlue"; 
            break;
        case 0xFFF08080:
            name = "LightCoral"; 
            break;
        case 0xFFE0FFFF:
            name = "LightCyan"; 
            break;
        case 0xFFFAFAD2:
            name = "LightGoldenRodYellow"; 
            break;
//        case 0xFFD3D3D3:
//            name = "LightGray"; 
//            break;
        case 0xFFD3D3D3:
            name = "LightGrey"; 
            break;
        case 0xFF90EE90:
            name = "LightGreen"; 
            break;
        case 0xFFFFB6C1:
            name = "LightPink"; 
            break;
        case 0xFFFFA07A:
            name = "LightSalmon"; 
            break;
        case 0xFF20B2AA:
            name = "LightSeaGreen"; 
            break;
        case 0xFF87CEFA:
            name = "LightSkyBlue"; 
            break;
//        case 0xFF778899:
//            name = "LightSlateGray"; 
//            break;
        case 0xFF778899:
            name = "LightSlateGrey"; 
            break;
        case 0xFFB0C4DE:
            name = "LightSteelBlue"; 
            break;
        case 0xFFFFFFE0:
            name = "LightYellow"; 
            break;
//        case 0xFF00FF00:
//            name = "Lime"; 
//            break;
        case 0xFF32CD32:
            name = "LimeGreen"; 
            break;
        case 0xFFFAF0E6:
            name = "Linen"; 
            break;
//        case 0xFFFF00FF:
//            name = "Magenta"; 
//            break;
        case 0xFF800000:
            name = "Maroon"; 
            break;
        case 0xFF66CDAA:
            name = "MediumAquaMarine"; 
            break;
        case 0xFF0000CD:
            name = "MediumBlue"; 
            break;
        case 0xFFBA55D3:
            name = "MediumOrchid"; 
            break;
        case 0xFF9370D8:
            name = "MediumPurple"; 
            break;
        case 0xFF3CB371:
            name = "MediumSeaGreen"; 
            break;
        case 0xFF7B68EE:
            name = "MediumSlateBlue"; 
            break;
        case 0xFF00FA9A:
            name = "MediumSpringGreen"; 
            break;
        case 0xFF48D1CC:
            name = "MediumTurquoise"; 
            break;
        case 0xFFC71585:
            name = "MediumVioletRed"; 
            break;
        case 0xFF191970:
            name = "MidnightBlue"; 
            break;
        case 0xFFF5FFFA:
            name = "MintCream"; 
            break;
        case 0xFFFFE4E1:
            name = "MistyRose"; 
            break;
        case 0xFFFFE4B5:
            name = "Moccasin"; 
            break;
        case 0xFFFFDEAD:
            name = "NavajoWhite"; 
            break;
        case 0xFF000080:
            name = "Navy"; 
            break;
        case 0xFFFDF5E6:
            name = "OldLace"; 
            break;
        case 0xFF808000:
            name = "Olive"; 
            break;
        case 0xFF6B8E23:
            name = "OliveDrab"; 
            break;
        case 0xFFFFA500:
            name = "Orange"; 
            break;
        case 0xFFFF4500:
            name = "OrangeRed"; 
            break;
        case 0xFFDA70D6:
            name = "Orchid"; 
            break;
        case 0xFFEEE8AA:
            name = "PaleGoldenRod"; 
            break;
        case 0xFF98FB98:
            name = "PaleGreen"; 
            break;
        case 0xFFAFEEEE:
            name = "PaleTurquoise"; 
            break;
        case 0xFFD87093:
            name = "PaleVioletRed"; 
            break;
        case 0xFFFFEFD5:
            name = "PapayaWhip"; 
            break;
        case 0xFFFFDAB9:
            name = "PeachPuff"; 
            break;
        case 0xFFCD853F:
            name = "Peru"; 
            break;
        case 0xFFFFC0CB:
            name = "Pink"; 
            break;
        case 0xFFDDA0DD:
            name = "Plum"; 
            break;
        case 0xFFB0E0E6:
            name = "PowderBlue"; 
            break;
        case 0xFF800080:
            name = "Purple"; 
            break;
//        case 0xFFFF0000:
//            name = "Red"; 
//            break;
        case 0xFFBC8F8F:
            name = "RosyBrown"; 
            break;
        case 0xFF4169E1:
            name = "RoyalBlue"; 
            break;
        case 0xFF8B4513:
            name = "SaddleBrown"; 
            break;
        case 0xFFFA8072:
            name = "Salmon"; 
            break;
        case 0xFFF4A460:
            name = "SandyBrown"; 
            break;
        case 0xFF2E8B57:
            name = "SeaGreen"; 
            break;
        case 0xFFFFF5EE:
            name = "SeaShell"; 
            break;
        case 0xFFA0522D:
            name = "Sienna"; 
            break;
        case 0xFFC0C0C0:
            name = "Silver"; 
            break;
        case 0xFF87CEEB:
            name = "SkyBlue"; 
            break;
        case 0xFF6A5ACD:
            name = "SlateBlue"; 
            break;
//        case 0xFF708090:
//            name = "SlateGray"; 
//            break;
        case 0xFF708090:
            name = "SlateGrey"; 
            break;
        case 0xFFFFFAFA:
            name = "Snow"; 
            break;
        case 0xFF00FF7F:
            name = "SpringGreen"; 
            break;
        case 0xFF4682B4:
            name = "SteelBlue"; 
            break;
        case 0xFFD2B48C:
            name = "Tan"; 
            break;
        case 0xFF008080:
            name = "Teal"; 
            break;
        case 0xFFD8BFD8:
            name = "Thistle"; 
            break;
        case 0xFFFF6347:
            name = "Tomato"; 
            break;
        case 0xFF40E0D0:
            name = "Turquoise"; 
            break;
        case 0xFFEE82EE:
            name = "Violet"; 
            break;
        case 0xFFF5DEB3:
            name = "Wheat"; 
            break;
//        case 0xFFFFFFFF:
//            name = "White"; 
//            break;
        case 0xFFF5F5F5:
            name = "WhiteSmoke"; 
            break;
//        case 0xFFFFFF00:
//            name = "Yellow"; 
//            break;
        case 0xFF9ACD32:
            name = "YellowGreen"; 
            break;

        default:
            // ???
            break;
        }
        
        return name;
    }

    public String getColorString()
    {
        // Lazy initialization.
        if(colorString == null) {
            colorString = toColorString(value);
        }
        return colorString;
    }
    
    @Override
    public String toString()
    {
        String name = toColorName();
        if(name == null) {
            name = getColorString();
        }
        return name;
    }

    
    // TBD
    // Implement equals()
    // ...    
    

}

package ms.airy.lib.core.popup.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;
import java.util.Map;


// TBD:
// Primarily to support Popup-related fragments or other support classes...
// Note that it is based on a "tag", not an instance of a fragment.
// Hence two fragments/objects using the same tag (for the purposes of managing prefs/data)
//    are one and the same as far as PopupHostPrefsManager is concerned.
public final class PopupHostPrefsManager
{
    // temporary
    private static final String SP_FILENAME_SUFFIX = ".popup_host_prefs";
    private static final String KEY_POPUP_TIME_SUFFIX = ".last_popup_time";
    // ...

    private static PopupHostPrefsManager INSTANCE = null;
    public static PopupHostPrefsManager getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new PopupHostPrefsManager(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;

    // Separate Shared Preferences file to store "popup host" related data.
    private final String sharedPrefsFile;
    private final SharedPreferences sharedPreferences;

    // "Cache". tag -> last popup time.
    private final Map<String, Long> popupTimeMap = new HashMap<String, Long>();

    private PopupHostPrefsManager(Context context)
    {
        this.context = context;
        sharedPrefsFile = context.getPackageName() + SP_FILENAME_SUFFIX;
        sharedPreferences = context.getSharedPreferences(sharedPrefsFile, Context.MODE_PRIVATE);
    }
    public String getSharedPrefsFile()
    {
        return sharedPrefsFile;
    }


//    protected String getSharedPrefsFile()
//    {
//        return this.sharedPrefsFile;
//    }

    private static String getLastPopupTimeKey(String fragmentTag)
    {
        return fragmentTag + KEY_POPUP_TIME_SUFFIX;
    }


    public long getLastPopupTime(String fragmentTag)
    {
        long lastPopupTime;
        if(popupTimeMap.get(fragmentTag) != null) {
            lastPopupTime = popupTimeMap.get(fragmentTag);
        } else {
            lastPopupTime = sharedPreferences.getLong(getLastPopupTimeKey(fragmentTag), 0L);
            popupTimeMap.put(fragmentTag, lastPopupTime);  // exclude 0L ????
        }
        return lastPopupTime;
    }
    public void setLastPopupTime(String fragmentTag)
    {
        long now = System.currentTimeMillis();
        setLastPopupTime(fragmentTag, now);
    }
    public void setLastPopupTime(String fragmentTag, long value)
    {
        setLastPopupTime(fragmentTag, value, false);
    }
    public void setLastPopupTime(String fragmentTag, long value, boolean force)
    {
        if(force || popupTimeMap.get(fragmentTag) == null || popupTimeMap.get(fragmentTag) != value) {
            popupTimeMap.put(fragmentTag, value);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putLong(getLastPopupTimeKey(fragmentTag), value);
            editor.commit();
        }
    }


    // For debugging/testing purposes
    protected void removeAllPrefs()
    {
        Log.w("======== PopupHostPrefsManager.removeAllPrefs()");

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }

}

package ms.airy.lib.core.analytics.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ms.airy.lib.core.analytics.factory.AnalyticsManagerFactory;
import ms.airy.lib.core.analytics.manager.AnalyticsManager;


public class BaseAnalyticsSupportFragment extends Fragment
{
    public static BaseAnalyticsSupportFragment newInstance()
    {
        BaseAnalyticsSupportFragment fragment = new BaseAnalyticsSupportFragment();
        Bundle args = new Bundle();
        // ..
        fragment.setArguments(args);
        return fragment;
    }

    private AnalyticsManager baseAnalyticsManager = null;

    public BaseAnalyticsSupportFragment()
    {
    }

    // TBD:
    // Lazy initialized.
    // It's important that baseAnalyticsManager initialized as late as possible
    //   (so that we have a chance to set/inject analyticsManager to the mangerFactory....)
    // ???  Does this work?
    // ???  Is there a better way?
    protected AnalyticsManager getAnalyticsManager()
    {
        // TBD:
//        if(baseAnalyticsManager == null) {
//            baseAnalyticsManager = new UniversalAnalyticsManager(getActivity());   // ???
//        }
        // ???
        if(baseAnalyticsManager == null) {
            baseAnalyticsManager = AnalyticsManagerFactory.getInstance(getActivity()).getAnalyticsManager();
        }
        return baseAnalyticsManager;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // Enable the menu ??
        // setHasOptionsMenu(true);
        // tbd
        // setRetainInstance(true);
        // ...

        Bundle args = getArguments();
        if (args != null) {
            // ...
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        return null;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        // ???
//        if(baseAnalyticsManager == null) {
//            baseAnalyticsManager = AnalyticsManagerFactory.getInstance(getActivity()).getAnalyticsManager();
//        }
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
    }

    @Override
    public void onStart()
    {
        super.onStart();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        // if(AnalyticsManagerFactory.getInstance(getActivity()).isTrackingEnabled() && getAnalyticsManager() != null) {
        if(getAnalyticsManager() != null && getAnalyticsManager().isTrackingEnabled()) {
            getAnalyticsManager().startSession();
        }
    }

    @Override
    public void onPause()
    {
        super.onPause();
        // if(AnalyticsManagerFactory.getInstance(getActivity()).isTrackingEnabled() && getAnalyticsManager() != null) {
        if(getAnalyticsManager() != null && getAnalyticsManager().isTrackingEnabled()) {
            getAnalyticsManager().endSession();
        }
    }

    @Override
    public void onStop()
    {
        super.onStop();
    }

    @Override
    public void onDetach()
    {
        super.onDetach();

    }

}

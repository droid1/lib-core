package ms.airy.lib.core.status.client.impl;

import ms.airy.lib.core.status.client.StatusMessageFetchClient;
import ms.airy.lib.core.status.common.StatusMessage;

import java.util.List;


/**
 */
public class MockStatusMessageFetchClient implements StatusMessageFetchClient
{

    @Override
    public StatusMessage fetchStatusMessage(long id)
    {
        if(Log.I) Log.i("MockStatusMessageFetchClient.fetchStatusMessage() id = " + id);

        return null;
    }

    @Override
    public List<Long> fetchNewMessages()
    {
        Log.i("MockStatusMessageFetchClient.fetchNewMessages()");

        return null;
    }

    @Override
    public List<Long> fetchMessages(long since)
    {
        if(Log.I) Log.i("MockStatusMessageFetchClient.fetchMessages() since = " + since);

        return null;
    }

}

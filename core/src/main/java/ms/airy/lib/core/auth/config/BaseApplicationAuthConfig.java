package ms.airy.lib.core.auth.config;

import android.content.Context;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ms.airy.lib.core.auth.common.AccountType;
import ms.airy.lib.core.auth.helper.AccountTypeUtil;


// TBD:
// To store auth-related "global" config..
// This is a "virtual singleton":
// It is implemented as a singleton, but clients can create subclasses, if necessary.
// TBD:
// The configs should be read from config files or resources, etc. (or, even from a remote server).
// For now, they are all hard-coded.
// Each app should create a derived class and overwrite each method...
// ....
public class BaseApplicationAuthConfig implements ApplicationAuthConfig
{
    // Note that many of the config options defined here have dependencies
    //     (although how to handle "conflicts" is really up to the specific app).
    // For example, if isPrimaryAccountAuthRequired() == true, then
    //                 isPrimaryAccountRequired() should be probably true as well.
    // As another example, if isApplicationAuthDisabled() == true, then
    //                 the rest of the "authRequired()" functions will be likely ignored.
    // ...

    private static BaseApplicationAuthConfig INSTANCE = null;
    public static BaseApplicationAuthConfig getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new BaseApplicationAuthConfig(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;

    protected BaseApplicationAuthConfig(Context context)
    {
        this.context = context;
    }


    // TBD
    // whether to use "custom" auth provider (e.g., airy)
    // custom auth provider list
    // etc...


    // Each application may have a different "principal account type"..
    // Principal account is the one that the app relies on for remote service, etc...
    // could be none.
    // Note that the user's "primary" account may not be of the apps' principal account type.
    // In fact, the user may not even have any accounts of the principal type.
    // TBD: Allow multiple principal account types???
    @Override
    public boolean hasPrincipalAccountType()
    {
        return AccountTypeUtil.isValid(getPrincipalAccountType());
    }
    @Override
    public int getPrincipalAccountType()
    {
        // temporary
        return AccountType.TYPE_GOOGLE;
    }


    // TBD:
    // account type list (ordered)
    // ...
    @Override
    public List<Integer> getAplicationTypeList()
    {
        return null;
    }


    // temporary.
    private static final Map<Integer, Set<String>> sDefaultScopeMap = new HashMap<Integer, Set<String>>();
    static {
        // temporary
        Set<String> googleScopes = new HashSet<String>();
        googleScopes.add("https://www.googleapis.com/auth/userinfo.email");
        googleScopes.add("https://www.googleapis.com/auth/userinfo.profile");
        sDefaultScopeMap.put(AccountType.TYPE_GOOGLE, googleScopes);

        Set<String> facebookScopes = new HashSet<String>();
        facebookScopes.add("");    // ???
        sDefaultScopeMap.put(AccountType.TYPE_FACEBOOK, facebookScopes);

        Set<String> twitterScopes = new HashSet<String>();
        twitterScopes.add("");    // ???
        sDefaultScopeMap.put(AccountType.TYPE_TWITTER, twitterScopes);

        Set<String> yahooScopes = new HashSet<String>();
        yahooScopes.add("");    // ???
        sDefaultScopeMap.put(AccountType.TYPE_YAHOO, yahooScopes);

        //...
    }

    // TBD:
    // We need setter/getters for app-specific scope sets for each accountType....
    @Override
    public Set<String> getScopeSet(int accountType)
    {
        // temporary
        return sDefaultScopeMap.get(accountType);
    }


    @Override
    public boolean isPrimaryAccountRequired()
    {
        // temporary
        return true;
    }

    // "Principal account" is the main account of the principal type.
    @Override
    public boolean isPrincipalAccountRequired()
    {
        // temporary
        return false;
    }


    // These apply to both primary and principal as well as other "main" account picks.
    // If set to true, an account will be automatically selected
    //                 without displaying the account picker to the user....

    @Override
    public boolean isAutoSelectAccount()
    {
        // temporary
        return false;
    }

    @Override
    public boolean isAutoSelectAccountIfOne()
    {
        if(isAutoSelectAccount() == true) {
            return true;
        }

        // temporary
        return true;
    }


    // Returns true if the app does not require/use auth at all.
    @Override
    public boolean isApplicationAuthDisabled()
    {
        // temporary
        return false;
    }


    // Note that these "authRequired()" functions are for "global" configs.
    // That is, even if "authRequired()" returns false, certain activities may still require such auth.
    // On the other hand, if "authRequired()" returns true,
    //         then the major portion of the app would not work if the user has not authorized the resource.
    // ...

    // Returns true if at least one account should be authorized (with some auth scopes).
    @Override
    public boolean isAuthRequired()
    {
        // temporary
        return false;
    }

    // Returns true if the primary account should be authorized.
    @Override
    public boolean isPrimaryAccountAuthRequired()
    {
        // temporary
        return false;
    }
    @Override
    public boolean isPrimaryAccountAuthRequired(Set<String> authScopes)
    {
        // temporary
        return false;
    }

    // Returns true if the principal account should be authorized.
    @Override
    public boolean isPrincipalAccountAuthRequired()
    {
        // temporary
        return false;
    }
    @Override
    public boolean isPrincipalAccountAuthRequired(Set<String> authScopes)
    {
        // temporary
        return false;
    }

    // Returns true if the main account of the google type should be authorized.
    @Override
    public boolean isGoogleAccountAuthRequired()
    {
        // temporary
        return false;
    }
    @Override
    public boolean isGoogleAccountAuthRequired(Set<String> authScopes)
    {
        // temporary
        return false;
    }


    // Regardless of the auth type,
    // whether we will try automatic auth attempt.
    // 0 means no auto attempts.
    // Returns # of repeats/tries for the given attempt.

    @Override
    public int getAutomaticAuthTries()
    {
        // temporary
        return 0;
    }


    // Whether to show login/logout menu items
    // in the ActionBar Options menu.


    // If set to true, no auth options will be disabled.
    @Override
    public boolean isAuthOptionsMenuDisabled()
    {
        // temporary
        return false;
    }

    // If true, include "login" menu, if possible.
    @Override
    public boolean includeAuthorizeOptionsMenu()
    {
        // temporary
        return true;
    }

    // If true, include "logout" menu, if possible.
    @Override
    public boolean includeUnauthorizeOptionsMenu()
    {
        // temporary
        return true;
    }

    @Override
    public boolean includeAccountConsoleMenu()
    {
        // temporary
        return true;
    }


    // Etc...
    // ...
    // ...


}

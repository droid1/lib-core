package ms.airy.lib.core.auth.prefs;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.SharedPreferences;

import java.util.Set;

import ms.airy.lib.core.auth.common.AccountAuthToken;
import ms.airy.lib.core.auth.common.AccountType;
import ms.airy.lib.core.auth.helper.AccountTypeUtil;
import ms.airy.lib.core.auth.util.OAuth2ScopeUtil;


// TBD:
// This is not about "user preferences".
// It is about SharedPreferences storage.
public final class AccountPrefsStoreManager
{
    // Note:
    // There is at most one "primary" account across all account types nad names.
    // for each type, there can be at most one "main" account.
    // The primary account is the main account of the given type.
    // Each main account can be "logged on" or "logged out"
    //      (e.g., whether the user authorized the app)

    // temporary
    private static final String SP_FILENAME_SUFFIX = ".account_prefs";
    private static final String KEY_PRIMARY_ACCOUNT_TYPE = "primary.account.type";
    private static final String KEY_PRIMARY_ACCOUNT_NAME = "primary.account.name";
//    private static final String KEY_PRIMARY_ACCOUNT_TYPE_AND_NAME = "primary.account";

    private static final String KEY_SUFFIX_MAIN_ACCOUNT_NAME = ".main.account.name";

    private static final String KEY_SUFFIX_AUTH_TOKEN = ".account.authtoken";
    private static final String KEY_SUFFIX_AUTH_SCOPES = ".account.authscopes";
    private static final String KEY_SUFFIX_AUTH_TIME = ".account.authtime";
    private static final String KEY_SUFFIX_AUTH_FAILURE_REASON = ".account.authfailure.reason";
    // ...


    private static AccountPrefsStoreManager INSTANCE = null;
    public static AccountPrefsStoreManager getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new AccountPrefsStoreManager(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;

    // Separate Shared Preferences file to store auth related data.
    private final String sharedPrefsFile;
    private final SharedPreferences sharedPreferences;

    private AccountPrefsStoreManager(Context context)
    {
        this.context = context;
        sharedPrefsFile = context.getPackageName() + SP_FILENAME_SUFFIX;
        sharedPreferences = context.getSharedPreferences(sharedPrefsFile, Context.MODE_PRIVATE);
    }
    public String getSharedPrefsFile()
    {
        return sharedPrefsFile;
    }

    private static String getPrefKeyPrefixForType(int type)
    {
        // temporary
        return "acccount-type-" + type;
    }
    private static String getMainAccountKeyForType(int type)
    {
        // temporary
        return getPrefKeyPrefixForType(type) + KEY_SUFFIX_MAIN_ACCOUNT_NAME;
    }

    private static String getPrefKeyPrefixForAccount(int type, String name)
    {
        // temporary
        return "acccount-" + type + "--" + name;
    }
    private static String getAuthTokenKeyForAccount(int type, String name)
    {
        // temporary
        return getPrefKeyPrefixForAccount(type, name) + KEY_SUFFIX_AUTH_TOKEN;
    }
    private static String getAuthScopesKeyForAccount(int type, String name)
    {
        // temporary
        return getPrefKeyPrefixForAccount(type, name) + KEY_SUFFIX_AUTH_SCOPES;
    }
    private static String getAuthTimeKeyForAccount(int type, String name)
    {
        // temporary
        return getPrefKeyPrefixForAccount(type, name) + KEY_SUFFIX_AUTH_TIME;
    }
    private static String getAuthFailureReasonKeyForAccount(int type, String name)
    {
        // temporary
        return getPrefKeyPrefixForAccount(type, name) + KEY_SUFFIX_AUTH_FAILURE_REASON;
    }


    public int getPrimaryAccountType()
    {
        int type = sharedPreferences.getInt(KEY_PRIMARY_ACCOUNT_TYPE, AccountType.TYPE_NONE);
        if(Log.D) Log.d("Primary account type = " + type);
        return type;
    }
    public String getPrimaryAccountName()
    {
        String name = sharedPreferences.getString(KEY_PRIMARY_ACCOUNT_NAME, null);
        if(Log.D) Log.d("Primary account name = " + name);
        return name;
    }
    public Account getPrimaryAccount()
    {
        AccountManager accountManager = AccountManager.get(context);
        int type = getPrimaryAccountType();
        // The stored value should always be valid... except if none has been set to begin with.
        if(! AccountTypeUtil.isValid(type)) {
            Log.w("Primary account not found.");
            return null;
        }
        Account[] accounts = accountManager.getAccountsByType(AccountTypeUtil.getTypeString(type));
        if(accounts == null || accounts.length == 0) {
            // This should not happen.
            Log.w("No accounts were found for type = " + type);
            return null;
        }
        String name = getPrimaryAccountName();
        // The stored name should generally be valid...
        // But, the account might have been removed from the device since the last time.
        if(name == null || name.isEmpty()) {
            Log.w("Primary account not found. Account name is invalid for type = " + type);
            return null;
        }

        Account account = null;
        for(Account a : accounts) {
            String n = a.name;
            if(name.equals(n)) {
                account = a;
                break;
            }
        }

        return account;
    }

    public void setPrimaryAccount(Account account)
    {
        int type = AccountTypeUtil.getAccountType(account.type);
        String name = account.name;
        setPrimaryAccount(type, name);
    }
    public void setPrimaryAccount(int type, String name)
    {
        int pt = getPrimaryAccountType();
        String pn = getPrimaryAccountName();
        if ((type == pt) && (name.equals(pn))) {
            // the account is already the primary account.
            Log.i("Primary account not changed. type = " + type + "; name = " + name);
        } else {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt(KEY_PRIMARY_ACCOUNT_TYPE, type);
            editor.putString(KEY_PRIMARY_ACCOUNT_NAME, name);
            String mn = getMainAccountNameForType(type);
            // if(type == pt || !name.equals(mn)) {  // (type == pt) redundant????
            if(!name.equals(mn)) {
                // Change the main account for the given type.
                editor.putString(getMainAccountKeyForType(type), name);
                // Remove the old auth token. ????
                editor.remove(getAuthTokenKeyForAccount(type, mn));
                editor.remove(getAuthTimeKeyForAccount(type, mn));

                // TBD:
                // Log out from the provider???
                // ...
            }
            editor.commit();
            if (Log.I) Log.i("Primary account set to type = " + type + "; name = " + name);
        }
    }


    public String getMainAccountNameForType(int type)
    {
        String name = sharedPreferences.getString(getMainAccountKeyForType(type), null);
        if(Log.D) Log.d("Main account name = " + name + " for type = " + type);
        return name;
    }
    public Account getMainAccountForType(int type)
    {
        AccountManager accountManager = AccountManager.get(context);
        Account[] accounts = accountManager.getAccountsByType(AccountTypeUtil.getTypeString(type));
        if(accounts == null || accounts.length == 0) {
            // This should not happen.
            Log.w("No accounts found for type = " + type);
            return null;
        }
        String name = getMainAccountNameForType(type);
        // The stored name should generally be valid...
        // But, the account might have been removed from the device since the last time.
        if(name == null || name.isEmpty()) {
            Log.w("Main account not found. Account name is invalid for type = " + type);
            return null;
        }

        Account account = null;
        for(Account a : accounts) {
            String n = a.name;
            if(name.equals(n)) {
                account = a;
                break;
            }
        }

        return account;
    }

    public void setMainAccount(Account account)
    {
        int type = AccountTypeUtil.getAccountType(account.type);
        String name = account.name;
        setMainAccount(type, name);
    }
    public void setMainAccount(int type, String name)
    {
        if(type == getPrimaryAccountType()) {
            // This is a problem.
            // What if other account of the given type was a primary account?
            // If we set this new type/anme as the main account of the given type,
            // we may end up with a primary account which is not a main account.
            // Hence, we need to do the following.
            // heck!!!!
            // TBD: Is there a beter way???
            setPrimaryAccount(type, name);
        } else {
            String mn = getMainAccountNameForType(type);
            if(name.equals(mn)) {
                // the account is already the main account.
                Log.i("Main account not changed. type = " + type + "; name = " + name);
            } else {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(getMainAccountKeyForType(type), name);
                // Remove the old auth token. ????
                editor.remove(getAuthTokenKeyForAccount(type, mn));
                editor.remove(getAuthTimeKeyForAccount(type, mn));
                editor.commit();
                if (Log.I) Log.i("Main account set to type = " + type + "; name = " + name);

                // TBD:
                // Log out from the provider???
                // ...
            }
        }
    }


    // Note:
    // The user can be logged on to no more than one account of the given type at any moment.
    // Every time the user switches accounts (e.g., by setting main accounts),
    // we basically delete the auth state of the given account type.


    // This only works for the current main accounts.
    public String getAuthToken(int type, String name)
    {
        String authToken = sharedPreferences.getString(getAuthTokenKeyForAccount(type, name), null);
        // Log.w(">>>>>>>>>> getAuthToken(): authToken = " + authToken + " for type = " + type + "; name = " + name);
        if(Log.D) Log.d("authToken = " + authToken + " for type = " + type + "; name = " + name);
        return authToken;
    }
    // Ditto.
    public String getAuthScopes(int type, String name)
    {
        String authScopes = sharedPreferences.getString(getAuthScopesKeyForAccount(type, name), null);
        if(Log.D) Log.d("authScopes = " + authScopes + " for type = " + type + "; name = " + name);
        return authScopes;
    }
    public Set<String> getAuthScopeSet(int type, String name)
    {
        String authScopes = getAuthScopes(type, name);
        return OAuth2ScopeUtil.generateScopeSet(authScopes);
    }
    public String getAuthFailureReason(int type, String name)
    {
        String failureReason = sharedPreferences.getString(getAuthFailureReasonKeyForAccount(type, name), null);
        if(Log.D) Log.d("failureReason = " + failureReason + " for type = " + type + "; name = " + name);
        return failureReason;
    }
    public long getAuthTime(int type, String name)
    {
        long authTime = sharedPreferences.getLong(getAuthTimeKeyForAccount(type, name), 0L);
        if(Log.D) Log.d("authTime = " + authTime + " for type = " + type + "; name = " + name);
        return authTime;
    }
    public AccountAuthToken getAccountAuthToken(int type, String name)
    {
        String authToken = getAuthToken(type, name);
        String authScopes = getAuthScopes(type, name);
        long authTime = getAuthTime(type, name);
        String failureReason = getAuthFailureReason(type, name);
        if(Log.D) Log.d("authToken = " + authToken + "; authScopes = " + authScopes + "; authTime = " + authTime + "; failureReason = " + failureReason + " for type = " + type + "; name = " + name);

        AccountAuthToken accountAuthToken = new AccountAuthToken(authToken, authScopes, authTime, failureReason);
        return accountAuthToken;
    }

    // These methods work for main accounts only.
    // Call setMainAccount() first before calling these.
    public void setAccountAuthToken(int type, String name, String authToken, String authScopes, long authTime)
    {
        setAccountAuthToken(type, name, authToken, authScopes, authTime, null);
    }
    public void setAccountAuthToken(int type, String name, long authTime, String failureReason)
    {
        setAccountAuthToken(type, name, null, null, authTime, failureReason);
    }
    // Note that this method can be used for both auth success and failure....
    public void setAccountAuthToken(int type, String name, String authToken, String authScopes, long authTime, String failureReason)
    {
        String pn = getMainAccountNameForType(type);
        if(name.equals(pn)) {
            SharedPreferences.Editor editor = sharedPreferences.edit();

            if(authToken != null && !authToken.isEmpty()) {   // success
                editor.putString(getAuthTokenKeyForAccount(type, name), authToken);
                editor.putString(getAuthScopesKeyForAccount(type, name), authScopes);
                // editor.remove(getAuthFailureReasonKeyForAccount(type, name));
                editor.putString(getAuthFailureReasonKeyForAccount(type, name), null);
            } else {   // failure
                editor.putString(getAuthTokenKeyForAccount(type, name), null);
                // editor.remove(getAuthScopesKeyForAccount(type, name));   // ????
                editor.putString(getAuthScopesKeyForAccount(type, name), null);
                editor.putString(getAuthFailureReasonKeyForAccount(type, name), failureReason);
            }
//            if(authTime == 0L) {
//                authTime = System.currentTimeMillis();   // ????
//            }
            editor.putLong(getAuthTimeKeyForAccount(type, name), authTime);

            editor.commit();
        } else {
            // ???
            Log.w("setAccountAuthToken() cannot be called for non-main accounts.");
        }
    }
    // This works for both main and non-main accounts.
    public void removeAccountAuthToken(int type, String name)
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(getAuthTokenKeyForAccount(type, name));
        editor.remove(getAuthScopesKeyForAccount(type, name));
        editor.remove(getAuthTimeKeyForAccount(type, name));
        editor.remove(getAuthFailureReasonKeyForAccount(type, name));
        editor.commit();

        // TBD:
        // Log out from the provider???
        // ...
    }



    // For debugging/testing purposes
    protected void removePrefsForAccount(int type, String name)
    {
        Log.w("======== AccountPrefsStoreManager.removePrefs() type = " + type + "; name = " + name);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(getAuthTokenKeyForAccount(type, name));
        editor.remove(getAuthScopesKeyForAccount(type, name));
        editor.remove(getAuthTimeKeyForAccount(type, name));
        editor.commit();
    }
    protected void removeAllPrefs()
    {
        Log.w("======== AccountPrefsStoreManager.removeAllPrefs()");

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }


}

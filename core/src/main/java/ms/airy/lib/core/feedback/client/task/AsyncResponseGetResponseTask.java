package ms.airy.lib.core.feedback.client.task;

import android.os.AsyncTask;

import ms.airy.lib.core.feedback.client.OnlineResponseClient;
import ms.airy.lib.core.feedback.client.factory.FeedbackClientFactory;
import ms.airy.lib.core.feedback.common.ResponseEntry;


/**
 */
public class AsyncResponseGetResponseTask extends AsyncTask<Long, Void, ResponseEntry>
{

    private final AsyncTaskCallback callback;
    public AsyncResponseGetResponseTask(AsyncTaskCallback callback)
    {
        this.callback = callback;
    }


    // Store input params during processing
    private long id;

    @Override
    protected ResponseEntry doInBackground(Long... params)
    {
        id = params[0];

        OnlineResponseClient client = FeedbackClientFactory.getInstance().getResponseClient();

        ResponseEntry responseEntry = client.getResponse(id);

        return responseEntry;
    }


    @Override
    protected void onPostExecute(ResponseEntry responseEntry)
    {
        // super.onPostExecute(aLong);

        this.callback.onGetResponse(responseEntry, id);
    }





    // Callback interface
    public static interface AsyncTaskCallback
    {
        void onGetResponse(ResponseEntry responseEntry, long id);
    }

}

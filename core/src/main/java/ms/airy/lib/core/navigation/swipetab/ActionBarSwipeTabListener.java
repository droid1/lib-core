package ms.airy.lib.core.navigation.swipetab;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.support.v4.view.ViewPager;


public class ActionBarSwipeTabListener<T extends Fragment> implements ActionBar.TabListener
{
    /**
     * The {@link android.support.v4.view.ViewPager} that will host the section contents.
     */
    private final ViewPager mViewPager;

//    private Fragment mFragment;
//    private final Activity mActivity;
//    private final String mTag;
//    private final Class<T> mClass;

//    /** Constructor used each time a new tab is created.
//     * @param activity  The host Activity, used to instantiate the fragment
//     * @param tag  The identifier tag for the fragment
//     * @param clz  The fragment's Class, used to instantiate the fragment
//     */
//    public ActionBarSwipeTabListener(Activity activity, String tag, Class<T> clz)
//    {
//        mActivity = activity;
//        mTag = tag;
//        mClass = clz;
//    }

    public ActionBarSwipeTabListener(ViewPager mViewPager)
    {
        this.mViewPager = mViewPager;
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft)
    {
        // When the tab is selected, switch to the
        // corresponding page in the ViewPager.
        mViewPager.setCurrentItem(tab.getPosition());

//
//
//        // Check if the fragment is already initialized
//        if (mFragment == null) {
//            // If not, instantiate and add it to the activity
//
//            // temporary
//            int pos = tab.getPosition();
//            CharSequence text = tab.getText();
//            mFragment = SwipeTabMainFragment.newInstance(pos, (text != null) ? text.toString() : null);
//            // temporary
//
//            ft.add(android.R.id.content, mFragment, mTag);
//        } else {
//            // If it exists, simply attach it in order to show it
//            ft.attach(mFragment);
//        }
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft)
    {
//        if (mFragment != null) {
//            // Detach the fragment, because another one is being attached
//            ft.detach(mFragment);
//        }
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft)
    {
        // User selected the already selected tab. Usually do nothing.
    }

}
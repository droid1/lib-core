package ms.airy.lib.core.dialog.stock;

import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.DialogInterface;

import ms.airy.lib.core.R;
import ms.airy.lib.core.dialog.ActionableAlertDialogHost;
import ms.airy.lib.core.dialog.ActionableAssertDialogHost;
import ms.airy.lib.core.dialog.ActionableAssertDialogListener;
import ms.airy.lib.core.dialog.base.BasicImageDialogFragment;

import java.util.HashSet;
import java.util.Set;


public class SimpleImageDialogFragment extends BasicImageDialogFragment implements ActionableAssertDialogHost
{
    public static SimpleImageDialogFragment newInstance(CharSequence title, int imageResId)
    {
        SimpleImageDialogFragment fragment = new SimpleImageDialogFragment();
        addArguments(fragment, title, imageResId);
        return fragment;
    }
    public static SimpleImageDialogFragment newInstance(int titleResId, int imageResId)
    {
        SimpleImageDialogFragment fragment = new SimpleImageDialogFragment();
        addArguments(fragment, titleResId, imageResId);
        return fragment;
    }
    public SimpleImageDialogFragment()
    {
    }


    ////////////////////////////////////////////////////
    // ActionableAssertDialog interface callbacks

    private final Set<ActionableAssertDialogListener> actionables = new HashSet<ActionableAssertDialogListener>();
    @Override
    public DialogFragment addActionable(ActionableAssertDialogListener actionable)
    {
        actionables.add(actionable);
        return this;
    }


    @Override
    protected AlertDialog.Builder buildDialogBuilder()
    {
        AlertDialog.Builder dialogBuilder = super.buildDialogBuilder();

        dialogBuilder.setPositiveButton(R.string.basic_image_dialog_ok,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int whichButton) {
                        for(ActionableAssertDialogListener a : actionables) {
                            a.onPositiveClick(SimpleImageDialogFragment.this.getTag());
                        }
                    }
                }
        );
        dialogBuilder.setNegativeButton(R.string.basic_image_dialog_dismiss,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int whichButton) {
                        for(ActionableAssertDialogListener a : actionables) {
                            a.onNegativeClick(SimpleImageDialogFragment.this.getTag());
                        }
                    }
                }
        );
        dialogBuilder.setNeutralButton(R.string.basic_image_dialog_cancel,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int whichButton) {
                        for(ActionableAssertDialogListener a : actionables) {
                            a.onNeutralClick(SimpleImageDialogFragment.this.getTag());
                        }
                    }
                }
        );

        return dialogBuilder;
    }



}

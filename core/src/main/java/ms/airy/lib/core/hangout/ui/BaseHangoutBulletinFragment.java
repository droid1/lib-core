package ms.airy.lib.core.hangout.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import ms.airy.lib.core.R;
import ms.airy.lib.core.fragment.listener.KeyboardEventListener;
import ms.airy.lib.core.fragment.listener.TouchEventListener;


/**
 */
public class BaseHangoutBulletinFragment extends Fragment implements View.OnClickListener, KeyboardEventListener, TouchEventListener
{

    public BaseHangoutBulletinFragment()
    {
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_base_hangout_bulletin, container, false);


        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
    }


    @Override
    public void onStart()
    {
        super.onStart();
    }

    @Override
    public void onStop()
    {
        super.onStop();
    }






    ///////////////////////////////////////////
    // View.OnClickListener interface

    @Override
    public void onClick(View v)
    {
        int id = v.getId();

        // leave
        // extend the time
        // invite friends
        // ...


    }



    ///////////////////////////////////////////
    // KeyboardEventListener interface

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent keyEvent)
    {
        // Override the Back button
        if(keyCode == KeyEvent.KEYCODE_BACK
                && keyEvent.getRepeatCount() == 0) {    // ???
            // TBD
        }

        return false;
    }

    @Override
    public boolean onKeyLongPress(int keyCode, KeyEvent keyEvent)
    {
        return false;
    }

    @Override
    public boolean onKeyMultiple(int keyCode, int repeats, KeyEvent keyEvent)
    {
        return false;
    }

    @Override
    public boolean onKeyShortcut(int keyCode, KeyEvent keyEvent)
    {
        return false;
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent keyEvent)
    {
        return false;
    }


    ///////////////////////////////////////////
    // TouchEventListener interface

    @Override
    public boolean onTouchEvent(MotionEvent motionEvent)
    {
        return false;
    }

}

package ms.airy.lib.core.phaser.common;


import ms.airy.lib.core.phaser.phase.PhaseHelper;
import ms.airy.lib.core.phaser.phase.PhaseUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * A "feature" can be a module, activity, a UI button, or anything...
 * Note that this is tailored to games, it can still be used for non-game apps.
 * For example, "badge" can mean different things in different apps, etc.
 */
public class Feature
{
    // (1) Note that string is easier to read.
    // Hence, we use "code" instead of "id".
    // The code may comprise a number of "parts" (e.g., separated by dash, etc.)
    //      to easily identify various attrs, etc...
    // private final long id;
    private final String code;
    // (2) type, name, desc are all optional.
    private String type = null;  // activity,  etc...
    private String name = null;
    private String description = null;
    // (3) Master flag.
    // If disabled==true, other fields do not matter.
    // The feature is disabled to everyone.
    // This should be normally false.
    // Only use it when absolutely necessary..
    private boolean disabled = false;   // master flag.
    // ** For all fields in the following, null means it's not set or it's not relevant.
    // (4) This feature is enabled only when the current phase number (system global, app version dependent)
    //          falls between minPhase and maxPhase, both inclusive.
    // TBD: Just use version number instead of "phase" ???
    private Integer minPhase = null;   // null means 1.
    private Integer maxPhase = null;   // null means infinity
    // (5) This feature may dependent on the current "level" of the game
    //     (Note that although this is dynamic, it is not really "user-dependent".
    //      This feature is contingent upon the game level, but not specific to a particular user.)
    private Integer minLevel = null;   // null means 1.
    private Integer maxLevel = null;   // null means infinity
    // (*) Another "master" flag.
    //     if set to true, this feature is "user-dependent".
    //     If set to false, the following attrs do not matter.
    // --> Use the "cache" var based on the values of the "user attrs"
    // private boolean userDependent = false;
    // ** The "bit mask" attr comprises both positive and negative bits.
    //    The absence of neither (from the pair) means the attr/bit-pair is not relevant.
    // (6) The feature is available only for the users who are in the specified state.
    //    e.g., logged on, etc.
    private Long state = 0L;       // bit mask
    // (7) The feature is available only for the users who have, or do not have, the specified roles.
    private Long role = 0L;        // bit mask
    // (8) The feature is available only for the users who have, or do not have, the specified badges/accomplishments.
    private Long badge = 0L;       // bit mask
    // (9) Available only if minRank <= user's rank <= maxRank, both inclusive.
    // Rank-based enabling/disabling is mainly useful
    //    when enabling/disabling features at the same level.
    // (E.g., features that are available at a certain level
    //    may only be availble to users with certain ranks, etc.)
    private Integer minRank = null;
    private Integer maxRank = null;
    // etc ....


    // "cache" vars..
    // For performance reasons....
//    private Boolean includedInPhase = null;
    private Map<Integer, Boolean> includedInPhase = new HashMap<Integer, Boolean>();
    private Boolean userDependent = null;
    // etc...


    // Ctor's

    public Feature(String code)
    {
        this.code = code;
    }

    public Feature(String code, String type)
    {
        this(code, type, null, null);
    }
    public Feature(String code, String type, String name, String description)
    {
        this.code = code;
        this.type = type;
        this.name = name;
        this.description = description;
    }

    public Feature(String code, Integer minPhase, Integer maxPhase)
    {
        this(code, minPhase, maxPhase, null, null);
    }
    public Feature(String code, Integer minPhase, Integer maxPhase, Integer minLevel, Integer maxLevel)
    {
        this.code = code;
        this.minPhase = minPhase;
        this.maxPhase = maxPhase;
        this.minLevel = minLevel;
        this.maxLevel = maxLevel;
    }


    public String getCode()
    {
        return code;
    }

    public String getType()
    {
        return type;
    }
    public void setType(String type)
    {
        this.type = type;
    }

    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }


    public boolean isDisabled()
    {
        return disabled;
    }
    public void setDisabled(boolean disabled)
    {
        this.disabled = disabled;
    }


    public Integer getMinPhase()
    {
        return minPhase;
    }
    public void setMinPhase(Integer minPhase)
    {
        this.minPhase = minPhase;
        includedInPhase = null;
    }

    public Integer getMaxPhase()
    {
        return maxPhase;
    }
    public void setMaxPhase(Integer maxPhase)
    {
        this.maxPhase = maxPhase;
        includedInPhase = null;
    }


    public Integer getMinLevel()
    {
        return minLevel;
    }
    public void setMinLevel(Integer minLevel)
    {
        this.minLevel = minLevel;
    }

    public Integer getMaxLevel()
    {
        return maxLevel;
    }
    public void setMaxLevel(Integer maxLevel)
    {
        this.maxLevel = maxLevel;
    }


    public Long getState()
    {
        return state;
    }
    public void setState(Long state)
    {
        this.state = state;
        if(state != null && state != 0L) {
            userDependent = true;
        } else {
            userDependent = null;
        }
    }

    public Long getRole()
    {
        return role;
    }
    public void setRole(Long role)
    {
        this.role = role;
        if(role != null && role != 0L) {
            userDependent = true;
        } else {
            userDependent = null;
        }
    }

    public Long getBadge()
    {
        return badge;
    }
    public void setBadge(Long badge)
    {
        this.badge = badge;
        if(badge != null && badge != 0L) {
            userDependent = true;
        } else {
            userDependent = null;
        }
    }


    public Integer getMinRank()
    {
        return minRank;
    }
    public void setMinRank(Integer minRank)
    {
        this.minRank = minRank;
    }

    public Integer getMaxRank()
    {
        return maxRank;
    }
    public void setMaxRank(Integer maxRank)
    {
        this.maxRank = maxRank;
    }



    // The "main" functions
    public boolean isFeatureAvailable()
    {
        // Input arg level==0 means, Is this feature available for all levels?
        return isFeatureAvailable(0);
    }
    public boolean isFeatureAvailable(int level)
    {
        // ???? What does the input mask 0L mean?????
        // Either they are unknown, or we don't care.... ????
        return isFeatureAvailable(level, 0L, 0L, 0L);
    }
    public boolean isFeatureAvailable(int level, long userState, long userRole, long userBadge)
    {
        // userRank==0 means it's unknown... ???
        return isFeatureAvailable(level, userState, userRole, userBadge, 0);
    }
    public boolean isFeatureAvailable(int level, long userState, long userRole, long userBadge, int userRank)
    {
        // processing by elimination....
        // Note the order...
        if(isDisabled()) {
            return false;
        }
        if(! isIncludedInPhase()) {
            return false;
        }
        if(! isEnabledForLevel(level)) {
            return false;
        }

        if(isUserDependent()) {
            if(! isAvailableForState(userState)) {
                return false;
            }
            if(! isAvailableForRole(userRole)) {
                return false;
            }
            if(! isAvailableForBadge(userBadge)) {
                return false;
            }
            if(! isAvailableForRank(userRank)) {
                return false;
            }
        }

        // finally
        return true;
    }




    // [1]
    protected boolean isIncludedInPhase()
    {
        // TBD:
        // Read the current phase, e.g., from config
        // and compute includedInPhase...
        // ???
        // ...
        // temporary
        int currentPhase = 1;
        return isIncludedInPhase(currentPhase);
    }
    public boolean isIncludedInPhase(int currentPhase)
    {
        if(includedInPhase.get(currentPhase) == null) {
            // (1)
            if(! isPhaseDependent()) {
                includedInPhase.put(currentPhase, true);
            } else {
                // TBD:
                // temporary
                boolean phaseIncluded = PhaseUtil.isPhaseIncluded(currentPhase, minPhase, maxPhase);
                includedInPhase.put(currentPhase, phaseIncluded);
            }
        }
        return includedInPhase.get(currentPhase);
    }

    // ????
    private boolean isPhaseDependent()
    {
        if((minPhase != null && minPhase > Phase.P01)
                || (maxPhase != null && maxPhase < Phase.FINAL)
                ) {
            return true;
        } else {
            return false;
        }
    }


    // [2]
    public boolean isEnabledForLevel(int level)
    {
        if(level == 0) {
            return true;
        }


        // TBD:
        return true;
    }
    private boolean isLevelDependent()
    {
        // TBD:
        // this is wrong...
        if((minLevel != null && minLevel > Level.MIN)
                || (maxLevel != null && maxLevel < Level.MAX)) {
            return true;
        } else {
            return false;
        }
    }



    // [3]

    public boolean isAvailableForState(long userState)
    {
        // userState==0L means we do not care about the user state
        //     regardlesss of the feature definition... ?????
        if(userState == 0L) {
            return true;
        }
        // This feature is not state dependent..
        if(state == null || state == 0L) {
            return true;
        }
        // ????
        if((state & userState) == state) {
            // userState is "consistent" with the state requirement.
            return true;
        } else {
            return false;
        }
    }

    public boolean isAvailableForRole(long userRole)
    {
        // userRole==0L means we do not care about the user role
        //     regardlesss of the feature definition... ?????
        if(userRole == 0L) {
            return true;
        }
        // This feature is not role dependent..
        if(role == null || role == 0L) {
            return true;
        }
        // ????
        if((role & userRole) == role) {
            // userRole is "consistent" with the role requirement.
            return true;
        } else {
            return false;
        }
    }

    public boolean isAvailableForBadge(long userBadge)
    {
        // userBadge==0L means we do not care about the user badge
        //     regardlesss of the feature definition... ?????
        if(userBadge == 0L) {
            return true;
        }
        // This feature is not badge dependent..
        if(badge == null || badge == 0L) {
            return true;
        }
        // ????
        if((badge & userBadge) == badge) {
            // userBadge is "consistent" with the badge requirement.
            return true;
        } else {
            return false;
        }
    }

    // TBD:
    public boolean isAvailableForRank(int userRank)
    {
        // ???
        if(userRank == 0) {
            return true;
        }
        if(minRank != null && Rank.isValid(minRank) && userRank < minRank ) {
            return false;
        }
        if(maxRank != null && Rank.isValid(maxRank) && userRank > maxRank) {
            return false;
        }
        return true;
    }



    protected boolean isUserDependent()
    {
        if(userDependent == null) {
            if(! isStateDependent()
                    && ! isRoleDependent()
                    && ! isBadgeDependent()
                    && ! isRankDependent()) {
                userDependent = false;
            } else {
                // TBD:
                // ....

                // temporary
                userDependent = true;
            }
        }
        return userDependent;
    }



    private boolean isStateDependent()
    {
        if(state != null && state != 0L) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isRoleDependent()
    {
        if(role != null && role != 0L) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isBadgeDependent()
    {
        if(badge != null && badge != 0L) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isRankDependent()
    {
        if((minRank != null && Rank.isValid(minRank))
                && (maxRank != null && Rank.isValid(maxRank))) {
            return true;
        } else {
            return false;
        }
    }

}



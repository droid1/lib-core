package ms.airy.lib.core.startup.lazy;


// TBD:
// ...


// Mainly for singletons that include "lazy initialization"
// The data can be initialized at startup, or delayed (e.g., using handlers)
// rather than waiting for the first use...
public interface LazyInitializable
{
    // TBD:
}

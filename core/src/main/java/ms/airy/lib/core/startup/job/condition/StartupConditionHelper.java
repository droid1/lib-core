package ms.airy.lib.core.startup.job.condition;

import android.content.Context;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ms.airy.lib.core.startup.job.StartupJob;
import ms.airy.lib.core.startup.prefs.StartupPrefsStoreManager;
import ms.airy.lib.core.startup.task.StartupTask;
import ms.airy.lib.core.app.VersionManager;


// TBD:
public final class StartupConditionHelper
{
    private static StartupConditionHelper INSTANCE = null;
    public static StartupConditionHelper getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new StartupConditionHelper(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;
    
    // TBD:
    private final int versionCode;

    private StartupConditionHelper(Context context)
    {
        this.context = context;
        
        // temporary.
        versionCode = VersionManager.getInstance(context).getVersionCode();
        // ...
    }

    // temporary
    // higher precedence comes first (that is, reversely sorted).
    private Comparator<StartupJob> jobComparator = new Comparator<StartupJob>()
    {
        @Override
        public int compare(StartupJob lhs, StartupJob rhs)
        {
            if(lhs == null && rhs == null) {
                return 0;
            } else if(lhs == null) {
                return 1;
            } else if(rhs == null) {
                return -1;
            } else {
                int lhsPriority = lhs.getPriority();
                int rhsPriority = rhs.getPriority();
                return (lhsPriority > rhsPriority) ? -1 :
                        ((lhsPriority < rhsPriority) ? 1 : 0);
            }
        }
    };

    public List<StartupJob> filterTasks(List<StartupJob> inList)
    {
        if(inList == null || inList.isEmpty()) {
            return inList;
        }

        // (1) check the user's counter, etc.
        // (2) get all tasks
        // (3) check if the task has been performed.

        // TBD:
        boolean isFirstStartup = StartupPrefsStoreManager.getInstance(context).isFirstStartup();
        int startupCounterForVersion = StartupPrefsStoreManager.getInstance(context).getStartupCounterForVersion(versionCode);

        List<StartupJob> outList = new ArrayList<StartupJob>();
        for(StartupJob sj : inList) {
            if(sj == null) {   // ???
                continue;
            }
            TaskCondition tc = sj.getCondition();
            if(tc == null) {   // ???
                continue;
            }
            StartupTask st = sj.getTask();
            if(st == null) {   // ???
                continue;
            }
            if(tc.isOnlyForFirstStartup()) {
                if(isFirstStartup) {
                    outList.add(sj);
                }
            } else {
                // We should really use equality, but just to be sure
                //    we use a small window.
                final int delta = 2;
                if(tc.getThresholdStartupCounter() <= startupCounterForVersion
                    && tc.getThresholdStartupCounter() + delta > startupCounterForVersion
                        ) {
                    long taskId = st.getId();
                    if(StartupPrefsStoreManager.getInstance(context).isTaskNew(taskId)) {
                        outList.add(sj);
                    }
                }
            }
        }

        // Sort the list reversely in terms of priority.
        Collections.sort(outList, jobComparator);

        return outList;
    }




}

package ms.airy.lib.core.layout;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Canvas;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;


// TBD:
// Draws texts/buttons at specified positions.
public class GraffitiLayout extends View
{

    public GraffitiLayout(Context context)
    {
        super(context);
        initControl(context, null, 0);
    }
    public GraffitiLayout(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        initControl(context, attrs, 0);
    }
    public GraffitiLayout(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        initControl(context, attrs, defStyle);
    }

    private void initControl(Context context, AttributeSet attrs, int defStyle)
    {
        setFocusable(false); // display only. no input handling...
    }



    @Override
    protected void onAttachedToWindow()
    {
        Log.d("onAttachedToWindow() Called.");
        super.onAttachedToWindow();
    }

    @Override
    protected void onDetachedFromWindow()
    {
        Log.d("onDetachedFromWindow() Called.");
        super.onDetachedFromWindow();
    }


    @Override
    protected Parcelable onSaveInstanceState()
    {
        Log.d("onSaveInstanceState() Called.");
        Parcelable superState = super.onSaveInstanceState();
        SavedState savedState = new SavedState(superState);
        // savedState.setDirty(dirty);
        return savedState;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state)
    {
        Log.d("onRestoreInstanceState() Called.");
        SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(savedState.getSuperState());
        // setDirty(savedState.isDirty());
    }

    protected static class SavedState extends BaseSavedState implements Parcelable
    {
        //private boolean dirty;
        // etc...

        public SavedState(Parcelable superState)
        {
            super(superState);
        }
        public SavedState(Parcel source)
        {
            super(source);
            // dirty = (source.readInt() != 0);
        }

//        public boolean isDirty()
//        {
//            return dirty;
//        }
//        public void setDirty(boolean dirty)
//        {
//            this.dirty = dirty;
//        }

        public static final Creator<SavedState> CREATOR = new Creator<SavedState>() {
            @Override
            public SavedState createFromParcel(Parcel source)
            {
                return new SavedState(source);
            }
            @Override
            public SavedState[] newArray(int size)
            {
                return new SavedState[size];
            }
        };

        @Override
        public void writeToParcel(Parcel dest, int flags)
        {
            super.writeToParcel(dest, flags);
            // dest.writeInt(dirty ? 1 : 0);
        }

        // For debugging...
        @Override
        public String toString()
        {
            return "SavedState{" +
                    // "dirty=" + dirty +
                    '}';
        }
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        if(Log.D) Log.d("onMeasure() Called: widthMeasureSpec = " + widthMeasureSpec + "; heightMeasureSpec = " + heightMeasureSpec);
        // super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        // resetOrientation();

        int totalWidth = measureWidth(widthMeasureSpec);
        int totalHeight = measureHeight(heightMeasureSpec);

        // TBD:

        setMeasuredDimension(totalWidth, totalHeight);
    }

    private int measureWidth(int measureSpec)
    {
        int result = 0;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);


        if(Log.V) Log.v("width: result = " + result);
        return result;
    }

    private int measureHeight(int measureSpec)
    {
        int result = 0;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        if(Log.V) Log.v("height: result = " + result);
        return result;
    }


    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom)
    {
        if(Log.D) Log.d("onLayout() Called: changed = " + changed + "; left = " + left + "; top = " + top + "; right = " + right + "; bottom = " + bottom);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh)
    {
        if(Log.D) Log.d("onSizeChanged() Called: w = " + w + "; h = " + h + "; oldw = " + oldw + "; oldh = " + oldh);
        super.onSizeChanged(w, h, oldw, oldh);
    }


    @Override
    protected void onDraw(Canvas canvas)
    {
        Log.d("onDraw() Called.");
        super.onDraw(canvas);
    }

    @Override
    protected void dispatchDraw(Canvas canvas)
    {
        Log.d("dispatchDraw() Called.");
        super.dispatchDraw(canvas);
    }


    @Override
    protected void onConfigurationChanged(Configuration newConfig)
    {
        Log.d("onConfigurationChanged() Called.");
        super.onConfigurationChanged(newConfig);
    }

}

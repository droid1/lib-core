package ms.airy.lib.core.dialog.popup;

import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import ms.airy.lib.core.R;
import ms.airy.lib.core.dialog.ActionableAssertDialogListener;
import ms.airy.lib.core.dialog.base.BasicMessageDialogFragment;

import java.util.HashSet;
import java.util.Set;


// http://developer.android.com/guide/topics/ui/dialogs.html
public class GenericPopupDialogFragment extends BasicMessageDialogFragment
{
    // TBD:
    protected static final String ARG_POPUP_PREFS_KEY = "generic_popup_prefs_key";
    protected static final String ARG_POPUP_CONTENT_FILE = "generic_popup_content_file";



    // Factory methods.
    public static GenericPopupDialogFragment newInstance(CharSequence title, CharSequence message)
    {
        GenericPopupDialogFragment fragment = new GenericPopupDialogFragment();
        addArguments(fragment, title, message);
        return fragment;
    }
    public static GenericPopupDialogFragment newInstance(CharSequence title, CharSequence message, CharSequence positiveLabel, CharSequence negativeLabel, CharSequence neutralLabel)
    {
        GenericPopupDialogFragment fragment = newInstance(title, message);
        addButtonLabelArguments(fragment, positiveLabel, negativeLabel, neutralLabel);
        return fragment;
    }
    public static GenericPopupDialogFragment newInstance(int titleResId, int messageResId)
    {
        GenericPopupDialogFragment fragment = new GenericPopupDialogFragment();
        addArguments(fragment, titleResId, messageResId);
        return fragment;
    }
    public static GenericPopupDialogFragment newInstance(int titleResId, int messageResId, int positiveLabelResId, int negativeLabelResId, int neutralLabelResId)
    {
        GenericPopupDialogFragment fragment =  newInstance(titleResId, messageResId);
        addButtonLabelArguments(fragment, positiveLabelResId, negativeLabelResId, neutralLabelResId);
        return fragment;
    }

    protected static DialogFragment addButtonLabelArguments(DialogFragment fragment, CharSequence positiveLabel, CharSequence negativeLabel, CharSequence neutralLabel)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        if(positiveLabel != null && positiveLabel.length() > 0) {
            args.putCharSequence(ARG_POSITIVE_LABEL, positiveLabel);
        }
        if(negativeLabel != null && negativeLabel.length() > 0) {
            args.putCharSequence(ARG_NEGATIVE_LABEL, negativeLabel);
        }
        if(neutralLabel != null && neutralLabel.length() > 0) {
            args.putCharSequence(ARG_NEUTRAL_LABEL, neutralLabel);
        }
        return fragment;
    }
    protected static DialogFragment addButtonLabelArguments(DialogFragment fragment, int positiveLabelResId, int negativeLabelResId, int neutralLabelResId)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        if(positiveLabelResId > 0) {
            args.putInt(ARG_POSITIVE_LABEL_RESID, positiveLabelResId);
        }
        if(negativeLabelResId > 0) {
            args.putInt(ARG_NEGATIVE_LABEL_RESID, negativeLabelResId);
        }
        if(neutralLabelResId > 0) {
            args.putInt(ARG_NEUTRAL_LABEL_RESID, neutralLabelResId);
        }
        return fragment;
    }

    public GenericPopupDialogFragment()
    {
    }


    ////////////////////////////////////////////////////
    // ActionableAssertDialog interface callbacks

    private final Set<ActionableAssertDialogListener> actionables = new HashSet<ActionableAssertDialogListener>();
    public DialogFragment addActionable(ActionableAssertDialogListener actionable)
    {
        actionables.add(actionable);
        return this;
    }



    private DialogInterface.OnClickListener positiveListener =
            new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int whichButton) {
                    for(ActionableAssertDialogListener a : actionables) {
                        a.onPositiveClick(GenericPopupDialogFragment.this.getTag());
                    }
                }
            };
    private DialogInterface.OnClickListener negativeListener =
            new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int whichButton)
                {
                    for(ActionableAssertDialogListener a : actionables) {
                        a.onNegativeClick(GenericPopupDialogFragment.this.getTag());
                    }
                }
            };
    private DialogInterface.OnClickListener neutralListener =
            new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int whichButton)
                {
                    for(ActionableAssertDialogListener a : actionables) {
                        a.onNeutralClick(GenericPopupDialogFragment.this.getTag());
                    }
                }
            };

    @Override
    protected AlertDialog.Builder buildDialogBuilder()
    {
        CharSequence positiveLabel = getArguments().getCharSequence(ARG_POSITIVE_LABEL);
        int positiveLabelResId = getArguments().getInt(ARG_POSITIVE_LABEL_RESID);
        CharSequence negativeLabel = getArguments().getCharSequence(ARG_NEGATIVE_LABEL);
        int negativeLabelResId = getArguments().getInt(ARG_NEGATIVE_LABEL_RESID);
        CharSequence neutralLabel = getArguments().getCharSequence(ARG_NEUTRAL_LABEL);
        int neutralLabelResId = getArguments().getInt(ARG_NEUTRAL_LABEL_RESID);

        AlertDialog.Builder dialogBuilder = super.buildDialogBuilder();

        if(positiveLabel != null && positiveLabel.length() > 0) {
            dialogBuilder.setPositiveButton(positiveLabel, positiveListener);
        } else {
            int posId = positiveLabelResId;
            if(posId == 0) {
                posId = R.string.basic_response_dialog_positive;
            }
            dialogBuilder.setPositiveButton(posId, positiveListener);
        }
        if(negativeLabel != null && negativeLabel.length() > 0) {
            dialogBuilder.setNegativeButton(negativeLabel, negativeListener);
        } else {
            int negId = negativeLabelResId;
            if(negId == 0) {
                negId = R.string.basic_response_dialog_negative;
            }
            dialogBuilder.setNegativeButton(negId, negativeListener);
        }
        if(neutralLabel != null && neutralLabel.length() > 0) {
            dialogBuilder.setNeutralButton(neutralLabel, neutralListener);
        } else {
            int neuId = neutralLabelResId;
            if(neuId == 0) {
                neuId = R.string.basic_response_dialog_neutral;
            }
            dialogBuilder.setNeutralButton(neuId, neutralListener);
        }

        return dialogBuilder;
    }


}

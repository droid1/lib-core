package ms.airy.lib.core.backup.base;

import android.app.backup.BackupAgentHelper;
import android.app.backup.SharedPreferencesBackupHelper;
import android.content.Context;

import ms.airy.lib.core.auth.prefs.AccountPrefsStoreManager;
import ms.airy.lib.core.config.prefs.PrefsConfigManager;
import ms.airy.lib.core.hangout.prefs.HangoutPrefsStoreManager;
import ms.airy.lib.core.popup.prefs.PopupHostPrefsManager;
import ms.airy.lib.core.popup.prefs.PopupInfoPrefsManager;
import ms.airy.lib.core.startup.prefs.StartupPrefsStoreManager;
import ms.airy.lib.core.user.prefs.UserDevicePrefsStoreManager;


public class BaseBackupAgentHelper extends BackupAgentHelper
{
    // ???
    private static final String PREFS_BACKUP_KEY = "base_prefs";
    private static final String FILES_BACKUP_KEY = "base_files";

    // Default no-arg contructor is required.
    public BaseBackupAgentHelper()
    {
    }

    @Override
    public void onCreate()
    {
        super.onCreate();

        // [1] Shared preferences.

        final String spFileUser = UserDevicePrefsStoreManager.getInstance(this).getSharedPrefsFile();
        final String spFileAccount = AccountPrefsStoreManager.getInstance(this).getSharedPrefsFile();
        final String spFileConfig = PrefsConfigManager.getInstance(this).getSharedPrefsFile();
        final String spFileHangout = HangoutPrefsStoreManager.getInstance(this).getSharedPrefsFile();
        final String spFilePopupHost = PopupHostPrefsManager.getInstance(this).getSharedPrefsFile();
        final String spFilePopupInfo = PopupInfoPrefsManager.getInstance(this).getSharedPrefsFile();
        final String spFileStartup = StartupPrefsStoreManager.getInstance(this).getSharedPrefsFile();

        SharedPreferencesBackupHelper helper = new SharedPreferencesBackupHelper(this,
                spFileUser,
                spFileAccount,
                spFileConfig,
                spFileHangout,
                spFilePopupHost,
                spFilePopupInfo,
                spFileStartup
        );
        addHelper(PREFS_BACKUP_KEY, helper);

        // [2] Internal data (under context.getFilesDir()
        // tbd...

//        final String dataFileXYZ = "xyz";
//        FileBackupHelper helper = new FileBackupHelper(this,
//                dataFileXYZ
//        );
//        addHelper(FILES_BACKUP_KEY, helper);

    }

}

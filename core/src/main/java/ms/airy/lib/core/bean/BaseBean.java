package ms.airy.lib.core.bean;

import android.content.ContentValues;

import ms.airy.lib.core.common.GUID;

// Place-holder for now....
public abstract class BaseBean
{
    // Sqlite3 auto-generated ID ("_id").
    private Long id;

    // All beans (DB records) include guid, and some timestamps, etc. ....
    private String guid;
    private Long createTime;
    private Long modifiedTime;

    public BaseBean()
    {
        this(null);
    }
    public BaseBean(Long id)
    {
        this(id, null);
    }
    public BaseBean(Long id, String guid)
    {
        this(id, guid, null, null);
    }
    public BaseBean(Long id, String guid, Long createTime, Long modifiedTime)
    {
        super();
        this.id = id;  // Can this be null???
//        if(guid == null || guid.isEmpty()) {
//            this.guid = GUID.generate();   // ????   Guid is automatically generated, if null, during saving....
//        } else {
            this.guid = guid;            
//        }
        this.createTime = createTime;
        this.modifiedTime = modifiedTime;
    }

    // No setter?
    public Long getId()
    {
        return this.id;
    }
    
    public String getGuid()
    {
        return guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public Long getCreateTime()
    {
        return createTime;
    }
    public void setCreateTime(Long createTime)
    {
        this.createTime = createTime;
    }

    public Long getModifiedTime()
    {
        return modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }

    
    // TBD: Column name constants????
    public ContentValues getContentValues()
    {
        ContentValues values = new ContentValues();
        
        // Note these column names should be exactly the same across all subclasses of BaseBean.... 
        if(id != null) {
            values.put("_id", id);
        }
        if(guid != null) {
            values.put("guid", guid);
        }
        if(createTime != null) {
            values.put("created_time", createTime);
        }
        if(modifiedTime != null) {
            values.put("modified_time", modifiedTime);
        }

        return values;
    }

    @Override
    public String toString()
    {
        return "BaseBean [id=" + id + ", guid=" + guid + ", createTime="
                + createTime + ", modifiedTime=" + modifiedTime + "]";
    }

    
}

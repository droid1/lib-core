package ms.airy.lib.core.config.json;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;

import ms.airy.lib.core.config.AbstractConfigManager;

import org.miniclient.json.LiteJsonParser;
import org.miniclient.json.MiniClientJsonException;
import org.miniclient.json.parser.MiniClientJsonParser;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Map;


/**
 * Note: We store the custom JSON file in the /res/raw directory.
 *       Although the config file is not exactly an resource,
 *       unfortunately this seems to be the only place where we can put an arbitrary file.
 */
public class JsonConfigManager extends AbstractConfigManager
{
    // Config files under /assets/config
    private static final String CONFIG_DIR = "config";

    // Properties file under the /res/raw directory.
    // It should be passed in, in the form of R.raw._name_.
    private int resourceId;
    // Or, we can use the file name. The file should be in /assets dir.
    private String configFile;
    // Lazy initialized.
    // The json file should be a one-level hashmap (for now).
//    private Object jsonRoot = null;
    private Map<String,Object> jsonRoot = null;


    public JsonConfigManager(Context context)
    {
        this(context, 0);
    }
    public JsonConfigManager(Context context, int resourceId)
    {
        super(context);
        this.resourceId = resourceId;
        this.configFile = null;
        // loadProperties();
    }
    public JsonConfigManager(Context context, String configFile)
    {
        super(context);
        this.resourceId = 0;
        this.configFile = configFile;
        // loadProperties();
    }

    private void loadProperties()
    {
        try {
            InputStream rawResource = null;
            Resources resources = getContext().getResources();
            if(resourceId > 0) {
                rawResource = resources.openRawResource(resourceId);
            } else {
                AssetManager assetManager = resources.getAssets();
                rawResource = assetManager.open(CONFIG_DIR + "/" + configFile);
            }

            LiteJsonParser jsonParser = new MiniClientJsonParser();
            Reader reader = new InputStreamReader(rawResource);
            try {
                // ???
                jsonRoot = (Map<String,Object>) ((Map<?,?>) jsonParser.parse(reader));
                if(Log.D) {
                    Log.d("Parsed: jsonRoot = " + jsonRoot);
                }
            } catch (MiniClientJsonException e) {
                Log.e("Failed to parse the json config file: " + resourceId, e);
            }
        } catch (Resources.NotFoundException e) {
            Log.e("Failed to find the json config file: " + resourceId, e);
        } catch (IOException e) {
            Log.e("Failed to load the json config file: " + resourceId, e);
        }
        if(Log.V) {
            if(jsonRoot != null) {
                Log.v("json config file loaded: " + jsonRoot);
            }
        }
    }


    protected int getResourceId()
    {
        return resourceId;
    }
    protected void setResourceId(int resourceId)
    {
        this.resourceId = resourceId;
        jsonRoot = null;   // ????
    }

    protected String getConfigFile()
    {
        return configFile;
    }
    protected void setConfigFile(String configFile)
    {
        this.configFile = configFile;
        jsonRoot = null;   // ????
    }


    protected Object getJsonRoot()
    {
        return jsonRoot;
    }


    @Override
    public Object get(String key)
    {
        if(jsonRoot == null) {
            loadProperties();
        }
        Object val = null;
        if(jsonRoot != null) {
            // ???
            val = jsonRoot.get(key);
        }
        return val;
    }

    @Override
    public Object get(String key, Object defVal)
    {
        if(jsonRoot == null) {
            loadProperties();
        }
        Object val = null;
        if(jsonRoot != null) {
            // ???
            val = jsonRoot.get(key);
            if(val == null) {
                val = defVal;
            }
        }
        return val;
    }

    @Override
    public String getString(String key)
    {
        String val = null;
        Object obj = get(key);
        if(obj != null && obj instanceof String) {  // TBD: Or, use toString() ???
            val = (String) obj;
        }
        return val;
    }

    @Override
    public String getString(String key, String defVal)
    {
        String val = null;
        Object obj = get(key, defVal);
        if(obj != null && obj instanceof String) {  // TBD: Or, use toString() ???
            val = (String) obj;
        }
        return val;
    }

}


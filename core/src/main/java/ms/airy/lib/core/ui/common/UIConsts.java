package ms.airy.lib.core.ui.common;


public final class UIConsts
{
    // Intent actions...
    public static final String ACTION_VIEW_FULL_SIZE_IMAGE = "ms.airy.lib.core.action.VIEW_FULL_SIZE_IMAGE";
    // ...

    // extra params
    public static final String EXTRA_FULL_SIZE_IMAGE_RESID = "ms.airy.lib.core.extra.FULL_SIZE_IMAGE_RESID";
    // ...

}

package ms.airy.lib.core.notification.common;

import android.app.Notification;


/**
 */
public class NotificationAndTimeStruct
{
    private int notificationId;
    private Notification notification;
    private long scheduledTime;   // Unix epoch time in millis.
    // tbd: jitter???


    public NotificationAndTimeStruct(Notification notification)
    {
        this(notification, 0L);  // 0 means now?
    }
    public NotificationAndTimeStruct(int notificationId, Notification notification)
    {
        this(notificationId, notification, 0L);
    }
    public NotificationAndTimeStruct(Notification notification, long scheduledTime)
    {
        this(0, notification, scheduledTime);
    }
    public NotificationAndTimeStruct(int notificationId, Notification notification, long scheduledTime)
    {
        this.notificationId = notificationId;
        this.notification = notification;
        this.scheduledTime = scheduledTime;
    }

    public int getNotificationId()
    {
        return notificationId;
    }
    public void setNotificationId(int notificationId)
    {
        this.notificationId = notificationId;
    }

    public Notification getNotification()
    {
        return notification;
    }
    public void setNotification(Notification notification)
    {
        this.notification = notification;
    }

    public long getScheduledTime()
    {
        return scheduledTime;
    }
    public void setScheduledTime(long scheduledTime)
    {
        this.scheduledTime = scheduledTime;
    }


    @Override
    public String toString()
    {
        return "NotificationAndTimeStruct{" +
                "notificationId=" + notificationId +
                ", notification=" + notification +
                ", scheduledTime=" + scheduledTime +
                '}';
    }
}

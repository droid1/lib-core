package ms.airy.lib.core.location.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import ms.airy.lib.core.location.common.LocationAccuracy;
import ms.airy.lib.core.location.common.PowerRequirement;
import ms.airy.lib.core.location.common.TrackingFrequency;


// TBD:
// This is not about "user preferences".
// It is about SharedPreferences storage.
// It stores the app-related global user settings.
public final class LocationPrefsStoreManager
{
    // temporary
    private static final String SP_FILENAME_SUFFIX = ".location_prefs";

    // These apply for "background tracking" only.
    // The app may still choose to do the "foreground tracking" based on the need
    //     say, even if trackingEnabled == false.
    // (The app may use the trackingFrenquency value for foreground tracking as well....
    //     --> Note that TrackingFrequency "enum" corresponds to two different set of values
    //     depending on whether the tracking is done in foreground or background...)
    private static final String KEY_TRACKING_ENABLED = "location.background.tracking.enabled";
    private static final String KEY_TRACKING_FREQUENCY = "location.background.tracking.frequency";
    private static final String KEY_LOCATION_ACCURACY = "location.background.tracking.accuracy";
    private static final String KEY_POWER_REQUIREMENT = "location.background.tracking.power";
    // ...

    private static LocationPrefsStoreManager INSTANCE = null;
    public static LocationPrefsStoreManager getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new LocationPrefsStoreManager(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;

    // Separate Shared Preferences file.
    private final String sharedPrefsFile;
    private final SharedPreferences sharedPreferences;

    // "Cache"
    private Boolean trackingEnabled = null;
    private Integer trackingFrequency = null;
    private Integer locationAccuracy = null;
    private Integer powerRequirement = null;


    // TBD:
    // Read the (app-specific) default values from the config???
    // ...

    private LocationPrefsStoreManager(Context context)
    {
        this.context = context;
        sharedPrefsFile = context.getPackageName() + SP_FILENAME_SUFFIX;
        sharedPreferences = context.getSharedPreferences(sharedPrefsFile, Context.MODE_PRIVATE);
    }
    public String getSharedPrefsFile()
    {
        return sharedPrefsFile;
    }


    public boolean isLocationTrackingEnabled()
    {
        if(this.trackingEnabled == null) {
            // default value; false???
            boolean trackingEnabled = sharedPreferences.getBoolean(KEY_TRACKING_ENABLED, false);
            if (Log.D) Log.d("LocationPrefsStoreManager: trackingEnabled = " + trackingEnabled);
            this.trackingEnabled = trackingEnabled;
        }
        return this.trackingEnabled;
    }

    public void setLocationTrackingEnabled(boolean trackingEnabled)
    {
        setLocationTrackingEnabled(trackingEnabled, false);
    }
    public void setLocationTrackingEnabled(boolean trackingEnabled, boolean force)
    {
        if(force || this.trackingEnabled == null || this.trackingEnabled != trackingEnabled) {
            this.trackingEnabled = trackingEnabled;
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean(KEY_TRACKING_ENABLED, trackingEnabled);
            editor.commit();
        }
    }

    public int getLocationTrackingFrequency()
    {
        if(this.trackingFrequency == null) {
            int defaultTrackingFrequency = TrackingFrequency.getDefaultFrequency();
            int trackingFrequency = sharedPreferences.getInt(KEY_TRACKING_FREQUENCY, defaultTrackingFrequency);
            if (Log.D) Log.d("LocationPrefsStoreManager: trackingFrequency = " + trackingFrequency);
            this.trackingFrequency = trackingFrequency;
        }
        return this.trackingFrequency;
    }

    public void setLocationTrackingFrequency(int trackingFrequency)
    {
        setLocationTrackingFrequency(trackingFrequency, false);
    }
    public void setLocationTrackingFrequency(int trackingFrequency, boolean force)
    {
        if(force || this.trackingFrequency == null || this.trackingFrequency != trackingFrequency) {
            this.trackingFrequency = trackingFrequency;
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt(KEY_TRACKING_FREQUENCY, trackingFrequency);
            editor.commit();
        }
    }

    public int getLocationTrackingAccuracy()
    {
        if(this.locationAccuracy == null) {
            int defaultTrackingAccuracy = LocationAccuracy.getDefaultAccuracy();
            int trackingAccuracy = sharedPreferences.getInt(KEY_LOCATION_ACCURACY, defaultTrackingAccuracy);
            if (Log.D) Log.d("LocationPrefsStoreManager: locationAccuracy = " + trackingAccuracy);
            this.locationAccuracy = trackingAccuracy;
        }
        return this.locationAccuracy;
    }

    public void setLocationTrackingAccuracy(int trackingAccuracy)
    {
        setLocationTrackingAccuracy(trackingAccuracy, false);
    }
    public void setLocationTrackingAccuracy(int trackingAccuracy, boolean force)
    {
        if(force || this.locationAccuracy == null || this.locationAccuracy != trackingAccuracy) {
            this.locationAccuracy = trackingAccuracy;
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt(KEY_LOCATION_ACCURACY, trackingAccuracy);
            editor.commit();
        }
    }


    public int getTrackingPowerRequirement()
    {
        if(this.powerRequirement == null) {
            int defaultPowerRequirement = PowerRequirement.getDefaultRequirement();
            int powerRequirement = sharedPreferences.getInt(KEY_POWER_REQUIREMENT, defaultPowerRequirement);
            if (Log.D) Log.d("LocationPrefsStoreManager: powerRequirement = " + powerRequirement);
            this.powerRequirement = powerRequirement;
        }
        return this.powerRequirement;
    }

    public void setTrackingPowerRequirement(int powerRequirement)
    {
        setTrackingPowerRequirement(powerRequirement, false);
    }
    public void setTrackingPowerRequirement(int powerRequirement, boolean force)
    {
        if(force || this.powerRequirement == null || this.powerRequirement != powerRequirement) {
            this.powerRequirement = powerRequirement;
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt(KEY_POWER_REQUIREMENT, powerRequirement);
            editor.commit();
        }
    }



    // For debugging/testing purposes
    public void removeAllPrefs()
    {
        Log.w("======== LocationPrefsStoreManager.removeAllPrefs()");

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }


}

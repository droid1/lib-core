package ms.airy.lib.core.loader.cursor;

import android.app.Activity;
import android.app.ListFragment;
import android.app.LoaderManager;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;


/**
 */
public class SampleCursorLoaderFragment extends ListFragment
{
    private CursorAdapter cursorAdapter = null;

    public SampleCursorLoaderFragment()
    {
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        // setEmptyText("No phone numbers");

        // setHasOptionsMenu(true);

        cursorAdapter = new SimpleCursorAdapter(getActivity(),
                android.R.layout.simple_list_item_2, null,
                new String[] { ContactsContract.Contacts.DISPLAY_NAME, ContactsContract.Contacts.CONTACT_STATUS },
                new int[] { android.R.id.text1, android.R.id.text2 }, 0);
//        cursorAdapter = new SimpleCursorAdapter(getActivity(),
//                android.R.layout.simple_list_item_2, null,
//                new String[] {SensorValueData.SensorValues.TYPE, SensorValueData.SensorValues.ACCURACY },
//                new int[] { android.R.id.text1, android.R.id.text2 }, 0);
        setListAdapter(cursorAdapter);

        LoaderManager.LoaderCallbacks<Cursor> loaderCallbacks = new SampleCursorLoaderCallbacks(getActivity(), cursorAdapter);
        getLoaderManager().initLoader(0, null, loaderCallbacks);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState)
    {
        // TBD:
        View rootView = null;  // inflater.inflate(R.layout.fragment_cursor_loader_main, container, false);


        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentInteractionListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
//        mListener = null;
    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id)
    {
        // super.onListItemClick(l, v, position, id);
        Log.i("Item clicked: position = " + position + "; id = " + id);
//        if (null != mListener) {
//            // Notify the active callbacks interface (the activity, if the
//            // fragment is attached to one) that an item has been selected.
//            // mListener.onFragmentInteraction(DummyContent.ITEMS.get(position).id);
//        }
    }

}

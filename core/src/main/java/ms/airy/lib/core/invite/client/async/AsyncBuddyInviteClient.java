package ms.airy.lib.core.invite.client.async;

import java.util.List;

import ms.airy.lib.core.invite.client.AsyncInviteCallback;
import ms.airy.lib.core.invite.client.AsyncInviteClient;
import ms.airy.lib.core.invite.client.task.AsyncBuddyGetInviteInfoTask;
import ms.airy.lib.core.invite.client.task.AsyncBuddyGetInvitesTask;
import ms.airy.lib.core.invite.client.task.AsyncBuddyGetRecipientInfoTask;
import ms.airy.lib.core.invite.client.task.AsyncBuddyGetRecipientsTask;
import ms.airy.lib.core.invite.client.task.AsyncBuddySendInviteTask;
import ms.airy.lib.core.invite.common.InviteInfo;
import ms.airy.lib.core.invite.common.RecipientInfo;


public class AsyncBuddyInviteClient implements AsyncInviteClient,
        AsyncBuddySendInviteTask.AsyncTaskCallback,
        AsyncBuddyGetInviteInfoTask.AsyncTaskCallback,
        AsyncBuddyGetInvitesTask.AsyncTaskCallback,
        AsyncBuddyGetRecipientInfoTask.AsyncTaskCallback,
        AsyncBuddyGetRecipientsTask.AsyncTaskCallback
{
    private final AsyncInviteCallback callback;

    public AsyncBuddyInviteClient(AsyncInviteCallback callback)
    {
        this.callback = callback;
    }


    @Override
    public void sendInvite(String recipient)
    {
        if(Log.I) Log.i("AsyncBuddyInviteClient.sendInvite(). recipient = " + recipient);

        (new AsyncBuddySendInviteTask(this)).execute(recipient);
    }

    @Override
    public void getInviteInfo(long inviteId)
    {
        if(Log.I) Log.i("AsyncBuddyInviteClient.getInviteInfo(). inviteId = " + inviteId);

        (new AsyncBuddyGetInviteInfoTask(this)).execute(inviteId);
    }

    @Override
    public void getInvites(int status)
    {
        if(Log.I) Log.i("AsyncBuddyInviteClient.getInvites(). status = " + status);

        (new AsyncBuddyGetInvitesTask(this)).execute(status);
    }

    @Override
    public void getRecipientInfo(String recipient)
    {
        if(Log.I) Log.i("AsyncBuddyInviteClient.getRecipientInfo(). recipient = " + recipient);

        (new AsyncBuddyGetRecipientInfoTask(this)).execute(recipient);
    }

    @Override
    public void getRecipients(int status)
    {
        if(Log.I) Log.i("AsyncBuddyInviteClient.getRecipients(). status = " + status);

        (new AsyncBuddyGetRecipientsTask(this)).execute(status);
    }



    ///////////////////////////////////////////////////////
    // AsyncBuddySendInviteTask.AsyncTaskCallback interface

    @Override
    public void onSendInvite(long invitedId, String recipient)
    {
        callback.onSendInvite(invitedId, recipient);
    }

    ///////////////////////////////////////////////////////
    // AsyncBuddyGetInviteInfoTask.AsyncTaskCallback interface

    @Override
    public void onGetInviteInfo(InviteInfo inviteInfo, long inviteId)
    {
        callback.onGetInviteInfo(inviteInfo, inviteId);
    }

    ///////////////////////////////////////////////////////
    // AsyncBuddyGetInvitesTask.AsyncTaskCallback interface

    @Override
    public void onGetInvites(List<Long> invites, int status)
    {
        callback.onGetInvites(invites, status);
    }

    ///////////////////////////////////////////////////////
    // AsyncBuddyGetRecipientInfoTask.AsyncTaskCallback interface

    @Override
    public void onGetRecipientInfo(RecipientInfo recipientInfo, String recipient)
    {
        callback.onGetRecipientInfo(recipientInfo, recipient);
    }

    ///////////////////////////////////////////////////////
    // AsyncBuddyGetRecipientsTask.AsyncTaskCallback interface

    @Override
    public void onGetRecipients(List<String> recipients, int status)
    {
        callback.onGetRecipients(recipients, status);
    }

}

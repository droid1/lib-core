package ms.airy.lib.core.location.common;


// This is for "background" processing.
// Note that the location data may be "processed"
//    whenever new (raw) location data is obtained (e.g., through scheduled location tracking)
public final class ProcessingInterval
{
    public static final int NONE = 0;      // ??? No processing???
    public static final int LONGEST = 1;   // Slowest.
    public static final int LONG = 2;      // Slow.
    public static final int NORMAL = 3;    // Normal.
    public static final int SHORT = 4;     // Fast.
    public static final int SHORTEST = 5;  // Fastest.

    private ProcessingInterval() {}


    // temporary
    public static int getDefaultInterval()
    {
        return NORMAL;
    }
    public static long getDefaultIntervalMillis()
    {
        return getPredefinedPollingInterval(getDefaultInterval());
    }


    // temporary
    // Note that although it is not strictly necessary,
    //    it may be convenient to have this list...
    public static long getPredefinedPollingInterval(int processingInterval)
    {
        switch(processingInterval) {
            case LONGEST:
            default:
                return 60 * 60 * 1000L;
            case LONG:
                return 30 * 60 * 1000L;
            case NORMAL:
                return 15 * 60 * 1000L;
            case SHORT:
                return 10 * 60 * 1000L;
            case SHORTEST:
                return 5 * 60 * 1000L;
        }
    }

}

package ms.airy.lib.core.control.util;


/**
 */
public final class CountDownUtil
{
    private CountDownUtil() {}


    public static CharSequence addCountDownTime(CharSequence phrase, long time, int countDownTimeDecimal)
    {
        CharSequence phraseWithoutTime = removeCountDownTime(phrase, countDownTimeDecimal);
        CharSequence phraseWithNewTime = phraseWithoutTime + " in " + formatCountDownTime(time, countDownTimeDecimal);
        return phraseWithNewTime;
    }
    private static CharSequence removeCountDownTime(CharSequence phrase, int countDownTimeDecimal)
    {
        // Heck...
        boolean phraseEndsWithTime = false;
        if(phrase.length() > 6) {   // temporary
            String s = phrase.toString();
            if(Character.isDigit(s.charAt(s.length() - 1))) {
                boolean valid = false;
                if(countDownTimeDecimal > 0) {
                    int iDot = s.lastIndexOf('.');
                    if(iDot == s.length() - countDownTimeDecimal - 1) {
                        valid = true;
                        s = s.substring(0, iDot);
                    } else {
                        valid = false;
                    }
                } else {
                    valid = true;
                }
                if(valid) {
                    if(Character.isDigit(s.charAt(s.length() - 1))
                          && Character.isDigit(s.charAt(s.length() - 2))
                            && s.charAt(s.length() -3) == ':') {
                        // TBD:
                        // etc ....
                        phraseEndsWithTime = true;
                    }
                }
            }
        }

        if(phraseEndsWithTime == false) {
            return phrase;
        }

        int idx = phrase.toString().lastIndexOf(" ");
        if(idx == -1) {
            return "";  // ???
        }
        if(idx >= 3) {
            idx -= 3;  // "xxx in mmm"
        }
        CharSequence PhraseWithoutTime = phrase.subSequence(0, idx);
        return PhraseWithoutTime;
    }


    public static CharSequence formatCountDownTime(long time, int countDownTimeDecimal)
    {
//        Date date = new Date(time);
//        // ???
//        String text = date.toGMTString();

        int milliSecs = (int) (time % 1000L);
        int centiSecs = milliSecs / 10;
        int deciSecs = milliSecs / 100;
        long rem1 = time / 1000L;
        int secs = (int) (rem1 % 60);
        long rem2 = rem1 / 60;
        int mins = (int) (rem2 % 60);
        long rem3 = rem2 / 60;
        int hours = (int) rem3;
//        int hours = (int) (rem3 % 24);
//        long rem4 = rem3 / 24;
//        int days = (int) rem4

        String text = "";
        switch(countDownTimeDecimal)
        {
            case 0:
                text = String.format("%d:%02d:%02d", hours, mins, secs);
                break;
            case 1:
            default:
                text = String.format("%d:%02d:%02d.%d", hours, mins, secs, deciSecs);
                break;
            case 2:
                text = String.format("%d:%02d:%02d.%02d", hours, mins, secs, centiSecs);
                break;
            case 3:
                text = String.format("%d:%02d:%02d.%03d", hours, mins, secs, milliSecs);
                break;
        }

        if(Log.V) Log.v("formatCountDownTime(): text = " + text + " for time = " + time);
        return text;
    }
}

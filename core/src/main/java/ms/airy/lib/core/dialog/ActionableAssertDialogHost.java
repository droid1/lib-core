package ms.airy.lib.core.dialog;

import android.app.DialogFragment;


public interface ActionableAssertDialogHost
{
    DialogFragment addActionable(ActionableAssertDialogListener actionable);
}

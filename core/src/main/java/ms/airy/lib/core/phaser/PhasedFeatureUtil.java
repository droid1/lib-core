package ms.airy.lib.core.phaser;

import ms.airy.lib.core.phaser.common.Phase;
import ms.airy.lib.core.phaser.phase.PhaseHelper;
import ms.airy.lib.core.phaser.common.Rank;
import ms.airy.lib.core.phaser.rank.RankHelper;
import ms.airy.lib.core.phaser.state.StateHelper;


// Not being used...
public final class PhasedFeatureUtil
{
    // static methods only.
    private PhasedFeatureUtil() {}


}

package ms.airy.lib.core.control;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.widget.TextView;

import ms.airy.lib.core.time.timer.TimerState;
import ms.airy.lib.core.time.timer.TimerRange;
import ms.airy.lib.core.time.timer.CountDownEventListener;
import ms.airy.lib.core.time.timer.FlexibleCountDownTimer;

import ms.airy.lib.core.control.util.CountDownUtil;
import ms.airy.lib.core.R;

import java.util.HashSet;
import java.util.Set;


/**
 * CountDown "text" timer.
 * It displays the counting down clock, or the time remaining, as text (as opposed to as a progress bar)
 */
public class CountDownTimerControl extends TextView implements FlexibleCountDownTimer.CountDownListener
{
    // timer state
    private int state;

    // Timer range, and the current value.
    private TimerRange timerRange = null;

    // Drives the count-down animation.
    private FlexibleCountDownTimer countDownTimer = null;

    // Number of digits below the second level in the count-down time.
    private int countDownTimeDecimal;
    
    // Colors
    private int mColorBackgroundNormal;    // Background of the control
    private int mColorBackgroundWarning;   // Background,  when val < threshold.
    private int mColorTextNormal;    // Count-down text.
    private int mColorTextWarning;   // Count-down text, when val < threshold.

    // ???
    private volatile boolean thresholdCrossHandled = false;

    
    public CountDownTimerControl(Context context)
    {
        super(context);
        initControl(context, null, 0);
    }
    public CountDownTimerControl(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        initControl(context, attrs, 0);
    }
    public CountDownTimerControl(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        initControl(context, attrs, defStyle);
    }

    private void initControl(Context context, AttributeSet attrs, int defStyle)
    {
        if(Log.D) Log.d("initControl(): attrs = " + attrs);

        setFocusable(false); // For display only. No user input.

        // temporary
        // state = TimerState.UNKNOWN;

        if(attrs != null) {
            TypedArray a1 = context.getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.TimerRange,
                    0, 0);
            try {
//                // hack
//                int imin = a1.getInt(R.styleable.TimerRange_timerRangeMin, 0);
//                int imax = a1.getInt(R.styleable.TimerRange_timerRangeMax, imin + 60);         // ???
//                int ithreshold = a1.getInt(R.styleable.TimerRange_timerRangeThreshold, imin);  // ???
//                long min = imin * 1000L;
//                long max = imax * 1000L;
//                long threshold = ithreshold * 1000L;
                int min = a1.getInt(R.styleable.TimerRange_timerRangeMin, 0);
                int max = a1.getInt(R.styleable.TimerRange_timerRangeMax, min + 60000);         // ???
                int threshold = a1.getInt(R.styleable.TimerRange_timerRangeThreshold, min);  // ???
                timerRange = new TimerRange(max, min, threshold);
                // ...
            } finally {
                a1.recycle();
            }
            TypedArray a2 = context.getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.BaseControl,
                    0, 0);
            try {
                int c1 = a2.getColor(R.styleable.BaseControl_backgroundColorNormal, Color.rgb(172, 172, 172));
                setColorBackgroundNormal(c1);
//                int c2 = a.getColor(R.styleable.BaseControl_backgroundColorWarning, Color.rgb(255, 255, 255));
                int defC2 = Color.rgb(255, 255, 255);
                if(c1 != Color.rgb(172, 172, 172)) {  // In case c1 has been set but not c2, then just use c1 for c2.
                    defC2 = c1;
                }
                int c2 = a2.getColor(R.styleable.BaseControl_backgroundColorWarning, defC2);
                setColorBackgroundWarning(c2);
                int c3 = a2.getColor(R.styleable.BaseControl_textColorNormal, Color.rgb(127, 127, 255));
                setColorTextNormal(c3);
//                int c4 = a.getColor(R.styleable.BaseControl_textColorWarning, Color.rgb(255, 127, 127));
                int defC4 = Color.rgb(255, 127, 127);
                if(c3 != Color.rgb(127, 127, 255)) {  // In case c3 has been set but not c4, then just use c3 for c4.
                    defC4 = c3;
                }
                int c4 = a2.getColor(R.styleable.BaseControl_textColorWarning, defC4);
                setColorTextWarning(c4);

            } finally {
                a2.recycle();
            }
            TypedArray a3 = context.getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.CountDownTimer,
                    0, 0);
            try {
                countDownTimeDecimal = a3.getInt(R.styleable.CountDownTimer_countDownTimerDecimal, 1);
            } finally {
                a3.recycle();
            }
        } else {
            // Use the default axisOrientationDependent, axis-orientation mapping, etc...
            // ...

            // ???? What's the best "default" value???
            timerRange = new TimerRange(10000L);

            countDownTimeDecimal = 1;

            // Default colors.
            setColorBackgroundNormal(Color.rgb(172, 172, 172));
            setColorBackgroundWarning(Color.rgb(255, 255, 255));
            setColorTextNormal(Color.rgb(127, 127, 255));
            setColorTextWarning(Color.rgb(255, 127, 127));
        }

        // ???
        state = TimerState.INITIALIZED;

        // ????
//        CharSequence text = formatCountDownTime(timerRange.getMax());
//        super.setText(text);
//
//        setTextColor(mColorTextNormal);
//        setBackgroundColor(mColorBackgroundNormal);
        setTextSize(24.0f);

        // ???
        initTimer(true);
    }


    protected void initTimer()
    {
        initTimer(false);
    }
    protected void initTimer(boolean force)
    {
        // TBD:
        // At this point,
        // timerRanger should not be null...
        // How to enforce that????

        // timerRange.reset();  // Make val == timerMax. Here? or Below?
        if(force || countDownTimer == null) {
            timerRange.reset();  // Make val == timerMax. Here? or Above?
            countDownTimer = new FlexibleCountDownTimer(timerRange, 100L);
            countDownTimer.addCountDownListener(this);
        }

        // ???
        if(countDownTimer == null) {
            setState(TimerState.INITIALIZED);
        } else {
            // Keep the current state???
            // ????
        }

        // ????
//        CharSequence text = formatCountDownTime(timerRange.getMax());
        CharSequence text = formatCountDownTime(timerRange.getVal());
        super.setText(text);

        // ???
        refreshTimer();
    }

    // ???
    private void refreshTimer()
    {
        // ???
        if(timerRange.isBelowThreshold()) {
            thresholdCrossHandled = true;
            setTextColor(mColorTextWarning);
            setBackgroundColor(mColorBackgroundWarning);
        } else {
            thresholdCrossHandled = false;
            setTextColor(mColorTextNormal);
            setBackgroundColor(mColorBackgroundNormal);
        }
    }


    public void update()
    {
        update(false);
    }
    public void update(boolean force)
    {
        update(force, false);
    }
    public void update(boolean force, boolean layout)
    {
        if(force || layout || isDirty() ) {   // layout==true implies force==true.
            if(layout) {
                requestLayout();
            }
            invalidate();
            // setDirty(false);  // ???
        } else {
            // ignore
        }
    }


    public int getState()
    {
        return state;
    }
    public void setState(int state)
    {
        this.state = state;
    }

    public TimerRange getTimerRange()
    {
        return timerRange;
    }
    public void setTimerRange(TimerRange timerRange)
    {
        this.timerRange = timerRange;
        // ???
        initTimer(true);
        // update(true);    // ???
        // ???
    }
    public void resetTimerRange(long max)
    {
        if(this.timerRange != null) {
            this.timerRange.reset(max);
        } else {
            // ????
            this.timerRange = new TimerRange(max);
        }
        // ???
        initTimer(true);
        // update(true);    // ???
        // ???
    }
    public void resetTimerRange(long max, long min)
    {
        if(this.timerRange != null) {
            this.timerRange.reset(max, min);
        } else {
            // ????
            this.timerRange = new TimerRange(max, min);
        }
        // ???
        initTimer(true);
        // update(true);    // ???
        // ???
    }
    public void resetTimerRange(long max, long min, long threshold)
    {
        if(this.timerRange != null) {
            this.timerRange.reset(max, min, threshold);
        } else {
            // ????
            this.timerRange = new TimerRange(max, min, threshold);
        }
        // ???
        initTimer(true);
        // update(true);    // ???
    }

//    public FlexibleCountDownTimer getCountDownTimer()
//    {
//        return countDownTimer;
//    }
//    public void setCountDownTimer(FlexibleCountDownTimer countDownTimer)
//    {
//        this.countDownTimer = countDownTimer;
//    }


    public int getCountDownTimeDecimal()
    {
        return countDownTimeDecimal;
    }
    public void setCountDownTimeDecimal(int countDownTimeDecimal)
    {
        this.countDownTimeDecimal = countDownTimeDecimal;
    }


    public int getColorBackgroundNormal()
    {
        return mColorBackgroundNormal;
    }
    public void setColorBackgroundNormal(int colorBackgroundNormal)
    {
        this.mColorBackgroundNormal = colorBackgroundNormal;
    }

    public int getColorBackgroundWarning()
    {
        return mColorBackgroundWarning;
    }
    public void setColorBackgroundWarning(int colorBackgroundWarning)
    {
        this.mColorBackgroundWarning = colorBackgroundWarning;
    }

    public int getColorTextNormal()
    {
        return mColorTextNormal;
    }
    public void setColorTextNormal(int colorTextNormal)
    {
        this.mColorTextNormal = colorTextNormal;
    }

    public int getColorTextWarning()
    {
        return mColorTextWarning;
    }
    public void setColorTextWarning(int colorTextWarning)
    {
        this.mColorTextWarning = colorTextWarning;
    }




    // TBD:
    // Timer life cycle methods....
    // ...



    // Name? init or reset?
    // this control can be re-used for multiple count-downs...
    // so, reset seems more appropriate...
    public void reset()
    {
        reset(TimerState.INITIALIZED);
    }
    public void reset(int state)
    {
        // Timer can be reset/initialized regardless of the current state.
        // Just initialize the timer with the current/default values.
        initTimer(true);
        setState(state);
        // update();   // ???
    }
    public void reset(long max)
    {
        resetTimerRange(max);
        reset();
    }
    public void reset(long max, long min)
    {
        resetTimerRange(max, min);
        reset();
    }
    public void reset(long max, long min, long threshold)
    {
        resetTimerRange(max, min, threshold);
        reset();
    }

    public void restart()
    {
        reset();
        start();
    }
    public void restart(long max)
    {
        reset(max);
        start();
    }
    public void restart(long max, long min)
    {
        reset(max, min);
        start();
    }
    public void restart(long max, long min, long threshold)
    {
        reset(max, min, threshold);
        start();
    }


    public void start()
    {
        if(getState() != TimerState.INITIALIZED) {
            // throw new IllegalStateException("Timer cannot be started when it has not been initialized.");
            Log.w("Timer cannot be started when it has not been initialized. Current state = " + getState());
            return;
        }

        if(countDownTimer != null) {
            countDownTimer.start();
        } else {
            // ???
            Log.w("countDownTimer is null. Cannot startCycle().");
        }

        // TBD
        setState(TimerState.RUNNING);
        // update();   // ???
    }
    public void pause()
    {
        pause(-1L);
    }
    public void pause(long extent)
    {
        if(extent >= 0L) {
            // Hack. We are using this as to re-create a snapshot of the timer...
            // bypass the state check...
        } else {
            if(getState() != TimerState.RUNNING) {
                // throw new IllegalStateException("Timer cannot be paused when it's not running.");
                Log.w("Timer cannot be paused when it's not running. Current state = " + getState());
                return;
            }
        }

        if(countDownTimer != null) {
            countDownTimer.pause(extent);
            // reset this timerControl's val???
            //   (note: extent could be -1 at this point.)
            // the caller should take care of this before calling pause(extent)...
        } else {
            // ???
            Log.w("countDownTimer is null. Cannot pause().");
        }

        // TBD:
        setState(TimerState.PAUSED);
        // update();   // ???
    }
    public void resume()
    {
        resume(-1L);
    }
    public void resume(long extent)
    {
        if(extent >= 0L) {
            // Hack. We are using this to re-create a snapshot of the timer...
            // --> bypass the state check...
        } else {
            if (getState() != TimerState.PAUSED) {
                // throw new IllegalStateException("Timer cannot be resumed when it has not been paused.");
                Log.w("Timer cannot be resumed when it has not been paused. Current state = " + getState());
                return;
            }
        }

        if(countDownTimer != null) {
            countDownTimer.resume(extent);
            // reset this timerControl's val???
            //   (note: extent could be -1 at this point.)
            // the caller should take care of this before calling resume(extent)...
        } else {
            // ???
            Log.w("countDownTimer is null. Cannot resume().");
        }

        // TBD:
        setState(TimerState.RUNNING);
        // update();   // ???
    }

    public void stop()
    {
        if(getState() != TimerState.RUNNING && getState() != TimerState.PAUSED) {
            // throw new IllegalStateException("Timer cannot be stopped when it's not running.");
            Log.w("Timer cannot be stopped when it's not running or paused. Current state = " + getState());
            return;
        }

        if(countDownTimer != null) {
            countDownTimer.stop();
        } else {
            // ???
            Log.w("countDownTimer is null. Cannot stopCycle().");
        }

        // TBD:
        setState(TimerState.STOPPED);
        // update();   // ???
    }

    public void finish()
    {
//        if(getRepeats() != RUNNING && getRepeats() != PAUSED && getRepeats() != STOPPED) {
//            // throw new IllegalStateException("Timer cannot be finished when it's not running/paused/stopped.");
//            Log.w("Timer cannot be finished when it's not running/paused/stopped. Current state = " + getRepeats());
//            return;
//        }

        if(countDownTimer != null) {
            countDownTimer.finish();
        } else {
            // ???
            Log.w("countDownTimer is null. Cannot finish().");
        }

        // TBD:
        setState(TimerState.FINISHED);
        // update();   // ???
    }



    // CountDownListener interface.

    @Override
    public void onTick(long millisUntilFinished)
    {
        if (Log.I) Log.i("onTick(): millisUntilFinished = " + millisUntilFinished);

        // ???
        long time = timerRange.getMin() + millisUntilFinished;
        CharSequence text = formatCountDownTime(time);
        super.setText(text);

        // Call this only once when the value cross from above threshold to below threshold??
        // ???
        timerRange.setVal(time);
        if(thresholdCrossHandled == false) {
            if (timerRange.isBelowThreshold()) {
            // if(time < timerRange.getThreshold()) {
                setTextColor(mColorTextWarning);
                setBackgroundColor(mColorBackgroundWarning);

                // Broadcast the event.
                for (CountDownEventListener listener : listeners) {
                    listener.onThresholdCross(this);
                }

                thresholdCrossHandled = true;
            }
        }

        // ???
        update(true);
    }

    @Override
    public void onFinish()
    {
        if (Log.I) Log.i("onFinish(): now = " + System.currentTimeMillis());

        // ???
        CharSequence text = formatCountDownTime(timerRange.getMin());
        super.setText(text);

        // ???
        setState(TimerState.FINISHED);
        update(true);

        // Broadcast the event.
        for(CountDownEventListener listener : listeners) {
            listener.onFinish(this);
        }
    }


    // temporary
    private CharSequence formatCountDownTime(long time)
    {
        return CountDownUtil.formatCountDownTime(time, countDownTimeDecimal);
    }



    // TBD:
    // How to save/restore countDownTimer ????
    // ....

    @Override
    public Parcelable onSaveInstanceState()
    {
        Log.d("PlayerTimerControl.onSaveInstanceState() Called.");

        Parcelable superState = super.onSaveInstanceState();
        SavedState savedState = new SavedState(superState);
        int currentState = getState();
        // Log.w(">>>>>>> onSaveInstanceState() currentState = " + currentState);
        savedState.setState(currentState);
        savedState.setTimerText(super.getText().toString());
        savedState.setTimerMax(getTimerRange().getMax());
        savedState.setTimerMin(getTimerRange().getMin());
        savedState.setTimerThreshold(getTimerRange().getThreshold());
        savedState.setTimerValue(getTimerRange().getVal());
        return savedState;
    }

    @Override
    public void onRestoreInstanceState(Parcelable state)
    {
        Log.d("PlayerTimerControl.onRestoreInstanceState() Called.");

        SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(savedState.getSuperState());
        int controlState = savedState.getState();
        String timerText = savedState.getTimerText();
        long timerMax = savedState.getTimerMax();
        long timerMin = savedState.getTimerMin();
        long timerThreshold = savedState.getTimerThreshold();
        long timerValue = savedState.getTimerValue();
        // ???
        resetTimerRange(timerMax, timerMin, timerThreshold);
        // ???
        initTimer(true);

        // Log.w(">>>>>>> onSaveInstanceState() controlState = " + controlState);
        setState(controlState);
        super.setText(timerText);
        getTimerRange().setVal(timerValue);
        long ext = timerValue - getTimerRange().getMin();
        // ???
        // TBD: Keep the state???
        if(controlState == TimerState.RUNNING) {
            pause(ext);   // this calls setRepeats(PAUSED)...
            resume();     // this calls setRepeats(RUNNING) ???
        } else if(controlState == TimerState.PAUSED) {
            pause(ext);   // Hack. This is needed because we do not properly save/restore countDownTimer...
        } else {
            // pause(ext);   // ???
            // Or, just keep the current state???
        }
        // ???
        refreshTimer();
    }

    protected static class SavedState extends BaseSavedState implements Parcelable
    {
        private int state;
        private String timerText;
        private long timerMax;
        private long timerMin;
        private long timerThreshold;
        private long timerValue;
        // etc...

        public SavedState(Parcelable superState)
        {
            super(superState);
        }
        public SavedState(Parcel source)
        {
            super(source);
            state = source.readInt();
            timerText = source.readString();
            timerMax = source.readLong();
            timerMin = source.readLong();
            timerThreshold = source.readLong();
            timerValue = source.readLong();
        }


        public int getState()
        {
            return state;
        }
        public void setState(int state)
        {
            this.state = state;
        }

        public String getTimerText()
        {
            return timerText;
        }
        public void setTimerText(String timerText)
        {
            this.timerText = timerText;
        }

        public long getTimerMax()
        {
            return timerMax;
        }
        public void setTimerMax(long timerMax)
        {
            this.timerMax = timerMax;
        }

        public long getTimerMin()
        {
            return timerMin;
        }
        public void setTimerMin(long timerMin)
        {
            this.timerMin = timerMin;
        }

        public long getTimerThreshold()
        {
            return timerThreshold;
        }
        public void setTimerThreshold(long timerThreshold)
        {
            this.timerThreshold = timerThreshold;
        }

        public long getTimerValue()
        {
            return timerValue;
        }
        public void setTimerValue(long timerValue)
        {
            this.timerValue = timerValue;
        }


        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            @Override
            public SavedState createFromParcel(Parcel source)
            {
                return new SavedState(source);
            }
            @Override
            public SavedState[] newArray(int size)
            {
                return new SavedState[size];
            }
        };

        @Override
        public void writeToParcel(Parcel dest, int flags)
        {
            super.writeToParcel(dest, flags);
            dest.writeInt(state);
            dest.writeString(timerText);
            dest.writeLong(timerMax);
            dest.writeLong(timerMin);
            dest.writeLong(timerThreshold);
            dest.writeLong(timerValue);
        }


        // For debugging...
        @Override
        public String toString()
        {
            return "SavedState{" +
                    "state=" + state +
                    "timerText=" + timerText +
                    "timerMax=" + timerMax +
                    "timerMin=" + timerMin +
                    "timerThreshold=" + timerThreshold +
                    "timerValue=" + timerValue +
                    '}';
        }
    }


    private final Set<CountDownEventListener> listeners = new HashSet<CountDownEventListener>();

    public void addCountDownEventListener(CountDownEventListener listener)
    {
        if(listener != null) {
            listeners.add(listener);
        }
    }


}

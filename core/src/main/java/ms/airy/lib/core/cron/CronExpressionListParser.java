package ms.airy.lib.core.cron;

import java.util.ArrayList;
import java.util.List;


/**
 * To support multiple cron expressions. 
 * E.g., use cases:
 * "every 10AM and 5PM weekdays, and 2PM Saturdays and Sundays"
 * etc.
 */
public class CronExpressionListParser implements CronTimeParsable
{
    // Input cron expression list
    //private final List<String> expressions;

    // List of CronExpressionParsers.
    private List<CronExpressionParser> parsers = null;

    // Note: starting from true...
    private boolean allParsed = true;

    // Ctors.
    public CronExpressionListParser(List<String> expressions) 
    {
        this(expressions, null);
    }
    public CronExpressionListParser(List<String> expressions, String timezoneID) 
    {
        // TBD:
        // validate expressions???
        // != null && !isEmpty() ....
        // ...
        //this.expressions = expressions;
        parsers = new ArrayList<CronExpressionParser>();
        if(expressions != null && !expressions.isEmpty()) {
            for(String e : expressions) {
                CronExpressionParser parser = new CronExpressionParser(e, timezoneID);
                if(parser != null && parser.isParsed()) {
                    parsers.add(parser);
                } else {
                    Log.w("Failed to parse the cron expression: " + parser.getExpression());
                    allParsed = false;
                    // bail out ???
                }
            }
        }
    }

//    public List<String> getExpressions()
//    {
//        return expressions;
//    }
//    public List<CronExpressionParser> getParsers()
//    {
//        return parsers;
//    }

    @Override
    public boolean isParsed()
    {
        return allParsed;
    }


    // TBD:
    // If isParsed() == false;
    // Return error for

    @Override
    public Long getNextCronTime()
    {
        return getNextCronTime(System.currentTimeMillis());
    }
    @Override
    public Long getNextCronTime(long now)
    {
        if(Log.I) Log.i("getNextCronTime() BEGIN: now = " + now);

        // TBD: Just use Math.min(a, b) ?
        Long nextCronTime = null;
        for(CronExpressionParser parser : parsers) {
            Long t = parser.getNextCronTime();
            if(t != null && t > 0L) {
                if(nextCronTime == null || nextCronTime == 0L) {
                    nextCronTime = t;
                } else {
                    if(t < nextCronTime) {
                        nextCronTime = t;
                    }
                }
            }
        }

        if(Log.I) Log.i("getNextCronTime() END: nextCronTime = " + nextCronTime);
        return nextCronTime;
    }

    // temporary
    public Long getNextCronTimeShifted(long delta)
    {
        return getNextCronTimeShifted(System.currentTimeMillis(), delta);
    }
    public Long getNextCronTimeShifted(long now, long delta)
    {
        if(Log.I) Log.i("getNextCronTime() BEGIN: now = " + now);

        // TBD: Just use Math.min(a, b) ?
        Long nextCronTime = null;
        for(CronExpressionParser parser : parsers) {
            Long t = parser.getNextCronTimeShifted(delta);
            if(t != null && t > 0L) {
                if(nextCronTime == null || nextCronTime == 0L) {
                    nextCronTime = t;
                } else {
                    if(t < nextCronTime) {
                        nextCronTime = t;
                    }
                }
            }
        }

        if(Log.I) Log.i("getNextCronTimeShifted() END: nextCronTime = " + nextCronTime);
        return nextCronTime;
    }



    @Override
    public Long getPreviousCronTime()
    {
        return getPreviousCronTime(System.currentTimeMillis());
    }
    @Override
    public Long getPreviousCronTime(long now)
    {
        if(Log.I) Log.i("getPreviousCronTime() BEGIN: now = " + now);

        // TBD: Just use Math.max(a, b) ?
        Long previousCronTime = null;
        for(CronExpressionParser parser : parsers) {
            Long t = parser.getPreviousCronTime();
            if(t != null && t > 0L) {
                if(previousCronTime == null || previousCronTime == 0L) {
                    previousCronTime = t;
                } else {
                    if(t > previousCronTime) {
                        previousCronTime = t;
                    }
                }
            }
        }

        if(Log.I) Log.i("getPreviousCronTime() END: previousCronTime = " + previousCronTime);
        return previousCronTime;
    }
    

    @Override
    public String toString() 
    {
        StringBuilder sb = new StringBuilder();
        for(CronExpressionParser parser : parsers) {
            sb.append(parser.toString()).append(" ");
        }
        return sb.toString();
    }
    
    
}

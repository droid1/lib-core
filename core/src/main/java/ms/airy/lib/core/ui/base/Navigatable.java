package ms.airy.lib.core.ui.base;

import android.content.Intent;

// This is sort of "marker interface".
// Classes implementing this interface not only have to implement all methods in this interface,
// but, they also have to implement the underlying navigation mechanism (e.g., currently using extra params)...
public interface Navigatable
{
    Intent getUpIntent();
    // ....
}

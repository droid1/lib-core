package ms.airy.lib.core.async;

import android.content.Context;

import java.util.HashSet;
import java.util.Set;

import ms.airy.lib.core.async.listener.AsyncRunnerListener;


// TBD:
// Experimental...
//...
// Not fully implemented...
// ....

public abstract class AsyncRunner   // implements Runnable
{
    // Application context
    private final Context context;
    // To distinguish this instance of AsyncRunner.
    private final String tag;

    // Callbacks.
    private final Set<AsyncRunnerListener> listeners = new HashSet<AsyncRunnerListener>();

    public AsyncRunner(Context context, String tag, AsyncRunnerListener listener)
    {
        this.context = context.getApplicationContext();
        this.tag = tag;
        listeners.add(listener);
    }

    // The main method.
    // TBD: Use generic types????
    protected abstract Object doInBackground(Object... params);


    // To be called by a broadcast receiver....
    public void postResult(String tag, Object result)
    {
        for(AsyncRunnerListener listener : listeners) {
            listener.onResultSuccess(tag, result);
        }
    }
    public void postError(String tag, String message)
    {
        for(AsyncRunnerListener listener : listeners) {
            listener.onResultFailure(tag, message);
        }
    }


//    @Override
//    public final void run()
//    {
//    }


    public final void execute(final Object... params)
    {
        Runnable runnable = new Runnable() {
            @Override
            public void run()
            {
                // TBD:
                // Is this polymorphic???
                Object result = AsyncRunner.this.doInBackground(params);

                // TBD:
                // Post the result to broadcast receiver...
                // which in turn calls postResult().
                // ...

                // ???
                // AsyncRunner.this.context.sendOrderedBroadcast();


            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }


}

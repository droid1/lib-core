package ms.airy.lib.core.invite.client.impl;


/**
 */
public abstract class AbstractBuddyInviteClient 
{
    // TBD:
    private String inviteWebServiceEndpoint;

    public AbstractBuddyInviteClient()
    {
        this(null);  // ????
    }
    protected AbstractBuddyInviteClient(String inviteWebServiceEndpoint)
    {
        this.inviteWebServiceEndpoint = inviteWebServiceEndpoint;
    }

    public String getInviteWebServiceEndpoint()
    {
        return inviteWebServiceEndpoint;
    }
    protected void setInviteWebServiceEndpoint(String inviteWebServiceEndpoint)
    {
        this.inviteWebServiceEndpoint = inviteWebServiceEndpoint;
        // TBD:
        // reset the miniclient...
        // ...
    }


}

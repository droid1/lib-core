package ms.airy.lib.core.time.handler;

import android.os.Looper;

import ms.airy.lib.core.time.common.PollingSchedule;

import java.util.ArrayList;
import java.util.List;


/**
 * Polling using constant intervals.
 */
public class ConstantPollingHandler extends PollingHandler
{
    // Polling interval.
    // In milli seconds.
    private long interval = 10000L;

    public ConstantPollingHandler()
    {
        init();
    }
    // TBD: do we need callback?
    public ConstantPollingHandler(Callback callback)
    {
        super(callback);
        init();
    }
    public ConstantPollingHandler(Looper looper)
    {
        super(looper);
        init();
    }
    // TBD: do we need callback?
    public ConstantPollingHandler(Looper looper, Callback callback)
    {
        super(looper, callback);
        init();
    }
    private void init()
    {
        setPollingSchedule(PollingSchedule.CONSTANT);
    }


    public final long getPollingInterval()
    {
        return interval;
    }
    public final void setPollingInterval(long interval)
    {
        if(Log.D) Log.d("setPollingInterval() called with interval = " + interval);
        this.interval = interval;
    }


    // Note the name of the methods.
    // The "fetch" method is not a "getter", and it is not idempotent.
    // Calling these methods changes the internal state of the handler.
    @Override
    protected long[] fetchNextDelays(int size)
    {
        long[] delays = computeNextDelays(size, this.interval);
        return delays;
    }
    @Override
    protected long[] fetchNextDelays(long duration)
    {
        long[] delays = computeNextDelays(duration, this.interval);
        return delays;
    }
    @Override
    protected long[] computeNextDelays(int size)
    {
        return computeNextDelays(size, this.interval);
    }
    @Override
    protected long[] computeNextDelays(long duration)
    {
        return computeNextDelays(duration, this.interval);
    }
    private long[] computeNextDelays(int size, long interval)
    {
        long[] delays = new long[size];
        for (int i = 0; i < size; i++) {
            delays[i] = interval;
        }
        return delays;
    }
//    private long[] computeNextDelays(long duration, long interval)
//    {
//        List<Long> list = new ArrayList<Long>();
//        long totalDelay = 0;
//        int cnt = 0;
//        while(totalDelay < duration && cnt < 10000) {   // arbitrary cutoff to prevent infinite loop.
//            long delay = interval;
//            list.add(delay);
//            totalDelay += delay;
//            cnt++;
//        }
//        // Long[] arr = list.toArray(new Long[]{});
//        long[] delays = new long[list.size()];
//        for (int i = 0; i < list.size(); i++) {
//            delays[i] = list.get(i);
//        }
//        return delays;
//    }
    private long[] computeNextDelays(long duration, long interval)
    {
        int size = (int) ((duration + (interval - 1L) )/ interval);
        return computeNextDelays(size, interval);
    }


}

package ms.airy.lib.core.help.fragment;

import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import ms.airy.lib.core.help.manager.HelpListHelper;
import ms.airy.lib.core.help.manager.HelpManager;


/**
 * Displays the help section list in /assets/help folder.
 * It uses ArrayAdapter.
 */
public class HelpSectionListFragment extends ListFragment
{
    // int.
    private static final String ARG_LAYOUT_RESID = "helplist_layout_resid";
    private static final String ARG_TEXTVIEW_ID = "helplist_textview_id";
    // ...


    // TBD:
    public static HelpSectionListFragment newInstance()
    {
        return newInstance(0);
    }
    public static HelpSectionListFragment newInstance(int layoutResId)
    {
        // The layout should be a single textView.
        return newInstance(layoutResId, 0);
    }
    public static HelpSectionListFragment newInstance(int layoutResId, int textViewId)
    {
        HelpSectionListFragment fragment = new HelpSectionListFragment();
        Bundle args = new Bundle();
        if(layoutResId > 0) {
            args.putInt(ARG_LAYOUT_RESID, layoutResId);
            // Note that you cannot set textView id without setting layout id.
            //   (based on ArrayAdapter ctors....)
            if (textViewId > 0) {
                args.putInt(ARG_TEXTVIEW_ID, textViewId);
            } else {
                // In this case,
                // the layout should be a single textView.
            }
        }
        fragment.setArguments(args);
        return fragment;
    }
    protected static HelpSectionListFragment addIntArgument(HelpSectionListFragment fragment, String key, int val)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        args.putInt(key, val);
        return fragment;
    }
    protected static HelpSectionListFragment addLongArgument(HelpSectionListFragment fragment, String key, long val)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        args.putLong(key, val);
        return fragment;
    }
    protected static HelpSectionListFragment addStringArgument(HelpSectionListFragment fragment, String key, String val)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        args.putString(key, val);
        return fragment;
    }
    public HelpSectionListFragment()
    {
    }


    // TBD
    private int layoutResId;
    private int textViewId;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // tbd
        setRetainInstance(true);
        // ...

        Bundle args = getArguments();
        if(args != null) {
            layoutResId = args.getInt(ARG_LAYOUT_RESID);
            textViewId = args.getInt(ARG_TEXTVIEW_ID);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {

        // TBD:
        // check if layoutResId==0 ....
        // ....

        // temporary
//        String[] sections = new String[] {
//                "China",
//                "Korea",
//                "Japan"
//        };

        String[] sections = HelpListHelper.getInstance(inflater.getContext()).getSectionTitleList();

        ArrayAdapter<String> adapter = null;
        if(layoutResId == 0) {
            adapter = new ArrayAdapter<String>(inflater.getContext(), android.R.layout.simple_list_item_1, sections);
        } else {
            if(textViewId == 0) {
                // The layout should be a single textView.
                adapter = new ArrayAdapter<String>(inflater.getContext(), layoutResId, sections);
            } else {
                adapter = new ArrayAdapter<String>(inflater.getContext(), layoutResId, textViewId, sections);
            }
        }
        setListAdapter(adapter);

        return super.onCreateView(inflater, container, savedInstanceState);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);


    }



    @Override
    public void onListItemClick(ListView l, View v, int position, long id)
    {
        // super.onListItemClick(l, v, position, id);

        if(Log.I) Log.i("onListItemClick(). position = " + position + "; id = " + id);

//        TextView view = (TextView) getListView().getItemAtPosition(position);

        // Heck
        // HelpListHelper methods need to be re-written!!!!

        long sectionId = HelpListHelper.getInstance(getActivity()).getSectionIdFromPosition(position);

        HelpManager.getInstance(getActivity()).showHelp(getActivity(), sectionId);
    }


}

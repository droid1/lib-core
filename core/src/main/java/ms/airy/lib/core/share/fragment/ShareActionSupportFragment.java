package ms.airy.lib.core.share.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ShareActionProvider;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import ms.airy.lib.core.R;
import ms.airy.lib.core.share.TextSharerable;
import ms.airy.lib.core.share.util.TextShareUtil;
import ms.airy.lib.core.toast.ToastHelper;


/**
 * Example usage:
 * In Activity.onCreate()
 *   fragmentTransaction.add(ShareActionSupportFragment.newInstance(shareSubject), fragmentTag);
 */
public class ShareActionSupportFragment extends Fragment
{
    // temporary
    private static final String DEFAULT_SHARE_MENU_LABEL = "Share";

//    public static final String ARG_TEXT_SHAREABLE = "share_text_shareable";
//    public static final String ARG_SHARE_SUBJECT = "share_share_subject";
//    public static final String ARG_SHARE_TEXT = "share_share_text";
//    public static final String ARG_SHARE_STREAM = "share_share_stream";
    public static final String ARG_MENU_LABEL = "share_menu_label";
    public static final String ARG_MENU_LABEL_RESID = "share_menu_label_resid";
    public static final String ARG_SHOW_CHOOSER = "share_show_chooser";
    // ...

    // Note that we cannot specify both menuLabel and menuLabelResId
    //     during construction.
    public static ShareActionSupportFragment newInstance()
    {
        return newInstance(null);
    }
    public static ShareActionSupportFragment newInstance(String menuLabel)
    {
        return newInstance(menuLabel, false);
    }
    public static ShareActionSupportFragment newInstance(int menuLabelResId)
    {
        return newInstance(menuLabelResId, false);
    }
    public static ShareActionSupportFragment newInstance(String menuLabel, boolean showChooser)
    {
        ShareActionSupportFragment fragment = new ShareActionSupportFragment();
        Bundle args = new Bundle();
        if(menuLabel != null) {
            args.putString(ARG_MENU_LABEL, menuLabel);
        }
        args.putBoolean(ARG_SHOW_CHOOSER, showChooser);
        fragment.setArguments(args);
        return fragment;
    }
    public static ShareActionSupportFragment newInstance(int menuLabelResId, boolean showChooser)
    {
        ShareActionSupportFragment fragment = new ShareActionSupportFragment();
        Bundle args = new Bundle();
        if(menuLabelResId > 0) {    // ???
            args.putInt(ARG_MENU_LABEL_RESID, menuLabelResId);
        }
        args.putBoolean(ARG_SHOW_CHOOSER, showChooser);
        fragment.setArguments(args);
        return fragment;
    }

    // Not used...
    protected static ShareActionSupportFragment addIntArgument(ShareActionSupportFragment fragment, String key, int val)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        args.putInt(key, val);
        return fragment;
    }
    protected static ShareActionSupportFragment addLongArgument(ShareActionSupportFragment fragment, String key, long val)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        args.putLong(key, val);
        return fragment;
    }
    protected static ShareActionSupportFragment addStringArgument(ShareActionSupportFragment fragment, String key, String val)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        args.putString(key, val);
        return fragment;
    }


    private String menuLabel = null;
    private int menuLabelResId = 0;
    private boolean showChooser = false;

    private ShareActionProvider mShareActionProvider = null;

    // Hack!!!
    // Master flag.
    // Start with true?????
    //  --> This does not make sense..
//    private boolean showShareMenu = true;
    // ...

    public ShareActionSupportFragment()
    {
    }


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
        // setRetainInstance(true);   // ???
        // ...

        Bundle args = getArguments();
        if(args != null) {
            // if both menuLabelResId and menuLabel are specified,
            //    menuLabelResId takes precedence...
            menuLabelResId = args.getInt(ARG_MENU_LABEL_RESID);
            if(menuLabelResId == 0) {
                menuLabel = args.getString(ARG_MENU_LABEL);
                if(menuLabel == null) {
                    menuLabel = DEFAULT_SHARE_MENU_LABEL;
                }
            }
            if(args.containsKey(ARG_SHOW_CHOOSER)) {
                showChooser = args.getBoolean(ARG_SHOW_CHOOSER);
            }
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        return null;
    }

    @Override
    public void onResume()
    {
        super.onResume();

        // ????
        // Where do you call this???
        //    (note that prepareShare() (which calls setShareIntentToActionBar())
        //        actually triggers the share action, e.g., opens the actin chooser, etc.)
        // prepareShare();
        // ????
    }

    // temporary
//    private static final int MENU_ITEM_SHARE = 772441;

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        // ???
//        if(showShareMenu) {
            // TBD:
            MenuItem menu1 = null;

            // (1) Dynamic creation of menu does not seem to work....
//            if (menuLabelResId > 0) {
//                menu1 = menu.add(Menu.NONE, MENU_ITEM_SHARE, Menu.NONE, menuLabelResId);
//            } else {
//                menu1 = menu.add(Menu.NONE, MENU_ITEM_SHARE, Menu.NONE, menuLabel);
//            }
//            if (menu1 != null) {
//                menu1.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
//                // ???
//                if (mShareActionProvider == null) {
//                    mShareActionProvider = new ShareActionProvider(getActivity());
//                }
//                if (mShareActionProvider != null) {
//                    menu1.setActionProvider(mShareActionProvider);
//                }
//            }

            // (2) We need to use the menu resource... ????
            // The following uses the example code from
            // http://developer.android.com/training/sharing/shareaction.html
            inflater.inflate(R.menu.actionbar_share, menu);
            menu1 = menu.findItem(R.id.menu_item_action_share);
            if (menu1 != null) {
                if (menuLabelResId > 0) {
                    menu1.setTitle(menuLabelResId);
                } else {
                    menu1.setTitle(menuLabel);
                }
                menu1.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
                mShareActionProvider = (ShareActionProvider) menu1.getActionProvider();
            }
            // ?????
            // Do we need to call prepareShare() ????
            // If we show the menu item as menu, we can just this in onOptionsItemSelected()
            // however, if the item is shown as action button,
            // we need to call this somewhere....
            if (mShareActionProvider != null) {
                prepareShare();
            }
            // ????
//        } else {
//            Log.i("showShareMenu is false.");
//        }

        super.onCreateOptionsMenu(menu, inflater);
    }

    // TBD:

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // ???
//        switch(item.getItemId()) {
//            case MENU_ITEM_SHARE:
//                startShare();
//                return true;
//        }
        // ???
        // Is calling prepareShare() enough???
        // Do we need this????
        if(item.getItemId() == R.id.menu_item_action_share) {
            startShare();
            return true;
        }
        // ???
        return super.onOptionsItemSelected(item);
    }

    // TBD:
    // Note that because of the way ShareActioProvider and this Fragment is implemented,
    // we may end up using a stale data when the share action is triggered.
    // In theory, the client needs to invalidate the share intent every time the data changes.
    // but, I'm not sure if that's possible.
    public void invalidateShareMenu()
    {
        // ????
        prepareShare();
        getActivity().invalidateOptionsMenu();
    }

    private void startShare()
    {
        // ???
        prepareShare();
        // ???
    }
    private void prepareShare()
    {
        Intent shareIntent = buildShareIntent();
        boolean shareIntentSet = false;
        if(shareIntent != null) {
            shareIntentSet = setShareIntentToActionProvider(shareIntent);
        } else {
            Log.i("Failed to start share because shareIntent is null!");
        }
//        if(shareIntentSet == false) {
//            ToastHelper.getInstance(getActivity()).showToast("Sharing not enabled.");
//        }
    }
    private boolean setShareIntentToActionProvider(Intent shareIntent)
    {
        if (mShareActionProvider != null) {
            mShareActionProvider.setShareIntent(shareIntent);
            return true;
        } else {
            Log.i("shareIntent cannot be set because mShareActionProvider is null!");
            return false;
        }
    }

    // TBD:
    // Localize the text strings????
    private Intent buildShareIntent()
    {
        if(Log.I) Log.i("buildShareIntent().");

        TextSharerable textSharerable = null;
        if(getActivity() instanceof TextSharerable) {
            textSharerable = (TextSharerable) getActivity();
        } else {
            Log.w("The parent activity is not instanceof TextSharerable!!!");
//            // ???? What to do???
//            ToastHelper.getInstance(getActivity()).showToast("Share is not enabled.");
            return null;
        }

        String shareSubject = textSharerable.getShareSubject();
        String shareText = textSharerable.getShareText();

        Intent sendIntent = TextShareUtil.buildShareIntent(shareText, shareSubject);

//        Intent sendIntent = null;
//        if(shareText == null || shareText.isEmpty()) {
////            // ???? What to do???
////            //   --> Remove/hide the share menu item???
////            // But how???
////            ToastHelper.getInstance(getActivity()).showToast("Nothing to share.");
//        } else {
//            sendIntent = new Intent(Intent.ACTION_SEND);
//            sendIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);  // ????
//            sendIntent.putExtra(Intent.EXTRA_TEXT, shareText);
//            if(shareSubject != null && !shareSubject.isEmpty()) {
//                sendIntent.putExtra(Intent.EXTRA_SUBJECT, shareSubject);
//            }
//            sendIntent.setType("text/plain");   // ????
//        }

        return sendIntent;
    }

    private void buildAndStartShareIntent()
    {
        if(Log.I) Log.i("buildAndStartShareIntent().");

        Intent sendIntent = buildShareIntent();
        if(sendIntent != null) {
            if(showChooser) {
                // TBD: Localize the text.
                getActivity().startActivity(Intent.createChooser(sendIntent, "Share via"));
            } else {
                getActivity().startActivity(sendIntent);
            }
        }
    }

}

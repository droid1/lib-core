package ms.airy.lib.core.status.manager;

import android.content.Context;

import java.net.URL;


public final class StatusManager
{
    private static StatusManager INSTANCE = null;
    public static StatusManager getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new StatusManager(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;

    // Remote feedback service URL
    private URL feedbackServiceURL = null;


    private StatusManager(Context context)
    {
        this.context = context;
    }



}

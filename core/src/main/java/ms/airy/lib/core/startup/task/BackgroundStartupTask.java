package ms.airy.lib.core.startup.task;


// For background tasks.
public interface BackgroundStartupTask extends StartupTask
{
    // if bg task, then create asyncTask and run this in the asynctask.
    void run();

}

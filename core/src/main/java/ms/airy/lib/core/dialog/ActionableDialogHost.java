package ms.airy.lib.core.dialog;

import android.app.DialogFragment;


public interface ActionableDialogHost
{
    DialogFragment addActionable(ActionableDialogListener actionable);
}

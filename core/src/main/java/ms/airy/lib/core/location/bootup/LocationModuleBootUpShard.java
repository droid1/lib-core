package ms.airy.lib.core.location.bootup;

import android.content.Context;
import android.content.Intent;

import ms.airy.lib.core.receiver.AbstractBroadcastReceiverShard;
import ms.airy.lib.core.receiver.BroadcastReceiverShard;


/**
 * BroadcastReceiver.onReceive() delegation.
 */
public class LocationModuleBootUpShard extends AbstractBroadcastReceiverShard implements BroadcastReceiverShard
{
    public LocationModuleBootUpShard()
    {
//        this(null);
    }
//    public LocationTrackerShard(Activity parentActivity)
//    {
//        // parentActivity could be null...
//        super(parentActivity);
//    }


    @Override
    public void onReceive(Context context, Intent intent)
    {
        Log.i("LocationModuleBootUpShard.onReceive().");


        // intent filter ???
        // ....
        String action = intent.getAction();
        if(Log.D) Log.d("onReceive() action = " + action);
        if(action == null ||
                !action.equals(Intent.ACTION_BOOT_COMPLETED)) {
            // bail out???
            if(Log.I) Log.i("Unhandled intent: action = " + action);
            return;
        }


        // TBD:
        LocationBootUpHelper.getInstance(context).prepareLocationModule();


    }


}

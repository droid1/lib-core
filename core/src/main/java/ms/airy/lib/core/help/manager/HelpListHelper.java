package ms.airy.lib.core.help.manager;

import android.content.Context;

import ms.airy.lib.core.help.common.HelpSection;
import ms.airy.lib.core.help.content.LocalContentLoader;
import ms.airy.lib.core.help.content.OnlineContentLoader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


public final class HelpListHelper
{
    // temporary
    private static final long REFRESH_INTERVAL = 12 * 3600 * 1000L;  // half a day.

    private static HelpListHelper INSTANCE = null;
    public static HelpListHelper getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new HelpListHelper(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Aplication context
    private final Context context;

    // "Cache"
    // Map of id -> Section title.
    private final Map<Long, String> sectionTitleMap = new HashMap<Long, String>();
    // Map of position -> id.  Heck. This is needed to support onItemClick in the List.
    // Is there a better way???
    private final Map<Integer, Long> sectionIdMap = new HashMap<Integer, Long>();
    // This is used to support ttl-based refreshing...
    private long lastRefreshed = 0L;

    private HelpListHelper(Context context)
    {
        this.context = context;

        // ???
        long[] ids = LocalContentLoader.getInstance(context).getSectionIds();
        OnlineContentLoader.getInstance(context).prefetchOnlineContent(ids);
        // ???
    }

    public void clearSectionMap()
    {
        sectionTitleMap.clear();
        sectionIdMap.clear();
        lastRefreshed = 0L;   // ????
    }
    private void buildSectionMap()
    {
        sectionTitleMap.clear();
        sectionIdMap.clear();

        // TBD:
        // Need more efficient and "better" algorithm....
        // ....

        // [1] Local files first. (Content may be from remote or HelpRegistry cache.)
        long[] ids = LocalContentLoader.getInstance(context).getSectionIds();
        if(ids != null) {
            for(long id : ids) {
                HelpSection section = HelpRegistry.getInstance(context).getSection(id);
                if(section != null) {
                    String title = section.getTitle();
                    if(title != null && !title.isEmpty()) {
                        sectionTitleMap.put(id, title);
                    } else {
                        // ????
                        Log.w("No title set for section id = " + id);
                    }
                } else {
                    // ????
                }
            }
        }

        // [2] Add all help sections which are not in the local folder.
        // HelpRegistry.getInstance(context).getCurrentRegistry();
        Set<Long> idSet = HelpRegistry.getInstance(context).getIdSetFromCurrentRegistry();
        if(idSet != null && !idSet.isEmpty()) {
            for(Long id : idSet) {
                if (! sectionTitleMap.containsKey(id)) {
                    HelpSection section = HelpRegistry.getInstance(context).getSection(id);
                    if(section != null) {
                        String title = section.getTitle();
                        if(title != null && !title.isEmpty()) {
                            sectionTitleMap.put(id, title);
                        } else {
                            // ????
                            Log.w("No title set for section id = " + id);
                        }
                    } else {
                        // ????
                    }
                }
            }
        }

        // Reset the timestamp.
        lastRefreshed = System.currentTimeMillis();
    }
    public Map<Long, String> getSectionTitleMap()
    {
        if(lastRefreshed < System.currentTimeMillis() - REFRESH_INTERVAL) {
            buildSectionMap();
        }
        return sectionTitleMap;
    }

    public String[] getSectionTitleList()
    {
        Map<Long, String> map = getSectionTitleMap();
        List<String> list = new ArrayList<String>(map.values());

        // Heck!!!!

        // TBD:
        // we probably need to use an ordered map for sectionTitleMap, if you use this "heck"
        // for now, sectionIdMap is refreshed only when getSectionTitleList() is called.
        sectionIdMap.clear();
        // TBD: does map.values() and map.keySet() return items in the same order????
        List<Long> ids = new ArrayList<Long>(map.keySet());
        for(int i=0; i<ids.size(); i++) {
            sectionIdMap.put(i, ids.get(i));
        }
        // ...

        return list.toArray(new String[]{});
    }
    // Heck!!!!
    // Need a better way!!!!!
    public Map<Integer, Long> getSectionIdMap()
    {
        // This method should be called after getSectionTitleList() is called.
        // ...
        return sectionIdMap;
    }
    // Ditto
    public long getSectionIdFromPosition(int position)
    {
        return sectionIdMap.get(position);
    }

}

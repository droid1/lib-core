package ms.airy.lib.core.dialog.stock;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

import ms.airy.lib.core.R;
import ms.airy.lib.core.dialog.ActionableAlertDialogListener;
import ms.airy.lib.core.dialog.base.BasicMessageDialogFragment;


// http://developer.android.com/guide/topics/ui/dialogs.html
public class ActionableYesNoDialogFragment extends BasicMessageDialogFragment
{
    //    private ActionableAlertDialog mActivity = null;

    // Factory methods.
    public static ActionableYesNoDialogFragment newInstance(CharSequence title, CharSequence message)
    {
        ActionableYesNoDialogFragment fragment = new ActionableYesNoDialogFragment();
        addArguments(fragment, title, message);
        return fragment;
    }
    public static ActionableYesNoDialogFragment newInstance(int titleResId, int messageResId)
    {
        ActionableYesNoDialogFragment fragment = new ActionableYesNoDialogFragment();
        addArguments(fragment, titleResId, messageResId);
        return fragment;
    }
    public ActionableYesNoDialogFragment()
    {
    }


    @Override
    protected AlertDialog.Builder buildDialogBuilder()
    {
        AlertDialog.Builder dialogBuilder = super.buildDialogBuilder();

        dialogBuilder.setPositiveButton(R.string.basic_yesno_dialog_yes,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        ((ActionableAlertDialogListener) getActivity()).onPositiveClick(ActionableYesNoDialogFragment.this.getTag());
                    }
                }
        )
        .setNegativeButton(R.string.basic_yesno_dialog_no,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        ((ActionableAlertDialogListener) getActivity()).onNegativeClick(ActionableYesNoDialogFragment.this.getTag());
                    }
                }
        );

        return dialogBuilder;
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try {
            ActionableAlertDialogListener mActivity = (ActionableAlertDialogListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement ActionableAlertDialog");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
//        mActivity = null;
    }


}

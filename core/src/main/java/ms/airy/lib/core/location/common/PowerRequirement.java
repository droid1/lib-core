package ms.airy.lib.core.location.common;


// To be used in user preference settings.
public final class PowerRequirement
{
    public static final int NONE = 0;   // ???
    public static final int LOW = 1;
    public static final int MEDIUM = 2;
    public static final int HIGH = 3;
    // ...

    private PowerRequirement() {}

    // temporary
    public static int getDefaultRequirement()
    {
        return NONE;
    }

}

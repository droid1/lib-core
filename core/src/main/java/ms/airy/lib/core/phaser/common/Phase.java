package ms.airy.lib.core.phaser.common;


public final class Phase
{
    // Phases.
    // Bit masks (to support multiple phases at the same time ????)
    public static final int P01 = 0x1 << 0;
    public static final int P02 = 0x1 << 1;
    public static final int P03 = 0x1 << 2;
    public static final int P04 = 0x1 << 3;
    // ...
//    // INITIAL is not a valid phase.
//    public static final int INITIAL = 0;
    // FINAL refers to the maximum upper bound > MAX phase.
    // public static final int FINAL = -1;   // This does not work since we use inequality to check the valid phase range...
    // public static final int FINAL = Integer.MAX_VALUE;
    public static final int FINAL = 0x1 << 30;//    // FINAL is not a valid phase.
//    public static final int INITIAL = 0;

    // ...

//    // TBD: Use config ???
//    // Inclusive. A single value (cannot be |'ed), for now...
//    private static final int CURRENT_PHASE = P01;
//    // ....
//
//    private static int INCLUDED_PHASES;
//    static {
//        // Is there a better way?
//        switch(CURRENT_PHASE) {
//            case P01:
//                INCLUDED_PHASES = P01;
//                break;
//            case P02:
//                INCLUDED_PHASES = P01 | P02;
//                break;
//            case P03:
//                INCLUDED_PHASES = P01 | P02 | P03;
//                break;
//            case P04:
//            default:
//                INCLUDED_PHASES = P01 | P02 | P03 | P04;
//                break;
//            // etc...
//        }
//    }

    // ....


    // static methods only.
    private Phase() {}


//    public static int getCurrentPhase()
//    {
//        return CURRENT_PHASE;
//    }
//
//    public static boolean isPhaseIncluded(int phase)
//    {
////        if(phase >= FINAL) {
////            return false;
////        } else {
////            return (INCLUDED_PHASES & phase) != 0;
////        }
//        return (INCLUDED_PHASES & phase) != 0;
//    }


    public static final class Range
    {
        private Integer min;   // Inclusive
        private Integer max;   // Exclusive

        public Range(Integer min, Integer max)
        {
            this.min = min;
            this.max = max;
        }

        public Integer getMin()
        {
            return min;
        }
        public Integer getMax()
        {
            return max;
        }
        public Integer setMax(Integer max)
        {
            Integer cur = this.max;
            this.max = max;
            return cur;
        }
    };


}

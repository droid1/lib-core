package ms.airy.lib.core.launcher;

import android.content.Context;


// TBD:
// ...
public final class LauncherActivityRegistry
{
    private static LauncherActivityRegistry INSTANCE = null;
    public static LauncherActivityRegistry getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new LauncherActivityRegistry(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;

    private LauncherActivityRegistry(Context context)
    {
        this.context = context;
    }


    // TBD

    // map
    // "condition" -> activity.class ????
    // ....

    // ....


}

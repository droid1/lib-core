package ms.airy.lib.core.about.ui;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import ms.airy.lib.core.R;
import ms.airy.lib.core.about.common.AboutInfo;
import ms.airy.lib.core.about.helper.AboutInfoRegistry;


// temporary
// Client/appn needs to override this or create a new activity:
// (1) Populate the aboutInfo list in AboutInfoRegistry, and
// (2) Add AboutInfoListFragment.
public class AboutInfoListActivity extends Activity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_info_list);

        if (savedInstanceState == null) {
            FragmentTransaction transaction = getFragmentManager().beginTransaction();

            // TBD:
            // Is there a better way?
            List<AboutInfo> aboutInfos = new ArrayList<AboutInfo>();
            // Populate aboutInfos here.
            // ....
            AboutInfoRegistry.getInstance(this).setAboutInfos(aboutInfos);

            // AboutInfoListFragment uses AboutInfoRegistry aboutInfo list implicitly.
            AboutInfoListFragment aboutInfoListFragment = AboutInfoListFragment.newInstance();
            transaction.add(R.id.container, aboutInfoListFragment);

            transaction.commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.about_info_list, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}

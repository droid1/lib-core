package ms.airy.lib.core.notification.scheduler;

import android.app.Activity;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;

import ms.airy.lib.core.notification.NotificationBuilder;
import ms.airy.lib.core.notification.common.NotificationAndTimeStruct;
import ms.airy.lib.core.cron.MultiCronHelper;

import java.util.Set;


/**
 * A generic utility class for scheduling notifications at a later time.
 * (Android framework does not seem to allow sending notification at a future time. ???)
 *
 * Note on implementation:
 *
 * NotificationScheduler is a singleton.
 * Initially, it was intended to have a single thread that handles
 *     accepting and scheduling notifications.
 *     But, that seems not possible without using "heavy machinery" like AIDL services, etc.
 * The best solution appears to be to spawn a separate thread every time
 *     new scheduling request is received.
 * Each thread is associated with a NotificationHandler.
 * (TBD: How to terminate the handler threads after notification(s) are processed??
 *      Or, are they automatically stopped when the run() method returns, just like "regular" thread?)
 *
 * Note that this is only sutiable for scheduling notification in the near future, while the app is running.
 *      if the app is closed, the handler/thread will be killed, etc,
 *      and hence this design will not work in general.
 * TBD: We need to use Broadcast receiver.
 */
public final class NotificationScheduler
{
    private static NotificationScheduler INSTANCE = null;
    public static NotificationScheduler getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new NotificationScheduler(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Counter for the handler threads.
    private int counter;
    // To set the notificationId
    private int notificationId;
    // Application context.
    private final Context context;
    // private final NotificationHandler notificationHandler;
//    private final SchedulerThread schedulerThread;
    private NotificationScheduler(Context context)
    {
        this.counter = 0;
        this.notificationId = 0;
        this.context = context;
        // notificationHandler = new NotificationHandler();  // ??? Looper ???
//        schedulerThread = new SchedulerThread("");
//        schedulerThread.start();
    }


    // TBD:

    public void scheduleNotification(int requestCode, Class<? extends Activity> clss, String contentTitle, String contentText, int smallIconRes, long notificationTime)
    {
        Notification notification = NotificationBuilder.buildNotification(context, requestCode, clss, contentTitle, contentText, smallIconRes);
        scheduleNotification(notification, notificationTime);
    }

    public void scheduleNotification(PendingIntent contentIntent, String contentTitle, String contentText, int smallIconRes, long notificationTime)
    {
        Notification notification = NotificationBuilder.buildNotification(context, contentIntent, contentTitle, contentText, smallIconRes);
        scheduleNotification(notification, notificationTime);
    }

    public void scheduleNotification(Notification notification, long notificationTime)
    {
        scheduleNotification(0, notification, notificationTime);  // ???
    }
    public void scheduleNotification(int notificationId, Notification notification, long notificationTime)
    {
        // Log.w(">>>>>>>>> notificationTime = " + notificationTime);

        String name = "Scheduler-" + counter++;
        SchedulerThread schedulerThread = new SchedulerThread(context, name);
        // Log.w(">>>>>>>>> AAA");

        schedulerThread.scheduleNotification(notificationId, notification, notificationTime);
        // Log.w(">>>>>>>>> BBB");

        schedulerThread.start();
    }

    public void scheduleNotifications(Set<NotificationAndTimeStruct> notificationTimePairs)
    {
        String name = "Scheduler-" + counter++;
        SchedulerThread schedulerThread = new SchedulerThread(context, name);
        schedulerThread.scheduleNotifications(notificationTimePairs);
        schedulerThread.start();
    }


    // temporary
    private static final int CRON_REPEATS = 10;

    // Note that all notifications in the cron repeats
    //           share the same notification ID.
    // delayMillis > 0 ==> delayed.
    // delayMillis < 0 ==> advanced.
    public void scheduleNotification(Notification notification, String cronExpression)
    {
        scheduleNotification(notification, cronExpression, 0L);
    }
    public void scheduleNotification(Notification notification, String cronExpression, long delayMillis)
    {
        scheduleNotification(0, notification, cronExpression, delayMillis);
    }
    public void scheduleNotification(int notificationId, Notification notification, String cronExpression)
    {
        scheduleNotification(notificationId, notification, cronExpression, 0L);
    }
    public void scheduleNotification(int notificationId, Notification notification, String cronExpression, long delayMillis)
    {
        scheduleNotification(notificationId, notification, cronExpression, delayMillis, CRON_REPEATS);
    }
    public void scheduleNotification(Notification notification, String cronExpression, int repeats)
    {
        scheduleNotification(notification, cronExpression, 0L, repeats);
    }
    public void scheduleNotification(Notification notification, String cronExpression, long delayMillis, int repeats)
    {
        scheduleNotification(0, notification, cronExpression, delayMillis, repeats);
    }
    public void scheduleNotification(int notificationId, Notification notification, String cronExpression, int repeats)
    {
        scheduleNotification(notificationId, notification, cronExpression, 0L, repeats);
    }
    public void scheduleNotification(int notificationId, Notification notification, String cronExpression, long delayMillis, int repeats)
    {
        scheduleNotification(notificationId, notification, cronExpression, delayMillis, repeats, false);
    }
    public void scheduleNotification(Notification notification, String cronExpression, int repeats, boolean useDifferentIds)
    {
        scheduleNotification(notification, cronExpression, 0L, repeats, useDifferentIds);
    }
    public void scheduleNotification(Notification notification, String cronExpression, long delayMillis, int repeats, boolean useDifferentIds)
    {
        scheduleNotification(0, notification, cronExpression, delayMillis, repeats, useDifferentIds);
    }
    public void scheduleNotification(int notificationId, Notification notification, String cronExpression, int repeats, boolean useDifferentIds)
    {
        scheduleNotification(notificationId, notification, cronExpression, 0L, repeats, useDifferentIds);
    }
    public void scheduleNotification(int notificationId, Notification notification, String cronExpression, long delayMillis, int repeats, boolean useDifferentIds)
    {
        if(Log.I) Log.i(">>>>>>>>> scheduleNotification(). notificationId = " + notificationId + "; cronExpression = " + cronExpression + "; delayMillis = " + delayMillis + "; repeats = " + repeats + "; useDifferentIds = " + useDifferentIds);

        String name = "Scheduler-" + counter++;
        SchedulerThread schedulerThread = new SchedulerThread(context, name);

//        // testing
//        Log.i("---------------- Calling gc");
//        System.gc();
//        Log.i("---------------- Called gc");

        // ???
        int notiId = 0;
        if(useDifferentIds) {
            if(notificationId > 0) {
                notiId = notificationId;
            } else {
                // notiId = this.notificationId;
            }
        } else {
            if(notificationId > 0) {
                notiId = notificationId;
            } else {
                notiId = this.notificationId++;
            }
        }

        long nextTime = System.currentTimeMillis();
        for(int i=0; i<repeats; i++) {
            Log.i("i = " + i);

//            nextTime = MultiCronHelper.getInstance().getNextCronTime(cronExpression, nextTime + 1000L);
            nextTime = MultiCronHelper.getInstance().getNextCronTimeShifted(cronExpression, nextTime + 1000L, delayMillis);

            if(Log.I) Log.i(">>>>>>>>> scheduleNotification(). nextTime = " + nextTime);

            // "Clone" notifications?
            // ???


            // By default, we use the same notification Id for all notifications repeated...
            if(useDifferentIds) {
                if(notificationId > 0) {
                    schedulerThread.scheduleNotification(notiId++, notification, nextTime);
                } else {
                    schedulerThread.scheduleNotification(this.notificationId++, notification, nextTime);
                }
            } else {
                schedulerThread.scheduleNotification(notiId, notification, nextTime);
            }
        }

        schedulerThread.start();
    }

}

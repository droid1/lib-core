package ms.airy.lib.core.location.receiver;

import android.content.Context;
import android.content.Intent;

import ms.airy.lib.core.location.common.LocationTrackerConsts;
import ms.airy.lib.core.location.tracking.LocationTrackingHelper;
import ms.airy.lib.core.receiver.AbstractBroadcastReceiverShard;
import ms.airy.lib.core.receiver.BroadcastReceiverShard;


/**
 * BroadcastReceiver.onReceive() delegation.
 */
public class LocationTrackerShard extends AbstractBroadcastReceiverShard implements BroadcastReceiverShard
{
    public LocationTrackerShard()
    {
//        this(null);
    }
//    public LocationTrackerShard(Activity parentActivity)
//    {
//        // parentActivity could be null...
//        super(parentActivity);
//    }


    @Override
    public void onReceive(Context context, Intent intent)
    {
        Log.i("LocationTrackerShard.onReceive().");


        // intent filter ???
        // ....
        String action = intent.getAction();
        if(Log.D) Log.d("onReceive() action = " + action);
        if(action == null ||
                ! (action.equals(LocationTrackerConsts.ACTION_SCHEDULED_LOCATION_TRACKING)
                || action.equals(LocationTrackerConsts.ACTION_SCHEDULED_LOCATION_PROCESSING))) {
            // bail out???
            if(Log.I) Log.i("Unhandled intent: action = " + action);
            return;
        }


        // TBD:
        LocationTrackingHelper.getInstance(context).startLocationTracking(false);




        // TBD:

        // the best way is
        //    (1) start intent service
        //    (2) do location tracking for a little while...
        //    (3) based on the data,
        //        optionally launch LocationTrackingScheduler
        //        for short term more frequent location tracking...
        //




    }


}

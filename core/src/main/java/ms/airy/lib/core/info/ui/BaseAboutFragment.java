package ms.airy.lib.core.info.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ms.airy.lib.core.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class BaseAboutFragment extends Fragment
{

    public BaseAboutFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_base_about, container, false);
        return rootView;
    }
}

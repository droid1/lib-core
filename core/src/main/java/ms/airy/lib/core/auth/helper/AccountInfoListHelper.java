package ms.airy.lib.core.auth.helper;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import ms.airy.lib.core.auth.common.AccountInfo;


// TBD:
public final class AccountInfoListHelper
{
    private static AccountInfoListHelper INSTANCE = null;
    public static AccountInfoListHelper getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new AccountInfoListHelper(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;

    // AccountInfo list
    private List<AccountInfo> accountInfoList = null;

    private AccountInfoListHelper(Context context)
    {
        this.context = context;
        // initList();
    }

    private void initList()
    {
        // Note that we may not have the primary account (yet).
        int primaryAcctType = AccountHelper.getInstance(context).getPrimaryAccountType();
        String primaryAcctName = AccountHelper.getInstance(context).getPrimaryAccountName();

        accountInfoList = new ArrayList<AccountInfo>();
        Integer[] types = AccountHelper.getInstance(context).getAllAccountTypes();
        // Note that the types list is filtered and ordered.
        for(int type : types) {
            String accountName = AccountHelper.getInstance(context).getMainAccountName(type);
            // Note that accountName can be null since the main account has not been set for the type.

            boolean isAuthorized = false;
            if(accountName != null) {
                isAuthorized = AuthHelper.getInstance(context).isAccountAuthorized(type, accountName);
            }

            AccountInfo accountInfo = new AccountInfo(type, accountName, isAuthorized);

            if(type == primaryAcctType && (primaryAcctName != null && primaryAcctName.equals(accountName))) {
                accountInfo.setPrimary();
            }

            accountInfoList.add(accountInfo);
        }
    }


    public List<AccountInfo> getAccountInfoList()
    {
        if(accountInfoList == null) {
            initList();
        }
        return accountInfoList;
    }

    public int getListSize()
    {
        if(accountInfoList == null) {
            initList();
        }
        return accountInfoList.size();
    }

    public boolean isListEmpty()
    {
        if(accountInfoList == null) {
            initList();
        }
        return accountInfoList.isEmpty();
    }


}

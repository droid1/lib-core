package ms.airy.lib.core.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;


// Temporary
// Vs. DateStringUtil???
public final class DateUtil
{
    private DateUtil() {}

    // TBD:
    // This does not really make sense (in Android apps)
    // We should really use the user's timezone and LOCALE
    // to format the date/time...
    // The date format should not be hard coded.....
    // ...


    // Format the timestamp to a datetime format.

    public static String formatDate(Long time)
    {
        return formatDate(time, false);
    }
    public static String formatDate(Long time, boolean omitYear)
    {
        if(time == null) {
            return null;  // ???
        }
        // SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a z");
        // SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss z");
        SimpleDateFormat dateFormat = null;
        if(omitYear) {
            dateFormat = new SimpleDateFormat("MM/dd HH:mm:ss");
        } else {
            dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        }
        //TimeZone timeZone = TimeZone.getDefault();
        ////TimeZone timeZone = TimeZone.getTimeZone("US/Pacific");
        //dateFormat.setTimeZone(timeZone);
        String date = dateFormat.format(new Date(time));
        return date;
    }
    public static String formatDate(Long time, String timeZone)
    {
        return formatDate(time, timeZone, false);
    }
    public static String formatDate(Long time, String timeZone, boolean omitYear)
    {
        if(time == null) {
            return null;  // ???
        }
        // SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a z");
        // SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss z");
        SimpleDateFormat dateFormat = null;
        if(omitYear) {
            dateFormat = new SimpleDateFormat("MM/dd HH:mm:ss");
        } else {
            dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        }
        TimeZone tz = TimeZone.getTimeZone(timeZone);
        dateFormat.setTimeZone(tz);
        String date = dateFormat.format(new Date(time));
        return date;
    }
    public static String formatDateNoSeconds(Long time)
    {
        return formatDateNoSeconds(time, false);
    }
    public static String formatDateNoSeconds(Long time, boolean omitYear)
    {
        if(time == null) {
            return null;  // ???
        }
        // SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm z");
        SimpleDateFormat dateFormat = null;
        if(omitYear) {
            dateFormat = new SimpleDateFormat("MM/dd HH:mm");
        } else {
            dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        }
        String date = dateFormat.format(new Date(time));
        return date;
    }
    public static String formatDateNoSeconds(Long time, String timeZone)
    {
        return formatDateNoSeconds(time, timeZone, false);
    }
    public static String formatDateNoSeconds(Long time, String timeZone, boolean omitYear)
    {
        if(time == null) {
            return null;  // ???
        }
        // SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a z");
        // SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm z");
        SimpleDateFormat dateFormat = null;
        if(omitYear) {
            dateFormat = new SimpleDateFormat("MM/dd HH:mm");
        } else {
            dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        }
        TimeZone tz = TimeZone.getTimeZone(timeZone);
        dateFormat.setTimeZone(tz);
        String date = dateFormat.format(new Date(time));
        return date;
    }

    public static String formatDateNoHours(Long time)
    {
        return formatDateNoHours(time, false);
    }
    public static String formatDateNoHours(Long time, boolean omitYear)
    {
        if(time == null) {
            return null;  // ???
        }
        // SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy z");
        SimpleDateFormat dateFormat = null;
        if(omitYear) {
            dateFormat = new SimpleDateFormat("MM/dd");
        } else {
            dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        }
        String date = dateFormat.format(new Date(time));
        return date;
    }
    public static String formatDateNoHours(Long time, String timeZone)
    {
        return formatDateNoHours(time, timeZone, false);
    }
    public static String formatDateNoHours(Long time, String timeZone, boolean omitYear)
    {
        if(time == null) {
            return null;  // ???
        }
        // SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy z");
        SimpleDateFormat dateFormat = null;
        if(omitYear) {
            dateFormat = new SimpleDateFormat("MM/dd");
        } else {
            dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        }
        TimeZone tz = TimeZone.getTimeZone(timeZone);
        dateFormat.setTimeZone(tz);
        String date = dateFormat.format(new Date(time));
        return date;
    }


    public static Long parseDate(String strDate)
    {
        if(strDate == null) {
            return null;  // ???
        }
        Long t = null;
        try {
            // SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a z");
            // SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss z");
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            //TimeZone timeZone = TimeZone.getDefault();
            ////TimeZone timeZone = TimeZone.getTimeZone("US/Pacific");
            //dateFormat.setTimeZone(timeZone);
            Date date = dateFormat.parse(strDate);
            t = date.getTime();
        } catch (ParseException e1) {
            if(Log.W) Log.w("Failed to parse strDate: " + strDate, e1);
        } catch (Exception e2) {
            if(Log.W) Log.w("Failed to parse Date: strDate = " + strDate, e2);
        }
        return t;
    }
    public static Long parseDate(String strDate, String timeZone)
    {
        if(strDate == null) {
            return null;  // ???
        }
        Long t = null;
        try {
            // SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm z");
            // SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a z");
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            TimeZone tz = TimeZone.getTimeZone(timeZone);
            dateFormat.setTimeZone(tz);
            Date date = dateFormat.parse(strDate);
            t = date.getTime();
        } catch (ParseException e1) {
            if(Log.W) Log.w("Failed to parse strDate: " + strDate, e1);
        } catch (Exception e2) {
            if(Log.W) Log.w("Failed to parse Date: strDate = " + strDate, e2);
        }
        return t;
    }

}

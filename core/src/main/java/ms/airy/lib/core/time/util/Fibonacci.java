package ms.airy.lib.core.time.util;


/**
 */
public final class Fibonacci
{
    // Fibonaccci sequence starting from {1,1}
    private static final int[] SEQ11 = new int[]{1,1,2,3,5,8,13,21,34,55,89,144,233,377,610,987};
    private static final int SEQ11_LEN = SEQ11.length;

    private Fibonacci() {}

    // Returns the Fibonacci number at position, index,
    //   for the sequence that starts with 1 and 1.
    public static int getNumnber(int index)
    {
        if(index < SEQ11_LEN) {
            return SEQ11[index];
        } else {
            // TBD: Use more efficient algo????
            int prev1 = SEQ11[SEQ11_LEN - 2];
            int prev2 = SEQ11[SEQ11_LEN - 1];
            int num = 0;
            for(int i=SEQ11_LEN; i<=index; i++) {
                num = prev1 + prev2;
                prev1 = prev2;
                prev2 = num;
            }
            return num;
        }
    }

    // Using two starting numbers, start1 and start2.
    // Indices of these two numbers are 0 and 1, respectively.
    public static int getNumnber(int index, int start1, int start2)
    {
        // TBD: Use more efficient algo????
        if(index == 0) {
            return start1;
        } else if(index == 1) {
            return start2;
        } else {
            int prev1 = start1;
            int prev2 = start2;
            int num = 0;
            for (int i = 2; i <= index; i++) {
                num = prev1 + prev2;
                prev1 = prev2;
                prev2 = num;
            }
            return num;
        }
    }

}

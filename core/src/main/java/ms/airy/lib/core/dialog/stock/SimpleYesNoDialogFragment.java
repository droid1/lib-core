package ms.airy.lib.core.dialog.stock;

import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.DialogInterface;

import ms.airy.lib.core.R;
import ms.airy.lib.core.dialog.ActionableAlertDialogHost;
import ms.airy.lib.core.dialog.ActionableAlertDialogListener;
import ms.airy.lib.core.dialog.base.BasicMessageDialogFragment;

import java.util.HashSet;
import java.util.Set;


// http://developer.android.com/guide/topics/ui/dialogs.html
public class SimpleYesNoDialogFragment extends BasicMessageDialogFragment implements ActionableAlertDialogHost
{
    public static SimpleYesNoDialogFragment newInstance(CharSequence title, CharSequence message)
    {
        SimpleYesNoDialogFragment fragment = new SimpleYesNoDialogFragment();
        addArguments(fragment, title, message);
        return fragment;
    }
    public static SimpleYesNoDialogFragment newInstance(int titleResId, int messageResId)
    {
        SimpleYesNoDialogFragment fragment = new SimpleYesNoDialogFragment();
        addArguments(fragment, titleResId, messageResId);
        return fragment;
    }
    public SimpleYesNoDialogFragment()
    {
    }


    ////////////////////////////////////////////////////
    // ActionableAlertDialog interface callbacks

    private final Set<ActionableAlertDialogListener> actionables = new HashSet<ActionableAlertDialogListener>();
    @Override
    public DialogFragment addActionable(ActionableAlertDialogListener actionable)
    {
        actionables.add(actionable);
        return this;
    }


    @Override
    protected AlertDialog.Builder buildDialogBuilder()
    {
        AlertDialog.Builder dialogBuilder = super.buildDialogBuilder();

        dialogBuilder.setPositiveButton(R.string.basic_yesno_dialog_yes,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int whichButton) {
                        for(ActionableAlertDialogListener a : actionables) {
                            a.onPositiveClick(SimpleYesNoDialogFragment.this.getTag());
                        }
                    }
                }
        )
        .setNegativeButton(R.string.basic_yesno_dialog_no,
                new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int whichButton)
                    {
                        for (ActionableAlertDialogListener a : actionables) {
                            a.onNegativeClick(SimpleYesNoDialogFragment.this.getTag());
                        }
                    }
                }
        );

        return dialogBuilder;
    }



}

package ms.airy.lib.core.control;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;

import ms.airy.lib.core.control.common.ControlAxis;
import ms.airy.lib.core.time.timer.TimerState;
import ms.airy.lib.core.time.timer.TimerRange;
import ms.airy.lib.core.time.timer.FlexibleCountDownTimer;

import ms.airy.lib.core.R;


/**
 * Count-down timer control / progress bar.
 * Cf. http://developer.android.com/training/custom-views/create-view.html
 */
public class GenericTimerControl extends RepeatingTimerControl implements FlexibleCountDownTimer.CountDownListener
{
    // Timer counts down from right to left for horz,
    //               and from top to bottom for vert...
    // Not configurable.

    // TBD:
    // Include indicators in this control????
    // ...



    // Geometry
    // The size of the control.
    // timer may be a part of the control (e.g., when we include indicators or paddings, etc...)
    private final static int HORZ_MIN_CONTROL_WIDTH = 96;
    private final static int HORZ_DEF_CONTROL_WIDTH = 128;
    private final static int HORZ_MAX_CONTROL_WIDTH = 480;
    private final static int HORZ_MIN_CONTROL_HEIGHT = 32;
    private final static int HORZ_DEF_CONTROL_HEIGHT = 48;
    private final static int HORZ_MAX_CONTROL_HEIGHT = 128;

    private final static int VERT_MIN_CONTROL_WIDTH = 32;
    private final static int VERT_DEF_CONTROL_WIDTH = 48;
    private final static int VERT_MAX_CONTROL_WIDTH = 128;
    private final static int VERT_MIN_CONTROL_HEIGHT = 96;
    private final static int VERT_DEF_CONTROL_HEIGHT = 128;
    private final static int VERT_MAX_CONTROL_HEIGHT = 480;

    // The timer (and indicator, if any) is drawn inside the control with the padding.
    // TBD: Different left, right, top, bottom paddings ????
    private final static int CONTROL_PADDING = 2;

    // The size of the timer.
    private final static int HORZ_MIN_TIMER_WIDTH = HORZ_MIN_CONTROL_WIDTH - 2*CONTROL_PADDING;
    private final static int HORZ_DEF_TIMER_WIDTH = HORZ_DEF_CONTROL_WIDTH - 2*CONTROL_PADDING;
    private final static int HORZ_MAX_TIMER_WIDTH = HORZ_MAX_CONTROL_WIDTH - 2*CONTROL_PADDING;
    private final static int HORZ_MIN_TIMER_HEIGHT = HORZ_MIN_CONTROL_HEIGHT - 2*CONTROL_PADDING;
    private final static int HORZ_DEF_TIMER_HEIGHT = HORZ_DEF_CONTROL_HEIGHT - 2*CONTROL_PADDING;
    private final static int HORZ_MAX_TIMER_HEIGHT = HORZ_MAX_CONTROL_HEIGHT - 2*CONTROL_PADDING;

    private final static int VERT_MIN_TIMER_WIDTH = VERT_MIN_CONTROL_WIDTH - 2*CONTROL_PADDING;
    private final static int VERT_DEF_TIMER_WIDTH = VERT_DEF_CONTROL_WIDTH - 2*CONTROL_PADDING;
    private final static int VERT_MAX_TIMER_WIDTH = VERT_MAX_CONTROL_WIDTH - 2*CONTROL_PADDING;
    private final static int VERT_MIN_TIMER_HEIGHT = VERT_MIN_CONTROL_HEIGHT - 2*CONTROL_PADDING;
    private final static int VERT_DEF_TIMER_HEIGHT = VERT_DEF_CONTROL_HEIGHT - 2*CONTROL_PADDING;
    private final static int VERT_MAX_TIMER_HEIGHT = VERT_MAX_CONTROL_HEIGHT - 2*CONTROL_PADDING;



    // Timer range, and the current value.
    private TimerRange timerRange = null;

    // TBD:
    // Use CountDownTimer or PropertyAnimator ?????

    private FlexibleCountDownTimer countDownTimer = null;

//    private ValueAnimator timerAnimator;
//    // ????
//    static {
//        ValueAnimator.setFrameDelay(50L);
//    }



    // Rects
    private Rect mRectControl;
    private Rect mRectTimer;
    private Rect mRectTimerProgressBar;    // "Active" part. "val"-dependent. This is a subset of mRectTimer.

    // Colors
    private int mColorBackground;             // Background of the control
    private int mColorTimer;                  // Background of the timer
    private int mColorTimerProgressNormal;    // "Active" part.
    private int mColorTimerProgressWarning;   // "Active" part, when val < threshold.

    // Paints.
    private Paint mPaintBackground;
    private Paint mPaintTimer;
    private Paint mPaintTimerProgressNormal;
    private Paint mPaintTimerProgressWarning;



    // For the control size.
    private int mHorzControlWidth;
    private int mHorzControlHeight;
    private int mHorzTimerWidth;
    private int mHorzTimerHeight;
    // private int mHorzTimerProgressBarWidth;
    // private int mHorzTimerProgressBarHeight;

    private int mVertControlWidth;
    private int mVertControlHeight;
    private int mVertTimerWidth;
    private int mVertTimerHeight;
    // private int mVertTimerProgressBarWidth;
    // private int mVertTimerProgressBarHeight;



    // Ctor's
    public GenericTimerControl(Context context)
    {
        super(context);
        initControl(context, null, 0);
    }
    public GenericTimerControl(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        initControl(context, attrs, 0);
    }
    public GenericTimerControl(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        initControl(context, attrs, defStyle);
    }

    private void initControl(Context context, AttributeSet attrs, int defStyle)
    {
        if(Log.D) Log.d("initControl(): attrs = " + attrs);

        // Default orientation-axis mapping.
        setOrientationAxis(Configuration.ORIENTATION_PORTRAIT, ControlAxis.AXIS_HORIZONTAL, Configuration.ORIENTATION_LANDSCAPE, ControlAxis.AXIS_HORIZONTAL);
        // setOrientationAxis(Configuration.ORIENTATION_PORTRAIT, AXIS_VERTICAL, Configuration.ORIENTATION_LANDSCAPE, AXIS_VERTICAL);
        // ...

        // Custom attributes.

        if(attrs != null) {
            TypedArray aa = context.getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.BaseControl,
                    0, 0);
            try {
                // TBD: Why does this not work????
                int c1 = aa.getColor(R.styleable.BaseControl_controlColorBackground, Color.rgb(255, 255, 255));
                setColorBackground(c1);
            } finally {
                aa.recycle();
            }
            TypedArray a0 = context.getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.AnamorphicControl,
                    0, 0);
            try {
                boolean axisOrientationDependent = a0.getBoolean(R.styleable.AnamorphicControl_axisOrientationDependent, isAxisOrientationDependent());
                setAxisOrientationDependent(axisOrientationDependent);

                String axisForPort = a0.getString(R.styleable.AnamorphicControl_controlAxisForPort);
                int axisPort = ControlAxis.convertAxisStringToInt(axisForPort);
                if(axisPort == ControlAxis.AXIS_UNDEFINED) {
                    axisPort = getAxisForOrientation(Configuration.ORIENTATION_PORTRAIT);
                }
                String axisForLand = a0.getString(R.styleable.AnamorphicControl_controlAxisForLand);
                int axisLand = ControlAxis.convertAxisStringToInt(axisForLand);
                if(axisLand == ControlAxis.AXIS_UNDEFINED) {
                    axisLand = getAxisForOrientation(Configuration.ORIENTATION_LANDSCAPE);
                }
                setOrientationAxis(Configuration.ORIENTATION_PORTRAIT, axisPort, Configuration.ORIENTATION_LANDSCAPE, axisLand);
            } finally {
                a0.recycle();
            }
            TypedArray a1 = context.getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.TimerControl,
                    0, 0);
            try {
                int c2 = a1.getColor(R.styleable.TimerControl_timerColorBackground, Color.rgb(63, 63, 127));
                setColorTimer(c2);
                int c3 = a1.getColor(R.styleable.TimerControl_timerColorProgressNormal, Color.rgb(127, 127, 255));
                setColorTimerProgressNormal(c3);
//                int c4 = a.getColor(R.styleable.TimerControl_timerColorProgressWarning, Color.rgb(255, 127, 127));
                int defC4 = Color.rgb(255, 127, 127);
                if(c3 != Color.rgb(127, 127, 255)) {  // In case c3 has been set but not c4, then just use c3 for c4.
                    defC4 = c3;
                }
                int c4 = a1.getColor(R.styleable.TimerControl_timerColorProgressWarning, defC4);
                setColorTimerProgressWarning(c4);
            } finally {
                a1.recycle();
            }
            TypedArray a2 = context.getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.TimerRange,
                    0, 0);
            try {
//                // hack
//                int imin = a2.getInt(R.styleable.TimerRange_timerRangeMin, 0);
//                int imax = a2.getInt(R.styleable.TimerRange_timerRangeMax, imin + 5);       // ???
//                int ithreshold = a2.getInt(R.styleable.TimerRange_timerRangeThreshold, imin);  // ???
//                long min = imin * 1000L;
//                long max = imax * 1000L;
//                long threshold = ithreshold * 1000L;
                int min = a2.getInt(R.styleable.TimerRange_timerRangeMin, 0);
                int max = a2.getInt(R.styleable.TimerRange_timerRangeMax, min + 5000);         // ???
                int threshold = a2.getInt(R.styleable.TimerRange_timerRangeThreshold, min);  // ???
                timerRange = new TimerRange(max, min, threshold);
                // ...
            } finally {
                a2.recycle();
            }
        } else {
            // Use the default axisOrientationDependent, axis-orientation mapping, etc...
            // ...

            // ???? What's the best "default" value???
            timerRange = new TimerRange(5000L);

            // Default colors.
            setColorBackground(Color.rgb(255, 255, 255));
            setColorTimer(Color.rgb(63, 63, 127));
            setColorTimerProgressNormal(Color.rgb(127, 127, 255));
            setColorTimerProgressWarning(Color.rgb(255, 127, 127));
        }

        // ???
        setRepeats(TimerState.INITIALIZED);

        // ???
        setMax(timerRange.getMax());
        setMin(timerRange.getMin());
        // ????


        // ???
        mRectControl = new Rect(0, 0, 10, 10);
        mRectTimer = new Rect(0, 0, 10, 10);
        mRectTimerProgressBar = new Rect(0, 0, 10, 10);


    }

    // TBD: What's the best place to call this method???
    // Note: The logic in this method depends on subclass's initControl(),
    //       and hence this cannot be put in initControl or Ctors.
    //       This should be called after subclass's Ctor and initControl is called.
    private void initResources()
    {
        Log.d("initResources()");

//        // TBD
//        Resources res = getContext().getResources();

        // ???
        initTimer();
    }

    @Override
    protected void initTimer(boolean force)
    {
        // super.initTimer();

        // TBD:
        // At this point,
        // timerRanger should not be null...
        // How to enforce that????

        // TBD:
        // CountDownTimer cannot be initialized here
        //    because the ctor takes millisInFuture....

        // timerRange.reset();  // Make val == max. Here? or Below?
        if(force || countDownTimer == null) {
            timerRange.reset();  // Make val == max. Here? or Above?
            countDownTimer = new FlexibleCountDownTimer(timerRange, 100L);
            countDownTimer.addCountDownListener(this);
        }

        // ???
        if(countDownTimer == null) {
            setRepeats(TimerState.INITIALIZED);
        } else {
            // Keep the current state???
            // ????
        }
    }



    // Note:
    // Colors and timerRange, etc.
    // cannot be changed after onAttachedToWindow()...
    // -->
    // Unfortunately, this does not work...
    // This control will be likely included in the layout xml file.
    // That means the control will be initialized with default values and everything alrady...
    // We should still be able to change these attributes after the control is initialized and already displayed...
    // ....

    public final int getColorBackground()
    {
        return mColorBackground;
    }
    public void setColorBackground(int colorBackground)
    {
        if(Log.I) Log.i("colorBackground = " + colorBackground);

        this.mColorBackground = colorBackground;
        mPaintBackground = new Paint();
        mPaintBackground.setColor(mColorBackground);
        setDirty();
        // update(true);    // ???
    }

    public final int getColorTimer()
    {
        return mColorTimer;
    }
    public void setColorTimer(int colorTimer)
    {
        if(Log.I) Log.i("colorTimer = " + colorTimer);

        this.mColorTimer = colorTimer;
        mPaintTimer = new Paint();
        mPaintTimer.setColor(mColorTimer);
        setDirty();
        // update(true);    // ???
    }

    public final int getColorTimerProgressNormal()
    {
        return mColorTimerProgressNormal;
    }
    public void setColorTimerProgressNormal(int colorTimerProgress)
    {
        if(Log.I) Log.i("colorTimerProgressNormal = " + colorTimerProgress);

        this.mColorTimerProgressNormal = colorTimerProgress;
        mPaintTimerProgressNormal = new Paint();
        mPaintTimerProgressNormal.setColor(mColorTimerProgressNormal);
        setDirty();
        // update(true);    // ???
    }

    public final int getColorTimerProgressWarning()
    {
        return mColorTimerProgressWarning;
    }
    public void setColorTimerProgressWarning(int colorTimerProgress)
    {
        if(Log.I) Log.i("colorTimerProgressWarning = " + colorTimerProgress);

        this.mColorTimerProgressWarning = colorTimerProgress;
        mPaintTimerProgressWarning = new Paint();
        mPaintTimerProgressWarning.setColor(mColorTimerProgressWarning);
        setDirty();
        // update(true);    // ???
    }


    // Note:
    // timerRange min/max are really read-only...
    // whereas val is settable...
    public final TimerRange getTimerRange()
    {
        return timerRange;
    }
    public void setTimerRange(TimerRange timerRange)
    {
        this.timerRange = timerRange;
        setMax(timerRange.getMax());
        setMin(timerRange.getMin());
        // ???
        initTimer(true);
        // update(true);    // ???
        // ???
    }
    public void resetTimerRange(long max)
    {
        if(this.timerRange != null) {
            this.timerRange.reset(max);
        } else {
            // ????
            this.timerRange = new TimerRange(max);
        }
        setMax(this.timerRange.getMax());
        setMin(this.timerRange.getMin());
        // ???
        initTimer(true);
        // update(true);    // ???
        // ???
    }
    public void resetTimerRange(long max, long min)
    {
        if(this.timerRange != null) {
            this.timerRange.reset(max, min);
        } else {
            // ????
            this.timerRange = new TimerRange(max, min);
        }
        setMax(this.timerRange.getMax());
        setMin(this.timerRange.getMin());
        // ???
        initTimer(true);
        // update(true);    // ???
        // ???
    }
    public void resetTimerRange(long max, long min, long threshold)
    {
        if(this.timerRange != null) {
            this.timerRange.reset(max, min, threshold);
        } else {
            // ????
            this.timerRange = new TimerRange(max, min, threshold);
        }
        setMax(this.timerRange.getMax());
        setMin(this.timerRange.getMin());
        // ???
        initTimer(true);
        // update(true);    // ???
    }




    // TBD:
    // Other settable attributes here...
    // ...





    // TBD:
    // Timer life cycle methods....
    // ...


    @Override
    public void reset(long max)
    {
        resetTimerRange(max);
        reset();
    }
    @Override
    public void reset(long max, long min)
    {
        resetTimerRange(max, min);
        reset();
    }
    public void reset(long max, long min, long threshold)
    {
        resetTimerRange(max, min, threshold);
        reset();
    }

    public void restart(long max, long min, long threshold)
    {
        reset(max, min, threshold);
        start();
    }


    @Override
    public void start()
    {
        if(getRepeats() != TimerState.INITIALIZED) {
            // throw new IllegalStateException("Timer cannot be started when it has not been initialized.");
            Log.w("Timer cannot be started when it has not been initialized. Current state = " + getRepeats());
            return;
        }

        if(countDownTimer != null) {
            countDownTimer.start();
        } else {
            // ???
            Log.w("countDownTimer is null. Cannot startCycle().");
        }

        // TBD
        setRepeats(TimerState.RUNNING);
        // update();   // ???
    }
    @Override
    public void pause(long extent)
    {
        if(extent >= 0L) {
            // Hack. We are using this as to re-create a snapshot of the timer...
            // bypass the state check...
        } else {
            if(getRepeats() != TimerState.RUNNING) {
                // throw new IllegalStateException("Timer cannot be paused when it's not running.");
                Log.w("Timer cannot be paused when it's not running. Current state = " + getRepeats());
                return;
            }
        }

        if(countDownTimer != null) {
            countDownTimer.pause(extent);
            // reset this timerControl's val???
            //   (note: extent could be -1 at this point.)
            // the caller should take care of this before calling pause(extent)...
        } else {
            // ???
            Log.w("countDownTimer is null. Cannot pause().");
        }

        // TBD:
        setRepeats(TimerState.PAUSED);
        // update();   // ???
    }
    @Override
    public void resume(long extent)
    {
        if(extent >= 0L) {
            // Hack. We are using this to re-create a snapshot of the timer...
            // --> bypass the state check...
        } else {
            if (getRepeats() != TimerState.PAUSED) {
                // throw new IllegalStateException("Timer cannot be resumed when it has not been paused.");
                Log.w("Timer cannot be resumed when it has not been paused. Current state = " + getRepeats());
                return;
            }
        }

        if(countDownTimer != null) {
            countDownTimer.resume(extent);
            // reset this timerControl's val???
            //   (note: extent could be -1 at this point.)
            // the caller should take care of this before calling resume(extent)...
        } else {
            // ???
            Log.w("countDownTimer is null. Cannot resume().");
        }

        // TBD:
        setRepeats(TimerState.RUNNING);
        // update();   // ???
    }
    @Override
    public void stop()
    {
        if(getRepeats() != TimerState.RUNNING && getRepeats() != TimerState.PAUSED) {
            // throw new IllegalStateException("Timer cannot be stopped when it's not running.");
            Log.w("Timer cannot be stopped when it's not running or paused. Current state = " + getRepeats());
            return;
        }

        if(countDownTimer != null) {
            countDownTimer.stop();
        } else {
            // ???
            Log.w("countDownTimer is null. Cannot stopCycle().");
        }

        // TBD:
        setRepeats(TimerState.STOPPED);
        // update();   // ???
    }
    @Override
    public void finish()
    {
//        if(getRepeats() != RUNNING && getRepeats() != PAUSED && getRepeats() != STOPPED) {
//            // throw new IllegalStateException("Timer cannot be finished when it's not running/paused/stopped.");
//            Log.w("Timer cannot be finished when it's not running/paused/stopped. Current state = " + getRepeats());
//            return;
//        }

        if(countDownTimer != null) {
            countDownTimer.finish();
        } else {
            // ???
            Log.w("countDownTimer is null. Cannot finish().");
        }

        // TBD:
        setRepeats(TimerState.FINISHED);
        // update();   // ???
    }



    // CountDownListener interface.

    @Override
    public void onTick(long millisUntilFinished)
    {
        if (Log.I) Log.i("onTick(): millisUntilFinished = " + millisUntilFinished);

        // ???
        update(true);
    }

    @Override
    public void onFinish()
    {
        if (Log.I) Log.i("onFinish(): now = " + System.currentTimeMillis());

        // ???
        setRepeats(TimerState.FINISHED);
        update(true);
    }



    @Override
    protected void onAttachedToWindow()
    {
        Log.d("PlayerTimerControl.onAttachedToWindow() Called.");
        super.onAttachedToWindow();

        // TBD:
        // This needs to be fixed.
        // onAttachedToWindow() may be called before OR after onMeasure()...
        // Hence this is not a good place to call initResources()...
        initResources();
    }

//    @Override
//    protected void onDetachedFromWindow()
//    {
//        Log.d("PlayerTimerControl.onDetachedFromWindow() Called.");
//        super.onDetachedFromWindow();
//    }




    @Override
    public Parcelable onSaveInstanceState()
    {
        Log.d("PlayerTimerControl.onSaveInstanceState() Called.");

        Parcelable superState = super.onSaveInstanceState();
        SavedState savedState = new SavedState(superState);
        savedState.setTimerThreshold(getTimerRange().getThreshold());
        savedState.setTimerValue(getTimerRange().getVal());
        return savedState;
    }

    @Override
    public void onRestoreInstanceState(Parcelable state)
    {
        Log.d("PlayerTimerControl.onRestoreInstanceState() Called.");

        SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(savedState.getSuperState());
        long timerThreshold = savedState.getTimerThreshold();
        long timerValue = savedState.getTimerValue();
        // ???
        resetTimerRange(getMax(), getMin(), timerThreshold);
        // ???
        initTimer(true);
        getTimerRange().setVal(timerValue);
        long ext = timerValue - getTimerRange().getMin();
        pause(ext);   // this calls setRepeats(PAUSED)...
        // ???
    }

    protected static class SavedState extends RepeatingTimerControl.SavedState implements Parcelable
    {
        private long timerThreshold;
        private long timerValue;
        // etc...

        public SavedState(Parcelable superState)
        {
            super(superState);
        }
        public SavedState(Parcel source)
        {
            super(source);
            timerThreshold = source.readLong();
            timerValue = source.readLong();
        }


        public long getTimerThreshold()
        {
            return timerThreshold;
        }
        public void setTimerThreshold(long timerThreshold)
        {
            this.timerThreshold = timerThreshold;
        }

        public long getTimerValue()
        {
            return timerValue;
        }
        public void setTimerValue(long timerValue)
        {
            this.timerValue = timerValue;
        }


        public static final Creator<SavedState> CREATOR = new Creator<SavedState>() {
            @Override
            public SavedState createFromParcel(Parcel source)
            {
                return new SavedState(source);
            }
            @Override
            public SavedState[] newArray(int size)
            {
                return new SavedState[size];
            }
        };

        @Override
        public void writeToParcel(Parcel dest, int flags)
        {
            super.writeToParcel(dest, flags);
            dest.writeLong(timerThreshold);
            dest.writeLong(timerValue);
        }


        // For debugging...
        @Override
        public String toString()
        {
            return "SavedState{" +
                    "timerThreshold=" + timerThreshold +
                    "timerValue=" + timerValue +
                    '}';
        }
    }




    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        if(Log.I) Log.i("PlayerTimerControl.onMeasure() Called: widthMeasureSpec = " + widthMeasureSpec + "; heightMeasureSpec = " + heightMeasureSpec);
        //super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        resetOrientation();

        // ...
        int totalWidth = measureWidth(widthMeasureSpec);
        int totalHeight = measureHeight(heightMeasureSpec);

        if(Log.D) Log.d(">>>> totalWidth = " + totalWidth);
        if(Log.D) Log.d(">>>> totalHeight = " + totalHeight);


        if(getControlAxis() == ControlAxis.AXIS_HORIZONTAL) {
            mRectControl = new Rect(
                    0,
                    0,
                    mHorzControlWidth,
                    mHorzControlHeight
            );
            int dw = (int) ((mHorzControlWidth - mHorzTimerWidth) / 2);
            int dh = (int) ((mHorzControlHeight - mHorzTimerHeight) / 2);
            mRectTimer = new Rect(
                    dw,
                    dh,
                    dw + mHorzTimerWidth,
                    dh + mHorzTimerHeight
            );
//            mRectTimerProgressBar = new Rect(
//                    dw,
//                    dh,
//                    dw + (int) (mHorzTimerWidth * getTimerRange().getFraction()),
//                    dh + mHorzTimerHeight
//            );
        } else if(getControlAxis() == ControlAxis.AXIS_VERTICAL) {
            mRectControl = new Rect(
                    0,
                    0,
                    mVertControlWidth,
                    mVertControlHeight
            );
            int dw = (int) ((mVertControlWidth - mVertTimerWidth) / 2);
            int dh = (int) ((mVertControlHeight - mVertTimerHeight) / 2);
            mRectTimer = new Rect(
                    dw,
                    dh,
                    dw + mVertTimerWidth,
                    dh + mVertTimerHeight
            );
//            mRectTimerProgressBar = new Rect(
//                    dw,
//                    dh + (int) (mVertTimerHeight * (1.0f - getTimerRange().getFraction())),
//                    dw + mVertTimerWidth,
//                    dh + mVertTimerHeight
//            );
        } else {
            Log.w("Invalid control axis: " + getControlAxis());
        }

        setMeasuredDimension(totalWidth, totalHeight);
    }

    private int measureWidth(int measureSpec)
    {
        int result = 0;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        // ...
        if (specMode == MeasureSpec.EXACTLY) {
            if(getControlAxis() == ControlAxis.AXIS_HORIZONTAL) {
                mHorzControlWidth = specSize;
                mHorzTimerWidth = Math.min(Math.max(mHorzControlWidth - 2*CONTROL_PADDING,
                                HORZ_MIN_TIMER_WIDTH),
                        Math.min(mHorzControlWidth, HORZ_MAX_TIMER_WIDTH));
            } else if(getControlAxis() == ControlAxis.AXIS_VERTICAL) {
                mVertControlWidth = specSize;
                mVertTimerWidth = Math.min(Math.max(mVertControlWidth - 2*CONTROL_PADDING,
                                VERT_MIN_TIMER_WIDTH),
                        Math.min(mVertControlWidth, VERT_MAX_TIMER_WIDTH));
            } else {
                Log.w("Invalid control axis: " + getControlAxis());
            }
            result = specSize;
        } else {
            if(getControlAxis() == ControlAxis.AXIS_HORIZONTAL) {
                mHorzControlWidth = HORZ_DEF_CONTROL_WIDTH;
                mHorzTimerWidth = HORZ_DEF_TIMER_WIDTH;
                result = mHorzControlWidth;
                if (specMode == MeasureSpec.AT_MOST) {
                    result = Math.min(result, specSize);
                    mHorzControlWidth = result;
                    mHorzTimerWidth = Math.min(Math.max(mHorzControlWidth - 2*CONTROL_PADDING,
                                    HORZ_MIN_TIMER_WIDTH),
                            Math.min(mHorzControlWidth, HORZ_MAX_TIMER_WIDTH));
                }
            } else if(getControlAxis() == ControlAxis.AXIS_VERTICAL) {
                mVertControlWidth = VERT_DEF_CONTROL_WIDTH;
                mVertTimerWidth = VERT_DEF_TIMER_WIDTH;
                result = mVertControlWidth;
                if (specMode == MeasureSpec.AT_MOST) {
                    result = Math.min(result, specSize);
                    mVertControlWidth = result;
                    mVertTimerWidth = Math.min(Math.max(mVertControlWidth - 2*CONTROL_PADDING,
                                    VERT_MIN_TIMER_WIDTH),
                            Math.min(mVertControlWidth, VERT_MAX_TIMER_WIDTH));
                }
            } else {
                Log.w("Invalid control axis: " + getControlAxis());
            }
        }

        if(Log.V) Log.v("width: result = " + result);
        return result;
    }

    private int measureHeight(int measureSpec)
    {
        int result = 0;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        // ...
        if (specMode == MeasureSpec.EXACTLY) {
            if(getControlAxis() == ControlAxis.AXIS_HORIZONTAL) {
                mHorzControlHeight = specSize;
                mHorzTimerHeight = Math.min(Math.max(mHorzControlHeight - 2*CONTROL_PADDING,
                                HORZ_MIN_TIMER_HEIGHT),
                        Math.min(mHorzControlHeight, HORZ_MAX_TIMER_HEIGHT));
            } else if(getControlAxis() == ControlAxis.AXIS_VERTICAL) {
                mVertControlHeight = specSize;
                mVertTimerHeight = Math.min(Math.max(mVertControlHeight - 2*CONTROL_PADDING,
                                VERT_MIN_TIMER_HEIGHT),
                        Math.min(mVertControlHeight, VERT_MAX_TIMER_HEIGHT));
            } else {
                Log.w("Invalid control axis: " + getControlAxis());
            }
            result = specSize;
        } else {
            if(getControlAxis() == ControlAxis.AXIS_HORIZONTAL) {
                mHorzControlHeight = HORZ_DEF_CONTROL_HEIGHT;
                mHorzTimerHeight = HORZ_DEF_TIMER_HEIGHT;
                result = mHorzControlHeight;
                if (specMode == MeasureSpec.AT_MOST) {
                    result = Math.min(result, specSize);
                    mHorzControlHeight = result;
                    mHorzTimerHeight = Math.min(Math.max(mHorzControlHeight - 2*CONTROL_PADDING,
                                    HORZ_MIN_TIMER_HEIGHT),
                            Math.min(mHorzControlHeight, HORZ_MAX_TIMER_HEIGHT));
                }
            } else if(getControlAxis() == ControlAxis.AXIS_VERTICAL) {
                mVertControlHeight = VERT_DEF_CONTROL_HEIGHT;
                mVertTimerHeight = VERT_DEF_TIMER_HEIGHT;
                result = mVertControlHeight;
                if (specMode == MeasureSpec.AT_MOST) {
                    result = Math.min(result, specSize);
                    mVertControlHeight = result;
                    mVertTimerHeight = Math.min(Math.max(mVertControlHeight - 2*CONTROL_PADDING,
                                    VERT_MIN_TIMER_HEIGHT),
                            Math.min(mVertControlHeight, VERT_MAX_TIMER_HEIGHT));
                }
            } else {
                Log.w("Invalid control axis: " + getControlAxis());
            }
        }

        if(Log.V) Log.v("height: result = " + result);
        return result;
    }



//    @Override
//    protected void onLayout(boolean changed, int left, int top, int right, int bottom)
//    {
//        if(Log.D) Log.d("PlayerTimerControl.onLayout() Called: changed = " + changed + "; left = " + left + "; top = " + top + "; right = " + right + "; bottom = " + bottom);
//    }
//
//    @Override
//    protected void onSizeChanged(int w, int h, int oldw, int oldh)
//    {
//        if(Log.D) Log.d("PlayerTimerControl.onSizeChanged() Called: w = " + w + "; h = " + h + "; oldw = " + oldw + "; oldh = " + oldh);
//        super.onSizeChanged(w, h, oldw, oldh);
//    }



//    // temporary
//
//    @Override
//    protected void dispatchDraw(Canvas canvas)
//    {
//        Log.d("dispatchDraw() Called.");
//        // super.dispatchDraw(canvas);
//
//
//
//        // Background colors
//        canvas.drawRect(mRectControl, mPaintBackground);
//        canvas.drawRect(mRectTimer, mPaintTimer);
//
//        // Draw the "progress bar"
//        switch(getRepeats()) {
//            case INITIALIZED:
//            case RUNNING:
//            case PAUSED:
//            case STOPPED:
//                int dw = mRectTimer.left;
//                int dh = mRectTimer.top;
//                if(getControlAxis() == AXIS_HORIZONTAL) {
////                    int dw = (int) ((mHorzControlWidth - mHorzTimerWidth) / 2);
////                    int dh = (int) ((mHorzControlHeight - mHorzTimerHeight) / 2);
//                    mRectTimerProgressBar = new Rect(
//                            dw,
//                            dh,
//                            dw + (int) (mHorzTimerWidth * getTimerRange().getFraction()),
//                            dh + mHorzTimerHeight
//                    );
//                } else if(getControlAxis() == AXIS_VERTICAL) {
////                    int dw = (int) ((mVertControlWidth - mVertTimerWidth) / 2);
////                    int dh = (int) ((mVertControlHeight - mVertTimerHeight) / 2);
//                    mRectTimerProgressBar = new Rect(
//                            dw,
//                            dh + (int) (mVertTimerHeight * (1.0f - getTimerRange().getFraction())),
//                            dw + mVertTimerWidth,
//                            dh + mVertTimerHeight
//                    );
//                }
//                boolean isCritical = getTimerRange().isBelowThreshold();
//                if(isCritical) {
//                    canvas.drawRect(mRectTimerProgressBar, mPaintTimerProgressWarning);
//                } else {
//                    canvas.drawRect(mRectTimerProgressBar, mPaintTimerProgressNormal);
//                }
//                break;
//            default:
//                // do not draw the progress bar..
//                break;
//        }
//
//
//    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        Log.d("PlayerTimerControl.onDraw() Called.");
        // super.onDraw(canvas);

        // Background colors
        canvas.drawRect(mRectControl, mPaintBackground);
        canvas.drawRect(mRectTimer, mPaintTimer);


        // Log.w(">>>>>>>>  mRectControl = " + mRectControl);
        // Log.w(">>>>>>>>  mRectTimer = " + mRectTimer);


        // Draw the "progress bar"
        switch(getRepeats()) {
            case TimerState.INITIALIZED:
            case TimerState.RUNNING:
            case TimerState.PAUSED:
            case TimerState.STOPPED:
                int dw = mRectTimer.left;
                int dh = mRectTimer.top;
                if(getControlAxis() == ControlAxis.AXIS_HORIZONTAL) {
//                    int dw = (int) ((mHorzControlWidth - mHorzTimerWidth) / 2);
//                    int dh = (int) ((mHorzControlHeight - mHorzTimerHeight) / 2);
                    mRectTimerProgressBar = new Rect(
                            dw,
                            dh,
                            dw + (int) (mHorzTimerWidth * getTimerRange().getFraction()),
                            dh + mHorzTimerHeight
                    );
                } else if(getControlAxis() == ControlAxis.AXIS_VERTICAL) {
//                    int dw = (int) ((mVertControlWidth - mVertTimerWidth) / 2);
//                    int dh = (int) ((mVertControlHeight - mVertTimerHeight) / 2);
                    mRectTimerProgressBar = new Rect(
                            dw,
                            dh + (int) (mVertTimerHeight * (1.0f - getTimerRange().getFraction())),
                            dw + mVertTimerWidth,
                            dh + mVertTimerHeight
                    );
                }
                boolean isCritical = getTimerRange().isBelowThreshold();
                if(isCritical) {
                    canvas.drawRect(mRectTimerProgressBar, mPaintTimerProgressWarning);
                } else {
                    canvas.drawRect(mRectTimerProgressBar, mPaintTimerProgressNormal);
                }
                break;
            default:
                // do not draw the progress bar..
                break;
        }
    }


    @Override
    protected void onConfigurationChanged(Configuration newConfig)
    {
        Log.d("PlayerTimerControl.onConfigurationChanged() Called.");
        super.onConfigurationChanged(newConfig);

//        newConfig.orientation

        // ???
        pause();

    }

}

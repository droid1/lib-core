package ms.airy.lib.core.phaser.state;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import ms.airy.lib.core.phaser.common.Feature;
import ms.airy.lib.core.phaser.common.State;
import ms.airy.lib.core.phaser.feature.FeatureHelper;


// TBD:
// FeatureMap data should be stored to/from persistent storage such as config.
// Just using a singleton may not be sufficient.....
public final class StateHelper
{
    // Map of feature code -> { state bit mask }.
    // FeatureMap is open ended. That is,
    // Features not in this map are supported for all states.
    private final Map<String, Long> featureMap;

    public StateHelper()
    {
        featureMap = new HashMap<String, Long>();
        init();
    }
    private void init()
    {
        // TBD:
        // Populate the featureMap....
        // ....

//        Set<Feature> features = FeatureHelper.getInstance().getFeatures();
//        if(features != null && !features.isEmpty()) {
//            for(Feature f : features) {
//                featureMap.put(f.getCode(), f.getState());
//            }
//        }
    }


    // Singleton
    private static final class StateHelperHolder
    {
        private static final StateHelper INSTANCE = new StateHelper();
    }
    public static StateHelper getInstance()
    {
        return StateHelperHolder.INSTANCE;
    }


    // add or update...
    public void addFeature(String featureCode, long state)
    {
        featureMap.put(featureCode, state);
    }


    public long getState(String featureCode)
    {
        if(! featureMap.containsKey(featureCode)) {
            return State.STATE_NONE;
        } else {
            return featureMap.get(featureCode);
        }
    }


    // TBD: Need to double-check this logic...
    public boolean isFeatureAvailable(String featureCode, long state)
    {
        if(! featureMap.containsKey(featureCode)) {
            return true;
        }
        long featureState = getState(featureCode);
        if(featureState == State.STATE_NONE ||
                (featureState & state) == featureState ) {
            return true;
        } else {
            return false;
        }
    }


}

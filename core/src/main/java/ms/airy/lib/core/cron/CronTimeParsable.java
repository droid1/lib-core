package ms.airy.lib.core.cron;


public interface CronTimeParsable
{
    boolean isParsed();

    Long getNextCronTime();
    Long getNextCronTime(long now);

    Long getPreviousCronTime();
    Long getPreviousCronTime(long now);
}

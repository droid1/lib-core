package ms.airy.lib.core.config.xml;

import android.content.Context;
import android.content.res.Resources;

import ms.airy.lib.core.config.AbstractConfigManager;


// For XML with name/value properties,
// Just using res/values resource files is the easiest...
// For XML files with more complex structure,
// put it under /res/xml and parse it.
// ....


// TBD:
public abstract class XmlConfigManager extends AbstractConfigManager
{
    // TBD:
    // ....

    public XmlConfigManager(Context context)
    {
        super(context);
    }

}


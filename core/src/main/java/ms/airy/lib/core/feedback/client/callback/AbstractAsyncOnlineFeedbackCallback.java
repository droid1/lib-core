package ms.airy.lib.core.feedback.client.callback;

import ms.airy.lib.core.feedback.client.AsyncFeedbackCallback;

/**
 */
public abstract class AbstractAsyncOnlineFeedbackCallback implements AsyncFeedbackCallback
{
    @Override
    public void onSendFeedback(long surveyEntryId, int answer, String comment)
    {

    }
}

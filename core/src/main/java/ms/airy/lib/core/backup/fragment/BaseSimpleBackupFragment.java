package ms.airy.lib.core.backup.fragment;

import android.app.Fragment;
import android.app.backup.BackupManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ms.airy.lib.core.backup.base.BaseBackupAgentHelper;


// TBD:
// ....
public class BaseSimpleBackupFragment extends Fragment
{

    public BaseSimpleBackupFragment()
    {
    }


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // tbd
        setRetainInstance(true);
        // ...
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return null;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
    }


    @Override
    public void onStart()
    {
        super.onStart();
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }

    @Override
    public void onPause()
    {
        super.onPause();
    }

    @Override
    public void onStop()
    {
        super.onStop();
    }

    @Override
    public void onDestroy()
    {
        // TBD:
        BackupManager manager = new BackupManager(getActivity());   // ???
        manager.dataChanged();
        // ...

        super.onDestroy();
    }
}

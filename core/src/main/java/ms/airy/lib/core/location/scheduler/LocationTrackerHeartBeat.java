package ms.airy.lib.core.location.scheduler;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import java.util.Date;

import ms.airy.lib.core.cron.MultiCronHelper;
import ms.airy.lib.core.location.common.LocationTrackerConsts;


// This is used to trigger regular location tracking in slow scale,
//      maybe, every hour or so....
public final class LocationTrackerHeartBeat
{
    private static LocationTrackerHeartBeat INSTANCE = null;
    public static LocationTrackerHeartBeat getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new LocationTrackerHeartBeat(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context.
    private final Context context;
    // AlarmManager. ??? Is it safe to hold on to the reference???
    private final AlarmManager alarmManager;

    private LocationTrackerHeartBeat(Context context)
    {
        this.context = context;
        this.alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    }


    // Note that
    // We are scheduling location tracking for a certain time duration,
    //    not just once.
    // At a scheduled time,
    //    the location listener is enabled,
    //    and it is removed at a certain later time (determined by other params, user prefs, etc.)...
    // ...

    // Note:
    // jitter, if > 0, is used to set the "window", (futureTime, futureTime + jitter).
    // Note that jitter is non-negative.
    // If negative/advanced jitter is needed, modify the futureTime.



    // TBD:

    public void scheduleLocationTracking(long futureTime)
    {
        scheduleLocationTracking(futureTime, 0L);   // No jitter? use default (random) jitter?
    }
    public void scheduleLocationTracking(long futureTime, long jitter)
    {
        if(Log.I) Log.i(">>>>>>>>> scheduleLocationTracking(). futureTime = " + futureTime + "; jitter = " + jitter);


        // Is this necessary???
        long now = System.currentTimeMillis();
        if(futureTime < now) {
            // ???
            Log.w("Location time is in the past: futureTime = " + futureTime);
            return;
        }
        // ???


        // TBD:
        // This is necessary to support multiple alarms (e.g., repeated locations from a cron.)
        // If you use the same requestCode, you'll see only one alarm (the last one).
        // We also tie the requestCode with the futureTime so that
        //    if multiple requests (e.g., with repeated notifs) are made,
        //    then it might help avoid duplicate alarms.
        // (Note that the location data is "shared" between all different modules in an app.
        // And (in the future) it may even be shared among different apps develeped by us....
        //  --> this possibly means that only one heartbeat/scheduler should run.....
        //      Or, at least, we should not create duplicate alarms (to be run at the same time)....)
        int requestCode = (int) (futureTime / 1000L);
        // ...

        // Location intent, to be sent to the broadcast receiver...
        Intent intent = new Intent(LocationTrackerConsts.ACTION_SCHEDULED_LOCATION_TRACKING);


        // PendingIntent broadcastIntent = PendingIntent.getBroadcast(context, requestCode, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        PendingIntent broadcastIntent = PendingIntent.getBroadcast(context, requestCode, intent, 0);

        // TBD:
        // This works only if target sdk >= 19.
//        if(jitter == 0L) {
//            this.alarmManager.setExact(AlarmManager.RTC, futureTime, broadcastIntent);
//        } else {
//            this.alarmManager.setWindow(AlarmManager.RTC, futureTime, jitter, broadcastIntent);
//        }
        // ???
        //this.alarmManager.set(AlarmManager.RTC, futureTime, broadcastIntent);
        this.alarmManager.set(AlarmManager.RTC_WAKEUP, futureTime, broadcastIntent);
        if(Log.I) Log.i(">>>>>>>>>>>>> Alarm set for location tracking: futureTime = " + new Date(futureTime).toString());
        // TBD:
    }


    // temporary
    // If we use FASTEST, there will be ~100 alarms per day...
    // ....
    // Using repeat count is not the best way to implement this...
    // --> How to do this????
    private static final int MAX_CRON_REPEATS = 500;

    // This can be a problem when repeats is a big number...
    // --> Run this in a separate thread????
    public void scheduleLocationTracking(String cronExpression)
    {
        scheduleLocationTracking(cronExpression, 0L);
    }
    public void scheduleLocationTracking(String cronExpression, long jitter)
    {
        scheduleLocationTracking(cronExpression, jitter, MAX_CRON_REPEATS);
    }
    public void scheduleLocationTracking(String cronExpression, long jitter, int repeats)
    {
        if(Log.I) Log.i(">>>>>>>>> scheduleLocationTracking(). cronExpression = " + cronExpression + "; jitter = " + jitter + "; repeats = " + repeats);


        long nextTime = System.currentTimeMillis();
        for(int i=0; i<repeats; i++) {
            Log.i("i = " + i);

            nextTime = MultiCronHelper.getInstance().getNextCronTime(cronExpression, nextTime + 1000L);

            if(Log.I) Log.i(">>>>>>>>> scheduleLocationTracking(). nextTime = " + nextTime);

            scheduleLocationTracking(nextTime, jitter);
        }
    }



    public void scheduleLocationProcessing(long pollingInterval)
    {
        scheduleLocationProcessing(pollingInterval, false);
    }
    public void scheduleLocationProcessing(long pollingInterval, boolean exact)
    {
        if (Log.I) Log.i(">>>>>>>>> scheduleLocationProcessing(). pollingInterval = " + pollingInterval + "; exact = " + exact);

        // ????
        int requestCode = (int) pollingInterval;
        // ...

        // Location intent, to be sent to the broadcast receiver...
        Intent intent = new Intent(LocationTrackerConsts.ACTION_SCHEDULED_LOCATION_PROCESSING);
        PendingIntent broadcastIntent = PendingIntent.getBroadcast(context, requestCode, intent, 0);

        // tbd:
        // AlarmManager behavior has changed.
        // Need to verify this, maybe after we set minSdk to 19 or so....
        long now = System.currentTimeMillis();
        if(exact) {
            this.alarmManager.setRepeating(AlarmManager.RTC, now + pollingInterval, pollingInterval, broadcastIntent);
        } else {
            this.alarmManager.setInexactRepeating(AlarmManager.RTC, now + pollingInterval, pollingInterval, broadcastIntent);
        }

        if(Log.I) Log.i(">>>>>>>>>>>>> Repeating alarm set for location processing: pollingInterval = " + pollingInterval);
    }



    // TBD:

    public void cancelLocationTracking(long futureTime)
    {
        // ???
        int requestCode = (int) (futureTime / 1000L);
        Intent intent = new Intent(LocationTrackerConsts.ACTION_SCHEDULED_LOCATION_TRACKING);
        PendingIntent cancelAlarmIntent = PendingIntent.getBroadcast(context, requestCode, intent, 0);
        this.alarmManager.cancel(cancelAlarmIntent);
    }
    // TBD: Does this work???
    public void cancelLocationTracking(String cronExpression, int repeats)
    {
        if(Log.I) Log.i(">>>>>>>>> cancelLocationTracking(). cronExpression = " + cronExpression + "; repeats = " + repeats);

        long nextTime = System.currentTimeMillis();
        for(int i=0; i<repeats; i++) {
            nextTime = MultiCronHelper.getInstance().getNextCronTime(cronExpression, nextTime + 1000L);
            if(Log.I) Log.i(">>>>>>>>> cancelLocationTracking(). i = " + i + "; nextTime = " + nextTime);
            cancelLocationTracking(nextTime);
        }
    }


}

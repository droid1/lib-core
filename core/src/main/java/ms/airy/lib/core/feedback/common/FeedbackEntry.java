package ms.airy.lib.core.feedback.common;


import java.io.Serializable;

/**
 * It corresponds to an answer to a question in questionnaire.
 */
public class FeedbackEntry implements Serializable
{
    // Essentially, every field can be null/empty.
    // however, at least one of answer and comment should be filled in.
    private long surveyEntryId; // 0L means it is not an answer to a specific survey question. E.g., general feedback comment, or bug report, etc.
    private String title;       // optional. copy of surveyEntry.title.
    private String question;    // can be null. copy of suervyeEntry.question.
    private int answer;         // -1 is not a valid choice.
    private String comment;
    private long createdTime;
    // private long postedTime;


    public FeedbackEntry()
    {
        this(null);
    }
    public FeedbackEntry(SurveyEntry surveyEntry)
    {
        this(surveyEntry, -1, null);
    }
    public FeedbackEntry(SurveyEntry surveyEntry, int answer, String comment)
    {
        if(surveyEntry != null) {
            this.surveyEntryId = surveyEntry.getId();
            this.title = surveyEntry.getTitle();
            this.question = surveyEntry.getQuestion();
        }
        this.answer = answer;
        this.comment = comment;
        this.createdTime = System.currentTimeMillis();   // ???
    }

    //    // Creates a template for the given surveyEntry.
//    public static FeedbackEntry newInstance(SurveyEntry surveyEntry)
//    {
//        FeedbackEntry feedbackEntry = new FeedbackEntry(surveyEntry);
//        return feedbackEntry;
//    }


    public long getSurveyEntryId()
    {
        return surveyEntryId;
    }
    public void setSurveyEntryId(long surveyEntryId)
    {
        this.surveyEntryId = surveyEntryId;
    }

    public String getTitle()
    {
        return title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getQuestion()
    {
        return question;
    }
    public void setQuestion(String question)
    {
        this.question = question;
    }

    public int getAnswer()
    {
        return answer;
    }
    public void setAnswer(int answer)
    {
        this.answer = answer;
    }

    public String getComment()
    {
        return comment;
    }
    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public long getCreatedTime()
    {
        return createdTime;
    }
    public void setCreatedTime(long createdTime)
    {
        this.createdTime = createdTime;
    }

    // For debugging...
    @Override
    public String toString()
    {
        return "FeedbackEntry{" +
                "surveyEntryId=" + surveyEntryId +
                ", title='" + title + '\'' +
                ", question='" + question + '\'' +
                ", answer=" + answer +
                ", comment='" + comment + '\'' +
                ", createdTime=" + createdTime +
                '}';
    }

}

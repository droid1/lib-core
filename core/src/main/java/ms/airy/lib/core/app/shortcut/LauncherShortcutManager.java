package ms.airy.lib.core.app.shortcut;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Parcelable;

import ms.airy.lib.core.R;
import ms.airy.lib.core.startup.StartupManager;


// TBD:
public final class LauncherShortcutManager
{
    private static LauncherShortcutManager INSTANCE = null;

    public static LauncherShortcutManager getInstance(Context context)
    {
        if (INSTANCE == null) {
            INSTANCE = new LauncherShortcutManager(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;
    private LauncherShortcutManager(Context context)
    {
        this.context = context;
    }


    // launcherActivity should be a Launcher activity.
    public void createShortcut(int shortcutNameResId, int iconResId, Class<?> launcherActivity)
    {
        String shortcutName = context.getResources().getString(shortcutNameResId);
        if(shortcutName != null && !shortcutName.isEmpty()) {
            createShortcut(shortcutName, iconResId, launcherActivity);
        } else {
            if(Log.I) Log.i("Failed to find the shortcutName for shortcutNameResId = " + shortcutNameResId);
        }
    }
    public void createShortcut(String shortcutName, int iconResId, Class<?> launcherActivity)
    {
        Intent shortcutIntent = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
        shortcutIntent.putExtra("duplicate", false);
        shortcutIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, shortcutName);
        Parcelable icon = Intent.ShortcutIconResource.fromContext(context, iconResId);
        shortcutIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, icon);
        shortcutIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, new Intent(context, launcherActivity));
        context.sendBroadcast(shortcutIntent);
    }

    // If it is the first run after installation
    public boolean createShortcutUponInstallation(int shortcutNameResId, int iconResId, Class<?> launcherActivity)
    {
        String shortcutName = context.getResources().getString(shortcutNameResId);
        if(shortcutName != null && !shortcutName.isEmpty()) {
            return createShortcutUponInstallation(shortcutName, iconResId, launcherActivity);
        } else {
            if(Log.I) Log.i("Failed to find the shortcutName for shortcutNameResId = " + shortcutNameResId);
            return false;
        }
    }
    public boolean createShortcutUponInstallation(String shortcutName, int iconResId, Class<?> launcherActivity)
    {
        return createShortcutUponInstallation(shortcutName, iconResId, launcherActivity, false);
    }
    public boolean createShortcutUponInstallation(int shortcutNameResId, int iconResId, Class<?> launcherActivity, boolean updateStartupTime)
    {
        String shortcutName = context.getResources().getString(shortcutNameResId);
        if(shortcutName != null && !shortcutName.isEmpty()) {
            return createShortcutUponInstallation(shortcutName, iconResId, launcherActivity, updateStartupTime);
        } else {
            if(Log.I) Log.i("Failed to find the shortcutName for shortcutNameResId = " + shortcutNameResId);
            return false;
        }
    }
    public boolean createShortcutUponInstallation(String shortcutName, int iconResId, Class<?> launcherActivity, boolean updateStartupTime)
    {
        if(StartupManager.getInstance(context).isFirstStartup(updateStartupTime)) {
            Log.i("Attempting to create an app shortcut.");
            createShortcut(shortcutName, iconResId, launcherActivity);
            return true;
        } else {
            Log.i("App shortcut will not be created since it is not the first launch.");
            return false;
        }
    }

    // If it is the first launch for the version.
    public boolean createShortcutIfFirstRun(int shortcutNameResId, int iconResId, Class<?> launcherActivity)
    {
        String shortcutName = context.getResources().getString(shortcutNameResId);
        if(shortcutName != null && !shortcutName.isEmpty()) {
            return createShortcutIfFirstRun(shortcutName, iconResId, launcherActivity);
        } else {
            if(Log.I) Log.i("Failed to find the shortcutName for shortcutNameResId = " + shortcutNameResId);
            return false;
        }
    }
    public boolean createShortcutIfFirstRun(String shortcutName, int iconResId, Class<?> launcherActivity)
    {
        return createShortcutIfFirstRun(shortcutName, iconResId, launcherActivity, false);
    }
    public boolean createShortcutIfFirstRun(int shortcutNameResId, int iconResId, Class<?> launcherActivity, boolean updateStartupCounter)
    {
        String shortcutName = context.getResources().getString(shortcutNameResId);
        if(shortcutName != null && !shortcutName.isEmpty()) {
            return createShortcutIfFirstRun(shortcutName, iconResId, launcherActivity, updateStartupCounter);
        } else {
            if(Log.I) Log.i("Failed to find the shortcutName for shortcutNameResId = " + shortcutNameResId);
            return false;
        }
    }
    public boolean createShortcutIfFirstRun(String shortcutName, int iconResId, Class<?> launcherActivity, boolean updateStartupCounter)
    {
        if(StartupManager.getInstance(context).isFirstStartupForVersion(updateStartupCounter)) {
            Log.i("Attempting to create an app shortcut.");
            createShortcut(shortcutName, iconResId, launcherActivity);
            return true;
        } else {
            if(Log.I) Log.i("App shortcut will not be created since it is not the first startup for the version.");
            return false;
        }
    }

}

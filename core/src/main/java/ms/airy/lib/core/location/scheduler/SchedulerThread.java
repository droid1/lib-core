package ms.airy.lib.core.location.scheduler;

import android.content.Context;
import android.os.HandlerThread;
import android.os.Looper;

import ms.airy.lib.core.location.prefs.LocationPrefsStoreManager;
import ms.airy.lib.core.location.tracking.LocationTrackingProcessor;
import ms.airy.lib.core.location.handler.LocationTrackingHandler;


/**
*/
public class SchedulerThread extends HandlerThread
{
    // TBD:
    // Is it safe to keep the reference to the handler?
    private LocationTrackingHandler locationHandler = null;

    private final Context context;
    public SchedulerThread(Context context, String name)
    {
        super(name);
        this.context = context.getApplicationContext();
    }

    // TBD:
    public void scheduleLocationTracking(long futureTime)
    {
        // ???
        // "Queue" messages ???
        // ...
    }

    @Override
    public synchronized void start()
    {
        super.start();

        // TBD:
        // Move this at the beginning of run() ???
        Looper looper = this.getLooper();
        if(looper != null) {
            locationHandler = new LocationTrackingHandler(context, looper);
//            locationHandler.addLocationEventCallback(LocationTrackingProcessor.getInstance(context));


            // ???
            int trackingFrequency = LocationPrefsStoreManager.getInstance(context).getLocationTrackingFrequency();

            long now = System.currentTimeMillis();

            // tbd:
            long delay = 0L;

            locationHandler.scheduleDelayedLocationTracking(trackingFrequency, delay);


        } else {
            // ????
            Log.w("Looper is null. Cannot proceed.");
        }
    }

    @Override
    public void run()
    {
        if(locationHandler == null) {
            // ???
            Log.w("locationHandler is null.");
            // return;   // ???  --> It seems like we cannot return without calling super.run().
        }

        super.run();
    }

}

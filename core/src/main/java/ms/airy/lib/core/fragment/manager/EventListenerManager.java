package ms.airy.lib.core.fragment.manager;


import android.app.Fragment;
import android.view.KeyEvent;

import ms.airy.lib.core.fragment.listener.KeyDownEventListener;

import java.util.ArrayList;
import java.util.List;


// Convenience class.
// Activities that delegate certain event handling to child fragments
//   need to implement some common code.
// EventListenerManager encapsulates this common code.
public abstract class EventListenerManager
{
    // Ordered list.
    private final List<Fragment> fragments = new ArrayList<Fragment>();


    public EventListenerManager()
    {
    }

    protected List<Fragment> getFragments()
    {
        return fragments;
    }

    // Mote that the order is important.
    public void addFragment(Fragment fragment)
    {
        fragments.add(fragment);
    }

    // etc...


}

package ms.airy.lib.core.status.client.impl;

import ms.airy.lib.core.status.client.StatusMessageSendClient;
import ms.airy.lib.core.status.common.StatusMessage;


/**
 */
public class MockStatusMessageSendClient implements StatusMessageSendClient
{

    @Override
    public long sendMessage(int type, String message)
    {
        if(Log.I) Log.i("MockStatusMessageSendClient.sendMessage() type = " + type + "; message = " + message);

        return 0;
    }

    @Override
    public void sendStatusMessage(StatusMessage statusMessage)
    {
        if(Log.I) Log.i("MockStatusMessageSendClient.sendStatusMessage() statusMessage = " + statusMessage);

    }
}

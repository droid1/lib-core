package ms.airy.lib.core.util;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;


public final class RandomUtil
{
    // Prevents instantiation...
    private RandomUtil() {}

    // TBD...
    private static Random sRandom = null;
    private static Random getRandom()
    {
        if(sRandom == null) {
            sRandom = new Random(System.currentTimeMillis());
        }
        return sRandom;
    }


    // ...

    // int range: [0, range-1].
    public static int[] pickRandomInts(int size, int range)
    {
        return pickRandomInts(size, range, false);
    }
    public static int[] pickRandomInts(int size, int range, boolean allowDuplicates)
    {
        return pickRandomInts(size, range, allowDuplicates, true);
    }
    public static int[] pickRandomInts(int size, int range, boolean allowDuplicates, boolean ordered)
    {
        if(size <= 0 || range <= 0) {
            if(Log.I) Log.i("Invalid size or range. size = " + size + "; range = " + range + "; allowDuplicates = " + allowDuplicates);
            return null;
        }
        if(size > range && allowDuplicates == false) {
            if(Log.I) Log.i("Invalid size or range. size = " + size + "; range = " + range + "; allowDuplicates = " + allowDuplicates);
            return null;
        }

        int[] picked = new int[size];
        if(size == 1) {
            int r = getRandom().nextInt(range);
            picked[0] = r;
            return picked;
        }
        if(size == range && allowDuplicates == false && ordered == true) {
            for(int i=0; i<size; i++) {
                picked[i] = i;
            }
            return picked;
        }

        // temporary
        final int MAX_LOOP = size * 10 + 100;  // arbitrary.
        Set<Integer> alreadyPicked = new HashSet<Integer>();
        int pickCtr = 0;
        int loopCtr = 0;
        while(++loopCtr <= MAX_LOOP && pickCtr < size) {
            int r = getRandom().nextInt(range);
            if(allowDuplicates == false && alreadyPicked.contains(r)) {
                if(Log.I) Log.i("Random number r already included: " + r);
                continue;
            }
            picked[pickCtr++] = r;
        }
        if(ordered == true) {
            Arrays.sort(picked);
        }

        return picked;
    }


}

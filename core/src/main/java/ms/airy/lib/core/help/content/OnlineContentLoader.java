package ms.airy.lib.core.help.content;


import android.content.Context;

import ms.airy.lib.core.help.client.AsyncHelpCallback;
import ms.airy.lib.core.help.client.AsyncHelpClient;
import ms.airy.lib.core.help.client.async.AsyncOnlineHelpClient;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


/**
 * Remote help content loader.
 * If the help content is found for a given section ID,
 * it overwrites, if any, the local help content.
 */
public class OnlineContentLoader implements ContentLoader, AsyncHelpCallback
{
    // Note:
    // Although this is implemented as a singleton,
    // it is somewhat unusual in that the singleton has a dependency on the web services URL.
    // At any given moment, you can have only one web services URL,
    // but it can be changed over time.
    // Whenever the URL changes, the "cache" is cleard.
    private static OnlineContentLoader INSTANCE = null;
    public static OnlineContentLoader getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new OnlineContentLoader(context.getApplicationContext());
        }
        return INSTANCE;
    }


    // We use the application context.
    private final Context context;
    // To be set by the client.
    // This must be set (e.g., via ctor, or via setter)
    // before OnlineContentLoader can be used.
    private String helpWebServiceEndpoint = null;

    protected OnlineContentLoader(Context context)
    {
        this(context, null);
    }
    protected OnlineContentLoader(Context context, String helpWebServiceEndpoint)
    {
        this.context = context;
        this.helpWebServiceEndpoint = helpWebServiceEndpoint;
    }


    protected Context getContext()
    {
        return this.context;
    }

    public String getHelpWebServiceEndpoint()
    {
        return helpWebServiceEndpoint;
    }
    public void setHelpWebServiceEndpoint(String helpWebServiceEndpoint)
    {
        this.helpWebServiceEndpoint = helpWebServiceEndpoint;
        contentCache.clear();
    }


    // "Cache"
    private final Map<Long, String> contentCache = new HashMap<Long, String>();

    @Override
    public String getContent(long sectionId)
    {
//        // TBD:
//        if(helpWebServiceEndpoint == null) {
//            Log.w("helpWebServiceEndpoint is null. Help content cannot be fetched.");
//            return null;    // ????
//        }

        String content = contentCache.get(sectionId);
        if(content != null && !content.isEmpty()) {
            return content;
        }
        // ????
        // Async load the content.
        loadOnlineContent(sectionId);
        // ....
        return null;  // ????
    }

    // To be called by the ContentLoaderCallback classes.
    protected void addContentToCache(long sectionId, String content)
    {
        if(content != null && !content.isEmpty()) {
            contentCache.put(sectionId, content);
        } else {
            // ?????
            Log.w("Invalid content for sectionId = " + sectionId);
        }
    }

    // TBD:
    // Load the online content asynchronously....
    public void loadOnlineContent(long sectionId)
    {
        // TBD:
        // Set client.helpWebServiceEndpoint
        // ...
        AsyncHelpClient client = new AsyncOnlineHelpClient(this);
        client.getSectionContent(sectionId);
    }

    // ????
    public void prefetchOnlineContent(long[] ids)
    {
        // Async load all sections for the given id list.
        if(ids != null && ids.length > 0) {
            for(long id : ids) {
                loadOnlineContent(id);
            }
        }
    }


    ////////////////////////////////////////////////////
    // AsyncHelpCallback interface

    @Override
    public void onGetSectionContent(long sectionId, String content)
    {
        addContentToCache(sectionId, content);
        for(ContentLoaderCallback callback : contentLoaderCallbacks) {
            callback.onOnlineContentLoaded(sectionId, content);
        }
    }



    ////////////////////////////////////////////////////
    // For handling ContentLoaderCallback

    private final Set<ContentLoaderCallback> contentLoaderCallbacks = new HashSet<ContentLoaderCallback>();

    public void addContentLoaderCallback(ContentLoaderCallback callback)
    {
        contentLoaderCallbacks.add(callback);
    }


}

package ms.airy.lib.core.fragment.listener;

import android.view.KeyEvent;


// This is needed to "relay" the parent activity's event to its child fragments.
public interface KeyMultipleEventListener
{
    boolean onKeyMultiple(int keyCode, int repeatCount, KeyEvent event);

}

package ms.airy.lib.core.help.manager;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;

import ms.airy.lib.core.dialog.ActionableAssertDialogListener;
import ms.airy.lib.core.dialog.stock.SimpleWebViewDialogFragment;
import ms.airy.lib.core.help.common.HelpSection;


public final class HelpManager implements ActionableAssertDialogListener
{
    // temporary
    private static final String DEF_HELP_DIALOG_TITLE = "Help";

    private static HelpManager INSTANCE = null;

    public static HelpManager getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new HelpManager(context);
        }
        return INSTANCE;
    }

    // Unfortunately, we cannot keep activity here.
    // It'll become stale.
    // Either we make HelpManager non-singleton and instantiate in onCreate() every time,
    //      or, pass the relevant activity in showHelp(), etc...
    // We choose the latter option...
//    // This is needed to get the fragment manager.
//    private final Activity activity;
    // We use the application context.
    private final Context context;

    // This is always true, for now.
    private boolean useDialogWebView;

    private HelpManager(Context context)
    {
//        // Note that, when we call showHelp(), the context should be Activity.
//        // Hence this if() should always be true.
//        if(context instanceof Activity) {
//            this.activity = (Activity) context;
//        } else {
//            this.activity = null;
//        }
        this.context = context.getApplicationContext();
        useDialogWebView = true;
    }



    // TBD:Map<Long, String> getSectionTitleMap()
    // Show help in web view
    // ...


    public boolean showHelp(Activity activity, long sectionId)
    {
        return showHelp(activity, sectionId, null);
    }
//    public boolean showHelp(FragmentManager fragmentManager, long sectionId)
//    {
//        return showHelp(fragmentManager, sectionId, null);
//    }
    public boolean showHelp(Activity activity, long sectionId, String title)
//    public boolean showHelp(FragmentManager fragmentManager, long sectionId, String title)
    {
        HelpSection section = HelpRegistry.getInstance(context).getSection(sectionId, title);

        if(section == null) {
            return false;  // ???
        }

        String sectionTitle = section.getTitle();
        if(sectionTitle == null) {   // is empty title allowed???
            sectionTitle = DEF_HELP_DIALOG_TITLE;
        }
        DialogFragment fragment1 = SimpleWebViewDialogFragment.newInstance(
                sectionTitle,
                0,
                section.getContent(),
                null,
                "",    // Exclude the negative button.
                null
        ).addActionable(this);

        // fragment1.getFragmentManager() seems to return null... Why not??
//        fragment1.show(fragment1.getFragmentManager(), "help_dialog");

//        if(this.activity == null) {
//            return false;
//        }
//        fragment1.show(this.activity.getFragmentManager(), "help_dialog");

        fragment1.show(activity.getFragmentManager(), "help_dialog");
//        fragment1.show(fragmentManager, "help_dialog");
        return true;
    }


    ////////////////////////////////////////////
    // ActionableAssertDialogListener interface

    @Override
    public void onPositiveClick(String tag)
    {
        if(Log.I) Log.i("HelpManager.onPositiveClick() tag = " + tag);

    }

    @Override
    public void onNegativeClick(String tag)
    {
        if(Log.I) Log.i("HelpManager.onNegativeClick() tag = " + tag);

    }

    @Override
    public void onNeutralClick(String tag)
    {
        if(Log.I) Log.i("HelpManager.onNeutralClick() tag = " + tag);

    }

}

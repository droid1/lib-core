package ms.airy.lib.core.status.client.factory;

import ms.airy.lib.core.status.client.StatusMessageFetchClient;
import ms.airy.lib.core.status.client.StatusMessageSendClient;
import ms.airy.lib.core.status.client.impl.MiniStatusMessageFetchClient;
import ms.airy.lib.core.status.client.impl.MiniStatusMessageSendClient;
import ms.airy.lib.core.status.client.impl.MockStatusMessageFetchClient;
import ms.airy.lib.core.status.client.impl.MockStatusMessageSendClient;


/**
 */
public class StatusClientFactory
{
    // temporary
    private static final int CLIENT_MOCK = 1;
    private static final int CLIENT_MINI = 2;
    // ...

    private static StatusClientFactory INSTANCE = null;

    public static StatusClientFactory getInstance()
    {
        if(INSTANCE == null) {
            INSTANCE = new StatusClientFactory();
        }
        return INSTANCE;
    }

    // mock or mini
    private int type;

    private StatusMessageSendClient sendClient = null;
    private StatusMessageFetchClient fetchClient = null;
    private StatusClientFactory()
    {
        // TBD:
        type = CLIENT_MOCK;
        // ...
    }

    public void useMockClient()
    {
        type = CLIENT_MOCK;
        sendClient = null;
        fetchClient = null;
    }
    public void useMiniClient()
    {
        type = CLIENT_MINI;
        sendClient = null;
        fetchClient = null;
    }



    public StatusMessageSendClient getSendClient()
    {
        if(sendClient == null) {
            switch(type) {
                case CLIENT_MOCK:
                default:
                    sendClient = new MockStatusMessageSendClient();
                    break;
                case CLIENT_MINI:
                    sendClient = new MiniStatusMessageSendClient();
                    break;
            }
        }
        return sendClient;
    }

    public StatusMessageFetchClient getFetchClient()
    {
        if(fetchClient == null) {
            switch(type) {
                case CLIENT_MOCK:
                default:
                    fetchClient = new MockStatusMessageFetchClient();
                    break;
                case CLIENT_MINI:
                    fetchClient = new MiniStatusMessageFetchClient();
                    break;
            }
        }
        return fetchClient;
    }


}

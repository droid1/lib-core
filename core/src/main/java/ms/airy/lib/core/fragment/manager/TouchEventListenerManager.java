package ms.airy.lib.core.fragment.manager;

import android.app.Fragment;
import android.view.KeyEvent;
import android.view.MotionEvent;

import ms.airy.lib.core.fragment.listener.KeyDownEventListener;
import ms.airy.lib.core.fragment.listener.KeyLongPressEventListener;
import ms.airy.lib.core.fragment.listener.KeyMultipleEventListener;
import ms.airy.lib.core.fragment.listener.KeyShortcutEventListener;
import ms.airy.lib.core.fragment.listener.KeyUpEventListener;
import ms.airy.lib.core.fragment.listener.TouchEventListener;


public final class TouchEventListenerManager extends EventListenerManager
{

    public TouchEventListenerManager()
    {
    }

    // Returning false means the parent activity still need to handle the event.
    // Returning true means that the parent activity need not, and should not,
    //    further handle the event.
    // If at least one of the fragments return true, it returns true
    //    without processing the rest of the fragments.

    public boolean handleTouchEvent(MotionEvent event)
    {
        for(Fragment f : getFragments()) {
            if(f instanceof TouchEventListener) {
                if(((TouchEventListener) f).onTouchEvent(event) ) {
                    return true;
                }
            }
        }
        return false;
    }


}

package ms.airy.lib.core.navigation.swipetab;

import android.app.Fragment;

/**
 * Created by harry on 1/30/14.
 */
public final class SwipeTabPageInfo
{
    /**
     * Tag, to be used by FragmentManager.
     */
    private String tag;

    /**
     * Tab/section title.
     */
    private String title;

    /**
     * Section content.
     */
    private Fragment fragment;

    // Ctor's

    public SwipeTabPageInfo()
    {
        this(null);
    }
    public SwipeTabPageInfo(String tag)
    {
        this(tag, null);
    }
    public SwipeTabPageInfo(String tag, String title)
    {
        this(tag, title, null);
    }
    public SwipeTabPageInfo(String tag, String title, Fragment fragment)
    {
        this.tag = tag;
        this.title = title;
        this.fragment = fragment;
    }

    public String getTag()
    {
        return tag;
    }
    public void setTag(String tag)
    {
        this.tag = tag;
    }

    public String getTitle()
    {
        return title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    public Fragment getFragment()
    {
        return fragment;
    }
    public void setFragment(Fragment fragment)
    {
        this.fragment = fragment;
    }

    // For debugging...
    @Override
    public String toString()
    {
        return "SwipeTabPageInfo{" +
                "tag='" + tag + '\'' +
                ", title='" + title + '\'' +
//                ", fragment=" + fragment +
                '}';
    }

}

package ms.airy.lib.core.contacts.loader;

import android.app.LoaderManager;
import android.content.Context;
import android.content.Loader;
import android.database.Cursor;
import android.widget.CursorAdapter;

import java.util.HashMap;
import java.util.Map;

import ms.airy.lib.core.contacts.common.CursorQueryStruct;
import ms.airy.lib.core.contacts.projection.ContactsDataProjectionRegistry;


// temporary
// not fully implemented/tested...
public abstract class BaseMultipleContactsCursorLoaderCallbacks implements LoaderManager.LoaderCallbacks<Cursor>
{
    private final Context context;

    // id -> CursorQueryStruct
    private final Map<Integer, CursorQueryStruct> queryMap = new HashMap<Integer, CursorQueryStruct>();


    protected BaseMultipleContactsCursorLoaderCallbacks(Context context)
    {
        this(context, null);
    }
    protected BaseMultipleContactsCursorLoaderCallbacks(Context context, Map<Integer, CursorQueryStruct> queryMap)
    {
        this.context = context;
        if(queryMap != null) {
            this.queryMap.putAll(queryMap);
        }
    }

    protected Context getContext()
    {
        return context;
    }


    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data)
    {
        Log.i("onLoadFinished() called.");

        if(Log.I) Log.i("data = " + data.toString());
        int cnt = data.getCount();
        if(Log.I) Log.i("count = " + cnt);

        // TBD
        int loaderId = loader.getId();
        CursorQueryStruct cursorQueryStruct = queryMap.get(loaderId);
        if(cursorQueryStruct != null) {
            cursorQueryStruct.getCursorAdapter().swapCursor(data);
        }
    }

    @Override
    public void onLoaderReset(Loader loader)
    {
        Log.i("onLoaderReset() called.");

        // TBD
        int loaderId = loader.getId();
        CursorQueryStruct cursorQueryStruct = queryMap.get(loaderId);
        if(cursorQueryStruct != null) {
            cursorQueryStruct.getCursorAdapter().swapCursor(null);
        }
    }

}

package ms.airy.lib.core.status.client.impl;

import ms.airy.lib.core.status.client.StatusMessageSendClient;
import ms.airy.lib.core.status.common.StatusMessage;
import ms.airy.lib.core.util.IdUtil;

import org.miniclient.ApiUserClient;
import org.miniclient.RestApiException;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 */
public class MiniStatusMessageSendClient extends AbstractStatusMessageClient implements StatusMessageSendClient
{
    private ApiUserClient miniStatusWebClient = null;

    public MiniStatusMessageSendClient()
    {
        this((String) null);
    }
    public MiniStatusMessageSendClient(String statusWebServiceEndpoint)
    {
        super(statusWebServiceEndpoint);
        this.miniStatusWebClient = MiniClientBuilder.getInstance().getStatusWebClient(statusWebServiceEndpoint);
    }
    public MiniStatusMessageSendClient(ApiUserClient miniStatusWebClient)
    {
        super(miniStatusWebClient.getResourceBaseUrl());
        this.miniStatusWebClient = miniStatusWebClient;
    }

    public ApiUserClient getMiniStatusWebClient()
    {
        return miniStatusWebClient;
    }
    protected void setMiniStatusWebClient(String statusWebServiceEndpoint)
    {
        super.setStatusWebServiceEndpoint(statusWebServiceEndpoint);
        this.miniStatusWebClient = MiniClientBuilder.getInstance().getStatusWebClient(statusWebServiceEndpoint);
    }
    protected void setMiniStatusWebClient(ApiUserClient miniStatusWebClient)
    {
        super.setStatusWebServiceEndpoint(miniStatusWebClient.getResourceBaseUrl());
        this.miniStatusWebClient = miniStatusWebClient;
    }

    @Override
    protected void setStatusWebServiceEndpoint(String statusWebServiceEndpoint)
    {
        super.setStatusWebServiceEndpoint(statusWebServiceEndpoint);
        this.miniStatusWebClient = MiniClientBuilder.getInstance().getStatusWebClient(statusWebServiceEndpoint);
    }


    @Override
    public long sendMessage(int type, String message)
    {
        if(Log.I) Log.i("MiniStatusMessageSendClient.sendMessage() type = " + type + "; message = " + message);

        // temporary
        long id = IdUtil.generateRandomId();
        StatusMessage statusMessage = new StatusMessage(id, type, message);
        sendStatusMessage(statusMessage);

        return id;
    }

    @Override
    public void sendStatusMessage(StatusMessage statusMessage)
    {
        if(Log.I) Log.i("MiniStatusMessageSendClient.sendStatusMessage() statusMessage = " + statusMessage);

        // temporary

        try {
            // TBD:
            miniStatusWebClient.create(statusMessage);
        } catch (RestApiException e) {
            Log.e("Failed to post status message.", e);
        } catch (IOException e) {
            Log.e("IO Exception while posting status message.", e);
        }

    }
}

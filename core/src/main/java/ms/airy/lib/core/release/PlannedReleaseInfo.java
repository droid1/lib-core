package ms.airy.lib.core.release;

import ms.airy.lib.core.util.DateRangeUtil;


public class PlannedReleaseInfo
{
    private final String releaseName;  // Id/PK.
    private String releaseDay;   // "yyyy-mm-dd"

    // Does this make sense?
    // This is the info that is to be put into the Manifest file...
    private int versionCode;
    private String versionName;

    // To display in the UI.
    // This will be displayed, if set,
    //     in the "next release info" menu to the user...
    private String releaseTitle;
    private String releaseMessage;
    // ...

    // TBD:
    private boolean cancelled;   // permanently cancelled? paused/held-off ??? Is this necessary? Just remove it from the list at release???
    // private boolean released;    // --> just use (releasedTime != 0L)
    private long releasedTime;   // 0L means it has not been released.


    public PlannedReleaseInfo(String releaseName, String releaseDay)
    {
        this(releaseName, releaseDay, 0, null);
    }
    public PlannedReleaseInfo(String releaseName, String releaseDay, int versionCode, String versionName)
    {
        this(releaseName, releaseDay, versionCode, versionName, null, null);
    }
    public PlannedReleaseInfo(String releaseName, String releaseDay, int versionCode, String versionName, String releaseTitle, String releaseMessage)
    {
        this.releaseName = releaseName;
        this.releaseDay = releaseDay;
        this.versionCode = versionCode;
        this.versionName = versionName;
        this.releaseTitle = releaseTitle;
        this.releaseMessage = releaseMessage;
        this.cancelled = false;
        this.releasedTime = 0L;
    }

    public final String getReleaseName()
    {
        return releaseName;
    }

    public final String getReleaseDay()
    {
        return releaseDay;
    }
    public final void setReleaseDay(String releaseDay)
    {
        this.releaseDay = releaseDay;
    }
    public final void delayReleaseByMonths(int months)
    {
        delayRelease(months, 0, 0);
    }
    public final void delayReleaseByWeeks(int weeks)
    {
        delayRelease(0, weeks, 0);
    }
    public final void delayReleaseByDays(int days)
    {
        delayRelease(0, 0, days);
    }
    public final void delayReleaseByMonthsAndDays(int months, int days)
    {
        delayRelease(months, 0, days);
    }
    public final void delayReleaseByWeeksAndDays(int weeks, int days)
    {
        delayRelease(0, weeks, days);
    }
    public final void delayRelease(int months, int weeks, int days)
    {
        // Is there a better way than doing this aweful iterations???
        String newReleaseDay = getReleaseDay();
        while((months--) > 0) {
            newReleaseDay = DateRangeUtil.getNextMonth(newReleaseDay);
        }
        while((weeks--) > 0) {
            newReleaseDay = DateRangeUtil.getNextWeek(newReleaseDay);
        }
        while((days--) > 0) {
            newReleaseDay = DateRangeUtil.getNextDay(newReleaseDay);
        }
        setReleaseDay(newReleaseDay);
    }

    public final int getVersionCode()
    {
        return versionCode;
    }
    public final void setVersionCode(int versionCode)
    {
        this.versionCode = versionCode;
    }

    public final String getVersionName()
    {
        return versionName;
    }
    public final void setVersionName(String versionName)
    {
        this.versionName = versionName;
    }


    public final String getReleaseTitle()
    {
        return releaseTitle;
    }
    public final void setReleaseTitle(String releaseTitle)
    {
        this.releaseTitle = releaseTitle;
    }

    public final String getReleaseMessage()
    {
        return releaseMessage;
    }
    public final void setReleaseMessage(String releaseMessage)
    {
        this.releaseMessage = releaseMessage;
    }


    public final boolean isCancelled()
    {
        return cancelled;
    }
    public final void setCancelled(boolean cancelled)
    {
        this.cancelled = cancelled;
    }
    public final void cancel()
    {
        setCancelled(true);
    }

    public final long getReleasedTime()
    {
        return releasedTime;
    }
    public final void setReleasedTime(long releasedTime)
    {
        this.releasedTime = releasedTime;
    }
    public final void markAsReleased()
    {
        setReleasedTime(System.currentTimeMillis());
    }



    // For debugging...
    @Override
    public String toString()
    {
        return "PlannedReleaseInfo{" +
                "releaseName='" + releaseName + '\'' +
                ", releaseDay='" + releaseDay + '\'' +
                ", versionCode=" + versionCode +
                ", versionName='" + versionName + '\'' +
                ", releaseTitle='" + releaseTitle + '\'' +
                ", releaseMessage='" + releaseMessage + '\'' +
                ", cancelled=" + cancelled +
                ", releasedTime=" + releasedTime +
                '}';
    }
}

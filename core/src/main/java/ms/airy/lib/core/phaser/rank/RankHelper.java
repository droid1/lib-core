package ms.airy.lib.core.phaser.rank;

import ms.airy.lib.core.phaser.common.Rank;

import java.util.HashMap;
import java.util.Map;


// TBD:
// FeatureMap data should be stored to/from persistent storage such as config.
// Just using a singleton may not be sufficient.....
public final class RankHelper
{

    // Map of feature Id -> { min Rank (inclusive), max (exclusive) }.
    // Min cannot be null, whereas max can be null, meaning there is no max.
    // FeatureMap is open ended. That is,
    // Features not in this map are supported for all Ranks >= minRank = 0;
    private final Map<String, Rank.Range> featureMap;

    public RankHelper()
    {
        featureMap = new HashMap<String, Rank.Range>();
        init();
    }
    private void init()
    {
        // TBD:
        // Populate the featureMap....
        // ....

//        Set<Feature> features = FeatureHelper.getInstance().getFeatures();
//        if(features != null && !features.isEmpty()) {
//            for(Feature f : features) {
//                featureMap.put(f.getCode(), new Rank.Range(f.getMinRank(), f.getMaxRank()));
//            }
//        }
    }


    // Singleton
    private static final class RankHelperHolder
    {
        private static final RankHelper INSTANCE = new RankHelper();
    }
    public static RankHelper getInstance()
    {
        return RankHelperHolder.INSTANCE;
    }


    public void addFeature(String featureCode, Integer minRank, Integer maxRank)
    {
        featureMap.put(featureCode, new Rank.Range(minRank, maxRank));
    }

    // Inclusive.
    public int getMinRank(String featureCode)
    {
        if(! featureMap.containsKey(featureCode)) {
            return Rank.R01;
        } else {
            return featureMap.get(featureCode).getMin();
        }
    }
    // Exclusive. ????
    public int getMaxRank(String featureCode)
    {
        if(! featureMap.containsKey(featureCode)) {
            return Rank.MAX;
        } else {
            Integer max = featureMap.get(featureCode).getMax();
            if(max == null) {
                return Rank.MAX;
            } else {
                return max;
            }
        }
    }
    public boolean hasMaxRank(String featureCode)
    {
        if(featureMap.containsKey(featureCode)) {
            if(featureMap.get(featureCode).getMax() != null) {
                return true;
            }
        }
        return false;
    }

    public boolean isFeatureAvailable(String featureCode, int Rank)
    {
        if(! featureMap.containsKey(featureCode)) {
            return true;
        }
        return (getMinRank(featureCode) <= Rank && getMaxRank(featureCode) > Rank);
    }


}

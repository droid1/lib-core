package ms.airy.lib.core.util;

import android.content.Context;
import android.content.pm.PackageManager;


// Android platform-related utility functions...
public class RuntimeUtil
{    
    // Prevents instantiation...
    private RuntimeUtil() {}

    // ...

    public static boolean isTargetTelevision(Context context)
    {
    	// TBD:
    	// Based on app-specific config????
    	// ...
    	//if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_TELEVISION)) {   // ???
    	if (context.getPackageManager().hasSystemFeature("com.google.android.tv")) {               // ???
    	    return true;
    	} else {
    		return false;
    	}
    }


}

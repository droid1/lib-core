package ms.airy.lib.core.dialog.stock;

import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.DialogInterface;

import ms.airy.lib.core.R;
import ms.airy.lib.core.dialog.ActionableNoticeDialogHost;
import ms.airy.lib.core.dialog.ActionableNoticeDialogListener;
import ms.airy.lib.core.dialog.base.BasicTimerDialogFragment;

import java.util.HashSet;
import java.util.Set;


public class SimpleTimerDialogFragment extends BasicTimerDialogFragment implements ActionableNoticeDialogHost
{
    public static SimpleTimerDialogFragment newInstance(CharSequence title)
    {
        SimpleTimerDialogFragment fragment = new SimpleTimerDialogFragment();
        addArguments(fragment, title);
        return fragment;
    }
    public static SimpleTimerDialogFragment newInstance(CharSequence title, long timerMax, long timerMin, long timerDelta)
    {
        SimpleTimerDialogFragment fragment = new SimpleTimerDialogFragment();
        addArguments(fragment, title);
        addArguments(fragment, timerMax, timerMin, timerDelta);
        return fragment;
    }
    public static SimpleTimerDialogFragment newInstance(CharSequence title, int layoutResId)
    {
        SimpleTimerDialogFragment fragment = new SimpleTimerDialogFragment();
        addArguments(fragment, title, layoutResId);
        return fragment;
    }
    public static SimpleTimerDialogFragment newInstance(CharSequence title, int layoutResId, long timerMax, long timerMin, long timerDelta)
    {
        SimpleTimerDialogFragment fragment = new SimpleTimerDialogFragment();
        addArguments(fragment, title, layoutResId);
        addArguments(fragment, timerMax, timerMin, timerDelta);
        return fragment;
    }
    public static SimpleTimerDialogFragment newInstance(int titleResId)
    {
        SimpleTimerDialogFragment fragment = new SimpleTimerDialogFragment();
        addArguments(fragment, titleResId);
        return fragment;
    }
    public static SimpleTimerDialogFragment newInstance(int titleResId, long timerMax, long timerMin, long timerDelta)
    {
        SimpleTimerDialogFragment fragment = new SimpleTimerDialogFragment();
        addArguments(fragment, titleResId);
        addArguments(fragment, timerMax, timerMin, timerDelta);
        return fragment;
    }
    public static SimpleTimerDialogFragment newInstance(int titleResId, int layoutResId)
    {
        SimpleTimerDialogFragment fragment = new SimpleTimerDialogFragment();
        addArguments(fragment, titleResId, layoutResId);
        return fragment;
    }
    public static SimpleTimerDialogFragment newInstance(int titleResId, int layoutResId, long timerMax, long timerMin, long timerDelta)
    {
        SimpleTimerDialogFragment fragment = new SimpleTimerDialogFragment();
        addArguments(fragment, titleResId, layoutResId);
        addArguments(fragment, timerMax, timerMin, timerDelta);
        return fragment;
    }
    public SimpleTimerDialogFragment()
    {
    }


    ////////////////////////////////////////////////////
    // ActionableNoticeDialog interface callbacks

    private final Set<ActionableNoticeDialogListener> actionables = new HashSet<ActionableNoticeDialogListener>();
    @Override
    public DialogFragment addActionable(ActionableNoticeDialogListener actionable)
    {
        actionables.add(actionable);
        return this;
    }


    @Override
    protected AlertDialog.Builder buildDialogBuilder()
    {
        AlertDialog.Builder dialogBuilder = super.buildDialogBuilder();

        // TBD:
        // How to make the count down continue
        // even after the dialog is dismissed?????

        dialogBuilder.setPositiveButton(R.string.basic_timer_dialog_ok,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //.Continue countdown
                        // --> This does not work. When the dialog is dismissed, the timer stops...
                        // ...

                        for(ActionableNoticeDialogListener a : actionables) {
                            a.onPositiveClick(SimpleTimerDialogFragment.this.getTag());
                        }
                    }
                }
        );
//        dialogBuilder.setNeutralButton(R.string.basic_timer_dialog_cancel,
//                new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int whichButton) {
//                        // Cancel countdown
//                        // ActionableTimerDialogFragment.this.stopTimerAndProgress();   // ?????
//                        // ...
//
//                        for(ActionableNoticeDialog a : actionables) {
//                            a.onNeutralClick(SimpleTimerDialogFragment.this.getTag());
//                        }
//                    }
//                }
//        );

        return dialogBuilder;
    }


}

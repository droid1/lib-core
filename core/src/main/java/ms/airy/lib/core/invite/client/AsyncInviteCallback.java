package ms.airy.lib.core.invite.client;

import java.util.List;

import ms.airy.lib.core.invite.common.InviteInfo;
import ms.airy.lib.core.invite.common.RecipientInfo;


public interface AsyncInviteCallback
{
    void onSendInvite(long invitedId, String recipient);
    void onGetInviteInfo(InviteInfo inviteInfo, long inviteId);
    void onGetInvites(List<Long> invites, int status);
    void onGetRecipientInfo(RecipientInfo recipientInfo, String recipient);
    void onGetRecipients(List<String> recipients, int status);
}

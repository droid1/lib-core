package ms.airy.lib.core.navigation.swipetab;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by harry on 1/30/14.
 */
public abstract class AbstractSwipeTabPageRegistry
{
    protected List<SwipeTabPageInfo> pageInfos;

    public AbstractSwipeTabPageRegistry()
    {
        this(null);
    }
    public AbstractSwipeTabPageRegistry(List<SwipeTabPageInfo> pageInfos)
    {
        if(pageInfos != null) {
            this.pageInfos = pageInfos;
        } else {
            this.pageInfos = new ArrayList<SwipeTabPageInfo>();
        }
        init();
    }
    abstract protected void init();

    public List<SwipeTabPageInfo> getPageInfos()
    {
        return pageInfos;
    }

}

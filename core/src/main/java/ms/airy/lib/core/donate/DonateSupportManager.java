package ms.airy.lib.core.donate;

import android.content.Context;


// TBD: Enable "donation" from users (e.g, using in-app billing)
// ...
// Donators/contributors might have "banners' on their apps
// or the app might have "walls" for these people...
// ....
public final class DonateSupportManager
{
    private static DonateSupportManager INSTANCE = null;
    public static DonateSupportManager getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new DonateSupportManager(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;


    private DonateSupportManager(Context context)
    {
        this.context = context;
    }

    // TBD

    // ....


}

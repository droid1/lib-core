package ms.airy.lib.core.view;

import android.view.View;


// Includes both onClick() and onLongClick()
public interface ComboClickListener extends View.OnClickListener, View.OnLongClickListener
{
}

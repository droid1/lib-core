package ms.airy.lib.core.memo.prefs;

import android.content.Context;


public final class MemoPrefsUtil
{
    private MemoPrefsUtil() {}


    public static boolean isInRefractoryPeriod(Context context, String fragmentTag, int refractorySecs)
    {
        long lastMemoTime = 0;   // = MemoInfoPrefsManager.getInstance(context).getLastMemoTime(fragmentTag);
        long now = System.currentTimeMillis();
        if(lastMemoTime + refractorySecs * 1000L < now) {
            return false;
        } else {
            return true;
        }
    }

    public static void setLastMemoTime(Context context, String fragmentTag)
    {
//        if(Log.D) Log.d("MemoPrefsUtil.setLastMemoTime() fragmentTag = " + fragmentTag);
//        MemoInfoPrefsManager.getInstance(context).setLastMemoTime(fragmentTag);
    }

}

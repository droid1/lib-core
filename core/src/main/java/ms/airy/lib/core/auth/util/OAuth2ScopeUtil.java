package ms.airy.lib.core.auth.util;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ms.airy.lib.core.auth.oauth2.*;


public final class OAuth2ScopeUtil
{
    private final static String OAUTH2_SCHEME = "oauth2:";

    private OAuth2ScopeUtil()
    {
    }


    // To be deleted. Use the Set version.
    private static String generateScopeString(List<String> scopeList)
    {
        if (scopeList == null || scopeList.isEmpty()) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(OAUTH2_SCHEME);
        for (int i = 0; i < scopeList.size(); i++) {
            sb.append(scopeList.get(i));
            if (i != scopeList.size() - 1) {
                sb.append(" ");
            }
        }
        String scopes = sb.toString();
        if (Log.D) Log.d("Scope String = " + scopes);
        return scopes;
    }

    public static String generateScopeString(Set<String> scopeSet)
    {
        if (scopeSet == null || scopeSet.isEmpty()) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(OAUTH2_SCHEME);
        for (String scope : scopeSet) {
            sb.append(scope).append(" ");
        }
        sb.deleteCharAt(sb.length() - 1);
        String scopes = sb.toString();
        if (Log.D) Log.d("Scope String = " + scopes);
        return scopes;
    }

    // To be deleted. Use the Set version.
    private static List<String> generateScopeList(String scopeString)
    {
        if (scopeString == null || scopeString.isEmpty()) {
            return null;
        }
        if (scopeString.startsWith(OAUTH2_SCHEME)) {
            scopeString = scopeString.substring(OAUTH2_SCHEME.length());
        }
        String[] scopes = scopeString.split("\\s+");
        if (Log.D) Log.d("Scope Array = " + Arrays.toString(scopes));
        return Arrays.asList(scopes);
    }

    public static Set<String> generateScopeSet(String scopeString)
    {
        if (scopeString == null || scopeString.isEmpty()) {
            return null;
        }
        if (scopeString.startsWith(OAUTH2_SCHEME)) {
            scopeString = scopeString.substring(OAUTH2_SCHEME.length());
        }
        String[] scopes = scopeString.split("\\s+");
        if (Log.D) Log.d("Scope Array = " + Arrays.toString(scopes));
        return new HashSet<String>(Arrays.asList(scopes));
    }


    // Scope comparison util methods.

    public static boolean isScopeEquivalent(Set<String> firstScopeSet, Set<String> secondScopeSet)
    {
        if(firstScopeSet == null && secondScopeSet == null) {
            // ????
            return true;
        }
        // or use A.containsAll(B) && B.containsAll(A) ???
        if(firstScopeSet != null && firstScopeSet.equals(secondScopeSet)) {
            return true;
        }
        return false;
    }

    public static boolean isScopeEquivalent(String firstScopes, String secondScopes)
    {
        return isScopeEquivalent(generateScopeSet(firstScopes), generateScopeSet(secondScopes));
    }

    // Returns true if firstScopeSet is "bigger" than secondScopeSet.
    public static boolean isScopeSufficient(Set<String> firstScopeSet, Set<String> secondScopeSet)
    {
        if (firstScopeSet == null && secondScopeSet == null) {
            // ????
            return true;
        }
        if(firstScopeSet != null && firstScopeSet.containsAll(secondScopeSet)) {
            return true;
        }
        return false;
    }
    public static boolean isScopeSufficient(String firstScopes, String secondScopes)
    {
        return isScopeSufficient(generateScopeSet(firstScopes), generateScopeSet(secondScopes));
    }

}
package ms.airy.lib.core.startup.job;

import ms.airy.lib.core.startup.job.condition.TaskCondition;
import ms.airy.lib.core.startup.task.StartupTask;


// Task to be performed at "startup"
// The name "task" is taken for Android AsyncTask,
//    and hence we use the name "job".
public interface StartupJob
{
    // Id?
    // (Task has id.)
    // long getId();   // ???

    // TBD:
    // Allow running the task multiple times?
    // e.g., list of (condition, task) with different condtions???

    // Condition
    TaskCondition getCondition();

    // Task
    StartupTask getTask();

    // TBD:
    // Certain tasks may need to be run every time a new version is installed.
    //    (e.g., for checking the existence of primary account, etc.)
    // Since we keep the task.id in the SharedPrefs,
    //    it may not be possible to do that without allowing multiple runs of the same task.
    // One workaround, for now, is that
    //    we change the task id (even for the same task) at each version release...
    // ...

    // TBD:
    // boolean siRepeatable();
    // int getMaxRuns();
    // ...


    // priority???
    // --> Just use ordered list in the registry???
    int getPriority();   // ???

    // fg or bg?
    // bg job cannot interact with ui.
    // (tbd: move this flag to Task?)
    boolean isBackgroundJob();

}

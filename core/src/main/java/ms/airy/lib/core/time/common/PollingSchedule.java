package ms.airy.lib.core.time.common;


/**
 */
public final class PollingSchedule
{
    // Polling schedule.
    // (1) constant interval.
    public static final int CONSTANT = 1;
    // (2) linearly increasing intervals. e.g., 10, 12, 14, ...
    public static final int LINEAR = 2;
    // (3) Uses Fibonacci sequence.
    public static final int FIBONACCI = 3;
    // etc...

    private PollingSchedule() {}


    public static int getDefaultSchedule()
    {
        // temporary
        return CONSTANT;
    }

}

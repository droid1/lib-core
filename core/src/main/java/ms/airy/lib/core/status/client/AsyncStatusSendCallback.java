package ms.airy.lib.core.status.client;

import ms.airy.lib.core.status.common.StatusMessage;


/**
 */
public interface AsyncStatusSendCallback
{
    void onSendMessage(long id, int type, String message);
    // void sendMessage(long id, int type, String message);
    void onSendStatusMessage(StatusMessage statusMessage);
}

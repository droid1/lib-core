package ms.airy.lib.core.invite.client.impl;

import org.miniclient.ApiUserClient;
import org.miniclient.impl.AbstractApiUserClient;
import org.miniclient.impl.base.DefaultApiUserClient;


// TBD:
public final class MiniClientBuilder
{
    // temporary
    private static final String DEF_INVITE_WEBSERVICE_ENDPOINT = "http://www.invitedb.com/v1/invite";
    private static final String DEF_RECIPIENT_WEBSERVICE_ENDPOINT = "http://www.invitedb.com/v1/recipient";
    // ...

    private String inviteWebServiceEndpoint = null;
    private String recipientWebServiceEndpoint = null;
    private AbstractApiUserClient inviteWebClient = null;
    private AbstractApiUserClient recipientWebClient = null;

    public MiniClientBuilder()
    {
        inviteWebServiceEndpoint = DEF_INVITE_WEBSERVICE_ENDPOINT;
        recipientWebServiceEndpoint = DEF_RECIPIENT_WEBSERVICE_ENDPOINT;
    }

    // Singleton
    private static final class MiniClientBuilderHolder
    {
        private static final MiniClientBuilder INSTANCE = new MiniClientBuilder();
    }
    public static MiniClientBuilder getInstance()
    {
        return MiniClientBuilderHolder.INSTANCE;
    }


    public String getInviteWebServiceEndpoint()
    {
        return inviteWebServiceEndpoint;
    }
    public void setInviteWebServiceEndpoint(String inviteWebServiceEndpoint)
    {
        this.inviteWebServiceEndpoint = inviteWebServiceEndpoint;
        if(this.inviteWebServiceEndpoint == null) {   // TBD: Validate the URL ???
            this.inviteWebServiceEndpoint = DEF_INVITE_WEBSERVICE_ENDPOINT;
        }
        inviteWebClient = null;  // reset.
    }

    public String getRecipientWebServiceEndpoint()
    {
        return recipientWebServiceEndpoint;
    }
    public void setRecipientWebServiceEndpoint(String recipientWebServiceEndpoint)
    {
        this.recipientWebServiceEndpoint = recipientWebServiceEndpoint;
        if(this.recipientWebServiceEndpoint == null) {   // TBD: Validate the URL ???
            this.recipientWebServiceEndpoint = DEF_RECIPIENT_WEBSERVICE_ENDPOINT;
        }
        recipientWebClient = null;  // reset.
    }

    public ApiUserClient getInviteWebClient(String inviteWebServiceEndpoint)
    {
        if(! this.inviteWebServiceEndpoint.equals(inviteWebServiceEndpoint)) {
            setInviteWebServiceEndpoint(inviteWebServiceEndpoint);
        }
        return getInviteWebClient();
    }
    public ApiUserClient getInviteWebClient()
    {
        if(this.inviteWebClient == null) {
            // TBD: UserCredential ????
            this.inviteWebClient = new DefaultApiUserClient(this.inviteWebServiceEndpoint);
            // TBD: Other configs.???
        }
        return this.inviteWebClient;
    }

    public ApiUserClient getRecipientWebClient(String recipientWebServiceEndpoint)
    {
        if(! this.recipientWebServiceEndpoint.equals(recipientWebServiceEndpoint)) {
            setRecipientWebServiceEndpoint(recipientWebServiceEndpoint);
        }
        return getRecipientWebClient();
    }
    public ApiUserClient getRecipientWebClient()
    {
        if(this.recipientWebClient == null) {
            // TBD: UserCredential ????
            this.recipientWebClient = new DefaultApiUserClient(this.recipientWebServiceEndpoint);
            // TBD: Other configs.???
        }
        return this.recipientWebClient;
    }

}

package ms.airy.lib.core.config.client.impl;

import org.miniclient.ApiUserClient;
import org.miniclient.impl.AbstractApiUserClient;
import org.miniclient.impl.base.DefaultApiUserClient;


// TBD:
public final class MiniClientBuilder
{
    // temporary
    private static final String DEF_CONFIG_WEBSERVICE_ENDPOINT = "http://www.configbay.com/v1/config";
    // ...

    private String configWebServiceEndpoint = null;
    private AbstractApiUserClient configWebClient = null;

    public MiniClientBuilder()
    {
        configWebServiceEndpoint = DEF_CONFIG_WEBSERVICE_ENDPOINT;
    }

    // Singleton
    private static final class MiniClientBuilderHolder
    {
        private static final MiniClientBuilder INSTANCE = new MiniClientBuilder();
    }
    public static MiniClientBuilder getInstance()
    {
        return MiniClientBuilderHolder.INSTANCE;
    }


    public String getConfigWebServiceEndpoint()
    {
        return configWebServiceEndpoint;
    }
    public void setConfigWebServiceEndpoint(String configWebServiceEndpoint)
    {
        this.configWebServiceEndpoint = configWebServiceEndpoint;
        if(this.configWebServiceEndpoint == null) {   // TBD: Validate the URL ???
            this.configWebServiceEndpoint = DEF_CONFIG_WEBSERVICE_ENDPOINT;
        }
        configWebClient = null;  // reset.
    }


    public ApiUserClient getConfigWebClient(String configWebServiceEndpoint)
    {
        if(! this.configWebServiceEndpoint.equals(configWebServiceEndpoint)) {
            setConfigWebServiceEndpoint(configWebServiceEndpoint);
        }
        return getConfigWebClient();
    }
    public ApiUserClient getConfigWebClient()
    {
        if(this.configWebClient == null) {
            // TBD: UserCredential ????
            this.configWebClient = new DefaultApiUserClient(this.configWebServiceEndpoint);
            // TBD: Other configs.???
        }
        return this.configWebClient;
    }

}

package ms.airy.lib.core.config.client.callback;

import ms.airy.lib.core.config.client.AsyncConfigCallback;
import ms.airy.lib.core.config.client.impl.*;


/**
 */
public abstract class AbstractAsyncConfigCallback implements AsyncConfigCallback
{
    @Override
    public void onGetProperty(Object value, String key)
    {
        if(Log.I) Log.i("AbstractAsyncConfigCallback.onGetProperty() value = " + value + "; key = " + key);

    }

    @Override
    public void onSetProperty(boolean result, String key, Object value)
    {
        if(Log.I) Log.i("AbstractAsyncConfigCallback.onSetProperty() result = " + result + "; key = " + key + "; value = " + value);

    }
}

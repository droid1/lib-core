package ms.airy.lib.core.popup.upload;

import android.content.Context;


// To "upload" popup responses,
// mainly, for analytics purposes...
// TBD: Just use google analytics ???
public final class UploadManager
{
    private static UploadManager INSTANCE = null;
    public static UploadManager getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new UploadManager(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;


    private UploadManager(Context context)
    {
        this.context = context;
    }


    // A user should be identified via a unique id.
    // and, optionally, user's account type/name, if the user has logged on.
    // (if the user's pii is used, the user consent may be required.)
    // ...





}

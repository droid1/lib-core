package ms.airy.lib.core.config.sync;

import android.content.Context;

import java.net.URL;


// Periodically update the config values (which have been previously used)
public final class ConfigSyncManager
{
    private static ConfigSyncManager INSTANCE = null;
    public static ConfigSyncManager getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new ConfigSyncManager(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Aplication context
    private final Context context;

    // Remote config service URL
    private URL configServiceURL = null;


    private ConfigSyncManager(Context context)
    {
        this.context = context;
    }


    // TBD:
    // ....


}

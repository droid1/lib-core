package ms.airy.lib.core.control.common;


/**
 */
public final class LayoutLocation
{
    // For timer pair display location...
    // Note that these are absolute locations regargless of hand pair direction
    // For top_bottom and left_right,
    //   timers are displayed on the opposites sides
    //   with the corresponding timer displayed closer to the associated hand.
    // public final static int LOCATION_NONE = 0;  // --> Do not include timers ???
    public final static int TOP = 1;
    public final static int BOTTOM = 2;
    public final static int LEFT = 3;
    public final static int RIGHT = 4;
    public final static int TOP_BOTTOM = 5;
    public final static int LEFT_RIGHT = 6;

    // static consts only.
    private LayoutLocation() {}
}

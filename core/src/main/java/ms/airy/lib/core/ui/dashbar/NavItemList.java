package ms.airy.lib.core.ui.dashbar;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;


// TBD: To be deleted..... ????
public class NavItemList
{
    // "Action items". {tag => item}
    private Map<String, NavItem> items;

    // TBD:
    // Curently "focused"/selected item????   (as in a tab control)
    // ....
    
    
    public NavItemList()
    {
        // ...
        items = new HashMap<String, NavItem>();
        // ...
    }
    
    public Collection<NavItem> getList()
    {
        return items.values();
    }
    
    public void resetList()
    {
        // ...
        items = new HashMap<String, NavItem>();
    }

    // add, remove, reorder, ....
    // ...
    
    public String addItem(NavItem item)
    {
        if(item == null) {
            if(Log.W) Log.w("item is null.");
            return null;
        }
        String tag = item.getIdTag();
        if(tag == null) {
            if(Log.W) Log.w("item tag is null.");
            return null;
        }
        items.put(tag, item);
        return tag;
    }
    
    public NavItem removeItem(String tag)
    {
        return items.remove(tag);
    }

    public NavItem removeItem(NavItem item)
    {
        String tag = item.getIdTag();
        if(tag == null) {
            if(Log.W) Log.w("item tag is null.");
            return null;
        }
        return removeItem(tag);
    }
    
}

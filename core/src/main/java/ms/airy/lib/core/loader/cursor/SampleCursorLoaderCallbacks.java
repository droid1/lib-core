package ms.airy.lib.core.loader.cursor;

import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Contacts;
import android.provider.ContactsContract;
import android.widget.CursorAdapter;
import android.widget.SimpleCursorAdapter;


/**
 */
public class SampleCursorLoaderCallbacks implements LoaderManager.LoaderCallbacks<Cursor>
{
    private final Context context;
    private final CursorAdapter cursorAdapter;

    public SampleCursorLoaderCallbacks(Context context, CursorAdapter cursorAdapter)
    {
        this.context = context;
        this.cursorAdapter = cursorAdapter;
    }

    private static final String[] CONTACTS_SUMMARY_PROJECTION = new String[] {
            ContactsContract.Contacts._ID,
            ContactsContract.Contacts.DISPLAY_NAME,
            ContactsContract.Contacts.CONTACT_STATUS,
            ContactsContract.Contacts.CONTACT_PRESENCE,
            ContactsContract.Contacts.PHOTO_ID,
            ContactsContract.Contacts.LOOKUP_KEY,
    };
//    private static final String[] PROJECTION = new String[] {
//            SensorValueData.SensorValues._ID,
//            SensorValueData.SensorValues.TYPE,
//            SensorValueData.SensorValues.ACCURACY
//    };

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args)
    {
        Log.w("onCreateLoader() called with id = " + id);

//        Uri baseUri = SensorValueData.SensorValues.CONTENT_URI;

        Uri baseUri = ContactsContract.Contacts.CONTENT_URI;


        // Now create and return a CursorLoader that will take care of
        // creating a Cursor for the data being displayed.
        String select = "((" + ContactsContract.Contacts.DISPLAY_NAME + " NOTNULL) AND ("
                + ContactsContract.Contacts.HAS_PHONE_NUMBER + "=1) AND ("
                + ContactsContract.Contacts.DISPLAY_NAME + " != '' ))";
//        String select = "";
        return new CursorLoader(context, baseUri,
                CONTACTS_SUMMARY_PROJECTION, select, null,
                ContactsContract.Contacts.DISPLAY_NAME + " COLLATE LOCALIZED ASC");
//        return new CursorLoader(context, baseUri,
//                PROJECTION, select, null,
//                SensorValueData.SensorValues.TYPE + " COLLATE LOCALIZED ASC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data)
    {
        Log.w("onLoadFinished() called.");

        Log.w("data = " + data.toString());
        int cnt = data.getCount();
        Log.w("count = " + cnt);

        cursorAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader loader)
    {
        Log.w("onLoaderReset() called.");
        cursorAdapter.swapCursor(null);
    }

}

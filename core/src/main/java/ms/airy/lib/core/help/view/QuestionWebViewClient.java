package ms.airy.lib.core.help.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import ms.airy.lib.core.help.common.HelpConsts;
import ms.airy.lib.core.help.ui.QuestionAndAnswerActivity;


// temporary
public class QuestionWebViewClient extends WebViewClient
{
    // private final Context context;
    private final Activity activity;
    public QuestionWebViewClient(Activity activity)
    {
        // ???
        // this.context = (context != null) ? context.getApplicationContext() : null;
        this.activity = activity;
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url)
    {
        // Log.w(">>>>>>>>>>>>  shouldOverrideUrlLoading() url = " + url);
        if(Log.I) Log.i(">>>>>>>>>>>>  shouldOverrideUrlLoading() url = " + url);

        // TBD:
        if(activity != null) { // ???
            Intent intent = null;
            Uri uri = Uri.parse(url);
            if(uri.getScheme().equals(HelpConsts.SCHEME_QUESTION)) {
                // (1) Internal
                String strId = uri.getPath();
                // Log.w(">>>>>>>>>>>>  shouldOverrideUrlLoading() strId = " + strId);
                if(Log.I) Log.i(">>>>>>>>>>>>  shouldOverrideUrlLoading() strId = " + strId);

                long qId = 0L;
                if(strId != null) {
                    try {
                        if(strId.startsWith("/")) {
                            strId = strId.substring(1);
                        }
                        qId = Long.parseLong(strId);
                    } catch(Exception e) {
                        // ignore.
                    }
                }
                // Log.w(">>>>>>>>>>>>  shouldOverrideUrlLoading() qId = " + qId);
                if(Log.I) Log.i(">>>>>>>>>>>>  shouldOverrideUrlLoading() qId = " + qId);

                if(qId > 0L) {
                    intent = new Intent(activity, QuestionAndAnswerActivity.class);
                    // intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.putExtra(HelpConsts.KEY_EXTRA_QUESTION_ID, qId);
                }
            } else {
                // (2) Otherwise, the link is not for a page on my site,
                // so launch another Activity that handles URLs
                intent = new Intent(Intent.ACTION_VIEW, uri);
            }
            if(intent != null) {
                activity.startActivity(intent);
                return true;
            }
        }
        // (3) ??? What to do????
        return false;
    }

}
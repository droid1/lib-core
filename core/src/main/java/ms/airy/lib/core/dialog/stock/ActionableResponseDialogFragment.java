package ms.airy.lib.core.dialog.stock;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import ms.airy.lib.core.R;
import ms.airy.lib.core.dialog.ActionableAssertDialogListener;
import ms.airy.lib.core.dialog.base.BasicMessageDialogFragment;


// http://developer.android.com/guide/topics/ui/dialogs.html
public class ActionableResponseDialogFragment extends BasicMessageDialogFragment
{
    //    private ActionableAssertDialog mActivity = null;

    // Use default button labels (includes all three buttons).
    public static ActionableResponseDialogFragment newInstance(CharSequence title, CharSequence message)
    {
        ActionableResponseDialogFragment fragment = new ActionableResponseDialogFragment();
        addArguments(fragment, title, message);
        return fragment;
    }
    public static ActionableResponseDialogFragment newInstance(CharSequence title, CharSequence message, CharSequence positiveLabel, CharSequence negativeLabel, CharSequence neutralLabel)
    {
        ActionableResponseDialogFragment fragment = newInstance(title, message);
        addButtonLabelArguments(fragment, positiveLabel, negativeLabel, neutralLabel);
        return fragment;
    }
    // Use default button labels (includes all three buttons).
    public static ActionableResponseDialogFragment newInstance(int titleResId, int messageResId)
    {
        ActionableResponseDialogFragment fragment = new ActionableResponseDialogFragment();
        addArguments(fragment, titleResId, messageResId);
        return fragment;
    }
    public static ActionableResponseDialogFragment newInstance(int titleResId, int messageResId, int positiveLabelResId, int negativeLabelResId, int neutralLabelResId)
    {
        ActionableResponseDialogFragment fragment =  newInstance(titleResId, messageResId);
        addButtonLabelArguments(fragment, positiveLabelResId, negativeLabelResId, neutralLabelResId);
        return fragment;
    }

    // label==null means, use the resource. label=="" means, do not display the button.
    protected static DialogFragment addButtonLabelArguments(DialogFragment fragment, CharSequence positiveLabel, CharSequence negativeLabel, CharSequence neutralLabel)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        if(positiveLabel != null) {
            args.putCharSequence(ARG_POSITIVE_LABEL, positiveLabel);
        }
        if(negativeLabel != null) {
            args.putCharSequence(ARG_NEGATIVE_LABEL, negativeLabel);
        }
        if(neutralLabel != null) {
            args.putCharSequence(ARG_NEUTRAL_LABEL, neutralLabel);
        }
        return fragment;
    }
    // labelResId==-1 means, Do not include the button.
    protected static DialogFragment addButtonLabelArguments(DialogFragment fragment, int positiveLabelResId, int negativeLabelResId, int neutralLabelResId)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        args.putInt(ARG_POSITIVE_LABEL_RESID, positiveLabelResId);
        args.putInt(ARG_NEGATIVE_LABEL_RESID, negativeLabelResId);
        args.putInt(ARG_NEUTRAL_LABEL_RESID, neutralLabelResId);
        return fragment;
    }

    public ActionableResponseDialogFragment()
    {
    }


    private DialogInterface.OnClickListener positiveListener =
            new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int whichButton) {
                    ((ActionableAssertDialogListener) getActivity()).onPositiveClick(ActionableResponseDialogFragment.this.getTag());
                }
            };
    private DialogInterface.OnClickListener negativeListener =
            new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int whichButton)
                {
                    ((ActionableAssertDialogListener) getActivity()).onNegativeClick(ActionableResponseDialogFragment.this.getTag());
                }
            };
    private DialogInterface.OnClickListener neutralListener =
            new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int whichButton)
                {
                    ((ActionableAssertDialogListener) getActivity()).onNeutralClick(ActionableResponseDialogFragment.this.getTag());
                }
            };

    @Override
    protected AlertDialog.Builder buildDialogBuilder()
    {
        CharSequence positiveLabel = getArguments().getCharSequence(ARG_POSITIVE_LABEL);
        int positiveLabelResId = getArguments().getInt(ARG_POSITIVE_LABEL_RESID);
        CharSequence negativeLabel = getArguments().getCharSequence(ARG_NEGATIVE_LABEL);
        int negativeLabelResId = getArguments().getInt(ARG_NEGATIVE_LABEL_RESID);
        CharSequence neutralLabel = getArguments().getCharSequence(ARG_NEUTRAL_LABEL);
        int neutralLabelResId = getArguments().getInt(ARG_NEUTRAL_LABEL_RESID);

        AlertDialog.Builder dialogBuilder = super.buildDialogBuilder();

        if(positiveLabel != null) {
            if(positiveLabel.length() > 0) {
                dialogBuilder.setPositiveButton(positiveLabel, positiveListener);
            }
        } else {
            int posId = positiveLabelResId;
            if(posId == 0) {
                posId = R.string.basic_response_dialog_positive;
            }
            if(posId >= 0) {
                dialogBuilder.setPositiveButton(posId, positiveListener);
            }
        }
        if(negativeLabel != null) {
            if(negativeLabel.length() > 0) {
                dialogBuilder.setNegativeButton(negativeLabel, negativeListener);
            }
        } else {
            int negId = negativeLabelResId;
            if(negId == 0) {
                negId = R.string.basic_response_dialog_negative;
            }
            if(negId >= 0) {
                dialogBuilder.setNegativeButton(negId, negativeListener);
            }
        }
        if(neutralLabel != null) {
            if(neutralLabel.length() > 0) {
                dialogBuilder.setNeutralButton(neutralLabel, neutralListener);
            }
        } else {
            int neuId = neutralLabelResId;
            if(neuId == 0) {
                neuId = R.string.basic_response_dialog_neutral;
            }
            if(neuId >= 0) {
                dialogBuilder.setNeutralButton(neuId, neutralListener);
            }
        }

        return dialogBuilder;
    }


    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try {
            ActionableAssertDialogListener mActivity = (ActionableAssertDialogListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement ActionableAssertDialog");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
//        mActivity = null;
    }


}

package ms.airy.lib.core.ui.dashbar;

public enum NavType
{
    HOME,
    UP,    // ???
    MENU,  // ???
    FRAGMENT,
    ACTIVITY,
    INTENT,
    CALLBACK

}

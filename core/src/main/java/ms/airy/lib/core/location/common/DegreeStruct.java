package ms.airy.lib.core.location.common;


public class DegreeStruct
{
    private int degrees;
    private int minutes;
    private int seconds;
    private String direction;   // N, S, E, W.


    public DegreeStruct(int degrees, String direction)
    {
        this(degrees, 0, direction);
    }
    public DegreeStruct(int degrees, int minutes, String direction)
    {
        this(degrees, minutes, 0, direction);
    }
    public DegreeStruct(int degrees, int minutes, int seconds, String direction)
    {
        this.degrees = degrees;
        this.minutes = minutes;
        this.seconds = seconds;
        this.direction = direction;
    }


//    // temporary
//    // Input should be either
//    //    N 37^ 10' 11"
//    // or
//    //    37^ 18' 11" N
//    // etc...
//    public static DegreeStruct buildDegreeStruct(String degreeStr)
//    {
//        return null;
//    }


    public int getDegrees()
    {
        return degrees;
    }
    public void setDegrees(int degrees)
    {
        this.degrees = degrees;
    }

    public int getMinutes()
    {
        return minutes;
    }
    public void setMinutes(int minutes)
    {
        this.minutes = minutes;
    }

    public int getSeconds()
    {
        return seconds;
    }
    public void setSeconds(int seconds)
    {
        this.seconds = seconds;
    }

    public String getDirection()
    {
        return direction;
    }
    public void setDirection(String direction)
    {
        this.direction = direction;
    }

    @Override
    public String toString()
    {
        return "DegreeStruct{" +
                "degrees=" + degrees +
                ", minutes=" + minutes +
                ", seconds=" + seconds +
                ", direction='" + direction + '\'' +
                '}';
    }
}

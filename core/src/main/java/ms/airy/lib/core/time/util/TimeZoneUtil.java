package ms.airy.lib.core.time.util;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.TimeZone;


public final class TimeZoneUtil
{
    private TimeZoneUtil() {}


    // Returns the user's current timezone.
    public static TimeZone getUserTimeZone()
    {
        // ???
        return TimeZone.getDefault();
    }

    // In hours....
    public static double getUserTimeZoneOffset()
    {
        double offset = (double) (getUserTimeZone().getOffset(System.currentTimeMillis()) / (1000L * 60 * 60));
        return offset;
    }
    // ????
    // tbd: need to test this.
    // the intention is to get a string like +2.0, -11.5, etc....
    public static String getUserTimeZoneOffsetString()
    {
        double offset = getUserTimeZoneOffset();
        DecimalFormat df = new DecimalFormat("##.#");   // does this work??
        // df.setRoundingMode(RoundingMode.DOWN);  // ???
        String strOffset = df.format(offset);
        return strOffset;
    }

}

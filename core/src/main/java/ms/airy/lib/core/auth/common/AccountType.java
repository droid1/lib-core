package ms.airy.lib.core.auth.common;

import ms.airy.lib.core.auth.helper.AccountTypeUtil;


public class AccountType
{
    public static final int TYPE_NONE = 0;   // Unknown, or unsupported by this app.
    public static final int TYPE_GOOGLE = (0x1 << 0);
    public static final int TYPE_FACEBOOK = (0x1 << 1);
    public static final int TYPE_TWITTER = (0x1 << 2);
    public static final int TYPE_YAHOO = (0x1 << 3);
    // linkedin? tumblr? ???
    // etc. ...

    // This text is to be used with Android API.
    public static final String STRING_GOOGLE = "com.google";
    public static final String STRING_FACEBOOK = "com.facebook.auth.login";
    public static final String STRING_TWITTER = "com.twitter.android.auth.login";
    public static final String STRING_YAHOO = "com.yahoo.mobile.client.share.sync";
    // etc..

    // For display. TBD: Use resource???
    public static final String NAME_NONE = "--";   // ?? Null, or invalid type...
    public static final String NAME_GOOGLE = "Google";
    public static final String NAME_FACEBOOK = "Facebook";
    public static final String NAME_TWITTER = "Twitter";
    public static final String NAME_YAHOO = "Yahoo!";
    // etc..


    private AccountType() {}


}

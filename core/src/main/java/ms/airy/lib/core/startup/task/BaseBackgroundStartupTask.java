package ms.airy.lib.core.startup.task;

import android.content.Context;

import ms.airy.lib.core.startup.prefs.StartupPrefsStoreManager;


public class BaseBackgroundStartupTask extends BaseStartupTask implements BackgroundStartupTask
{
    public BaseBackgroundStartupTask(Context context)
    {
        super(context);
    }

    public BaseBackgroundStartupTask(Context context, long id)
    {
        super(context, id);
    }

    // TBD:
    @Override
    public void run()
    {
        Log.i("BaseBackgroundStartupTask.run()");

        // ???
        StartupPrefsStoreManager.getInstance(getContext()).increaseTaskRunCounter(getId());
    }

}

package ms.airy.lib.core.feedback.client.factory;

import ms.airy.lib.core.feedback.client.OnlineFeedbackClient;
import ms.airy.lib.core.feedback.client.OnlineResponseClient;
import ms.airy.lib.core.feedback.client.OnlineSurveyClient;
import ms.airy.lib.core.feedback.client.impl.MiniOnlineFeedbackClient;
import ms.airy.lib.core.feedback.client.impl.MiniOnlineResponseClient;
import ms.airy.lib.core.feedback.client.impl.MiniOnlineSurveyClient;
import ms.airy.lib.core.feedback.client.impl.MockOnlineFeedbackClient;
import ms.airy.lib.core.feedback.client.impl.MockOnlineResponseClient;
import ms.airy.lib.core.feedback.client.impl.MockOnlineSurveyClient;


/**
 */
public class FeedbackClientFactory
{
    // temporary
    private static final int CLIENT_MOCK = 1;
    private static final int CLIENT_MINI = 2;
    // ...

    private static FeedbackClientFactory INSTANCE = null;

    public static FeedbackClientFactory getInstance()
    {
        if(INSTANCE == null) {
            INSTANCE = new FeedbackClientFactory();
        }
        return INSTANCE;
    }

    // mock or mini
    private int type;

    private OnlineSurveyClient surveyClient = null;
    private OnlineFeedbackClient feedbackClient = null;
    private OnlineResponseClient responseClient = null;
    private FeedbackClientFactory()
    {
        // TBD:
        type = CLIENT_MOCK;
        // ...
    }

    public void useMockClient()
    {
        type = CLIENT_MOCK;
        surveyClient = null;
        feedbackClient = null;
        responseClient = null;
    }
    public void useMiniClient()
    {
        type = CLIENT_MINI;
        surveyClient = null;
        feedbackClient = null;
        responseClient = null;
    }



    public OnlineSurveyClient getSurveyClient()
    {
        if(surveyClient == null) {
            switch(type) {
                case CLIENT_MOCK:
                default:
                    surveyClient = new MockOnlineSurveyClient();
                    break;
                case CLIENT_MINI:
                    surveyClient = new MiniOnlineSurveyClient();
                    break;
            }
        }
        return surveyClient;
    }

    public OnlineFeedbackClient getFeedbackClient()
    {
        if(feedbackClient == null) {
            switch(type) {
                case CLIENT_MOCK:
                default:
                    feedbackClient = new MockOnlineFeedbackClient();
                    break;
                case CLIENT_MINI:
                    feedbackClient = new MiniOnlineFeedbackClient();
                    break;
            }
        }
        return feedbackClient;
    }


    public OnlineResponseClient getResponseClient()
    {
        if(responseClient == null) {
            switch(type) {
                case CLIENT_MOCK:
                default:
                    responseClient = new MockOnlineResponseClient();
                    break;
                case CLIENT_MINI:
                    responseClient = new MiniOnlineResponseClient();
                    break;
            }
        }
        return responseClient;
    }

}

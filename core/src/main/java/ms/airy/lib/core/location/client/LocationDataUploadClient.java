package ms.airy.lib.core.location.client;

import android.location.Location;

import java.util.List;


public interface LocationDataUploadClient
{
    // tbd...
    void uploadLocationData(long startTime, List<Location> locations);
}

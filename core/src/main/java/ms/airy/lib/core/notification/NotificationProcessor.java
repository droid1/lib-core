package ms.airy.lib.core.notification;

import android.app.Notification;
import android.content.Context;

import ms.airy.lib.core.notification.handler.NotificationEventCallback;


/**
 */
public final class NotificationProcessor implements NotificationEventCallback
{
    // temporary
    private static final String DEFAULT_TAG = "NOTI_PROC";

    private static NotificationProcessor INSTANCE = null;
    public static NotificationProcessor getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new NotificationProcessor(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Notification counter.
    private int notificationId;
    // Application-level context.
    private final Context context;
    private NotificationProcessor(Context context)
    {
        notificationId = 0;
        this.context = context;
    }


    public void showNotification(int notificationId, Notification notification)
    {
        int notiId;
        if(notificationId > 0) {
            notiId = notificationId;
        } else {
            // ???
            notiId = this.notificationId++;
        }
        NotificationHelper.getInstance(context, DEFAULT_TAG).showNotification(notiId, notification);
    }



    //////////////////////////////////////////////////////////
    // NotificationEventCallback interface

    @Override
    public void processScheduledNotificationEvent(int notificationId, Notification notification)
    {
        if(Log.I) Log.i("NotificationProcessor.processScheduledNotificationEvent(). notificationId = " + notificationId);

        showNotification(notificationId, notification);
    }

}

package ms.airy.lib.core.location.receiver;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;


// TBD:
// ....
public class BaseLocationTrackerIntentService extends IntentService
{
    public BaseLocationTrackerIntentService()
    {
        super("BaseLocationTrackerIntentService");
    }

    // TBD:
    // ...


    @Override
    protected void onHandleIntent(Intent intent)
    {
        // TBD:
        // ...

        Bundle extras = intent.getExtras();
        // ....

    }


}

package ms.airy.lib.core.hangout.prefs;

import android.content.Context;
import android.content.SharedPreferences;


// TBD:
// This is not about "user preferences".
// It is about SharedPreferences storage.
// It stores the app-related global user settings.
public final class HangoutPrefsStoreManager
{
    // temporary
    private static final String SP_FILENAME_SUFFIX = ".hangout_prefs";
    // user prefs. boolean.
    private static final String KEY_HANGOUT_NOTIFY_ENABLED = "hangout.prefs.notification.enabled";
    private static final boolean DEFAULT_NOTIFY_ENABLED = true;
    // user prefs. int. notify before X seconds.
    private static final String KEY_HANGOUT_NOTIFY_LEADTIME = "hangout.prefs.notification.leadtime";
    private static final int DEFAULT_NOTIFY_LEADTIME = 0;
    // ...

    private static HangoutPrefsStoreManager INSTANCE = null;
    public static HangoutPrefsStoreManager getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new HangoutPrefsStoreManager(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;

    // Separate Shared Preferences file.
    private final String sharedPrefsFile;
    private final SharedPreferences sharedPreferences;

    // "Cache"
    private Boolean notifyEnabled = null;
    private Integer notifyLeadTime = null;

    private HangoutPrefsStoreManager(Context context)
    {
        this.context = context;
        sharedPrefsFile = context.getPackageName() + SP_FILENAME_SUFFIX;
        sharedPreferences = context.getSharedPreferences(sharedPrefsFile, Context.MODE_PRIVATE);
    }
    public String getSharedPrefsFile()
    {
        return sharedPrefsFile;
    }


    public boolean isNotifyEnabled()
    {
        if(this.notifyEnabled == null) {
            boolean notifyEnabled = sharedPreferences.getBoolean(KEY_HANGOUT_NOTIFY_ENABLED, DEFAULT_NOTIFY_ENABLED);
            if (Log.D) Log.d("HangoutPrefsStoreManager: notifyEnabled = " + notifyEnabled);
            this.notifyEnabled = notifyEnabled;
        }
        return this.notifyEnabled;
    }
    public void resetNotifyEnabled()
    {
        setNotifyEnabled(DEFAULT_NOTIFY_ENABLED);
    }
    public void setNotifyEnabled(boolean notifyEnabled)
    {
        setNotifyEnabled(notifyEnabled, false);
    }
    public void setNotifyEnabled(boolean notifyEnabled, boolean force)
    {
        if(force || this.notifyEnabled == null || this.notifyEnabled != notifyEnabled) {
            this.notifyEnabled = notifyEnabled;
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean(KEY_HANGOUT_NOTIFY_ENABLED, notifyEnabled);
            editor.commit();
        }
    }

    public int getNofityLeadTime()
    {
        if(this.notifyLeadTime == null) {
            int notifyLeadTime = sharedPreferences.getInt(KEY_HANGOUT_NOTIFY_LEADTIME, DEFAULT_NOTIFY_LEADTIME);
            if (Log.D) Log.d("HangoutPrefsStoreManager: notifyLeadTime = " + notifyLeadTime);
            this.notifyLeadTime = notifyLeadTime;
        }
        return this.notifyLeadTime;
    }
    public void resetNofityLeadTime()
    {
        setNofityLeadTime(DEFAULT_NOTIFY_LEADTIME);
    }
    public void setNofityLeadTime(int notifyLeadTime)
    {
        setNofityLeadTime(notifyLeadTime, false);
    }
    public void setNofityLeadTime(int notifyLeadTime, boolean force)
    {
        if(force || this.notifyLeadTime == null || this.notifyLeadTime != notifyLeadTime) {
            this.notifyLeadTime = notifyLeadTime;
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt(KEY_HANGOUT_NOTIFY_LEADTIME, notifyLeadTime);
            editor.commit();
        }
    }



    // For debugging/testing purposes
    protected void removeAllPrefs()
    {
        Log.w("======== HangoutPrefsStoreManager.removeAllPrefs()");

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }


}

package ms.airy.lib.core.help.ui;

import android.app.Fragment;
import android.app.ListFragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import ms.airy.lib.core.R;
import ms.airy.lib.core.help.common.HelpConsts;
import ms.airy.lib.core.help.common.QuestionInfo;
import ms.airy.lib.core.help.helper.QuestionInfoRegistry;
import ms.airy.lib.core.help.ui.data.QuestionInfoRowAdapter;
import ms.airy.lib.core.toast.ToastHelper;
import ms.airy.lib.core.util.RandomUtil;


public class QuestionInfoListFragment extends ListFragment
{
    public static QuestionInfoListFragment newInstance()
    {
        QuestionInfoListFragment fragment = new QuestionInfoListFragment();
        return fragment;
    }

    private List<QuestionInfo> questionInfoList = null;
    private QuestionInfoRowAdapter adapter = null;
    public QuestionInfoListFragment()
    {
    }


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // Enable the menu
        setHasOptionsMenu(true);
//        // tbd
//        setRetainInstance(true);

        Bundle args = getArguments();
        if (args != null) {
            // ????
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
//        View rootView = inflater.inflate(R.layout.fragment_question_info_list, container, false);
//        return rootView;
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        if(questionInfoList == null) {   // ???
            questionInfoList = QuestionInfoRegistry.getInstance(getActivity()).getQuestionInfos();
        }
        adapter = new QuestionInfoRowAdapter(getActivity(), questionInfoList);
        setListAdapter(adapter);

        // Temporary
        this.setEmptyText("Info Not Available");    // ???
        // Temporary


//        this.getListView().setLongClickable(true);
//        getListView().setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
//        {
//            @Override
//            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id)
//            {
//                if(Log.I) Log.i("onItemLongClick() position = " + position + "; id = " + id);
//
//                QuestionInfo questionInfo = (QuestionInfo) adapter.getItem(position);
//                if(questionInfo != null) {
//                    // int imgResId = questionInfo.getImageResId();
//                    // int imgResId = questionInfo.getImageResId(ImageSize.SIZE_SMALL);
//                    int imgResId = questionInfo.getImageResId(ImageSize.SIZE_LARGE);
//                    if(imgResId > 0) {
//                        if(Log.I) Log.i("onItemLongClick() imgResId = " + imgResId);
//
//                        // TBD:
//                        Intent intent = new Intent(getActivity(), FullSizeImageViewActivity.class);
//                        intent.putExtra(UIConsts.EXTRA_FULL_SIZE_IMAGE_RESID, imgResId);
//                        startActivity(intent);
//
//                        return true;
//                    }
//                }
//                return false;
//            }
//        });


    }

//    @Override
//    public void onResume()
//    {
//        super.onResume();
//        // This is not needed for our purposes..
//        if(adapter != null) {
//            adapter.notifyDataSetChanged();
//        }
//    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id)
    {
        // super.onListItemClick(l, v, position, id);
        if(Log.D) Log.d("onListItemClick(). position = " + position + "; id = " + id);

//        QuestionInfoRowAdapter adapter = (QuestionInfoRowAdapter) l.getAdapter();
        QuestionInfo questionInfo = (QuestionInfo) adapter.getItem(position);
        processQuestionInfo(questionInfo);

    }


    private void processQuestionInfo(QuestionInfo questionInfo)
    {
        // temporary
        long itemId = questionInfo.getId();
        if(Log.I) Log.i(">>>>>>> onListItemClick(). itemId = " + itemId);
        // ....

        if(itemId > 0L) {
            Intent intent = new Intent(getActivity(), QuestionAndAnswerActivity.class);
            intent.putExtra(HelpConsts.KEY_EXTRA_QUESTION_ID, itemId);
            getActivity().startActivity(intent);
        } else {
            // ???
            // ToastHelper.getInstance(getActivity()).showToast("Invalid item.");
        }
    }


    // temporary
    private int getMenuItemId(long questionInfoId)
    {
        final int MENU_ITEM_BASE = 123551;
        return (int) (MENU_ITEM_BASE + questionInfoId);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        // temporary
        // in # of chars
        // (this should be read from resources? so that it can be device size dependent)
        // final int MAX_LENGTH_QUESTION_MENU_ITEM = 24;  /// ???
        final int MAX_LENGTH_QUESTION_MENU_ITEM = 30;  /// ???
        // temporary
        final int MAX_COUNT_QUESTION_MENU_ITEMS = 5;  /// ???
        // temporary

        // final List<QuestionInfo> questionInfoList = QuestionInfoRegistry.getInstance(getActivity()).getQuestionInfos();
        if(questionInfoList != null && !questionInfoList.isEmpty()) {
            final int maxMenuItems = Math.min(questionInfoList.size(), MAX_COUNT_QUESTION_MENU_ITEMS);

            int[] picked = RandomUtil.pickRandomInts(maxMenuItems, questionInfoList.size());
            for(int i : picked) {
            // for(int i=0; i<maxMenuItems; i++) {
                QuestionInfo info = questionInfoList.get(i);
                int miId = getMenuItemId(info.getId());
                String question = info.getQuestion(getActivity());
                String truncatedQuestion = "";
                if(question != null) {
                    // TBD:
                    int len = question.length();
                    if(len <= MAX_LENGTH_QUESTION_MENU_ITEM) {
                        truncatedQuestion = question;
                    } else {
                        // truncatedQuestion = question.substring(0, MAX_LENGTH_QUESTION_MENU_ITEM - 3) + "..?";
                        truncatedQuestion = question.substring(0, MAX_LENGTH_QUESTION_MENU_ITEM - 2) + "..";
                    }
                }
                String miLabel = "Q: " + truncatedQuestion;
                menu.add(Menu.NONE, miId, Menu.NONE, miLabel);
            }
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int miId = item.getItemId();

        // final List<QuestionInfo> questionInfoList = QuestionInfoRegistry.getInstance(getActivity()).getQuestionInfos();
        if(questionInfoList != null && !questionInfoList.isEmpty()) {
            for(QuestionInfo info : questionInfoList) {
                if(miId == getMenuItemId(info.getId())) {
                    processQuestionInfo(info);
                    return true;
                }
            }
        }

        return super.onOptionsItemSelected(item);
    }


}

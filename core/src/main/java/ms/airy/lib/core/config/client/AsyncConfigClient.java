package ms.airy.lib.core.config.client;


public interface AsyncConfigClient
{
//    void getProperty(String key);
//    void getProperty(String key, String defValue);
//    void setProperty(String key, String value);
    void getProperty(String key);
    void setProperty(String key, Object value);
}

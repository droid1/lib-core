package ms.airy.lib.core.auth.util;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;


// Not being used.
// To be deleted.
public final class AccountManagerUtil
{
    private AccountManagerUtil() {}


    // ????
    public Account getAccount(Context context, String accountName)
    {
        Account[] accounts = AccountManager.get(context).getAccounts();
        for(Account acc : accounts) {
            if(accountName.equals(acc.name)) {
                return acc;
            }
        }
        return null;
    }

}

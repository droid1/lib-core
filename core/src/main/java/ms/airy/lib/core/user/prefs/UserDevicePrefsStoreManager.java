package ms.airy.lib.core.user.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import ms.airy.lib.core.util.IdUtil;


// TBD:
// This is not about "user preferences".
// It is about SharedPreferences storage.
public final class UserDevicePrefsStoreManager
{
    // Note:
    // "User" is very "elusive" on Android.
    // We can use an "account" as a user, but a user can have multiple accounts
    //      and can switch between them.
    // A user is not a device either since the user may have multiple devices.
    // User can change/upgrade device over time.
    // There is really no best single way to identify a "user".
    // We need to use at least two (and, possibly more) identifiers.
    // 
    // (1) "User id"
    // When a user installs an app for the first time, a random user id is created.
    //    this id is used to uniquely identify the user/device.
    //    When the user upgrades the device, the same user id is used (through backup).
    // However, since the user can have multiple devices,
    //    and they might have installed the app on these devices,
    //    a user can have multiple/different user ids.
    // ..
    // (2) Primary account
    // If the user picks a primary account (which can change over time)
    //    the user is identified by this account across all different devices. 
    // ...
    // In theory,
    // user id and primary account can identify a user (in some sense uniquely)
    // but, although user id is semi-permanent, primary account can change any time.
    // hence there is really no persistent way of uniquely identify the user.
    // (Any (many-to-many) mapping between the user id and accounts will become "inconsistent" over time.)
    // ...
    // (3) Other ids
    // Other (external) services may use ids (e.g., registration ids), etc...

    // The sole purpose of this Shared Preference file is to store the unique user id.
    //  (which is more like a device id).
    

    // temporary
    private static final String SP_FILENAME_SUFFIX = ".user_device_prefs";
    private static final String KEY_USER_DEVICE_ID = "user.device.id";
    // ...

    private static UserDevicePrefsStoreManager INSTANCE = null;
    public static UserDevicePrefsStoreManager getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new UserDevicePrefsStoreManager(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;

    // Separate Shared Preferences file to store auth related data.
    private final String sharedPrefsFile;
    private final SharedPreferences sharedPreferences;

    // "cache"
    private String userDeviceId = null;

    private UserDevicePrefsStoreManager(Context context)
    {
        this.context = context;
        sharedPrefsFile = context.getPackageName() + SP_FILENAME_SUFFIX;
        sharedPreferences = context.getSharedPreferences(sharedPrefsFile, Context.MODE_PRIVATE);
    }
    public String getSharedPrefsFile()
    {
        return sharedPrefsFile;
    }


    public String getUserDeviceId()
    {
        if(userDeviceId == null) {
            String id = sharedPreferences.getString(KEY_USER_DEVICE_ID, null);
            if (Log.D) Log.d("User/Device id found in the Shared Prefs = " + id);
            if(id == null || id.isEmpty()) {
                id = createUserDeviceId();
                // storeUserDeviceId(id);
            }
            userDeviceId = id;
        }
        return userDeviceId;
    }

    private String createUserDeviceId()
    {
        // TBD: Use GUID?
        String id = Long.toString(IdUtil.generateRandomId());
        Log.w("New User/Device id created: " + id);
        storeUserDeviceId(id);   // Always store the newly created id....
        return id;
    }
    private void storeUserDeviceId(String id)
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_USER_DEVICE_ID, id);
        editor.commit();
        if (Log.I) Log.i("User/Device id stored: " + id);
    }



    // For debugging/testing purposes
    protected void removeAllPrefs()
    {
        Log.w("======== AccountPrefsStoreManager.removeAllPrefs()");

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }


}

package ms.airy.lib.core.audio;

import android.content.Context;
import android.media.MediaPlayer;


// http://developer.android.com/guide/topics/media/index.html
public class SoundHelper 
{
    private boolean mUseAlertSound = true;  // TBD: Use prefs...
    private MediaPlayer mAlertSoundPlayer = null;
    private int mResourceAlertSound;

    private Context mContext;
    
//    public SoundHelper(Context context)
//    {
//        this(context, 0);  // ????
//    }
    public SoundHelper(Context context, int alertSoundResId)
    {
        mContext = context;
        mResourceAlertSound = alertSoundResId;
        //initSoundPlayers();
    }
    
    // Lazy initialization
    private void initSoundPlayers()
    {
        // TBD
        // Based on the user prefs...
        try {
            mAlertSoundPlayer = MediaPlayer.create(mContext, mResourceAlertSound);
        } catch(Exception ex) {
            // What happened???
            if(Log.E) Log.e("Media player creation failed.", ex);
            mAlertSoundPlayer = null;
        }
        // TBD: When to release these??? mRockSoundPlayer.release(), etc.
        // ...
    }

    public void playAlertSound()
    {
        if(mUseAlertSound) {
            if(mAlertSoundPlayer == null) {
                initSoundPlayers();
            }
            if(mAlertSoundPlayer != null) {
                // TBD
                try {
                    //mAlertSoundPlayer.reset();
                    //mAlertSoundPlayer.prepare();
                    mAlertSoundPlayer.start();
                } catch(Exception ex) {
                    // ignore
                    if(Log.W) Log.w("Failed to play the alert sound.", ex);
                }
            } else {
                // ???
                if(Log.W) Log.w("Could not play the alert sound because the media player initialization failed.");
            }
        }
    }

    
    // TBD: Use prefs... ???
    public boolean isUseAlertSound()
    {
        return mUseAlertSound;
    }
    public void setUseAlertSound(boolean useAlertSound)
    {
        this.mUseAlertSound = useAlertSound;
    }
  
    
    
}

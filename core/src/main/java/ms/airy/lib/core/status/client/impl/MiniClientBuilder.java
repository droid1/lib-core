package ms.airy.lib.core.status.client.impl;

import org.miniclient.ApiUserClient;
import org.miniclient.impl.AbstractApiUserClient;
import org.miniclient.impl.base.DefaultApiUserClient;


// TBD:
public final class MiniClientBuilder
{
    // temporary
    private static final String DEF_STATUS_WEBSERVICE_ENDPOINT = "http://www.statusstoa.com/v1/status";
    // ...

    private String statusWebServiceEndpoint = null;
    private AbstractApiUserClient statusWebClient = null;

    public MiniClientBuilder()
    {
        statusWebServiceEndpoint = DEF_STATUS_WEBSERVICE_ENDPOINT;
    }

    // Singleton
    private static final class MiniClientBuilderHolder
    {
        private static final MiniClientBuilder INSTANCE = new MiniClientBuilder();
    }
    public static MiniClientBuilder getInstance()
    {
        return MiniClientBuilderHolder.INSTANCE;
    }


    public String getStatusWebServiceEndpoint()
    {
        return statusWebServiceEndpoint;
    }
    public void setStatusWebServiceEndpoint(String statusWebServiceEndpoint)
    {
        this.statusWebServiceEndpoint = statusWebServiceEndpoint;
        if(this.statusWebServiceEndpoint == null) {   // TBD: Validate the URL ???
            this.statusWebServiceEndpoint = DEF_STATUS_WEBSERVICE_ENDPOINT;
        }
        statusWebClient = null;  // reset.
    }


    public ApiUserClient getStatusWebClient(String statusWebServiceEndpoint)
    {
        if(! this.statusWebServiceEndpoint.equals(statusWebServiceEndpoint)) {
            setStatusWebServiceEndpoint(statusWebServiceEndpoint);
        }
        return getStatusWebClient();
    }
    public ApiUserClient getStatusWebClient()
    {
        if(this.statusWebClient == null) {
            // TBD: UserCredential ????
            this.statusWebClient = new DefaultApiUserClient(this.statusWebServiceEndpoint);
            // TBD: Other configs.???
        }
        return this.statusWebClient;
    }

}

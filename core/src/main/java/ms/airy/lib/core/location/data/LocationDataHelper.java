package ms.airy.lib.core.location.data;

import android.content.Context;
import android.location.Location;

import java.util.List;


// Note:
// We have two tables.
// One for storing "raw" data, and one for fetching the "processed" data.
// The location data in the raw table is periodically processed
//    and the processed data table is populated.
public final class LocationDataHelper
{
    private static LocationDataHelper INSTANCE = null;
    public static LocationDataHelper getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new LocationDataHelper(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;


    private LocationDataHelper(Context context)
    {
        this.context = context;
    }


    // TBD:
    // ....


    public void storeLocation(Location location)
    {
        Log.w("LocationDataHelper.storeLocation() called with location = " + location);

        // tbd:



    }


    public List<Location> fetchLocations(long startTime)
    {
        // tbd:


        return null;
    }


}

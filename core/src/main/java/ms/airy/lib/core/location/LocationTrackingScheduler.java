package ms.airy.lib.core.location;

import android.content.Context;
import android.location.Location;

import ms.airy.lib.core.location.common.ProcessingInterval;
import ms.airy.lib.core.location.common.TrackingFrequency;
import ms.airy.lib.core.location.prefs.LocationPrefsStoreManager;
import ms.airy.lib.core.location.scheduler.LocationTrackerHeartBeat;
import ms.airy.lib.core.location.scheduler.LocationTrackingStarter;
import ms.airy.lib.core.location.scheduler.SchedulerThread;


// tbd...
public final class LocationTrackingScheduler
{
    private static LocationTrackingScheduler INSTANCE = null;
    public static LocationTrackingScheduler getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new LocationTrackingScheduler(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;


    private LocationTrackingScheduler(Context context)
    {
        this.context = context;
    }



    // TBD:
    // These methods need to be called when the app starts..
    // How to do this?????
    // ....
    // Use "android.intent.action.BOOT_COMPLETED" ??? on BroadcaseReceiver???
    // <uses-permission android:name="android.permission.RECEIVE_BOOT_COMPLETED" />
    // Or,
    // use /bootup/BootupManager ????
    // ???



    public void scheduleLocationTracking()
    {
        scheduleLocationTracking(false);
    }
    public void scheduleLocationTracking(final boolean runInForeground)
    {
        if(runInForeground) {

            // TBD:

            // ???
            int trackingFrequency = LocationPrefsStoreManager.getInstance(context).getLocationTrackingFrequency();

            long intervalMillis = TrackingFrequency.getPollingInterval(trackingFrequency);

//            SchedulerThread schedulerThread = new SchedulerThread(context, "LocationTrackingScheduler");
//            schedulerThread.start();

            LocationTrackingStarter.getInstance(context).scheduleLocationTracking();

            // ...

        } else {
            // Note that the user can enable/disable (background) tracking from prefs ui.
            // This scheduleXXX() methods will need to be called
            //   whenever user prefs change, including when the trackingFrequency is updated...
            // .....
            boolean trackingEnabled = LocationPrefsStoreManager.getInstance(context).isLocationTrackingEnabled();
            if(trackingEnabled == false) {
                Log.w("Currently location tracking is disabled by the user.");
                return;   // ????
            }

            Thread thread = new Thread() {
                @Override
                public void run()
                {
                    // ???
                    int trackingFrequency = LocationPrefsStoreManager.getInstance(context).getLocationTrackingFrequency();
                    String cronExpression = TrackingFrequency.getUpdateCronExpression(trackingFrequency, runInForeground);

                    // ???
                    long jitter = 0L;
                    int repeats = 100;
                    LocationTrackerHeartBeat.getInstance(context).scheduleLocationTracking(cronExpression, jitter, repeats);
                }
            };
            thread.run();

        }
    }


    // Cancel background tracking only.
    public void cancelLocationTracking()
    {
        cancelLocationTracking(false);
    }
    private void cancelLocationTracking(final boolean runInForeground)
    {
        if (runInForeground) {
            // not supported.
        } else {
            Thread thread = new Thread() {
                @Override
                public void run()
                {
                    // ???
                    int trackingFrequency = LocationPrefsStoreManager.getInstance(context).getLocationTrackingFrequency();
                    String cronExpression = TrackingFrequency.getUpdateCronExpression(trackingFrequency, runInForeground);
                    int repeats = 100;
                    LocationTrackerHeartBeat.getInstance(context).cancelLocationTracking(cronExpression, repeats);
                }
            };
            thread.run();
        }
    }

    // TBD:
    // how to disable/remove previous (background) alarms (with a given frequency????)
    // When the user changes the prefs (e.g., trackingEnabled, trackingFrequency, etc.)
    // we need to remove the existing alarms and create new ones...
    // ....
    // These methods are to be used only for background/passive tracking schedule...
    private void removeLocationTracking(int trackingFrequency)
    {
        // TBD
    }
    private void updateLocationTrackingFrequency(int oldFrequency, int newFrequency)
    {
        // TBD
    }
    public void disableLocationTracking()
    {
        // TBD
    }
    public void enableLocationTracking()
    {
        // TBD
    }



    // "Scheduled location data processing" is always done "in the background".
    // Location data processing may be triggered "in the foreground" as well...
    public void scheduleLocationProcessing()
    {
        // Note that (background) tracking is disabled,
        // there still might be location data gathered (e.g., in the "foreground/active" tracking)
        // and, hence we need to run the processing "cron" regardless...
        // ....


        // Do we need to run this in a separate thread???
        Thread thread = new Thread() {
            @Override
            public void run()
            {
                // ???
                // int trackingFrequency = LocationPrefsStoreManager.getInstance(context).getLocationTrackingFrequency();

                // TBD:
                // Read it from the app config????
                // ...
                // Also note that,
                //     although it's not strictly necessary,
                //     the processing interval should have dependency on trackingFrequency.
                // The LESS frequently you fetch the location data,
                //     the MORE often you need to process the data as it may appear counter-intuitive
                //     since there will be longer "gap" for less frequent location fetch.
                // --> Use adaptive interval value???
                long intervalMillis = ProcessingInterval.getDefaultIntervalMillis();

                boolean exact = false;
                LocationTrackerHeartBeat.getInstance(context).scheduleLocationProcessing(intervalMillis, exact);
            }
        };
        thread.run();

    }



}

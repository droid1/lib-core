package ms.airy.lib.core.help.helper;

import android.content.Context;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ms.airy.lib.core.help.common.QuestionInfo;


// Heck
// Used mainly to pass a list of QuestionInfo beans to the fragment....
public class QuestionInfoRegistry
{
    private static QuestionInfoRegistry INSTANCE = null;
    public static QuestionInfoRegistry getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new QuestionInfoRegistry(context!= null ? context.getApplicationContext() : null);  // ???
        } else {
            if(INSTANCE.getContext() == null && context != null) {  // ???
                // TBD:
                // Need to "clone" the existing singleton ????
                INSTANCE = new QuestionInfoRegistry(context.getApplicationContext());  // ???
            }
        }
        return INSTANCE;
    }

    private final Context context;
    private QuestionInfoRegistry(Context context)
    {
        this.context = context;
    }
    protected final Context getContext()
    {
        return context;
    }

    // temporary
    // private final List<QuestionInfo> questionInfos = new ArrayList<QuestionInfo>();
    private final Map<Long, QuestionInfo> questionInfoMap = new HashMap<Long, QuestionInfo>();

    // This should be called every time a new list is created since it's a global singleton.
    public void reset()
    {
        // this.questionInfos.clear();
        this.questionInfoMap.clear();
    }
    public void addQuestionInfo(QuestionInfo questionInfo)
    {
        // this.questionInfos.add(questionInfo);
        if(questionInfo != null && questionInfo.getId() > 0L) {
            this.questionInfoMap.put(questionInfo.getId(), questionInfo);
        }
    }
    public void setQuestionInfos(List<QuestionInfo> questionInfos)
    {
        // this.questionInfos.clear();
        // this.questionInfos.addAll(questionInfos);
        this.questionInfoMap.clear();
        if(questionInfos != null && !questionInfos.isEmpty()) {
            for(QuestionInfo qi : questionInfos) {
                this.questionInfoMap.put(qi.getId(), qi);
            }
        }
    }

    // ????
    public List<QuestionInfo> getQuestionInfos()
    {
        // return this.questionInfos;
        return new ArrayList<QuestionInfo>(this.questionInfoMap.values());
    }

    public QuestionInfo getQuestionInfo(long questionId)
    {
        return this.questionInfoMap.get(questionId);
    }


    // tbd
    // ....


}

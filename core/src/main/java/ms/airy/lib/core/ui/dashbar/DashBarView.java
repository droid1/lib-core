package ms.airy.lib.core.ui.dashbar;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.KeyEvent;
import android.widget.LinearLayout;

// http://developer.android.com/guide/topics/ui/custom-components.html
public class DashBarView extends LinearLayout
{
    // temporary
    protected static final int RUNTIME_OS_VERSION = android.os.Build.VERSION.SDK_INT;

    // ....
    private NavButtonLayout mNavButtonLayout = null;
    private NavButton mHomeButton = null;
    private NavButton mMenuButton = null;
    // ....
    
    
    public DashBarView(Context context)
    {
        super(context);
        init();
        //...
    }

    public DashBarView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
        //...
    }

    public DashBarView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        init();
        //...
    }

    
    
    private void init()
    {
        if(Log.D) Log.d("init()");

        // TBD
        setOrientation(VERTICAL);
        setMinimumWidth(300);  // temporary
        // ...
        
        // Initialize the child elements.
        mNavButtonLayout = new NavButtonLayout(getContext());
        
        
        
//        // test
//        TextView tv2 = new TextView(getContext());
//        tv2.setText("xxxxx");
//        tv2.setWidth(200);
//        
//        Button btn1 = new Button(getContext());
//        btn1.setText("yyyyy");
//
//        NavButton btn2 = new NavButton(getContext());
//        //btn2.setIcon(icon);
//        //btn2.setImageURI(Uri.parse("http://media.photobucket.com/image/ladybug%20bmp/JosephAnthony/th_ladybug.bmp"));
//        
//        addView(tv2);
//        addView(btn1);
//        addView(btn2);
//        // test
        

        
        
        // Redo the layout...
        layoutChildViews();

    }
    
    
    // TBD:
    private void layoutChildViews()
    {
        if(Log.D) Log.d("layoutChildViews()");
        
        
        // [0] Remove all views first.
        this.removeAllViews();
        

        // [1] Home
        if(mHomeButton != null) {
            //LinearLayout.LayoutParams homeParams = new LinearLayout.LayoutParams(getLayoutParams());  // ????
            LinearLayout.LayoutParams homeParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT); // ????
//            ViewGroup vg = (ViewGroup)(mHomeButton.getParent());
//            if(vg != null) {
//                vg.removeView(mHomeButton);
//            }
            addView(mHomeButton, homeParams);
        }        
        
        // [2] Nav buttons
        if(mNavButtonLayout != null) {
            //LinearLayout.LayoutParams navParams = new LinearLayout.LayoutParams(getLayoutParams());  // ????
            LinearLayout.LayoutParams navParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT); // ????
//            ViewGroup vg = (ViewGroup)(mNavButtonLayout.getParent());
//            if(vg != null) {
//                vg.removeView(mNavButtonLayout);
//            }
            addView(mNavButtonLayout, navParams);
        }
        
        
        // ???
        LinearLayout bufferSpace = new LinearLayout(getContext());
        LinearLayout.LayoutParams bufferParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT); // ????
        bufferParams.weight = 0.5f;  // ???
        bufferParams.height = 100;   // ???
        addView(bufferSpace, bufferParams);
        // ???
        
        
        // [3] Menu
        //     TBD: How to put menu button at the bottom??????
        if(mMenuButton != null) {
            //LinearLayout.LayoutParams menuParams = new LinearLayout.LayoutParams(getLayoutParams());  // ????
            LinearLayout.LayoutParams menuParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT); // ????
            menuParams.weight = 10.0f;
            menuParams.height = mMenuButton.getHeight();
            menuParams.gravity = Gravity.BOTTOM;
//            ViewGroup vg = (ViewGroup)(mMenuButton.getParent());
//            if(vg != null) {
//                vg.removeView(mMenuButton);
//            }
            addView(mMenuButton, menuParams);
        }

        // ....
        
        
        
        // ?????
        invalidate();
    }
    
    
    
    
    
    public NavButton getHomeButton()
    {
        return mHomeButton;
    }

    public void setHomeButton(NavButton homeButton)
    {
        this.mHomeButton = homeButton;
        layoutChildViews();  // ????
    }

    public NavButton getMenuButton()
    {
        return mMenuButton;
    }

    public void setMenuButton(NavButton menuButton)
    {
        this.mMenuButton = menuButton;
        layoutChildViews();  // ????
    }

    
    public void addNavButton(NavButton button)
    {
        if(Log.D) Log.d("addNavButton()");
        mNavButtonLayout.addNavButton(button);
        layoutChildViews();  // ????
    }
    public void addNavButtons(NavButton... buttons)
    {
        if(Log.D) Log.d("addNavButtons()");
        mNavButtonLayout.addNavButtons(buttons);
        layoutChildViews();  // ????
    }
    public void addNavButton(int location, NavButton button)
    {
        if(Log.D) Log.d("addNavButton(): location = " + location);
        mNavButtonLayout.addNavButton(location, button);
        layoutChildViews();  // ????
    }
    public void addNavButtons(int location, NavButton... buttons)
    {
        if(Log.D) Log.d("addNavButtons(): location = " + location);
        mNavButtonLayout.addNavButtons(location, buttons);
        layoutChildViews();  // ????
    }    
    
    // TBD:
    //public void removeNavButton(String tag) {}
    //public void removeNavButtons(String... tags) {}
    

    
    
    
    
    
    
    @Override
    public int getOrientation()
    {
        if(Log.D) Log.d("getOrientation()");

        // TODO Auto-generated method stub
        return super.getOrientation();
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        if(Log.D) Log.d("onDraw()");

        // TODO Auto-generated method stub
        super.onDraw(canvas);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b)
    {
        if(Log.D) Log.d("onLayout()");

        // TODO Auto-generated method stub
        super.onLayout(changed, l, t, r, b);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        if(Log.D) Log.d("onMeasure()");

        // TODO Auto-generated method stub
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    public void setOrientation(int orientation)
    {
        if(Log.D) Log.d("setOrientation()");

        // TODO Auto-generated method stub
        super.setOrientation(orientation);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if(Log.D) Log.d("onKeyDown()");

        // TODO Auto-generated method stub
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyShortcut(int keyCode, KeyEvent event)
    {
        if(Log.D) Log.d("onKeyShortcut()");

        // TODO Auto-generated method stub
        return super.onKeyShortcut(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event)
    {
        if(Log.D) Log.d("onKeyUp()");

        // TODO Auto-generated method stub
        return super.onKeyUp(keyCode, event);
    }
    
    
    
}

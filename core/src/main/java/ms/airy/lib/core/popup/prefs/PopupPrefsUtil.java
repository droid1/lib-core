package ms.airy.lib.core.popup.prefs;

import android.content.Context;


public final class PopupPrefsUtil
{
    private PopupPrefsUtil() {}


    public static boolean isInRefractoryPeriod(Context context, String fragmentTag, int refractorySecs)
    {
        long lastPopupTime = PopupHostPrefsManager.getInstance(context).getLastPopupTime(fragmentTag);
        long now = System.currentTimeMillis();
        if(lastPopupTime + refractorySecs * 1000L < now) {
            return false;
        } else {
            return true;
        }
    }

    public static void setLastPopupTime(Context context, String fragmentTag)
    {
        if(Log.D) Log.d("PopupPrefsUtil.setLastPopupTime() fragmentTag = " + fragmentTag);
        PopupHostPrefsManager.getInstance(context).setLastPopupTime(fragmentTag);
    }

}

package ms.airy.lib.core.ui.view;


/**
 * Set of callback methods to be triggered by Activity lifecycle events.
 */
public interface ActivityLifeCycleView
{
    // onStart(), etc...
}

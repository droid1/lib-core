package ms.airy.lib.core.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.HashSet;
import java.util.Set;

import dalvik.system.PathClassLoader;
import ms.airy.lib.core.config.res.ResourceConfigManager;


// Note:
// We can have one "global" broadcast receiver per app.
// This receiver needs to be declared in the manifest file with all possible intent list.
// At run time, "shards" (with specific intent handlers) are added/removed....
//   --> Unfortunately, this is not generally possible
//       since we'll likely use <receiver> declaration in hte manifest file.
// Either an app needs to subclass AppGlobalBroadcastReceiver and hard-code all shards,
// or we need to add shards based on the config (<meta-data>), etc...

// TBD:
// Use WakefulBroadcastReceiver ????
public class AppGlobalBroadcastReceiver extends BroadcastReceiver
{
    // temporary.
    // A comma-separated list of BroadcastReceiverShard class full paths.
    private static final String KEY_RECEIVER_SHARDS = "airy.broadcast_receiver.shards";

    // Shard list.
    private final Set<BroadcastReceiverShard> receiverShards;

    // For lazy initialization...
    private boolean initialized;

    public AppGlobalBroadcastReceiver()
    {
        receiverShards = new HashSet<BroadcastReceiverShard>();
        initialized = false;
    }

    // Initialize AppGlobalBroadcastReceiver here...
    protected void init(Context context)
    {
//        // temporary
//          BroadcastReceiverShard shard1 = new NotificationHandlerShard(null);
//        addShard(shard1);
//        // etc...


        // TBD:
        // Load all "boot up" shards first ???
        // location tracking, notifications, alarms...
        // ...
        // ...


        // Config-based shard list.
        ResourceConfigManager resourceConfigManager = new ResourceConfigManager(context);
        String shards = resourceConfigManager.getString(KEY_RECEIVER_SHARDS);
        if(Log.I) Log.i(">>>>>> AppGlobalBroadcastReceiver.init(). shards = " + shards);
        if(shards != null) {
            String[] shardArray = shards.split("\\s*,\\s*");
            for(String className : shardArray) {
                if(className == null || className.isEmpty()) {  // ???
                    continue;
                }
                try {
//                    // ??
//                    // String packageName = ClassPackageUtil.getPackageName(className);
//                    String packageName = context.getPackageName();
//                    // ??
//                    String apkName = context.getPackageManager().getApplicationInfo(packageName, 0).sourceDir;
//                    PathClassLoader classLoader = new PathClassLoader(apkName, ClassLoader.getSystemClassLoader());
//                    Class<?> clss = classLoader.loadClass(className);

                    Class<?> clss = Class.forName(className);

                    Object obj = clss.newInstance();
                    // TBD:
                    // If we use PathClassLoader, then we get ClassCastException...
                    BroadcastReceiverShard shard = (BroadcastReceiverShard) obj;
//                    AbstractBroadcastReceiverShard shard = (AbstractBroadcastReceiverShard) obj;
                    // ...
                    addShard(shard);
                    if(Log.I) Log.i("BroadcastReceiverShard loaded: " + className);
                } catch(Exception e) {
                    Log.e("Failed to load a shard class: " + className, e);
                }
            }
        }

        initialized = true;
    }


    public void addShard(BroadcastReceiverShard shard)
    {
        receiverShards.add(shard);
    }

    public void removeShard(BroadcastReceiverShard shard)
    {
        receiverShards.remove(shard);
    }


    @Override
    public void onReceive(Context context, Intent intent)
    {
        Log.i("AppGlobalBroadcastReceiver.onReceive().");
        if(initialized == false) {
            init(context);
        }

        for(BroadcastReceiverShard shard : receiverShards) {
            // TBD: intent filter ???
            // ....
            shard.onReceive(context, intent);
        }
    }

}

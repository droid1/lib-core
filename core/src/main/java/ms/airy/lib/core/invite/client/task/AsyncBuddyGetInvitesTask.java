package ms.airy.lib.core.invite.client.task;

import android.os.AsyncTask;

import java.util.List;

import ms.airy.lib.core.invite.client.BuddyInviteClient;
import ms.airy.lib.core.invite.client.factory.InviteClientFactory;
import ms.airy.lib.core.invite.common.InviteInfo;


/**
 */
public class AsyncBuddyGetInvitesTask extends AsyncTask<Integer, Void, List<Long>>
{
    private final AsyncTaskCallback callback;
    public AsyncBuddyGetInvitesTask(AsyncTaskCallback callback)
    {
        this.callback = callback;
    }

    // Store input params during processing
    private int status;


    @Override
    protected List<Long> doInBackground(Integer... params)
    {
        status = (Integer) params[0];

        BuddyInviteClient client = InviteClientFactory.getInstance().getInviteClient();

        List<Long> invites = client.getInvites(status);

        return invites;
    }

    @Override
    protected void onPostExecute(List<Long> invites)
    {
        // super.onPostExecute(aLong);

        this.callback.onGetInvites(invites, status);
    }




    // Callback interface
    public static interface AsyncTaskCallback
    {
        void onGetInvites(List<Long> invites, int status);
    }

}

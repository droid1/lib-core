package ms.airy.lib.core.fragment;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;


/**
 * Created by harry on 2/27/14.
 */
public final class FragmentMessage implements Serializable, Parcelable
{
    private int code;
    private String message;
    private Bundle data;

    public FragmentMessage(int code, String message)
    {
        this(code, message, null);
    }
    public FragmentMessage(int code, String message, Bundle data)
    {
        this.code = code;
        this.message = message;
        this.data = data;
    }
    private FragmentMessage(Parcel source)
    {
        readFromParcel(source);
//        code = source.readInt();
//        message = source.readString();
//        data = source.readBundle();
    }

    public int getCode()
    {
        return code;
    }
    public void setCode(int code)
    {
        this.code = code;
    }

    public String getMessage()
    {
        return message;
    }
    public void setMessage(String message)
    {
        this.message = message;
    }

    public Bundle getData()
    {
        return data;
    }
    public void setData(Bundle data)
    {
        this.data = data;
    }



    @Override
    public int describeContents()
    {
        return hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeInt(code);
        dest.writeString(message);
        dest.writeBundle(data);
    }
    public void readFromParcel(Parcel source)
    {
        code = source.readInt();
        message = source.readString();
        data = source.readBundle();
    }

    // Required variable for Parcelable.
    public static Parcelable.Creator<FragmentMessage> CREATOR = new Parcelable.Creator<FragmentMessage>() {
        @Override
        public FragmentMessage createFromParcel(Parcel source)
        {
            return new FragmentMessage(source);
        }

        @Override
        public FragmentMessage[] newArray(int size)
        {
            return new FragmentMessage[size];
        }
    };


    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || !(o instanceof FragmentMessage)) return false;

        FragmentMessage that = (FragmentMessage) o;

        if (code != that.code) return false;
        if (data != null ? !data.equals(that.data) : that.data != null) return false;
        if (!message.equals(that.message)) return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = code;
        result = 31 * result + message.hashCode();
        result = 31 * result + (data != null ? data.hashCode() : 0);
        return result;
    }


    // Primarily for debugging....

    @Override
    public String toString()
    {
        return "FragmentMessage{" +
                "code=" + code +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }

}

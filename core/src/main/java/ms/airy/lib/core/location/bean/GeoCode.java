package ms.airy.lib.core.location.bean;


import ms.airy.lib.core.location.common.DegreeStruct;

// Subset of Android Location class.
// http://developer.android.com/reference/android/location/Location.html
// It does not include, among other things, Bearing, Speed, Provider, etc....
// This class will be primarily used to indicate a fixed point on the map.
// (TBD: Include at least altitude????)
public class GeoCode
{
    private final double latitude;
    private final double longitude;
    private float accuracy;   // 0.0f means the accuracy is unknown.
    private long time;        // 0L means the data time is unknown.


    public GeoCode(double latitude, double longitude)
    {
        this(latitude, longitude, 0.0f);
    }
    public GeoCode(double latitude, double longitude, float accuracy)
    {
        this(latitude, longitude, accuracy, 0L);
    }
    public GeoCode(double latitude, double longitude, float accuracy, long time)
    {
        this.latitude = latitude;
        this.longitude = longitude;
        this.accuracy = accuracy;
        this.time = time;
    }

    public GeoCode(DegreeStruct latitudeDegree, DegreeStruct longitudeDegree)
    {
        this(latitudeDegree, longitudeDegree, 0.0f);
    }
    public GeoCode(DegreeStruct latitudeDegree, DegreeStruct longitudeDegree, float accuracy)
    {
        this(latitudeDegree, longitudeDegree, accuracy, 0L);
    }
    public GeoCode(DegreeStruct latitudeDegree, DegreeStruct longitudeDegree, float accuracy, long time)
    {
        this.latitude = convertDegreeToDecimal(latitudeDegree);
        this.longitude = convertDegreeToDecimal(longitudeDegree);
        this.accuracy = accuracy;
        this.time = time;
    }


    // temporary
    public static double convertDegreeToDecimal(DegreeStruct degreeStruct)
    {
        double val = degreeStruct.getDegrees()
                + (degreeStruct.getMinutes() / 60.0)
                + (degreeStruct.getSeconds() / 3600.0);
        int sign = 1;
        // TBD: Need to check this...
        if(degreeStruct.getDirection().equals("N")
                || degreeStruct.getDirection().equals("E")) {
            sign = 1;
        } else {
            sign = -1;
        }
        return val + sign;
    }
    // tbd.
//    public static DegreeStruct convertDecimalToDegree(double decimal, boolean isLatitude)
//    {
//
//    }


    public double getLatitude()
    {
        return latitude;
    }

    public double getLongitude()
    {
        return longitude;
    }

    public float getAccuracy()
    {
        return accuracy;
    }
    public void setAccuracy(float accuracy)
    {
        this.accuracy = accuracy;
    }

    public long getTime()
    {
        return time;
    }
    public void setTime(long time)
    {
        this.time = time;
    }

    // For debugging...
    @Override
    public String toString()
    {
        return "GeoCode{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                ", accuracy=" + accuracy +
                ", time=" + time +
                '}';
    }

}

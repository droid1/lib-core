package ms.airy.lib.core.feedback.client;

import java.util.List;

import ms.airy.lib.core.feedback.common.ResponseEntry;


// TBD:
public interface AsyncResponseCallback
{
    void onGetResponse(ResponseEntry response, long responseId);
    void onGetNewResponses(List<Long> respones);
}

package ms.airy.lib.core.status.client.task;

import android.os.AsyncTask;

import ms.airy.lib.core.status.client.StatusMessageFetchClient;
import ms.airy.lib.core.status.client.factory.StatusClientFactory;
import ms.airy.lib.core.status.common.StatusMessage;

import java.util.List;


/**
 */
public class AsyncStatusFetchStatusMessageTask extends AsyncTask<Long, Void, StatusMessage>
{
    private final AsyncTaskCallback callback;
    public AsyncStatusFetchStatusMessageTask(AsyncTaskCallback callback)
    {
        this.callback = callback;
    }

    // Store input params during processing
    private long id;


    @Override
    protected StatusMessage doInBackground(Long... params)
    {
        id = params[0];

        StatusMessageFetchClient client = StatusClientFactory.getInstance().getFetchClient();

        StatusMessage statusMessage = client.fetchStatusMessage(id);

        return statusMessage;
    }




    @Override
    protected void onPostExecute(StatusMessage statusMessage)
    {
        // super.onPostExecute(aLong);

        this.callback.onFetchStatusMessage(statusMessage, id);
    }



    // Callback interface
    public static interface AsyncTaskCallback
    {
        void onFetchStatusMessage(StatusMessage statusMessage, long id);
    }

}

package ms.airy.lib.core.feedback.client.async;

import java.util.List;

import ms.airy.lib.core.feedback.client.AsyncResponseCallback;
import ms.airy.lib.core.feedback.client.AsyncResponseClient;
import ms.airy.lib.core.feedback.client.task.AsyncResponseGetNewResponsesTask;
import ms.airy.lib.core.feedback.client.task.AsyncResponseGetResponseTask;
import ms.airy.lib.core.feedback.common.ResponseEntry;


public class AsyncOnlineResponseClient implements AsyncResponseClient,
        AsyncResponseGetResponseTask.AsyncTaskCallback,
        AsyncResponseGetNewResponsesTask.AsyncTaskCallback
{
    private final AsyncResponseCallback callback;
//    private final AsyncResponseGetResponseTask asyncResponseGetResponseTask;
//    private final AsyncResponseGetNewResponsesTask asyncResponseGetNewResponsesTask;


    public AsyncOnlineResponseClient(AsyncResponseCallback callback)
    {
        this.callback = callback;
//        this.asyncResponseGetResponseTask = new AsyncResponseGetResponseTask(this);
//        this.asyncResponseGetNewResponsesTask = new AsyncResponseGetNewResponsesTask(this);
    }

    @Override
    public void getResponse(long responseId)
    {
        if(Log.I) Log.i("AsyncOnlineResponseClient.getResponse(). responseId = " + responseId);

        (new AsyncResponseGetResponseTask(this)).execute(responseId);
    }

    @Override
    public void getNewResponses()
    {
        if(Log.I) Log.i("AsyncOnlineResponseClient.getNewResponses().");

        (new AsyncResponseGetNewResponsesTask(this)).execute();
    }



    ///////////////////////////////////////////////////////////////
    // AsyncResponseGetResponseTask.AsyncTaskCallback interface

    @Override
    public void onGetResponse(ResponseEntry responseEntry, long id)
    {
        callback.onGetResponse(responseEntry, id);
    }


    ///////////////////////////////////////////////////////////////
    // AsyncResponseGetNewResponsesTask.AsyncTaskCallback interface

    @Override
    public void onGetNewResponses(List<Long> responses)
    {
        callback.onGetNewResponses(responses);
    }

}

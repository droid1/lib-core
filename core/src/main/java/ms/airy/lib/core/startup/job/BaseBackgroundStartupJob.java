package ms.airy.lib.core.startup.job;

import ms.airy.lib.core.startup.job.condition.TaskCondition;
import ms.airy.lib.core.startup.task.BackgroundStartupTask;
import ms.airy.lib.core.startup.task.StartupTask;


public class BaseBackgroundStartupJob extends BaseStartupJob implements BackgroundStartupJob
{

    public BaseBackgroundStartupJob(TaskCondition condition, BackgroundStartupTask task)
    {
        super(condition, task);
    }
    public BaseBackgroundStartupJob(TaskCondition condition, BackgroundStartupTask task, int priority)
    {
        super(condition, task, priority, true);
    }



}

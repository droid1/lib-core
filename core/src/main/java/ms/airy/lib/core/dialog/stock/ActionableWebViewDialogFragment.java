package ms.airy.lib.core.dialog.stock;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import ms.airy.lib.core.R;
import ms.airy.lib.core.dialog.ActionableAssertDialogListener;
import ms.airy.lib.core.dialog.base.BasicWebViewDialogFragment;

import java.net.URL;


public class ActionableWebViewDialogFragment extends BasicWebViewDialogFragment
{
//    private ActionableAssertDialog mActivity = null;

    public static ActionableWebViewDialogFragment newInstance(CharSequence title)
    {
        return newInstance(title, 0);
    }
    public static ActionableWebViewDialogFragment newInstance(CharSequence title, int layoutResId)
    {
        ActionableWebViewDialogFragment fragment = new ActionableWebViewDialogFragment();
        addArguments(fragment, title, layoutResId);
        return fragment;
    }
    public static ActionableWebViewDialogFragment newInstance(CharSequence title, String contentData)
    {
        return newInstance(title, 0, contentData);
    }
    public static ActionableWebViewDialogFragment newInstance(CharSequence title, int layoutResId, String contentData)
    {
        ActionableWebViewDialogFragment fragment = new ActionableWebViewDialogFragment();
        addArguments(fragment, title, layoutResId);
        addArguments(fragment, contentData, null);
        return fragment;
    }
    public static ActionableWebViewDialogFragment newOkCancelInstance(CharSequence title, int layoutResId, String contentData)
    {
        return newInstance(title, layoutResId, contentData, null, "", null);
    }
    public static ActionableWebViewDialogFragment newInstance(CharSequence title, int layoutResId, String contentData, CharSequence positiveLabel, CharSequence negativeLabel, CharSequence neutralLabel)
    {
        ActionableWebViewDialogFragment fragment = newInstance(title, layoutResId, contentData);
        addButtonLabelArguments(fragment, positiveLabel, negativeLabel, neutralLabel);
        return fragment;
    }

    public static ActionableWebViewDialogFragment newInstance(CharSequence title, URL contentURL)
    {
        return newInstance(title, 0, contentURL);
    }
    public static ActionableWebViewDialogFragment newInstance(CharSequence title, int layoutResId, URL contentURL)
    {
        ActionableWebViewDialogFragment fragment = new ActionableWebViewDialogFragment();
        addArguments(fragment, title, layoutResId);
        addArguments(fragment, null, contentURL);
        return fragment;
    }
    public static ActionableWebViewDialogFragment newOkCancelInstance(CharSequence title, int layoutResId, URL contentURL)
    {
        return newInstance(title, layoutResId, contentURL, null, "", null);
    }
    public static ActionableWebViewDialogFragment newInstance(CharSequence title, int layoutResId, URL contentURL, CharSequence positiveLabel, CharSequence negativeLabel, CharSequence neutralLabel)
    {
        ActionableWebViewDialogFragment fragment = newInstance(title, layoutResId, contentURL);
        addButtonLabelArguments(fragment, positiveLabel, negativeLabel, neutralLabel);
        return fragment;
    }

    public static ActionableWebViewDialogFragment newInstance(int titleResId)
    {
        return newInstance(titleResId, 0);
    }
    public static ActionableWebViewDialogFragment newInstance(int titleResId, int layoutResId)
    {
        ActionableWebViewDialogFragment fragment = new ActionableWebViewDialogFragment();
        addArguments(fragment, titleResId, layoutResId);
        return fragment;
    }
    public static ActionableWebViewDialogFragment newInstance(int titleResId, String contentData)
    {
        return newInstance(titleResId, 0, contentData);
    }
    public static ActionableWebViewDialogFragment newInstance(int titleResId,  int layoutResId, String contentData)
    {
        ActionableWebViewDialogFragment fragment = new ActionableWebViewDialogFragment();
        addArguments(fragment, titleResId, layoutResId);
        addArguments(fragment, contentData, null);
        return fragment;
    }
    public static ActionableWebViewDialogFragment newOkCancelInstance(int titleResId, int layoutResId, String contentData)
    {
        return newInstance(titleResId, layoutResId, contentData, 0, -1, 0);
    }
    public static ActionableWebViewDialogFragment newInstance(int titleResId,  int layoutResId, String contentData, int positiveLabelResId, int negativeLabelResId, int neutralLabelResId)
    {
        ActionableWebViewDialogFragment fragment = newInstance(titleResId, layoutResId, contentData);
        addButtonLabelArguments(fragment, positiveLabelResId, negativeLabelResId, neutralLabelResId);
        return fragment;
    }

    public static ActionableWebViewDialogFragment newInstance(int titleResId, URL contentURL)
    {
        return newInstance(titleResId, 0, contentURL);
    }
    public static ActionableWebViewDialogFragment newInstance(int titleResId,  int layoutResId, URL contentURL)
    {
        ActionableWebViewDialogFragment fragment = new ActionableWebViewDialogFragment();
        addArguments(fragment, titleResId, layoutResId);
        addArguments(fragment, null, contentURL);
        return fragment;
    }
    public static ActionableWebViewDialogFragment newOkCancelInstance(int titleResId, int layoutResId, URL contentURL)
    {
        return newInstance(titleResId, layoutResId, contentURL, 0, -1, 0);
    }
    public static ActionableWebViewDialogFragment newInstance(int titleResId,  int layoutResId, URL contentURL, int positiveLabelResId, int negativeLabelResId, int neutralLabelResId)
    {
        ActionableWebViewDialogFragment fragment = newInstance(titleResId, layoutResId, contentURL);
        addButtonLabelArguments(fragment, positiveLabelResId, negativeLabelResId, neutralLabelResId);
        return fragment;
    }

    // label==null means, use the resource. label=="" means, do not display the button.
    protected static DialogFragment addButtonLabelArguments(DialogFragment fragment, CharSequence positiveLabel, CharSequence negativeLabel, CharSequence neutralLabel)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        if(positiveLabel != null) {
            args.putCharSequence(ARG_POSITIVE_LABEL, positiveLabel);
        }
        if(negativeLabel != null) {
            args.putCharSequence(ARG_NEGATIVE_LABEL, negativeLabel);
        }
        if(neutralLabel != null) {
            args.putCharSequence(ARG_NEUTRAL_LABEL, neutralLabel);
        }
        return fragment;
    }
    // labelResId==-1 means, Do not include the button.
    protected static DialogFragment addButtonLabelArguments(DialogFragment fragment, int positiveLabelResId, int negativeLabelResId, int neutralLabelResId)
    {
        Bundle args = fragment.getArguments();
        if(args == null) {
            args = new Bundle();
            fragment.setArguments(args);
        }
        args.putInt(ARG_POSITIVE_LABEL_RESID, positiveLabelResId);
        args.putInt(ARG_NEGATIVE_LABEL_RESID, negativeLabelResId);
        args.putInt(ARG_NEUTRAL_LABEL_RESID, neutralLabelResId);
        return fragment;
    }

    public ActionableWebViewDialogFragment()
    {
    }


    private DialogInterface.OnClickListener positiveListener =
            new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int whichButton) {
                    ((ActionableAssertDialogListener) getActivity()).onPositiveClick(ActionableWebViewDialogFragment.this.getTag());
                }
            };
    private DialogInterface.OnClickListener negativeListener =
            new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int whichButton)
                {
                    ((ActionableAssertDialogListener) getActivity()).onNegativeClick(ActionableWebViewDialogFragment.this.getTag());
                }
            };
    private DialogInterface.OnClickListener neutralListener =
            new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int whichButton)
                {
                    ((ActionableAssertDialogListener) getActivity()).onNeutralClick(ActionableWebViewDialogFragment.this.getTag());
                }
            };


    protected AlertDialog.Builder buildDialogBuilder()
    {
        CharSequence positiveLabel = getArguments().getCharSequence(ARG_POSITIVE_LABEL);
        int positiveLabelResId = getArguments().getInt(ARG_POSITIVE_LABEL_RESID);
        CharSequence negativeLabel = getArguments().getCharSequence(ARG_NEGATIVE_LABEL);
        int negativeLabelResId = getArguments().getInt(ARG_NEGATIVE_LABEL_RESID);
        CharSequence neutralLabel = getArguments().getCharSequence(ARG_NEUTRAL_LABEL);
        int neutralLabelResId = getArguments().getInt(ARG_NEUTRAL_LABEL_RESID);

        AlertDialog.Builder dialogBuilder = super.buildDialogBuilder();

        if(positiveLabel != null) {
            if(positiveLabel.length() > 0) {
                dialogBuilder.setPositiveButton(positiveLabel, positiveListener);
            }
        } else {
            int posId = positiveLabelResId;
            if(posId == 0) {
                posId = R.string.basic_webview_dialog_ok;
            }
            if(posId >= 0) {
                dialogBuilder.setPositiveButton(posId, positiveListener);
            }
        }
        if(negativeLabel != null) {
            if(negativeLabel.length() > 0) {
                dialogBuilder.setNegativeButton(negativeLabel, negativeListener);
            }
        } else {
            int negId = negativeLabelResId;
            if(negId == 0) {
                negId = R.string.basic_webview_dialog_dismiss;
            }
            if(negId >= 0) {
                dialogBuilder.setNegativeButton(negId, negativeListener);
            }
        }
        if(neutralLabel != null) {
            if(neutralLabel.length() > 0) {
                dialogBuilder.setNeutralButton(neutralLabel, neutralListener);
            }
        } else {
            int neuId = neutralLabelResId;
            if(neuId == 0) {
                neuId = R.string.basic_webview_dialog_cancel;
            }
            if(neuId >= 0) {
                dialogBuilder.setNeutralButton(neuId, neutralListener);
            }
        }

        return dialogBuilder;
    }


    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try {
            ActionableAssertDialogListener mActivity = (ActionableAssertDialogListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement ActionableAssertDialog");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
//        mActivity = null;
    }


}

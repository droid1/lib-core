package ms.airy.lib.core.popup.util;


/**
 */
public final class PopupLoaderUtil
{
    private PopupLoaderUtil() {}


    public static boolean isIncludingThisPopup(String fileName)
    {
        if(fileName == null || !fileName.endsWith(".json")
                || fileName.endsWith("-old.json")
                || fileName.endsWith("-template.json")) {
            return false;
        } else {
            return true;
        }
    }


    // Not being used.
    // To be deleted.....
    // Popup file name: popup-011.json  --> Popup id: 11.
    // This function also acts as a filter.
    // Files with particular patterns (e.g., xxx-old.json) are filtered out.
    private static long getPopupIdFromFileName(String fileName)
    {
        if(fileName == null || !fileName.endsWith(".json")
                || fileName.endsWith("-old.json")
                || fileName.endsWith("-template.json")) {
            return 0L;
        }
        int i1 = fileName.lastIndexOf(".");
        if(i1 == -1) {
            i1 = fileName.length();  // No suffix/extension.
        }
        int i2 = fileName.lastIndexOf("-");
        if(i2 == -1) {
            return 0L;   // ???
        }
        String idStr = fileName.substring(i2+1, i1);
        long popupId = 0L;
        try {
            popupId = Long.parseLong(idStr);
        } catch(NumberFormatException e) {
            // ignore
            Log.w("Invalid popup Id: " + idStr, e);
        }
        return popupId;
    }


}

package ms.airy.lib.core.control;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;

import ms.airy.lib.core.time.timer.TimerState;


// TBD:
// Use interface ????
public abstract class TimerControl extends AnamorphicControl
{
    // Timer state.
    // ready, started, paused, stopped, ended, etc.. ????
//    private boolean started;
//    private boolean paused;
//    private boolean ended;
    private int state;

    // TBD:
    // Include timerRange here???
    // ....


    // Note: min <= max.
    // We support only "count down" timers.
    private long max;
    private long min;



    public TimerControl(Context context)
    {
        super(context);
        initControl();
    }
    public TimerControl(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        initControl();
    }
    public TimerControl(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        initControl();
    }

    private void initControl()
    {
        setFocusable(false); // For display only. No user input.

        // temporary
        state = TimerState.UNKNOWN;

        // ????
        max = 10000L;
        min = 0L;
    }


    public int getRepeats()
    {
        return state;
    }
    public void setRepeats(int state)
    {
        this.state = state;
        setDirty();  // ???
    }

    public long getMax()
    {
        return max;
    }
    public void setMax(long max)
    {
        // TBD: Validate????
        this.max = max;
    }

    public long getMin()
    {
        return min;
    }
    public void setMin(long min)
    {
        // TBD: Validate????
        this.min = min;
    }

    // Note that the max arg comes before min.
    public void setRange(long max, long min)
    {
        // TBD: Validate????
        this.max = max;
        this.min = min;
    }



    protected void initTimer()
    {
        initTimer(false);
    }
    protected void initTimer(boolean force)
    {
        // TBD:
        state = TimerState.INITIALIZED;
    }




    // TBD:
    // Timer life cycle methods....
    // ...

    // Name? init or reset?
    // this control can be re-used for multiple count-downs...
    // so, reset seems more appropriate...
    public void reset()
    {
        reset(TimerState.INITIALIZED);
    }
    public void reset(int state)
    {
        // Timer can be reset/initialized regardless of the current state.
        // Just initialize the timer with the current/default values.
        initTimer(true);
        setRepeats(state);
        // update();   // ???
    }
    public void reset(long max)
    {
        setMax(max);
        reset();
    }
    public void reset(long max, long min)
    {
        setRange(max, min);
        reset();
    }
    public void restart()
    {
        reset();
        start();
    }
    public void restart(long max)
    {
        reset(max);
        start();
    }
    public void restart(long max, long min)
    {
        reset(max, min);
        start();
    }
    public abstract void start();
    public void pause()
    {
        pause(-1L);
    }
    public abstract void pause(long extent);    // value? extent???
    public void resume()
    {
        resume(-1L);
    }
    public abstract void resume(long extent);
    public abstract void stop();
    public abstract void finish();

    // Note:
    // even without calling finish(),
    // state becomes FINISHED
    // when the timer counts down normally to val==min.
    // ...



    @Override
    public void update(boolean force, boolean layout)
    {
        if(force || layout || isDirty() ) {   // layout==true implies force==true.
            if(layout) {
                requestLayout();
            }
            invalidate();
            setDirty(false);  // ???
        } else {
            // ignore
        }
    }



    @Override
    public Parcelable onSaveInstanceState()
    {
        Log.d("onSaveInstanceState() Called.");
        Parcelable superState = super.onSaveInstanceState();
        SavedState savedState = new SavedState(superState);
        savedState.setState(getRepeats());
        savedState.setMax(getMax());
        savedState.setMin(getMin());
        return savedState;
    }

    @Override
    public void onRestoreInstanceState(Parcelable state)
    {
        Log.d("onRestoreInstanceState() Called.");
        SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(savedState.getSuperState());
        setRepeats(savedState.getState());
        setMax(savedState.getMax());
        setMin(savedState.getMin());
    }


    protected static class SavedState extends AnamorphicControl.SavedState implements Parcelable
    {
        private int state;
        private long max;
        private long min;
        // etc...

        public SavedState(Parcelable superState)
        {
            super(superState);
        }
        public SavedState(Parcel source)
        {
            super(source);
            state = source.readInt();
            max = source.readLong();
            min = source.readLong();
        }


        public int getState()
        {
            return state;
        }
        public void setState(int state)
        {
            this.state = state;
        }

        public long getMax()
        {
            return max;
        }
        public void setMax(long max)
        {
            this.max = max;
        }

        public long getMin()
        {
            return min;
        }
        public void setMin(long min)
        {
            this.min = min;
        }


        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            @Override
            public SavedState createFromParcel(Parcel source)
            {
                return new SavedState(source);
            }
            @Override
            public SavedState[] newArray(int size)
            {
                return new SavedState[size];
            }
        };

        @Override
        public void writeToParcel(Parcel dest, int flags)
        {
            super.writeToParcel(dest, flags);
            dest.writeInt(state);
            dest.writeLong(max);
            dest.writeLong(min);
        }


        // For debugging...
        @Override
        public String toString()
        {
            return "SavedState{" +
                    "state=" + state +
                    "max=" + max +
                    "min=" + min +
                    '}';
        }
    }
}

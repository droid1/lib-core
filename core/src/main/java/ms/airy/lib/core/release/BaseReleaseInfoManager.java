package ms.airy.lib.core.release;

import android.content.Context;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import ms.airy.lib.core.util.DateStringUtil;


// TBD:
// Does it make sense to have "release" planning classes
//   in the source code???
public class BaseReleaseInfoManager
{
    private static BaseReleaseInfoManager INSTANCE = null;
    public static BaseReleaseInfoManager getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new BaseReleaseInfoManager(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;

    // Release schedule.
    // Can we have only one???
    // Having two seem a bit wasteful/error-prone, but it makes certain methods more efficient...
    // ...
    // release name -> releaseInfo
    private final Map<String, PlannedReleaseInfo> releasePlanMap = new HashMap<String, PlannedReleaseInfo>();
    // Release date based on "my" timezone. (me==developer, not the app user)
    // release day -> releaseInfo
    // (Note that this assumes you cannot have more than one release on a single day!!!!)
    private final SortedMap<String, PlannedReleaseInfo> releaseDayMap = new TreeMap<String, PlannedReleaseInfo>();


    protected BaseReleaseInfoManager(Context context)
    {
        this.context = context;
        init();
    }
    private /* protected */ void init()
    {
        // TBD:
        // read release plans from the configs...
        // ...
        // or, just subclass this, and overwrite init()...
        // --> Or, just do this in ctor...
        // ...
    }

    protected final Context getContext()
    {
        return context;
    }


    // It can return null.
    // releaseName and releaseDay should be unique...
    public final PlannedReleaseInfo getPlannedReleaseForName(String releaseName)
    {
        return releasePlanMap.get(releaseName);
    }
    public final PlannedReleaseInfo getPlannedReleaseForDay(String releaseDay)
    {
        return releaseDayMap.get(releaseDay);
    }


    public final void addPlannedRelease(PlannedReleaseInfo releaseInfo)
    {
        releasePlanMap.put(releaseInfo.getReleaseName(), releaseInfo);
        releaseDayMap.put(releaseInfo.getReleaseDay(), releaseInfo);
    }


    // Note that since we only keep references,
    //     we can use either map to find the desired releaseInfo...

    public final void cancelPlannedRelease(String releaseName)
    {
        PlannedReleaseInfo releaseInfo = releasePlanMap.get(releaseName);
        if(releaseInfo != null) {
            releaseInfo.setCancelled(true);
        }
    }

    public final void reschedulePlannedRelease(String releaseName, String releaseDay)
    {
        PlannedReleaseInfo releaseInfo = releasePlanMap.get(releaseName);
        if(releaseInfo != null) {
            releaseInfo.setReleaseDay(releaseDay);
        }
    }

    public final void delayPlannedRelease(String releaseName, int months, int weeks, int days)
    {
        PlannedReleaseInfo releaseInfo = releasePlanMap.get(releaseName);
        if(releaseInfo != null) {
            releaseInfo.delayRelease(months, weeks, days);
        }
    }


    // temporary
    // Note that getNextPlannedRelease() does not really require fast up-to-date info...
    // We can cache it within the time range where this manager class is loaded...
    // (The "caching" works only with the no-arg version (based on "now")...)
    private PlannedReleaseInfo nextPlannedReleaseInfo = null;
    private boolean nextPlannedReleaseInfoCached = false;

    // Returns the upcoming planned release info, if any.
    public final PlannedReleaseInfo getNextPlannedRelease()
    {
        if(nextPlannedReleaseInfoCached == false) {
            long now = System.currentTimeMillis();
            nextPlannedReleaseInfo = getNextPlannedRelease(now);
            nextPlannedReleaseInfoCached = true;
        }
        return nextPlannedReleaseInfo;  // this could still be null.
    }
    public final PlannedReleaseInfo getNextPlannedRelease(long now)
    {
        // Note that releaseDayMap is sorted by releaseDay asc.
        for(String day : releaseDayMap.keySet()) {
            PlannedReleaseInfo relInfo = releaseDayMap.get(day);
            if(relInfo.getReleasedTime() == 0L && relInfo.isCancelled() == false) {
                long relTime = DateStringUtil.parseDate(day, "US/Pacific");   // "My" timezone.
                if(relTime > now) {
                    return relInfo;
                }
            }
        }
        // None found.
        return null;
    }

    // this includes already released or cancelled release plans...
    public final List<PlannedReleaseInfo> getAllPlannedReleases()
    {
        return new ArrayList<PlannedReleaseInfo>(releaseDayMap.values());
    }
    // Sorted by releaseDay asc.
    public final List<PlannedReleaseInfo> getUpcomingPlannedReleases()
    {
        long now = System.currentTimeMillis();
        return getUpcomingPlannedReleases(now);
    }
    public final List<PlannedReleaseInfo> getUpcomingPlannedReleases(long now)
    {
        List<PlannedReleaseInfo> list = new ArrayList<PlannedReleaseInfo>();
        for(String day : releaseDayMap.keySet()) {
            PlannedReleaseInfo relInfo = releaseDayMap.get(day);
            if(relInfo.getReleasedTime() == 0L && relInfo.isCancelled() == false) {
                long relTime = DateStringUtil.parseDate(day, "US/Pacific");   // "My" timezone.
                if(relTime > now) {
                    list.add(relInfo);
                }
            }
        }
        return list;
    }

}

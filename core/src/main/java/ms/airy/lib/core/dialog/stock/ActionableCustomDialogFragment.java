package ms.airy.lib.core.dialog.stock;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

import ms.airy.lib.core.R;
import ms.airy.lib.core.dialog.ActionableAssertDialogListener;
import ms.airy.lib.core.dialog.base.BasicCustomDialogFragment;


public class ActionableCustomDialogFragment extends BasicCustomDialogFragment
{
//    private ActionableAssertDialog mActivity = null;

    public static ActionableCustomDialogFragment newInstance(CharSequence title, int layoutResId)
    {
        ActionableCustomDialogFragment fragment = new ActionableCustomDialogFragment();
        addArguments(fragment, title, layoutResId);
        return fragment;
    }
    public static ActionableCustomDialogFragment newInstance(int titleResId, int layoutResId)
    {
        ActionableCustomDialogFragment fragment = new ActionableCustomDialogFragment();
        addArguments(fragment, titleResId, layoutResId);
        return fragment;
    }
    public ActionableCustomDialogFragment()
    {
    }


    protected AlertDialog.Builder buildDialogBuilder()
    {
        AlertDialog.Builder dialogBuilder = super.buildDialogBuilder();

        dialogBuilder.setPositiveButton(R.string.basic_custom_dialog_ok,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int whichButton) {
                        ((ActionableAssertDialogListener) getActivity()).onPositiveClick(ActionableCustomDialogFragment.this.getTag());
                    }
                }
        );
        dialogBuilder.setNegativeButton(R.string.basic_custom_dialog_dismiss,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int whichButton) {
                        ((ActionableAssertDialogListener) getActivity()).onNegativeClick(ActionableCustomDialogFragment.this.getTag());
                    }
                }
        );
        dialogBuilder.setNeutralButton(R.string.basic_custom_dialog_cancel,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int whichButton) {
                        ((ActionableAssertDialogListener) getActivity()).onNeutralClick(ActionableCustomDialogFragment.this.getTag());
                    }
                }
        );

        return dialogBuilder;
    }


    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try {
            ActionableAssertDialogListener mActivity = (ActionableAssertDialogListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement ActionableAssertDialog");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
//        mActivity = null;
    }


}

package ms.airy.lib.core.dialog;


// Just use ActionableXXXListeners?????
public interface AlertDialogDismissalCallback
{
    // This includes positive and item click, etc.
    // The semantics is really app-dependent....
    void onDialogDismissed();
    // No callback when the dialog action is not positive-nature...
//    // This includes negative and neutral click etc...
//    // Or, back button, etc.????  (Back button does not generate event....)
//    void onDialogCanceled();
}

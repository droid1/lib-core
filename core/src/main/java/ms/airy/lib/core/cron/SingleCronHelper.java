package ms.airy.lib.core.cron;


/**
 * Parsing a cron expression every time when needed might be too expensive.
 * In many cases, the app might have just one expression (or a small number).
 * Cache the parser when it's parsed once.
 */
public abstract class SingleCronHelper
{
    // Its (concrete) subclasses should be singletons...

//    private static SingleCronHelper INSTANCE = null;
//
////    public static MultiCronHelper getInstance(Context context)
////    {
////        if(INSTANCE == null) {
////            INSTANCE = new MultiCronHelper(context.getApplicationContext());
////        }
////        return INSTANCE;
////    }
////
////    private final Context context;
////    private MultiCronHelper(Context context)
////    {
////        this.context = context;
////    }
//
//    public static SingleCronHelper getInstance()
//    {
//        if(INSTANCE == null) {
//            INSTANCE = new SingleCronHelper();
//        }
//        return INSTANCE;
//    }

    protected SingleCronHelper()
    {
        // Initialize parser here? Or, in getParser() ???
    }

    // Lazy initialized...
    protected CronTimeParsable parser = null;


    // To be overridden in subclasses.
    protected abstract CronTimeParsable getParser();
//    {
//        if(parser == null) {
//            // TBD:
//            // Get the cron expression from the stystem config...
//            // initialize the parser..
//            // ...
//        }
//        return parser;
//    }

    public Long getNextCronTime()
    {
        CronTimeParsable parser = getParser();
        if(parser != null) {
            return parser.getNextCronTime();
        }
        return null;
    }
    public Long getNextCronTime(long now)
    {
        CronTimeParsable parser = getParser();
        if(parser != null) {
            return parser.getNextCronTime(now);
        }
        return null;
    }

    // temporary
    public Long getNextCronTimeShifted(long delta)
    {
        CronTimeParsable parser = getParser();
        if(parser != null) {
            if(parser instanceof CronExpressionParser) {
                return ((CronExpressionParser) parser).getNextCronTimeShifted(delta);
            } else if(parser instanceof CronExpressionListParser) {
                 return ((CronExpressionListParser) parser).getNextCronTimeShifted(delta);
            } else {
                // ???
                return parser.getNextCronTime() + delta;
            }
        }
        return null;
    }
    public Long getNextCronTimeShifted(long now, long delta)
    {
        CronTimeParsable parser = getParser();
        if(parser != null) {
            if(parser instanceof CronExpressionParser) {
                return ((CronExpressionParser) parser).getNextCronTimeShifted(now, delta);
            } else if(parser instanceof CronExpressionListParser) {
                    return ((CronExpressionListParser) parser).getNextCronTimeShifted(now, delta);
            } else {
                // ???
                return parser.getNextCronTime(now) + delta;
            }
        }
        return null;
    }


    public Long getPreviousCronTime()
    {
        CronTimeParsable parser = getParser();
        if(parser != null) {
            return parser.getPreviousCronTime();
        }
        return null;
    }
    public Long getPreviousCronTime(long now)
    {
        CronTimeParsable parser = getParser();
        if(parser != null) {
            return parser.getPreviousCronTime(now);
        }
        return null;
    }

}

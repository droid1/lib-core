package ms.airy.lib.core.feedback.client;

import ms.airy.lib.core.feedback.common.SurveyEntry;


public interface OnlineSurveyClient
{
    SurveyEntry getSpecialSurvey();
    SurveyEntry getDailySurvey(long time);
    SurveyEntry getSurvey(long id);
}

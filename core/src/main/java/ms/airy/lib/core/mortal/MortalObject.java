package ms.airy.lib.core.mortal;

import java.util.Timer;
import java.util.TimerTask;


// TBD:
// Generalize this to time-dependent multi-value object?
// e.g., using Timer by changing values on a regular interval.
//       or, more generally, using a handler.
public class MortalObject<T>
{
    private T currentValue;
    private final T livingValue;
    private final T afterlifeValue;
    private final long lifetime;    // ttl
    private final boolean resetOnlyIfUnchanged;
    // if set to true (before the lifetime ends),
    // the currentValue (whatever the current value is) will not be reset to afterlifeValue.
    // This is one way flag.
    // It starts as false, and once it is set to true, it cannot be reset to false.
    private boolean stopped;

    // TBD:
    // callback? when the value becomes dead?
    // ...

    public MortalObject(T livingValue, T afterlifeValue, long lifetime)
    {
        this(livingValue, afterlifeValue, lifetime, false);
    }
    public MortalObject(T livingValue, T afterlifeValue, long lifetime, boolean resetOnlyIfUnchanged)
    {
        // TBD: Check if (livingValue != afterlifeValue) ???
        this.livingValue = livingValue;
        this.afterlifeValue = afterlifeValue;
        this.lifetime = lifetime;
        this.resetOnlyIfUnchanged = resetOnlyIfUnchanged;
        this.stopped = false;

        // TBD: Use a separate start() method ???
        //   ( --> doing this way requires additional methods such as "isStarted()", etc.
        // For now, object construction automatically starts ticking.
        startTicking();
    }

    public T getValue()
    {
        return currentValue;
    }
    public void setValue(T currentValue)
    {
        this.currentValue = currentValue;
    }

    public boolean isStopped()
    {
        return stopped;
    }
    public void setStopped()
    {
        this.stopped = true;
    }

    // TBD: Use a different thread?
    private void startTicking()
    {
        this.currentValue = this.livingValue;
        new Timer().schedule(new TimerTask()
        {
            @Override
            public void run()
            {
                if(MortalObject.this.stopped == true) {
                    // do nothing.
                    Log.i("MortalObject.startTicking().run(). Current value will not be reset because stopped == true.");
                } else {
                    // Which is better, (1) or (2) ???
                    if (MortalObject.this.resetOnlyIfUnchanged == true) {
                        // [1]
                        // TBD: Use equals() for value comparison?????
                        // "reference ==" should also be fine since it implies the value has not changed.
                        // the problematic situation is
                        //     when the currentValue has been reassigned by a different object but with the same livingValue...
                        //if (MortalObject.this.currentValue == MortalObject.this.livingValue) {
                        if ((MortalObject.this.currentValue == MortalObject.this.livingValue)
                                || (MortalObject.this.currentValue != null
                                && MortalObject.this.currentValue.equals(MortalObject.this.livingValue)
                        )) {
                            MortalObject.this.currentValue = MortalObject.this.afterlifeValue;
                            Log.d("MortalObject.startTicking().run(). Current value changed to the after-life value.");
                        } else {
                            // The current value has changed (e.g., by somebody else)
                            // Do not change.
                            Log.i("MortalObject.startTicking().run(). Current value will not be reset because the initial value has been changed.");
                        }
                    } else {
                        // [2]
                        MortalObject.this.currentValue = MortalObject.this.afterlifeValue;
                        Log.d("MortalObject.startTicking().run(). Current value changed to the after-life value.");
                    }
                }
            }
        }, lifetime);
    }

}

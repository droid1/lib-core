package ms.airy.lib.core.help.client.callback;

import ms.airy.lib.core.help.client.AsyncHelpCallback;


/**
 */
public abstract class AbstractAsyncOnlineHelpCallback implements AsyncHelpCallback
{

    @Override
    public void onGetSectionContent(long sectionId, String content)
    {
        if(Log.I) Log.i("AbstractAsyncOnlineHelpCallback.onGetSectionContent(). sectionId = " + sectionId + ": content = " + content);


    }


}

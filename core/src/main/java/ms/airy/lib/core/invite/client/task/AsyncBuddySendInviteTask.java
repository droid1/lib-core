package ms.airy.lib.core.invite.client.task;

import android.os.AsyncTask;

import ms.airy.lib.core.invite.client.BuddyInviteClient;
import ms.airy.lib.core.invite.client.factory.InviteClientFactory;


/**
 */
public class AsyncBuddySendInviteTask extends AsyncTask<String, Void, Long>
{
    private final AsyncTaskCallback callback;
    public AsyncBuddySendInviteTask(AsyncTaskCallback callback)
    {
        this.callback = callback;
    }

    // Store input params during processing
    private String recipient;



    @Override
    protected Long doInBackground(String... params)
    {
        recipient = (String) params[0];

        BuddyInviteClient client = InviteClientFactory.getInstance().getInviteClient();

        Long id = client.sendInvite(recipient);

        return id;
    }

    @Override
    protected void onPostExecute(Long id)
    {
        // super.onPostExecute(aLong);

        this.callback.onSendInvite(id, recipient);
    }




    // Callback interface
    public static interface AsyncTaskCallback
    {
        void onSendInvite(long invitedId, String recipient);
    }

}

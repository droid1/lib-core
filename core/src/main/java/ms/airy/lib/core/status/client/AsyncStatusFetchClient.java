package ms.airy.lib.core.status.client;

import ms.airy.lib.core.status.common.StatusMessage;

import java.util.List;


/**
 */
public interface AsyncStatusFetchClient
{
    // TBD: Add filters such as type, etc. ???

    // void fetchMessage(long id);
    void fetchStatusMessage(long id);
    void fetchNewMessages();
    // void fetchNewStatusMessages();
    void fetchMessages(long since);
    // void fetchStatusMessages(long since);
}

package ms.airy.lib.core.common;

import java.util.HashMap;
import java.util.Map;


// "Enum"
public final class ImageSize
{
//      m    h   xh  xxh
//
//      8   12   16   24        pico
//     12   18   24   36        micro         pico
//     16   24   32   48        mini          micro          pico
//     24   36   48   72        tiny          mini           micro
//     32   48   64   96        (petite)      -              -
//
//     36   54   72  108        small         tiny           mini
//     48   72   96  144        medium        small          tiny
//     64   96  128  192        large         medium         small        tiny
//     96  144  192  288        xlarge        large          medium       small
//    128  192  256  384        xxllarge      xlarge         large        medium
////  192  288  384  576      --xxxlarge    --xxlarge      --xlarge     --large
//    256  384  512  768        xxxlarge      xxlarge        xlarge       large
//    512  768 1024 1536        xxxxlarge     xxxlarge       xxlarge      xlarge
//   1024 1536 2048 3072                      xxxxlarge      xxxlarge     xxlarge


    // Just use null?
    public static final String SIZE_UNKNOWN = "unknown";

    // Primarily to be used as a value for custom "flag_size" attr.
    public static final String SIZE_MICRO = "micro";
    public static final String SIZE_MINI = "mini";
    public static final String SIZE_TINY = "tiny";
    public static final String SIZE_SMALL = "small";
    public static final String SIZE_MEDIUM = "medium";
    public static final String SIZE_LARGE = "large";
    public static final String SIZE_XLARGE = "xlarge";
    public static final String SIZE_XXLARGE = "xxlarge";
    public static final String SIZE_XXXLARGE = "xxxlarge";
    public static final String SIZE_XXXXLARGE = "xxxxlarge";

    private ImageSize() {};


    // for efficiency...
    private static final Map<String, String> decrMap = new HashMap<String,String>();
    static {
        decrMap.put(SIZE_MICRO, SIZE_MICRO);
        decrMap.put(SIZE_MINI, SIZE_MICRO);
        decrMap.put(SIZE_TINY, SIZE_MINI);
        decrMap.put(SIZE_SMALL, SIZE_TINY);
        decrMap.put(SIZE_MEDIUM, SIZE_SMALL);
        decrMap.put(SIZE_LARGE, SIZE_MEDIUM);
        decrMap.put(SIZE_XLARGE, SIZE_LARGE);
        decrMap.put(SIZE_XXLARGE, SIZE_XLARGE);
        decrMap.put(SIZE_XXXLARGE, SIZE_XXLARGE);
        decrMap.put(SIZE_XXXXLARGE, SIZE_XXXLARGE);
    }


    public static boolean isValid(String size)
    {
        return decrMap.containsKey(size);
    }

    public static String getOneSizeSmaller(String size)
    {
        return decrMap.get(size);
    }

    public static String getTwoSizeSmaller(String size)
    {
        return decrMap.get(decrMap.get(size));
    }
    public static String getThreeSizeSmaller(String size)
    {
        return decrMap.get(decrMap.get(decrMap.get(size)));
    }
    public static String getFourSizeSmaller(String size)
    {
        return decrMap.get(decrMap.get(decrMap.get(decrMap.get(size))));
    }


}

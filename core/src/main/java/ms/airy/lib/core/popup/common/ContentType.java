package ms.airy.lib.core.popup.common;


/**
 */
public final class ContentType
{
    // public static final String CT_UNKNOWN = "unknown";
    public static final String CT_TEXT = "text";
    public static final String CT_HTML = "html";
    public static final String CT_LIST = "list";    // single choice list
    public static final String CT_MULTI = "multi";  // multiple choice list
    // etc. ...

    private ContentType() {}


    public static boolean isValid(String contentType)
    {
        if(CT_TEXT.equalsIgnoreCase(contentType)
                || CT_HTML.equalsIgnoreCase(contentType)
                || CT_LIST.equalsIgnoreCase(contentType)
                || CT_MULTI.equalsIgnoreCase(contentType)
                ) {
            return true;
        } else {
            return false;
        }
    }

    public static String toCanonicalString(String contentType)
    {
        if(isValid(contentType)) {
            return contentType.toLowerCase();
        } else {
            // return CT_UNKNOWN;
            return null;
        }
    }

    public static boolean isText(String contentType)
    {
        return CT_TEXT.equalsIgnoreCase(contentType);
    }
    public static boolean isHtml(String contentType)
    {
        return CT_HTML.equalsIgnoreCase(contentType);
    }
    public static boolean isList(String contentType)
    {
        return (CT_LIST.equalsIgnoreCase(contentType)
                || CT_MULTI.equalsIgnoreCase(contentType));
    }
    public static boolean isSingleChoiceList(String contentType)
    {
        return CT_LIST.equalsIgnoreCase(contentType);
    }
    public static boolean isMultiChoiceList(String contentType)
    {
        return CT_MULTI.equalsIgnoreCase(contentType);
    }

}

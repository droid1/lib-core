package ms.airy.lib.core.invite.client.task;

import android.os.AsyncTask;

import java.util.List;

import ms.airy.lib.core.invite.client.BuddyInviteClient;
import ms.airy.lib.core.invite.client.factory.InviteClientFactory;


/**
 */
public class AsyncBuddyGetRecipientsTask extends AsyncTask<Integer, Void, List<String>>
{
    private final AsyncTaskCallback callback;
    public AsyncBuddyGetRecipientsTask(AsyncTaskCallback callback)
    {
        this.callback = callback;
    }

    // Store input params during processing
    private int status;


    @Override
    protected List<String> doInBackground(Integer... params)
    {
        status = (Integer) params[0];

        BuddyInviteClient client = InviteClientFactory.getInstance().getInviteClient();

        List<String> recipients = client.getRecipients(status);

        return recipients;
    }

    @Override
    protected void onPostExecute(List<String> recipients)
    {
        // super.onPostExecute(aLong);

        this.callback.onGetRecipients(recipients, status);
    }




    // Callback interface
    public static interface AsyncTaskCallback
    {
        void onGetRecipients(List<String> recipients, int status);
    }

}

package ms.airy.lib.core.help.content;


/**
*/
public interface ContentLoaderCallback
{
    void onOnlineContentLoaded(long sectionId, String content);
}

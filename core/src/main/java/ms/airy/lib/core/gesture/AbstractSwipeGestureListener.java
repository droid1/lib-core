package ms.airy.lib.core.gesture;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;



/**
 */
public abstract class AbstractSwipeGestureListener implements OnTouchListener, SwipeGestureListener
{
    private final GestureDetector gestureDetector;

    public AbstractSwipeGestureListener(Context context)
    {
        gestureDetector = new GestureDetector(context, new GestureListener());
    }


    @Override
    public boolean onTouch(View v, MotionEvent event)
    {
        return gestureDetector.onTouchEvent(event);
    }

    @Override
    public void onSwipeRightward()
    {
        Log.v("onSwipeRightward()");
    }

    @Override
    public void onSwipeLeftward()
    {
        Log.v("onSwipeLeftward()");
    }

    @Override
    public void onSwipeUpward()
    {
        Log.v("onSwipeUpward()");
    }

    @Override
    public void onSwipeDownward()
    {
        Log.v("onSwipeDownward()");
    }


    private final class GestureListener extends GestureDetector.SimpleOnGestureListener
    {
        // TBD:
        private static final int SWIPE_THRESHOLD = 100;
        private static final int SWIPE_VELOCITY_THRESHOLD = 100;

        @Override
        public boolean onDown(MotionEvent e)
        {
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
        {
            boolean result = false;
            try {
                float diffY = e2.getY() - e1.getY();
                float diffX = e2.getX() - e1.getX();
                if (Math.abs(diffX) > Math.abs(diffY)) {
                    if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                        if (diffX > 0) {
                            onSwipeRightward();
                        } else {
                            onSwipeLeftward();
                        }
                        result = true;
                    }
                } else {
                    if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                        if (diffY > 0) {
                            onSwipeDownward();
                        } else {
                            onSwipeUpward();
                        }
                        result = true;
                    }
                }
            } catch (Exception e) {
                Log.e("Error while processing fling.", e);
            }
            return result;
        }
    }




}

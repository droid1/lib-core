package ms.airy.lib.core.ui.dashbar;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;


// To be deleted?????
// http://developer.android.com/guide/topics/fundamentals/fragments.html
public class DashBarFragment extends Fragment
{
    // Menu list
    private List<String> menuList;

    // Required by the framework.
    public DashBarFragment()
    {
        // ...
        menuList = new ArrayList<String>();
        // ...
    }

    
    public static DashBarFragment newInstance()
    {
        if(Log.D) Log.d("");
        DashBarFragment fragment = new DashBarFragment();
        // ....
        Bundle args = new Bundle();
        //args.putInt("index", index);
        fragment.setArguments(args);
        // ....
        return fragment;
    }

    
    // ...
    private void addMember(String member)
    {
        // ...
    }
    private List<String> getMemberList()
    {
        // ...
        return null;
    }
    // ...
    
    
    
    @Override
    public void onAttach(Activity activity)
    {
        if(Log.D) Log.d("onAttach()");
        super.onAttach(activity);
        // TBD: Save the parent activity???
        // ...
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        if(Log.D) Log.d("onCreate()");
        super.onCreate(savedInstanceState);
        
        // Keep this permanently as long as the parent Activity is active.
        setRetainInstance(true);
        
        
        // TBD: Read the User/Family DB. ??? Or Do that in onActivityCreated() ???
        // ...
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState)
    {
        if(Log.D) Log.d("onCreateView()");
        //return super.onCreateView(inflater, container, savedInstanceState);

        
        
        ViewGroup view = new LinearLayout(getActivity());
        ((LinearLayout) view).setOrientation(LinearLayout.VERTICAL);
        
        Button btn1 = new Button(getActivity());
        btn1.setText("Dash Button");
        view.addView(btn1);

        TextView tv2 = new TextView(getActivity());
        tv2.setText("ddd");
        tv2.setWidth(100);
        view.addView(tv2);

        
        return view;
        
        
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.dashbar_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        if(Log.D) Log.d("onActivityCreated()");
        super.onActivityCreated(savedInstanceState);
        // TBD: Initialize the fragment fields with the data returned from activity???
        // ....
    }

    @Override
    public void onStart()
    {
        if(Log.D) Log.d("onStart()");
        super.onStart();
        
        // TBD:
        // Initialize ....
        // ...
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(Log.D) Log.d("onActivityResult()");
        super.onActivityResult(requestCode, resultCode, data);
        
        // TBD:
        // ...
        
    }


    
    
    @Override
    public void onDetach()
    {
        // TODO Auto-generated method stub
        super.onDetach();
    }

    @Override
    public View getView()
    {
        // TODO Auto-generated method stub
        return super.getView();
    }

    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        // TODO Auto-generated method stub
        return super.onContextItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
            ContextMenuInfo menuInfo)
    {
        // TODO Auto-generated method stub
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        // TODO Auto-generated method stub
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // TODO Auto-generated method stub
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu)
    {
        // TODO Auto-generated method stub
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        // TODO Auto-generated method stub
        super.onSaveInstanceState(outState);
    }

    
    
    
}

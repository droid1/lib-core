package ms.airy.lib.core.feedback.client.task;

import android.os.AsyncTask;

import java.util.List;

import ms.airy.lib.core.feedback.client.OnlineResponseClient;
import ms.airy.lib.core.feedback.client.factory.FeedbackClientFactory;
import ms.airy.lib.core.feedback.common.ResponseEntry;


/**
 */
public class AsyncResponseGetNewResponsesTask extends AsyncTask<Void, Void, List<Long>>
{

    private final AsyncTaskCallback callback;
    public AsyncResponseGetNewResponsesTask(AsyncTaskCallback callback)
    {
        this.callback = callback;
    }


    // Store input params during processing
    private long id;

    @Override
    protected List<Long> doInBackground(Void... params)
    {
        OnlineResponseClient client = FeedbackClientFactory.getInstance().getResponseClient();

        List<Long> responses = client.getNewResponses();

        return responses;
    }


    @Override
    protected void onPostExecute(List<Long> responses)
    {
        // super.onPostExecute(aLong);

        this.callback.onGetNewResponses(responses);
    }





    // Callback interface
    public static interface AsyncTaskCallback
    {
        void onGetNewResponses(List<Long> responses);
    }

}

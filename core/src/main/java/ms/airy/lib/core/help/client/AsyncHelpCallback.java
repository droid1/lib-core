package ms.airy.lib.core.help.client;


/**
 */
public interface AsyncHelpCallback
{
    void onGetSectionContent(long sectionId, String content);

}

package ms.airy.lib.core.cron;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Parsing a cron expression every time when needed might be too expensive.
 * In many cases, the app might have just one expression (or a small number).
 * Cache the parser when it's parsed once.
 *
 * Note that we only support UTC.
 * for now...
 */
public final class MultiCronHelper
{
    private static MultiCronHelper INSTANCE = null;

//    public static MultiCronHelper getInstance(Context context)
//    {
//        if(INSTANCE == null) {
//            INSTANCE = new MultiCronHelper(context.getApplicationContext());
//        }
//        return INSTANCE;
//    }
//
//    private final Context context;
//    private MultiCronHelper(Context context)
//    {
//        this.context = context;
//    }

    public static MultiCronHelper getInstance()
    {
        if(INSTANCE == null) {
            INSTANCE = new MultiCronHelper();
        }
        return INSTANCE;
    }

    private MultiCronHelper()
    {
    }

    // cron expression -> parser mappping.
    // Note that a single expression string is converted to List<String> of one element.
    private final Map<List<String>, CronTimeParsable> parsers = new HashMap<List<String>, CronTimeParsable>();


    private CronTimeParsable getParser(List<String> exprList)
    {
        if(exprList == null || exprList.isEmpty()) {
            Log.w("Invalid arg.");
            return null;   // ???
        }
        if(parsers.containsKey(exprList)) {
            Log.d("Parser found in the cache for exprList = " + exprList);
            return parsers.get(exprList);
        } else {
            CronTimeParsable parser = null;
            if(exprList.size() == 1) {
                String expr = exprList.get(0);
                parser = new CronExpressionParser(expr);
            } else {
                parser = new CronExpressionListParser(exprList);
            }
            if(parser != null && parser.isParsed()) {
                Log.d("Parser stored in the cache for exprList = " + exprList);
                parsers.put(exprList, parser);
            } else {
                // ???
                Log.w("Failed to create a parser.");
            }
            return parser;
        }
    }
    private CronTimeParsable getParser(String expression)
    {
        if (expression == null || expression.isEmpty()) {
            Log.w("Invalid arg. Cron expression cannot be null/empty.");
            return null;   // ???
        }
//        List<String> exprList = Arrays.asList(new String[]{expression});
        List<String> exprList = new ArrayList<String>();
        exprList.add(expression);
        return getParser(exprList);
    }


    public Long getNextCronTime(List<String> exprList)
    {
        CronTimeParsable parser = getParser(exprList);
        if(parser != null) {
            return parser.getNextCronTime();
        }
        return null;
    }
    public Long getNextCronTime(List<String> exprList, long now)
    {
        CronTimeParsable parser = getParser(exprList);
        if(parser != null) {
            return parser.getNextCronTime(now);
        }
        return null;
    }

    public Long getNextCronTime(String expression)
    {
        CronTimeParsable parser = getParser(expression);
        if(parser != null) {
            return parser.getNextCronTime();
        }
        return null;
    }
    public Long getNextCronTime(String expression, long now)
    {
        CronTimeParsable parser = getParser(expression);
        if(parser != null) {
            return parser.getNextCronTime(now);
        }
        return null;
    }


    // temporary
    public Long getNextCronTimeShifted(List<String> exprList, long delta)
    {
        CronTimeParsable parser = getParser(exprList);
        if(parser != null) {
            if(parser instanceof CronExpressionParser) {
                return ((CronExpressionParser) parser).getNextCronTimeShifted(delta);
            } else if(parser instanceof CronExpressionListParser) {
                return ((CronExpressionListParser) parser).getNextCronTimeShifted(delta);
            } else {
                // ???
                return parser.getNextCronTime() + delta;
            }
        }
        return null;
    }
    public Long getNextCronTimeShifted(List<String> exprList, long now, long delta)
    {
        CronTimeParsable parser = getParser(exprList);
        if(parser != null) {
            if(parser instanceof CronExpressionParser) {
                return ((CronExpressionParser) parser).getNextCronTimeShifted(now, delta);
            } else if(parser instanceof CronExpressionListParser) {
                return ((CronExpressionListParser) parser).getNextCronTimeShifted(now, delta);
            } else {
                // ???
                return parser.getNextCronTime(now) + delta;
            }
        }
        return null;
    }

    public Long getNextCronTimeShifted(String expression, long delta)
    {
        CronTimeParsable parser = getParser(expression);
        if(parser != null) {
            if(parser instanceof CronExpressionParser) {
                return ((CronExpressionParser) parser).getNextCronTimeShifted(delta);
            } else if(parser instanceof CronExpressionListParser) {
                return ((CronExpressionListParser) parser).getNextCronTimeShifted(delta);
            } else {
                // ???
                return parser.getNextCronTime() + delta;
            }
        }
        return null;
    }
    public Long getNextCronTimeShifted(String expression, long now, long delta)
    {
        CronTimeParsable parser = getParser(expression);
        if(parser != null) {
            if(parser instanceof CronExpressionParser) {
                return ((CronExpressionParser) parser).getNextCronTimeShifted(now, delta);
            } else if(parser instanceof CronExpressionListParser) {
                return ((CronExpressionListParser) parser).getNextCronTimeShifted(now, delta);
            } else {
                // ???
                return parser.getNextCronTime(now) + delta;
            }
        }
        return null;
    }



    public Long getPreviousCronTime(List<String> exprList)
    {
        CronTimeParsable parser = getParser(exprList);
        if(parser != null) {
            return parser.getPreviousCronTime();
        }
        return null;
    }
    public Long getPreviousCronTime(List<String> exprList, long now)
    {
        CronTimeParsable parser = getParser(exprList);
        if(parser != null) {
            return parser.getPreviousCronTime(now);
        }
        return null;
    }

    public Long getPreviousCronTime(String expression)
    {
        CronTimeParsable parser = getParser(expression);
        if(parser != null) {
            return parser.getPreviousCronTime();
        }
        return null;
    }
    public Long getPreviousCronTime(String expression, long now)
    {
        CronTimeParsable parser = getParser(expression);
        if(parser != null) {
            return parser.getPreviousCronTime(now);
        }
        return null;
    }

}

package ms.airy.lib.core.control.common;


// To be used by "anamorphic" controls.
public final class ControlAxis
{
    // Control alignment. horz vs vert.
    public final static int AXIS_UNDEFINED = 0;
    public final static int AXIS_HORIZONTAL = 1;
    public final static int AXIS_VERTICAL = 2;
    // Special consts.
    // Default value for a given control type???
    public final static int AXIS_CONTROL_DEFAULT = 4;
    // TBD: how to get the parent value for a given control????
    public final static int AXIS_MATCH_PARENT = 8;
    public final static int AXIS_OPPOSITE_PARENT = 16;
    // ...

    // These are primarily used for setting layout attribute values.
    public final static String STR_AXIS_HORIZONTAL = "horizontal";
    public final static String STR_AXIS_VERTICAL = "vertical";
    public final static String STR_AXIS_CONTROL_DEFAULT = "default";
    public final static String STR_AXIS_MATCH_PARENT = "match_parent";
    public final static String STR_AXIS_OPPOSITE_PARENT = "opposite_parent";


    // Static vars/methods only.
    private ControlAxis() {}


    public static boolean isValidAxis(int axis)
    {
        // tbd: what about match_parent, opposite_parent ???
        switch(axis) {
            case AXIS_HORIZONTAL:
            case AXIS_VERTICAL:
                return true;
        }
        return false;
    }
    public static boolean isValidValue(int axis)
    {
        switch(axis) {
            case AXIS_HORIZONTAL:
            case AXIS_VERTICAL:
            case AXIS_CONTROL_DEFAULT:
            case AXIS_MATCH_PARENT:
            case AXIS_OPPOSITE_PARENT:
                return true;
        }
        return false;
    }

    public static int opposite(int axis)
    {
        switch(axis) {
            case AXIS_HORIZONTAL:
                return AXIS_VERTICAL;
            case AXIS_VERTICAL:
                return AXIS_HORIZONTAL;
        }
        // ????
        return axis;
    }


    // To parse the custom attr from layout files.
    public static int convertAxisStringToInt(String strAxis)
    {
        if(strAxis == null) {
            return AXIS_UNDEFINED;
        } else {
            if(strAxis.equals(STR_AXIS_HORIZONTAL)) {
                return AXIS_HORIZONTAL;
            } else if(strAxis.equals(STR_AXIS_VERTICAL)) {
                return AXIS_VERTICAL;
            } else if(strAxis.equals(STR_AXIS_CONTROL_DEFAULT)) {
                return AXIS_CONTROL_DEFAULT;
            } else if(strAxis.equals(STR_AXIS_MATCH_PARENT)) {
                return AXIS_MATCH_PARENT;
            } else if(strAxis.equals(STR_AXIS_OPPOSITE_PARENT)) {
                return AXIS_OPPOSITE_PARENT;
            } else {
                Log.w("Undefined axis name: " + strAxis);
                return AXIS_UNDEFINED;
            }
        }
    }


}

package ms.airy.lib.core.backup.base;

import android.app.backup.BackupAgent;
import android.app.backup.BackupDataInput;
import android.app.backup.BackupDataOutput;
import android.os.ParcelFileDescriptor;

import java.io.IOException;


// TBD:
// http://developer.android.com/guide/topics/data/backup.html
public class BaseBackupAgent extends BackupAgent
{
    public BaseBackupAgent()
    {
    }

    @Override
    public void onBackup(ParcelFileDescriptor oldState, BackupDataOutput data, ParcelFileDescriptor newState) throws IOException
    {


    }

    @Override
    public void onRestore(BackupDataInput data, int appVersionCode, ParcelFileDescriptor newState) throws IOException
    {


    }

}

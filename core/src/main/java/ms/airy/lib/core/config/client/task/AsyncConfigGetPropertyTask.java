package ms.airy.lib.core.config.client.task;

import android.os.AsyncTask;

import ms.airy.lib.core.config.client.OnlineConfigClient;
import ms.airy.lib.core.config.client.factory.ConfigClientFactory;


/**
 */
public class AsyncConfigGetPropertyTask extends AsyncTask<String, Void, Object>
{
    private final AsyncTaskCallback callback;

    public AsyncConfigGetPropertyTask(AsyncTaskCallback callback)
    {
        this.callback = callback;
    }

    // Store input params during processing...
    private String key;

    @Override
    protected Object doInBackground(String... params)
    {
        key = (String) params[0];

        OnlineConfigClient client = ConfigClientFactory.getInstance().getConfigClient();

        Object value = client.getProperty(key);

        return value;
    }

    @Override
    protected void onPostExecute(Object value)
    {
        this.callback.onGetProperty(value, key);
    }



    // Callback interface
    public static interface AsyncTaskCallback
    {
        void onGetProperty(Object value, String key);
    }

}

package ms.airy.lib.core.cron;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Iterator;
import java.util.NavigableSet;
import java.util.TimeZone;
import java.util.TreeSet;


/**
 * Parses the cron expression:
 * http://en.wikipedia.org/wiki/Cron
 */
public class CronExpressionParser implements CronTimeParsable
{
    // Input cron expression
    private final String expression;
    
    // Time is to be interpreted based on this TimeZone.
    // Currently, it's not being used...
    private final TimeZone timezone;
    
    // Set to true once the expression is parsed. (We could just use cronFields1 == null?)
    private boolean parsed = false;

    // The parsed cron expression fields. Array of sorted sets.
    private NavigableSet<Integer>[] cronFields1;
    // New implementation, using long integer bitmasks.
    private long[] cronFields2;

    // Ctor. Expression/Timezone are read-only vars.
    public CronExpressionParser(String expression) 
    {
        this(expression, null);
    }
    public CronExpressionParser(String expression, String timezoneID) 
    {
        this.expression = expression;
        if(timezoneID != null && !timezoneID.isEmpty()) {
            this.timezone = TimeZone.getTimeZone(timezoneID);
        } else {
            // Use GMT, by default.
            this.timezone = TimeZone.getTimeZone("UTC");
            //this.timezone = TimeZone.getDefault();
        }
        try {
            // Now, using the new implementation.
//            cronFields1 = parse1(this.expression);
            cronFields2 = parse2(this.expression);
            parsed = true;
        } catch(CronExpressionParseException e) {
            if(Log.W) Log.w("Failed to parse the cron expression: " + this.expression, e);
            // cronFields1 = null;  // ???
        }
    }

    // No setters.
    public String getExpression()
    {
        return expression;
    }
    public String getTimeZoneID()
    {
        return timezone.getID();
    }
    @Override
    public boolean isParsed()
    {
        return parsed;
    }
    public NavigableSet<Integer>[] getCronFields()
    {
        return cronFields1;
    }


    // Parses the crontab expression,
    // and returns an array of sorted-sets of enumerated field values.
    // Note: This function can be used "stand-alone" without instantiating a CronExpressionParser.
    public static NavigableSet<Integer>[] parse1(String expression) throws CronExpressionParseException
    {
        if(Log.D) Log.d("parse() BEGIN: expression = " + expression);
        if(expression == null || expression.isEmpty()) {
            throw new CronExpressionParseException("Null/Empty cron expression.");
        }
        
        @SuppressWarnings("unchecked")
        NavigableSet<Integer>[] cronFieldSets = new NavigableSet[CronConstants.NUM_FIELDS];
        //String[] cronElements = expression.split("\\s+", NUM_FIELDS);
        String[] cronElements = expression.split("\\s+");
        int length = cronElements.length;
        if(length < CronConstants.NUM_FIELDS - 1) {
            throw new CronExpressionParseException("Incorrect field count: " + length);
        } else if(length < CronConstants.NUM_FIELDS) {
            // Year field is optional.
            Log.i("(Optional) Year field is missing.");
        } else if(length > CronConstants.NUM_FIELDS) {
            // ignore the trailing fields...
            if(Log.W) Log.w("Incorrect field count: " + length + ". Trailing fields will be ignored.");
        }
        for(int f = 0; f < length; ++f) {
            cronFieldSets[f] = new TreeSet<Integer>();
            // [1] "Atoms" - Separated by commas
            String[] atoms = cronElements[f].split(",");
            for (String atom : atoms) {
                // [2] "Steps" - Specified as a division
                String numerator = atom;
                int step = 1;
                if(atom.contains("/")) {
                    String[] division = atom.split("/", 2);
                    numerator = division[0];
                    try {
                        step = Integer.valueOf(division[1]);
                    } catch (Exception e) {
                        throw new CronExpressionParseException("Incorrect division format: atom = " + atom, e);
                    }
                }
                // [3] "Ranges" - Separated by dash
                int rangeStart;
                int rangeEnd;
                if("*".equals(numerator)) {
                    rangeStart = CronConstants.RANGES[f][0];
                    rangeEnd = CronConstants.RANGES[f][1];
                } else if("?".equals(numerator) && (f == CronConstants.DAYS_OF_MONTH || f == CronConstants.DAYS_OF_WEEK)) {
                    // ?????
                    rangeStart = CronConstants.RANGES[f][0];
                    rangeEnd = CronConstants.RANGES[f][1];
                } else {
                    if(numerator.contains("-")) {
                        String[] ranges = numerator.split("-", 2);
                        try {
                            rangeStart = Integer.valueOf(ranges[0]);
                            rangeEnd = Integer.valueOf(ranges[1]);
                        } catch (Exception e) {
                            if(f == CronConstants.MONTHS && (CronConstants.sMapMonths.containsKey(ranges[0].toUpperCase()) && CronConstants.sMapMonths.containsKey(ranges[1].toUpperCase()))) {
                                rangeStart = CronConstants.sMapMonths.get(ranges[0].toUpperCase());
                                rangeEnd = CronConstants.sMapMonths.get(ranges[1].toUpperCase());
                            } else if(f == CronConstants.DAYS_OF_WEEK && (CronConstants.sMapDaysOfWeek.containsKey(ranges[0].toUpperCase()) && CronConstants.sMapDaysOfWeek.containsKey(ranges[1].toUpperCase()))) {
                                rangeStart = CronConstants.sMapDaysOfWeek.get(ranges[0].toUpperCase());
                                rangeEnd = CronConstants.sMapDaysOfWeek.get(ranges[1].toUpperCase());
                            } else {
                                throw new CronExpressionParseException("Incorrect range format: numerator = " + numerator, e);
                            }
                        }
                    } else {   // Single value.
                        try {
                            rangeStart = Integer.valueOf(numerator);
                        } catch (Exception e) {
                            if(f == CronConstants.MONTHS && CronConstants.sMapMonths.containsKey(numerator.toUpperCase())) {
                                rangeStart = CronConstants.sMapMonths.get(numerator.toUpperCase());
                            } else if(f == CronConstants.DAYS_OF_WEEK && CronConstants.sMapDaysOfWeek.containsKey(numerator.toUpperCase())) {
                                rangeStart = CronConstants.sMapDaysOfWeek.get(numerator.toUpperCase());
                            } else {
                                throw new CronExpressionParseException("Incorrect range format: numerator = " + numerator, e);
                            }
                        }
                        rangeEnd = rangeStart;
                    }
                }
                if(f == CronConstants.DAYS_OF_WEEK) {  // Note: We support formats like "0-3", "5-7"=="0,5-6", "6-8"=="0-1,6", etc...
                    // To support formats like SAT-SUN, 5-1, etc.
                    if(rangeStart > rangeEnd) {
                        rangeEnd += 7;
                    }
                    for(int i = rangeStart; i <= rangeEnd; i += step ) {
                        cronFieldSets[CronConstants.DAYS_OF_WEEK].add(i % 7);   // 7 -> 0
                    }
                } else {
                    for(int i = rangeStart; i <= rangeEnd; i += step ) {
                        cronFieldSets[f].add(i);
                    }
                }
            }
        }
        // This is not really necessary, but doing it here is kind of convenient for later use...
        if(cronFieldSets[CronConstants.YEARS] == null) {
            cronFieldSets[CronConstants.YEARS] = new TreeSet<Integer>();
            for(int i = CronConstants.RANGES[CronConstants.YEARS][0]; i <= CronConstants.RANGES[CronConstants.YEARS][1]; i++) {
                cronFieldSets[CronConstants.YEARS].add(i);
            }
        }
        
        // For debugging purposes...
//        if(Log.V) Log.v("cronFieldSets = " + toDebugString(cronFieldSets));
        if(Log.I) Log.i("cronFieldSets = " + toDebugString(cronFieldSets));

        Log.d("parse() END:");
        return cronFieldSets;
    }


    public static long[] parse2(String expression) throws CronExpressionParseException
    {
        if(Log.D) Log.d("parse() BEGIN: expression = " + expression);
        if(expression == null || expression.isEmpty()) {
            throw new CronExpressionParseException("Null/Empty cron expression.");
        }

        @SuppressWarnings("unchecked")
        long[] cronFieldSets = new long[CronConstants.NUM_FIELDS];
        //String[] cronElements = expression.split("\\s+", NUM_FIELDS);
        String[] cronElements = expression.split("\\s+");
        int length = cronElements.length;
        if(length < CronConstants.NUM_FIELDS - 1) {
            throw new CronExpressionParseException("Incorrect field count: " + length);
        } else if(length < CronConstants.NUM_FIELDS) {
            // Year field is optional.
            Log.i("(Optional) Year field is missing.");
        } else if(length > CronConstants.NUM_FIELDS) {
            // ignore the trailing fields...
            if(Log.W) Log.w("Incorrect field count: " + length + ". Trailing fields will be ignored.");
        }
        for(int f = 0; f < length; ++f) {
            cronFieldSets[f] = 0;
            // [1] "Atoms" - Separated by commas
            String[] atoms = cronElements[f].split(",");
            for (String atom : atoms) {
                // [2] "Steps" - Specified as a division
                String numerator = atom;
                int step = 1;
                if(atom.contains("/")) {
                    String[] division = atom.split("/", 2);
                    numerator = division[0];
                    try {
                        step = Integer.valueOf(division[1]);
                    } catch (Exception e) {
                        throw new CronExpressionParseException("Incorrect division format: atom = " + atom, e);
                    }
                }
                // [3] "Ranges" - Separated by dash
                int rangeStart;
                int rangeEnd;
                if("*".equals(numerator)) {
                    rangeStart = CronConstants.RANGES[f][0];
                    rangeEnd = CronConstants.RANGES[f][1];
                } else if("?".equals(numerator) && (f == CronConstants.DAYS_OF_MONTH || f == CronConstants.DAYS_OF_WEEK)) {
                    // ?????
                    rangeStart = CronConstants.RANGES[f][0];
                    rangeEnd = CronConstants.RANGES[f][1];
                } else {
                    if(numerator.contains("-")) {
                        String[] ranges = numerator.split("-", 2);
                        try {
                            rangeStart = Integer.valueOf(ranges[0]);
                            rangeEnd = Integer.valueOf(ranges[1]);
                        } catch (Exception e) {
                            if(f == CronConstants.MONTHS && (CronConstants.sMapMonths.containsKey(ranges[0].toUpperCase()) && CronConstants.sMapMonths.containsKey(ranges[1].toUpperCase()))) {
                                rangeStart = CronConstants.sMapMonths.get(ranges[0].toUpperCase());
                                rangeEnd = CronConstants.sMapMonths.get(ranges[1].toUpperCase());
                            } else if(f == CronConstants.DAYS_OF_WEEK && (CronConstants.sMapDaysOfWeek.containsKey(ranges[0].toUpperCase()) && CronConstants.sMapDaysOfWeek.containsKey(ranges[1].toUpperCase()))) {
                                rangeStart = CronConstants.sMapDaysOfWeek.get(ranges[0].toUpperCase());
                                rangeEnd = CronConstants.sMapDaysOfWeek.get(ranges[1].toUpperCase());
                            } else {
                                throw new CronExpressionParseException("Incorrect range format: numerator = " + numerator, e);
                            }
                        }
                    } else {   // Single value.
                        try {
                            rangeStart = Integer.valueOf(numerator);
                        } catch (Exception e) {
                            if(f == CronConstants.MONTHS && CronConstants.sMapMonths.containsKey(numerator.toUpperCase())) {
                                rangeStart = CronConstants.sMapMonths.get(numerator.toUpperCase());
                            } else if(f == CronConstants.DAYS_OF_WEEK && CronConstants.sMapDaysOfWeek.containsKey(numerator.toUpperCase())) {
                                rangeStart = CronConstants.sMapDaysOfWeek.get(numerator.toUpperCase());
                            } else {
                                throw new CronExpressionParseException("Incorrect range format: numerator = " + numerator, e);
                            }
                        }
                        rangeEnd = rangeStart;
                    }
                }
                if(f == CronConstants.DAYS_OF_WEEK) {  // Note: We support formats like "0-3", "5-7"=="0,5-6", "6-8"=="0-1,6", etc...
                    // To support formats like SAT-SUN, 5-1, etc.
                    if(rangeStart > rangeEnd) {
                        rangeEnd += 7;
                    }
                    cronFieldSets[CronConstants.DAYS_OF_WEEK] = CronFieldUtil.add(CronConstants.DAYS_OF_WEEK, cronFieldSets[CronConstants.DAYS_OF_WEEK], rangeStart, rangeEnd, step, 7);
                } else {
                    cronFieldSets[f] = CronFieldUtil.add(f, cronFieldSets[f], rangeStart, rangeEnd, step);
                }
            }
        }
        // This is not really necessary, but doing it here is kind of convenient for later use...
        // if(cronFieldSets[YEARS] == =1) {
            cronFieldSets[CronConstants.YEARS] = 0;
            cronFieldSets[CronConstants.YEARS] = CronFieldUtil.add(CronConstants.YEARS, cronFieldSets[CronConstants.YEARS], CronConstants.RANGES[CronConstants.YEARS][0], CronConstants.RANGES[CronConstants.YEARS][1], 1);
        // }

        // For debugging purposes...
//        if(Log.V) Log.v("cronFieldSets = " + toDebugString(cronFieldSets));
        if(Log.I) Log.i("cronFieldSets = " + toDebugString(cronFieldSets));

        Log.d("parse() END:");
        return cronFieldSets;
    }


    // TBD:
    protected NavigableSet<Integer> getMinutes1()
    {
        return cronFields1[CronConstants.MINUTES];
    }
    protected NavigableSet<Integer> getHours1()
    {
        return cronFields1[CronConstants.HOURS];
    }
    protected NavigableSet<Integer> getDays1()
    {
        return cronFields1[CronConstants.DAYS_OF_MONTH];
    }
    protected NavigableSet<Integer> getMonths1()
    {
        return cronFields1[CronConstants.MONTHS];
    }
    protected NavigableSet<Integer> getDaysOfWeek1()
    {
        return cronFields1[CronConstants.DAYS_OF_WEEK];
    }
    protected NavigableSet<Integer> getYears1()
    {
        return cronFields1[CronConstants.YEARS];
    }

    protected long getMinutes()
    {
        return cronFields2[CronConstants.MINUTES];
    }
    protected long getHours()
    {
        return cronFields2[CronConstants.HOURS];
    }
    protected long getDays()
    {
        return cronFields2[CronConstants.DAYS_OF_MONTH];
    }
    protected long getMonths()
    {
        return cronFields2[CronConstants.MONTHS];
    }
    protected long getDaysOfWeek()
    {
        return cronFields2[CronConstants.DAYS_OF_WEEK];
    }
    protected long getYears()
    {
        return cronFields2[CronConstants.YEARS];
    }


//    // temporary
//    // Note that the return value may not be the same as the input cron expression.
//    public String buildCronExpression()
//    {
//        StringBuilder sb = new StringBuilder();
//        int[] mins = CronFieldUtil.getValuesAscending(CronConstants.MINUTES, cronFields2[CronConstants.MINUTES]);
//        if(mins != null) {
//            for (int i = 0; i < mins.length; i++) {
//                sb.append(min[i]);
//                if(i != mins.length-1) {
//                    sb.append(",");
//                }
//            }
//        }
//
//        // etc...
//
//
//
//        String exp = sb.toString();
//        if(Log.I) Log.i("Cron expression built: " + exp);
//        return exp;
//    }
//

    // When is the earliest time when the cron job is scheduled to run (after "now")?
    @Override
    public Long getNextCronTime()
    {
        return getNextCronTime(System.currentTimeMillis());
    }
    @Override
    public Long getNextCronTime(long now)
    {
        // Now, using the new implementation...
        return getNextCronTime2(now);
    }

    // temporary
    public Long getNextCronTimeShifted(long delta)
    {
        return getNextCronTimeShifted(System.currentTimeMillis(), delta);
    }
    public Long getNextCronTimeShifted(long now, long delta)
    {
        // Note that how the time is shifted twice in computing this.
        Long t = getNextCronTime(now - delta);
        if(t == null) {   // ???
            return null;
        }
        return t + delta;
    }


    protected Long getNextCronTime1()
    {
        return getNextCronTime1(System.currentTimeMillis());
    }
    protected Long getNextCronTime1(long now)
    {
        if(Log.D) Log.d("getNextCronTime() BEGIN: now = " + now);
        if(!isParsed()) {
            Log.w("Parser not initialized!");
            return null;   // ???
        }

        // Log.w("AAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
        
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        // calendar.setTimeZone(this.timezone);
        calendar.setTimeInMillis(now);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;  // Note Calendar month -> cron expression month.
        //int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        //int second = calendar.get(Calendar.SECOND);

        // Log.w("BBBBBBBBBBBBBBBBBBBBBBBBBBBBBB");


        // TBD:
        // The following implementation seems to have large memory footprint.
        // It's almost unusable in Android (although it can still be used in Web apps, probably)
        // Need a better algorithm.


        // Target value.
        Long nextCronTime = null;
        
        NavigableSet<Integer> tailYears = cronFields1[CronConstants.YEARS].tailSet(year, true);
        Iterator<Integer> itYears = tailYears.iterator();
        
        // TBD: To avoid too many iterations (which is unlikely, but possible in certain pathological cases),
        //      we need to do either one or both of the following:
        //      (1) Set the max iterations.
        //      (2) Set a certain limit/delta after now, and returns null if nextCronTime is not found within the delta.
        // For now,
        //      Just log it....
        int counter = 0;

        GIANT_NESTED_LOOP:
        while(itYears.hasNext()) {
            int y = itYears.next();
            NavigableSet<Integer> tailMonths = null;
            if(y==year) {
                tailMonths = cronFields1[CronConstants.MONTHS].tailSet(month, true);
            } else {  // y > year
                tailMonths = cronFields1[CronConstants.MONTHS];
            }

            // Log.w("DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD");

            Iterator<Integer> itMonths = tailMonths.iterator();
            while(itMonths.hasNext()) {
                int m = itMonths.next();
                NavigableSet<Integer> tailDays = null;
                if(y==year && m==month) {
                    tailDays = cronFields1[CronConstants.DAYS_OF_MONTH].tailSet(dayOfMonth, true);
                } else {  // y > year
                    tailDays = cronFields1[CronConstants.DAYS_OF_MONTH];
                }

                // Log.w("EEEEEEEEEEEEEEEEEEEEEEEEEE");

                Iterator<Integer> itDays = tailDays.iterator();
                while(itDays.hasNext()) {
                    int d = itDays.next();
                    int dayOfWeek = CronTimeUtil.getDayOfWeek(y, m, d);   // 0~6.
                    //if(isValidDate(y,m,d)) {
                    if(dayOfWeek >= 0 && cronFields1[CronConstants.DAYS_OF_WEEK].contains(dayOfWeek)) {
                        NavigableSet<Integer> tailHours = null;                        
                        if(y==year && m==month && d==dayOfMonth) {
                            tailHours = cronFields1[CronConstants.HOURS].tailSet(hour, true);
                        } else {  // y > year
                            tailHours = cronFields1[CronConstants.HOURS];
                        }

                        // Log.w("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF");

                        Iterator<Integer> itHours = tailHours.iterator();
                        while(itHours.hasNext()) {
                            int hr = itHours.next();                            
                            NavigableSet<Integer> tailMinutes = null;                        
                            if(y==year && m==month && d==dayOfMonth && hr==hour) {
                                tailMinutes = cronFields1[CronConstants.MINUTES].tailSet(minute, true);
                            } else {  // y > year
                                tailMinutes = cronFields1[CronConstants.MINUTES];
                            }

                            // Log.w("GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGg");

                            Iterator<Integer> itMinutes = tailMinutes.iterator();
                            while(itMinutes.hasNext()) {
                                int mn = itMinutes.next();

                                // OK, all these loops to test this one thing... :)
                                long t = CronTimeUtil.getTime(y, m, d, hr, mn);
                                if(t >= now) {
                                    nextCronTime = t;
                                    break GIANT_NESTED_LOOP;
                                }
                                
                                counter++;
                                if(Log.V) {
                                    if(counter % 10 == counter) {
                                        Log.v("Iteration counter: " + counter);
                                    }
                                }
                            }
                        }
                    } else {
                        // ignore, and continue...
                    }
                }
            }
        }
        // End of GIANT_NESTED_LOOP

        Log.w("ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ");

        if(Log.D) Log.d("getNextCronTime() END: nextCronTime = " + nextCronTime);
        return nextCronTime;
    }



    protected Long getNextCronTime2()
    {
        return getNextCronTime2(System.currentTimeMillis());
    }
    protected Long getNextCronTime2(long now)
    {
        if(Log.D) Log.d("getNextCronTime() BEGIN: now = " + now);
        if(!isParsed()) {
            Log.w("Parser not initialized!");
            return null;   // ???
        }

        // Log.w("AAAAAAAAAAAAAAAAAAAAAAAAAAAAA");

        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        // calendar.setTimeZone(this.timezone);
        calendar.setTimeInMillis(now);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;  // Note Calendar month -> cron expression month.
        //int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        //int second = calendar.get(Calendar.SECOND);

        if(Log.I) Log.i(">>> year = " + year + "; month = " + month + "; dayOfMonth = " + dayOfMonth + "; hour = " + hour + "; minute = " + minute);

        // Log.w("BBBBBBBBBBBBBBBBBBBBBBBBBBBBBB");



        // Target value.
        Long nextCronTime = null;

        long tailYears = CronFieldUtil.tail(CronConstants.YEARS, cronFields2[CronConstants.YEARS], year);
        int[] itYears = CronFieldUtil.getValuesAscending(CronConstants.YEARS, tailYears);

        if(Log.D) Log.d(":::::::: tailYears = " + Long.toBinaryString(tailYears));
        if(Log.D) Log.d(":::::::: itYears = " + Arrays.toString(itYears));


        // TBD: To avoid too many iterations (which is unlikely, but possible in certain pathological cases),
        //      we need to do either one or both of the following:
        //      (1) Set the max iterations.
        //      (2) Set a certain limit/delta after now, and returns null if nextCronTime is not found within the delta.
        // For now,
        //      Just log it....
        int counter = 0;

        GIANT_NESTED_LOOP:
        for(int iy=0;iy<itYears.length; iy++) {
            int y = itYears[iy];
            long tailMonths = 0L;
            if(y==year) {
                tailMonths = CronFieldUtil.tail(CronConstants.MONTHS, cronFields2[CronConstants.MONTHS], month);
            } else {  // y > year
                tailMonths = cronFields2[CronConstants.MONTHS];
            }

            // Log.w("DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD");

            int[] itMonths = CronFieldUtil.getValuesAscending(CronConstants.MONTHS, tailMonths);

            if(Log.D) Log.d(":::::::: tailMonths = " + Long.toBinaryString(tailMonths));
            if(Log.D) Log.d(":::::::: itMonths = " + Arrays.toString(itMonths));


            for(int im=0;im<itMonths.length; im++) {
                int m = itMonths[im];
                long tailDays = 0L;
                if(y==year && m==month) {
                    tailDays = CronFieldUtil.tail(CronConstants.DAYS_OF_MONTH, cronFields2[CronConstants.DAYS_OF_MONTH], dayOfMonth);
                } else {  // y > year
                    tailDays = cronFields2[CronConstants.DAYS_OF_MONTH];
                }

                // Log.w("EEEEEEEEEEEEEEEEEEEEEEEEEE");

                int[] itDays = CronFieldUtil.getValuesAscending(CronConstants.DAYS_OF_MONTH, tailDays);


                if(Log.D) Log.d(":::::::: tailDays = " + Long.toBinaryString(tailDays));
                if(Log.D) Log.d(":::::::: itDays = " + Arrays.toString(itDays));

                for(int id=0;id<itDays.length; id++) {
                    int d = itDays[id];
                    int dayOfWeek = CronTimeUtil.getDayOfWeek(y, m, d);   // 0~6.
                    //if(isValidDate(y,m,d)) {
                    if(dayOfWeek >= 0 && CronFieldUtil.contains(CronConstants.DAYS_OF_WEEK, cronFields2[CronConstants.DAYS_OF_WEEK], dayOfWeek)) {
                        long tailHours = 0L;
                        if(y==year && m==month && d==dayOfMonth) {
                            tailHours = CronFieldUtil.tail(CronConstants.HOURS, cronFields2[CronConstants.HOURS], hour);
                        } else {  // y > year
                            tailHours = cronFields2[CronConstants.HOURS];
                        }

                        // Log.w("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF");

                        int[] itHours = CronFieldUtil.getValuesAscending(CronConstants.HOURS, tailHours);

                        if(Log.D) Log.d(":::::::: tailHours = " + Long.toBinaryString(tailHours));
                        if(Log.D) Log.d(":::::::: itHours = " + Arrays.toString(itHours));

                        for(int ih=0;ih<itHours.length; ih++) {
                            int hr = itHours[ih];
                            long tailMinutes = 0L;
                            if(y==year && m==month && d==dayOfMonth && hr==hour) {
                                tailMinutes = CronFieldUtil.tail(CronConstants.MINUTES, cronFields2[CronConstants.MINUTES], minute);
                            } else {
                                tailMinutes = cronFields2[CronConstants.MINUTES];
                            }

                            // Log.w("GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGg");

                            int[] itMinutes = CronFieldUtil.getValuesAscending(CronConstants.MINUTES, tailMinutes);


                            if(Log.D) Log.d(":::::::: tailMinutes = " + Long.toBinaryString(tailMinutes));
                            if(Log.D) Log.d(":::::::: itMinutes = " + Arrays.toString(itMinutes));

                            for(int imn=0;imn<itMinutes.length; imn++) {
                                int mn = itMinutes[imn];

                                // OK, all these loops to test this one thing... :)
                                long t = CronTimeUtil.getTime(y, m, d, hr, mn);
                                // Log.w(">>>>>>>>>>>>>>>>>>>>>>>>. time = " + t + ": now = " + now);
                                if(t >= now) {
                                    nextCronTime = t;
                                    break GIANT_NESTED_LOOP;
                                }

                                counter++;
                                if(Log.V) {
                                    if(counter % 10 == counter) {
                                        Log.v("Iteration counter: " + counter);
                                    }
                                }
                            }
                        }
                    } else {
                        // ignore, and continue...
                    }
                }
            }
        }
        // End of GIANT_NESTED_LOOP

        // Log.w("ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ");

        if(Log.D) Log.d("getNextCronTime() END: nextCronTime = " + nextCronTime);
        return nextCronTime;
    }


    // When was the last time when the cron job was scheduled to run (prior to "now")?
    @Override
    public Long getPreviousCronTime()
    {
        return getPreviousCronTime(System.currentTimeMillis());
    }
    @Override
    public Long getPreviousCronTime(long now)
    {
        // Now, using the new implementation...
        return getPreviousCronTime2(now);
    }

    protected Long getPreviousCronTime1()
    {
        return getPreviousCronTime1(System.currentTimeMillis());
    }
    protected Long getPreviousCronTime1(long now)
    {

        if(Log.D) Log.d("getPreviousCronTime() BEGIN: now = " + now);
        if(!isParsed()) {
            Log.w("Parser not initialized!");
            return null;   // ???
        }

        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        // calendar.setTimeZone(this.timezone);
        calendar.setTimeInMillis(now);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;  // Note Calendar month -> cron expression month.
        //int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        //int second = calendar.get(Calendar.SECOND);

        // Target value.
        Long previousCronTime = null;
        
        NavigableSet<Integer> headYears = cronFields1[CronConstants.YEARS].headSet(year, true);
        Iterator<Integer> itYears = headYears.descendingIterator();
        
        // Ditto...
        int counter = 0;

        GIANT_NESTED_LOOP:
        while(itYears.hasNext()) {
            int y = itYears.next();
            NavigableSet<Integer> headMonths = null;
            if(y==year) {
                headMonths = cronFields1[CronConstants.MONTHS].headSet(month, true);
            } else {  // y > year
                headMonths = cronFields1[CronConstants.MONTHS];
            }
            Iterator<Integer> itMonths = headMonths.descendingIterator();
            while(itMonths.hasNext()) {
                int m = itMonths.next();
                NavigableSet<Integer> headDays = null;
                if(y==year && m==month) {
                    headDays = cronFields1[CronConstants.DAYS_OF_MONTH].headSet(dayOfMonth, true);
                } else {  // y > year
                    headDays = cronFields1[CronConstants.DAYS_OF_MONTH];
                }
                Iterator<Integer> itDays = headDays.descendingIterator();
                while(itDays.hasNext()) {
                    int d = itDays.next();
                    int dayOfWeek = CronTimeUtil.getDayOfWeek(y, m, d);   // 0~6.
                    //if(isValidDate(y,m,d)) {
                    if(dayOfWeek >= 0 && cronFields1[CronConstants.DAYS_OF_WEEK].contains(dayOfWeek)) {
                        NavigableSet<Integer> headHours = null;                        
                        if(y==year && m==month && d==dayOfMonth) {
                            headHours = cronFields1[CronConstants.HOURS].headSet(hour, true);
                        } else {  // y > year
                            headHours = cronFields1[CronConstants.HOURS];
                        }
                        Iterator<Integer> itHours = headHours.descendingIterator();
                        while(itHours.hasNext()) {
                            int hr = itHours.next();                            
                            NavigableSet<Integer> headMinutes = null;                        
                            if(y==year && m==month && d==dayOfMonth && hr==hour) {
                                headMinutes = cronFields1[CronConstants.MINUTES].headSet(minute, true);
                            } else {  // y > year
                                headMinutes = cronFields1[CronConstants.MINUTES];
                            }
                            Iterator<Integer> itMinutes = headMinutes.descendingIterator();
                            while(itMinutes.hasNext()) {
                                int mn = itMinutes.next();

                                // OK, all these loops to test this one thing... :)
                                long t = CronTimeUtil.getTime(y, m, d, hr, mn);
                                if(t <= now) {
                                    previousCronTime = t;
                                    break GIANT_NESTED_LOOP;
                                }
                                
                                counter++;
                                if(Log.V) {
                                    if(counter % 10 == counter) {
                                        Log.v("Iteration counter: " + counter);
                                    }
                                }
                            }
                        }
                    } else {
                        // ignore, and continue...
                    }
                }
            }
        }
        // End of GIANT_NESTED_LOOP

        if(Log.D) Log.d("getPreviousCronTime() END: previousCronTime = " + previousCronTime);
        return previousCronTime;
    }


    protected Long getPreviousCronTime2()
    {
        return getPreviousCronTime2(System.currentTimeMillis());
    }
    protected Long getPreviousCronTime2(long now)
    {
        if(Log.D) Log.d("getPreviousCronTime() BEGIN: now = " + now);
        if(!isParsed()) {
            Log.w("Parser not initialized!");
            return null;   // ???
        }

        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        // calendar.setTimeZone(this.timezone);
        calendar.setTimeInMillis(now);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;  // Note Calendar month -> cron expression month.
        //int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        //int second = calendar.get(Calendar.SECOND);

        // Target value.
        Long previousCronTime = null;

        long headYears = CronFieldUtil.head(CronConstants.YEARS, cronFields2[CronConstants.YEARS], year);
        int[] itYears = CronFieldUtil.getValuesDescending(CronConstants.YEARS, headYears);

        if(Log.D) Log.d(":::::::: headYears = " + Long.toBinaryString(headYears));
        if(Log.D) Log.d(":::::::: itYears = " + Arrays.toString(itYears));

        // Ditto...
        int counter = 0;

        GIANT_NESTED_LOOP:
        for(int iy=0;iy<itYears.length; iy++) {
            int y = itYears[iy];
            long headMonths = 0L;
            if(y==year) {
                headMonths = CronFieldUtil.head(CronConstants.MONTHS, cronFields2[CronConstants.MONTHS], month);
            } else {  // y > year
                headMonths = cronFields2[CronConstants.MONTHS];
            }
            int[] itMonths = CronFieldUtil.getValuesDescending(CronConstants.MONTHS, headMonths);

            if(Log.D) Log.d(":::::::: headMonths = " + Long.toBinaryString(headMonths));
            if(Log.D) Log.d(":::::::: itMonths = " + Arrays.toString(itMonths));

            for(int im=0;im<itMonths.length; im++) {
                int m = itMonths[im];
                long headDays = 0L;
                if(y==year && m==month) {
                    headDays = CronFieldUtil.head(CronConstants.DAYS_OF_MONTH, cronFields2[CronConstants.DAYS_OF_MONTH], dayOfMonth);
                } else {  // y > year
                    headDays = cronFields2[CronConstants.DAYS_OF_MONTH];
                }
                int[] itDays = CronFieldUtil.getValuesDescending(CronConstants.DAYS_OF_MONTH, headDays);

                if(Log.D) Log.d(":::::::: headDays = " + Long.toBinaryString(headDays));
                if(Log.D) Log.d(":::::::: itDays = " + Arrays.toString(itDays));

                for(int id=0;id<itDays.length; id++) {
                    int d = itDays[id];
                    int dayOfWeek = CronTimeUtil.getDayOfWeek(y, m, d);   // 0~6.
                    //if(isValidDate(y,m,d)) {
                    if(dayOfWeek >= 0 && CronFieldUtil.contains(CronConstants.DAYS_OF_WEEK, cronFields2[CronConstants.DAYS_OF_WEEK], dayOfWeek)) {
                        long headHours = 0L;
                        if(y==year && m==month && d==dayOfMonth) {
                            headHours = CronFieldUtil.head(CronConstants.HOURS, cronFields2[CronConstants.HOURS], hour);
                        } else {  // y > year
                            headHours = cronFields2[CronConstants.HOURS];
                        }
                        int[] itHours = CronFieldUtil.getValuesDescending(CronConstants.HOURS, headHours);

                        if(Log.D) Log.d(":::::::: headHours = " + Long.toBinaryString(headHours));
                        if(Log.D) Log.d(":::::::: itHours = " + Arrays.toString(itHours));

                        for(int ih=0;ih<itHours.length; ih++) {
                            int hr = itHours[ih];
                            long headMinutes = 0L;
                            if(y==year && m==month && d==dayOfMonth && hr==hour) {
                                headMinutes = CronFieldUtil.head(CronConstants.MINUTES, cronFields2[CronConstants.MINUTES], minute);
                            } else {
                                headMinutes = cronFields2[CronConstants.MINUTES];
                            }
                            int[] itMinutes = CronFieldUtil.getValuesDescending(CronConstants.MINUTES, headMinutes);

                            if(Log.D) Log.d(":::::::: headMinutes = " + Long.toBinaryString(headMinutes));
                            if(Log.D) Log.d(":::::::: itMinutes = " + Arrays.toString(itMinutes));

                            for(int imn=0;imn<itMinutes.length; imn++) {
                                int mn = itMinutes[imn];

                                // OK, all these loops to test this one thing... :)
                                long t = CronTimeUtil.getTime(y, m, d, hr, mn);
                                if(t <= now) {
                                    previousCronTime = t;
                                    break GIANT_NESTED_LOOP;
                                }

                                counter++;
                                if(Log.V) {
                                    if(counter % 10 == counter) {
                                        Log.v("Iteration counter: " + counter);
                                    }
                                }
                            }
                        }
                    } else {
                        // ignore, and continue...
                    }
                }
            }
        }
        // End of GIANT_NESTED_LOOP

        if(Log.D) Log.d("getPreviousCronTime() END: previousCronTime = " + previousCronTime);
        return previousCronTime;
    }



    @Override
    public String toString() 
    {
//        if(isParsed() && cronFields1 != null) {
//            // TBD:
//            return toDebugString(cronFields1);
//        }
        if(isParsed() && cronFields2 != null) {
            // TBD:
            return toDebugString(cronFields2);
        }

        // TBD:
        return expression;
    }
    private static String toDebugString(NavigableSet<Integer>[] cronFieldSets)
    {
        if(cronFieldSets != null) {
            // TBD: Validate cronFieldSets...
            StringBuilder sb = new StringBuilder();
            sb.append("{");
            for(int j=0; j<cronFieldSets.length; j++) {
                sb.append("[");
                NavigableSet<Integer> fieldSet = cronFieldSets[j];
                if(fieldSet != null) {
                    Iterator<Integer> itFields = fieldSet.iterator();
                    while(itFields.hasNext()) {
                        Integer val = itFields.next();
                        sb.append(val);
                        if(itFields.hasNext()) {
                            sb.append(",");
                        }
                    }
                }
                sb.append("]");
                if(j < cronFieldSets.length-1) {
                    sb.append(",");
                }
            }
            sb.append("}");
            return sb.toString();
        } else {
            return null;
        }
    }


    private static String toDebugString(long[] cronFieldSets)
    {
        if(cronFieldSets != null) {
            // TBD: Validate cronFieldSets...
            StringBuilder sb = new StringBuilder();
            sb.append("{");
            for(int j=0; j<cronFieldSets.length; j++) {
                sb.append("[");
                long fieldSet = cronFieldSets[j];
                sb.append(Long.toBinaryString(fieldSet));
                sb.append("]");
                if(j < cronFieldSets.length-1) {
                    sb.append(",");
                }
            }
            sb.append("}");
            return sb.toString();
        } else {
            return null;
        }
    }


}
package ms.airy.lib.core.time.handler;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;

import java.util.HashSet;
import java.util.Set;


/**
 * For "polling".
 * This can be used only for a small number of pollings, because of the way it's implemented.
 */
public abstract class PollingHandler extends Handler
{
    // temporary
    private static final int POLLING_EVENT = 1397;

    // temporary
    // If art repeats==-1 is used, we used the max number.
    // Note that because of the way we implement repetition,
    //    MAX_REPEATS cannot be a big number. It'll be very inefficient...
    // Clearly, this number cannot be too small either.
    // The best thing to do is, to let the client use the explicit repeats arg.
    private static final int MAX_REPEATS = 1000;   // arbitrary number.

    // Is this necessary?
    // For now, we just use different subclasses for different polling schedules....
    //   This is kind of limiting since the schedule cannot be dynamically changed...
    //      (--> subclass from PollingHandler and implement different schedules...)
    //   but, it is more convenient...
    // (TBD: Use a strategy pattern???)
    // polling schedule.
    private int pollingSchedule;

//    // "Start time".
//    // if 0L, the "current time" (whenever the polling method is called) is used.
//    private long pollingStartTime = 0L;


    // TBD:

    public PollingHandler()
    {
        init();
    }
    // TBD:
    //     do we need callback? (PollingHandler is a pretty specialized class.)
    public PollingHandler(Callback callback)
    {
        super(callback);
        init();
    }
    public PollingHandler(Looper looper)
    {
        super(looper);
        init();
    }
    // TBD: do we need callback?
    public PollingHandler(Looper looper, Callback callback)
    {
        super(looper, callback);
        init();
    }
    private void init()
    {
    }


    // No setters...
    protected int getPollingSchedule()
    {
        return pollingSchedule;
    }
    protected final void setPollingSchedule(int pollingSchedule)
    {
        this.pollingSchedule = pollingSchedule;
    }


//    // If 0L, "now" will be used as a start time.
//    public long getPollingStartTime()
//    {
//        return pollingStartTime;
//    }
//    public void setPollingStartTime(long pollingStartTime)
//    {
//        this.pollingStartTime = pollingStartTime;
//    }


    // Note the name of the methods.
    // The "fetch" method is not a "getter", and it is not idempotent.
    // Calling these methods changes the internal state of the handler.
    public long fetchNextDelay()
    {
        long[] delays = fetchNextDelays(1);
        return delays[0];
    }
    protected abstract long[] fetchNextDelays(int size);
    protected abstract long[] fetchNextDelays(long duration);
    public long computeNextDelay()
    {
        long[] delays = computeNextDelays(1);
        return delays[0];
    }
    protected abstract long[] computeNextDelays(int size);
    protected abstract long[] computeNextDelays(long duration);



    // The "main" methods of PollingHandler

//    public void startPolling(long startTime, long interval, int repeats)
//    {
//    }
//    public void startPolling(long startTime, long interval, long endTime)
//    {
//    }

    public void startPolling(int repeats)
    {
        long currentUptime = SystemClock.uptimeMillis();
        startPolling(currentUptime, repeats);
    }
    public void startPolling(long startUptime, int repeats)
    {
        deliverDefaultMessageDelayed(startUptime, repeats);
    }
    public void startPolling(long duration)
    {
        long currentUptime = SystemClock.uptimeMillis();
        startPolling(currentUptime, duration);
    }
    public void startPolling(long startUptime, long duration)
    {
        deliverDefaultMessageDelayed(startUptime, duration);
    }



    // TBD:
    // Support start, stop, restart, etc...???
//    public void stopPolling()
//    {
//        // ???
//    }


    public boolean deliverDefaultMessageDelayed()
    {
        return deliverEmptyMessageDelayed(POLLING_EVENT);
    }
//    public boolean deliverDefaultMessageDelayed(long startUptime)
//    {
//        return deliverEmptyMessageDelayed(POLLING_EVENT, startUptime);
//    }
    public void deliverDefaultMessageDelayed(int repeats)
    {
        deliverEmptyMessageDelayed(POLLING_EVENT, repeats);
    }
    public void deliverDefaultMessageDelayed(long startUptime, int repeats)
    {
        deliverEmptyMessageDelayed(POLLING_EVENT, startUptime, repeats);
    }
    public void deliverDefaultMessageDelayed(long duration)
    {
        deliverEmptyMessageDelayed(POLLING_EVENT, duration);
    }
    public void deliverDefaultMessageDelayed(long startUptime, long duration)
    {
        deliverEmptyMessageDelayed(POLLING_EVENT, startUptime, duration);
    }

    public boolean deliverEmptyMessageDelayed(int what)
    {
        // long currentUptime = SystemClock.uptimeMillis();
        long nextUptime = fetchNextDelay();
        return sendEmptyMessageAtTime(what, nextUptime);
    }
//    public boolean deliverEmptyMessageDelayed(int what, long startUptime)
//    {
//        long nextUptime = startUptime + fetchNextDelay();
//        return sendEmptyMessageAtTime(what, nextUptime);
//    }
    public void deliverEmptyMessageDelayed(int what, int repeats)
    {
        long startUptime = SystemClock.uptimeMillis();
        deliverEmptyMessageDelayed(what, startUptime, repeats);
    }
    public void deliverEmptyMessageDelayed(int what, long duration)
    {
        long startUptime = SystemClock.uptimeMillis();
        deliverEmptyMessageDelayed(what, startUptime, duration);
    }
    public void deliverEmptyMessageDelayed(int what, long startUptime, int repeats)
    {
        if(repeats == -1) {
            repeats = MAX_REPEATS;
        }
        long uptime1 = startUptime;
        long[] delays = fetchNextDelays(repeats);
        for(int i=0; i<repeats; i++) {
            long uptime2 = uptime1 + delays[i];
            boolean suc = sendEmptyMessageAtTime(what, uptime2);
            Log.d("Called sendEmptyMessageAtTime() at uptime = " + uptime2 + ". Result = " + suc);
            uptime1 = uptime2;
        }
    }
    public void deliverEmptyMessageDelayed(int what, long startUptime, long maxDuration)
    {
        long uptime1 = startUptime;
        long[] delays = fetchNextDelays(maxDuration);
        for(int i=0; i<delays.length; i++) {
            long uptime2 = uptime1 + delays[i];
            boolean suc = sendEmptyMessageAtTime(what, uptime2);
            Log.d("Called sendEmptyMessageAtTime() at uptime = " + uptime2 + ". Result = " + suc);
            uptime1 = uptime2;
        }
    }

    public boolean deliverMessageDelayed(Message msg)
    {
        // long currentUptime = SystemClock.uptimeMillis();
        long nextUptime = fetchNextDelay();
        return sendMessageAtTime(msg, nextUptime);
    }
//    public boolean deliverMessageDelayed(Message msg, long startUptime)
//    {
//        long nextUptime = startUptime + fetchNextDelay();
//        return sendMessageAtTime(msg, nextUptime);
//    }
    public void deliverMessageDelayed(Message msg, int repeats)
    {
        long uptime1 = SystemClock.uptimeMillis();
        deliverMessageDelayed(msg, uptime1, repeats);
    }
    public void deliverMessageDelayed(Message msg, long startUptime, int repeats)
    {
        if(repeats == -1) {
            repeats = MAX_REPEATS;
        } else if(repeats > MAX_REPEATS) {
            Log.w("Input repeats is too big: repeats = " + repeats);
            repeats = MAX_REPEATS;
        }
        long uptime1 = startUptime;
        long[] delays = fetchNextDelays(repeats);
        for(int i=0; i<repeats; i++) {
            long uptime2 = uptime1 + delays[i];
            boolean suc = sendMessageAtTime(msg, uptime2);
            Log.d("Called sendMessageAtTime() at uptime = " + uptime2 + ". Result = " + suc);
            uptime1 = uptime2;
        }
    }
    public void deliverMessageDelayed(Message msg, long startUptime, long maxDuration)
    {
        long uptime1 = startUptime;
        long[] delays = fetchNextDelays(maxDuration);
        for(int i=0; i<delays.length; i++) {
            long uptime2 = uptime1 + delays[i];
            boolean suc = sendMessageAtTime(msg, uptime2);
            Log.d("Called sendMessageAtTime() at uptime = " + uptime2 + ". Result = " + suc);
            uptime1 = uptime2;
        }
    }



    @Override
    public void handleMessage(Message msg)
    {
        switch(msg.what) {
            case POLLING_EVENT:
                doPollingEventProcessing();
                // return; // ???
                break;
            default:
                Log.i("Unrecognized message: what = " + msg.what);
        }
        super.handleMessage(msg);
    }

    private void doPollingEventProcessing()
    {
        Log.d("doPollingEventProcessing()");
        for(PollingEventCallback callback : pollingEventCallbacks) {
            callback.processPollingEvent();
        }
    }


    // TBD:
    // Event callbacks
    private final Set<PollingEventCallback> pollingEventCallbacks = new HashSet<PollingEventCallback>();
    protected final Set<PollingEventCallback> getPollingEventCallbacks()
    {
        return pollingEventCallbacks;
    }

    // This needs to be called by the "client"
    public final void addPollingEventCallback(PollingEventCallback pollingEventCallback)
    {
        pollingEventCallbacks.add(pollingEventCallback);
    }


}

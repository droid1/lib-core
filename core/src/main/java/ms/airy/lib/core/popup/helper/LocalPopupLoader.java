package ms.airy.lib.core.popup.helper;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;

import ms.airy.lib.core.popup.common.PopupInfo;
import ms.airy.lib.core.popup.util.JsonContentParser;
import ms.airy.lib.core.popup.util.PopupLoaderUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Local popup files are stored under /assets/popup directory.
 */
public class LocalPopupLoader
{
    private static final String POPUP_DIR = "popup";

    private static LocalPopupLoader INSTANCE = null;
    public static LocalPopupLoader getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new LocalPopupLoader(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // We use the application context.
    private final Context context;
    protected LocalPopupLoader(Context context)
    {
        this.context = context;
    }

    protected Context getContext()
    {
        return this.context;
    }



//    public PopupInfo getPopupInfo(long popupId)
//    {
//        return getPopupMap().get(popupId);
//    }
    public PopupInfo getPopupInfo(String prefKey)
    {
        return getPopupMap().get(prefKey);
    }

//    public String getJsonContent(long popupId)
//    {
//        return getPopupMap().get(popupId);
//    }

    // TBD:
    // Load all json files into a map:

    // Popup Content map.
//    private Map<Long, PopupInfo> popupMap = null;
    private Map<String, PopupInfo> popupMap = null;
//    private Map<Long, String> popups = null;

//    public Map<Long, PopupInfo> getPopupMap()
    public Map<String, PopupInfo> getPopupMap()
    {
        if(popupMap == null) {
            loadPopupFiles();
        }
        return popupMap;
    }
//    public Map<Long, String> getPopupMap()
//    {
//        if(popups == null) {
//            loadPopupFiles();
//        }
//        return popups;
//    }

    protected void loadPopupFiles()
    {
//        popupMap = new HashMap<Long, PopupInfo>();
        popupMap = new HashMap<String, PopupInfo>();
//        popups = new HashMap<Long, String>();
        try {
            Resources resources = getContext().getResources();
            AssetManager assetManager = resources.getAssets();
            String[] files = assetManager.list(POPUP_DIR);
            if(Log.I) {
                Log.i(">>>>>>>>> files = " + files);
                if(files != null) {
                    Log.i(">>>>>>>>> files = " + files.length);
                }
            }

            if(files != null && files.length > 0) {
                for(String f : files) {
                    if(Log.I) Log.i(">>>>>>>>> f = " + f);

                    boolean including = PopupLoaderUtil.isIncludingThisPopup(f);
                    if(including == false) {
                        Log.w("Excluding the popup file: name = " + f);
                        continue;
                    }
                    try {
                        String fpath = POPUP_DIR + "/" + f;
                        InputStream inputStream = assetManager.open(fpath);

                        PopupInfo popupInfo = JsonContentParser.buildPopupInfo(inputStream);
//                        long popupId = popupInfo.getId();
//                        popupMap.put(popupId, popupInfo);
                        String prefKey = popupInfo.getPrefKey();
                        popupMap.put(prefKey, popupInfo);
//                        if(Log.I) Log.i("Popup loaded: popupId = " + popupId + ": popupInfo = " + popupInfo);
                        if(Log.I) Log.i("Popup loaded: prefKey = " + prefKey + ": popupInfo = " + popupInfo);


//                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
//
//                        StringBuilder sb = new StringBuilder();
//                        String line;
//                        while((line = bufferedReader.readLine()) != null) {
//                            sb.append(line).append("\n");
//                        }
//                        String jsonContent = sb.toString();
//
//                        // TBD:
//                        // Parse json content?
//
////                        PopupInfo popupPopup = new PopupInfo();
////                        popupPopup.setJsonContent(jsonContent);
////                        popupMap.put(popupId, popupPopup);
//
//                        popups.put(popupId, jsonContent);
//                        if(Log.I) Log.i("Popup loaded: popupId = " + popupId + ": jsonContent = " + jsonContent);

                    } catch (IOException e) {
                        Log.e("Failed to load popup file: " + f, e);
                    }
                }
            }
        } catch (IOException e) {
            Log.e("Failed to load popup files.", e);
        }
    }


}

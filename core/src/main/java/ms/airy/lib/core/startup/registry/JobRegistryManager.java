package ms.airy.lib.core.startup.registry;

import android.content.Context;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ms.airy.lib.core.startup.job.StartupJob;
import ms.airy.lib.core.startup.job.condition.StartupConditionHelper;
import ms.airy.lib.core.startup.task.BackgroundStartupTask;
import ms.airy.lib.core.startup.task.StartupTask;


// TBD:
// Abstract Factory.
public final class JobRegistryManager
{
    private static JobRegistryManager INSTANCE = null;
    public static JobRegistryManager getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new JobRegistryManager(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;

    private JobRegistryManager(Context context)
    {
        this.context = context;
    }

    // TBD:
    // How to set startupJobRegistry?? Use config???
    private StartupJobRegistry startupJobRegistry;

    public StartupJobRegistry getStartupJobRegistry()
    {
        return startupJobRegistry;
    }
    public void setStartupJobRegistry(StartupJobRegistry startupJobRegistry)
    {
        this.startupJobRegistry = startupJobRegistry;
        // reset "cache" variables...
        jobList = null;
        taskList = null;
        foregroundTaskList = null;
        backgroundTaskList = null;
    }

    // "cache"
    private List<StartupJob> jobList = null;
    private List<StartupTask> taskList = null;
    private List<StartupTask> foregroundTaskList = null;
    private List<StartupTask> backgroundTaskList = null;


    // Returns the filtered/sorted job list.
    public List<StartupJob> getStartupJobs()
    {
        if(jobList == null) {  // This check relies on the fact that inList is never null.
            List<StartupJob> inList = getStartupJobRegistry().getStartupJobs();
            jobList = StartupConditionHelper.getInstance(context).filterTasks(inList);
        }
        return jobList;
    }

    // Returns the filtered/sorted task list.
    public List<StartupTask> getStartupTasks()
    {
        if(taskList == null) {
            buildStartupTaskLists();
        }
        return taskList;
    }
    public List<StartupTask> getForegroundStartupTasks()
    {
        if(foregroundTaskList == null) {
            buildStartupTaskLists();
            if(Log.I) Log.i("Foreground tasks found: " + foregroundTaskList.size());
        }
        return foregroundTaskList;
    }
    public List<StartupTask> getBackgroundStartupTasks()
    {
        if(backgroundTaskList == null) {
            buildStartupTaskLists();
            if(Log.I) Log.i("Background tasks found: " + backgroundTaskList.size());
        }
        return backgroundTaskList;
    }

    private void buildStartupTaskLists()
    {
        List<StartupJob> jobs = getStartupJobs();
        taskList = new ArrayList<StartupTask>();
        foregroundTaskList = new ArrayList<StartupTask>();
        backgroundTaskList = new ArrayList<StartupTask>();

        // TBD:
        // In theory, we can have duplicate tasks
        //     because different jobs may have included the same tasks.
        // We can at least do de-dup based on task.id, for now...

        Set<Long> seenIt = new HashSet<Long>();
        for (StartupJob job : jobs) {
            StartupTask task = job.getTask();
            long id = task.getId();
            if (seenIt.contains(id)) {
                if (Log.I) Log.i("Task already included from other jobs. id = " + id);
                continue;
            }
            taskList.add(task);
            if(job.isBackgroundJob()) {
                if(task instanceof BackgroundStartupTask) {
                    backgroundTaskList.add(task);
                } else {
                    Log.w("backgroundJob==true, but task is not instanceof BackgroundStartupTask.");
                }
            } else {
                // Either foreground/background tasks can be used.
                foregroundTaskList.add(task);
            }
            seenIt.add(id);
        }
    }


}

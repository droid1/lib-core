package ms.airy.lib.core.phaser.common;


// set of "binary" user badges.
public final class Badge
{
    // User's "state".
    // "Two-Bit" masks. 0x1==presence, 0x2==absence of a trait, for each pair of bits.
    //                  0x00 means don't care. 0x11 is invalid.
    public static final long BADGE_MASTER = 0x1 << 0;
    public static final long NOT_BADGE_MASTER = 0x1 << 1;
    // etc..

    // All bits off.
    public static final long BADGE_NONE = 0;


    // static methods only.
    private Badge() {}


}

package ms.airy.lib.core.auth.common;

import java.io.Serializable;


// Currently logged on Account + AuthToken, if any,
public class AccountAuthToken implements Serializable
{
//    // private Account account;
//    private final int accountType;
//    private final String accountName;
    private String authToken;
    // private Set<String> authScopeSet;
    private String authScopes;
    private long authTime;   // last "refreshed" time???
    // ???
    // private long expirationTime;
    // Used only if authToken == null
    private String failureReason;


//    public AccountAuthToken(int accountType, String accountName)
//    {
//        this(accountType, accountName, null, null, 0L);
//    }
//    public AccountAuthToken(int accountType, String accountName, String authToken, String authScopes, long authTime)
//    {
//        this.accountType = accountType;
//        this.accountName = accountName;
//        this.authToken = authToken;
//        this.authScopes = authScopes;
//        this.authTime = authTime;
//    }
    public AccountAuthToken()
    {
        this(null, null, 0L);
    }
    public AccountAuthToken(String authToken, String authScopes, long authTime)
    {
        this(authToken, authScopes, authTime, null);
    }
    public AccountAuthToken(long authTime, String failureReason)
    {
        this(null, null, authTime, failureReason);
    }
    public AccountAuthToken(String authToken, String authScopes, long authTime, String failureReason)
    {
        this.authToken = authToken;
        this.authScopes = authScopes;
        this.authTime = authTime;
        this.failureReason = failureReason;
    }

//    public int getAccountType()
//    {
//        return accountType;
//    }
//    public String getAccountName()
//    {
//        return accountName;
//    }

    public String getAuthToken()
    {
        return authToken;
    }
    public void setAuthToken(String authToken)
    {
        this.authToken = authToken;
    }

    public String getAuthScopes()
    {
        return authScopes;
    }
    public void setAuthScopes(String authScopes)
    {
        this.authScopes = authScopes;
    }

    public long getAuthTime()
    {
        return authTime;
    }
    public void setAuthTime(long authTime)
    {
        this.authTime = authTime;
    }

    public String getFailureReason()
    {
        return failureReason;
    }
    public void setFailureReason(String failureReason)
    {
        this.failureReason = failureReason;
    }


    // For debugging...
    @Override
    public String toString()
    {
        return "AccountAuthToken{" +
                "authToken='" + authToken + '\'' +
                ", authScopes='" + authScopes + '\'' +
                ", authTime=" + authTime +
                ", failureReason='" + failureReason + '\'' +
                '}';
    }
}

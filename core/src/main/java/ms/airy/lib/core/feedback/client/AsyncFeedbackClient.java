package ms.airy.lib.core.feedback.client;


public interface AsyncFeedbackClient
{
    void sendFeedback(long surveyEntryId, int answer, String comment);
}

package ms.airy.lib.core.app;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;


public final class VersionManager
{
    private static VersionManager INSTANCE = null;
    public static VersionManager getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new VersionManager(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;

    // "cache"
    private int versionCode = -1;
    private String versionName = null;

    private VersionManager(Context context)
    {
        this.context = context;
    }


    public int getVersionCode()
    {
        if(versionCode == -1) {
            initVersionVariables();
        }
        return versionCode;
    }
    public String getVersionName()
    {
        if(versionName == null) {
            initVersionVariables();
        }
        return versionName;
    }

    private void initVersionVariables()
    {
        String packageName = null;
        try {
            packageName = context.getPackageName();   // package name of this app.
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(packageName, 0);
            versionCode = packageInfo.versionCode;
            versionName = packageInfo.versionName;
        } catch(PackageManager.NameNotFoundException e) {
            Log.e("Failed to get the version code: packageName = " + packageName, e);
        }
    }


}

package ms.airy.lib.core.feedback.client.task;

import android.os.AsyncTask;

import ms.airy.lib.core.feedback.client.OnlineSurveyClient;
import ms.airy.lib.core.feedback.client.factory.FeedbackClientFactory;
import ms.airy.lib.core.feedback.common.SurveyEntry;


/**
 */
public class AsyncSurveyGetDailySurveyTask extends AsyncTask<Long, Void, SurveyEntry>
{
    private final AsyncTaskCallback callback;
    public AsyncSurveyGetDailySurveyTask(AsyncTaskCallback callback)
    {
        this.callback = callback;
    }


    // Store input params during processing
    private long time;

    @Override
    protected SurveyEntry doInBackground(Long... params)
    {
        time = params[0];

        OnlineSurveyClient client = FeedbackClientFactory.getInstance().getSurveyClient();

        SurveyEntry surveyEntry = client.getDailySurvey(time);

        return surveyEntry;
    }


    @Override
    protected void onPostExecute(SurveyEntry surveyEntry)
    {
        // super.onPostExecute(aLong);

        this.callback.onGetDailySurvey(surveyEntry, time);
    }




    // Callback interface
    public static interface AsyncTaskCallback
    {
        void onGetDailySurvey(SurveyEntry surveyEntry, long time);
    }

}

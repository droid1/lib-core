package ms.airy.lib.core.phaser.common;


// TBD: Do we need this?
//      Just use integer ????
public final class Level
{
    // Game Level definitions.
//    public static final int L00 = 0;
    public static final int L01 = 1;
    public static final int L02 = 2;
    public static final int L03 = 3;
    public static final int L04 = 4;
    public static final int L05 = 5;
    // ...

    // Inclusive
    public static final int MIN = L01;
    // Exclusive
    public static final int MAX = L05 + 1;

    // Static constants/methods only.
    private Level() {}


    public static boolean isValid(int level)
    {
        return (level >= MIN) && (level < MAX);
    }




    public static final class Range
    {
        private Integer min;   // Inclusive
        private Integer max;   // Exclusive

        public Range(Integer min, Integer max)
        {
            this.min = min;
            this.max = max;
        }

        public Integer getMin()
        {
            return min;
        }
        public Integer getMax()
        {
            return max;
        }
        public Integer setMax(Integer max)
        {
            Integer cur = this.max;
            this.max = max;
            return cur;
        }
    };


}

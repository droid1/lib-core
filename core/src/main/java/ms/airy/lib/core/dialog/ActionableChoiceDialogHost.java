package ms.airy.lib.core.dialog;

import android.app.DialogFragment;


public interface ActionableChoiceDialogHost
{
    DialogFragment addActionable(ActionableChoiceDialogListener actionable);
}

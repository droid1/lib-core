package ms.airy.lib.core.user;

import android.content.Context;

import ms.airy.lib.core.user.prefs.UserDevicePrefsStoreManager;


public final class UserManager
{
    private static UserManager INSTANCE = null;
    public static UserManager getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new UserManager(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;

    // "cache"
    private String userDeviceId = null;

    private UserManager(Context context)
    {
        this.context = context;
    }


    // tbd
    public String getUserDeviceId()
    {
        if(userDeviceId == null) {
            userDeviceId = UserDevicePrefsStoreManager.getInstance(context).getUserDeviceId();
        }
        return userDeviceId;
    }


}

package ms.airy.lib.core.contacts.loader;

import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.widget.CursorAdapter;


public class DefaultContactsDataCursorLoaderCallbacks extends BaseContactsDataCursorLoaderCallbacks
{
    public DefaultContactsDataCursorLoaderCallbacks(Context context, CursorAdapter cursorAdapter, String selectQuery)
    {
        super(context, cursorAdapter, selectQuery);
    }

    public DefaultContactsDataCursorLoaderCallbacks(Context context, CursorAdapter cursorAdapter, String selectQuery, String[] projection)
    {
        super(context, cursorAdapter, selectQuery, projection);
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args)
    {
        Log.w("onCreateLoader() called with id = " + id);



        Uri baseUri = ContactsContract.Contacts.CONTENT_URI;

        // ???
        Uri emailUri = ContactsContract.CommonDataKinds.Email.CONTENT_URI;


        // Now create and return a CursorLoader that will take care of
        // creating a Cursor for the data being displayed.
//        String select = "((" + ContactsContract.Contacts.DISPLAY_NAME + " NOTNULL) AND ("
//                + ContactsContract.Contacts.HAS_PHONE_NUMBER + "=1) AND ("
//                + ContactsContract.Contacts.DISPLAY_NAME + " != '' ))";

        return new CursorLoader(getContext(), baseUri,
                getProjection(), getSelectQuery(), null,
                ContactsContract.Contacts.DISPLAY_NAME_PRIMARY + " COLLATE LOCALIZED ASC");
    }



}

package ms.airy.lib.core.startup.task;

import android.app.Activity;
import android.content.Context;

import ms.airy.lib.core.startup.prefs.StartupPrefsStoreManager;


public class BaseForegroundStartupTask extends BaseStartupTask implements ForegroundStartupTask
{
    public BaseForegroundStartupTask(Context context)
    {
        super(context);
    }

    public BaseForegroundStartupTask(Context context, long id)
    {
        super(context, id);
    }


    // TBD:
    @Override
    public void run(Activity activity)
    {
        Log.i("BaseForegroundStartupTask.run()");

        // ???
        StartupPrefsStoreManager.getInstance(getContext()).increaseTaskRunCounter(getId());
    }

}

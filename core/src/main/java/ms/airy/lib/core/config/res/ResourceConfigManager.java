package ms.airy.lib.core.config.res;

import android.content.Context;
import android.content.res.Resources;

import java.util.HashMap;
import java.util.Map;

import ms.airy.lib.core.config.AbstractConfigManager;


/**
 * Config values from the res/values files...
 */
public class ResourceConfigManager extends AbstractConfigManager
{
    // "Cache"
    private final Map<String,Object> valueMap = new HashMap<String, Object>();

    public ResourceConfigManager(Context context)
    {
        super(context);
    }


    // TBD:
    // This implementation does not really make sense (e.g., very inefficient)
    // Is there a better way???

    @Override
    public Object get(String key)
    {
        if(valueMap.containsKey(key)) {
            return valueMap.get(key);
        }
        Object val = null;
        // Check the string type first!
        val = getString(key);
        if(val == null) {
            val = getBoolean(key);
            if(val == null) {
                val = getInt(key);
                if(val == null) {
                    val = getFloat(key);
                }
            }
        }
        return val;
    }

    @Override
    public Object get(String key, Object defVal)
    {
        Object val = get(key);
        if(val != null) {
            return val;
        } else {
            return defVal;
        }
    }

    @Override
    public String getString(String key)
    {
        if(valueMap.containsKey(key)) {
            return (String) valueMap.get(key);
        }
        int resId = getContext().getResources().getIdentifier(key, "string", getContext().getPackageName());
        if(resId > 0) {
            try {
                String val = getContext().getResources().getString(resId);
                valueMap.put(key, val);
                return val;
            } catch(Resources.NotFoundException e) {
                Log.d("String resource not found: resId = " + resId, e);
                return null;
            }
        } else {
            if(Log.I) Log.i("String config not found for key = " + key);
            return null;
        }
    }

    @Override
    public String getString(String key, String defVal)
    {
        String val = getString(key);
        if(val != null) {
            return val;
        } else {
            return defVal;
        }
    }


    public Boolean getBoolean(String key)
    {
        if(valueMap.containsKey(key)) {
            return (Boolean) valueMap.get(key);
        }
        // Note "bool" not "boolean"
        int resId = getContext().getResources().getIdentifier(key, "bool", getContext().getPackageName());
        if(resId > 0) {
            try {
                boolean val = getContext().getResources().getBoolean(resId);
                valueMap.put(key, val);
                return val;
            } catch(Resources.NotFoundException e) {
                Log.d("Boolean resource not found: resId = " + resId, e);
                return null;
            }
        } else {
            if(Log.I) Log.i("Boolean config not found for key = " + key);
            return null;
        }
    }
    public Integer getInt(String key)
    {
        if(valueMap.containsKey(key)) {
            return (Integer) valueMap.get(key);
        }
        int resId = getContext().getResources().getIdentifier(key, "integer", getContext().getPackageName());
        if(resId > 0) {
            try {
                int val = getContext().getResources().getInteger(resId);
                valueMap.put(key, val);
                return val;
            } catch(Resources.NotFoundException e) {
                Log.d("Integer resource not found: resId = " + resId, e);
                return null;
            }
        } else {
            if(Log.I) Log.i("Integer config not found for key = " + key);
            return null;
        }
    }
    public Float getFloat(String key)
    {
        if(valueMap.containsKey(key)) {
            return (Float) valueMap.get(key);
        }
            // ???
        int resId = getContext().getResources().getIdentifier(key, "fraction", getContext().getPackageName());
        if(resId > 0) {
            try {
                // ????
                float val = getContext().getResources().getFraction(resId, 1, 1);
                valueMap.put(key, val);
                return val;
            } catch(Resources.NotFoundException e) {
                Log.d("Float resource not found: resId = " + resId, e);
                return null;
            }
        } else {
            if(Log.I) Log.i("Float config not found for key = " + key);
            return null;
        }
    }


}


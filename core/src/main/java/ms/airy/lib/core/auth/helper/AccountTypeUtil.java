package ms.airy.lib.core.auth.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ms.airy.lib.core.auth.common.AccountType;


public final class AccountTypeUtil
{

    private AccountTypeUtil() {}

    // temporary.
    // types are ordered.
    // the first one should be the app's "principal" account type.
    // TBD: read this list from config....
    public static List<Integer> orderedTypeList = new ArrayList<Integer>();

    static {
        orderedTypeList.add(AccountType.TYPE_GOOGLE);
        orderedTypeList.add(AccountType.TYPE_FACEBOOK);
        orderedTypeList.add(AccountType.TYPE_TWITTER);
    }

    public static List<Integer> getOrderedTypeList()
    {
        return orderedTypeList;
    }

    // TBD: Does this work???
    // To be consistent across the app,
    //     every time this class is loaded, we need to call this method with the same list.
    // --> Is there a better way???
    public static void setOrderedTypeList(List<Integer> orderedTypeList)
    {
        orderedTypeList = orderedTypeList;
    }



    // mappings
    public static final Map<Integer, String> typeMap = new HashMap<Integer, String>();
    static {
        typeMap.put(AccountType.TYPE_GOOGLE, AccountType.STRING_GOOGLE);
        typeMap.put(AccountType.TYPE_FACEBOOK, AccountType.STRING_FACEBOOK);
        typeMap.put(AccountType.TYPE_TWITTER, AccountType.STRING_TWITTER);
        typeMap.put(AccountType.TYPE_YAHOO, AccountType.STRING_YAHOO);
        // etc. ...
    }

    public static final Map<String, Integer> reverseTypeMap = new HashMap<String, Integer>();
    static {
        reverseTypeMap.put(AccountType.STRING_GOOGLE, AccountType.TYPE_GOOGLE);
        reverseTypeMap.put(AccountType.STRING_FACEBOOK, AccountType.TYPE_FACEBOOK);
        reverseTypeMap.put(AccountType.STRING_TWITTER, AccountType.TYPE_TWITTER);
        reverseTypeMap.put(AccountType.STRING_YAHOO, AccountType.TYPE_YAHOO);
        // etc. ...
    }

    public static final Map<Integer, String> nameMap = new HashMap<Integer, String>();
    static {
        nameMap.put(AccountType.TYPE_GOOGLE, AccountType.NAME_GOOGLE);
        nameMap.put(AccountType.TYPE_FACEBOOK, AccountType.NAME_FACEBOOK);
        nameMap.put(AccountType.TYPE_TWITTER, AccountType.NAME_TWITTER);
        nameMap.put(AccountType.TYPE_YAHOO, AccountType.NAME_YAHOO);
        // etc. ...
    }

    public static final Map<String, Integer> reverseNameMap = new HashMap<String, Integer>();
    static {
        reverseNameMap.put(AccountType.NAME_GOOGLE, AccountType.TYPE_GOOGLE);
        reverseNameMap.put(AccountType.NAME_FACEBOOK, AccountType.TYPE_FACEBOOK);
        reverseNameMap.put(AccountType.NAME_TWITTER, AccountType.TYPE_TWITTER);
        reverseNameMap.put(AccountType.NAME_YAHOO, AccountType.TYPE_YAHOO);
        // etc. ...
    }


    // Note that we use orderedTypeList as a filter.
    public static boolean isValid(int type)
    {
        return orderedTypeList.contains(type);
    }

    public static boolean isValidString(String typeString)
    {
        return orderedTypeList.contains(reverseTypeMap.get(typeString));
    }

    public static boolean isValidName(String typeName)
    {
        return orderedTypeList.contains(reverseNameMap.get(typeName));
    }


    public static String getTypeString(int accountType)
    {
        if(typeMap.containsKey(accountType)) {
            return typeMap.get(accountType);
        } else {
            // ???
            return null;
        }
    }

    public static int getAccountType(String typeString)
    {
        if(reverseTypeMap.containsKey(typeString)) {
            return reverseTypeMap.get(typeString);
        } else {
            return AccountType.TYPE_NONE;   // ????
        }
    }

    public static String getTypeName(int accountType)
    {
        if(nameMap.containsKey(accountType)) {
            return nameMap.get(accountType);
        } else {
            // ???
            return null;
        }
    }

    // TBD:
    public static List<String> getAccountTypeStrings(int accountType)
    {
        // TBD:
        // based on accountType,
        // add all corresponding strings???
        // ...
        return null;
    }
}

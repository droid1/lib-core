package ms.airy.lib.core.popup.common;

import java.util.HashMap;
import java.util.Map;


/**
 * Interstitial (dialog) types.
 */
public final class PopupType
{
    // tbd
    // note that splash/ad types are special in that these popups may be shown repeatedly.
    // all other popup types are generally shown once (unless user action is delay/ignore, etc.)
    // public static final int PT_UNKNOWN = 0;
    public static final int PT_EULA = 1;          // for new user, or new updated eula. Or, "welcome" screen???
    public static final int PT_CONSENT = 2;       // e.g., "would you like to try new beta feature?", "would you like to log in?", etc.
    public static final int PT_INPUT = 3;         // Requesting user input (e.g., age, etc.)
    public static final int PT_CHANGE_LOGS = 4;   // after new version release.
    public static final int PT_ANNOUNCEMENT = 5;  // e.g., new version coming, etc.
    public static final int PT_NOTICE = 6;        // info/warning about this particular activity, new version already available, the feature requires network connection or higher user rank, etc.
    public static final int PT_QUICK_HELP = 7;    // e.g., tip of the day.
    public static final int PT_AD = 8;
    public static final int PT_SPLASH = 9;
    // ...

    private PopupType() {}


    // Precedence is like priority
    // but it is associated with popup type and not with individual popups.
    // Higher number means higher precedence.
    private static final Map<Integer, Integer> precedenceMap = new HashMap<Integer, Integer>();
    static {
        precedenceMap.put(PT_EULA, 100);
        precedenceMap.put(PT_CONSENT, 90);
        precedenceMap.put(PT_INPUT, 80);
        precedenceMap.put(PT_CHANGE_LOGS, 70);
        precedenceMap.put(PT_ANNOUNCEMENT, 60);
        precedenceMap.put(PT_NOTICE, 50);
        precedenceMap.put(PT_QUICK_HELP, 40);
        precedenceMap.put(PT_AD, 20);
        precedenceMap.put(PT_SPLASH, 10);
    }

    // default action type mapping
    private static final Map<Integer, Integer> actionTypeMap = new HashMap<Integer, Integer>();
    static {
        actionTypeMap.put(PT_EULA, ActionType.AT_POSITIVE_NEGATIVE);
        actionTypeMap.put(PT_CONSENT, ActionType.AT_POSITIVE_NEGATIVE_NEUTRAL);
        actionTypeMap.put(PT_INPUT, ActionType.AT_POSITIVE_NEGATIVE_NEUTRAL);
        actionTypeMap.put(PT_CHANGE_LOGS, ActionType.AT_POSITIVE_NEUTRAL);
        actionTypeMap.put(PT_ANNOUNCEMENT, ActionType.AT_POSITIVE_NEGATIVE_NEUTRAL);
        actionTypeMap.put(PT_NOTICE, ActionType.AT_POSITIVE_NEGATIVE_NEUTRAL);
        actionTypeMap.put(PT_QUICK_HELP, ActionType.AT_POSITIVE_NEUTRAL);
        actionTypeMap.put(PT_AD, ActionType.AT_POSITIVE_NEUTRAL);
        actionTypeMap.put(PT_SPLASH, ActionType.AT_NEUTRAL);
    }

    // default content type mapping.
    private static final Map<Integer, String> contentTypeMap = new HashMap<Integer, String>();
    static {
        contentTypeMap.put(PT_EULA, ContentType.CT_HTML);
        contentTypeMap.put(PT_CONSENT, ContentType.CT_TEXT);
        contentTypeMap.put(PT_INPUT, ContentType.CT_TEXT);
        contentTypeMap.put(PT_CHANGE_LOGS, ContentType.CT_HTML);
        contentTypeMap.put(PT_ANNOUNCEMENT, ContentType.CT_HTML);
        contentTypeMap.put(PT_NOTICE, ContentType.CT_TEXT);
        contentTypeMap.put(PT_QUICK_HELP, ContentType.CT_TEXT);
        contentTypeMap.put(PT_AD, ContentType.CT_HTML);
        contentTypeMap.put(PT_SPLASH, ContentType.CT_HTML);
    }

    // default values for max "repeats"
    // 0: invalid, -1: no limit, 1: no "repeat", ...
    private static final Map<Integer, Integer> maxRepeatsMap = new HashMap<Integer, Integer>();
    static {
        // temporary
        maxRepeatsMap.put(PT_EULA, 1);
        maxRepeatsMap.put(PT_CONSENT, 5);
        maxRepeatsMap.put(PT_INPUT, 5);
        maxRepeatsMap.put(PT_CHANGE_LOGS, 1);
        maxRepeatsMap.put(PT_ANNOUNCEMENT, 1);
        maxRepeatsMap.put(PT_NOTICE, 5);
        maxRepeatsMap.put(PT_QUICK_HELP, -1);
        maxRepeatsMap.put(PT_AD, -1);
        maxRepeatsMap.put(PT_SPLASH, -1);
    }


    // default values for "refractory period", in seconds.
    // this value is generally related to the action type.
    // and, it is only relevant when the user's response including reprompting,
    //          e.g., later (and, possibly, no or cancel), etc.
    // 0 means no refractory period.
    // -1 means infinite refractory period. ??? -> does it make sense???
    private static final Map<Integer, Integer> refractorySecsMap = new HashMap<Integer, Integer>();
    static {
        // temporary
        refractorySecsMap.put(PT_EULA, 0);
        refractorySecsMap.put(PT_CONSENT, 12 * 3600);
        refractorySecsMap.put(PT_INPUT, 2 * 24 * 3600);
        refractorySecsMap.put(PT_CHANGE_LOGS, 24 * 3600);
        refractorySecsMap.put(PT_ANNOUNCEMENT, 0);
        refractorySecsMap.put(PT_NOTICE, 24 * 3600);
        refractorySecsMap.put(PT_QUICK_HELP, -1);
        refractorySecsMap.put(PT_AD, 1 * 3600);
        refractorySecsMap.put(PT_SPLASH, 0);
    }


    // temporary
    public static int getDefaultType()
    {
        // ???
        return PT_SPLASH;
    }

    public static int getPrecedence(int popupType)
    {
        if(precedenceMap.containsKey(popupType)) {
            return precedenceMap.get(popupType);
        } else {
            // ???
            return 0;
        }
    }

    public static int getDefaultActionType(int popupType)
    {
        if(actionTypeMap.containsKey(popupType)) {
            return actionTypeMap.get(popupType);
        } else {
            // ???
            return ActionType.AT_UNKNOWN;
        }
    }

    public static String getDefaultContentType(int popupType)
    {
        if(contentTypeMap.containsKey(popupType)) {
            return contentTypeMap.get(popupType);
        } else {
            // ???
            return null;
        }
    }

    public static int getDefaultMaxRepeats(int popupType)
    {
        if(maxRepeatsMap.containsKey(popupType)) {
            return maxRepeatsMap.get(popupType);
        } else {
            // ???
            return 1;
        }
    }

    public static int getDefaultRefractorySecs(int popupType)
    {
        if(refractorySecsMap.containsKey(popupType)) {
            return refractorySecsMap.get(popupType);
        } else {
            // ???
            return 0;
        }
    }


    // returns -1, 0, 1 if typeOne's precedence is
    //         lower/equal/higher than that of typeTwo.
    public static int compareTypes(int typeOne, int typeTwo)
    {
        if(!precedenceMap.containsKey(typeOne) && !precedenceMap.containsKey(typeTwo)) {
            // ????
            return 0;
        } else if(!precedenceMap.containsKey(typeOne)) {
            return -1;
        } else if(!precedenceMap.containsKey(typeTwo)) {
            return 1;
        } else {
            if(precedenceMap.get(typeOne) < precedenceMap.get(typeTwo)) {
                return -1;
            } else if(precedenceMap.get(typeOne) > precedenceMap.get(typeTwo)) {
                return 1;
            } else {
                return 0;
            }
        }
    }


}

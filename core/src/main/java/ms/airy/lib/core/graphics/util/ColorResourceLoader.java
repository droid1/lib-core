package ms.airy.lib.core.graphics.util;

import android.content.Context;


// "Office hours"
public final class ColorResourceLoader
{
    private static ColorResourceLoader INSTANCE = null;
    public static ColorResourceLoader getInstance(Context context)
    {
        if(INSTANCE == null) {
            INSTANCE = new ColorResourceLoader(context.getApplicationContext());
        }
        return INSTANCE;
    }

    // Application context
    private final Context context;


    private ColorResourceLoader(Context context)
    {
        this.context = context;
    }


    // TBD:
    // ....

//    // ???
//    public int loadColor(int colorResId)
//    {
//        // What to return if loading fails???
//    }
    public int loadColor(int colorResId, int defaultColor)
    {
        int color = defaultColor;
        try {
            color = context.getResources().getColor(colorResId);
        } catch(Exception e) {
            // Ignore
            if(Log.I) Log.i("Color resources not found: colorResId = " + colorResId, e);
        }
        return color;
    }


}

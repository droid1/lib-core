package ms.airy.lib.core.phaser.spacetime;

// import ms.airy.lib.core.phaser.common.TimeLocation;

import java.util.HashMap;
import java.util.Map;


// Place holder for now.
// ....


// TBD:
// FeatureMap data should be stored to/from persistent storage such as config.
// Just using a singleton may not be sufficient.....
public final class TimeLocationHelper
{

//    // Map of feature Id -> { min TimeLocation (inclusive), max (exclusive) }.
//    // Min cannot be null, whereas max can be null, meaning there is no max.
//    // FeatureMap is open ended. That is,
//    // Features not in this map are supported for all TimeLocations >= minTimeLocation = 0;
//    private final Map<String, TimeLocation.Range> featureMap;

    public TimeLocationHelper()
    {
//        featureMap = new HashMap<String, TimeLocation.Range>();
        init();
    }
    private void init()
    {
        // TBD:
        // Populate the featureMap....
        // ....

//        Set<Feature> features = FeatureHelper.getInstance().getFeatures();
//        if(features != null && !features.isEmpty()) {
//            for(Feature f : features) {
//                featureMap.put(f.getCode(), new TimeLocation.Range(f.getMinTimeLocation(), f.getMaxTimeLocation()));
//            }
//        }
    }


    // Singleton
    private static final class TimeLocationHelperHolder
    {
        private static final TimeLocationHelper INSTANCE = new TimeLocationHelper();
    }
    public static TimeLocationHelper getInstance()
    {
        return TimeLocationHelperHolder.INSTANCE;
    }


    public void addFeature(String featureCode, Integer minTimeLocation, Integer maxTimeLocation)
    {
//        featureMap.put(featureCode, new TimeLocation.Range(minTimeLocation, maxTimeLocation));
    }

//    // Inclusive.
//    public int getMinTimeLocation(String featureCode)
//    {
//        if(! featureMap.containsKey(featureCode)) {
//            return TimeLocation.R01;
//        } else {
//            return featureMap.get(featureCode).getMin();
//        }
//    }
//    // Exclusive. ????
//    public int getMaxTimeLocation(String featureCode)
//    {
//        if(! featureMap.containsKey(featureCode)) {
//            return TimeLocation.MAX;
//        } else {
//            Integer max = featureMap.get(featureCode).getMax();
//            if(max == null) {
//                return TimeLocation.MAX;
//            } else {
//                return max;
//            }
//        }
//    }
//    public boolean hasMaxTimeLocation(String featureCode)
//    {
//        if(featureMap.containsKey(featureCode)) {
//            if(featureMap.get(featureCode).getMax() != null) {
//                return true;
//            }
//        }
//        return false;
//    }

//    public boolean isFeatureAvailable(String featureCode, int TimeLocation)
//    {
//        if(! featureMap.containsKey(featureCode)) {
//            return true;
//        }
//        return (getMinTimeLocation(featureCode) <= TimeLocation && getMaxTimeLocation(featureCode) > TimeLocation);
//    }


}

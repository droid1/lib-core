package ms.airy.lib.core.feedback.client;


public interface OnlineFeedbackClient
{
    // TBD: Feedback message id ???
    void sendFeedback(long surveyEntryId, int answer, String comment);
}
